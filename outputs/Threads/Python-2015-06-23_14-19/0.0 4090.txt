
============================================================================
Subject: [Python-Dev] Issue 18312 "fix" broke buildbots
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Issue 18312 "fix" broke buildbots
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
I'm not sure how my change broke the buildbots, but apparently it did. I
need to run to a meeting now, but I can roll this back in the next few
hours.

If someone else wants to roll it back before I get to it, feel free.

Sorry about the problem. I tested it locally, I'm not sure how the
buildbots are affected.

Eric.



----------------------------------------
Subject:
[Python-Dev] Issue 18312 "fix" broke buildbots
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 02 Jul 2013 09:52:56 -0400, "Eric V. Smith" <eric at trueblade.com> wrote:

I'm no longer sure it was your patch.  Anyone who wants to look
at the buildbots please do:

http://buildbot.python.org/all/builders/x86%20Tiger%202.7/builds/2030
http://buildbot.python.org/all/builders/x86%20Tiger%203.3/builds/742
http://buildbot.python.org/all/builders/x86%20Tiger%203.x/builds/6522
http://buildbot.python.org/all/builders/AMD64%20FreeBSD%209.0%20dtrace%2Bclang%203.3/builds/699
http://buildbot.python.org/all/builders/x86%20Windows7%202.7/builds/2125
http://buildbot.python.org/all/builders/bolen-dmg-3.x/builds/278
http://buildbot.python.org/all/builders/x86%20XP-4%203.3/builds/832

I haven't looked at all of these.  The blamelist cites Richard Oudkerk,
but at least one of the logs I looked at was about @test files, in
a number of different tests.

I don't really have time to look at it firther right now either, I've
got a client with an issue that needs solved asap...

--David



----------------------------------------
Subject:
[Python-Dev] Issue 18312 "fix" broke buildbots
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
On 07/02/2013 10:33 AM, R. David Murray wrote:

I've looked at a few of these, and they don't seem related to my change.
I'll review it some more this afternoon.

Eric.





----------------------------------------
Subject:
[Python-Dev] Issue 18312 "fix" broke buildbots
----------------------------------------
Author: David Bole
Attributes: []Content: 
"Eric V. Smith" <eric at trueblade.com> writes:

(...)

The above set (which are all on the x86 Tiger buildbot) appear due to some
sort of filesystem corruption on the buildbot machine, so it appears that
the timing of the failures and particular changesets is coincidental.

I've repaired the filesystem and the disk passes a surface scan, so
hopefully it'll be cleaned up.  I've also re-queued the most recent builds
in each branch.

-- David


