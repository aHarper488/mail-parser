
============================================================================
Subject: [Python-Dev] re.findall() should return named tuple
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] re.findall() should return named tuple
----------------------------------------
Author: Philipp A
Attributes: []Content: 
hi devs,

just an idea that popped up in my mind: re.findall() returns a list of
tuples, where every entry of each tuple represents a match group.
since match groups can be named, we are able to use named tuples instead of
plain tuples here, in the same fashion as namedtuple?s rename works:
misssing group names get renamed to _1 and so on. i suggest to add the
rename keyword option, to findall, defaulting to True, since mixed
positional and named tuples are more common than in usual use cases of
namedtuple.

do you think it?s a good idea?

finally: should i join the mailing list to see answers? should i file a
PEP? i have no idea how the inner workings of python development are, but i
wanted to share this idea with you :)

thanks for keeping python great,
philipp
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111208/c9e3df2c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] re.findall() should return named tuple
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/8/2011 8:31 AM, Philipp A. wrote:

I have not used named tuples or re.findall (much), so I have no opinion).


Ideas like this should either go the the python-ideas list or to the 
tracker at bugs.python.org as a feature request. If you post to the 
list, you should either subscribe at mail.python.org or follow it as a 
newsgroup at news.gmane.org (which is what I do). Posting a tracker 
issue requires registration of some sort.

-- 
Terry Jan Reedy



