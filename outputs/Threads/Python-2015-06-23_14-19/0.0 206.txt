
============================================================================
Subject: [Python-Dev] Remove "unit test needed"
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Remove "unit test needed"
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

I would like to see ?unit test needed? removed from the workflow menu in
the bug tracker. The reason is that we don't do test-driven development
(or, at least, most of us don't) and this stage entry is therefore
useless and confusing. Saying to someone that an unit test is needed
happens during the patch review - it isn't a separate stage in itself.

The reason I'm asking is that I've seen some triagers bumping a lot of
issues to ?unit test needed? lately, and I find this annoying. What we
need is patches, not unit tests per se.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Remove "unit test needed"
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Thu, Aug 12, 2010 at 12:56 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Is that stage supposed to (at least partly) capture the idea
'reproducible test-case needed', or 'verification needed'?  That would
seem like a more useful notion.

Mark



----------------------------------------
Subject:
[Python-Dev] Remove "unit test needed"
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Aug 12, 2010 at 9:18 AM, Mark Dickinson <dickinsm at gmail.com> wrote:
..
..
+1

I have two problems with  ?unit test needed?:

1.  Similar to Antoine, I find it ambiguous whether a bug without a
patch is in ?unit test needed? or "patch needed" stage.

2.  I much prefer a small script reproducing the bug to a regression
suite patch.  In most cases unit tests require too much irrelevant
scaffolding to be useful in understanding or debugging the issue.

I remember there was an idea somewhere to replace "patch" tag with a
check-list with boxes for code, tests, and docs.  I think that would
be better than "unit test needed" stage.



----------------------------------------
Subject:
[Python-Dev] Remove "unit test needed"
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Thu, Aug 12, 2010 at 11:02 AM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

Are you suggesting check boxes for what's needed, or for what's present?

Boxes indicating the needs would probably be most useful.  These
should probably all be set by default on new issues.

Using keywords would be acceptable, I think.  Similar to "needs review".


? -Fred

--
Fred L. Drake, Jr.? ? <fdrake at gmail.com>
"A storm broke loose in my mind."? --Albert Einstein



----------------------------------------
Subject:
[Python-Dev] Remove "unit test needed"
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Aug 12, 2010 at 1:31 PM, Fred Drake <fdrake at acm.org> wrote:
Please see  http://wiki.python.org/moin/DesiredTrackerFeatures .
Ezio, thanks for the link.



----------------------------------------
Subject:
[Python-Dev] Remove "unit test needed"
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 12 Aug 2010 11:02:29 -0400
Alexander Belopolsky <alexander.belopolsky at gmail.com> wrote:

To me the simpler the better. If there's a patch then it's the
reviewer's task to point out what's missing - and that's
context-dependent: some fixes don't need a doc change or a test case
(or, at least, they can do without).

The rationale is that the more complex a system is, the less likely we
are to use it. Our current "keywords" are severely under-used.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Remove "unit test needed"
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 12/08/2010 12:56, Antoine Pitrou wrote:

I often see patches without a test, and have assumed this is what this 
stage is for - where a patch is provided without a corresponding test.

On the other hand checkboxes for fix / test / docs sounds fine.

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.



