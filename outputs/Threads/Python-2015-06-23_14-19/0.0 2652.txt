
============================================================================
Subject: [Python-Dev] Handling deprecations in the face of PEP 384
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
As I clean up Python/import.c and move much of its functionality into
Lib/imp.py, I am about to run into some stuff that was not kept private to
the file. Specifically, I have PyImport_GetMagicTag() and NullImporter_Type
which I would like to chop out and move to Lib/imp.py.

PyImport_getMagicTag(), correct (assuming I follow through with this; I
might not bother)? What about NullImporter_Type (it lacks a Py prefix so I
am not sure if this is considered public or not)?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120420/033de166/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Apr 20, 2012 at 6:59 PM, Brett Cannon <brett at python.org> wrote:

Yeah, PyImporter_GetMagicTag() looks like a public API, parallel with
PyImporter_GetMagicNumber(). Maybe it was accidentally not documented?
I'm not sure when it was introduced. Should we even deprecate it? I'd
say do the same thing you're doing for GetMagicNumber().

NullImporter_Type looks like it was accidentally not made static, so
don't fret about that.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Apr 21, 2012 at 12:16 PM, Guido van Rossum <guido at python.org> wrote:

I'd keep it and just make it a convenience wrapper for the call back
into the Python code.


Yeah, the lack of the Py_ prefix suggests this one being visible is
just an accident of the implementation, and the name is unusual enough
that it never caused a symbol collision for any third parties.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 20, 2012, at 09:59 PM, Brett Cannon wrote:


I'd have to go back into my archives for the discussions about the PEP, but my
recollection is that we intentionally made PyImport_GetMagicTag() a public API
method.  Thus no leading underscore.  It's a bug that it's not documented, but
OTOH, it's unlikely there are, or would be, many consumers for it.

Strictly speaking, I do think you need to deprecate the APIs.  I like Nick's
suggestion to make them C wrappers which just call back into Python.

-Barry



----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

All that PEP 384 gives you is that you MAY deprecate certain API
(namely, all API not guaranteed as stable). If an API is not in the
restricted set, this doesn't mean that it SHOULD be deprecated at
some point. So there is no need to deprecate anything.

OTOH, if the new implementation cannot readily support the
API anymore, it can certainly go away. If it was truly private
(i.e. _Py_*), it can go away immediately. Otherwise, it should be
deprecated-then-removed.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sat, Apr 21, 2012 at 16:55, "Martin v. L?wis" <martin at v.loewis.de> wrote:


I meant "at least deprecate" as in "I can't just remove it from Python 3.3".

-Brett


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120421/f8cad7d0/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sat, Apr 21, 2012 at 12:10, Barry Warsaw <barry at python.org> wrote:


That was my plan, but the amount of code it will take to wrap them is
making me not care. =) For PyImport_GetMagicTag() I would need to expose a
new attribute on sys or somewhere which specifies the VM name. For
PyImport_GetMagicNumber() I have to do a bunch of bit twiddling to convert
a bytes object into a long which I am just flat-out not in the mood to
figure out how to do. And all of this will lead to the same amount of C
code as there currently is for what is already implemented, so I just don't
care anymore. =)

But I'm glad the clarifications are there about the stable ABI and how we
are handling it.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120421/4781809b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Sat, Apr 21, 2012 at 4:17 PM, Brett Cannon <brett at python.org> wrote:

I thought I already (mostly) worked it all out in that patch on
issue13959.  I felt really good about the approach for the magic tag
and magic bytes.

Once find_module() and reload() are done in imp.py, I'm hoping to
follow up on a few things.  That includes the unresolved mailing list
thread about sys.implementation (or whatever it was), which will help
with the magic tag.  Anyway, I don't want to curtail the gutting of
import.c quite yet (as he hears cries of "bring out your dead!").

-eric


p.s.  I understand your sentiment here, considering that mothers are
often exhausted by childbirth and the importlib bootstrap was a big
baby.  You were in labor for, what, 6 years.  <wink>  [There's an
analogy that could keep on giving. :) ]



----------------------------------------
Subject:
[Python-Dev] Handling deprecations in the face of PEP 384
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sat, Apr 21, 2012 at 20:54, Eric Snow <ericsnowcurrently at gmail.com>wrote:


You didn't update Python/import.c in your patches so that the public C API
continued to function. That's what is going to take a bunch of C code to
continue to maintain, not the Python side of it.



Even w/ all of that gutted, a decent chunk of coding is holding on to dear
life thanks to PyImport_ExecCodeModuleObject() (and those that call it).
IOW the C API as it is currently exposed is going to end up being the
limiting factor of how many lines get deleted in the very end.



It's also about maintainability. It isn't worth upping complexity just to
shift some stuff into Python code, especially when it is such simple stuff
as the magic number and tag which places practically zero burden on other
VMs to implement.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120421/6d11aab3/attachment.html>

