
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.2): Issue #11919: try
 to fix test_imp failure on some buildbots.
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Issue #11919: try
 to fix test_imp failure on some buildbots.
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
This seems to be changing what is tested -- are you saying that
filenames with an included directory name are not intended to be
supported?

On 4/25/11, antoine.pitrou <python-checkins at python.org> wrote:



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Issue #11919: try
 to fix test_imp failure on some buildbots.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le mardi 26 avril 2011 ? 10:03 -0400, Jim Jewett a ?crit :

I don't know, but that's not the point of this very test.
(I also find it a bit surprising that find_module() would accept a
module name - and not a filename - containing a slash and treat it as
some kind of directory path)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Issue #11919: try
 to fix test_imp failure on some buildbots.
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mardi 26 avril 2011 ? 10:03 -0400, Jim Jewett a ?crit :

The test checks the Python parser, not the imp module :-)

I don't understand why: sometimes, find_module() accepts a (relative)
path, sometimes it doesn't.

Victor


