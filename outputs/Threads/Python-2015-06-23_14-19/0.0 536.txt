
============================================================================
Subject: [Python-Dev] 2.7 hg mirror
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] 2.7 hg mirror
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 4 Jul 2010 10:34:57 -0500
Benjamin Peterson <benjamin at python.org> wrote:


A Mercurial mirror is also available for the 2.7 maintenance branch:
http://code.python.org/hg/branches/release2.7-maint/
As usual, the full list of mirrors can be viewed at:
http://code.python.org/hg

Regards

Antoine.



