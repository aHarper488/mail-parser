
============================================================================
Subject: [Python-Dev] Assigned To field usage
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Assigned To field usage
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
R. David Murray wrote:

That sounds like a fair description of the way I use it as well. The
most common case where I will assign a bug directly to someone is if I
want a yea or nay from the release manager in deciding whether or not
something is acceptable for inclusion in a beta or rc release.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Assigned To field usage
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 


I personally think issues should not get assigned unless the person it
is being assigned to is known to accept such assignments, e.g. by having
asked for them to happen.

For example, I would prefer not to be assigned any issues, because I'm
unlikely to act on them in the six months - unless, as David says, I'm
the *only* person who could move the issue forward in that time.

Regards,
Martin

