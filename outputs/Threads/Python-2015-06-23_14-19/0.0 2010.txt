
============================================================================
Subject: [Python-Dev] unittest.main() --catch parameter
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] unittest.main() --catch parameter
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
I am looking at --help of test runner and asking the question: what is
the use case for -c, --catch option? It doesn't look like it should be
present in generic runner. I also can't find reasons to waste short
option for it. There will be big problems with people complaining
about BC break even if this option is not used by anyone.

Usage: tests.py [options] [test] [...]

Options:
  -h, --help       Show this message
  -v, --verbose    Verbose output
  -q, --quiet      Minimal output
  -f, --failfast   Stop on first failure
  -c, --catch      Catch control-C and display results
  -b, --buffer     Buffer stdout and stderr during test runs


--
anatoly t.



----------------------------------------
Subject:
[Python-Dev] unittest.main() --catch parameter
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 03/03/2011 20:31, anatoly techtonik wrote:
It catches keyboard interrupt and instead of just bombing out of the 
test run it reports all the results collected so far.

Without this option interrupting a test run with a ctrl-c kills the run 
and reports nothing. Seeing an unexpected failure or error during a long 
test run and having to wait to the end of the test run to see the 
traceback can be annoying, this feature solves that problem.


Nose, django and other test runners provide this option, so it is 
functionality that people seem to value.


I don't understand this sentence, sorry.

All the best,

Michael Foord


-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] unittest.main() --catch parameter
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Thu, Mar 3, 2011 at 10:44 PM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

Why not just leave this behavior by default and just return -1 if the
Ctrl-C was pressed?


If the option is useless, people won't allow to remove it, because it
will break "backward compatibility" (BC), even if they don't use this
option themselves.




----------------------------------------
Subject:
[Python-Dev] unittest.main() --catch parameter
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 03/03/2011 21:54, anatoly techtonik wrote:

Because it means installing a signal handler which is not necessarily 
appropriate for all test systems. We *could* make it the default in the 
future (for the test runner only - not when unittest is used via the api).


So you want it on by default but are also worried about the backwards 
compatibility issues of it even existing as an option? Anyway, your 
assertion that the option is or may be useless is unfounded. Don't worry 
about it.

All the best,

Michael Foord



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] unittest.main() --catch parameter
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Thu, Mar 3, 2011 at 11:58 PM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

What does it mean - "not approriate"? Do signal handlers crash or kill
people on some test systems? If tests are run with output buffering
then it's reasonable to turn off handling of Ctrl-C. If they are
executed by buildbots, I don't see how it hurts the test process. In
any case there should be a way to turn Ctrl-C handing using some
internal flag, but moving it into user command doesn't seem like a
good idea to me.

By the way, is calling unittest.main() is using unittest via api?


I am worried that I won't have space to add more useful options to the
runner or they will be lost for users in the abundance of highly
technical parameters that runner provides. I am concerned that users
will never ever understand the true Awesomeness of the New Runner
(tm). ;)
-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] unittest.main() --catch parameter
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 04/03/2011 01:33, anatoly techtonik wrote:

Because the system under test may use its own signal handlers.

What's more relevant are the following two facts:

* It isn't guaranteed to work, the keyboard interrupt can interrupt code 
at any arbitrary point so the system *could* be in an unstable state and 
result reporting may then fail.

* It changes the default behaviour of pressing control-c which normally 
stops code immediately. This continues to execute an arbitrary amount of 
code after pressing control-c.

For all these reasons I think it is better to make it a user option than 
on by default. Besides which it is already done and released in two 
major versions of Python (2.7 & 3.2).

(The behaviour *is* configurable / controllable via the api - which I 
guess is what you mean by "some internal flag". The command line option 
is the way to control it when using the default runner and not the api.)


Not really, that's the standard test runner. Perhaps slightly debatable.


:-)

Well there are only 52 short alphabetical options available, but yes we 
should be careful with them and I'm not proposing adding any more at the 
moment.

All the best,

Michael Foord

-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html


