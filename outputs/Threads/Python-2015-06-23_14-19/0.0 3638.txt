
============================================================================
Subject: [Python-Dev] cpython (3.2): add gc_collects to weakref tests
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.2): add gc_collects to weakref tests
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 13 Nov 2012 21:26:51 +0100 (CET)
philip.jenvey <python-checkins at python.org> wrote:

While this is necessary for non-refcounted implementations, it does
relax the requirement quite a bit for CPython (absence of reference
cycles can be a significant feature). I think perhaps gc_collect()
should be called only for non-CPython implementations.

Regards

Antoine.



