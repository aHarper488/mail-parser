
============================================================================
Subject: [Python-Dev] configparser 1.1 - one last bug fix needed
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: =?utf-8?Q?=C5=81ukasz_Langa?
Attributes: []Content: 
Hi there.
 
There's one last thing that needs to be done with configparser for 3.2.
Raymond, Fred, Michael and Georg already expressed their approval on that so
unless anybody finds a flaw in the idea expressed below, I'm going to make
the change for 3.2b2:

- the ConfigParser class will be removed

- the SafeConfigParser class will be renamed to ConfigParser

- 2to3 will rename SafeConfigParser classes to ConfigParser

- 2to3 will warn on the subtle behaviour change when ConfigParser classes
  are found

What's the difference?
----------------------

Both ConfigParser and SafeConfigParser implement interpolation, e.g.  option
values can contain special tokens similar to those implemented by Python's
string formatting: %(example)s. These tokens are replaced during get()
operations by values from respective keys (either from the current section
or from the DEFAULT section).

SafeConfigParser was originally introduced to fix a couple of ConfigParser
problems:

- when a token didn't match the %(name)s syntax, it was simply treated as
  a raw value. This caused configuration errors like %var or %(no_closing_s)
  to be missed.

- if someone actually wanted to store arbitrary strings in values, including
  Python formatting strings, there was no way to escape %(name)s in the
  configuration. The programmer had to know in advance that some value may
  hold %(name)s and only get() values from that option using 
  
  get('section', 'option', raw=True)

  Then however, that option could not use interpolation anymore.

- set() originally allowed to store non-string values in the parser. This
  was not meant to be a feature and caused trouble when the user tried to
  save the configuration to a file or get the stored values back using typed
  get() methods.

SafeConfigParser solves these problems by validating interpolation syntax
(only %(name)s or %% are allowed, the latter being an escaped percent sign)
and raising exceptions on syntax errors, and validating type on set()
operations so that no non-string values can be passed to the parser using
the API.

Why change that?
----------------

When ConfigParser was left alone, it remained the default choice for most
end users, including our own distutils and logging libs. This was a very
weak choice, and most current ConfigParser users are not aware of the
interpolation quirks. I had to close a couple of issues related to people
trying to store non-string values internally in the parser.

The current situation needlessly complicates the documentation. Explaining
all the above quirks to each new user who only wants to parse an INI file is
weak at best. Moreover, users trust Python to do the right thing by default
and according to their intuition. In this case, going for the default
configparser.ConfigParser class without consulting the documentation is
clearly a suboptimal choice.

One last argument is that SafeConfigParser is an awkward name. It implicates
the other parsers are somehow unsafe, or that this specific parser protects
users from something. This is generally considered a naming antipattern.

When?
-----

You might ask whether this can be done for 3.2 (e.g. is that a feature or
a bugfix). In Raymond's words, the beta process should be used to flesh out
the APIs, test whether they work as expected and fix suboptimal decisions
before we hit the release candidate stage. He consideres this essentially
a bugfix and I agree.

You might ask why do that now and not for 3.3. We believe that 3.2 is the
last possible moment of introducing a change like that. The adoption rate is
currently still low and application authors porting projects from 2.x expect
incompatibilities. When they are non-issues, handled by 2to3, there's
nothing to be afraid of.

But isn't that... INCOMPATIBLE?!
--------------------------------

Yes, it is. Thanks to the low py3k adoption rate now's the only moment where
there's marginal risk of introducing silent incompatibility (there are
hardly any py3k projects out there). Projects ported from Python 2.x will be
informed by 2to3 of the change. We believe that this will fix more bugs than
it introduces.

Support for bare % signs would be the single case where ConfigParser might
have appeared a more natural solution. In those cases we expect that users
will rather choose to turn off interpolation whatsoever.

Summary
-------

If you have any strong, justified arguments against this bugfix, speak up.
Otherwise the change will be made on Thursday.

-- 
Interpolating regards,
?ukasz Langa
tel. +48 791 080 144
WWW http://lukasz.langa.pl/





----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
+1 from me.

If anyone complains too much, perhaps we can offer to retain the old
ConfigParser as "_BuggyConfigParser"? (that idea is only partially
tongue-in-cheek...)

Regards,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101214/a6db8361/attachment.html>



----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: =?utf-8?Q?=C5=81ukasz_Langa?
Attributes: []Content: 
Wiadomo?? napisana przez Nick Coghlan w dniu 2010-12-14, o godz. 01:20:


We may leave the LegacyInterpolation class if someone *really* needs to support that. That way RawConfigParser(interpolation=LegacyInterpolation()) is effectively the old ConfigParser.

-- 
Nondeprecatorily yours,
?ukasz Langa
tel. +48 791 080 144
WWW http://lukasz.langa.pl/

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101214/80a63eb4/attachment.html>



----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 14 Dec 2010 10:20:04 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:


Or we can put it in the "buggy" module which receives all buggy code.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Antoine Pitrou wrote:

The good thing about that idea is that maintenance of buggy.py will be 
so simple!



-- 
Steven




----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Tue, Dec 14, 2010 at 6:24 AM, Steven D'Aprano <steve at pearwood.info> wrote:

It's self-describing, and needs no further documentation.  :-)


? -Fred

--
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"A storm broke loose in my mind."? --Albert Einstein



----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Dec 14, 2010, at 3:38 AM, Fred Drake wrote:


And psychologically more effective than deprecation.
Anyone writing "import buggy" probably feels like they
have got some explaining to do :-)


Raymond
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101214/aad9acf5/attachment.html>



----------------------------------------
Subject:
[Python-Dev] configparser 1.1 - one last bug fix needed
----------------------------------------
Author: Tim Golde
Attributes: []Content: 
On 14/12/2010 17:01, Raymond Hettinger wrote:

They might be intro retro-robotics:

   http://www.anf.nildram.co.uk/beebcontrol/buggies/bbc_buggy/index.html

(For those not in the UK, this was a very popular schools
project kit on the back of the famous BBC micro initiative)

TJG

