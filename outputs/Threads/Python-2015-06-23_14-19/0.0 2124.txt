
============================================================================
Subject: [Python-Dev] devguide: managing "+1 heads"
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devguide: managing "+1 heads"
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

As far as I remember, python-dev decided that each branch should have a
single head. We probably have even a push hook to avoid mistakes. Or we
should :).

But we don't explain what is suppose to be done when a developer is
working in a feature, she updates her repository clone, rebase her
patches, collapses all her local changesets to a single giant changeset,
commit it locally, merge them to "default" and then she tries to push.
But somebody else "raced" her and commit first, so she is trying to
generate a new head.

The standard approach in mercurial is for her to pull the changes and to
do a merge before trying to push again (and hope nobody else "raced" her
again, this time). But the merge will be visible in the project history,
and I have the feeling that python-dev tries hard to keep a lineal history.

Another usual approach would be to "pull" the changes and "rebase" the
local unpushed changesets before trying again, to get a linear history.
But no idea of what would happen when we have multiple local commits in
several different branches. Never tried in this situation, and not sure
that I want to try :).

In any case, devguide is silent about this. I would suggest to write
something to fill this gap.

See, for instance, <http://hg.python.org/cpython/rev/9a817ab166e0>,
<http://hg.python.org/cpython/graph/9a817ab166e0>.

PS: I didn't merged 9a817ab166e0 to "3.x" because I don't know what
plans Raymond has for 6544accfefc3. I guess he is waiting for buildbot
results before merging to 3.x.

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/

jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBTYALuplgi5GaxT1NAQIpwgP/U7MTC59QKOn5/4PgFEe3L1q7tsiAs6Tb
jLYhL8vXBjyY3Ct5bOXW9MBcagHa+Bk/hz/ohGcGP+PL3ZtqgXE9Zv6ZXRydnhWb
GslPQnCnHjp8KMa3iE6wDMRskY46iDQtVh1QOo9UTi001jn5mqo2CLDugmFHVU+l
lFEnXBCotGo=
=SBiW
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] devguide: managing "+1 heads"
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 16 Mar 2011 02:00:42 +0100
Jesus Cea <jcea at jcea.es> wrote:

This is indeed the standard approach, so I'm not sure what the point of
mentioning it in the devguide would be. I don't think the devguide
should turn into a Mercurial tutorial: there are plenty of them on the
Web.

Regardless, if you want to experiment with other approaches, please go
ahead and report the results here.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: managing "+1 heads"
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 16/03/11 02:15, Antoine Pitrou wrote:

My point is that I was getting the "+1 head" warning. I know how to
solve it by myself, but I checked the devguide to learn if there was
some kind of policy about it. The devguide is silent about it.

Maybe a simple "try to keep the history lineal, as possible" and "feel
free to merge heads in the standard mercurial way".

In fact, we have discussed here the approach of collapsing local changes
to a single changeset when pushing to the central repository, but I
don't see any reference to this policy in the devguide.

Unwritten "culture" is not good. Better for new contributors to read a
document and learn "the way" that to pick some details here, some there
after stepping over a lot of toes.

For instance, merging between branches (in which direction) is
established here, but not in the devguide.

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBTYAUUZlgi5GaxT1NAQJp8wQAmuj5QLohUvAGRetajubTlizIMhgUEk1l
9kYn9XcfdtETHkM2t3Fmi73FscslNWXTT11kR1rqoyJUjS7XklcPGYtKQqWBAo+b
qICXLKo6C150lRe5VRDWBlJCvUTpFGQddh3ouTjfPjW43sO1Sj/OWJb4H1tSkyjL
smKW5SaSnXE=
=46ku
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] devguide: managing "+1 heads"
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/3/15 Jesus Cea <jcea at jcea.es>:

What are you talking about?
http://docs.python.org/devguide/committing.html#forward-porting



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] devguide: managing "+1 heads"
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 16 Mar 2011 02:37:21 +0100
Jesus Cea <jcea at jcea.es> wrote:

Well, can you propose a patch to add or improve wording?


It is:
http://docs.python.org/devguide/committing.html#forward-porting

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: managing "+1 heads"
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 16/03/11 02:42, Benjamin Peterson wrote:

I beg your pardon. 3AM in Spain. Time to get some sleep, it seems...

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBTYAWOZlgi5GaxT1NAQJD6gP5AbqT+PP/1DPmzVUXy0iqWuy+HE1PJC6X
dBzvE3abcqCsCEIhmqn8NW//jXUzGZ162dF+3xJ9IViCZhOol3n5d+E/Yna67/uh
48LJIV0bUrXDkg/nm0/VJGyx0GUqFx546mv9M3dsjMOxd7Q6ZbSfCp/clyWa8Drn
IwfdJMkGnAI=
=NW3O
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] devguide: managing "+1 heads"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Mar 15, 2011 at 9:15 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Given our stated preference for pushing collapsed changesets, it does
seem worthwhile to explicitly mention the pull/merge/push case as a
case where a collapsed changeset isn't expected.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

