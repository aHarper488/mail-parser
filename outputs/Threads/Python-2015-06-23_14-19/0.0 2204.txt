
============================================================================
Subject: [Python-Dev] cpython (3.1): #2650: Refactor re.escape to use
 enumerate().
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.1): #2650: Refactor re.escape to use
 enumerate().
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 26.03.2011 20:00, schrieb Terry Reedy:

No - I believe he is critizing that a stylistic change is done
in a maintenance branch. It's not a bug fix, AFAICT, so it should not
have been done.

Regards,
Martin

P.S. I haven't looked into the specific context, but the diff alone
may actually cause behavior changes, depending on what pattern exactly
is.



----------------------------------------
Subject:
[Python-Dev] cpython (3.1): #2650: Refactor re.escape to use
 enumerate().
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
On 27/03/2011 0.03, Georg Brandl wrote:

Hi,
these commits are part of #2650[0].
First, I refactored the existing tests[1] and added a few more tests[2] 
to have better coverage.  Tests are usually ported to maintenance 
branches as well (because they could uncover bugs and also make merging 
easier), so I started working on 3.1.
Then I refactored the function[3], and since the refactoring was trivial 
and I had extensive tests to make sure that the behavior was unchanged I 
included the refactoring in 3.1 too.

FWIW I've been porting most of the commits that I do on 3.2 on 3.1 too 
(i.e. I'm considering both of them maintenance branches), and merging 3 
branches rather than 2 doesn't make much difference with mercurial.

[0]: http://bugs.python.org/issue2650
[1]: http://hg.python.org/cpython/rev/1402c719b7cf
[2]: http://hg.python.org/cpython/rev/9147f7ed75b3
[3]: http://hg.python.org/cpython/rev/ed02db9921ac

Best Regards,
Ezio Melotti




