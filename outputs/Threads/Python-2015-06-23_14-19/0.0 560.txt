
============================================================================
Subject: [Python-Dev] Call for Applications - PSF Sponsored Sprints
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Call for Applications - PSF Sponsored Sprints
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
[Sending this to Python-Dev, as it might very well be of interest as the sprints
are focused on Python "core" tasks]

The PSF is happy to open our first call for applications for sprint funding!

Have you ever had a group of people together to hack towards a common goal?
You've hosted a sprint!

Have you ever wanted to get a group of like minded Pythonistas together to hack
for a day? You're going to want to hold a sprint!

Whether you call them Sprints, Hackfests, Hack-a-thons, or any other name,
they're a great way to hang out with like-minded developers and work on common
code. Sprints are an unbeatable way to build friendships and contacts that will
last for years to come, and they're a great way to learn about something new if
you're just starting out.

The Python Software Foundation has set aside funds to be distributed to
world-wide sprint efforts. We're anticipating 2-3 events per month focused on
covering topics to help the entire community:

 - Python Core bug triage and patch submission (on-boarding new contributors)
 - Python Core documentation (including process documentation) improvements
 - Porting libraries/applications to Python 3
 - Python website/wiki content improvements
 - PyPI packaging hosting site improvements
 - Contribution to other "core" projects, such as packaging related issues.

If you are interested in holding a sprint on any of the topics above and you're
looking for some money to help out with sprint costs, we can help (up to a max
of $250 USD). Prepare an application including the following information:

 - Date and Location: Where will the event be? What day and time?
 - Organizers: Who are the event organizers and sprint coach? Is the sprint
   being run by a Python user group?
 - Attendees: How many participants do you expect?
 - Goal: What is the focus and goal of the sprint?
 - Budget: How much funding you are requesting, and what will you use it for?
 - Applications should be sent to: sprints at python.org with the subject "Sprint
   Funding Application - <location>"

We encourage anyone - even those who have never held, or been to a sprint - to
consider holding one. We will help you as much as we can with welcome packets,
advertising, and hooking you up with required resources - anything to make it
possible.

As part of being approved, the you will need to agree to deliver a report
(hopefully, with pictures!) of the sprint to the Sprint Committee, so we can
post it on the sprint blog and site:

http://www.pythonsprints.com

If you have any questions or need more information, contact us by email at
sprints at python.org.

More information is up on our blog:
    http://pythonsprints.com/2010/07/8/call-applications-now-open/

