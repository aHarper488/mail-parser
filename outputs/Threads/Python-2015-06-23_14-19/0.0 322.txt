
============================================================================
Subject: [Python-Dev] Remove HTTP 0.9 support
Post Count: 23
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

I would like to remove HTTP 0.9 support from http.client and
http.server. I've opened an issue at http://bugs.python.org/issue10711
for that. Would anyone think it's a bad idea?

(HTTP 1.0 was devised in 1996)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Wed, Dec 15, 2010 at 1:39 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

+1


? -Fred

--
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"A storm broke loose in my mind."? --Albert Einstein



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 12/15/2010 10:39 AM, Antoine Pitrou wrote:

Please address the following comment from the server.py source:

     # The default request version.  This only affects responses up until
     # the point where the request line is parsed, so it mainly decides what
     # the client gets back when sending a malformed request line.
     # Most web servers default to HTTP 0.9, i.e. don't send a status line.
     default_request_version = "HTTP/0.9"

I realize this is a somewhat obscure point, and in general, if your 
interest in http.client and http.server implies that some of the many 
outstanding bug reports for that code will get resolved, I have no 
concern for dropping support for HTTP 0.9 protocol, other than the above.

Glenn
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101215/ab860286/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 15 Dec 2010 12:58:51 -0800
Glenn Linderman <v+python at g.nevcal.com> wrote:

What do you mean by "address"? The patch changes this to 1.0.
And, as the comment says, this only affects what happens when the
client sends a syntactically invalid request line, so whether the server
does a 0.9-style or 1.0-style response is unimportant.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 12/15/2010 1:25 PM, Antoine Pitrou wrote:

Just what you did... justify the unimportance of not changing it :)  
Since now it is different than "most web servers".
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101215/8a9eb178/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 15 Dec 2010 14:20:54 -0800
Glenn Linderman <v+python at g.nevcal.com> wrote:


Well, I think the "most web servers" comment itself is outdated.
Try e.g. www.mozilla.org or www.google.com or www.msn.com.
(but www.python.org or www.apache.org still have the legacy behaviour)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Wed, Dec 15, 2010 at 02:20:54PM -0800, Glenn Linderman wrote:

+1 to removing HTTP 0.9 related code in http.client and http.server.

Until today, I hadn't cared to read any details of HTTP 0.9 except
that I knew that some code was present in http library to support it.
Reading a bit about the HTTP 0.9 'Internet Draft' written 1991 is
enough to convince that, if we have to fall back on any old behavior
falling back to HTTP 1.0 is perfectly fine.

Regarding this comment

# Most web servers default to HTTP 0.9, i.e. don't send a status line.

Even HTTP 0.9 says that response SHOULD start with status line, but
gives a suggestion that clients can "tolerate" bad server server
behaviors when they don't send the status line and in that the case
response is the body.

http://www.w3.org/Protocols/HTTP/Response.html

So, It cannot be associated with the behavior "most webservers", back
then and even more so now.

-- 
Senthil




----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Wed, Dec 15, 2010 at 11:29:27PM +0100, Antoine Pitrou wrote:

What legacy behavior did you observe in these?

-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: =?iso-8859-1?q?Andr=E9_Malo?
Attributes: []Content: 
* Antoine Pitrou wrote:


HTTP/0.9 support is still recommended (RFC 2616 is from 1999, but still 
current).

I'm wondering, why you would consider touching that at all. Is it broken? 
Does it stand in the way of anything? If not, why throw away a feature?

nd
-- 
Already I've seen people (really!) write web URLs in the form:
http:\\some.site.somewhere
[...] How soon until greengrocers start writing "apples $1\pound"
or something?                           -- Joona I Palaste in clc



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le jeudi 16 d?cembre 2010 ? 16:14 +0800, Senthil Kumaran a ?crit :

-> Request:
xyzzy

-> Response:
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>405 Method Not Allowed</title>
</head><body>
<h1>Method Not Allowed</h1>
<p>The requested method xyzzy is not allowed for the URL /.</p>
<hr>
<address>Apache/2.3.8 (Unix) mod_ssl/2.3.8 OpenSSL/1.0.0a Server at
www.apache.org Port 80</address>
</body></html>

(notice how the response has no headers)





----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 16 Dec 2010 07:42:08 +0100
Andr? Malo <nd at perlig.de> wrote:

Well, it complicates maintenance and makes fixing issues such as
http://bugs.python.org/issue6791 less likely.

Note that the patch still accepts servers and clients which advertise
themselves as 0.9 (using "HTTP/0.9" as a version string). It just
removes support for the style of "simple response" without headers that
0.9 allowed.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: =?iso-8859-1?q?Andr=E9_Malo?
Attributes: []Content: 
On Thursday 16 December 2010 15:23:05 Antoine Pitrou wrote:

I'd vote for removing it from the client code and keeping it in the server.


HTTP/0.9 doesn't *have* a version string.

GET /foo

is a HTTP/0.9 request.

GET /foo HTTP/0.9

isn't actually (it's a paradoxon, alright ;). It simply isn't a valid HTTP 
request, which would demand a 505 response.

nd



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Thu, Dec 16, 2010 at 10:52 AM, Andr? Malo <nd at perlig.de> wrote:

If it must be maintained anywhere, it should be in the client,
according to the basic principle of "accept what you can, generate
carefully."

Python.org's HTTP/0.9 responses appear to be in response to HTTP/0.9
requests only.  A request claiming to be HTTP 1.0, but without a Host:
header, gets a redirect to the same page.

I'm still in favor of removing HTTP 0.9 support entirely.


? -Fred

--
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"A storm broke loose in my mind."? --Albert Einstein



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Thu, Dec 16, 2010 at 02:20:37PM +0100, Antoine Pitrou wrote:

Well, Error response without headers was observed in www.mozilla.org
and www.google.com for Invalid requests.

But, I observed something surprising at www.apache.org
If you do GET / HTTP/1.0
You do get the valid Response with headers.

But if you do GET /
You get a valid response Without headers.

I was afraid if this behavior was to support HTTP 0.9 style where the
the reponse is sent without the headers.

Actually, it is turning out to be true:

http://ftp.ics.uci.edu/pub/ietf/http/rfc1945.html#Response

According to HTTP 1.0, When a request is Simple-Request, it means a
VERB URL (without a version) and it generally corresponds to HTTP 0.9
And when a server receives such a Simple-Request, it sends a
Simple-Response where it does not send the headers back.

I think, the same is exhibited by other Servers as well
www.google.com, www.mozilla.org where for Invalid Request without
version, you are sending a Simple-Request (HTTP 0.9) style and getting
the corresponding response.

Given these, any assumption that servers no longer support HTTP/0.9
becomes false. So nuking it will require some thought.

-- 
Senthil



-- 



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: James Y Knigh
Attributes: []Content: 

On Dec 16, 2010, at 3:14 AM, Senthil Kumaran wrote:


Actually no. That document is describing almost-HTTP 1.0. Here is the actual document you were looking for:
http://www.w3.org/Protocols/HTTP/AsImplemented.html

HTTP 0.9 had no headers, no status line, nothing but "GET $url <crlf>" and a stream of data in response.

James



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Thu, Dec 16, 2010 at 11:21:37AM -0500, James Y Knight wrote:

Yeah. I know it was almost-HTTP 1.0, but the same docs say that if
protocol version was not specified, it is assumed to be 0.9. So, I
thought it was a good reference point to understand the behavior.


Actually, you are right. I seems be be actual defined behavior of HTTP
0.9. As explained in that above doc and also in RFC 1945 Request
section.

-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Thu, Dec 16, 2010 at 04:52:43PM +0100, Andr? Malo wrote:

Yes, this is an important point. Many webservers seem to exhibit the
HTTP 0.9 response behaviors when you don't specify the version.


-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 17 Dec 2010 00:52:14 +0800
Senthil Kumaran <orsenthil at gmail.com> wrote:

Yes, but only error or redirect responses:

$ nc www.google.fr 80
GET /
HTTP/1.0 302 Found
Location: http://www.google.fr/
[etc.]

$ nc www.mozilla.org 80
GET /
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>403 Forbidden</title>
</head><body>
<h1>Forbidden</h1>
<p>You don't have permission to access /error/noindex.html
on this server.</p>
</body></html>


That's quite understandable, since most HTTP servers will expect a
"host" header to know which site is actually desired.
So a HTTP 0.9 client sending Simple-Requests has very little chance of
being useful these days.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
All this talk of modern servers that also still support HTTP/0.9 is
irrelevant. Unless anybody knows of a server that *only* supports HTTP
0.9 (and that's relevant to users of httplib) let's please kill
support in the client.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: exarkun at twistedmatrix.co
Attributes: []Content: 
On 05:02 pm, solipsis at pitrou.net wrote:

Note that by using `nc` to test this, you're already generating an 
illegal request (unless you're doing something very special with your 
keyboard ;).  When you hit return, nc will send \n.  However, lines are 
delimited by \r\n in HTTP.

I doubt this makes a difference to the point being discussed, but it 
_could_.  I suggest performing your tests with telnet, instead.

Jean-Paul



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: =?iso-8859-1?q?Andr=E9_Malo?
Attributes: []Content: 
* Fred Drake wrote:


*scratching head* exactly why I would keep support in the server.

nd
-- 
package Hacker::Perl::Another::Just;print
qq~@{[reverse split/::/ =>__PACKAGE__]}~;

#  Andr? Malo  #  http://www.perlig.de  #



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Thu, Dec 16, 2010 at 1:30 PM,  <exarkun at twistedmatrix.com> wrote:

I received similar results using telnet earlier today.


? -Fred

--
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"A storm broke loose in my mind."? --Albert Einstein



----------------------------------------
Subject:
[Python-Dev] Remove HTTP 0.9 support
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Senthil Kumaran wrote:


But as long as httplib only sends requests with a version
number >= 1.0, it should be able to expect headers in the
response, shouldn't it?

-- 
Greg

