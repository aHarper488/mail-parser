
============================================================================
Subject: [Python-Dev] It is always loo late (Was: Relative imports in
	Py3k)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] It is always loo late (Was: Relative imports in
	Py3k)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 13 Oct 2010 16:55:03 +0300
anatoly techtonik <techtonik at gmail.com> wrote:

No, there isn't. If you want to know what Python 3 is about, you can :
- read the docs
- read the changelogs
- download it and try it (especially alpha/beta versions)
- read python-dev and python-checkins
- read the source code
- follow the bug tracker

If you aren't willing to do anything of that, then I don't think your
complaints belong here.  comp.lang.python (also named python-list, or
gmane.comp.python.general) is a more appropriate venue for your
messages.


And that's a good thing, because Python is not your toy project and
development of Python is not about satisfying your personal tastes.

Antoine.



