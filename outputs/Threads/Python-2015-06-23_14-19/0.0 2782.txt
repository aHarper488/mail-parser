
============================================================================
Subject: [Python-Dev] why is _PyBytes_Join not public but PyUnicode_Join
	is?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] why is _PyBytes_Join not public but PyUnicode_Join
	is?
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Fri, Aug 31, 2012 at 3:43 AM, Gregory P. Smith <greg at krypto.org> wrote:

I wondered about the same thing a month ago -
http://mail.python.org/pipermail/python-dev/2012-July/121031.html

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120831/bf40a120/attachment.html>



----------------------------------------
Subject:
[Python-Dev] why is _PyBytes_Join not public but PyUnicode_Join
	is?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Aug 31, 2012 at 8:24 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

The other aspect we're conscious of these days is that folks like the
IronClad and cpyext developers *are* making a concerted effort to
emulate the full C API of CPython-the-runtime, not just implementing
Python-the-language.

External tools like Dave Malcolm's static analyser for gcc also need
to be taught the refcounting semantics of any new API additions.

So, unless there's a compelling reason for direct public access from
C, the preferred option is to only expose the corresponding Python API
via the general purpose APIs for calling back into Python from C
extensions. This minimises the induced workload on other groups, as
well as making future maintenance easier for CPython itself.

New additions are still possible - they're just not the default any more.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia

