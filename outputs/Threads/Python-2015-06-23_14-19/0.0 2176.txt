
============================================================================
Subject: [Python-Dev] python -m test -jN
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] python -m test -jN
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hey Barry,


Please take a look at http://docs.python.org/devguide/runtests.html
and learn about the -j option ;)

(on a dual-core 4-thread machine:

./python -m test -j4
[...]
600.34user 26.36system 4:13.44elapsed 247%CPU (0avgtext+0avgdata
871456maxresident)k 110544inputs+152624outputs
(72major+6806058minor)pagefaults 0swaps

)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] python -m test -jN
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Antoine> Please take a look at
    Antoine> http://docs.python.org/devguide/runtests.html and learn about
    Antoine> the -j option ;)

I just gave it a try.  Several tests failed:

    test_builtin test_distutils test_imp test_peepholer test_pydoc
    test_unicode test_unittest

OTOH, when I run a simple "make test" only distutils fails.

This is on my Macbook Pro running Leopard.

Skip



----------------------------------------
Subject:
[Python-Dev] python -m test -jN
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 23 Mar 2011 11:10:29 -0500
skip at pobox.com wrote:

I would suggest adding the -W option to re-run the failing tests in
verbose mode, and then open individual issues about them.

Regards

Antoine.

