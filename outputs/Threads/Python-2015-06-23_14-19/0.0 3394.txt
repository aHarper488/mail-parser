
============================================================================
Subject: [Python-Dev] 2012 Language Summit Report
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] 2012 Language Summit Report
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
As with last year, I've put together a summary of the Python Language
Summit which took place last week at PyCon 2012. This was compiled
from my notes as well as those of Eric Snow and Senthil Kumaran, and I
think we got decent coverage of what was said throughout the day.

http://blog.python.org/2012/03/2012-language-summit-report.html

If you have questions or comments about discussions which occurred
there, please create a new thread for your topic.

Feel free to contact me directly if I've left anything out or
misprinted anything.



----------------------------------------
Subject:
[Python-Dev] 2012 Language Summit Report
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/14/2012 10:12 AM, Brian Curtin wrote:

Nicely done. Thank you.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] 2012 Language Summit Report
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 14.03.2012 15:12, Brian Curtin wrote:

Thanks for the comprehensive report (I'm still reading).  May I request
for the future that you also paste a copy in the email to the group, for
purposes of archiving and ease of discussing?  (Just like we also post
PEPs to python-dev for discussion even when they are already online.)

cheers,
Georg




----------------------------------------
Subject:
[Python-Dev] 2012 Language Summit Report
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 3/14/2012 8:57 AM, Terry Reedy wrote:
Thanks. Almost feels like I was there!
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120314/ffbe1051/attachment.html>



----------------------------------------
Subject:
[Python-Dev] 2012 Language Summit Report
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Wed, Mar 14, 2012 at 13:52, Georg Brandl <g.brandl at gmx.net> wrote:

Certainly -- good idea. I have a few updates and corrections to make
this evening, then I'll get a copy of it posted.

