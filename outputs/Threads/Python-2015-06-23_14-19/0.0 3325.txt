
============================================================================
Subject: [Python-Dev] odd "tuple does not support assignment"
	confusion...
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] odd "tuple does not support assignment"
	confusion...
----------------------------------------
Author: Simon Cros
Attributes: []Content: 
l[0] += [1]

is the same as

l[0] = l[0] + [1]

Does that make the reason for the error clearer? The problem is the
attempt to assign a value to l[0].

It is not the same as

e = l[0]
e += [1]

which is the equivalent to

e = l[0]
e = e + [1]

This never assigns a value to l[0].

Schiavo
Simon



----------------------------------------
Subject:
[Python-Dev] odd "tuple does not support assignment"
	confusion...
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sat, 03 Mar 2012 03:06:33 +0400, "Alex A. Naanou" <alex.nanou at gmail.com> wrote:

What is even more fun is that the append actually worked (try printing
l).

This is not a bug, it is a quirk of how extended assignment works.
I think there's an issue report in the tracker somewhere that
discusses it.

--David



----------------------------------------
Subject:
[Python-Dev] odd "tuple does not support assignment"
	confusion...
----------------------------------------
Author: Simon Cros
Attributes: []Content: 
On Sat, Mar 3, 2012 at 1:38 AM, R. David Murray <rdmurray at bitdance.com> wrote:

Now that is just weird. :)



----------------------------------------
Subject:
[Python-Dev] odd "tuple does not support assignment"
	confusion...
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/2/2012 6:06 PM, Alex A. Naanou wrote:


The place for 'fun little things' is python-list, mirrored as 
gmane.comp.python.general.


This has been discussed several times on python-list.
Searching group gmane.comp.python.general for 'augmented assignment 
tuple' at search.gmane.com returns about 50 matches.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] odd "tuple does not support assignment"
	confusion...
----------------------------------------
Author: Alex A. Naano
Attributes: []Content: 
I knew this was a feature!!!

....features such as these should be fixed! %)

On Sat, Mar 3, 2012 at 03:38, R. David Murray <rdmurray at bitdance.com> wrote:



-- 
Alex.

