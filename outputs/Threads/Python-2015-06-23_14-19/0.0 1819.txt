
============================================================================
Subject: [Python-Dev] status of absolute_import w/ python 2.7
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] status of absolute_import w/ python 2.7
----------------------------------------
Author: Sylvain =?utf-8?B?VGjDqW5hdWx0?
Attributes: []Content: 
Hi there,

the documentation state that absolute_import feature is the default
behaviour with python 2.7, though it seems that it behave differently
with the __future__ import :

$ cat package/__init__.py

import subpackage

$ python2.7
Python 2.7.1+ (default, Apr 20 2011, 22:33:39) 
[GCC 4.5.2] on linux2
Type "help", "copyright", "credits" or "license" for more information.

$ cat package/__init__.py

from __future__ import absolute_import
import subpackage

$ python2.7
Python 2.7.1+ (default, Apr 20 2011, 22:33:39) 
[GCC 4.5.2] on linux2
Type "help", "copyright", "credits" or "license" for more information.
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "package/__init__.py", line 23, in <module>
    import subpackage
ImportError: No module named subpackage


Maybe the doc should be fixed ?
-- 
Sylvain Th?nault                               LOGILAB, Paris (France)
Formations Python, Debian, M?th. Agiles: http://www.logilab.fr/formations
D?veloppement logiciel sur mesure:       http://www.logilab.fr/services
CubicWeb, the semantic web framework:    http://www.cubicweb.org




----------------------------------------
Subject:
[Python-Dev] status of absolute_import w/ python 2.7
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Fri, Jul 8, 2011 at 06:51, Sylvain Th?nault
<sylvain.thenault at logilab.fr>wrote:


So are you claiming that the import of 'package' w/o the __future__
statement actually succeeds even though there is no package.subpackage
module? Obviously that would be a flat-out bug, but I just double-checked my
sanity and that does nto work with a CPython 2.7 checkout.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110712/164e3dd0/attachment.html>



----------------------------------------
Subject:
[Python-Dev] status of absolute_import w/ python 2.7
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Jul 13, 2011 at 1:41 PM, Brett Cannon <brett at python.org> wrote:

No, the problem is that __future__.absolute_import claims to be the
default behaviour in 2.7, but this does not appear to actually be the
case - it still tries the implicit relative import. E.g, given the
following setup:

cwd
  /package
    /__init__.py
    /submodule.py
    /submodule2.py

an "import submodule" in __init__.py or submodule2.py will succeed.

Now, the what's new for 2.7 doesn't actually *say* we made that change
and I can't find any evidence for it in NEWS either, so I think the
bug is actually in the __future__ module (and docs:
http://docs.python.org/library/__future__).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] status of absolute_import w/ python 2.7
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jul 13, 2011, at 02:40 PM, Nick Coghlan wrote:


I think that's right.  The change was not made for 2.7.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110713/8c1b4cfa/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] status of absolute_import w/ python 2.7
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 13/07/2011 06:40, Nick Coghlan a ?crit :

I seemed to recall the change was done in 2.6, but I found only that:


http://docs.python.org/dev/whatsnew/2.6#porting-to-python-2-6

Regards

