
============================================================================
Subject: [Python-Dev] logging package vs unicode
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] logging package vs unicode
----------------------------------------
Author: INADA Naok
Attributes: []Content: 
FWIW, I think the best way to avoid UnicodeError is: Don't use chars
out of ASCII in Python2.
I prefer '%r' to '%s' because it escapes chars not printable or out of
ASCII with backslash.

On Mon, Jul 5, 2010 at 9:34 PM, Chris Withers <chris at simplistix.co.uk> wrote:



-- 
INADA Naoki? <songofacandy at gmail.com>



----------------------------------------
Subject:
[Python-Dev] logging package vs unicode
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
Hi All,

The documentation for the logging package doesn't include any mention of 
unicode.

What's the recommended usage in terms of passing unicode vs encoded strings?

How does this differ from Python 2 to Python 3?

cheers,

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk

