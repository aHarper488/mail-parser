
============================================================================
Subject: [Python-Dev] [RELEASED] Python 3.3.0 alpha 3
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On behalf of the Python development team, I'm happy to announce the
third alpha release of Python 3.3.0.

This is a preview release, and its use is not recommended in
production settings.

Python 3.3 includes a range of improvements of the 3.x series, as well
as easier porting between 2.x and 3.x.  Major new features and changes
in the 3.3 release series are:

* PEP 380, Syntax for Delegating to a Subgenerator ("yield from")
* PEP 393, Flexible String Representation (doing away with the
  distinction between "wide" and "narrow" Unicode builds)
* PEP 409, Suppressing Exception Context
* PEP 3151, Reworking the OS and IO exception hierarchy
* A C implementation of the "decimal" module, with up to 80x speedup
  for decimal-heavy applications
* The import system (__import__) is based on importlib by default
* The new "packaging" module, building upon the "distribute" and
  "distutils2" projects and deprecating "distutils"
* The new "lzma" module with LZMA/XZ support
* PEP 3155, Qualified name for classes and functions
* PEP 414, explicit Unicode literals to help with porting
* PEP 418, extended platform-independent clocks in the "time" module
* The new "faulthandler" module that helps diagnosing crashes
* A "collections.ChainMap" class for linking mappings to a single unit
* Wrappers for many more POSIX functions in the "os" and "signal"
  modules, as well as other useful functions such as "sendfile()"
* Hash randomization, introduced in earlier bugfix releases, is now
  switched on by default

For a more extensive list of changes in 3.3.0, see

    http://docs.python.org/3.3/whatsnew/3.3.html (*)

To download Python 3.3.0 visit:

    http://www.python.org/download/releases/3.3.0/

Please consider trying Python 3.3.0 with your code and reporting any bugs
you may notice to:

    http://bugs.python.org/


Enjoy!

(*) Please note that this document is usually finalized late in the release
    cycle and therefore may have stubs and missing entries at this point.

--
Georg Brandl, Release Manager
georg at python.org
(on behalf of the entire python-dev team and 3.3's contributors)



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Georg Brandl wrote:

Don't forget PEP 412 ;)

Rather than a long list of PEPs would it be better to split it into two 
parts?
1. language & library changes.
The details are important here, so that the PEPs should probably be 
fairly prominent.

2. Performance enhancements
People want to know how much faster 3.3 is or how less memory it uses.
Who cares which PEP does what (apart from the authors)?

Or maybe three parts?
New features.
Behavioural changes (i.e. bug fixes)
Performance enhancements

Cheers,
Mark.




----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, May 2, 2012 at 7:55 PM, Mark Shannon <mark at hotpy.org> wrote:

The release PEPs are mainly there for *our* benefit, not end users.

For end users, it's the What's New document that matters. For
performance numbers, the goal is to eventually have speed.python.org
providing regular results, but there's a fair bit of work still
involved in bringing that online with meaningful 3.x figures.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Nick Coghlan wrote:

The What's New document also starts with a long list of PEPs.
This seems to be the standard format as What's New for 3.2 follows the 
same layout.

Perhaps adding an overview or highlights at the start would be a good
idea.


Like some meaningful benchmarks for 3.x; there are very few :(

Cheers,
Mark.




----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

You seem to assume that Python users are not able to grasp long itemized 
lists including numbers. I think readers are very capable
of filtering this kind of information.

As for presenting highlights: the PEPs *are* the highlights of a new 
release. The numerous bug fixes and minor enhancements don't get listed
at all.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Martin v. L?wis wrote:

Just because readers are capable of filtering a long list of PEPs in an 
arbitrary order does not mean that they should have to.
Many readers will just skim the list, but would probably read a summary 
in full.


But PEPs can have very different purposes.
It would be useful to summarize the language changes (with links to the 
relevant PEPs) separately to library extensions and optimizations.

If the reader is interested in new features, then information about
optimisations are just clutter. And vice-versa.

Cheers,
Mark.



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Any such summary prose will be written by the What's New author
(Raymond Hettinger for the 3.x series). Such text definitely *won't*
be written until after feature freeze (which occurs with the first
beta, currently planned for late June).

Until that time, the draft What's New is primarily rough notes written
by everyone else for Raymond's benefit (and, of course, for the
benefit of anyone checking out the alpha and beta releases).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.3.0 alpha 3
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 05/07/2012 11:00 AM, Mark Shannon wrote:

Sorry, I think that's tough luck then.  The list isn't nearly long enough
to warrant splitting up.  The announcement should stay compact.  And as Nick
said, the "What's New" will be there for those who want a longer overview
by topics.

Georg


