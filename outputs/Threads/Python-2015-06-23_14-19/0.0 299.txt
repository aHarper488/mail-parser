
============================================================================
Subject: [Python-Dev] futures API
Post Count: 22
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Thomas Nag
Attributes: []Content: 
Hello,

I am looking forward to replacing a piece of code (http://code.google.com/p/waf/source/browse/trunk/waflib/Runner.py#86) by the futures module which was announced in python 3.2 beta. I am a bit stuck with it, so I have a few questions about the futures:

1. Is the futures API frozen?
2. How hard would it be to return the tasks processed in an output queue to process/consume the results while they are returned? The code does not seem to be very open for monkey patching.
3. How hard would it be to add new tasks dynamically (after a task is executed) and have the futures object never complete?
4. Is there a performance evaluation of the futures code (execution overhead) ?

Thanks,
Thomas



      



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Thu, Dec 9, 2010 at 04:26, Thomas Nagy <tnagyemail-mail at yahoo.fr> wrote:

It will be once Python 3.2 final is released.

-Brett




----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 09/12/2010 16:36, Brett Cannon wrote:

Now that 3.2 beta 1 is out the api is effectively frozen though, the 
only changes that should be made are bugfixes unless you can convince 
the release manager that an api change is really *essential*.

Michael



-- 

http://www.voidspace.org.uk/

READ CAREFULLY. By accepting and reading this email you agree,
on behalf of your employer, to release me from all obligations
and waivers arising from any and all NON-NEGOTIATED agreements,
licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap,
confidentiality, non-disclosure, non-compete and acceptable use
policies (?BOGUS AGREEMENTS?) that I have entered into with your
employer, its partners, licensors, agents and assigns, in
perpetuity, without prejudice to my ongoing rights and privileges.
You further represent that you have the authority to release me
from any BOGUS AGREEMENTS on behalf of your employer.




----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 

On Dec 9, 2010, at 4:26 AM, Thomas Nagy wrote:


Yes.


You can associate a callback with a submitted future. That callback  
could add the future to your queue.


I'm not sure that I understand your question. You can submit new work  
to an Executor at until time until it is shutdown and a work item can  
take as long to complete as you want. If you are contemplating tasks  
that don't complete then maybe you could be better just scheduling a  
thread.


No. Scott Dial did make some performance improvements so he might have  
a handle on its overhead.

Cheers,
Brian





----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Dec 9, 2010, at 9:02 AM, Brian Quinlan wrote:


Yes, unless the current API is defective in some way.  A beta1 release is a chance for everyone to exercise the new API and discover whether it is problematic in any real world applications.


The would be a good example for the docs.


FWIW, the source code is short and readable.  From my quick read, it looks to be a relatively thin wrapper/adapter around existing tools.  Most of the work still gets done by the threads or processes themselves.  Think of this as a cleaner, more centralized API around the current toolset -- there is no deep new technology under the hood.

Raymond



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 

On Dec 9, 2010, at 2:39 PM, Raymond Hettinger wrote:


I don't know what Thomas' use case is but I expect that taking the  
results of a future and asynchronously sticking it in another queue is  
not typical.

Cheers,
Brian





----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Thomas Nag
Attributes: []Content: 
--- El jue, 9/12/10, Brian Quinlan escribi?:

Ok, it works. I was thinking the object was cleaned up immediately after it was used.


Ok.

I have a process running for a long time, and which may use futures of different max_workers count. I think it is not too far-fetched to create a new futures object each time. Yet, the execution becomes slower after each call, for example with http://freehackers.org/~tnagy/futures_test.py:

"""
import concurrent.futures
from queue import Queue
import datetime

class counter(object):
? ? def __init__(self, fut):
? ? ? ? self.fut = fut

? ? def run(self):
? ? ? ? def look_busy(num, obj):
? ? ? ? ? ? tot = 0
? ? ? ? ? ? for x in range(num):
? ? ? ? ? ? ? ? tot += x
? ? ? ? ? ? obj.out_q.put(tot)

? ? ? ? start = datetime.datetime.utcnow()
? ? ? ? self.count = 0
? ? ? ? self.out_q = Queue(0)
? ? ? ? for x in range(1000):
? ? ? ? ? ? self.count += 1
? ? ? ? ? ? self.fut.submit(look_busy, self.count, self)

? ? ? ? while self.count:
? ? ? ? ? ? self.count -= 1
? ? ? ? ? ? self.out_q.get()

? ? ? ? delta = datetime.datetime.utcnow() - start
? ? ? ? print(delta.total_seconds())

fut = concurrent.futures.ThreadPoolExecutor(max_workers=20)
for x in range(100):
? ? # comment the following line
? ? fut = concurrent.futures.ThreadPoolExecutor(max_workers=20)
? ? c = counter(fut)
? ? c.run()
"""

The runtime grows after each step:
0.216451
0.225186
0.223725
0.222274
0.230964
0.240531
0.24137
0.252393
0.249948
0.257153
...

Is there a mistake in this piece of code?

Thanks,
Thomas



      



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Dec 10, 2010 at 11:36 PM, Thomas Nagy <tnagyemail-mail at yahoo.fr> wrote:

This isn't an "overhead" question, it's a "how prompt is the resource
release" question.

Given that you've created circular references between the futures and
your counter class, the answer is probably "not prompt at all".

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 
Oops. I accidentally replied off-list:


On Dec 10, 2010, at 5:36 AM, Thomas Nagy wrote:


There is no mistake that I can see but I suspect that the circular  
references that you are building are causing the ThreadPoolExecutor to  
take a long time to be collected. Try adding:

	c = counter(fut)
	c.run()
+	fut.shutdown()

Even if that fixes your problem, I still don't fully understand these  
numbers because I would expect the runtime to fall after a while as  
ThreadPoolExecutors are collected.

Cheers,
Brian


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101210/1b45d817/attachment.html>



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Thomas Nag
Attributes: []Content: 
--- El vie, 10/12/10, Brian Quinlan escribi?:

The shutdown call is indeed a good fix :-) Here is the time response of the calls to counter() when shutdown is not called:
http://www.freehackers.org/~tnagy/runtime_futures.png

After trying to stop the program by using CTRL+C, the following error may appear, after which the process cannot be interrupted:

"""
19:18:12 /tmp/build> python3.2 futures_test.py
0.389657
0.417173
0.416513
0.421424
0.449666
0.482273
^CTraceback (most recent call last):
? File "futures_test.py", line 36, in <module>
? ? c.run()
? File "futures_test.py", line 22, in run
? ? self.fut.submit(look_busy, self.count, self)
? File "/usr/local/lib/python3.2/concurrent/futures/thread.py", line 114, in submit
? ? self._work_queue.put(w)
? File "/usr/local/lib/python3.2/queue.py", line 135, in put
? ? self.not_full.acquire()
KeyboardInterrupt
"""

It is not expected, is it?

Thomas



      



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 

On Dec 10, 2010, at 10:51 AM, Thomas Nagy wrote:


FWIW, I think that you are confusion the term "future" with  
"executor". A future represents a single work item. An executor  
creates futures and schedules their underlying work.

Hmmm....that is very suspicious - it looks like the  
ThreadPoolExecutors are not being collected. If you are feeling bored  
you could figure out why not :-)


It isn't surprising. Python lock acquisitions are not interruptible  
and anytime you interrupt a program that manipulates locks you may  
kill the code that was going to cause the lock to be released.

Cheers,
Brian





----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Thomas Nag
Attributes: []Content: 
--- El vie, 10/12/10, Thomas Nagy escribi?:

The problem also occurs when using a callback:
http://www.freehackers.org/~tnagy/futures_test2.py

If it is necessary to catch KeyboardInterrupt exceptions to cancel the futures execution, then how about adding this detail to the docs?

Thomas



      



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 

On Dec 10, 2010, at 11:39 AM, Thomas Nagy wrote:


AFAIK, catching KeyboardInterrupt exceptions is not sufficient.

Cheers,
Brian





----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Dec 11, 2010 at 6:07 AM, Brian Quinlan <brian at sweetapp.com> wrote:

finally blocks and with statements can get you a fairly long way,
though. futures does everything right on this front, as far as I can
see.

In this case, the problem is in the design of the test program. It
*must* get exactly as many items in the queue as were submitted to the
executor. If one is ever missing (e.g. due to a poorly timed
KeyboardInterrupt in the callback, as clearly happened in the
presented trace), it will hang in the final call to self.out_q.get().
There's a reason Queue.get offers both nonblocking and
block-with-timeout functionality. (Alternately, the management of
out_q and count could be made smarter, such that an exception in
adding a result to out_q triggered an appropriate adjustment in the
count of expected values)

I also agree with Brian that the variable naming for the sample code
and comments like "may use futures of different max_workers count. I
think it is not too far-fetched to create a new futures object each
time" suggests a fundamental confusion of the terms "future" and
"executor". Executors are not futures - the return value from an
executor's submit method is a future.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Thomas Nag
Attributes: []Content: 
--- El vie, 10/12/10, Brian Quinlan escribi?:

Ah yes, sorry. I have also realized that the executor is not the killer feature I was expecting, it can only replace a little part of the code I have: controlling the exceptions and the workflow is the most complicated part.

I have also observed a minor performance degradation with the executor replacement (3 seconds for 5000 work items). The amount of work items processed by unit of time does not seem to be a straight line: http://www.freehackers.org/~tnagy/runtime_futures_2.png . Out of curiosity, what is the "_thread_references" for?

The source file for the example is in:
http://www.freehackers.org/~tnagy/futures_test3.py

The diagram was created by:
http://www.freehackers.org/~tnagy/futures_test3.plot

Thomas



      



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Scott Dia
Attributes: []Content: 
On 12/11/2010 9:44 AM, Thomas Nagy wrote:

You're test code does 50,000,000 of list appends. I suspect your
benchmark is telling you more about the behavior of large lists than the
overhead of the futures module. You should retry that experiment with
the list pre-allocated. Beyond that, the curve in that line is not
exactly a large amount of variance from a straight line.

-- 
Scott Dial
scott at scottdial.com
scodial at cs.indiana.edu



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 

On Dec 11, 2010, at 6:44 AM, Thomas Nagy wrote:


That looks pretty linear to me.


There is a big comment above it in the code:

# Workers are created as daemon threads. This is done to allow the  
interpreter
# to exit when there are still idle threads in a ThreadPoolExecutor's  
thread
# pool (i.e. shutdown() was not called). However, allowing workers to  
die with
# the interpreter has two undesirable properties:
#   - The workers would still be running during interpretor shutdown,
#     meaning that they would fail in unpredictable ways.
#   - The workers could be killed while evaluating a work item, which  
could
#     be bad if the callable being evaluated has external side-effects  
e.g.
#     writing to a file.
#
# To work around this problem, an exit handler is installed which  
tells the
# workers to exit when their work queues are empty and then waits  
until the
# threads finish.

_thread_references = set()
_shutdown = False

def _python_exit():
     global _shutdown
     _shutdown = True
     for thread_reference in _thread_references:
         thread = thread_reference()
         if thread is not None:
             thread.join()

Is it still unclear why it is there? Maybe you could propose some  
additional documentation.

Cheers,
Brian





----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Brian Quinlan wrote:


Close to, but not quite. The graph seems to be slightly curved, with the 
amount of work done per second decreasing for large amounts of work. 
Assuming that this performance degradation is real, and not an artifact 
of the measurement technique, it seems to be quite small. I'd be happy 
to describe it as "linear" in the same way we describe dictionary 
lookups as constant-time, even though technically that's not strictly 
true. (They're linear in the number of items with a matching hash, and 
there are probably other complications as well.)

As drawn, the curve seems to fall away like a log graph, which might 
suggest to the casual viewer that this is a good thing. It may be better 
to reverse the axes, that is to have the independent variable, number of 
tasks, on the horizontal axis, and the dependent variable, cost (time 
taken), vertically. This will make it clear that the incremental cost of 
doing one extra task increases (slightly) as the number of tasks goes up.



-- 
Steven




----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Dec 12, 2010 at 6:53 AM, Brian Quinlan <brian at sweetapp.com> wrote:

Did you get my question the other day as to whether a
weakref.WeakKeySet might be a better choice? I believe you would be
able to get rid of the periodic sweep for dead references if you did
that, and I didn't spot any obvious downsides.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Thomas Nag
Attributes: []Content: 
--- El s?b, 11/12/10, Brian Quinlan escribi?:

Ok.
 

I was thinking that if exceptions have to be caught - and it is likely to be the case in general - then this scheme is redundant. Now I see that the threads are getting their work items from a queue, so it is clear now.

Thanks for all the information,

Thomas



      



----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 

On Dec 11, 2010, at 6:33 PM, Nick Coghlan wrote:


No I didn't, sorry! Could you resent it if it has more details then  
the paragraph above?

Cheers,
Brian





----------------------------------------
Subject:
[Python-Dev] futures API
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Dec 12, 2010 at 12:36 PM, Brian Quinlan <brian at sweetapp.com> wrote:

There wasn't much more detail, as there actually isn't a great deal to
the idea. This was the main comment in the previous email:

"It seems to me that using WeakSet would mean you could get rid of
_remove_dead_thread_references altogether (the implicit callbacks
would handle that part), and then a set() call in _python_exit would
give you concrete references to work with for cleanup purposes."

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

