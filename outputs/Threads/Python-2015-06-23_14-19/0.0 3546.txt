
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Describe the default
 hash correctly, and mark a couple of CPython
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Describe the default
 hash correctly, and mark a couple of CPython
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/20/2012 4:31 AM, nick.coghlan wrote:


I don't understand what you were trying to say with
x == y implies x is y
but I know you know that that is not true ;=0.




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Describe the default
 hash correctly, and mark a couple of CPython
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
It's true for the default comparison definition for user defined classes,
which is what that paragraph describes.

--
Sent from my phone, thus the relative brevity :)
On May 21, 2012 2:32 AM, "Terry Reedy" <tjreedy at udel.edu> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120521/37b7fa7e/attachment-0001.html>

