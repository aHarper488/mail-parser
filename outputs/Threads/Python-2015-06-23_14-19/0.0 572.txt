
============================================================================
Subject: [Python-Dev] Getting an optional parameter instead of creating
 a	socket	internally
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Getting an optional parameter instead of creating
 a	socket	internally
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 13/04/10 04:03, exarkun at twistedmatrix.com wrote:

Jean-Paul, I would like to have this for 3.2. How is the PEP going?.

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBTDnfL5lgi5GaxT1NAQJOIAP+IAARGsWGeReG21Mc70AxT9e82TqrPY65
053GpfnqDW/poCHdHKv5NeDPso02tDeJvZ53cB23ximQKM9qg1j9XzXP/5AJcjke
eVJaS9K8K6/Z1o97iDZb3Evkt7q2Dn7VG4QjJn6cy9lh841HDRFn/+HIuQLgoMyh
stvK53cj7n4=
=JrAg
-----END PGP SIGNATURE-----

