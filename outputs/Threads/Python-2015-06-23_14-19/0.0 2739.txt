
============================================================================
Subject: [Python-Dev] cpython: Add yet another test for
	subprocess.Popen.communicate
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Add yet another test for
	subprocess.Popen.communicate
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 15 Aug 2012 21:54:06 +0200 (CEST)
andrew.svetlov <python-checkins at python.org> wrote:

This test is wrong. You need to write your test data as binary data on
the binary output streams, as in the other tests. Using the text
output streams introduces a spurious line ending conversion, which makes
the test fail under Windows:
http://buildbot.python.org/all/builders/AMD64%20Windows7%20SP1%203.x/builds/486


You should use self.assertStderrEqual() instead.

Regards

Antoine.


-- 
Software development and contracting: http://pro.pitrou.net





----------------------------------------
Subject:
[Python-Dev] cpython: Add yet another test for
	subprocess.Popen.communicate
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
Fixed in 150fa296f5b9.
New version uses binary stream for output.
assertStderrEqual cannot be applied because it strips newlines which
are subject for check.

On Thu, Aug 16, 2012 at 1:25 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:



-- 
Thanks,
Andrew Svetlov

