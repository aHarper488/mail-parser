
============================================================================
Subject: [Python-Dev] Misc/NEWS in 2.7 and 3.2
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Misc/NEWS in 2.7 and 3.2
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi,

I noticed that the top-level section in Misc/NEWS (i.e. the section
where we add entries) for 3.3 is for 3.3.0a2 (the next release), but in
2.7 and 3.2 we?re still adding entries to the sections corresponding to
the last RCs.  Will the RMs move things when they merge back their
release clones, or should we start new sections and move the latest
entries there?

Cheers

