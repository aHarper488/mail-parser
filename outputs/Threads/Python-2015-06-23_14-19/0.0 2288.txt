
============================================================================
Subject: [Python-Dev] Hello!
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Hello!
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 
Hi,

My name is Charles-Fran?ois Natali, I've been using Python for a
couple years, and I've recently been granted commit priviledge.
I just wanted to say hi to everyone on this list, and let you know
that I'm really happy and proud of joining this great community.

Cheers,

cf



----------------------------------------
Subject:
[Python-Dev] Hello!
----------------------------------------
Author: Ross Lagerwal
Attributes: []Content: 
On Fri, 2011-05-20 at 19:01 +0200, Charles-Fran?ois Natali wrote:

Congratulations, welcome.

Ross




----------------------------------------
Subject:
[Python-Dev] Hello!
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 20 May 2011 19:01:26 +0200
Charles-Fran?ois Natali <cf.natali at gmail.com> wrote:


Welcome, and keep up the good work.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Hello!
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, May 21, 2011 at 9:59 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Indeed!

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

