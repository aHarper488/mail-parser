
============================================================================
Subject: [Python-Dev] Python 2.6.6 release candidate 2 now available.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 2.6.6 release candidate 2 now available.
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
Hello fellow Python enthusiasts,

The source tarballs and Windows installers for the second (and hopefully last)
Python 2.6.6 release candidate is now available:

    http://www.python.org/download/releases/2.6.6/

We've had a handful of important fixes since rc1, and of course a huge number
of bugs have been fixed since 2.6.5, with the full NEWS file available here:

    http://www.python.org/download/releases/2.6.6/NEWS.txt

We would love it if you can download, install, and test this version with your
favorite projects and on your favorite platforms.  We expect to release Python
2.6.6 final on August 24, 2010.

Please note that with the release of Python 2.7 final on July 3, 2010, and in
accordance with Python policy, Python 2.6.6 is the last scheduled bug fix
maintenance release of the 2.6 series.  Because of this, your testing of this
release candidate will help immensely.  We plan on continuing to support
source-only security fixes in Python 2.6 for the next five years.

My thanks go out to everyone who has contributed with code, testing and bug
tracker gardening for Python 2.6.6.  The excellent folks on #python-dev are
true Pythonic heros.

Enjoy,
-Barry
(on behalf of the Python development community)
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100817/a9ee5a11/attachment.pgp>

