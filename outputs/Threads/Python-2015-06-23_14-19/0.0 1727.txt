
============================================================================
Subject: [Python-Dev] devguide: Cover how to (un-)apply a patch.
Post Count: 24
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 17 Jan 2011 23:37:07 +0100
brett.cannon <python-checkins at python.org> wrote:

Or, simply and more reliably, use the corresponding VCS incantation
("svn revert -R ." or "hg revert -a").

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
Done

On Mon, Jan 17, 2011 at 15:14, Antoine Pitrou <solipsis at pitrou.net> wrote:



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Antoine> On Mon, 17 Jan 2011 23:37:07 +0100
    Antoine> brett.cannon <python-checkins at python.org> wrote:
    >> +
    >> +To undo a patch, do::
    >> +
    >> +    patch -R -p0 < patch.diff
    >> +

    Antoine> Or, simply and more reliably, use the corresponding VCS
    Antoine> incantation ("svn revert -R ." or "hg revert -a").

I prefer Brett's solution.  It's one command instead of one command per VCS.
It works with other version control systems and provides me the opportunity
to save a copy I can restore later.

Skip



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 18.01.2011 01:19, schrieb skip at pobox.com:

It assumes you already have the copy.

Georg




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Oleg Broytma
Attributes: []Content: 
On Mon, Jan 17, 2011 at 06:19:50PM -0600, skip at pobox.com wrote:

   "hg revert" saves files before reverting as *.orig.

Oleg.
-- 
     Oleg Broytman            http://phdru.name/            phd at phdru.name
           Programmers don't die, they just GOSUB without RETURN.



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    >> I prefer Brett's solution.  It's one command instead of one command
    >> per VCS.  It works with other version control systems and provides me
    >> the opportunity to save a copy I can restore later.

    Georg> It assumes you already have the copy.

Sure, but the way to get the input to the patch command is easy, and is
probably almost the same for any version control system:

    whatever-vcs diff > patch.diff

The odds that someone will remember the syntax for the diff command for the
VCS are much higher than the revert command.  My guess is "diff" is executed
more often than any other version control commands except "update" and
"commit", and far more often than "revert".  Personally, I'm not sure I've
ever used "revert" more than a handful of times in my entire professional
lifetime.

I realize the world is passing me by and that I'm rapidly turning into a
dinosaur w.r.t. distributed version control, but as you write/update the
developer's guide remember that proficiency in Python does not necessarily
equate to proficiency in version control systems, especially with the less
frequently used commands.  I personally would prefer that more general
commands and concepts be used where possible so that newcomers not be put
off unnecessarily by the complexity of version control.

Skip



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Jan 19, 2011 at 5:32 AM,  <skip at pobox.com> wrote:

Interesting. I almost *never* reverse patches - I always use the SVN
revert command.

Usually, this is because I will have edited the source tree since
applying the patch. Reversion has the advantage of not getting
confused by any additional changes. I also usually use "svn diff" to
save a copy before I revert in case I change my mind.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Nick> Usually, this is because I will have edited the source tree since
    Nick> applying the patch. Reversion has the advantage of not getting
    Nick> confused by any additional changes. I also usually use "svn diff"
    Nick> to save a copy before I revert in case I change my mind.

I routinely use CVS and Subversion at work, occasionally SCCS (yes, we still
have a little of that other dinosaur laying about - our sysadmins, what can
I say? they are luddites).  Most of my interaction with these tools is
mediated through the Emacs vc package, so my direct use of the command line
is reduced even from what you might think normal.  It's generally only when
I need to operate on a group of files that I revert to using the command
line.  That tends to be to check in a group of files or discard one or
changes before checking in, generally by taking a diff and unapplying it
with with patch, perhaps first saving it to a file.  If I want to revert a
change after checking it in, I can just pipe the confirmation email through
patch.

S




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
skip at pobox.com wrote:


What he said, only bolded and underlined.


-- 
Steven




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 20 Jan 2011 01:23:26 +1100
Steven D'Aprano <steve at pearwood.info> wrote:

I'm not sure what the issue is. Is there something, concretely, that
needs to be fixed?





----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Antoine Pitrou wrote:

You'll have to ask Skip if he thinks there's a concrete problem. I 
haven't seen one, but I've only been reading this thread with one eye 
and it may be I've missed the mother of all problems.

The (non-concrete) issue, as I understand it, is simple: be aware that 
not all Python developers are necessarily expert in DVCSes, and please 
keep it simple.


-- 
Steven




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 20 Jan 2011 01:54:37 +1100
Steven D'Aprano <steve at pearwood.info> wrote:

Well "svn revert" is one of the basic SVN commands (that I personally
use far more often than "patch -R", but YMMV). We're not talking about
some advanced use of Mercurial queues. The point is a bit subtler here
though: if you use "patch -R" after you have done some changes of your
own, the checkout will not be restored to its pristine state, which
may bite you later. "svn revert -R ." ensures everything is clean.

Arguably, even "patch" isn't familiar to Windows developers. It doesn't
come bundled and has to be installed separately, and I've seen some
people use the TortoiseSVN GUI for applying patches.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    >> What he said, only bolded and underlined.

    Antoine> I'm not sure what the issue is. Is there something, concretely,
    Antoine> that needs to be fixed?

Strictly speaking, nothing needs to be "fixed" because nothing is broken.
Rephrasing my earlier messages:

    1. Being a sophisticated Python programmer (and thus being a potential
       core developer) does not necessarily equate to being a sophisticated
       user of (especially distributed) version control systems.  I have
       been programming in Python for about 15 years and have made
       contributions to the core off-and-on for about 10 years.  I have
       never, not even once, been tempted to learn about or use svnmerge.
       Even considering the more mundane subcommands of the normal svn and
       hg commands (not to mention cvs, bzr, git, darcs, etc) there are
       plenty of different ways to structure the workflow, not all of which
       will make sense for each of those vcs's, nor will they all make sense
       to all potential users.

    2. There is more than one way to skin many of the cats involved in
       version control.  My preference to use "vcs diff | patch -p0 -R" or
       "patch -p0 -R < some-email" in preference to "vcs revert <some
       flags>" is just one example.  I'm sure I will be able to master "svn
       revert" and "hg revert" if necessary, but that knowledge won't
       transfer at all to CVS (no revert command) and won't transfer 100% to
       other vcs's because their revert commands will have semantic
       differences or use different command line flags to dictate the
       specifics of the action to perform.

    3. Not everyone will use the command line (strange as that may seem
       coming from a decades-long Unix user).  Many Windows users (and
       probably some Mac users) will have GUIs like TortoiseHg.  Smart/lazy/
       memory-challenged Emacs and vim users will have version control
       commands built into their editors precisely to paper over the arcane
       differences which exist between vcs's even for common operations.

Skip



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
Steven D'Aprano <steve at pearwood.info> wrote:


Indeed.  I now have to deal with an unholy mix of CVS, Subversion, git,
and Mercurial -- a twisty maze of little one-letter options, all so
similar, all too powerful.  At least with CVS and Subversion you could
concentrate your mistakes on a single file :-).

Bill



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 19 Jan 2011 10:36:04 -0600
skip at pobox.com wrote:

Ok, thank you but... are you suggesting something or not?




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Antoine> Ok, thank you but... are you suggesting something or not?

Yes.  Keep the vcs command recommendations simple.  At least mention idioms
which likely to apply across a wider range of version control systems.

S



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 19/01/2011 11:35, Nick Coghlan wrote:

Ditto, same here. For me (by no stretch of the imagination an "expert" 
VCS user) the revert commands (of svn, Hg and bzr) are basically 
straightforward (and cross-platform). To me it is tinkering with the 
patch command that is arcane...

All the best,

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 19/01/2011 19:10, skip at pobox.com wrote:

The revert works with svn, hg and bzr. Using patch is not going to work 
on Windoze unless cygwin has been installed.

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Jan 19, 2011 at 10:10,  <skip at pobox.com> wrote:

I was hoping this would flame out, but two days of discussion suggests
otherwise.

I am of the opinion of always listing how to use the CVS to its
fullest. It is the thing you will have to interact with the most when
doing work on Python, so trying to avoid it is not doing anyone any
favours.

That being said, I am not opposed to someone (other than me as I am
not going to bother) **adding** a not about `patch -R`, but it should
not replace the `svn revert` explanation.



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 19 Jan 2011 19:20:01 +0100
Michael Foord <fuzzyman at voidspace.org.uk> wrote:

You don't need cygwin, just something much smaller with "GNU" in its
name: http://gnuwin32.sourceforge.net/packages/patch.htm

(yes, the suggestion is already in the dev guide)




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/1/19 Michael Foord <fuzzyman at voidspace.org.uk>:

I thought you were supposed to use some variant of "update" on hg
instead revert, though.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 


I think what is discouraged is to "hg revert" to a different revision.
We are talking about reverting your working copy to its pristine state.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 1/19/2011 1:25 PM, Brett Cannon wrote:

As a neophyte vcs user, I like specific commands that can only do what I 
want, and not screw up with a wrong flag, so I agree with this.

The most important thing is being clear about which data will have which 
effect on which other data.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] devguide: Cover how to (un-)apply a patch.
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 19/01/2011 19:47, Antoine Pitrou wrote:

Unfortunately gnuwin32 patch doesn't play well with Windows 7.

I remember giving up on it completely and installing cygwin. This page 
seems to explain the details:

     http://math.nist.gov/oommf/software-patchsets/patch_on_Windows7.html

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html


