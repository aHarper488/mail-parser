
============================================================================
Subject: [Python-Dev] cpython: Fix time.steady(strict=True): don't use
	CLOCK_REALTIME
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Fix time.steady(strict=True): don't use
	CLOCK_REALTIME
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 26 Mar 2012 22:53:37 +0200
victor.stinner <python-checkins at python.org> wrote:

Victor, could we have a PEP on all this?
I think everyone has lost track of what you are trying to do with these
new methods.

cheers

Antoine.



