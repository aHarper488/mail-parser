
============================================================================
Subject: [Python-Dev] [Python-checkins] r86976 - in
	python/branches/py3k: Doc/library/configparser.rst
	Doc/library/fileformats.rst Lib/configparser.py
	Lib/test/test_cfgparser.py Misc/NEWS
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86976 - in
	python/branches/py3k: Doc/library/configparser.rst
	Doc/library/fileformats.rst Lib/configparser.py
	Lib/test/test_cfgparser.py Misc/NEWS
----------------------------------------
Author: =?utf-8?Q?=C5=81ukasz_Langa?
Attributes: []Content: 
Wiadomo?? napisana przez ?ric Araujo w dniu 2010-12-03, o godz. 19:35:


There have been minor incompatibilities, all have been thoroughly discussed with Fred and other developers. No changes should cause silent breakage, though.


Sure. Why not? :)


As we discussed this on IRC, this is an unfortunate slip caused by me trying to wrap up documentation updates in one go. Will remember to do that separately now. Thanks!


Corrected in rev 86999, thanks.


You're right that my formatting looks ugly but yours is not that much better either ;) How about:

        value_getter = lambda option: self._interpolation.before_get(self,
            section, option, d[option], d)
        if raw:
            value_getter = lambda option: d[option]
        return [(option, value_getter(option))
                for option in d.keys()]

-- 
Best regards,
?ukasz Langa
tel. +48 791 080 144
WWW http://lukasz.langa.pl/

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101203/668b63f3/attachment.html>

