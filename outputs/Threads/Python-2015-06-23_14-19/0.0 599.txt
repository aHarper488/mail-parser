
============================================================================
Subject: [Python-Dev] Whither 'trunk'
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Whither 'trunk'
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
The 'trunk' branch appears to have been frozen 12 days ago when 2.7 was 
released. I presume py3k is now the main development branch. Correct?

There are doc(s) on the site the directed people to the 'trunk' branch. 
If not updated (as seems from a python-list post today, but I asked the 
OP), it/they should be.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Whither 'trunk'
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Thu, Jul 15, 2010 at 09:19, Terry Reedy <tjreedy at udel.edu> wrote:

Yes.


What pages? I thought I updated the developer docs, but maybe I only
did the dev FAQ.

