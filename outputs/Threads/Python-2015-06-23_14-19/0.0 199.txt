
============================================================================
Subject: [Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/10/2010 9:13 AM, Benjamin Peterson wrote:

If I were committing a patch and was checking to see whether a name that 
started with a decorated A (or any other letter) were already in the 
list, I would look in the appropriate place in the A (or other) section, 
not after Z.

Everyone working on the English-based Python distribution knows the 
order of the 26 English letters. Please use that order (including for 
decorated versions and tranliterations) instead of various idiosyncratic 
and possibly conflicting nationality-based rules.

For instance, suppose a 'Jean Charbol' posts a patch? Should we really 
have to ask his/her 'nationality' before adding the name to the list? 
Suppose 'Charbol' was born in Spain but works in France? In Spain, at 
least, 'ch' words are alphabetized in dictionaries between 'c' and 'd' 
words. Did everyone already know that? I an mot ever sure if all 
Spanish-speaking countries still do that.

I am under the impression that either the Irish or Scots have some fussy 
rules for Mc/Mac/O names but I don't know them and don't think we should 
observe them in our list.

Librarians who filed author cards by birth nationality rules made the 
now-obsolete card catalogs less useful for users who not know both birth 
nationality and rule. Lets not repeat that mistake.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/10/2010 3:25 PM, Terry Reedy wrote:


Since the list is now utf-8 instead of latin-1 encoded, we could include 
the actual native character name, if supplied, in parentheses after the 
English-alphabetized transliteration.

If we were to follow native rules, all Japanese names, for instance, 
should be separately listed and ordered according to the Japanese order, 
which is quite different from the European orders.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/8/10 Terry Reedy <tjreedy at udel.edu>:

No, but if he complains about it, we should change it.


How often are people trying to search through Misc/ACKS, though?



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Tue, Aug 10, 2010 at 3:25 PM, Terry Reedy <tjreedy at udel.edu> wrote:
..

I believe, the golden standard for this type of works can be found in
the index pages of The Art of Computer Programming,

http://www-cs-faculty.stanford.edu/~knuth/help.html#exotic

It would be quite an effort to redo Misc/ACKS in that way, and even
with ASCII transliteration of every name, there is still ambiguity: is
"Van Rossum" sorted under "V", or under "R"? (See
http://www.python.org/~guido/ for an answer.)

Since it is apparent that no formal rule can be agreed upon, I think
best effort "rough alphabetical" order is just fine.  BTW, what is
Arfrever Frehtes Taifersar Arahesis' last name? :-)



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/10/2010 3:44 PM, Benjamin Peterson wrote:


If "In rough English alphabetical order" is extended with "unless the 
person requests otherwise", then it should also be extended with "in 
which case the name is suffixed with '(phbr)' [or something similar] for 
'put here by request'" so that a later, diligent person seeking to 
improve the ordering will not think that it out of standard order by 
accident or initial committer laziness and move it back. I believe we 
are having this discussion in part precisedly because Astrand after Z 
was not so tagged and was thought to have just been quickly appended.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 


So where do you put ???????? ?????????????

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Tue, Aug 10, 2010 at 6:29 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:
..

or ????????? ???????????? for that matter? :-)



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
On Wed, Aug 11, 2010 at 12:35 AM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

James Tauber did a UCA implementation in Python it seems:
http://jtauber.com/blog/2006/01/27/python_unicode_collation_algorithm/,

we could use this as a pre-commit hook to check changes on ACKS ;)



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 11.08.2010 00:35, schrieb Alexander Belopolsky:

If you care about that, feel free to add that spelling to the file.
Somebody proposed to put it along with some latin transliteration,
which I can sympathize with.

If just the nickname in cyrillic is fine with you, it's of course
fine, as well.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Tue, Aug 10, 2010 at 6:50 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:
..
That was Donald Knuth:
http://www-cs-faculty.stanford.edu/~knuth/help.html#exotic


I am more than happy with my entry in its current form. :-)

BTW, does anybody know if

Jiba = Jean-Baptiste LAMY ("Jiba")?

CCing SF address to find out.



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/10/2010 6:29 PM, "Martin v. L?wis" wrote:

As I said above, where the transliterated version Geor.. goes,
with the tranliteration followed by '(???????? ????????????)' as I 
suggested elsewhere

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
On Wed, Aug 11, 2010 at 12:56 AM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:
..

Yes that's it. He work on Soya 3d



----------------------------------------
Subject:
[Python-Dev] r83893 - python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 10 Aug 2010 15:25:52 -0400
Terry Reedy <tjreedy at udel.edu> wrote:

How does that solve anything?

I just had to decide whether ?Jason V. Miller? had to come before or
after ?Jay T. Miller? ('Jason' < 'Jay' but 'V' > 'T'). Knowledge of the
?English? alphabet isn't enough to make a resolution: an idiosyncratic
rule is still needed.
(and before you claim that rule is well-known: I had to ask)

Regards

Antoine.



