
============================================================================
Subject: [Python-Dev] [PEPs] Support the /usr/bin/python2 symlink
 upstream
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [PEPs] Support the /usr/bin/python2 symlink
 upstream
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
Folks, please stop CC'ing peps at python.org for non-PEP submissions.  They all
get held for moderator approval.  I've approved a few of them, but I'm going
to start rejecting them (so you get a bounce :) unless the message actually
contains a PEP.

cheerfully-co-editing-peps-ly y'rs,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110304/64e93b69/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] [PEPs] Support the /usr/bin/python2 symlink
 upstream
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 05, 2011, at 01:33 AM, Nick Coghlan wrote:


Thanks!
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110304/9f3fd155/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] [PEPs] Support the /usr/bin/python2 symlink
 upstream
----------------------------------------
Author: Westley =?ISO-8859-1?Q?Mart=EDnez?
Attributes: []Content: 
On Fri, 2011-03-04 at 22:21 +0100, "Martin v. L?wis" wrote:

I think the python2.exe or python3.exe (or both) for Windows would be a
good idea. I also think we need to seriously consider fixing the
double-click action for Windows. .py2 and .py3 extensions could work,
but seems clumsy.




----------------------------------------
Subject:
[Python-Dev] [PEPs] Support the /usr/bin/python2 symlink
 upstream
----------------------------------------
Author: Sridhar Ratnakuma
Attributes: []Content: 

On Sunday, March 6, 2011 at 9:53 AM, Brian Curtin wrote: 
 ActivePython for Windows includes the following binaries on Windows: 

* python.exe
* python3.2.exe
* python3.xe 
(plus the `pythonw` versions)

python3.2.exe is better than python32.exe because that matches the invocation (eg: "python3.2 -m foo") on Unix.

Similarly for 2.x (python2.exe / python2.7.exe).

All of this is particularly useful if the installer adds Python directory to %PATH% (which is the case for ActivePython, but not the one from python.org).

-srid 
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110307/901dc25f/attachment.html>

