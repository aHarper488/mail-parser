
============================================================================
Subject: [Python-Dev] transform() and untransform() methods,
 and the codec registry
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] transform() and untransform() methods,
 and the codec registry
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
Guido van Rossum wrote:

Indeed.


Sure, but those two particular methods only provide interfaces
to the codecs sub-system without actually requiring any major
implementation changes.

Furthermore, they "help ease adoption of Python 3.x" (quoted from
PEP 3003), since the functionality they add back was removed from
Python 3.0 in a way that makes it difficult to port Python2
applications to Python3.


How should I read this ? Do want the methods to be removed again
and added back in 3.3 ?

Frankly, I'm a bit tired of constantly having to argue against
cutting down the Unicode and codec support in Python3.

-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Dec 06 2010)
________________________________________________________________________

::: Try our new mxODBC.Connect Python Database Interface for free ! ::::


   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/



----------------------------------------
Subject:
[Python-Dev] transform() and untransform() methods,
 and the codec registry
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Notice that they wouldn't be inaccessible even if transform
is taken out again: codecs.lookup would still find them.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] transform() and untransform() methods,
 and the codec registry
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 09/12/2010 19:42, Guido van Rossum a ?crit :


Although I?d regret not having transform/untransform in 3.2, I think
there needs to be a discussion about this namespace issue before the new
methods ship.

Regards


