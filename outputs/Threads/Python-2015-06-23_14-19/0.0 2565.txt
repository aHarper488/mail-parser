
============================================================================
Subject: [Python-Dev] an alternative to embedding policy in PEP 418 (was:
 PEP 418: Add monotonic clock)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] an alternative to embedding policy in PEP 418 (was:
 PEP 418: Add monotonic clock)
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 28Mar2012 23:40, Victor Stinner <victor.stinner at gmail.com> wrote:
| > Does this primarily give a high resolution clock, or primarily a
| > monotonic clock? That's not clear from either the name, or the PEP.
| 
| I expect a better resolution from time.monotonic() than time.time(). I
| don't have exact numbers right now, but I began to document each OS
| clock in the PEP.

I wish to raise an alternative to these set-in-stone policy-in-the-library
choices, and an alternative to any proposal that does fallback in a function
whose name suggests otherwise.

Off in another thread on PEP 418 I suggested a cleaner approach to
offering clocks to the user: let the user ask!

My (just two!) posts on this are here:

  http://www.mail-archive.com/python-dev at python.org/msg66174.html
  http://www.mail-archive.com/python-dev at python.org/msg66179.html

The second post is more important as it fleshes out my reasons for
considering this appraoch better.

I've just finished sketching out a skeleton here:

  https://bitbucket.org/cameron_simpson/css/src/fb476fcdcfce/lib/python/cs/clockutils.py

In short:

  - take Victor's hard work on system clocks and classifying thm by
    feature set

  - tabulate access to them in a list of clock objects

  - base access class goes (example user call):

      # get a clock object - often a singleton under the hood
      T = get_clock(T_MONOTONIC|T_HIRES) or get_clock(T_STEADY|T_HIRES)
      # what kind of clock did I get?
      print T.flags
      # get the current time
      now = T.now

  - offer monotonic() and/or steady() etc as convenience functions
    calling get_clock() in a fashion like the above example

  - don't try to guess the user's use case ahead of time

This removes policy from the library functions and makes it both simple
and obvious in the user's calling code, and also makes it possible for
the user to inspect the clock and find out what quality/flavour of clock
they got.

Please have a glance through the code, especially the top and botth bits;
it is only 89 lines long and includes (presently) just a simple object for
time.time() and (importantly for the bikeshedding) an example synthetic
clock to give a monotonic caching clock from another non-monotonic clock
(default, again, time.time() in this prototype).

Suitably fleshed out with access to the various system clocks, this can
offer all the current bikeshedding in a simple interface and without
constraining user choices to "what we thought of, or what we thought
likely".

Cheers,
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

Availability: Samples Q1/97
              Volume  H2/97
So, it's vapor right now, but if you want to sell vapor in 1997 you
better had damn fast vapor then...
        - Burkhard Neidecker-Lutz on the DEC Alpha 21264, October 1996

