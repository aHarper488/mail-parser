
============================================================================
Subject: [Python-Dev] A sad state of doctests in the python manual
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] A sad state of doctests in the python manual
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
I have just discovered that sphinx supports running doctests embedded
in ReST documentation.   It looks like it is as simple as "cd Doc;
make doctest".  The result, however is not encouraging:

$ make doctest
...
Doctest summary
===============
 1162 tests
  262 failures in tests
    0 failures in setup code
...



----------------------------------------
Subject:
[Python-Dev] A sad state of doctests in the python manual
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On the second look, the problem may not be that bad - "make doctest"
picks up system python instead of the one from the source tree.  I'll
try to figure out how to rerun the doctests properly.

On Thu, Oct 28, 2010 at 4:48 PM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:



----------------------------------------
Subject:
[Python-Dev] A sad state of doctests in the python manual
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Oct 28, 2010 at 4:52 PM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

Nope, the problem is even worse.  It looks like Sphinx in py3k
requires 2.x python:

$ ../python.exe tools/sphinx-build.py -b doctest -d build/doctrees -D
latex_paper_size=  . build/doctest
Traceback (most recent call last):
  File "tools/sphinx-build.py", line 27, in <module>
    from sphinx import main
  File "tools/sphinx/__init__.py", line 44
    except ImportError, err:
                      ^
SyntaxError: invalid syntax



----------------------------------------
Subject:
[Python-Dev] A sad state of doctests in the python manual
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Oct 28, 2010, at 04:57 PM, Alexander Belopolsky wrote:


It would be really cool if you fixed this! <wink>

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101028/896e9a53/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] A sad state of doctests in the python manual
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Oct 28, 2010 at 5:18 PM, Barry Warsaw <barry at python.org> wrote:
..
Working on it.  Stay tuned. :-)



----------------------------------------
Subject:
[Python-Dev] A sad state of doctests in the python manual
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Oct 28, 2010 at 5:30 PM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

See http://bugs.python.org/issue10224

