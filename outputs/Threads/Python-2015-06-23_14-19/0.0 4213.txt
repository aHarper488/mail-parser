
============================================================================
Subject: [Python-Dev] Backports galore
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Backports galore
----------------------------------------
Author: =?utf-8?Q?=C5=81ukasz_Langa?
Attributes: []Content: 
Now we have (at least) the following libraries backported from 3.2+ to
older versions of Python by members of the core team:

- configparser
- enum34
- singledispatch
- subprocess32
- unittest2

There are also unofficial backports like billiard (multiprocessing).

I would be happy if all those were more discoverable by the community
at large. Having a single namespace for backports would be great but
my spidey sense forebodes large flocks of bike sheds flying that way.

Can we put links to those backports in the docs of older versions of
Python? Most users would be better off using the updated packages
while still deploying on an older release of Python.

-- 
Best regards,
?ukasz Langa

WWW: http://lukasz.langa.pl/
Twitter: @llanga
IRC: ambv on #python-dev




----------------------------------------
Subject:
[Python-Dev] Backports galore
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 16 June 2013 19:30, ?ukasz Langa <lukasz at langa.pl> wrote:

Step one is compiling a list. I started a page for that here:
http://wiki.python.org/moin/StandardLibraryBackports

(It's reachable from the front page of the wiki via the Useful Modules page)


It's probably better to start with a list anyone can edit, rather than
links that can only be updated by core developers.

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Backports galore
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
There is also faulthandler on PyPI. It is not really a backport since the
project developement started on PyPI.

Victor

Le dimanche 16 juin 2013, ?ukasz Langa a ?crit :

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130616/f2327462/attachment.html>

