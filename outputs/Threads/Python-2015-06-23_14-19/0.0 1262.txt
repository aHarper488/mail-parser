
============================================================================
Subject: [Python-Dev] url shortening services (was Re: standards for
 distribution names)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] url shortening services (was Re: standards for
 distribution names)
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
On 17/09/2010 12:54, Dan Buch wrote:

To echo a please from the main python list, please can we all stop using 
url shortening services?

This isn't twitter, we have no character limits, and as a result the 
cons by far outweigh any pros...

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk

