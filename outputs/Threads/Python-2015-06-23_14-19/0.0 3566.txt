
============================================================================
Subject: [Python-Dev] Accepting PEP 405 (Python Virtual Environments)
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Accepting PEP 405 (Python Virtual Environments)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
As the latest round of updates that Carl and Vinay pushed to the PEPs
repo have addressed my few remaining questions, I am accepting PEP 405
for inclusion in Python 3.3.

Thanks to all involved in working out the spec for what to model
directly on virtualenv, and areas where cleaner solutions could be
found given the power to tweak the behaviour of the core interpreter
and the standard library.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Accepting PEP 405 (Python Virtual Environments)
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 25.05.2012 10:44, schrieb Nick Coghlan:

Great!  Please remember that the next 3.3 alpha is scheduled for this
weekend, so please let me know in which timescale you plan to implement
this PEP.  If you want to commit it before this alpha, I can shift it
by a few days, but not a whole week since I'm on vacation for one week
from June 2nd.

Georg




----------------------------------------
Subject:
[Python-Dev] Accepting PEP 405 (Python Virtual Environments)
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Georg Brandl <g.brandl <at> gmx.net> writes:


I believe it is ready to integrate now. I aim to do it tomorrow (26 May) a.m.
UTC, so that it can make the next alpha.

Regards,

Vinay Sajip






----------------------------------------
Subject:
[Python-Dev] Accepting PEP 405 (Python Virtual Environments)
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Georg Brandl <g.brandl <at> gmx.net> writes:


It's now implemented in the default branch :-)

Regards,

Vinay Sajip


