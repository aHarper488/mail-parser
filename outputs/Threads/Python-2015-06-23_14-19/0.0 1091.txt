
============================================================================
Subject: [Python-Dev] A new warning category?
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

In the http://bugs.python.org/issue10093 discussion, I proposed to add a
specific warning category for unclosed files. The rationale is that
these warnings will happen in destructors and therefore filtering by
line number and filename doesn't make sense. So a new category would be
useful in order to allow defining specific rules.
Do you think it would go against the moratorium?

As for the category name, I would suggest ResourceWarning if we use it
specifically for resource-consumption warnings. Or perhaps DebugWarning
if we want to put all kinds of debugging helpers in it.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 14 Oct 2010 11:25:39 +0200
Antoine Pitrou <solipsis at pitrou.net> wrote:

Come to think of it, another reason for a separate warning category is
that we problably want those disabled by default (unless we're running
in pydebug mode perhaps).

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On Thu, 14 Oct 2010 08:25:39 pm Antoine Pitrou wrote:

Sounds like a reasonable suggestion to me.


It's not clear to me one way or the other, but I suspect yes.


-- 
Steven D'Aprano



----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Thu, Oct 14, 2010 at 02:25, Antoine Pitrou <solipsis at pitrou.net> wrote:

As one of the co-authors of the PEP I say no.


I say start with ResourceWarning and if we decide to generalize we can
make ResourceWarning subclass DebugWarning without breaking code.



----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 14.10.2010 11:25, schrieb Antoine Pitrou:

I think that warning categories are a library feature: they are
mentioned in the library documentation, and not mentioned in the
reference manual (in fact, only a single warning *is* mentioned in
the reference manual, namely that exceptions in __del__ are printed -
which actually doesn't use the warnings module).

If it is the library feature, then it is exempt from the moratorium:

Allowed to Change
The standard library

    As the standard library is not directly tied to the language
definition it is not covered by this moratorium.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Brett Cannon writes:

 > As one of the co-authors of the PEP I say no.

Procedural question: Doesn't authorship disqualify you from BDF1P, so
you can't pronounce?

(Benevolent Dictator for One PEP)




----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
  On 15/10/2010 08:22, Stephen J. Turnbull wrote:
I think Brett meant co-authorship of the *moratorium* PEP (which is 
already accepted).

At least that was how I read what Brett wrote.

All the best,

Michael



-- 

http://www.voidspace.org.uk/

READ CAREFULLY. By accepting and reading this email you agree,
on behalf of your employer, to release me from all obligations
and waivers arising from any and all NON-NEGOTIATED agreements,
licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap,
confidentiality, non-disclosure, non-compete and acceptable use
policies (?BOGUS AGREEMENTS?) that I have entered into with your
employer, its partners, licensors, agents and assigns, in
perpetuity, without prejudice to my ongoing rights and privileges.
You further represent that you have the authority to release me
from any BOGUS AGREEMENTS on behalf of your employer.




----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Oct 15, 2010 at 7:44 PM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

That's the way I took it as well.

The other factor here is that the moratorium was largely to help other
implementations get back up to speed with CPython as they dealt with
the transition to 3.x. Adding a new warning type isn't the kind of
moving target that something like PEP 380 would be.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] A new warning category?
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Fri, Oct 15, 2010 at 03:27, Nick Coghlan <ncoghlan at gmail.com> wrote:

And that is the way to take it. =)


Exactly. This is especially true if the other VMs use warnings.py
which will take care of all the details for them.

-Brett


