
============================================================================
Subject: [Python-Dev] Relative path in co_filename for zipped modules
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Relative path in co_filename for zipped modules
----------------------------------------
Author: Vitaly Murashe
Attributes: []Content: 
Dear Python developers community,

Recently I found out that it not possible to debug python code if it
is a part of zip-module.
Python version being used is 3.3.0

Well known GUI debuggers like Eclipse+PyDev or PyCharm are unable to
start debugging and give the following warning:
---
pydev debugger: CRITICAL WARNING: This version of python seems to be
incorrectly compiled (internal generated filenames are not absolute)
pydev debugger: The debugger may still function, but it will work
slower and may miss breakpoints.
---
So I started my own investigation of this issue and results are the following.

At first I took traditional python debugger 'pdb' to analyze how it
behaves during debugging of zipped module.
'pdb' showed me some backtaces and filename part for stack entries
looks malformed.
I expected something like
'full-path-to-zip-dir/my_zipped_module.zip/subdir/test_module.py'
but realy it looks like 'full-path-to-current-dir/subdir/test_module.py'

Source code in pdb.py and bdb.py (which one are a part of python
stdlib) gave me the answer why it happens.

The root cause are inside Bdb.format_stack_entry() + Bdb.canonic()

Please take a look at the following line inside 'format_stack_entry' method:

    filename = self.canonic(frame.f_code.co_filename)

For zipped module variable 'frame.f_code.co_filename' holds _relative_
file path started from the root of zip archive like
'subdir/test_module.py'
And as result Bdb.canonic() method gives what we have -
'full-path-to-current-dir/subdir/test_module.py'
---
So my final question.
Could anyone confirm that it is a bug in python core subsystem which
one is responsible for loading zipped modules,
or something is wrong with my zipped module ?

If it is a bug could you please submit it into official python bugtracker ?
I never did it before and afraid to do something wrong.

Thanks,
Vitaly Murashev



----------------------------------------
Subject:
[Python-Dev] Relative path in co_filename for zipped modules
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
Please file this as a bug report on bugs.python.org so that it doesn't
get lost.

See also http://bugs.python.org/issue13328 (which looks like a different bug
but could also be causing you problems).

--David

On Wed, 26 Jun 2013 00:11:04 +0400, Vitaly Murashev <vitaly.murashev at gmail.com> wrote:



----------------------------------------
Subject:
[Python-Dev] Relative path in co_filename for zipped modules
----------------------------------------
Author: Vitaly Murashe
Attributes: []Content: 
Reported as http://bugs.python.org/issue18307

On Wed, Jun 26, 2013 at 12:19 AM, R. David Murray <rdmurray at bitdance.com> wrote:

