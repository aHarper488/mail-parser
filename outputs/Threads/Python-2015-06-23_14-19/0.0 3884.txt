
============================================================================
Subject: [Python-Dev] Getting a list of registered codecs
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Getting a list of registered codecs
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
Before I raise a bug for this, can someone confirm if I've simply missed
something? I don't see any way, either in the docs or in the helpstrings
from the codecs, of listing the codecs that have been registered.

FWIW, I picked this up when I was looking at writing a simple encoding
converter, and I wanted to add a flag to list what conversions were
supported.
Paul.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130430/02b6774b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Getting a list of registered codecs
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On Tue, Apr 30, 2013 at 10:15:58AM +0100, Paul Moore wrote:

This may be of help:

http://hg.python.org/releasing/3.3.1/file/tip/Tools/unicode/listcodecs.py


-- 
Steven



----------------------------------------
Subject:
[Python-Dev] Getting a list of registered codecs
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
On 30.04.2013 11:15, Paul Moore wrote:

It would be possible to get a list of registered codec search functions,
but there's no API to ask the search functions for a list of supported
codecs.

If you're just looking for a list of codecs supported by the stdlib
encodings module, you can use the helper that Steven mentioned, or
you can scan the encoding aliases dictionary for codecs (but this will
not necessarily return all available codecs).

-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Apr 30 2013)
________________________________________________________________________
2013-04-30: Released eGenix PyRun 1.2.0 ...       http://egenix.com/go44
2013-04-17: Released eGenix mx Base 3.2.6 ...     http://egenix.com/go43

::::: Try our mxODBC.Connect Python Database Interface for free ! ::::::

   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/



----------------------------------------
Subject:
[Python-Dev] Getting a list of registered codecs
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
On 30.04.2013 11:52, Paul Moore wrote:

Nothing in particular, except maybe that it can be expensive to generate
such a list (e.g. you'd have to verify that the codec modules import
correctly and provide the needed getregentry() API).


-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Apr 30 2013)
________________________________________________________________________
2013-04-30: Released eGenix PyRun 1.2.0 ...       http://egenix.com/go44
2013-04-17: Released eGenix mx Base 3.2.6 ...     http://egenix.com/go43

::::: Try our mxODBC.Connect Python Database Interface for free ! ::::::

   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/



----------------------------------------
Subject:
[Python-Dev] Getting a list of registered codecs
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 30 April 2013 10:42, M.-A. Lemburg <mal at egenix.com> wrote:


OK, so there's no way to determine in advance what values of enc will work
in bytestr.decode(enc) or str.encode(enc)?

Is there a reason why not? As I say, a tool that offers to re-encode a file
could reasonably be expected to list the encodings it supported (if only to
help the user work out which way to spell utf-16le or utf16le or utf16-le
or utf-16-le or... :-))

I've raised http://bugs.python.org/issue17878 for this. Further discussion
may be more appropriate there than on python-dev.

Paul
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130430/5b140ef6/attachment.html>

