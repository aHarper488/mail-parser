
============================================================================
Subject: [Python-Dev] skip all TestCase methods if resource is not available
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] skip all TestCase methods if resource is not available
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Currently it is possible to mark individual test methods with:
            test_support.requires('network')

However, sometimes it is necessary to skip the whole TestCase if
'network' resource is not available counting the number of skipped
tests at the same time. Are there any standard means to do this?
-- 
anatoly t.

