
============================================================================
Subject: [Python-Dev] r88501 - python/branches/py3k/Lib/smtplib.py
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] r88501 - python/branches/py3k/Lib/smtplib.py
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
You're sure this will not cause tedious conflicts with backports?

Georg

On 22.02.2011 16:56, giampaolo.rodola wrote:






----------------------------------------
Subject:
[Python-Dev] r88501 - python/branches/py3k/Lib/smtplib.py
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 
Mmmm probably. smtplib patches aren't too big/many though.
Should I revert the change?


2011/2/23 Georg Brandl <g.brandl at gmx.net>:



----------------------------------------
Subject:
[Python-Dev] r88501 - python/branches/py3k/Lib/smtplib.py
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 24.02.2011 20:51, Giampaolo Rodol? wrote:

It's probably fine if you do the same change to the maintenance
branches as well.

Georg





----------------------------------------
Subject:
[Python-Dev] r88501 - python/branches/py3k/Lib/smtplib.py
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 
Done for Python 3.1 and 2.7.

2011/2/24 Georg Brandl <g.brandl at gmx.net>:

