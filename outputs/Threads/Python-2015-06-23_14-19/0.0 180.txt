
============================================================================
Subject: [Python-Dev] [Python-checkins] r83763 - in
	python/branches/py3k: Doc/library/signal.rst
	Lib/test/test_signal.py Misc/NEWS Modules/signalmodule.c
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r83763 - in
	python/branches/py3k: Doc/library/signal.rst
	Lib/test/test_signal.py Misc/NEWS Modules/signalmodule.c
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Fri, Aug 6, 2010 at 21:59, Ezio Melotti <ezio.melotti at gmail.com> wrote:


The sliced check was to make it more convenient to also check "os2" at the
same time in the first hunk of the change. Windows is "win32" regardless of
32 or 64-bit so that check works.

  class InterProcessSignalTests(unittest.TestCase):

Thanks for noticing this. Corrected in r83771 (py3k), r83772
(release31-maint), and r83773 (release27-maint).

 +

Good point. Fixed in the above revisions.

 if __name__ == "__main__":
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100806/f12be380/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r83763 - in
	python/branches/py3k: Doc/library/signal.rst
	Lib/test/test_signal.py Misc/NEWS Modules/signalmodule.c
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 7 August 2010 04:57, Brian Curtin <brian.curtin at gmail.com> wrote:


Wouldn't

if sys.platform in ('win32', 'os2', 'riscos'):

work just as well?

Paul



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r83763 - in
	python/branches/py3k: Doc/library/signal.rst
	Lib/test/test_signal.py Misc/NEWS Modules/signalmodule.c
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Sat, Aug 7, 2010 at 08:21, Hirokazu Yamamoto
<ocean-city at m2.ccsnet.ne.jp>wrote:



I had thought about doing this via switch statement. I'll propose a patch
and post it on #9324.

As for the "out of range" comment -- true, it's not technically a range on
Windows, but it matches the exception wording when we raise on Mac/Linux for
the same reason. I can change that.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100807/85bfbebc/attachment-0001.html>

