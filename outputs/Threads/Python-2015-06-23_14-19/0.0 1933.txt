
============================================================================
Subject: [Python-Dev] fatal error callback issue
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] fatal error callback issue
----------------------------------------
Author: Tom Whittoc
Attributes: []Content: 
I'm writing in regards to http://bugs.python.org/issue1195571

I'm embedding Python in my application and ran into a need for this
functionality. I wrote a similar patch myself, and was about to submit
it. When I searched for similar issues I found that this one has been
available since 2005.

I'd really like to help get this patch approved and integrated into
the python sources. I'm sure many other python embedders have run into
this in the past - it's a legitimate concern for any process which is
not 100% hosted in the Python world.

Thanks.
Tom.



----------------------------------------
Subject:
[Python-Dev] fatal error callback issue
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/8/2011 3:30 PM, Tom Whittock wrote:

Add a comment like this to the issue itself. Also review the most recent 
patch, considering Victor's comments. Is it better than yours? Can you 
improve it? Can you test it?

-- 
Terry Jan Reedy


