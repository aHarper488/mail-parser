
============================================================================
Subject: [Python-Dev] Interested in GSoC for biopython
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Interested in GSoC for biopython
----------------------------------------
Author: =?gb18030?B?yO7vow==?
Attributes: []Content: 
Hi,

I'm Zheng from the University of Georgia. I heard about the GSoC several weeks before and found biopython also involved in the peoject. I plan to apply for the GSoC 2013, hoping to make some contributions this summer. I browsed the proposals in biopython wiki sites and find two of them are all interesting topics, especially the codon alignment functionality. I know this has been implemented by pal2nal, and pal2nal is a good program as it accounts for the mismatches between protein and DNA sequences. However, it may raise an error when the protein sequence contains * indicating a stop codon, which is typical when the sequence is translated from genomic DNA. Maybe I could write a python implementation that relax this requirement. Many interesting statistical tests based on codon alignment can also be implemented. As I am new to this group, can anyone give me some suggestions about what I could do while preparing my proposal? Do I need to read the souce code of some major classes in BioPython to better understand how it works as well as the programming style? Thanks.


Best,
Zheng Ruan
Institute of Bioinformatics
The University of Georgia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130323/7ed01825/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Interested in GSoC for biopython
----------------------------------------
Author: Oscar Benjami
Attributes: []Content: 
On 22 March 2013 21:34, ?? <rz1991 at foxmail.com> wrote:
[SNIP]

This mailing list is for development of Python, not biopython. Try
asking on one of the biopython mailing lists:
http://biopython.org/wiki/Mailing_lists


Oscar

