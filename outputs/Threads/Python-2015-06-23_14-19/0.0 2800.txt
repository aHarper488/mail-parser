
============================================================================
Subject: [Python-Dev] Is is worth disentangling distutils?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Is is worth disentangling distutils?
----------------------------------------
Author: Antonio Cavall
Attributes: []Content: 
Hi,
I wonder if is it worth/if there is any interest in trying to "clean" up 
distutils: nothing in terms to add new features, just a *major* cleanup 
retaining the exact same interface.


I'm not planning anything like *adding features* or rewriting 
rpm/rpmbuild here, simply cleaning up that un-holy code mess. Yes it 
served well, don't get me wrong, and I think it did work much better 
than anything it was meant to replace it.

I'm not into the py3 at all so I wonder how possibly it could 
fit/collide into the big plan.

Or I'll be wasting my time?

Thanks




----------------------------------------
Subject:
[Python-Dev] Is is worth disentangling distutils?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Mon, Dec 10, 2012 at 1:22 AM, Antonio Cavallo
<a.cavallo at cavallinux.eu> wrote:

If you're not doing it on Python 3 then you are wasting your time.

