
============================================================================
Subject: [Python-Dev] next alpha sequence value from var pointing to array
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] next alpha sequence value from var pointing to array
----------------------------------------
Author: jdmorga
Attributes: []Content: 
Hello,

I am hoping that this list is a good place to ask this question.I am 
still fairly new to python, but find it to be a great scripting 
language.Here is my issue:

I am attempting to utilize a function to receive any sequence of letter 
characters and return to me the next value in alphabetic order e.g. send 
in "abc" get back "abd".I found a function on StackExchange (Rosenfield, 
A 1995) that seems to work well enough (I think):

def next(s):

strip_zs = s.rstrip('z')

if strip_zs:

return strip_zs[:-1] + chr(ord(strip_zs[-1]) + 1) + 'a' * (len(s) - 
len(strip_zs))

else:

return 'a' * (len(s) + 1)

I have found this function works well if I call it directly with a 
string enclosed in quotes:

returnValue = next("abc")

However, if I call the function with a variable populated from a value I 
obtain from an array[] it fails returning only ^K

Unfortunately, because I don't fully understand this next function I 
can't really interpret the error.Any help would be greatly appreciated.

Thanks ahead of time,

Derek

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120606/21d28dff/attachment-0001.html>

