
============================================================================
Subject: [Python-Dev] Why does _PyUnicode_FromId return a new reference?
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Why does _PyUnicode_FromId return a new reference?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Given it returns an eternal object, and it's almost always used
temporarily (for attribute lookup, string joining, etc.), it would seem
more practical for it to return a borrowed reference.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Why does _PyUnicode_FromId return a new reference?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 05.11.2011 23:26, schrieb Antoine Pitrou:

For purity reasons: all PyUnicode_From* functions return new references
(most of them return actually new objects most of the time); having
PyUnicode_FromId return a borrowed reference would break uniformity.
I personally it difficult to remember which functions return borrowed
references, and wish there were fewer of them in the API (with
PyArg_ParseTuple being the notable exception where borrowed references
are a good idea).

Now, practicality beats purity, so the real answer is: I just didn't
consider that it might return a borrowed reference.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Why does _PyUnicode_FromId return a new reference?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le 06/11/2011 08:08, "Martin v. L?wis" a ?crit :

I agree with this general sentiment. For PyUnicode_FromId, though, I 
think it makes sense to return a borrowed reference.

Regards

Antoine.


