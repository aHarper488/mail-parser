
============================================================================
Subject: [Python-Dev] PEP 411: Provisional packages in the Python
 standard library
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 411: Provisional packages in the Python
 standard library
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Jim J. Jewett wrote:

+1


-- 
Steven



----------------------------------------
Subject:
[Python-Dev] PEP 411: Provisional packages in the Python
 standard library
----------------------------------------
Author: Toshio Kuratom
Attributes: []Content: 
On Sat, Feb 11, 2012 at 04:32:56PM +1000, Nick Coghlan wrote:
Would using
    warnings.warn('This is a provisional API and may change radically from'
            ' release to release', ProvisionalWarning)

where ProvisionalWarning is a new exception/warning category (a subclaass of
FutureWarning?) be considered too intrusive?

-Toshio
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 198 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120211/a02c9eeb/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 411: Provisional packages in the Python
 standard library
----------------------------------------
Author: Isaac Morlan
Attributes: []Content: 
On Sat, 11 Feb 2012, Steven D'Aprano wrote:


Could the documentation generator simply insert the boilerplate if and 
only if the package has the __provisional__ attribute?  I'm not an expert 
in Python documentation but isn't it generated from properly-formatted 
comments within the Python source?

Isaac Morland			CSCF Web Guru
DC 2554C, x36650		WWW Software Specialist

