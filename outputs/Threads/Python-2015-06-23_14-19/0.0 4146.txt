
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.3): return NULL here
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): return NULL here
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/7/23 Christian Heimes <christian at python.org>:

It might always return NULL, but the compiler sees (PyObject *)NULL
when this function returns dl_funcptr.


--
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): return NULL here
----------------------------------------
Author: Gregory P. Smit
Attributes: []Content: 
On Tue, Jul 23, 2013 at 8:46 AM, Ronald Oussoren <ronaldoussoren at mac.com>wrote:


It seems fair to turn those on in 3.4 and require that third party
extensions clean up their code when porting from 3.3 to 3.4.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130723/5168cb3d/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): return NULL here
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 24 Jul, 2013, at 8:43, Gregory P. Smith <greg at krypto.org> wrote:


In this case its "just" code cleanup, the issue I filed (see above) is for another -Werror flag
that causes compile errors with some valid C99 code that isn't valid C89. That's good for
CPython itself because its source code is explicitly C89, but is not good when building 3th-party
extensions.

A proper fix requires tweaking the configure script, Makefile and distutils and that's not really
a fun prospect ;-)

Ronald





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): return NULL here
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Wed, 24 Jul 2013 09:01:30 +0200,
Ronald Oussoren <ronaldoussoren at mac.com> a ?crit :

Agreed. We shouldn't impose specific error flags on 3rd-party extension
writers.

Regards

Antoine.



