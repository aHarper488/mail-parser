
============================================================================
Subject: [Python-Dev] [Python-checkins] devguide: Start a doc on running
 and writing unit	tests.
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] devguide: Start a doc on running
 and writing unit	tests.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 


Not on Windows.
C:\Programs\Python32>./python -m test
'.' is not recognized as an internal or external command,
operable program or batch file.

python -m test
  works (until it failed, separate issue).

I would like to know, insofar as possible, how to run tests from the 
interpreter prompt (or IDLE simulation thereof)

from whatmod import whatfunc; whatfunc() # ??

ditto for such remaining alternatives you give as can be made from prompt.

Besides the convenience for Windows users (for whom the Command Prompt 
window is hidden away and possibly unknown), I think we should know if 
any tests are incompatible with interactive mode.

---
Terry Jan Reedy



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] devguide: Start a doc on running
 and writing unit	tests.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 05 Jan 2011 17:43:32 -0500
Terry Reedy <tjreedy at udel.edu> wrote:

This will not run the right interpreter, unless this is an installed
build.
You must use:
- "PCbuild\python_d.exe" in debug mode
- "PCbuild\python.exe" in release mode
- "PCbuild\amd64\python_d.exe" in 64-bit debug mode
- "PCbuild\amd64\python.exe" in 64-bit release mode


You can't. There is no such supported thing.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] devguide: Start a doc on running
 and writing unit	tests.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 


Is there a way to skip a particular test, such as one that crashes the 
test process?

Terry



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] devguide: Start a doc on running
 and writing unit	tests.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 05 Jan 2011 18:00:18 -0500
Terry Reedy <tjreedy at udel.edu> wrote:

-x test_foo




