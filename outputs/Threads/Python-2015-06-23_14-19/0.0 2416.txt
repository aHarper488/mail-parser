
============================================================================
Subject: [Python-Dev] What it takes to change a single keyword.
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] What it takes to change a single keyword.
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

In principle, python-list would be more appropriate, but this really
is a border case. So welcome!


Hmm. I also think editing Grammar/Grammar should be sufficient. Try
restricting yourself to ASCII keywords first; this just worked fine for
me.

Of course, if you change a single keyword, none of the existing Python
code will work anymore. See for yourself by changing 'def' to 'fed' (say).

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] What it takes to change a single keyword.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
2011/10/1 "Martin v. L?wis" <martin at v.loewis.de>:

For any changes where that isn't sufficient, then
http://docs.python.org/devguide/grammar.html provides a helpful list
of additional places to check (and
http://docs.python.org/devguide/compiler.html provides info on how it
all hangs together).

However, rather than *changing* the keywords, it would likely be
better to allow *alternate* keywords to avoid the problem Martin
mentioned with existing Python code failing to run (including the
entire standard library).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] What it takes to change a single keyword.
----------------------------------------
Author: =?ISO-8859-9?Q?Ya=FEar_Arabac=FD?
Attributes: []Content: 
Thanks to you both, I have made some progress on introducing my own keywords
to python interpreter. I think it is very kind of you to answer my question.
I think I can take it from here. Thanks again :)

02 Ekim 2011 05:42 tarihinde Nick Coghlan <ncoghlan at gmail.com> yazd?:




-- 
http://yasar.serveblog.net/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111002/4f3004e5/attachment.html>



----------------------------------------
Subject:
[Python-Dev] What it takes to change a single keyword.
----------------------------------------
Author: Francisco Martin Brugu
Attributes: []Content: 
Just Info on the links:

Those:

[1] <http://docs.python.org/devguide/compiler.html#id3>Skip Montanaro?s 
Peephole Optimizer Paper 
(http://www.foretec.com/python/workshops/1998-11/proceedings/papers/montanaro/montanaro.html)

[Wang97] <http://docs.python.org/devguide/compiler.html#id2>Daniel C. 
Wang, Andrew W. Appel, Jeff L. Korn, and Chris S. Serra. The Zephyr 
Abstract Syntax Description Language. 
<http://www.cs.princeton.edu/%7Edanwang/Papers/dsl97/dsl97.html> In 
Proceedings of the Conference on Domain-Specific Languages, pp. 213?227, 
1997.


are a 404

Cheers,

francis







----------------------------------------
Subject:
[Python-Dev] What it takes to change a single keyword.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
Please file a bug about the dead links so we can fix/remove them.

On Wed, Oct 5, 2011 at 13:29, Francisco Martin Brugue <
francisco.martin at web.de> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111005/31fd8eb6/attachment.html>



----------------------------------------
Subject:
[Python-Dev] What it takes to change a single keyword.
----------------------------------------
Author: Francisco Martin Brugu
Attributes: []Content: 
On 10/06/2011 12:00 AM, Brett Cannon wrote:
 > Please file a bug about the dead links so we can fix/remove them.
 >

it's done in http://bugs.python.org/issue13117 (and I've also tried with 
a patch).

Cheers,

francis




----------------------------------------
Subject:
[Python-Dev] What it takes to change a single keyword.
----------------------------------------
Author: =?ISO-8859-9?Q?Ya=FEar_Arabac=FD?
Attributes: []Content: 
Hi,

First of all, I am sincerely sorry if this is wrong mailing list to ask this
question. I checked out definitions of couple other mailing list, and this
one seemed most suitable. Here is my question:

Let's say I want to change a single keyword, let's say import keyword, to be
spelled as something else, like it's translation to my language. I guess it
would be more complicated than modifiying Grammar/Grammar, but I can't be
sure which files should get edited.

I'am asking this, because, I am trying to figure out if I could translate
keyword's into another language, without affecting behaviour of language.


-- 
http://yasar.serveblog.net/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110928/c86fe4e7/attachment.html>

