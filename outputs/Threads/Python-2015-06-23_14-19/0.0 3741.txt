
============================================================================
Subject: [Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Sat, Oct 27, 2012 at 7:36 AM, nick.coghlan
<python-checkins at python.org> wrote:


It might be worth clarifying what "latest" means for both the "x" and
"x.y" forms.  For example, should "http://docs.python.org/3/" today
link to the released 3.3.0 version, the in-progress 3.3.1 version, or
the in-progress 3.4.0?

For the "x" form, I think we mean the second because that is what it
points to today, i.e. the in-development version of the highest
released minor version of the major version "x".

There's a slight mismatch with how we're doing it today because
"http://docs.python.org/3/" shows 3.3.0 in the title even though it's
the in-progress 3.3.1.  The title should perhaps reflect that it's
post 3.3.0 (and similarly for the 2.7 and 3.2 pages).

--Chris



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Oct 28, 2012 at 9:34 PM, Georg Brandl <g.brandl at gmx.net> wrote:

Yes, I realised last night that it would be good to be more explicit
about this. The "release" hierarchy should be for versions exactly as
they were released, while the shorter ones draw directly from source
control (and thus may contain fixes which otherwise haven't been
released yet).

I updated the PEP to describe what Georg has put in place more
explicitly (initially I was going to propose a change to the way /dev/
URLs are handled, but then I realised what is already there made more
sense).

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Sun, Oct 28, 2012 at 4:34 AM, Georg Brandl <g.brandl at gmx.net> wrote:

One reason to change would be to avoid possible confusion created on
pages like this--

http://docs.python.org/3.3/whatsnew/3.2.html

where it says--

Author: Raymond Hettinger
Release: 3.3.0
Date: October 27, 2012

Would there be any disadvantage to changing the in-development titles
to read something like 3.3.0+, etc?

--Chris



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 28.10.2012 13:19, schrieb Chris Jerdonek:

Well, that block is a little silly anyway.  I would just delete the
"Release" and "Date" lines.


I'm not sure it would lower any confusion, instead of creating more.
("What is that + anyway? Do I need another version?" etc.)

Georg




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 28.10.2012 13:30, schrieb Antoine Pitrou:

Agreed, I've now removed them.

Georg




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 28.10.2012 13:54, schrieb Chris Jerdonek:

Right.  I think they are remnants from the LaTeX age; you can remove
them all if you want.

Georg




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Mark Lawrenc
Attributes: []Content: 
On 28/10/2012 12:39, Georg Brandl wrote:

Is it worth removing this as well, it's a couple of lines down

"See also PEP 392 - Python 3.2 Release Schedule" ?

-- 
Cheers.

Mark Lawrence.




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 28.10.2012 17:23, schrieb Mark Lawrence:

Why?  PEP 392 is not out of date, and for those who want to know when
exactly 3.2 and its minor versions were released, it is useful.

Georg




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: PEP for updating the URL
	layout on docs.python.org
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Sat, Oct 27, 2012 at 7:36 AM, nick.coghlan
<python-checkins at python.org> wrote:

Related to this, just checking to see if anyone is taking
responsibility for following up with http://www.python.org.  I imagine
they have several links that may no longer point to the correct
version (e.g. the "Documentation" link under "Quick Links (2.7.3)" in
the left column of the home page now results in
http://docs.python.org/3/, and similarly for 2.x on
http://www.python.org/doc/ ).

--Chris

