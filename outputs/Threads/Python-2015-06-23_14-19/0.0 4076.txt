
============================================================================
Subject: [Python-Dev] usefulness of "extension modules" section in Misc/NEWS
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] usefulness of "extension modules" section in Misc/NEWS
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 
Hi,

What's exactly the guideline for choosing between the "Library" and
"Extension modules" section when updating Misc/NEWS?
Is it just the fact that the modified files live under Lib/ or Modules/?

I've frequently made a mistake when updating Misc/NEWS, and when
looking at it, I'm not the only one.

Is there really a good reason for having distinct sections?

If the intended audience for this file are end users, ISTM that the
only things that matters is that it's a library change, the fact that
the modification impacted Python/C code isn't really relevant.

Also, for example if you're rewriting a library from Python to C (or
vice versa), should it appear under both sections?

FWIW, the What's new documents don't have such a distinction.

Cheers,

cf

