
============================================================================
Subject: [Python-Dev] I need help with IO testuite
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] I need help with IO testuite
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Hi all. I am modifying IO module for Python 3.2, and I am unable to
understand the mechanism used in IO testsuite to test both the C and the
Python implementation.

In particular I need to test that the implementation passes some
parameters to the OS.

The module uses "Mock" classes, but I think "Mock" is something else,
and I don't see how it interpose between the C/Python code and the OS.

If somebody could explain the mechanism a bit...

Thanks for your time and attention.

Some background: http://bugs.python.org/issue10142

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBTOPnJplgi5GaxT1NAQLVqQP/cf9+hdLdoSMzY+cSquq7YZMiQOQ0aMEH
ZRn+su4F3qg5e8MgEQOXFj9uGEjVDLwonE4nBZ+T3ovBcPCyGaLB/K/YttZGVM5/
O3gpzZss9bkMvuWQCblyEJp8uzJC831AwPDMg1Q0nbMiTnJlW5dY1CX9BD0gYPBW
oIVBt2oBfCI=
=hq7M
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] I need help with IO testuite
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 17 Nov 2010 15:31:02 +0100
Jesus Cea <jcea at jcea.es> wrote:

It doesn't interpose between Python and the OS: it mocks the OS.  It
is, therefore, a mock (!).

Consequently, if you want to test that parameters are passed to the OS,
you shouldn't use a mock, but an actual file. There are several tests
which already do that, it shouldn't be too hard to write your own.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] I need help with IO testuite
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Nov 18, 2010 at 12:31 AM, Jesus Cea <jcea at jcea.es> wrote:

The "Mock" refers to stubbing out or substituting various layers of
the IO stack with the Python implementations in the test file. It
isn't related specifically to the C/Python switching.


The actual C/Python switching happens later in the file. It is best to
start from the bottom of the file (with the list of test cases that
are actually executed) and work your way up from there.

For what Amaury is talking about, what you can test is that the higher
layers of the IO stack (e.g. BufferedReader) correctly pass the new
flags down to the RawIO layer. You're correct that you can't really
test that RawIO is actually passing the flags down to the OS. However,
if you have a way to check whether the filesystem in use is ZFS, you
may be able to create a conditionally executed test, such that correct
behaviour can be verified just by running on a machine that uses ZFS
for its temp directory.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] I need help with IO testuite
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Nov 18, 2010 at 12:58 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

On further thought, the test should probably be unconditional - just
allow a ValueError as an acceptable result that indicates the
underlying filesystem isn't ZFS.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

