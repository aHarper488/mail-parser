
============================================================================
Subject: [Python-Dev] FS: [issue9141] Allow objects to decide if they can be
 collected by GC
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] FS: [issue9141] Allow objects to decide if they can be
 collected by GC
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
Hi there. Antoine Pitrou suggested that I float this on python-dev again.  The new patch should
1) be much simpler and less hacky
2) remove the special case code for PyGenObject from gcmodule.c
K

________________________________________
Fr?: Kristj?n Valur J?nsson [report at bugs.python.org]
Sent: 5. apr?l 2012 11:55
To: Kristj?n Valur J?nsson
Efni: [issue9141] Allow objects to decide if they can be collected by GC

Kristj?n Valur J?nsson <kristjan at ccpgames.com> added the comment:

Here is a completely new patch.  This approach uses the already existing tp_is_gc enquiry slot to signal garbage collection.
The patch modifies the generator object to use this new mechanism.
The patch keeps the old PyGen_NeedsFinalizing() API, but this can now go away, unless people think it might be used in extension modules

(why do we always expose all those internal apis from the dll? I wonder.)

----------
Added file: http://bugs.python.org/file25131/ob_is_gc.patch

_______________________________________
Python tracker <report at bugs.python.org>
<http://bugs.python.org/issue9141>
_______________________________________

