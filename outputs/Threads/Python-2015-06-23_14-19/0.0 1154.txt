
============================================================================
Subject: [Python-Dev] Regular scheduled releases
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Regular scheduled releases
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

I don't feel like producing a complete list of build steps; the entire
process takes about four hours.
The steps that are difficult to automate are:
- code signing
- testing the resulting installer
- uploading

However, in a significant number of releases, some of the build steps
failed, so it requires some investigation to find the source of the
problem.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Regular scheduled releases
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Sat, Oct 30, 2010 at 14:09, "Martin v. L?wis" <martin at v.loewis.de> wrote:

So is most of this scripted, or is there just a process in your head?


Why are those steps difficult to automate? I'd think that uploading is
just some ftp commands. Signing might be a thing because you don't
want the keys distributed. Testing the resulting installer could
probably be automated in part, and people could still download the
nightlies to do more testing.


Well, sure, but if we do continuous builds we find failures like that sooner.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Regular scheduled releases
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 30.10.2010 14:29, schrieb Dirkjan Ochtman:

Define "most". There hundreds of commands that run automatically (like
all compiler invocations). But still, there are about 20 steps that are
in my head, some written down in plain English.

But yes, "most" of the steps (by sheer number) are automated.


No, it's ssh, and you need access to the right key. More importantly,
it's editing content.ht, to update the links, compute and copy the
md5sums and size, and svn add the GPG signatures.

It *could* be automated, I suppose. It just isn't, and it would take
quite some time to automate it.


People will never ever test nightly builds. Been there, done that.
Instead, the nightly build process will break, and nobody will fix
it for months (or even complain, for that matter).

I also doubt that the installer could be automatically tested with
reasonable effort.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Regular scheduled releases
----------------------------------------
Author: David Bole
Attributes: []Content: 
"Martin v. L?wis" <martin at v.loewis.de> writes:


Certainly seems to be past experience.

I know Martin knows this, but for other readers who may not, my
Windows XP buildbot built nightly windows installers (the basic MSI
package, close but not necessarily a fully signed new release as
Martin makes) starting in September, 2007.  It ran successfully for
about 6 months, at which point it started to fail fairly consistently.
Nobody noticed, and Martin and I finally just shut it down in
December, deciding it wasn't worth the effort to try to fix.

My OSX buildbot has been building nightly DMG images (though again, I
suspect Ronald has a few extra steps beyond what its doing for a full
release) since April.  I'd actually be interested in knowing if anyone
is using them - I suspect perhaps not.

In both cases, getting the process going actually took quite a bit of
effort (even stuff like having to fix the buildbot upload code in the
Windows case), not just on my part, but with the help of Martin and
Ronald.  But without actual use of the result, it's hard to think it
was worth it.  I'm pretty sure my default reaction to a break-down in
the current OSX build process at this point would be to first suggest
disabling it unless there were real users.

-- David


