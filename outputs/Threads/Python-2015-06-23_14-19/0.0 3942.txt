
============================================================================
Subject: [Python-Dev] why do we allow this syntax?
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
Hi All,

Just had a bit of an embarrassing incident in some code where I did:

sometotal =+ somevalue

I'm curious why this syntax is allowed? I'm sure there are good reasons, 
but thought I'd ask...

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Donald Stuff
Attributes: []Content: 
On Friday, February 8, 2013 at 10:39 AM, Chris Withers wrote:
I'm guessing this gets parsed as sometotal = +somevalue 


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130208/cdac6f25/attachment.html>



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/2/8 Chris Withers <chris at simplistix.co.uk>:

That's just a strange way of expressing

sometotal = +somevalue



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Sat, Feb 9, 2013 at 2:39 AM, Chris Withers <chris at simplistix.co.uk> wrote:

For the same reason that you can negate a value with:

sometotal = -somevalue

The unary + and - operators are seldom problematic.

ChrisA



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Xavier More
Attributes: []Content: 

On 2013-02-08, at 16:39 , Chris Withers wrote:


sometotal = (expression) is valid syntax, and +value is valid syntax.

Thus what you wrote is perfectly normal syntax, it's the assignment of a
pos'd value, badly formatted. pep8.py will warn against it (it'll
complain that the whitespace around `+` is wonky). But I see no
justification for disallowing this, anymore than for disallowing the
rougly equivalent (and just as error-prone) `sometotal = -somevalue`.



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Oscar Benjami
Attributes: []Content: 
On 8 February 2013 15:39, Chris Withers <chris at simplistix.co.uk> wrote:

Because '+' can represent an unary prefix operator just the same as '-':

-3


Oscar



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 08.02.13 17:39, Chris Withers wrote:

And why this syntax is allowed?

     pi = 3,14

And this:

     fruits = [
         'apple',
         'pear',
         'banana'
         'ananas',
         'mango'
     ]





----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
On 08/02/2013 15:42, Benjamin Peterson wrote:

Indeed, but why should this be possible? When could it do something 
useful? :-)

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/2/8 Chris Withers <chris at simplistix.co.uk>:

+ is a normal overridable operator.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Oscar Benjami
Attributes: []Content: 
On 8 February 2013 16:10, Benjamin Peterson <benjamin at python.org> wrote:

Decimal.__pos__ uses it to return a Decimal instance that has the
default precision of the current Decimal context:

Decimal('0.33333333333333333333333333333333333333')
Decimal('0.3333333333333333333333333333')


Oscar



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
On 08/02/2013 16:17, Oscar Benjamin wrote:

That's the answer I was hoping wouldn't be coming...

Oh well, guess I'll have to improve on my sloppy typing.

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Xavier More
Attributes: []Content: 
On 2013-02-08, at 18:45 , Chris Withers wrote:


Or just run flake8 (with a bit of configuration to disable the annoying stuff).



----------------------------------------
Subject:
[Python-Dev] why do we allow this syntax?
----------------------------------------
Author: Ian Cordasc
Attributes: []Content: 
On Feb 8, 2013 3:37 PM, "Xavier Morel" <catch-all at masklinn.net> wrote:
stuff).

As flake8's maintainer, I second this.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130208/57dc00ba/attachment.html>

