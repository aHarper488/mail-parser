
============================================================================
Subject: [Python-Dev] Getting an optional parameter instead of creating
	a socket internally (was: Re: stdlib socket usage and &quot;
	keepalive&quot; )
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Getting an optional parameter instead of creating
	a socket internally (was: Re: stdlib socket usage and &quot;
	keepalive&quot; )
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Apr 12, 2010 at 4:19 PM, Jesus Cea <jcea at jcea.es> wrote:

Yeah, this sounds like a useful change. Would be very useful for testing too.

-- 
--Guido van Rossum (python.org/~guido)

