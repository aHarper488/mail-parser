
============================================================================
Subject: [Python-Dev] How to get commit access
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] How to get commit access
----------------------------------------
Author: chandr
Attributes: []Content: 
I am waiting for the bug Issue5111 (httplib: wrong Host header when
connecting to IPv6 litteral URL)  to be fixed for a very long.

Even I attached patches, test patches. How to get commit access so that I
can fix such issues ( HTTP lib )

- Chandrasekar
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101018/d06efa16/attachment.html>



----------------------------------------
Subject:
[Python-Dev] How to get commit access
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
Hello Chandrasekar,

On Mon, Oct 18, 2010 at 10:05:56PM +0530, chandru wrote:

I just had a look at the bug. Looks like a minor change and tests are
there too. I shall check that it does not break of any of existing
changes and commit it.


The details on "How to contribute to Python" are here:
http://www.python.org/dev/contributing/

-- 
Senthil

Blutarsky's Axiom:
	Nothing is impossible for the man who will not listen to reason.

