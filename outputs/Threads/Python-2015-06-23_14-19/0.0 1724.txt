
============================================================================
Subject: [Python-Dev] devguide: Add a doc outlining how to add something
 to the stdlib.
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devguide: Add a doc outlining how to add something
 to the stdlib.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 16 Jan 2011 21:38:43 +0100
brett.cannon <python-checkins at python.org> wrote:

Actually, most feature requests get approved without this intermediate
step. So I would suggest directing people to the tracker instead.
Only very large or controversial additions usually get refused on these
grounds.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: Add a doc outlining how to add something
 to the stdlib.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jan 17, 2011 at 12:00, Antoine Pitrou <solipsis at pitrou.net> wrote:

I weakened it to a suggestion, but didn't cut it entirely as I still
think it's a good step to take.

-Brett




----------------------------------------
Subject:
[Python-Dev] devguide: Add a doc outlining how to add something
 to the stdlib.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jan 17, 2011 at 12:32, R. David Murray <rdmurray at bitdance.com> wrote:

done

