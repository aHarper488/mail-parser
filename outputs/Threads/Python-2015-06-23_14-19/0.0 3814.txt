
============================================================================
Subject: [Python-Dev] [Announcement] New mailing list for code quality tools
 including Flake8, Pyflakes and Pep8
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Announcement] New mailing list for code quality tools
 including Flake8, Pyflakes and Pep8
----------------------------------------
Author: Ian Cordasc
Attributes: []Content: 
Hello,

There's a new mailing-list related to Python code-quality tools.

Are you concerned about the evolution of various code checkers?
Do you have questions or suggestions?

Subscribe here:
http://mail.python.org/mailman/listinfo/code-quality

Best regards,
Ian

