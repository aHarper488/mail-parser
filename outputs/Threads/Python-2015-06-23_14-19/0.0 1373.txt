
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (merge 3.2 -> default):
 Correctly merging #9319 into 3.3?
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (merge 3.2 -> default):
 Correctly merging #9319 into 3.3?
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

If a patch in 3.2 is not applicable in 3.3, a "null merge" should be
done. If not, next developer tring to merge will find some other
unrelated code to merge, and she doesn't have the context knowledge to
know what to do :-).

In this case, I merged code that doesn't actually compile, breaking the
build for 20 minutes :-).

And yes, I fully realized that I should try to compile locally first.
Dealing with this unexpected merge when merging my own patch was...
unexpected, and the code seemed sensible enough.

Do we have some hat-of-shame I should wear because breaking the build? :).

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCUAwUBTbTgpplgi5GaxT1NAQJqegP3QSVIf6yszZrFJEgKTaK4XXvHB965PdYN
T9g8bx5IKXmiMjDBCatjuA2AAtwnL0Wd2Dw0tnGhRTqYHD2l+cMcFw/2JtV4L6sC
c0fKm2o+V8gSW7KZwdvgNWiQlzE3lp2DiD/ng3gM3JlK/EKghIH8acDiJsHHrQtS
7T7iSLllOw==
=+50u
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (merge 3.2 -> default):
 Correctly merging #9319 into 3.3?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 25 Apr 2011 04:47:03 +0200
Jesus Cea <jcea at jcea.es> wrote:

You should *always* recompile and run the affected tests before checking
in a change. Even if the changes look "trivial".
By trying to save a little time on your side your may lose a lot of
other people's time.


The tests are still broken it seems:

======================================================================
ERROR: test_issue9319 (test.test_imp.ImportTests)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/Users/pythonbuildbot/buildarea/3.x.hansen-osx-x86-2/build/Lib/test/test_imp.py", line 181, in test_issue9319
    imp.find_module, "test/badsyntax_pep3120")
  File "/Users/pythonbuildbot/buildarea/3.x.hansen-osx-x86-2/build/Lib/unittest/case.py", line 574, in assertRaises
    callableObj(*args, **kwargs)
ImportError: No module named 'test/badsyntax_pep3120'


Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (merge 3.2 -> default):
 Correctly merging #9319 into 3.3?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le lundi 25 avril 2011 ? 04:47 +0200, Jesus Cea a ?crit :

Correct. Sorry, I forgot that. And yes, the 3.2 fix was not applicable
to 3.3, that's why I forgot to merge.


Hum, you may read the history of the issue to decide what to do, or ask
the commiter to do the merge.


He he, it was a trap! When you touch one of my commit, all buildbots
turn red! :-)


Don't worry, it doesn't matter if you quickly fix your mistake.

Victor


