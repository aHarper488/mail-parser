
============================================================================
Subject: [Python-Dev] framework build for 2.7 using old sqlite?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] framework build for 2.7 using old sqlite?
----------------------------------------
Author: Doug Hellman
Attributes: []Content: 
I'm trying to write a little program that uses the full text search extension module for sqlite with Python 2.7 on Snow Leopard.  I installed Python by downloading the DMG file from python.org.  According to the Python docs (http://docs.python.org/library/sqlite3.html#sqlite3.Connection.enable_load_extension), 2.7 should include the functions for handling extension modules, but when I try to use them they are not defined (I get an AttributeError when I call the related methods on the Connection object).

In Modules/_sqlite/connection.c I see that there is an #ifdef for HAVE_LOAD_EXTENSION, which is in turn only defined if both the version number is high enough and SQLITE_OMIT_LOAD_EXTENSION is not set.

I think the problem is that the build of Python in the DMG I download was created with an old version of the SQLite libraries:

	farnsworth:dhellmann:~:503 $ which python
	/Library/Frameworks/Python.framework/Versions/2.7/bin/python

	farnsworth:dhellmann:~:501 $ python
	Python 2.7 (r27:82508, Jul  3 2010, 21:12:11) 
	[GCC 4.0.1 (Apple Inc. build 5493)] on darwin
	Type "help", "copyright", "credits" or "license" for more information.
	>>> import sqlite3
	>>> sqlite3.version_info
	(2, 6, 0)


Can anyone confirm that the installer image for OS X 10.5 and later (http://python.org/ftp/python/2.7/python-2.7-macosx10.5.dmg) was created with an old SQLite library?  Is there some way to update that DMG, or do I need to build Python from source myself?

Thanks,
Doug




----------------------------------------
Subject:
[Python-Dev] framework build for 2.7 using old sqlite?
----------------------------------------
Author: Doug Hellman
Attributes: []Content: 

On Oct 3, 2010, at 11:29 AM, Doug Hellmann wrote:


Forget that, the version info is for pysqlite, not the underlying libraries.

I found that Python's setup.py has SQLITE_OMIT_LOAD_EXTENSION set, which disables this feature (http://svn.python.org/view?view=rev&revision=78688).

Doug


