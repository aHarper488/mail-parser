
============================================================================
Subject: [Python-Dev] Issue #8863 adds a new PYTHONNOFAULTHANDLER
	environment variable
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Issue #8863 adds a new PYTHONNOFAULTHANDLER
	environment variable
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

In 2008, I proposed a patch to raise a Python exception on SIGSEVG 
signal. In some cases, it's possible to catch the exception, log it, and 
continue the execution. But because in some cases, the Python internal 
state is corrupted, the idea was rejected (see the issue #3999).

Someone asked me to display the Python backtrace on SIGSEGV, instead of 
raising an exception. I implemented this idea in issue #8863. After 9 
versions, I think that the patch is ready for inclusion. It catchs 
SIGSEGV, SIGFPE, SIGBUS and SIGILL signals, and also display the Python 
backtrace on fatal errors. Because some operating systems have their own 
fault handler (eg. Ubuntu with apport), Dave Malcolm asked me to add an 
option disable the Python handler. That's why I added the 
PYTHONNOFAULTHANDLER environment variable (we all love long variable 
names!). For an OS vendor, it looks like an environment variable is more 
practical than a command line option. Eg. Mandriva sets 
PYTHONDONTWRITEBYTECODE for the whole system.

Raymond Hettinger asked me on IRC to write an email about the new 
environment variable, so here is the question: do you agree to add the 
new variable?

Victor



----------------------------------------
Subject:
[Python-Dev] Issue #8863 adds a new PYTHONNOFAULTHANDLER
	environment variable
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 18.12.2010 01:55, schrieb Victor Stinner:

I very much like having a traceback on (some) segmentation faults, but
it's clear there needs to be a way to turn it off.  An environment variable
seems to be the obvious choice (for the reasons you stated for
PYTHONDONTWRITEBYTECODE).

Georg




----------------------------------------
Subject:
[Python-Dev] Issue #8863 adds a new PYTHONNOFAULTHANDLER
	environment variable
----------------------------------------
Author: James Y Knigh
Attributes: []Content: 

On Dec 17, 2010, at 7:55 PM, Victor Stinner wrote:


I think instead of calling abort() to kill the process, you should:
- install the signal handler with SA_NODEFER|SA_RESETHAND (or if sigaction is not present, explicitly reset the action to SIG_DFL and unblock first thing upon entering the handler), and then,
- at the end of the handler, kill(getpid(), orig_signal) in order to abort the process.

This has two advantages: 1) the process's exit code will actually show the correct signal, 2) it might let the OS fault handlers work properly as well -- I'm not sure. If it does, you may want to experiment with whether having or omitting SA_NODEFER gives a better backtrace (from the OS mechanism) in that case.

If #2 actually works, you may not even need the env var, which would be nice. 

James



----------------------------------------
Subject:
[Python-Dev] Issue #8863 adds a new PYTHONNOFAULTHANDLER
	environment variable
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 18.12.2010 14:57, schrieb Victor Stinner:

Well, without a closer I assume that for some crashes it's just not
possible anymore for the Python interpreter to even print out the
traceback?

Georg




----------------------------------------
Subject:
[Python-Dev] Issue #8863 adds a new PYTHONNOFAULTHANDLER
	environment variable
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 18.12.2010 14:57, schrieb Victor Stinner:

In any case, this is coming pretty late; beta 2 is scheduled for this
weekend, and even if this is something that only kicks in when all hope
is lost anyway, it is a new feature.  I should like to hear approval
from a few more devs before I will let this go into 3.2.

Georg





----------------------------------------
Subject:
[Python-Dev] Issue #8863 adds a new PYTHONNOFAULTHANDLER
	environment variable
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le lundi 20 d?cembre 2010 08:22:40, vous avez ?crit :

I tested when the GIL released: the fault handler is unable to retrieve "the" 
thread state and so the backtrace is not printed. Which thread state should be 
retrieve? I don't know yet: maybe the last active thread state?

It doesn't look trivial to fix this issue because if there is no thread (only 
the main thread), release the GIL clears the thread state pointer (to it to 
NULL) without storing it somewhere else (whereas with multiple threads, the 
last active thread is stored in "gil_last_holder". I should ask Antoine (or 
someone one) how the new GIL works.


Another variable might be created to store the (pointer to the) last active 
thread state.


We all love deadlocks :-)

Victor

