
============================================================================
Subject: [Python-Dev] Range __contains__ and objects with __index__ methods
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Range __contains__ and objects with __index__ methods
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Starting in Python 3.2, range() supports fast containment checking for
integers (i.e. based on an O(1) arithmetic calculation rather than an
O(N) iteration through the entire sequence).

Currently, this fast path ignores objects that implement __index__ -
they are relegated to the slow iterative search.

This seems wrong to me - the semantics of __index__ are such that it
is meant to be used for objects that are alternative representations
of the corresponding Python integers (e.g. numpy scalars, or integers
that use a particular number of bits in memory). Under that
interpretation, if an object provides __index__, we should use the
fast path instead of calling __eq__ multiple times.

Thoughts?

Regards,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

