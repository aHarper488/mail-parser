
============================================================================
Subject: [Python-Dev] Use our strict mbcs codec instead of the Windows
 ANSI	API
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Use our strict mbcs codec instead of the Windows
 ANSI	API
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

Can you please elaborate what APIs you are talking about exactly?

If it's the byte APIs (i.e. using bytes as file names), then I'm -1 on
this proposal. People that explicitly use bytes for file names deserve
to get whatever exact platform semantics the platform has to offer. This
is true on Unix, and it is also true on Windows.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Use our strict mbcs codec instead of the Windows
 ANSI	API
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

So your proposal is that abspath(b".") shall raise a UnicodeError in
this case?

Are you serious???


Except people running into the very issues you are trying to resolve.
I'm not sure these people are really helped by having their applications
crash all of a sudden.

Regards,
Martin

