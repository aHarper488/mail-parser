
============================================================================
Subject: [Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Sat, May 22, 2010 at 11:38 AM, Guido van Rossum <guido at python.org> wrote:
[snip]

So, after some cool down - and the last rounds of discussion which
triggered some jiggery-pokery on Brian's part, I'm accepting PEP 3148
"futures - execute computations asynchronously". I feel that it's a
good addition, and a good start for something bigger down the road.

Brian - you'll need to provide someone such as Martin or Georg your
public key for ssh access into SVN, and you'll need developer access
to the bug tracker.

jesse



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/7/11 Jesse Noller <jnoller at gmail.com>:

Oh, don't worry. He's already been hacking happily on Windows stuff.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Sun, Jul 11, 2010 at 10:07 PM, Benjamin Peterson <benjamin at python.org> wrote:

Wrong Brian - that's Brian Curtin, this is Brian Quinlan - I double
checked the committer's list (http://www.python.org/dev/committers).

We now have two Brians. I say we name them PresentBrian and FutureBrian.

jesse



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/7/11 Jesse Noller <jnoller at gmail.com>:

My apologies, Brians!


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Titus von der Malsbur
Attributes: []Content: 
Hi I learned about the futures PEP only today.  I saw the example on
http://code.google.com/p/pythonfutures/

One thing that worries me is that this approach seems to bypass the
usual exception handling mechanism of Python.  In particular I'm
wondering why you have to do things like:

  if future.exception() is not None:
    ...

This reminds me a lot of how things are done in C but it's not very
pythonic.  Wouldn't it be possible and nicer to raise the exception --
if there was one inside the asynchronous job -- when the result of the
future is accessed?

  try:
    print('%r page is %d bytes' % (url, len(future.result())))
  except FutureError:
    print('%r generated an exception: %s' % (url, future.exception()))
    
Best,

  Titus



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jul 13, 2010 at 12:19 AM, Titus von der Malsburg
<malsburg at gmail.com> wrote:

That's what actually happens, so you can code it either way (either
just calling result() and dealing with any exception that was raised,
or else checking for a non-None exception value directly).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Titus von der Malsbur
Attributes: []Content: 
On Tue, Jul 13, 2010 at 12:48:35AM +1000, Nick Coghlan wrote:

That's great!  None of the examples I found used the pythonic
exception style, that's why I assumed that checking the "return value"
is the only possibility.  Reading the PEP carefully would have helped.
:-)

  Titus



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Brian Quinla
Attributes: []Content: 

On 13 Jul 2010, at 00:59, Titus von der Malsburg wrote:


I'd add that it would feel more natural to me to write:

  try:
    print('%r page is %d bytes' % (url, len(future.result())))
- except FutureError:
-   print('%r generated an exception: %s' % (url, future.exception()))
+ except FutureError as e:
+   print('%r generated an exception: %s' % (url, e))

Cheers,
Brian



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Titus von der Malsburg wrote:

I had to read the pep fairly carefully before I noticed
this too, so perhaps it could be made more prominent.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] PEP 3148 ready for pronouncement [ACCEPTED]
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jul 12, 2010 at 02:13, Jesse Noller <jnoller at gmail.com> wrote:


Brian, did you ever get your keys to Martin or Georg? If not please do (or
send it to me) and let us know what your bugs.python.org username is to get
Developer privileges.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100723/5e6f4ead/attachment.html>

