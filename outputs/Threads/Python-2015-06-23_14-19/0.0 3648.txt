
============================================================================
Subject: [Python-Dev] Generally boared by installation (Re: Setting project
	home path the best way)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting project
	home path the best way)
----------------------------------------
Author: Christian Tisme
Attributes: []Content: 
Hi guys,

I am bored of installing things. 
Bored of things that happen to not work for some minor reasons. 
Reasons that are not immediately obvious. 
Things that don't work because some special case was not handled. 
Things that compile for half an hour and then complain that something is not as expected. 
May it be a compiler, a library, a command like pip or easy-install, a system like macports or homebrew,
virtualenv, whatsoever. 

These things are all great if they work. 

When they do not work, the user is in some real trouble. And he reads hundreds
Of blogs and sites and emails, which all answer a bit of slightly related questions, but all in all - 

This is not how Python should work !!

I am really bored and exhausted and annoyed by those packages which
Pretend to make my life eadier, but they don't really. 

Something is really missing. I want something that is easy to use in all
cases, also if it fails. 

Son't get me wrong, I like things like pip or virtualenv or homebrew. 
I just think they have to be rewritten completely. They have the wrong assumption that things work!

The opposite should be the truth: by default, things go wrong. Correctness is very fragile. 

I am thinking of a better concept that is harder to break. I thin to design a setup tool that is much more checking itself and does not trust in any assumption. 

Scenario:
After hours and hours, I find how to modify setup.py to function almost correctly for PySide. 

This was ridiculously hard to do! Settings for certain directories, included and stuff are not checked when they could be, but after compiling a lot of things!

After a lot of tries and headaches, I find out that virtualenv barfs on a simple
link like ./.Python, the executable, when switching from stock Python to a different (homebrew) version!!

This was obviously never tested well, so it frustrates me quite a lot.  

I could fill a huge list full of complaints like that if I had time. But I don't. 

Instead, I think installation scripts are generally still wrong by concept today and need to
be written in a different way. 

I would like to start a task force and some sprints about improving this
situation. 
My goal is some unbreakable system of building blocks that are self-contained with no dependencies, that have a defined interface to talk to, and that know themselves completely by introspection. 

They should not work because they happen to work around all known defects, but by design and control. 

Whoever is interested to work with me on this is hereby honestly welcomed!

Cheers - chris

Sent from my Ei4Steve

On Nov 15, 2012, at 10:17, Kristj?n Valur J?nsson <kristjan at ccpgames.com> wrote:


