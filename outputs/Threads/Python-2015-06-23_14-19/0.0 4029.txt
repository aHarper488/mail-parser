
============================================================================
Subject: [Python-Dev] peps: Updates in response to Barry Warsaw's
	feedback
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] peps: Updates in response to Barry Warsaw's
	feedback
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun,  6 Jan 2013 08:25:44 +0100 (CET)
nick.coghlan <python-checkins at python.org> wrote:

Since we are here, I would bikeshed a little and point out that
"initializing" and "initialiazed" are states, not phases. Either way,
the nomenclature should be consistent.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] peps: Updates in response to Barry Warsaw's
	feedback
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Jan 6, 2013 at 10:26 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

States and phases are generally the same thing (e.g. states of matter
vs phases of matter).

I've switched back and forth between state, stage and phase at various
times, though, so the PEP is currently internally inconsistent. If
there are any leftover "state" and "stage" references, it's just a
mistake since "phase" is the terminology I finally settled on.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia

