
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.2): zip() returns an
 iterator, make a list() of it; thanks to Martin from docs@
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): zip() returns an
 iterator, make a list() of it; thanks to Martin from docs@
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Sun, Aug 12, 2012 at 1:25 AM, sandro.tosi <python-checkins at python.org> wrote:


Is there a reason we don't run the doctests in the Doc/ folder's .rst
files as part of regrtest (e.g. via DocFileSuite), or is that
something we just haven't gotten around to doing?

--Chris



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): zip() returns an
 iterator, make a list() of it; thanks to Martin from docs@
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
Just now doctest-like code blocks in Doc/* are used for two different targets:
1. regular doctests
2. notation for documentation
While former can be tested the later will definitely fail (missing
variables, functions, files etc.)

Also docs contains mixed notation, when, say, function declared as
regular code block than called from doctest (see functools.lru_cache
examples). Doctest obviously failed because it cannot find function.

For now if you will try to run doctest on Doc/**.rst you will get *a
lot* of failures.

I doubt if we will convert all docs to pass doctests, at least quickly.
Also making docs doctest-safe sometimes requires less clean and worse
readable notation.

On Sun, Aug 12, 2012 at 3:40 PM, Chris Jerdonek
<chris.jerdonek at gmail.com> wrote:



-- 
Thanks,
Andrew Svetlov



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): zip() returns an
 iterator, make a list() of it; thanks to Martin from docs@
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Aug 12, 2012 at 11:00 PM, Andrew Svetlov
<andrew.svetlov at gmail.com> wrote:

About the only thing that could work in a reasonable way is a doctest
mode for 3.4 where it could be told to ignore files unless they
contained some kind of "doctest-safe" marker.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): zip() returns an
 iterator, make a list() of it; thanks to Martin from docs@
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
Sounds good.

On Sun, Aug 12, 2012 at 4:37 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:



-- 
Thanks,
Andrew Svetlov



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): zip() returns an
 iterator, make a list() of it; thanks to Martin from docs@
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Sun, Aug 12, 2012 at 6:37 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

I created an issue for this here:

http://bugs.python.org/issue15629

--Chris

