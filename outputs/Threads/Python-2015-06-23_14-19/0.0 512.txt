
============================================================================
Subject: [Python-Dev] Taking over the Mercurial Migration
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Taking over the Mercurial Migration
----------------------------------------
Author: Dan Buc
Attributes: []Content: 
/me throws hat into ring.  I'm in the middle of migrating fairly
large chunks of an overgrown codebase from Subversion to Mercurial,
so I might actually have worthwhile input :)

-- 
~Dan

On Wed, Jun 30, 2010 at 10:41:51AM +0200, Georg Brandl wrote:
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 230 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100630/dd7287ca/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Taking over the Mercurial Migration
----------------------------------------
Author: =?ISO-8859-15?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 01.07.2010 02:01, schrieb Dan Buch:

To all the volunteers: an issue that apparently requires immediate
attention is that r21112 fails to convert. For details, see

http://mail.python.org/pipermail/python-committers/2010-June/000977.html

So somebody please reproduce the problem, propose a patch, and get in
contact with Dirkjan to integrate it.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Taking over the Mercurial Migration
----------------------------------------
Author: Dan Buc
Attributes: []Content: 
On Thu, Jul 01, 2010 at 06:37:22AM +0200, "Martin v. L?wis" wrote:

Does anybody know if there's already an issue tracking the failure
so that volunteers can better reproduce the issue?  Is a full checkout
of /projects/python via hgsubversion all that's required, perhaps?

-- 
~Dan

-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 230 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100701/dd8a1c88/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Taking over the Mercurial Migration
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Thu, Jul 1, 2010 at 14:09, Dan Buch <daniel.buch at gmail.com> wrote:

I work from a full svnsync of the projects repo (including python). I
can probably put up a tarball somewhere.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Taking over the Mercurial Migration
----------------------------------------
Author: Dan Buc
Attributes: []Content: 
Dirkjan,

I assume having such a tarball available would be a good thing, but what
do I know!? :)

Are your steps for reproducing the referenced problem with
cvs2svn-generated revs available on an issue, wiki page or PEP?

-- 
~Dan

On Thu, Jul 01, 2010 at 02:19:06PM +0200, Dirkjan Ochtman wrote:
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 230 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100701/9413f7fc/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Taking over the Mercurial Migration
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Thu, Jul 1, 2010 at 15:23, Dan Buch <daniel.buch at gmail.com> wrote:

I'm putting one up. I'll email you the address privately in order to
preserve some bandwidth. Anyone else who wants a copy: just email me.


Install hgsubversion (tip) and get the pymigr repo, then just run this:

$ hg clone -A pymigr/author-map --branchmap pymigr/branchmap
file:///path/to/svn-repo/python

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Taking over the Mercurial Migration
----------------------------------------
Author: Dan Buc
Attributes: []Content: 
Excellent!  Much thanks, Dirkjan.

-- 
~Dan

On Thu, Jul 01, 2010 at 04:14:16PM +0200, Dirkjan Ochtman wrote:
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 230 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100701/5cd97fd5/attachment.pgp>

