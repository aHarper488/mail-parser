
============================================================================
Subject: [Python-Dev] Low-Level Encoding Behavior on Python 3
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Low-Level Encoding Behavior on Python 3
----------------------------------------
Author: Armin Ronache
Attributes: []Content: 
Hi everybody,

We (me and Carl Meyer) did some experimentation with encoding behavior 
on Python 3.  Carl did some hacking on getting virtualenv running on 
Python 3 and it turned out that his version of virtualenv did not work 
on Python 3 on my server either.  So none of the virtulenv installations 
did though they all seemed to work for some people.

Looking closer the problem is that virtualenv was assuming that 
'open(filename).read()' works.  However on my particular system the 
default encoding in Python 3 for files was 'ASCII'.  That encoding was 
picked up because of three things: a) Python 3's default encoding for 
opening files is picked up from the system locale, b) the ssh server 
accepts the client's encoding for everything (including filenames) and 
c) the OS X default installation for many people does not initialize 
locales properly which forces the server to fall back to 'POSIX' which 
then by applications (including Python) is picked up as ASCII.

Now this showcases a couple of problems on different levels:

-   developers assume that the default for encodings is UTF-8 because
     that is the encoding on their local machine.  Now falling back to
     the platform dependent encoding is documented but does not make a
     lot of sense.  The limiting platform is probably Windows which
     historically has problems with UTF-8 in the notepad editor.

     As a compromise I recommend UTF-8 for POSIX and UTF-8-sig for
     Windows as the Windows editor feels happier with this encoding.
     As the latter reads every file of the former that should not cause
     that many problems in practice

-   Seeing that SSH happily overrides the filesystem encoding I would
     like to forward this issue to some of the linux maintainers.  Having
     the SSH client override your filesystem encoding sounds like a
     terrible decision.  Apparently Python guesses the filesystem
     encoding from LC_CTYPES which however is overriden by connecting
     SSH clients.  Seeing how ubuntu and a bunch of other distributions
     are using Gnome which uses UTF-8 for filesystems as somewhat
     established default I would argue that Python should just assume
     UTF-8 as default encoding on a Linux environment.

-   Inform Apple about the fact that some Snow Leopard machines are
     by default setting the LC_CTYPES (and all other locales) variables
     to something that is not even a valid locale.  I am not yet sure why
     it does not happen on all machines, but it happens on more than one
     at PyCon alone.  On top of that I know that issue because it broke
     the Python "Babel" package for a while which is why I added a work-
     around for that particular problem.

     I will either way file a bug report at Apple for what the SSH client
     is doing on mixed local environments.


Are we missing anything?  Any suggestions?


Regards,
Armin



----------------------------------------
Subject:
[Python-Dev] Low-Level Encoding Behavior on Python 3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hi,


I may be mistaken, but you seem to conflate two things: encoding of
file names, and encoding of file contents. I guess that virtualenv
chokes on the file contents, but most of your argument seems related to
encoding of file names (aka "filesystem encoding").

In any case, it would be best for virtualenv to specify the encoding
explicitly. If it doesn't know what that should be, perhaps there's a
deeper problem ;)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Low-Level Encoding Behavior on Python 3
----------------------------------------
Author: Armin Ronache
Attributes: []Content: 
Hi,

On 3/16/11 3:48 AM, Antoine Pitrou wrote:
These are two pretty unrelated problems but both are problems 
nonetheless.  The filename encoding should not be guessed from the 
environment variables as those are from the connecting client.  The 
default encoding for file contents also should not be platform 
dependent.  It *will* lead to people thinking it works when in practice 
it will break if they move their code to a remote server and SSH into it 
and then trigger the code execution.

I argue that the first is just wrong (filename encoding guessing) and 
the latter is dangerous (file content encoding being platform dependent).

virtualenv itself is already fixed and explicitly tells it to read with 
UTF-8 encoding.


Regards,
Armin



----------------------------------------
Subject:
[Python-Dev] Low-Level Encoding Behavior on Python 3
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Armin Ronacher, 16.03.2011 16:57:

Antoine was arguing that it's not the fault of CPython that virtualenv 
expects it to correctly guess the encoding of a file it wants to read. It 
tries an educated guess based on the current environment setup, and if 
that's not correctly configured, it's the user's fault. As you indicated 
yourself, it does work most of the time. That's all you should expect from 
a default.



That's the right way to deal with encoded file content.

Stefan


