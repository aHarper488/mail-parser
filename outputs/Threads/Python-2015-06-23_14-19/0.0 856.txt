
============================================================================
Subject: [Python-Dev] Dead modules
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Dead modules
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 23 May 2010 12:43:57 +0200
Dirkjan Ochtman <dirkjan at ochtman.nl> wrote:

I disagree that a stdlib module is a dead module. It is perfectly
possible to augment the API with new functionality without breaking
compatibility. You can also deprecate old APIs if you want.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Dead modules
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Sun, May 23, 2010 at 12:51, Antoine Pitrou <solipsis at pitrou.net> wrote:

Right, it wasn't intended as that harsh... but it does come with a
rather impressive set of constraints in terms of what you can do with
the API.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Dead modules
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
(Sending again - I didn't mean to drop python-dev from the cc list when 
I originally sent this via the gmail web interface)

On Sun, May 23, 2010 at 9:00 PM, Dirkjan Ochtman <dirkjan at ochtman.nl 
<mailto:dirkjan at ochtman.nl>> wrote:

    Right, it wasn't intended as that harsh... but it does come with a
    rather impressive set of constraints in terms of what you can do with
    the API.

True, but in some cases (especially low level infrastructure), it is 
worth accepting those constraints in order to achieve other aims (such 
as standardisation of techniques). Things like itertools, collections, 
functools, unittest owe their existence largely to the choice of gains 
in standardisation over flexibility of API updates.
Besides, popular PyPI modules don't have that much more freedom than the 
stdlib when it comes to API changes. The only real difference is that 
the 18-24 month release cycle for the stdlib is a lot *slower* than that 
of many PyPI packages, so feedback on any changes we make is 
correspondingly delayed. Hence the existence of projects like distutils2 
and unittest2 to enable that faster feedback cycle to inform the updates 
passed back into the more slowly evolving stdlib modules, as well as the 
desire to copy prior art wherever it makes sense to do so (whether that 
is other languages, existing PyPI modules or the internal code bases of 
large corporate contributors).

Cheers,
Nick.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100526/49e5e3a6/attachment.html>

