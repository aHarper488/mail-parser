
============================================================================
Subject: [Python-Dev] The fate of Distutils in Python 2.7
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] The fate of Distutils in Python 2.7
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Hello,

This is a follow-up of the Pycon summit + sprints on packaging.

This is what we have planned to do:

1. refactor distutils in a new standalone version called distutils2
[this is done already and we are actively working in the code]
2. completely revert distutils in Lib/ and Doc/ so the code + doc is
the same than the current 2.6.x branch
3. leave the new sysconfig module, that is used by the Makefile and
the site module

The rest of the work will happen in distutils2 and we will try to
release a version asap for Python 2.x and 3.x (2.4 to 3.2), and the
goal
is to put it back in the stdlib in Python 3.3

Distutils in Python will be feature-frozen and I will only do bug
fixes there.  All feature requests will be redirected to Distutils2.

I think the easiest way to manage this for me and for the feedback of
the community is to add in bugs.python.org a "Distutils2" component,
so I can
start to reorganize the issues in there and reassign new issues to
Distutils2 when it applies.

Regards
Tarek

-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] The fate of Distutils in Python 2.7
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Fri, Feb 26, 2010 at 13:44, Tarek Ziad? <ziade.tarek at gmail.com> wrote:

I assume you want the Distutils2 component to auto-assign to you like
Distutils currently does? If so I can add the component for you if people
don't object to the new component.

-Brett



-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100226/491c2918/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] The fate of Distutils in Python 2.7
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
On Fri, Feb 26, 2010 at 11:13 PM, Brett Cannon <brett at python.org> wrote:
[..]

Sounds good -- Thanks



----------------------------------------
Subject:
[Python-Dev] The fate of Distutils in Python 2.7
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Fri, Feb 26, 2010 at 14:15, Tarek Ziad? <ziade.tarek at gmail.com> wrote:


Done.

-Brett
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100228/07e761ed/attachment.html>



----------------------------------------
Subject:
[Python-Dev] The fate of Distutils in Python 2.7
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Just FYI : I am post-poning the revert of Distutils to 2.6.x right
after 2.7a4 has been tagged to avoid any problems (this is in 3 days)

The revert is ready but 3 days is not long enough to make sure
everything is going smooth.

On Fri, Feb 26, 2010 at 10:44 PM, Tarek Ziad? <ziade.tarek at gmail.com> wrote:



-- 
Tarek Ziad? | http://ziade.org

