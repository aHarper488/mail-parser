
============================================================================
Subject: [Python-Dev] Is it intentional that "sys.__debug__ = 1" is
	illegal in Python 2.7?
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
	illegal in Python 2.7?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/7/30 Barry Warsaw <barry at python.org>:

The reason behind this was to make __debug__ assignment consistent
with that of other reserved names. For example, x.None = 3 raised and
thus, so should x.__debug__ = 3.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
	illegal in Python 2.7?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Jul 30, 2010 at 1:26 PM, Barry Warsaw <barry at python.org> wrote:

Well it is a reserved name so those packages that were setting it
should have known that they were using undefined behavior that could
change at any time.


-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
	illegal in Python 2.7?
----------------------------------------
Author: Guilherme Pol
Attributes: []Content: 
2010/7/30 Barry Warsaw <barry at python.org>:

Doesn't the section
http://docs.python.org/reference/lexical_analysis.html#reserved-classes-of-identifiers
make this clear enough ?




-- 
-- Guilherme H. Polo Goncalves



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
	illegal in Python 2.7?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Jul 30, 2010 at 1:53 PM, Barry Warsaw <barry at python.org> wrote:

No, since it is covered here:

http://docs.python.org/reference/lexical_analysis.html#reserved-classes-of-identifiers

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
	illegal in Python 2.7?
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On Sat, 31 Jul 2010 07:44:42 am Guido van Rossum wrote:
wrote:


I have a small concern about the wording of that, specifically this:

"System-defined names. These names are defined by the interpreter and 
its implementation (including the standard library); applications 
SHOULD NOT EXPECT TO DEFINE additional names using this convention. The 
set of names of this class defined by Python may be extended in future 
versions."  [emphasis added]

This implies to me that at some time in the future, Python may make it 
illegal to assign to any __*__ name apart from those in a list 
of "approved" methods. Is that the intention? I have always understood 
that if you create your own __*__ names, you risk clashing with a 
special method, but otherwise it is allowed, if disapproved off. I 
would not like to see it become forbidden.


-- 
Steven D'Aprano

