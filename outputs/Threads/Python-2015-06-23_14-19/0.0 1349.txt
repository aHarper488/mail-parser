
============================================================================
Subject: [Python-Dev] Releases for recent security vulnerability
Post Count: 21
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Gustavo Nare
Attributes: []Content: 
Hi all,

How come a description of how to exploit a security vulnerability
comes before a release for said vulnerability? I'm talking about this:
http://blog.python.org/2011/04/urllib-security-vulnerability-fixed.html

My understanding is that the whole point of asking people not to
report security vulnerability publicly was to allow time to release a
fix.

If developers haven't had enough time to release the fix, that's fine.
But I can't think of a sensible reason why it should be announced
first.

Cheers,

 - Gustavo.



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Fri, Apr 15, 2011 at 09:35:06AM +0100, Gustavo Narea wrote:

Yes, I agree with you. I am surprised that it made it to blog and just
catching more attention (via Responses/Retweets) than what it is
worth.

FWIW, if we analyze the technical details more carefully,
urllib/urllib2 as a library could have redirected to file:// url, but
it is library and not web-server and person who wrote the server could
catch the redirection and handle it at higher level too. This may
sound less drastic than what it appears in the post.

Anyways it was an issue and it is fixed.

-- 
Senthil

<calc> Knghtbrd: irc doesn't compile c code very well ;)



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Apr 15, 2011 3:46 AM, "Gustavo Narea" <me at gustavonarea.net> wrote:

To me, the fix *was* released. Sure, no fancy installers were generated yet,
but people who are susceptible to this issue 1) now know about it, and 2)
have a way to patch their system *if needed*.

If that's wrong, I apologize for writing the post too early. On top of that,
it seems I didn't get all of the details right either, so apologies on that
as well.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110415/dd411cf9/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Fri, Apr 15, 2011 at 8:30 AM, Brian Curtin <brian.curtin at gmail.com> wrote:

The code is open source: Anyone watching the commits/list know that
this issue was fixed. It's better to keep it in the public's eyes, so
they know *something was fixed and they should patch* than to rely on
people *not* watching these channels.

Assume the bad guys already knew about the exploit: We have to spread
the knowledge of the fix as far and as wide as we can so that people
even know there is an issue, and that it was fixed. This applies to
users and *vendors* as well.

A blog post is good communication to our users. I have to side with
Brian on this one.

jesse



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 15 Apr 2011 08:36:16 -0400
Jesse Noller <jnoller at gmail.com> wrote:

True. However, many open source projects take the habit of cutting a
release when a hole is discovered and fixed. It depends how seriously
they (and their users) take security. Of course, there are different
kinds of security issues, more or less severe. I don't know how severe
the above issue is.

Relying on a vendor distribution (such as a Linux distro, or
ActiveState) is hopefully enough to get these security updates in time
without patching anything by hand. I don't think many people compile
Python for production use, but many do use our Windows installers.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Fri, Apr 15, 2011 at 8:59 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Antoine,

I actually expect many companies build their own Python for production use;
relying on the system Python has long been considered a stability vulnerability
by many of us.  This is especially the case for large deployments,
where machines
are less likely to receive updates quickly.

I'd strongly recommend making sure releases are available for download quickly
in cases like this, even if (in any particular case) we think a vulnerability is
unlikely to affect many users.  Whenever we think something like that, we're
always wrong.


  -Fred

-- 
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"Give me the luxuries of life and I will willingly do without the necessities."
?? --Frank Lloyd Wright



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Fri, Apr 15, 2011 at 8:59 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Agreed; but all I'm defending is the post describing what, and how it
was fixed. Hiding it until we get around to eventually cutting a
release doesn't make the fix, or vulnerability go away. We need to
issue a release *quickly* - and we need to notify all of our consumers
quickly.

jesse



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Gustavo Nare
Attributes: []Content: 
Hello,

On 15/04/11 13:30, Brian Curtin wrote:

No, it wasn't. It was *committed* to the repository.


Well, that's a long shot. I doubt the people/organizations affected are
all aware. And I doubt they are all capable of patching their system or
getting a patched Python from a trusted party.

Three weeks after this security vulnerability was *publicly* reported on
bugs.python.org, and two days after it was semi-officially announced,
I'm still waiting for security updates for my Ubuntu and Debian systems!

I reckon if this had been handled differently (i.e., making new releases
and communicating it via the relevant channels [1]), we wouldn't have
the situation we have right now.

May I suggest that you adopt a policy for handling security issues like
Django's?
http://docs.djangoproject.com/en/1.3/internals/contributing/#reporting-security-issues

Cheers,

[1] For example,
<http://mail.python.org/mailman/listinfo/python-announce-list>,
<http://www.python.org/news/>, <http://www.python.org/news/security/>.

-- 
Gustavo Narea <xri://=Gustavo>.
| Tech blog: =Gustavo/(+blog)/tech  ~  About me: =Gustavo/about |




----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Apr 16, 2011 at 9:45 PM, Gustavo Narea <me at gustavonarea.net> wrote:

Nope, we would have a situation where the security team were still
attempting to coordinate with the release managers to cut new source
releases and new binary releases, and not even releasing the source
level patches that *will* allow many, many people to fix the problem
on their own.

I don't agree that such a situation would be better than the status
quo (i.e. where both the problem and *how to fix it yourself* are
public knowledge).

The *exact* patches for all affected versions of Python are readily
available by checking the changesets linked from
http://bugs.python.org/issue11662#msg132517


When the list of people potentially using the software is "anyone
running Linux or Mac OS X and an awful lot of people running Windows
or an embedded device", private pre-announcements simply aren't a
practical reality. Neither is "stopping all other development" when
most of the core development team aren't on the security at python.org
list and don't even know a security issue exists until it is announced
publicly. Take those two impractical steps out of the process, and
what you have *is* the python.org procedure for dealing with security
issues.

And when official python.org releases require coordination of
volunteers scattered around the planet, there is a harsh trade-off to
be made when it comes to deciding how long to wait before publishing
the information people need in order to fix the issue themselves.

Bumping the priority of the next round of python.org releases should
definitely be on the agenda, but the "rapid response" side of things
needs to come from the OS vendors with paid release engineers. Dealing
with security issues on behalf of their end users is one of the key
reasons they're getting paid for free software in the first place.

It may be worth asking the OS vendors whether or not they have
representatives that receive the security at python.org notifications,
and if not, why they haven't approached python-dev about receiving
such notifications.


Agreed that an announcement should be made on those locations, with a
list of links to the exact changesets for each affected version.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Sat, Apr 16, 2011 at 06:45, Gustavo Narea <me at gustavonarea.net> wrote:


Yep, and that's enough for me. If you have a vulnerable system, you can now
patch it with an accepted fix.




Hence why this blog exists and why this post was made...

And I doubt they are all capable of patching their system or

Maybe that's where the post fell short. Should I have added a section with
an example of how to apply the patch to an example system like 2.6?




I don't really think there's a "situation" here, and I fail to see how the
development blog isn't one of the relevant channels.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110416/bb0b3412/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 16 Apr 2011 21:32:48 -0500
Brian Curtin <brian.curtin at gmail.com> wrote:

If we want to make official announcements (like releases or security
warnings), I don't think the blog is appropriate. A separate
announcement channel (mailing-list or newsgroup) would be better, where
people can subscribe knowing they will only get a couple of e-mails a
year.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Sun, Apr 17, 2011 at 7:48 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Sounds like python-announce to me, with a matching entry on the front
of www.python.org.


  -Fred

-- 
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"Give me the luxuries of life and I will willingly do without the necessities."
?? --Frank Lloyd Wright



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Sun, Apr 17, 2011 at 7:48 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

And whose responsibility is it to email yet another mythical list? The
person posting the fix? The person who found and filed the CVE? The
release manager?

Brian *helped* us by raising awareness of the issue: At least now
there's a chance that one or more of the OS vendors *saw* that this
was an issue that was fixed.



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le dimanche 17 avril 2011 ? 09:30 -0400, Jesse Noller a ?crit :

Well, whose responsibility is it to make blog posts about security
issues? If you can answer this question then the other question
shouldn't be any more difficult to answer ;)

I don't think the people who may be interested in security announcements
want to monitor a generic development blog, since Python is far from the
only piece of software they rely on. /I/ certainly wouldn't want to.

Also, I think Gustavo's whole point is that if we don't have a
well-defined, deterministic procedure for security announcements and
releases, then it's just as though we didn't care about security at all.
Saying "look, we mentioned this one on our development blog" isn't
really reassuring for the target group of people.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 17 Apr 2011 08:30:33 -0400
Fred Drake <fdrake at acm.org> wrote:

Looking at python-announce, it can receive an arbitrary amount of
announcements from third-party projects, or even call for papers for
random conferences. It's probably easy to (dis)miss an important message
in all the churn.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Sun, Apr 17, 2011 at 9:42 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

I'm not arguing against us having a well defined, deterministic
procedure! We need one, for sure - I'm just defending Brian's actions
as perfectly rational and reasonable. Without his post, that CVE would
have been published, publicly available on other sites (CVE tracking
sites, and hence on the radar for people looking to exploit it), and
no one would be the wiser.

At least it got *some* attention this way. Is it the right thing to do
moving forward? Probably not - but do we have the people/person
willing to head up defining the policy and procedure, and do we have
the needed contacts in the OS vendors/3rd party distributors to notify
them rapidly in the case of fixing something like this?

A lag of several weeks from fixing a security issue to a source level
release from us that OS vendors can run with is too slow honestly.

jesse



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Jacob Kaplan-Mos
Attributes: []Content: 
On Sat, Apr 16, 2011 at 9:23 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Just to fill in a bit of missing detail about our process since the
doc doesn't perfectly describe what happens:

* Our pre-announce list is *really* short. It consists of release
managers for various distributions that distribute packaged versions
of Django -- Ubuntu, RedHat, and the like. Yes it's a bit of
bookkeeping, but we feel it's really important to our users: not
everyone installs the Django package *we* put out, so we think it's
important to coordinate security releases with downstream distributors
so that users get a fixed version of Django regardless of how they're
installing Django in the first place.

* We don't really halt all development. I don't know why that's in
there, except maybe that it pre-dates there being more than a
couple-three committers. The point is just that we treat the security
issue as our most important issue at the moment and fix it as quickly
as possible.

I don't really have a point here as it pertains to python-dev, but I
thought it's important to clarify what Django *actually* does if it's
being discussed as a model.

Jacob



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 17 Apr 2011 09:30:17 -0400, Jesse Noller <jnoller at gmail.com> wrote:

That fact that Brian helped publicize it is not really relevant to
Antoine's point.  The *obvious* answer to your question about whose
responsibility it is is: *the security team*.  Brian's blog post would
then have been much more like he envisioned it when he wrote it, a peek
inside the process, rather than appearing to be the primary announcement
as many seem to be perceiving it.

That's how distributions, at least, handle this.  There's a mailing list for
security related announcements on which only the "security officer" or
"security team" posts announcements, and security related announcements
*only*.  Then then the people responsible for security in any context
(a distribution, a security manager for a company, J Random User) can
subscribe to it and get *only* security announcements.  That allows them
to easily prioritize those announcements on receipt.

Python should have such a mailing list.

--
R. David Murray           http://www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Apr 18, 2011 at 12:03 AM, Jacob Kaplan-Moss <jacob at jacobian.org> wrote:

I'd rather have Red Hat and Canonical reps *on* the
security at python.org list rather than a separate pre-announce list.


That makes a lot more sense.


I'd personally like to see a couple of adjustments to
http://www.python.org/news/security/:

1. Identify a specific point-of-contact for the security list, for
security-related questions that aren't actually security issues (e.g.
how would a core developer go about asking to join the PSRT?)
2. Specifically state on the security page where vulnerabilities and
fixes will be announced and the information those announcements will
contain (as a reference for the PSRT when responding to an issue, and
also to inform others of the expected procedure)

The current page does a decent job of describing how to report a
security issue, but doesn't describe anything beyond that.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Gustavo Narea writes:

 > Well, that's a long shot. I doubt the people/organizations affected are
 > all aware.

That's really not Python's responsibility.  That's theirs.  Caveats:
Python should have a single place where security patches are announced
*first*, before developer blogs and the like.  Python's documentation
should make it clear at the most important entry points that the
appropriate place to report possible security issues is
security at python.org, not the tracker.  In particular, the tracker's
top page (the one you get from http://bugs.python.org/) should make
that clear.

Ironically, Brian's blog entry outlines a plausible security policy,
but a quick Google didn't find it elsewhere on site:python.org.  Oops,
a different search did find it -- under News/Security Advisories.

The tracker suggestion submitted as
http://psf.upfronthosting.co.za/roundup/meta/issue393.

 > And I doubt they are all capable of patching their system or
 > getting a patched Python from a trusted party.

Then they shouldn't be running Python in a security context, should
they?  Seriously, if they want somebody else to take care of their
security issues for them, they should pay for it.  As in almost areas
of life, security is at best worth what you pay for it.

 > Three weeks after this security vulnerability was *publicly* reported on
 > bugs.python.org,

Again, that's an issue with the reporter not knowing the policy, not
the policy itself, which is to report to security at python.org,
cf. Brian's post and the Security Advisory page.  The caveats above
apply, though.

 > and two days after it was semi-officially announced,
 > I'm still waiting for security updates for my Ubuntu and Debian systems!

Yeah, well, so much for depending on Ubuntu and Debian.  There are
reasons why people pay for RHEL.

 > I reckon if this had been handled differently (i.e., making new releases
 > and communicating it via the relevant channels [1]), we wouldn't have
 > the situation we have right now.

Of course not.  So what?  The question is "what is the best way to
reduce risks?"  *It is not possible to reduce all risks
simultaneously.*  What you are saying is "please keep things obscure
until I'm up to date."

It seems to be a consensus in the security community that most
security holes *are* sufficiently obscure that announcing the problem
before announcing a fix is likely to increase the likelihood of black
hats finding exploits and successfully executing them more than it
increases the likelihood that (3rd party) white hats will find and
contribute a fix.  So the question is whether to rely on obscurity
after the fix is devised.

Now, once there is a fix, there's always hysteresis in implementation,
as you note with respect to Ubuntu and Debian.  If you don't announce
the fix once available, you are increasing risk to conscientious,
patch-capable admins dramatically compared to the case where you give
them the patch.  I don't see why your Ubuntu/Debian systems should
take precedence over systems of those who prefer to rely on self-
built-and-maintained systems.  (In fact, since I generally fall into
the latter category, may I suggest it should be the other way around?
<wink />)

 > May I suggest that you adopt a policy for handling security issues like
 > Django's?
 > http://docs.djangoproject.com/en/1.3/internals/contributing/#reporting-security-issues

I'm -1 on that, except to the extent that it corresponds to existing
policy.  Eg,

    This will probably mean a new release of Django, but in some cases
    it may simply be patches against current releases.

seems to apply to the current case.  I really don't think the policy
Django claims is appropriate for Python for the following reasons,
among others:

    Halt all other development as long as is needed to develop a fix,
    including patches against the current and two previous releases.

is nonsense.  Perhaps what this means is that the "long-time, highly
trusted Django developers" on the security at django list *volunteer* to
put other Django work aside until the security hole is patched.  But
certainly the "hundreds of contributors around the world" won't stop
their work -- they won't know about the moratorium since they're not
on the security list.
In the case of Python, it's not even possible to stop commits without
closing the repo, as not all committers are on the security list.

Even for the security crew, in many cases of security problems, it's
something simple and readily fixed like a buffer overflow or an URL
traversal issue that just needs a simple filter on input before
executing a risky procedure.  So who does the fixing, reviewing, etc
should be decided on a case-by-case basis based on the problem itself
and available expertise IMO.

And this

    Pre-notify everyone we know to be running the affected version(s)
    of Django.

seems positively counter-productive if they're serious about
"everyone".  Surely the black hats are running the affected versions
of Django to test their exploits!  So they get to find out they're
running out of time to execute while the people running affected
versions remain vulnerable until the release.




----------------------------------------
Subject:
[Python-Dev] Releases for recent security vulnerability
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Nick Coghlan writes:

 > I'd personally like to see a couple of adjustments to
 > http://www.python.org/news/security/:

For another thing, it needs to be more discoverable.

For yet another thing, it has two ancient entries on it.  Surely there
are more than that?


