
============================================================================
Subject: [Python-Dev] next alpha sequence value from var pointing to
	array
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] next alpha sequence value from var pointing to
	array
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Jun 7, 2012 at 12:32 PM, jdmorgan <jdmorgan at unca.edu> wrote:

Close, but not quite the right place. This is a list for the design
and development *of* Python itself, rather than a list for using
Python.

For this kind of question, you want python-list at python.org.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

