
============================================================================
Subject: [Python-Dev] cpython (3.3): configparser: preserve section
	order when using `__setitem__` (issue #16820)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.3): configparser: preserve section
	order when using `__setitem__` (issue #16820)
----------------------------------------
Author: =?iso-8859-2?Q?=A3ukasz_Langa?
Attributes: []Content: 
Wiadomo?? napisana przez Antoine Pitrou <solipsis at pitrou.net> w dniu 1 sty 2013, o godz. 22:48:


Indeed, I missed updating NEWS, sorry. Will do that now for all fixes I did in the past days.

-- 
Best regards,
?ukasz Langa
Senior Systems Architecture Engineer

IT Infrastructure Department
Grupa Allegro Sp. z o.o.

http://lukasz.langa.pl/
+48 791 080 144


