
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #15102: find
 python.exe in OutDir, not SolutionDir.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #15102: find
 python.exe in OutDir, not SolutionDir.
----------------------------------------
Author: Jeremy Klot
Attributes: []Content: 

In order for this change to accurately reflect the OutDir in the x64
builds, all imports of x64.props need to be moved to be before the
pyproject.props import statements.

