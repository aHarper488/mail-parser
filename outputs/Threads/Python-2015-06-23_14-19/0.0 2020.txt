
============================================================================
Subject: [Python-Dev] rXXX links in the bug tracker after the migration
 to Mercurial
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] rXXX links in the bug tracker after the migration
 to Mercurial
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 04.03.2011 18:17, schrieb Georg Brandl:

Is it really necessary to have the square brackets? ISTM that
the syntax of the short or long hash is unambiguous enough.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] rXXX links in the bug tracker after the migration
 to Mercurial
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I searched the messages, and it turns out that primarily long numbers
would give false positives:

 Python 1.6a2 (#7, Apr 24 2000, 23:02:54)  [GCC pgcc-2.91.66 19990314
 minidom (as the proposed documentation in patch 101821 explains) does
 Closed as Duplicate; see bug 435026 for details.  It's an
 the test is extended to 2000000 objects on my machine
 IRIX rattler 6.5 10120734 IP32
 hash("DNSSEC") == 8704052292078464
 [New Thread 2305843009213881680 (LWP 23166)]

So I guess mandating square brackets is reasonable - alternatively,
mandating a fixed length could have worked as well, I guess.

Regards,
Martin

