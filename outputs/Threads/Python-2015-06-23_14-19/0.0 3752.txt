
============================================================================
Subject: [Python-Dev] ABI stability between bugfix versions?
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] ABI stability between bugfix versions?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

To fix the crash in http://bugs.python.org/issue13992, I have to change
the definition of the Py_TRASHCAN_SAFE_BEGIN and Py_TRASHCAN_SAFE_END
macros in a way that makes them stop rely on global variables.

Despite being undocumented, these two macros are used in a couple of
third-party extension modules (*) such as greenlet and py-judy. This
means that such an extension compiled for 2.7.3 wouldn't work with
2.7.4, and vice-versa. Is there something we can do to alleviate the
issue? Add a note in the release notes?

Regards

Antoine.


(*) http://code.google.com/codesearch#search&q=Py_TRASHCAN_SAFE_BEGIN


-- 
Software development and contracting: http://pro.pitrou.net





----------------------------------------
Subject:
[Python-Dev] ABI stability between bugfix versions?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 03.09.2012 12:11, schrieb Antoine Pitrou:

I think breaking existing extension modules in a bugfix release is
really not acceptable.

I also think that the breakage can be reduced by keeping the global
variable. There would then be two counters: the global used by
old modules, the per-thread counter used by recompiled modules.
I believe this would require separate versions of the helper functions
also.

In the worst case, this could cause the nesting to go twice as deep,
which is not a problem, since the nesting is only 50, anyway.

The question also is about the other direction: modules compiled
for 2.7.4 that are run on 2.7.3. I think this would be more problematic
in your patch, since the code would just access fields in the thread
state which aren't there. If the helper functions are called
differently, the extension would fail to load, which IMO is the better
reaction.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] ABI stability between bugfix versions?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 03 Sep 2012 22:37:31 +0200
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Indeed. That sounds workable, despite the added complexity.


Right, that's a better behaviour than crashing.

Thank you,

Antoine.

