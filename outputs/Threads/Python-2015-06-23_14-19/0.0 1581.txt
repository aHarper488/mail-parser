
============================================================================
Subject: [Python-Dev] Python Unit Tests
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python Unit Tests
----------------------------------------
Author: Wesley Mesquit
Attributes: []Content: 
Hi all,

I starting to explore python 3k core development environment. So, sorry in
advance for any mistakes, but I really don't know what is the best list to
post this, since it not a "use of python" issue, and probably is not a dev
issue, it is more like a "dev env" question.

I have ran the test suit, and got the messages below.

~/python_dev/python$ make testall

./python -Wd -E -bb  ./Lib/test/regrtest.py -uall -l
== CPython 3.2rc2+ (py3k:88376, Feb 7 2011, 18:31:28) [GCC 4.4.5]
==   Linux-2.6.35-24-generic-x86_64-with-debian-squeeze-sid little-endian
==   /home/wesley/python_dev/python/build/test_python_3387
Testing with flags: sys.flags(debug=0, division_warning=0, inspect=0,
interactive=0, optimize=0, dont_write_bytecode=0, no_user_site=0, no_site=0,
ignore_environment=1, verbose=0, bytes_warning=2, quiet=0)

[...]

[198/349] test_ossaudiodev
test_ossaudiodev skipped -- [Errno 2] No such file or directory: '/dev/dsp'

[...]

[200/349] test_parser
Expecting 's_push: parser stack overflow' in next line
s_push: parser stack overflow

[...]

[321/349] test_urllib2net
/home/wesley/python_dev/python/Lib/socket.py:333: ResourceWarning: unclosed
<socket.socket object, fd=8, family=2, type=2049, proto=6>
  self._sock = None
/home/wesley/python_dev/python/Lib/urllib/request.py:2134: ResourceWarning:
unclosed <socket.socket object, fd=7, family=2, type=2049, proto=6>
  sys.exc_info()[2])
/home/wesley/python_dev/python/Lib/urllib/request.py:2134: ResourceWarning:
unclosed <socket.socket object, fd=8, family=2, type=2049, proto=6>
  sys.exc_info()[2])
/home/wesley/python_dev/python/Lib/socket.py:333: ResourceWarning: unclosed
<socket.socket object, fd=8, family=2, type=1, proto=6>
  self._sock = None
/home/wesley/python_dev/python/Lib/socket.py:333: ResourceWarning: unclosed
<socket.socket object, fd=9, family=2, type=1, proto=6>
  self._sock = None
/home/wesley/python_dev/python/Lib/socket.py:333: ResourceWarning: unclosed
<socket.socket object, fd=9, family=2, type=2049, proto=6>
  self._sock = None
/home/wesley/python_dev/python/Lib/socket.py:333: ResourceWarning: unclosed
<socket.socket object, fd=7, family=2, type=2049, proto=6>
  self._sock = None
[323/349] test_urllibnet
/home/wesley/python_dev/python/Lib/socket.py:333: ResourceWarning: unclosed
<socket.socket object, fd=7, family=2, type=1, proto=6>
  self._sock = None


24 tests skipped:
    test_bz2 test_curses test_dbm_gnu test_dbm_ndbm test_gdb
    test_kqueue test_ossaudiodev test_readline test_smtpnet
    test_socketserver test_sqlite test_ssl test_startfile test_tcl
    test_timeout test_tk test_ttk_guionly test_ttk_textonly
    test_urllib2net test_urllibnet test_winreg test_winsound
    test_xmlrpc_net test_zipfile64
9 skips unexpected on linux2:
    test_bz2 test_dbm_gnu test_dbm_ndbm test_readline test_ssl
    test_tcl test_tk test_ttk_guionly test_ttk_textonly
sys:1: ResourceWarning: unclosed file <_io.TextIOWrapper name='/dev/null'
mode='a' encoding='UTF-8'>


But running each of them individually:

:~/python_dev/python$ ./python Lib/test/regrtest.py  test_ossaudiodev
[1/1] test_ossaudiodev
test_ossaudiodev skipped -- Use of the `audio' resource not enabled
1 test skipped:
    test_ossaudiodev
Those skips are all expected on linux2.

./python Lib/test/regrtest.py test_parser
[1/1] test_parser
Expecting 's_push: parser stack overflow' in next line
s_push: parser stack overflow
1 test OK.

./python Lib/test/regrtest.py test_urllib2net[1/1] test_urllib2net
test_urllib2net skipped -- Use of the `network' resource not enabled
1 test skipped:
    test_urllib2net
Those skips are all expected on linux2.

Is there any reason for the different results?

Regards,

Wesley

-- 
Wesley Mesquita
Computer Engineer
http://www.wesleymesquita.com
Mobile: +55 11 95249272
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110207/a93ef776/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Python Unit Tests
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Mon, 07 Feb 2011 20:38:28 -0200, Wesley Mesquita <wesleymesquita at gmail.com> wrote:

This is normal since you don't have a /dev/dsp.  That's what the
skip message means.


"Expecting" means the next message is expected.  Someone should
probably check to see if this test can (in Python3) be rewritten so that
message isn't generated, but for now it is Expected and completely
normal.


There are some ResourceWarnings we haven't cured yet (the ResourceWarning is
a fairly new innovation).  I'm not sure why they don't show up when
you run the tests individually.


These would be because you don't have the correct system/development
libraries installed for bz2, gnudbm, ndbm, readline, openssl,
tcl, and tk when you compiled your Python.  So, these skips are
actually expected if you don't have those libraries, but if you want
a complete development/test environment you should install the
necessary packages and recompile.

I imagine at least some of this is already covered in the dev guide
(I haven't made time to review it yet).  If any of it is unclear
to you, please provide feedback so we can improve the guide (which
is new).

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Python Unit Tests
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
Wesley Mesquita wrote:

Yes: you are not using the same options on the stand-alone
tests as you are on the suite run. Most importantly, you
are not enabling all resources (-uall).

-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Feb 08 2011)
________________________________________________________________________

::: Try our new mxODBC.Connect Python Database Interface for free ! ::::


   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/



----------------------------------------
Subject:
[Python-Dev] Python Unit Tests
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Feb 8, 2011 at 11:00 AM, R. David Murray <rdmurray at bitdance.com> wrote:

Almost certainly the missing "-uall" meant the relevant tests didn't
actually run the second time around.


I put together a list a while back of the minimal set of dev packages
needed to do a full Python build on Kubuntu:
http://www.boredomandlaziness.org/2010/01/kubuntu-dev-packages-to-build-python.html

The apt-get build dependencies command added as a comment to that post
should work on any apt-based Linux variant (although, at least on
Kubuntu, it brings down quite a lot of stuff you don't actually need
in order to build Python). Presumably there's something similar
available for other packaging systems (if not, the minimal package
list may still provide a useful starting point)

I don't believe anything that platform specific is in the dev guide,
though (it wasn't in the old README files, that's why I made my own
list for later reference).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

