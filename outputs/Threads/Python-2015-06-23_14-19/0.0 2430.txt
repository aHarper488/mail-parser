
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: pyexpat uses the new
	Unicode API
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: pyexpat uses the new
	Unicode API
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le 03/10/2011 11:10, Amaury Forgeot d'Arc a ?crit :

The Unicode API is supposed to only deliver ready strings. But all 
extensions written for Python 3.2 use the "legacy" API 
(PyUnicode_FromUnicode and PyUnicode_FromString(NULL, size)) and so no 
string is ready.

But *no*, you don't have to update your extension reading strings to add 
a call to PyUnicode_READY. You only have to call PyUnicode_READY if you 
use the new API (e.g. PyUnicode_READ_CHAR), so if you modify your code. 
Another extract of my commit (on pyexpat):

-    name = PyUnicode_AS_UNICODE(nameobj);
+    first_char = PyUnicode_READ_CHAR(nameobj, 0);

Victor

