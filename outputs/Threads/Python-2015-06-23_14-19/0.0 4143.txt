
============================================================================
Subject: [Python-Dev] cpython: Add modeling file for Coverity Scan.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Add modeling file for Coverity Scan.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 23 Jul 2013 01:31:24 +0200 (CEST)
christian.heimes <python-checkins at python.org> wrote:

Can't you write "typedef ssize_t Py_ssize_t" instead?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython: Add modeling file for Coverity Scan.
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 23.07.2013 08:27, schrieb Antoine Pitrou:

No, but it really doesn't matter. Coverity just needs a similar type for
modeling.

"modeling_file.c", line 23: error #20:
          identifier "ssize_t" is undefined
  typedef ssize_t Py_ssize_t;
          ^

1 error detected in the compilation of "modeling_file.c".
ERROR: cov-translate returned with code 2

