
============================================================================
Subject: [Python-Dev] French sprint this week-end
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] French sprint this week-end
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

I organize an online sprint on CPython this week-end with french 
developers. At least six developers will participe, some of them don't 
know C, most know Python.

Do you know simple task to start contributing to Python? Something 
useful and not boring if possible :-) There is the "easy" tag on the bug 
tracker, but many issues have a long history, already have a patch, etc. 
Do know other generic task like improving code coverage or support of 
some rare platforms?

Eric Araujo, Antoine Pitrou and Charles Fran?ois Natali should help me, 
so I'm not alone to organize the sprint.

Don't watch the buildbot until Monday. You can expect more activity on 
our bug tracker (and maybe on the #python-dev channel) ;-)

--

If you speak french, join #python-dev-fr IRC channel (on Freenode) and 
see the wiki page http://wiki.python.org/moin/SprintFranceDec2011

Victor



----------------------------------------
Subject:
[Python-Dev] French sprint this week-end
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Victor Stinner <victor.stinner at haypocalc.com> wrote:

On some buildbots compiler warnings are starting to accumulate. Installing
a recent version of gcc and fixing those might be a good task. If the
participants are new to buildbot, it might even be interesting for them. :)


Stefan Krah





----------------------------------------
Subject:
[Python-Dev] French sprint this week-end
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Fri, Dec 16, 2011 at 11:00, Stefan Krah <stefan at bytereef.org> wrote:

Do we have buildbots that build Python with Clang instead of GCC? The
reason I'm asking is that Clang's diagnostics are usually better, and
fixing all its warnings could nicely complement fixing GCC's qualms.

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111216/982f2aa8/attachment.html>



----------------------------------------
Subject:
[Python-Dev] French sprint this week-end
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Fri, Dec 16, 2011 at 10:17, Eli Bendersky <eliben at gmail.com> wrote:

The box running my buildslave has clang installed, so someone with
access to the buildmaster could probably set that up without too much
trouble.

Cheers,

Dirkjan

