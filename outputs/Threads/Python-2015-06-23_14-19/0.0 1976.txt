
============================================================================
Subject: [Python-Dev] PEP 380 acceptance (was Re: EuroPython Language
 Summit report)
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 380 acceptance (was Re: EuroPython Language
 Summit report)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Jun 26, 2011 at 1:16 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

I hit a snag with this. The real tests of the PEP 380 functionality
aren't currently part of the patch - they're a big set of "golden
output" tests in the zipfile hosted on Greg's site. Those need to be
refactored into proper unittest or doctest based additions to the test
suite and incorporated into the patch before I could commit this with
a clear conscience. That's not going to be as quick as I first
thought.

Renaud's patch mostly applies cleanly at the moment - the only change
is that the "#endif" for the Py_LIMITED_API check needs to be moved in
pyerrors.h so it also covers the new StopIteration struct definition.

Regards,
Nick.

[1] http://www.cosc.canterbury.ac.nz/greg.ewing/python/yield-from/yield_from.html

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 380 acceptance (was Re: EuroPython Language
 Summit report)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 1:09 AM, renaud <rndblnch at gmail.com> wrote:

It would be good if you could take a look at Greg's original test
suite, consider ways of bringing it into the main regression tests and
then update the patch queue on bitbucket accordingly.

My preference is for something unittest based, essentially taking the
"golden output" comparisons and turning them into appropriate
self.assert* invocations.

Given the number of tests Greg has, it will probably make more sense
to do it as a new test subdirectory rather than as a single test file
(although that depends on how many tests are in each file - if there
are only a few, or if they overlap a lot, then having them as separate
test cases within a single file may be a better choice).


Yep, that does help.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

