
============================================================================
Subject: [Python-Dev] urllib2/urllib incompatibility?
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] urllib2/urllib incompatibility?
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 

I encountered what seems like an incompatibility between urllib and urllib2 in
the way they handle file:// URLs, is this a bug? I had a look on the bug tracker
and via Google to see if there were prior reports, but perhaps my search-fu is
deficient. Here's a console session to illustrate:

vinay at eta-karmic:/tmp$ echo Hello, world! >hello.txt
vinay at eta-karmic:/tmp$ cat hello.txt 
Hello, world!
vinay at eta-karmic:/tmp$ python
Python 2.6.4 (r264:75706, Dec  7 2009, 18:45:15) 
[GCC 4.4.1] on linux2
Type "help", "copyright", "credits" or "license" for more information.
'Hello, world!\n'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/usr/lib/python2.6/urllib2.py", line 124, in urlopen
    return _opener.open(url, data, timeout)
  File "/usr/lib/python2.6/urllib2.py", line 389, in open
    response = self._open(req, data)
  File "/usr/lib/python2.6/urllib2.py", line 407, in _open
    '_open', req)
  File "/usr/lib/python2.6/urllib2.py", line 367, in _call_chain
    result = func(*args)
  File "/usr/lib/python2.6/urllib2.py", line 1240, in file_open
    return self.parent.open(req)
  File "/usr/lib/python2.6/urllib2.py", line 389, in open
    response = self._open(req, data)
  File "/usr/lib/python2.6/urllib2.py", line 407, in _open
    '_open', req)
  File "/usr/lib/python2.6/urllib2.py", line 367, in _call_chain
    result = func(*args)
  File "/usr/lib/python2.6/urllib2.py", line 1287, in ftp_open
    raise URLError('ftp error: no host given')
urllib2.URLError: <urlopen error ftp error: no host given>

Anyone seen this before? Should I file an issue on the tracker? If I've missed
something obvious, sorry for the noise, but please do tell!

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] urllib2/urllib incompatibility?
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Mon, May 24, 2010 at 08:49:56AM +0000, Vinay Sajip wrote:


The actual (and Valid) file:// url in your case is 'file:///tmp/hello.txt'
And this was fine and consistent.



The extra '/' is making it in invalid url in urllib2, I think that be
tracked as bug at least to show a consistent behaviour.  The local
file open methods of urllib2 and urllib are different.

-- 
Senthil

You may my glories and my state dispose,
But not my griefs; still am I king of those.
		-- William Shakespeare, "Richard II"



----------------------------------------
Subject:
[Python-Dev] urllib2/urllib incompatibility?
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Senthil Kumaran <orsenthil <at> gmail.com> writes:


Yes, it seems to be a bug in urllib which permits an invalid URL to pass. I came
across it by accident, I normally use urls like file://localhost/tmp/hello.txt
so I don't trip over the /// hurdles :-)

I'll raise an issue on the tracker.

Regards,

Vinay Sajip



