
============================================================================
Subject: [Python-Dev] test_ctypes failure on AIX 5.3 using python-2.6.2 and
	libffi-3.0.9
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] test_ctypes failure on AIX 5.3 using python-2.6.2 and
	libffi-3.0.9
----------------------------------------
Author: swamy sangames
Attributes: []Content: 
Hi All,

I built the python-2.6.2 with the latest libffi-3.0.9 in AIX 5.3 using xlc
compiler.
When i try to run the ctypes test cases, two failures are seen in
test_bitfields.

*test_ints (ctypes.test.test_bitfields.C_Test) ... FAIL
test_shorts (ctypes.test.test_bitfields.C_Test) ... FAIL*

I have attached the full test case result.

If i change the type c_int and c_short to c_unit and c_ushort of class
"BITS(Structure)" in file
test_bitfields.py then no failures are seen.

Has anyone faced the similar issue or any help is appreciated.


Thanks,
Sangamesh
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0003.htm>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: ctype-testcases
Type: application/octet-stream
Size: 22996 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0003.obj>



----------------------------------------
Subject:
[Python-Dev] test_ctypes failure on AIX 5.3 using python-2.6.2 and
	libffi-3.0.9
----------------------------------------
Author: swamy sangames
Attributes: []Content: 
Hi All,

I built the python-2.6.2 with the latest libffi-3.0.9 in AIX 5.3 using xlc
compiler.
When i try to run the ctypes test cases, two failures are seen in
test_bitfields.

*test_ints (ctypes.test.test_bitfields.C_Test) ... FAIL
test_shorts (ctypes.test.test_bitfields.C_Test) ... FAIL*

I have attached the full test case result.

If i change the type c_int and c_short to c_unit and c_ushort of class
"BITS(Structure)" in file
test_bitfields.py then no failures are seen.

Has anyone faced the similar issue or any help is appreciated.


Thanks,
Sangamesh
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0004.htm>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: ctype-testcases
Type: application/octet-stream
Size: 22996 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0004.obj>



----------------------------------------
Subject:
[Python-Dev] test_ctypes failure on AIX 5.3 using python-2.6.2 and
	libffi-3.0.9
----------------------------------------
Author: swamy sangames
Attributes: []Content: 
Hi All,

I built the python-2.6.2 with the latest libffi-3.0.9 in AIX 5.3 using xlc
compiler.
When i try to run the ctypes test cases, two failures are seen in
test_bitfields.

*test_ints (ctypes.test.test_bitfields.C_Test) ... FAIL
test_shorts (ctypes.test.test_bitfields.C_Test) ... FAIL*

I have attached the full test case result.

If i change the type c_int and c_short to c_unit and c_ushort of class
"BITS(Structure)" in file
test_bitfields.py then no failures are seen.

Has anyone faced the similar issue or any help is appreciated.


Thanks,
Sangamesh
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0005.htm>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: ctype-testcases
Type: application/octet-stream
Size: 22996 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0005.obj>



----------------------------------------
Subject:
[Python-Dev] test_ctypes failure on AIX 5.3 using python-2.6.2 and
	libffi-3.0.9
----------------------------------------
Author: swamy sangames
Attributes: []Content: 
Hi All,

I built the python-2.6.2 with the latest libffi-3.0.9 in AIX 5.3 using xlc
compiler.
When i try to run the ctypes test cases, two failures are seen in
test_bitfields.

*test_ints (ctypes.test.test_bitfields.C_Test) ... FAIL
test_shorts (ctypes.test.test_bitfields.C_Test) ... FAIL*

I have attached the full test case result.

If i change the type c_int and c_short to c_unit and c_ushort of class
"BITS(Structure)" in file
test_bitfields.py then no failures are seen.

Has anyone faced the similar issue or any help is appreciated.


Thanks,
Sangamesh
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0006.htm>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: ctype-testcases
Type: application/octet-stream
Size: 22996 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0006.obj>



----------------------------------------
Subject:
[Python-Dev] test_ctypes failure on AIX 5.3 using python-2.6.2 and
	libffi-3.0.9
----------------------------------------
Author: swamy sangames
Attributes: []Content: 
Hi All,

I built the python-2.6.2 with the latest libffi-3.0.9 in AIX 5.3 using xlc
compiler.
When i try to run the ctypes test cases, two failures are seen in
test_bitfields.

*test_ints (ctypes.test.test_bitfields.C_Test) ... FAIL
test_shorts (ctypes.test.test_bitfields.C_Test) ... FAIL*

I have attached the full test case result.

If i change the type c_int and c_short to c_unit and c_ushort of class
"BITS(Structure)" in file
test_bitfields.py then no failures are seen.

Has anyone faced the similar issue or any help is appreciated.


Thanks,
Sangamesh
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0007.htm>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: ctype-testcases
Type: application/octet-stream
Size: 22996 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100107/14588c00/attachment-0007.obj>

