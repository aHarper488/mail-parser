
============================================================================
Subject: [Python-Dev] Do we need __length_hint__ at all? (Was PEP 0424:
	A method for exposing a length hint)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Do we need __length_hint__ at all? (Was PEP 0424:
	A method for exposing a length hint)
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jul 16, 2012, at 1:37 AM, Mark Shannon wrote:


Unless pre-sized by with a length prediction, a growing list
periodically needs to call realloc() which can move all the
data to a new location in memory.    Pre-sizing avoids that entirely.


A great deal of thought and care went into the current design.
It has already been "tweaked".


Raymond


P.S.  The dictionary code also uses presizing for copies, updates, set conversion, etc.
It is a perfectly reasonable technique to pre-allocate the correct size container
when the ultimate length is knowable in advance.



