
============================================================================
Subject: [Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
Post Count: 16
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Daniel Holt
Attributes: []Content: 
Only if it is not an OrderedDict




----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On 17.08.2012 21:27, Guido van Rossum wrote:

Sounds good. For best backwards compatibility, I'd restrict the sorting
to the exact dict type, since people may be using non-dict mappings
which already have a different stable order.


I think this cannot be done, in particular not for 2.6 and 3.1 - it's
not a security fix (*).

Strictly speaking, it isn't even a bug fix, since it doesn't restore
the original behavior that some people (like your test case) relied
on. In particular, if somebody has fixed PYTHONHASHSEED to get a stable
order, this change would break such installations. By that policy, it
could only go into 3.4.

OTOH, if it also checked whether there is randomized hashing, and sort
only in that case, I think it should be backwards compatible in all
interesting cases.

Regards,
Martin

(*) I guess some may claim that the current implementation leaks
some bits of the hash seed, since you can learn the seed from
the parameter order, so sorting would make it more secure. However,
I would disagree that this constitutes a feasible threat.



----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Thanks, I filed http://bugs.python.org/issue15719 to track this.

On Fri, Aug 17, 2012 at 12:50 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Sat, Aug 18, 2012 at 5:27 AM, Guido van Rossum <guido at python.org> wrote:

Hmm. ISTM this is putting priority on the unit test above the
functionality of actual usage. Although on the other hand, sorting
parameters on a URL is nothing compared to the cost of network
traffic, so it's unlikely to be significant.

Chris Angelico



----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Joao S. O. Buen
Attributes: []Content: 
On 17 August 2012 18:52, Chris Angelico <rosuav at gmail.com> wrote:

I don't think this behavior is only desirable to unit tests: having
URL's been formed in predictable way  a good thing in any way one
thinks about it.

   js
  -><-




----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Joao S. O. Bueno writes:

 > I don't think this behavior is only desirable to unit tests: having
 > URL's been formed in predictable way  a good thing in any way one
 > thinks about it.

Especially if you're a hacker.  One more thing you may be able to use
against careless sites that don't expect the unexpected to occur in
URLs.

I'm not saying this is a bad thing, but we should remember that the
whole point of PYTHONHASHSEED is that regularities can be exploited
for devious and malicious purposes, and reducing regularity makes many
attacks more difficult.  "*Any* way one thinks about it" is far too
strong a claim.

Steve







----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 18 Aug 2012 14:23:13 +0900
"Stephen J. Turnbull" <stephen at xemacs.org> wrote:

That's unsubstantiated. Give an example of how sorted URLs compromise
security.

Regards

Antoine.


-- 
Software development and contracting: http://pro.pitrou.net





----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Joao S. O. Buen
Attributes: []Content: 
On 18 August 2012 02:23, Stephen J. Turnbull <stephen at xemacs.org> wrote:

Ageeded that "any way one thinks about it" is far too strong a claim -
but I still hold to the point. Maybe "most ways one thinks about it"
:-)  .





----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 17.08.2012 21:27, schrieb Guido van Rossum:

I vote -0. The issue can also be addressed with a small and simple
helper function that wraps urlparse and compares the query parameter. Or
you cann urlencode() with `sorted(qs.items)` instead of `qs` in the
application.

The order of query string parameter is actually important for some
applications, for example Zope, colander+deform and other form
frameworks use the parameter order to group parameters.

Therefore I propose that the query string is only sorted when the query
is exactly a dict and not some subclass or class that has an items() method.

    if type(query) is dict:
        query = sorted(query.items())
    else:
        query = query.items()

Christian




----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Sat, Aug 18, 2012 at 6:28 AM, Christian Heimes <lists at cheimes.de> wrote:

Hm. That's actually a good point.


That's already in the bug I filed. :-) I also added that the sort may
fail if the keys mix e.g. bytes and str (or int and str, for that
matter).

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: MRA
Attributes: []Content: 
On 18/08/2012 18:34, Guido van Rossum wrote:
One possible way around that is to add the class names, perhaps only if
sorting raises an exception:

def make_key(pair):
     return type(pair[0]).__name__, type(pair[1]).__name__, pair

if type(query) is dict:
     try:
         query = sorted(query.items())
     except TypeError:
         query = sorted(query.items(), key=make_key)
else:
     query = query.items()




----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 8/18/2012 11:47 AM, MRAB wrote:

Seems adequate to me. Most programs wouldn't care about the order, 
because most web frameworks grab whatever is there in whatever order, 
and present it to the web app in their own order.

Programs that care, or which talk to web apps that care, are unlikely to 
want the order from a non-randomized dict, and so have already taken 
care of ordering issues, so undoing the randomization seems like a 
solution in search of a problem (other than for poorly written test cases).
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120818/8635ad05/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Antoine Pitrou writes:

 > That's unsubstantiated.

Sure.  If I had a CVE, I would have posted it.

 > Give an example of how sorted URLs compromise security.

That's not how you think about security; the right question about
sorted URLs is "how do you know that they *don't* compromise
security?"  We know that mishandling URLs *can* compromise security
(eg, via bugs in directory traversal).

But you know that.  What you presumably mean here is "why do you think
randomly changing query parameter order in URLs is more secure than
sorted order?"  The answer to that is that since the server can't
depend on order, it *must* handle more configurations of parameters by
design (and presumably in implementation and testing), and therefore
will be robust against more kinds of parameter configurations.  Eg,
there will be no temptation to optimize processing by handling
parameters in sorted order.

Is this a "real" danger?  Maybe not.  But every unnecessary regularity
in inputs that a program's implementation depends on is a potential
attack vector via irregular inputs.

Remember, I was responding to a claim that sorted order is *always*
better.  That's a dangerous kind of claim to make about anything that
could be input to an Internet server.

Steve



----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Joao S. O. Bueno writes:

 > Ageeded that "any way one thinks about it" is far too strong a claim -
 > but I still hold to the point. Maybe "most ways one thinks about it"
 > :-)  .

100% agreement now.<wink/>




----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 19 Aug 2012 20:55:31 +0900
"Stephen J. Turnbull" <stephen at xemacs.org> wrote:

Ok, so you have no evidence.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Should urlencode() sort the query parameters (if
 they come from a dict)?
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Sat, Aug 18, 2012 at 1:55 PM, Glenn Linderman <v+python at g.nevcal.com> wrote:


I am of the same thought too. Changing a behavior based on the test
case expectation, no matter if the behavior is a harmless change is
still a change. Coming to the point testing query string could be
useful in some cases and then giving weightage to the change seems
interesting use case, but does not seem to warrant a change. I think,
I like Christian Heimes suggestion that a wrapper to compare query
strings would be useful and in Guido's original test case, a tittle
test code change would have been good.

Looks like Guido has withdrawn the bug report too.

-- 
Senthil

