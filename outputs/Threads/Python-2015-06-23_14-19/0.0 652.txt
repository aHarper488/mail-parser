
============================================================================
Subject: [Python-Dev] Fwd: Readability of hex strings (Was: Use of
 coding cookie in 3.x stdlib)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Fwd: Readability of hex strings (Was: Use of
 coding cookie in 3.x stdlib)
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 26.07.2010 22:28, schrieb anatoly techtonik:

It doesn't matter *why* you post to a mailing list -- you just have to be
subscribed to post.  Whoever redirects you there is usually not in the
position of automatically subscribing anyone, and most people wouldn't
much like being subscribed automatically, don't you think?

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.


