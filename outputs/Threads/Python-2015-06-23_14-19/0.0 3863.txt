
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.3): backported
	rev	79713 from 3.4, test_recursion_limit skipped for -O0
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): backported
	rev	79713 from 3.4, test_recursion_limit skipped for -O0
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 24 Apr, 2013, at 8:14, Ronald Oussoren <ronaldoussoren at mac.com> wrote:


In particular, this patch appears to fix the crash that's the reason for disabling the test:

diff --git a/Python/thread_pthread.h b/Python/thread_pthread.h
--- a/Python/thread_pthread.h
+++ b/Python/thread_pthread.h
@@ -28,7 +28,7 @@
  */
 #if defined(__APPLE__) && defined(THREAD_STACK_SIZE) && THREAD_STACK_SIZE == 0
 #undef  THREAD_STACK_SIZE
-#define THREAD_STACK_SIZE       0x500000
+#define THREAD_STACK_SIZE       0x550000
 #endif
 #if defined(__FreeBSD__) && defined(THREAD_STACK_SIZE) && THREAD_STACK_SIZE == 0
 #undef  THREAD_STACK_SIZE

Without this patch test_recursion_limit fails due to a crash, with the patch the test passes (debug build, x86_64, OSX 10.8.3).

Ronald


