
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Handle a possible race
	condition
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Handle a possible race
	condition
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, May 1, 2012 at 10:35 AM, raymond.hettinger
<python-checkins at python.org> wrote:

To get the desired effect, I believe you also need s/if currsize/elif currsize/

Cheers,
Nick.


-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

