
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #18520: Add a new
 PyStructSequence_InitType2() function, same than
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #18520: Add a new
 PyStructSequence_InitType2() function, same than
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
"Add a new PyStructSequence_InitType2()"

I added a new function because I guess that it would break the API (and
ABI) to change the return type of a function in a minor release.

Tell me if you have a better name than PyStructSequence_InitType2() ;-)

"Ex" suffix is usually used when parameters are added. It is not the case
here.

Victor
Le 22 juil. 2013 23:59, "victor.stinner" <python-checkins at python.org> a
?crit :

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130723/b648345b/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #18520: Add a new
 PyStructSequence_InitType2() function, same than
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
We've cheerfully broken the ABI before on minor releases, though if
it's part of the stable ABI, we can't be cavaliar about that anymore.

2013/7/22 Victor Stinner <victor.stinner at gmail.com>:



-- 
Regards,
Benjamin

