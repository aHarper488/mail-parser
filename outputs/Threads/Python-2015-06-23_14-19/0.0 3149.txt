
============================================================================
Subject: [Python-Dev] Doc/ACKS and Misc/ACKS
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Doc/ACKS and Misc/ACKS
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 23 Jul 2012 18:38:30 +0200
Jesus Cea <jcea at jcea.es> wrote:

That said, we could probably merge Doc/ACKS and Misc/ACKS (*). There
doesn't seem to be any strong argument for separating doc contributions
from other contributions.

(*) I think perhaps ?ric already proposed it at some point

Regards

Antoine.


-- 
Software development and contracting: http://pro.pitrou.net





----------------------------------------
Subject:
[Python-Dev] Doc/ACKS and Misc/ACKS
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

+1



----------------------------------------
Subject:
[Python-Dev] Doc/ACKS and Misc/ACKS
----------------------------------------
Author: Meador Ing
Attributes: []Content: 
On Mon, Jul 23, 2012 at 12:17 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:


+1

-- Meador



----------------------------------------
Subject:
[Python-Dev] Doc/ACKS and Misc/ACKS
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 23/07/12 19:30, Eli Bendersky wrote:

+1 too.

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBUA2XtJlgi5GaxT1NAQKiXQQAnOmVaALBmcAbEK7vImQ03m6tdh86ZyU/
VyRuoHVgHxsOn83h2VG+94zjNutedIMK9rq1hEhhPApJcXnYwftMpgEwlyj7vLFA
RUz8c02sKpoi/T8BGv2xVdW09yeMCUwzTDAuaS73NqscwcGplibaSPU5oKOjqetc
NhS0JdGQcr8=
=Ifpc
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] Doc/ACKS and Misc/ACKS
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Mon, Jul 23, 2012 at 1:28 PM, Jesus Cea <jcea at jcea.es> wrote:

Before everyone else on the list just writes a two character "+1"
response, can we just assume that if you don't speak up, you're ok
with it?

Especially when it's about an ack file...



----------------------------------------
Subject:
[Python-Dev] Doc/ACKS and Misc/ACKS
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Brian Curtin wrote:

You mean I don't get to be clever and say

+1 + too == +3

?

;)

~Ethan~

