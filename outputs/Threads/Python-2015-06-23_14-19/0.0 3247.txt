
============================================================================
Subject: [Python-Dev] What's the best way to debug python3 source code?
	(for this bug: http://bugs.python.org/issue15068)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] What's the best way to debug python3 source code?
	(for this bug: http://bugs.python.org/issue15068)
----------------------------------------
Author: gmspr
Attributes: []Content: 
@martin,

I'm working on this bug, http://bugs.python.org/issue15068
I tried this with gdb
(gdb)run
blabla
blabla
blabla
CTRL+D
CTRL+D
(gdb)backtrace

0xb7f08348 in ___newselect_nocancel () at ../sysdeps/unix/syscall-template.S:82
82??? ../sysdeps/unix/syscall-template.S: No such file or directory.
??? in ../sysdeps/unix/syscall-template.S
Current language:? auto
The current source language is "auto; currently asm".
(gdb) backtrace
#0? 0xb7f08348 in ___newselect_nocancel () at ../sysdeps/unix/syscall-template.S:82
#1? 0xb7b43f9f in readline_until_enter_or_signal (prompt=0xb7b578d0 ">>> ", signal=0xbfffed54)
??? at /home/user1/python/Python-3.2.3/Modules/readline.c:987
#2? 0xb7b440ed in call_readline (sys_stdin=0xb7f84420, sys_stdout=0xb7f844c0, prompt=0xb7b578d0 ">>> ")
??? at /home/user1/python/Python-3.2.3/Modules/readline.c:1082
#3? 0x0811a15b in PyOS_Readline (sys_stdin=0xb7f84420, sys_stdout=0xb7f844c0, prompt=0xb7b578d0 ">>> ")
??? at ../Parser/myreadline.c:200
#4? 0x0811b92d in tok_nextc (tok=0x82e1d08) at ../Parser/tokenizer.c:897
#5? 0x0811c39b in tok_get (tok=0x82e1d08, p_start=0xbfffef20, p_end=0xbfffef1c) at ../Parser/tokenizer.c:1306
#6? 0x0811cda2 in PyTokenizer_Get (tok=0x82e1d08, p_start=0xbfffef20, p_end=0xbfffef1c) at ../Parser/tokenizer.c:1687
#7? 0x08119866 in parsetok (tok=0x82e1d08, g=0x81df0c0, start=256, err_ret=0xbfffefd8, flags=0xbfffefd4)
??? at ../Parser/parsetok.c:150
#8? 0x081197a6 in PyParser_ParseFileFlagsEx (fp=0xb7f84420, filename=0x818ac7a "<stdin>", enc=0xb7c57750 "UTF-8", g=
??? 0x81df0c0, start=256, ps1=0xb7b578d0 ">>> ", ps2=0xb7b578e8 "... ", err_ret=0xbfffefd8, flags=0xbfffefd4)
??? at ../Parser/parsetok.c:100
#9? 0x080cb96d in PyParser_ASTFromFile (fp=0xb7f84420, filename=0x818ac7a "<stdin>", enc=0xb7c57750 "UTF-8", start=256, ps1=
??? 0xb7b578d0 ">>> ", ps2=0xb7b578e8 "... ", flags=0xbffff194, errcode=0xbffff044, arena=0x8285a50)
??? at ../Python/pythonrun.c:1941
#10 0x080c9c9c in PyRun_InteractiveOneFlags (fp=0xb7f84420, filename=0x818ac7a "<stdin>", flags=0xbffff194)
??? at ../Python/pythonrun.c:1175
#11 0x080c99ed in PyRun_InteractiveLoopFlags (fp=0xb7f84420, filename=0x818ac7a "<stdin>", flags=0xbffff194)
??? at ../Python/pythonrun.c:1086
#12 0x080c98b5 in PyRun_AnyFileExFlags (fp=0xb7f84420, filename=0x818ac7a "<stdin>", closeit=0, flags=0xbffff194)
??? at ../Python/pythonrun.c:1055
#13 0x080deb5c in run_file (fp=0xb7f84420, filename=0x0, p_cf=0xbffff194) at ../Modules/main.c:307
#14 0x080df5c0 in Py_Main (argc=1, argv=0x81f7008) at ../Modules/main.c:733
#15 0x0805c3d9 in main (argc=1, argv=0xbffff2f4) at ../Modules/python.c:63

But i can't get any clue which file to look at. 

Thanks
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120618/96a990c0/attachment.html>

