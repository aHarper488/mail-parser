
============================================================================
Subject: [Python-Dev] Snakebite build slaves and developer SSH/GPG public
	keys
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Snakebite build slaves and developer SSH/GPG public
	keys
----------------------------------------
Author: Trent Nelso
Attributes: []Content: 
Hi folks,

    I've set up a bunch of Snakebite build slaves over the past week.
    One of the original goals was to provide Python committers with
    full access to the slaves, which I'm still keen on providing.

    What's a nice simple way to achieve that in the interim?  Here's
    what I was thinking:

        - Create a new hg repo: hg.python.org/keys.

        - Committers can push to it just like any other repo (i.e.
          same ssh/authz configuration as cpython).

        - Repo is laid out as follows:
            keys/
                <python username>/
                    ssh     (ssh public key)
                    gpg     (gpg public key)

        - Prime the repo with the current .ssh/authorized_keys
          (presuming you still use the --tunnel-user facility?).

    That'll provide me with everything I need to set up the relevant
    .ssh/authorized_keys stuff on the Snakebite side.  GPG keys will
    be handy if I ever need to send passwords over e-mail (which I'll
    probably have to do initially for those that want to RDP into the
    Windows slaves).

    Thoughts?

    As for the slaves, here's what's up and running now:

        - AMD64 Mountain Lion [SB]
        - AMD64 FreeBSD 8.2 [SB]
        - AMD64 FreeBSD 9.1 [SB]
        - AMD64 NetBSD 5.1.2 [SB]
        - AMD64 OpenBSD 5.1 [SB]
        - AMD64 DragonFlyBSD 3.0.2 [SB]
        - AMD64 Windows Server 2008 R2 SP1 [SB]
        - x86 NetBSD 5.1.2 [SB]
        - x86 OpenBSD 5.1 [SB]
        - x86 DragonFlyBSD 3.0.2 [SB]
        - x86 Windows Server 2003 R2 SP2 [SB]
        - x86 Windows Server 2008 R2 SP1 [SB]

    All the FreeBSD ones use ZFS, all the DragonFly ones use HAMMER.
    DragonFly, NetBSD and OpenBSD are currently reporting all sorts
    of weird and wonderful errors, which is partly why I want to set
    up ssh access sooner rather than later.

    Other slaves on the horizon (i.e. hardware is up, OS is installed):

        - Windows 8 x64 (w/ VS2010 and VS2012)
        - HP-UX 11iv2 PA-RISC
        - HP-UX 11iv3 Itanium (64GB RAM)
        - AIX 5.3 RS/6000
        - AIX 6.1 RS/6000
        - AIX 7.1 RS/6000
        - Solaris 9 SPARC
        - Solaris 10 SPARC

    Nostalgia slaves that probably won't ever see green:
        - IRIX 6.5.33 MIPS
        - Tru64 5.1B Alpha

    If anyone wants ssh access now to the UNIX platforms in order to
    debug/test, feel free to e-mail me directly with your ssh public
    keys.

    For committers on other Python projects like Buildbot, Django and
    Twisted that may be reading this -- yes, the plan is to give you
    guys Snakebite access/slaves down the track too.  I'll start looking
    into that after I've finished setting up the remaining slaves for
    Python.  (Setting up a keys repo will definitely help (doesn't have
    to be hg -- feel free to use svn/git/whatever, just try and follow
    the same layout).)

    Regards,

        Trent "that-took-a-bit-longer-than-expected" Nelson.

