
============================================================================
Subject: [Python-Dev] packaging
Post Count: 12
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Hey,

I just wanted to summarize what we've started at the sprint (and
hopefully finish 1 to 7 this week):

1/ distutils2 is merged as the "packaging" Python package in the
standard library
2/ distutils2 will provide a "pysetup" script in Python to run all
packaging tools (pysetup is a wrapper that will run
"packaging.run.main")
3/ pkgutil gets the new API (PEP 376) we implemented in distutils2._backport
4/ sysconfig installations paths are moved to a sysconfig.cfg file.
5/ the sysconfig,cfg file will be located besides sysconfig.py in the
standard library
6/ sysconfig will lookup for sysconfig.cfg in several places and merge
sections from down to bottom:
     1/ current directory
     2/ per-user site-packages
     3/ global site-packages
7/ a backport for 2.4 to 3.2 will be provided for "packaging" using
the "distutils2" name
8/ we will release distutils2 in the next 18 months
9/ once 3.3 is out, the backport will just get bug fixes


Cheers
Tarek

-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 14 Mar 2011 15:39:55 -0400
Tarek Ziad? <ziade.tarek at gmail.com> wrote:

Why does it get yet another name?
We already have distutils, setuptools, distribute, distutils2... now
"packaging"?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 14, 2011, at 09:46 PM, Antoine Pitrou wrote:


We need a unique name for Python 3.3, otherwise third party modules can't be
written to conditionally depend on the batteries-included version in 3.3, or a
standalone backport for older Pythons.

Guido's already pronounced that it will be 'packaging' in 3.3.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110314/412c468e/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
On Mon, Mar 14, 2011 at 4:46 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

For  it makes sense to have a "packaging" namespace in the standard
library, where we'll be able to add more modules, features etc.

"Distutils" becomes a misnomer now that we'll have features like:

- display the list of installed projects
- search projects at PyPI
- ...

And it's also a good way to prevent any conflict with 3.3 : the
standalone version for 2.4 to 3.2 is "distutils2", and people won't
have to deal with the same package being in the stdlib and at PyPI.
(like json vs simplejson, unittest vs unittest2...)

Cheers
Tarek




-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 14 Mar 2011 18:00:50 -0400
Tarek Ziad? <ziade.tarek at gmail.com> wrote:

But doesn't it also mean many setup.py scripts will have very tedious
import sequences, such as:

try:
    from packaging.compiler import FooCompiler
    from packaging.commands import BarCommand
except ImportError:
    try:
        from distutils2.compiler import FooCompiler
        from distutils2.commands import BarCommand
    except ImportError:
        try:
            from setuptools.compiler import FooCompiler
            from setuptools.commands import OtherNameForBarCommand as \
                                            BarCommand
        except ImportError:
            from distutils.compiler import FooCompiler
            from distutils.commands import OtherNameForBarCommand as \
                                           BarCommand

(I'm still remembering the import dances which were necessary to get
cElementTree/ElementTree in the 2.4-2.5 days)

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Setup.py is gone in distutils2 and therefore in packaging
Le 14 mars 2011 18:27, "Antoine Pitrou" <solipsis at pitrou.net> a ?crit :
http://mail.python.org/mailman/options/python-dev/ziade.tarek%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110314/c065aa7e/attachment.html>



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 14 March 2011 22:34, Tarek Ziad? <ziade.tarek at gmail.com> wrote:

Where can I find the documentation? The distutils2 docs ("A simple
example") still use setup.py. See
http://packages.python.org/Distutils2/distutils/introduction.html#a-simple-example

Paul



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Antoine Pitrou wrote:

 > ...

Maybe this will be the killer app for the "or" enhancement
to the import statement. :-)

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Mar 15, 2011 at 10:27 AM, Greg Ewing
<greg.ewing at canterbury.ac.nz> wrote:

Except that won't help, since even if it were added right now, pre-3.3
compatible code couldn't use it :)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
On Tue, Mar 15, 2011 at 7:50 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

or if you backport it, we could add a new fallback ;)

try:
    from __future__ import or_importer
except ImportError:
    XXX <-- previous proposal
 else:
    or based proposal







-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Mar 15, 2011 at 9:48 AM, Tarek Ziad? <ziade.tarek at gmail.com> wrote:

Alas, the fallback trick doesn't work for the from __future__ compiler hacks.

What you could do though, is isolate the logic for the import
fallbacks and then do:

from myproject.import_fallbacks import compiler, commands

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] packaging
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Tarek Ziad? wrote:


This could easily be fixed if we allowed run-time access
to the time machine:

   from __future__ import temporal_mechanics, or_importer
   import timemachine
   timemachine.backport_feature("or_importer", from_version = "3.4")
   ...

(For obvious reasons, this will work despite the future
import of or_importer occurring before the code that
backports it.)

Manual use of the time machine would be required to
backport the timemachine module itself, after which
there would never be any backwards compatibility
problems again.

-- 
Greg

