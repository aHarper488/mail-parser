
============================================================================
Subject: [Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
Post Count: 12
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: =?utf-8?B?S3Jpc3Rqw6FuIFZhbHVyIErDs25zc29u?
Attributes: []Content: 
Yes!
For many years I have been very frustrated by the install-centric nature of python.  I am biased, of course, by the fact that I am developing an application where python is embedded, an application that needs to run out of the box.  A developer may have many many versions (branches) of the application on his drive, and each needs to work separately.
We have managed to isolate things, by patching python (and contributing that patch) to override the default library seach path (and ignore environment paths) when python is started up thogh the api.  All well and good.
But recently we have started in increasing amount to use external libraries and packages and we have been introduced to the dependency hell that is public python packages.  In this install-centric world, developers reference huge external packages without a second thought, which cause large dependency trees.  Using a simple tool may require whole HTTP frameworks to be downloaded.
What is worse is when there are versioning conflicts between those dependencies.

I don't have a well formed solution in mind, but I would see it desirable to have a way for someone to release his package with all its dependencies as a self-contained and isolated unit.  E.g. if package foo.py relies on functionality from version 1.7 of bar.py, then that functionality could be bottled up for foo?s exclusive usage.
Another package, baz.py, could then also make use of bar, but version 1.8.  The two bar versions would be isolated.

Perhaps this is just a pipedream.  Even unpossible.  But it doesn't harm to try to think about better ways to do things.
K


-----Original Message-----
From: Christian Tismer [mailto:tismer at stackless.com] 
Sent: 15. n?vember 2012 23:10
To: Kristj?n Valur J?nsson
Cc: python-dev at python.org
Subject: Generally boared by installation (Re: [Python-Dev] Setting project home path the best way)

Hi guys,

I am bored of installing things. 
Bored of things that happen to not work for some minor reasons. 
Reasons that are not immediately obvious. 
Things that don't work because some special case was not handled. 
Things that compile for half an hour and then complain that something is not as expected. 
May it be a compiler, a library, a command like pip or easy-install, a system like macports or homebrew, virtualenv, whatsoever. 

These things are all great if they work. 

When they do not work, the user is in some real trouble. And he reads hundreds Of blogs and sites and emails, which all answer a bit of slightly related questions, but all in all - 

This is not how Python should work !!

I am really bored and exhausted and annoyed by those packages which Pretend to make my life eadier, but they don't really. 

Something is really missing. I want something that is easy to use in all cases, also if it fails. 

Son't get me wrong, I like things like pip or virtualenv or homebrew. 
I just think they have to be rewritten completely. They have the wrong assumption that things work!

The opposite should be the truth: by default, things go wrong. Correctness is very fragile. 

I am thinking of a better concept that is harder to break. I thin to design a setup tool that is much more checking itself and does not trust in any assumption. 

Scenario:
After hours and hours, I find how to modify setup.py to function almost correctly for PySide. 

This was ridiculously hard to do! Settings for certain directories, included and stuff are not checked when they could be, but after compiling a lot of things!

After a lot of tries and headaches, I find out that virtualenv barfs on a simple link like ./.Python, the executable, when switching from stock Python to a different (homebrew) version!!

This was obviously never tested well, so it frustrates me quite a lot.  

I could fill a huge list full of complaints like that if I had time. But I don't. 

Instead, I think installation scripts are generally still wrong by concept today and need to be written in a different way. 

I would like to start a task force and some sprints about improving this situation. 
My goal is some unbreakable system of building blocks that are self-contained with no dependencies, that have a defined interface to talk to, and that know themselves completely by introspection. 

They should not work because they happen to work around all known defects, but by design and control. 

Whoever is interested to work with me on this is hereby honestly welcomed!

Cheers - chris

Sent from my Ei4Steve

On Nov 15, 2012, at 10:17, Kristj?n Valur J?nsson <kristjan at ccpgames.com> wrote:





----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Nov 18, 2012 at 8:54 PM, Kristj?n Valur J?nsson <
kristjan at ccpgames.com> wrote:

Easily bundling dependencies is a key principle behind the ability to
execute directories and zipfiles that contain a top level __main__.py file
that was added back in 2.6 (although the zipfile version doesn't play
nicely with extension modules).

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121118/e0da2c51/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
I'm intrigued.  I thought this was merely so that one could do
python -m mypackage.mysubpackage
Can you refer me to the rationale and discussion about this feature?

K

From: Nick Coghlan [mailto:ncoghlan at gmail.com]
Sent: 18. n?vember 2012 11:25
To: Kristj?n Valur J?nsson
Cc: Christian Tismer; python-dev at python.org
Subject: Re: [Python-Dev] Generally boared by installation (Re: Setting project home path the best way)

Easily bundling dependencies is a key principle behind the ability to execute directories and zipfiles that contain a top level __main__.py file that was added back in 2.6 (although the zipfile version doesn't play nicely with extension modules).

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com<mailto:ncoghlan at gmail.com>   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121120/66800b3c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Nov 20, 2012 at 7:06 PM, Kristj?n Valur J?nsson <
kristjan at ccpgames.com> wrote:


It was part of a fairly long progression in changes to the command line
execution support :)

Top level package execution with -m came first in 2.4, arbitrary package
execution for -m arrived in 2.5 (along with the runpy module), directory
and zipfile execution (by supplying a valid sys.path entry as the "script"
command line argument) was added in 2.6/3.0, and finally officially
supported package execution via -m only arrived in 2.7 and 3.1 (a broken
version of the last accidentally existed in 2.5, but that was just a
mistake that arose when merging the import emulations in runpy and pkgutil,
and had a side effect that violated at least one of the import system
invariants).

In the specific case of directory and zipfile execution, discussion
happened on the tracker: http://bugs.python.org/issue1739468

It was never brought up on python-dev because Guido was participating
directly in the tracker discussion. Unfortunately, we then also forgot to
mention it in the original version of the 2.6 What's New doc, so the vast
majority of people missed the addition :(

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121120/29969587/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Christian Tisme
Attributes: []Content: 
On 20.11.12 12:39, Nick Coghlan wrote:

Hi Nick,

thank you very much for this story and the link to the issue tracker!
A very good move for Python which is really not mentioned enough
to make people more aware of a great feature.

I think part of this discussion should get a more prominent place
for gems that can be found without knowing what to search. ;-)

Is the issue tracker permanent enough to reference it?
Maybe there could be some auxiliary info page with proper keywords
that collects links to relevant discussions like this.
Do we have such a thing already?

ciao - chris

-- 
Christian Tismer             :^)   <mailto:tismer at stackless.com>
Software Consulting          :     Have a break! Take a ride on Python's
Karl-Liebknecht-Str. 121     :    *Starship* http://starship.python.net/
14482 Potsdam                :     PGP key -> http://pgp.uni-mainz.de
phone +49 173 24 18 776  fax +49 (30) 700143-0023
PGP 0x57F3BF04       9064 F4E1 D754 C2FF 1619  305B C09C 5A3B 57F3 BF04
       whom do you want to sponsor today?   http://www.stackless.com/

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121120/a2715f50/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Nov 20, 2012 at 11:45 PM, Christian Tismer <tismer at stackless.com>wrote:


Technically, that's the "using" guide:
http://docs.python.org/2/using/cmdline.html#interface-options (see the
explanation of what's executable under the "<script>" tag)

I also wrote up a summary last year on my blog:
http://www.boredomandlaziness.org/2011/03/what-is-python-script.html

(The main change since that post is that the Python launcher brings shebang
line support to Windows, although I haven't checked if it works properly
with prefixed zip files)

Maybe I should plan to sign up to present an updated version of my PyCon AU
2010 "What is a Python script?" lightning talk at PyCon US 2013 :)



I've been referencing that particular issue for years now, so I certainly
think so :)



I've sometimes thought it may be a good idea to split out a separate "What
is a Python script?" section in the using guide, separate from the existing
descriptions under the interpreter options. I've never tried to figure out
the details of how that would actually work, though.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121121/642d32f1/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Daniel Holt
Attributes: []Content: 
On Tue, Nov 20, 2012 at 9:44 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:


The wheel projects' own wheel file (a zip file) takes advantage of this
feature in its own way:

python wheel-0.14.0-py2.py3-none-any.whl/wheel

Python looks inside the /wheel directory of the zip file, finds
__main__.py, and executes it; the archive can be used to install itself or
any other wheel file. (There is no __main__.py at the root because the
wheel design would install that __main__.py into the root of site-packages).


This underutilized feature of executing directories with __main__.py is
very useful for implementing Python applications instead of just libraries.

It might be interesting to define a "wheels" format which would be a bunch
of unpacked wheel files and a __main__.py like:

__main__.py
package1-x86.whl/...
package1-armel.whl/...
package2-noarch.whl/

__main__.py would add the appropriate packages to PYTHONPATH based on the
architecture, unpack dll's pkg_resources style, and run the program.


There is some activity in the tracker about adding the missing "add this to
PYTHONPATH" / "isolate self from the environment" command line arguments to
Python.

Daniel Holth
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121120/d9b97357/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
Where in the tracker?  I tried searching but didn't find it.

I contributed to the pep405 discussions with similar concerns back in march:
http://mail.python.org/pipermail/python-dev/2012-March/117894.html


From: Python-Dev [mailto:python-dev-bounces+kristjan=ccpgames.com at python.org] On Behalf Of Daniel Holth


There is some activity in the tracker about adding the missing "add this to PYTHONPATH" / "isolate self from the environment" command line arguments to Python.

Daniel Holth
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121122/407890da/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Nov 22, 2012 at 9:44 PM, Kristj?n Valur J?nsson <
kristjan at ccpgames.com> wrote:


This one: http://bugs.python.org/issue13475

This and the issue about being able to configure coverage.py cleanly in
subprocesses (http://bugs.python.org/issue14803) are the two issues which
have got me thinking about the interpreter startup and configuration
process in general lately. Both would add *more* complexity to an already
complicated and hard to follow initialisation sequence, so I now think we
should be look at opportunities for rationalisation *first*, before we make
things even messier.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121122/b00a17de/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Thu, Nov 22, 2012 at 5:09 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

There's also http://bugs.python.org/issue15716 ("Ability to specify
the PYTHONPATH via a command line flag").

-eric



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Tue, Nov 27, 2012 at 9:19 PM, Eric Snow <ericsnowcurrently at gmail.com> wrote:

I knew there was one more: http://bugs.python.org/issue16499 ("CLI
option for isolated mode").

-eric



----------------------------------------
Subject:
[Python-Dev] Generally boared by installation (Re: Setting
 project home path the best way)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Nov 28, 2012 at 2:46 PM, Eric Snow <ericsnowcurrently at gmail.com>wrote:


Along with another PYIOENCODING related one that the Blender folks reported
(Christian Heimes pointed it out to me earlier today).

Anyway, I created a page on the wiki for the data gathering process:

http://wiki.python.org/moin/CPythonInterpreterInitialization

It's now a matter of going through and sorting out:

1. What gets set during startup?
2. Where does it get set (or modified)?
3. How is that configured?

Once we have a good view of that, *then* we can start looking for ways to
simplify the code, make the whole system more embedding friendly (i.e. by
giving the embedding app total control via a simple and clean API) and
still support the proposals for improvements.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121128/9fc6bafa/attachment.html>

