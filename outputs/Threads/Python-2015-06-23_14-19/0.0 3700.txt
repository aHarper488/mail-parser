
============================================================================
Subject: [Python-Dev] Issues Pending Review
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Issues Pending Review
----------------------------------------
Author: =?UTF-8?Q?Michele_Orr=C3=B9?
Attributes: []Content: 
Hello, I have some issues pending 'patch review' hanging for more than
two weeks, could somebody please check them out?

HTTPServer does not correctly handle bad headers /
http://bugs.python.org/issue16083
rlcompleter adds builtins when custom dict is used /
http://bugs.python.org/issue5256
a -= b should be fast if a is a small set and b is a large set /
http://bugs.python.org/issue8425 (note: hettinger said to wait for his
-noisy before applying)
Add support for IEEE 754 contexts to decimal module. /
http://bugs.python.org/issue8786

-- 
?



----------------------------------------
Subject:
[Python-Dev] Issues Pending Review
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

Zitat von Michele Orr? <maker.py at gmail.com>:


In case nobody picks it up, my 5-for-1 offer still stands: if you review
five issues, I'll review one of yours.

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] Issues Pending Review
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 14.10.12 21:38, martin at v.loewis.de wrote:

What 50 issues you want I'll review.





----------------------------------------
Subject:
[Python-Dev] Issues Pending Review
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

Is that a general offer or just for the OP? :-)
Seriously, getting an issue reviewed by Martin is something worth fighting
for, and reviewing other 5 issues seems like a small price. This is not to
imply that Martin doesn't review others' issues often enough, just that his
reviews come from a high authority and can almost always put a final
resolution on the problem.

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121016/cba98bcf/attachment.html>

