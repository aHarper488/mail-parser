
============================================================================
Subject: [Python-Dev] Using PEP384 Stable ABI for the lzma extension module
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Using PEP384 Stable ABI for the lzma extension module
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
Hi,

Has someone already tried to *really* use Py_LIMITED_API
for some "serious" extension module?
I wanted to give it a try for the _lzma module (see issue 6715)
because liblzma does not compile with Microsoft compilers; an
alternative could be to use mingw to (pre)build _lzma.pyd, which would
link with a static liblzma.a also compiled with mingw.

Mixing compilers in a Python process is one of the reasons of PEP384,
so I added #define Py_LIMITED_API on top of the module,
and "fixed" the issues one by one:

- Py_LIMITED_API is incompatible with --with-pydebug, and compilation stops.
  I skipped the check to continue.

- I replaced PyBytes_GET_SIZE() with Py_SIZE(), which is OK,
and PyBytes_AS_STRING() with PyBytes_AsString(), which may
have a slight performance impact.

- I replaced
      Py_TYPE(self)->tp_free((PyObject *)self);
  with PyObject_Del(self), I hope this is the same thing
  (for a non-GC object)

- _PyBytes_Resize() is missing; I moved it under a Py_LIMITED_API
  section.

- For the "y*" argument spec, the Py_buffer structure is required
  (only for two fields: buf and len), as well as PyBuffer_Release()

- PyType_FromSpec() does not call PyType_Ready(), which caused
  crashes in __new__.

Now the module seems to work correctly and passes tests... at least on
Linux in a standard environment.  I will do other tests on Windows.

What do you think about using the stable ABI even in shipped extensions?
Have you already used it somewhere else?

Cheers,

-- 
Amaury Forgeot d'Arc

