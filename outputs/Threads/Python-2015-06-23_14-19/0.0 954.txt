
============================================================================
Subject: [Python-Dev] unexpected traceback/stack behavior with chained
	exceptions (issue 1553375)
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] unexpected traceback/stack behavior with chained
	exceptions (issue 1553375)
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
Issue 1553375 [1] proposes a patch to add an 'allframes' option to the
traceback printing and formatting routines so that the full traceback
from the top of the execution stack down to the exception is printed,
instead of just from the point where the exception is caught down to
the exception.  This is useful when the reason you are capturing the
traceback is to log it, and you have several different points in your
application where you do such traceback logging.  You often really want
to know the entire stack in that case; logging only from the capture
point down can lose important debugging information depending on how
the application is structured.

The patch seems to work well, except for one problem that I don't have
enough CPython internals knowledge to understand.  If the traceback we
are printing has a chained traceback, the resulting full traceback shows
the line that is printing the traceback instead of the line from the 'try'
block.  (It prints the expected line if there is no chained traceback).

So, is this a failure in my understanding of how tracebacks are supposed
to work, or a bug in how chained tracebacks are constructed?

[1] http://bugs.python.org/issue1553375

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] unexpected traceback/stack behavior with chained
	exceptions (issue 1553375)
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Nick Coghlan <ncoghlan <at> gmail.com> writes:


Good point, Nick. There are times when you'd want to know how you got to a
certain point in code, irrespective of whether any exception occurred. So your
suggestion makes sense, and I'll try and see if I can get it into 3.2.

Another benefit of this is that a user only gets this if they want it; if I were
to use the allframes flag in logging, then everyone would get the print_stack()
even if they didn't want it.

Regards,

Vinay Sajip


