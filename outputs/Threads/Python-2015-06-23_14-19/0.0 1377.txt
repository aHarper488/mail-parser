
============================================================================
Subject: [Python-Dev] Why are there no 'set' and 'frozenset' types in
	the	'types' module?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Why are there no 'set' and 'frozenset' types in
	the	'types' module?
----------------------------------------
Author: exarkun at twistedmatrix.co
Attributes: []Content: 
On 02:01 pm, haael at interia.pl wrote:

Maybe this is what you're after?
[<type 'type'>,
<type 'weakref'>,
<type 'weakcallableproxy'>,
<type 'weakproxy'>,
<type 'int'>,
<type 'basestring'>,
<type 'bytearray'>,
<type 'list'>,
<type 'NoneType'>,
<type 'NotImplementedType'>,
<type 'traceback'>,
<type 'super'>,
<type 'xrange'>,
<type 'dict'>,
<type 'set'>,
<type 'slice'>,
<type 'staticmethod'>,
<type 'complex'>,
<type 'float'>,
<type 'buffer'>,
<type 'long'>,
<type 'frozenset'>,
<type 'property'>,
<type 'tuple'>,
<type 'enumerate'>,
<type 'reversed'>,
<type 'code'>,
<type 'frame'>,
<type 'builtin_function_or_method'>,
<type 'instancemethod'>,
<type 'function'>,
<type 'classobj'>,
<type 'dictproxy'>,
<type 'generator'>,
<type 'getset_descriptor'>,
<type 'wrapper_descriptor'>,
<type 'instance'>,
<type 'ellipsis'>,
<type 'member_descriptor'>,
<type 'EncodingMap'>,
<type 'module'>,
<type 'classmethod'>,
<type 'file'>]

Jean-Paul

