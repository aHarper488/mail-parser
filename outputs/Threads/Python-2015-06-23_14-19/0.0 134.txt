
============================================================================
Subject: [Python-Dev] Is it intentional that "sys.__debug__ = 1" is
 illegal in Python 2.7?
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
 illegal in Python 2.7?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Barry Warsaw wrote:

I don't see that there's any difference. Once upon a time,
__debug__ wasn't special, and someone decided to use it for
their own purposes. Then Guido decided to make it special,
and broke their code, which is within the rules as you
just stated them. The rule doesn't say anything about what
*kinds* of breakage are allowed, so anything goes, including
making it impossible to assign to the name any more.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
 illegal in Python 2.7?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Aug 01, 2010, at 09:56 PM, Benjamin Peterson wrote:


BTW, thanks to Georg for closing the documentation issue.
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100802/4c1a2595/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
 illegal in Python 2.7?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jul 30, 2010, at 01:42 PM, Guido van Rossum wrote:


Shouldn't it be described here then?

http://docs.python.org/reference/lexical_analysis.html#identifiers

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100730/3cb9a4c3/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
 illegal in Python 2.7?
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 30/07/2010 21:53, Barry Warsaw wrote:

And raise a DeprecationWarning one release before becoming invalid syntax?

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies ("BOGUS AGREEMENTS") that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100730/b2deffca/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Is it intentional that "sys.__debug__ = 1" is
 illegal in Python 2.7?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jul 31, 2010, at 08:32 AM, Steven D'Aprano wrote:


I'm with Steven on this one.  I've always understood the rules on
double-underscore names to mean that Python reserves the use of those names
for its own purposes, and is free to break your code if you define your own.
That's very different than saying it's forbidden to use double-underscore
names for your own purposes or assign to them, which is I think what's going
on with the sys.__debug__ example.

If that's the rule, I'd want to make this section of the documentation much
stronger about the prohibitions.  I've just never considered Python's rule
here to be that strong.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100731/083bc77c/attachment.pgp>

