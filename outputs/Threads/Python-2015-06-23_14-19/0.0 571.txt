
============================================================================
Subject: [Python-Dev] Threading bug review + commit privs
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Threading bug review + commit privs
----------------------------------------
Author: Reid Kleckne
Attributes: []Content: 
Hey all,

I'm porting some fixes for threading.py that we applied to unladen-swallow:
http://bugs.python.org/issue6643

We ran into these bizarre race conditions involving fork + threads
while running the test suite with a background JIT compilation thread.
 I really wish we weren't trying to support forking from a child
thread, but it's already in the test suite.  I've "solved" the problem
by throwing away radioactive locks that may have been held across a
fork.*

If I could get a reviewer to look at this, I would be very grateful,
since reviewing threading patches is somewhat tricky.  =/

I'm also expecting to be doing more work merging unladen-swallow into
the py3k-jit branch, so I was wondering if I could get commit
privileges for that.

Thanks,
Reid

* In general I wouldn't think this is safe, but we already do it for
_active_limbo_lock in threading.py.  One of the problems I've
encountered is that on OS X, releasing locks held by other threads
across a fork results in a crash.  Furthermore, when locks are
deallocated, the destructor does a non-blocking acquire and then
release, which I would think would crash.  However, we get lucky here,
because any thread that holds a lock across a fork usually holds a
reference to it on the stack.  Therefore the lock is leaked and the
destructor never run.  Moral: fork + threads is *crazy*, avoid it if
you can.



----------------------------------------
Subject:
[Python-Dev] Threading bug review + commit privs
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 11.07.2010 15:23, schrieb Reid Kleckner:


Sure -- send your public key to me, and I'll set you up.

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.


