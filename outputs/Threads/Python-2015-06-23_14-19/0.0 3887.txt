
============================================================================
Subject: [Python-Dev] SSL sockets and settimeout
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] SSL sockets and settimeout
----------------------------------------
Author: Jon Ribben
Attributes: []Content: 
Am I correct in thinking that Python's newfangled socket.settimeout()
feature does not play well with SSL wrapped sockets? Would there be
any interest in making it so that it did?

