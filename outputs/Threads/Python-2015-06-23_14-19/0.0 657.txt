
============================================================================
Subject: [Python-Dev] GIL musings (was Re: Thoughts fresh after
	EuroPython)
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] GIL musings (was Re: Thoughts fresh after
	EuroPython)
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/07/2010 11:50, Nick Coghlan wrote:

Breaking binary compatibility with C extensions would be "difficult" 
once PEP 384 (stable binary ABI) has gone into effect. As you intimate, 
Ironclad demonstrates that C extensions *can* be interfaced with a 
different garbage collection system whilst maintaining binary 
compatibility. It does impose constraints however (which is why the PyPy 
c-ext implementors chose source compatibility rather than binary 
compatibility).

Michael





-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] GIL musings (was Re: Thoughts fresh after
	EuroPython)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 28 Jul 2010 11:56:16 +0100
Michael Foord <fuzzyman at voidspace.org.uk> wrote:

"Stable" doesn't mean eternal. At worse, we could call the result
Python 4.0.

It should be noted, though, that a full GC can be detrimental to
real-time applications. Kristj?n has already explained how some of his
software disabled the cyclic GC, and took care of breaking cycles
manually instead.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] GIL musings (was Re: Thoughts fresh after
	EuroPython)
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 


On 28 Jul, 2010,at 12:56 PM, Michael Foord <fuzzyman at voidspace.org.uk> wrote:

On 28/07/2010 11:50, Nick Coghlan wrote:

Breaking binary compatibility with C extensions would be "difficult" 
once PEP 384 (stable binary ABI) has gone into effect. As you intimate, 
Ironclad demonstrates that C extensions *can* be interfaced with a 
different garbage collection system whilst maintaining binary 
compatibility. It does impose constraints however (which is why the PyPy 
c-ext implementors chose source compatibility rather than binary 
compatibility).
?
The HotPy author mentioned that he has a scheme where refcounts could be used by C extensions while the system natively uses a copying collector, but I got the impression that this was not fully fleshed out yet.

Apple's Objective-C garbage collector has a simular feature: you can use CFRetain/CFRelease to manage refcounts and the GC will only collect objects where the CF reference count is 0. ?This is a non-copying collector in a C environment though, which makes this scheme easier to implement than with a full generational copying collector.

It should therefore be possible to have an interpreter where the VM uses a real GC and while extensions using the stable ABI could work as is, but that probably requires that Py_INCREF and Py_DECREF expand into function calls in the stable ABI.

Implementing this would still be a significant amount of work.

Ronald



Michael





-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100728/407ee1b6/attachment.html>



----------------------------------------
Subject:
[Python-Dev] GIL musings (was Re: Thoughts fresh after
	EuroPython)
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/07/2010 12:43, Ronald Oussoren wrote:

Ironclad artificially inflates the refcount by one when objects are 
created. If an object is eligible for garbage collection *and* the 
refcount is 1 (so the C extension doesn't hold any references to it) 
then Ironclad decrements the refcount to zero and nature takes its 
course. That's a simplification of course (particularly around what 
happens when an object is eligible for garbage collection but the 
refcount is above 1).

This allows Py_INCREF and Py_DECREF to remain as macros - switching to 
functions would have a performance cost I guess.

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] GIL musings (was Re: Thoughts fresh after
	EuroPython)
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Thu, Jul 29, 2010 at 6:53 AM, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

There are always concurrent or incremental GCs which do not stop the world.

