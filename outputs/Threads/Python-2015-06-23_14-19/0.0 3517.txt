
============================================================================
Subject: [Python-Dev] Allow use of sphinx-autodoc in the standard
	library documentation?
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Allow use of sphinx-autodoc in the standard
	library documentation?
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 10 May 2012 15:02:20 +1000, Nick Coghlan <ncoghlan at gmail.com> wrote:

Yes.  Our reason is that docstrings should be relatively lightweight,
and that the sphinx docs should be the more expansive version of the
documentation.

Yes, this creates a double-maintenance burden, and the two sometimes
slip of of sync.  But it is a long-standing rule and will doubtless
require considerable bikeshedding if we want to change it :)

--David



----------------------------------------
Subject:
[Python-Dev] Allow use of sphinx-autodoc in the standard
	library documentation?
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 10.05.2012 07:02, Nick Coghlan wrote:

The one reason that prevented me from ever proposing this is that to do
this, you have to build the docs with exactly the Python you want the
documentation for.  This can create unpleasant dependencies for e.g.
distributions, and also for developers who cannot build the docs
without first building Python, which can be a hassle, especially under
Windows.  But of course we want people to build the docs before
committing...

The other issue is the extensiveness of the docstrings vs. separate docs.
So far, the latter have always been more comprehensive than the docstrings,
which works nicely for me (although crucial info is sometimes missing in
the docstring).

This difference can be kept, to a degree, even with autodoc, by putting
additional content into the autodoc directive, but that renders one big
autodoc advantage moot: having the documentation in one place only.  Even
worse, if someone changes the docstring, the addendum in the rst file may
become wrong/obsolete/incomprehensible.

cheers,
Georg




----------------------------------------
Subject:
[Python-Dev] Allow use of sphinx-autodoc in the standard
	library documentation?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Thanks, that's pretty much what I thought (although I hadn't  considered
the sys.path and version dependency) .

I'll proceed with the original plan.

Cheers,
Nick.
--
Sent from my phone, thus the relative brevity :)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120511/1a1f3acb/attachment.html>

