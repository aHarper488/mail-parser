
============================================================================
Subject: [Python-Dev] hg extensions
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] hg extensions
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
s/use/used/



----------------------------------------
Subject:
[Python-Dev] hg extensions
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

Mimicking git branches.


I don?t really understand (I don?t really want to, I don?t care for
bookmarks), but maybe this will help:
http://mercurial.selenic.com/wiki/BookmarksExtension#Configuration

Regards



----------------------------------------
Subject:
[Python-Dev] hg extensions
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 27 Feb 2011 01:25:12 +0100
?ric Araujo <merwok at netwok.org> wrote:

I've hardly ever used git but I would be surprised if its change
tracking abilities were so poor.


That's vaguely better (since it allows to advance one bookmark instead
of both), but it still doesn't allow tracking incoming upstream changes.
Meaning that as soon as I "hg pull" (not to mention "hg merge"), I lose
the ability to easily make a collapsed diff of my local changes.

Regards

Antoine.



