
============================================================================
Subject: [Python-Dev] A couple of PEP 418 comments
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] A couple of PEP 418 comments
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

I'm just starting a new thread since the old ones are so crowded.
First, overall I think the PEP is starting to look really good and
insightful! (congratulations to Victor)

I have a couple of comments, mostly small ones:


I think "implementation" is a better name here (more precise, and
perhaps also more accurate :-)).


The descriptions should really stress the scope of the result's
validity. My guess (or wish :-)) would be:

- time.monotonic(): system-wide results, comparable from one process to
  another
- time.perf_counter(): process-wide results, comparable from one thread
  to another (?)
- time.process_time(): process-wide, by definition

It would also be nice to know if some systems may be unable to
implement time.monotonic().


Do we care? :) Precision under recent Windows variants (XP or later)
would be more useful.

Is there a designated dictator for this PEP?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] A couple of PEP 418 comments
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

time.monotonic() and time.perf_counter() are process-wide on Windows
older than Vista because of GetTickCount() overflow, on other OSes,
they are system-wide.


You can find such information in the following section:
http://www.python.org/dev/peps/pep-0418/#clock-monotonic-clock-monotonic-raw-clock-boottime

All OSes provide a monotonic clock, except GNU/Hurd. You mean that it
should be mentioned in the time.monotonic() section?


You can get the precision on Windows Seven in the following table:
http://www.python.org/dev/peps/pep-0418/#monotonic-clocks

I will move the precision of monotonic clock of Windows 9x info into this table.

Victor



----------------------------------------
Subject:
[Python-Dev] A couple of PEP 418 comments
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Fri, Apr 13, 2012 at 11:29, Victor Stinner

I would just remove it entirely. It's not relevant since it's not supported.



----------------------------------------
Subject:
[Python-Dev] A couple of PEP 418 comments
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 13 Apr 2012 18:29:10 +0200
Victor Stinner <victor.stinner at gmail.com> wrote:

Perhaps, but you should say in the PEP, not here ;-)
By the way, I wonder if it may be a problem if monotonic() is
process-wide under Windows.


Yes, that would be clearer.

Regards

Antoine.



