
============================================================================
Subject: [Python-Dev] Adding test case methods to TestCase subclasses (was:
	python and super)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Adding test case methods to TestCase subclasses (was:
	python and super)
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Ethan Furman <ethan at stoneleaf.us> writes:


TestCase subclasses is a multiple-inheritance use case that I share. The
mix-ins add test cases (methods named ?test_? on the mix-in class) to
the TestCase subclass. I would prefer not to use multiple inheritance
for this if it can be achieved in a better way.

How can composition add test cases detectable by Python 2's ?unittest?
and Python 3's ?unittest2??

-- 
 \         ?The userbase for strong cryptography declines by half with |
  `\      every additional keystroke or mouseclick required to make it |
_o__)                                             work.? ?Carl Ellison |
Ben Finney


