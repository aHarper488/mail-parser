
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #15478: Fix
 test_os on Windows (os.chown is missing)
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #15478: Fix
 test_os on Windows (os.chown is missing)
----------------------------------------
Author: Jeremy Klot
Attributes: []Content: 
On Tue, Oct 30, 2012 at 6:07 PM, victor.stinner
<python-checkins at python.org> wrote:

Also missing on Windows is os.lchown.



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #15478: Fix
 test_os on Windows (os.chown is missing)
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

2012/10/31 Jeremy Kloth <jeremy.kloth at gmail.com>:

Fixed, but there were also more errors introduced by #15478. test_os
should now pass again on Windows.

Victor

