
============================================================================
Subject: [Python-Dev] API refactoring tracker field for Python4
Post Count: 23
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
There are many API changes and proposals that were forgotten and
didn't get into Python 3, although they should be, because it was the
only chance to change things with backwards compatibility break. For
example http://bugs.python.org/issue1559549

This happened, because of poor bug management, where community doesn't
play any role in determining which issues are desired.
This mostly because of limitation of our tracker and desire of people
to extend it to get damn "stars", module split, sorting, digging and
tagging options.

I won't be surprised if things won't change in the next couple of
years, that's why I'd like to propose a very small change, so that
when time will come to create Python4 (and standard library won't be
separated from interpreter by this time), everybody can get quickly
get a list of proposed API enhancements and filter which are eligible
for the next BC API break. This change is a simple "api-refactoring"
flag that could be added to corresponding issues by tracker users.
--
anatoly t.



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Fri, Jan 7, 2011 at 11:20, anatoly techtonik <techtonik at gmail.com> wrote:



That can be added in 3.3.
To answer your comment on the issue: no investigation is needed. It didn't
make it in yet because there was no code written for it. It's really not a
big deal, it happens all the time.



The community absolutely plays a role in determining which issues are
desired. They do this by action when they want something. A patch says a
whole lot about desire.



I have no idea what any of this means.

I won't be surprised if things won't change in the next couple of


I'm not sure I see the need for such a flag, as there are probably too few
cases for this in the first place.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110107/c9cd60f0/attachment.html>



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Fri, Jan 7, 2011 at 7:41 PM, Brian Curtin <brian.curtin at gmail.com> wrote:

Don't you think that if more people were aware of this issue, the
patch could be made faster?

Don't you think that if people could review issues and "star" them
then such minor issues could be scheduled for release not only by
"severity" status as decided be release manager and several core
developers, but also by community vote?

Patch requires time, experience and approved contribution agreement,
which you've sent using ground mail beforehand. Voting doesn't require
any of this, but helps core developers see what user community wants.
With the list of desired features Jesse Noller sponsored sprints will
have more value for all of us.


Stars:
 go http://code.google.com/p/support/issues/list
 find Stars column
 guess

Module split:
 try to get all issues for 'os' module
 try to subscribe to all commits for 'CGIHTTPServer'

Sorting:
 click on column titles in bug tracker search results

Tagging:
 as a tracker user, try to add tag 'easy' to some easy issue


I haven't started using Python 3 yet, but I already know some annoying
API issues that are not fixed there. Unfortunately, I don't remember
them to give you a list. That's why I asked for a flag.
-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Fri, Jan 7, 2011 at 12:14, anatoly techtonik <techtonik at gmail.com> wrote:


Maybe, but someone still has to write the code. You could start a facebook
group for the issue and it could have 10,000 "likes", but it still doesn't
solve the problem. I'm reminded of the saying "9 women can't have a baby in
1 month"...

I do think it would be great if more people were involved in the issue
tracker. I don't know what it will take to get more people involved, but I
know it involves a lot more than modifying the tracker itself.



I'm not sure thatt's the right answer here. I'd rather people "star" or vote
on issues by completing a step of the process rather than just clicking a
thumbs up button. Writing a test case or checking that a patch applies on a
particular branch is a vote to me.



I think the fact that it requires no "skin in the game" is a negative point.
I don't show up at government meetings and vote on things -- I don't have
that power. If I want something voted on, I go through a representative and
I tell them my story, my side of things, and show them what I want and why I
want it.

If we just let people vote on things, the first issue that would be created
would be "Remove the GIL" and it would have 10,000 votes and zero patches.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110107/afc502b0/attachment.html>



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Fri, Jan 7, 2011 at 1:14 PM, anatoly techtonik <techtonik at gmail.com> wrote:
..

Anyone can already cast his or her vote by posting a comment with +1
or -1 in it.  Doing so brings the issue to the top of the default view
and gets an e-mail into many developers' mailboxes.   Number of votes
is never a deciding factor on any issue, so tallying them
automatically is rather pointless.  A vote that is accompanied by a
rationale or a patch will always carry greater weight than just a +1.

-1 on the "star system" for the tracker

(Note that some kind of vote/star system is contemplated for the
community documentation.)



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Virgil Dupra
Attributes: []Content: 

On 2011-01-07, at 7:14 PM, anatoly techtonik wrote:


Two things. First, technically, the bug tracker already has "stars". It's the nosy list. You can even run a search by nosy count.

Second, I'm not sure starring matters that much. Ultimately, for something to be done, you need a patch. Sure, sometimes, the patch is going to be made by someone who has no interest in it, but I think most of the time the patch is submitted by someone wanting the patch to be applied. I don't think the number of stars affect the likeliness of a patch being created very much.

Maybe you can point to a google code project for which starring is used intensively and to observably good results?

Virgil Dupras



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Fri, Jan 7, 2011 at 12:14, anatoly techtonik <techtonik at gmail.com> wrote:


This reminds me of my inbox, where I star emails all the time and do
absolutely nothing different to them compared to non-starred emails. I
personally don't see the need for that, so that's a -1 for me.



No solution for this right now, but people have suggested that we add
drop-downs for each module. I'm -0 on that.



You can subscribe to the python-checkins mailing list and create a filter
that looks for whatever you want.



This could probably be solved with a patch to our Roundup instance.



You probably need escalated privileges for this. If you can't change it, you
can always request on the issue that a field be changed.




If you haven't used it yet, then how are you already annoyed...?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110107/4ac534c9/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 7 Jan 2011 13:36:26 -0500
Alexander Belopolsky <alexander.belopolsky at gmail.com> wrote:


I certainly hope casual users don't start posting lots of +1s and -1s
around, though.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 7 Jan 2011 12:39:34 -0600
Brian Curtin <brian.curtin at gmail.com> wrote:

Anatoly is apparently annoyed by a lot of things he never participates
in (such as contributing to Python, for example).

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 

+1
I've been thinking about such a thing as well and I think it would be useful.
Every now and then I go to the bug tracker to see whether the modules
I usually maintain (mainly ftplib, asyncore, asynchat) need some
attention.
I do this by using the plain text search but a select box containing
all the module names would be better.


--- Giampaolo
http://code.google.com/p/pyftpdlib/
http://code.google.com/p/psutil/


2011/1/7 anatoly techtonik <techtonik at gmail.com>:



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Jan 7, 2011 at 10:36 AM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

The tracker on Google Code uses stars. We use this tracker to track
external App Engine issues. It works very well to measure how
widespread a particular issue or need is (even if we don't always fix
the highest-star issues first -- the top issues are "unfixable" like
PHP support :-).

Maybe it works because in that tracker, a star means you get emailed
when the issue is updated; this makes people think twice before
frivolously adding a star. This is not quite the same as the "nosy"
list: adding a star is less work in the UI, you don't have to think up
something meaningful to say, and no email is generated merely because
someone adds or removes a star.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 07.01.2011 19:39, schrieb Brian Curtin:


He *could* also behave reasonable for a while, leading to him being granted
tracker maintenance privileges.

Georg




----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 07/01/2011 19:11, Guido van Rossum wrote:
In our issue tracker it is more or less the same. Adding yourself as 
nosy sends you emails when it is updated and there is a convenient 
button for adding yourself as nosy without having to think up a 
meaningful comment.

The only (sometimes annoying but sometimes useful or interesting) 
difference is that you also get emailed when someone else adds 
themselves as nosy.

Michael


-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 7 Jan 2011 11:11:47 -0800
Guido van Rossum <guido at python.org> wrote:


I'd also mention that many bugzilla installs have a "voting" facility
where people can vote for a limited number of issues of their choice (I
think the number of votes also depends on the user's number of
contributions, although I'm not sure).

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Jan 7, 2011 at 11:15 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

Ah, that must be new -- I didn't realize that. Nice. Now I also want a
button to *remove* myself from the nosy list.

(Of course, a better UI for adding/removing yourself could be a star.
Clicking the star changes your nosy status. It should work
immediately, unlike the existing [+] button.)


Maybe that could be fixed? Then the remaining feature would be a way
to sort issue lists by number of nosy people, and to display the
length of the nosy list.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Jan 7, 2011 at 11:18 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

The latter part sounds like overengineering by geeks too worried about abuse.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 07/01/2011 18:44, Antoine Pitrou wrote:
Well, some indication of how many users this affects may be useful when 
looking at issues to work on.

Launchpad has a button for "this affects me" and you can see how many 
users are affected by an issue (or have declared that at least). Not 
sure if this sends you email, but I'm pretty sure it is different to 
subscribing to an issue - which is nice. Sometimes I care about an issue 
but can neither fix it myself nor want to receive every email from the 
discussion on the issue tracker.

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 07/01/2011 19:22, Guido van Rossum wrote:
It is. Sorry I should have made that clearer.

Me too - but it was considered unnecessary clutter in the UI. I


Right, you still need to submit after clicking [+] at the moment.


Sounds good too me.

Michael

-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Meador Ing
Attributes: []Content: 
On Fri, Jan 7, 2011 at 11:20 AM, anatoly techtonik <techtonik at gmail.com> wrote:

Adding a few new features to the issue tracker isn't going to make the
forgotten changes problems (assuming that it is, indeed, a problem)
that you mentioned magically go away.  Tools alone don't fix problems,
there are people using the tools involved too, and getting people to
use tools effectively is much more difficult.  Adding more features to
a tool that is not be used effectively, just makes it be used even
less effectively.

I speak from recent experiences of helping roll out JIRA to a 50 man
engineering team.  The one regret that I have is that we turned too
many stars, bells, and whistles on instead of helping people create
good issue reports.  Some times there is very good reason to add such
features, but significant amount of data should be there backing that
decision up.  It is better to wait until the data is there pointing to
the problem.

I grabbed the following descriptions from a reply from another part of
this thread:


JIRA has voting, which I have used.  However, it boils back to the
tools vs. people problem.  Enabling voting is useless if no one honors
the votes.  I have seen this happen.  You must have community support.


I have myself wanted this as well before.  However, the downside is
that having more options to select from will inevitably increase the
amount of incorrect selections that are made.  Fewer choices, better
data.  I would rather have better data.


You can just do sorted searches, right?


Are you suggesting that *any* tracker user be allowed to place
arbitrary tags on an issue?  If so, then I think that would be more
confusing as there would be no uniformity to the entries.  I like the
keywords in use on the tracker today better.

-- Meador



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 07/01/2011 19:39, Brian Curtin a ?crit :

I proposed that on
http://wiki.python.org/moin/DesiredTrackerFeatures#new-field-for-module-package
and R. David Murray replied that this had already been brought up and
shot down numerous times on python-dev.  I?ve been unable to find those
threads.

Regards




----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

http://bugs.python.org/issue?@action=search&@columns=title,id,nosy_count&status=1&@sort=-nosy_count

You can create an URL like this through the search form.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 09 Jan 2011 22:52:47 +0100
?ric Araujo <merwok at netwok.org> wrote:


A drop-down would be terribly cumbersome. An input field with realtime
completion would be probably better.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] API refactoring tracker field for Python4
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Antoine Pitrou writes:

 > A drop-down [list of modules] would be terribly cumbersome.

On the XEmacs tracker, we use a multilink with a checkbox list for the
modules field.  This allows you to type in the text field, to check
multiple boxes, and provides input checking.  In my typical usage, I
don't find this cumbersome at all; it's my preferred UI for that field.
(OTOH, I set it up, so my favorite components are all at the top. :-)

The main problem is format of the checkbox page.  By default it only
allows a limited number of entries per page, the limit is pretty low
because its formatted as a single column, and if you switch pages it
"forgets" the entries you've already checked.  This shouldn't be hard
to improve, but I haven't bothered as I have yet to hear a complaint
about it.

 > An input field with realtime completion would be probably better.

Maybe.  I've often been unable to remember the initial letter of a
package, which makes completion difficult. ;-)



