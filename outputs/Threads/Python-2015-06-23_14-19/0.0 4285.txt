
============================================================================
Subject: [Python-Dev] Followup - Re: Bad python 2.5 build on OSX 10.8
	mountain lion
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Followup - Re: Bad python 2.5 build on OSX 10.8
	mountain lion
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
Way back on 2012-10-05 23:45:11 GMT in article 
<nad-7C84D6.16451105102012 at news.gmane.org>, I wrote:


FYI, today Apple finally released OS X 10.8.3, the next maintenance 
release of Mountain Lion, and it does include a recompiled version of 
Python 2.5.6 that appears to solve the sign-extension problem:
2**31-1 is now 2147483647L.

-- 
 Ned Deily,
 nad at acm.org


