
============================================================================
Subject: [Python-Dev] Daemon creation code in the standard library (was:
	Inherance of file descriptor and handles on Windows (PEP 446))
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Daemon creation code in the standard library (was:
	Inherance of file descriptor and handles on Windows (PEP 446))
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Guido van Rossum <guido at python.org> writes:


Work continues on the PEP 3143-compatible ?python-daemon?, porting it to
Python 3 and aiming for inclusion in the standard library.

Interested parties are invited to join us on the discussion forums
<URL:http://lists.alioth.debian.org/cgi-bin/mailman/admin/python-daemon-devel>.

-- 
 \            ?Politics is not the art of the possible. It consists in |
  `\       choosing between the disastrous and the unpalatable.? ?John |
_o__)                                    Kenneth Galbraith, 1962-03-02 |
Ben Finney


