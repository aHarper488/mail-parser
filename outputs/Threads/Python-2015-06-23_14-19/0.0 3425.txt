
============================================================================
Subject: [Python-Dev] GSoC 2012: Python Core Participation?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] GSoC 2012: Python Core Participation?
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

I'm wondering whether Python Core should participate
in GSoC 2012 or not, as core contributors have shown
little interest in acting as mentors in the past.

If you are a core committer and volunteer as GSoC
mentor for 2012, please let me know by Friday
(March 23rd).

Regards,
Martin






----------------------------------------
Subject:
[Python-Dev] GSoC 2012: Python Core Participation?
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Good evening,

There is a number of interesting things to implement in packaging, and
at least one student who manifested their interest, but unfortunately I
am presently unable to say if I?ll have the time to mentor.  If other
core developers would like to act as mentors like happened last year, I
will be available for questions and reviews.

Regards

