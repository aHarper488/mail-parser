
============================================================================
Subject: [Python-Dev] Add Gentoo packagers of external modules to Misc/ACKS
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Add Gentoo packagers of external modules to Misc/ACKS
----------------------------------------
Author: TaeWon
Attributes: []Content: 
Please add the Gentoo packagers of external modules to Misc/ACKS:

Rob Cakebread
Corentin Chary
Ian Delaney
Sebastien Fabbro
Mike Gilbert
Carsten Lohrke
Jan Matejka
Rafael Martins
Patrick McLean
Tiziano M?ller
Dirkjan Ochtman
Bryan ?stergaard
Krzysztof Pawlik
Ali Polatel
Thomas Raschbacher
Jesus Rivero
Lisa M. Seelye
Fernando Serboncini
Jason Shoemaker
Lukasz Strzygowski
Michael Tindal
Alastair Tse
Amadeusz ?o?nowski
Marien Zwart


