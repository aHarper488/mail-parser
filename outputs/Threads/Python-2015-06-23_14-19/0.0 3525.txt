
============================================================================
Subject: [Python-Dev] Accepting PEP 415 (alternative implementation strategy
 for PEP 409's "raise exc from None" syntax)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Accepting PEP 415 (alternative implementation strategy
 for PEP 409's "raise exc from None" syntax)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
As the subject line says, as Guido's delegate, I'm accepting
Benjamin's PEP 415 with the current reference implementation. This PEP
changes the implementation of the new "raise exc from None" syntax to
eliminate the use of Ellipsis as a "not set" sentinel value in favour
of a separate "__suppress_context__" attribute on exceptions. This new
flag defaults to False, but is implicitly set to True whenever a value
is assigned to __cause__ (regardless of whether that happens via
direct assignment , the new syntax or the C API).

The question of how the builtin and standard library exception display
routines should handle the cause where both __cause__ and __context__
are set and __suppress_context__ has been explicitly set to False will
be decided independently of the PEP acceptance (see
http://bugs.python.org/issue14805).

Regards,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

