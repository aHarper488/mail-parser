
============================================================================
Subject: [Python-Dev] path joining on Windows and imp.cache_from_source()
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] path joining on Windows and imp.cache_from_source()
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
imp.cache_from_source() (and thus also imp.source_from_cache()) has special
semantics compared to how os.path.join() works. For instance, if you look
at test_imp you will notice it tries to use the same path separator as is
the farthest right in the path it is given::

  self.assertEqual(imp.cache_from_source('\\foo\\bar/baz/qux.py',
True), '\\foo\\bar\\baz/__pycache__/qux.{}.pyc'.format(self.tag))

But if you do the same basic operation using ntpath, you will notice it
simply doesn't care::

  >>> ntpath.join(ntpath.split('a\\b/c/d.py')[0], '__pycache__',
'd.cpython-32.pyc')
  'a\\b/c\\__pycache__\\d.cpython-32.pyc

Basically imp.cache_from_source() goes to a bunch of effort to reuse the
farthest right separator when there is an alternative separator *before*
and path splitting is done. But if you look at ntpath.join(), it doesn't
even attempt that much effort.

Now that we can reuse os.path.join() (directly for source_from_cache(),
indirectly through easy algorithmic copying in cache_from_source()) do we
want to keep the "special" semantics, or can I change it to match what
ntpath would do when there can be more than one path separator on an OS
(i.e. not do anything special)?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120421/eab3949c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] path joining on Windows and imp.cache_from_source()
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

This goes back to

http://codereview.appspot.com/842043/diff/1/3#newcode787

where Antoine points out that the code needs to look for altsep.

He then suggests "keep the right-most of both". I don't think he
literally meant that the right-most separator should then also be
used to separate __pycache__, but only that the right-most of
either SEP or ALTSEP is what separates the module name.

In any case, Barry apparently took this comment to mean that the
rightmost separator should be preserved.

So I don't think this is an important feature.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] path joining on Windows and imp.cache_from_source()
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 4/21/2012 8:53 PM, Brett Cannon wrote:

Is there an issue here with importing from zip files, which use / 
separator, versus importing from the file system, which on Windows can 
use either / or \ ?  I don't know if imp.cache_from_source cares or is 
aware, but it is the only thing I can think of that might have an impact 
on such semantics.  (Well, the other is command line usage, but I don't 
think you are dealing with command lines at that point.)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120421/7f9ea2e4/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] path joining on Windows and imp.cache_from_source()
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sun, Apr 22, 2012 at 01:44, Glenn Linderman <v+python at g.nevcal.com>wrote:


Right now zipimport doesn't even support __pycache__ (I think). Besides,
zipimport already does a string substitution of os.altsep with os.sep (see
Modules/zipimport.c:90 amongst other places) so it also doesn't care in the
end.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120422/c3c50694/attachment.html>



----------------------------------------
Subject:
[Python-Dev] path joining on Windows and imp.cache_from_source()
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sun, Apr 22, 2012 at 01:45, "Martin v. L?wis" <martin at v.loewis.de> wrote:


OK, then I'll go back to ntpath.join()/split() semantics of caring on split
about altsep but not on join to keep it consistent w/ os.path and what
people are used to.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120422/16343d78/attachment.html>



----------------------------------------
Subject:
[Python-Dev] path joining on Windows and imp.cache_from_source()
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 22 Apr 2012 07:45:27 +0200
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Indeed :-)

Thanks

Antoine.



