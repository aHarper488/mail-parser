
============================================================================
Subject: [Python-Dev] Sphinx issue in What's New in Python 3.3 doc
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Sphinx issue in What's New in Python 3.3 doc
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

In the first example of the "PEP 409: Suppressing exception context"
section, I read "from None...".
http://docs.python.org/dev/whatsnew/3.3.html#pep-409-suppressing-exception-context

It's confusing because I don't remember what was the last choice for
the PEP: None or ... :-)

The reST "code" looks correct in Doc/whatsnew/3.3.rst:

    ...             raise AttributeError(attr) from None
    ...

Victor



----------------------------------------
Subject:
[Python-Dev] Sphinx issue in What's New in Python 3.3 doc
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 26.08.2012 22:16, Victor Stinner wrote:

Hi Victor,

this is fixed in the latest Pygments, and will be fine in the doc once I
update its version used for building.  Until then, you could disable
syntax highlighting on that particular code block.

Georg


