
============================================================================
Subject: [Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
Post Count: 36
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Tue, Jun 28, 2011 at 10:30:14AM +1000, Nick Coghlan wrote:


http://bugs.python.org/issue12491 is the issue to track it.  The
glossary term should give us a stance on what is meant by attributes.

For the other issue (10403), I just concentrated on removing the term
members and used attributes and methods appropriately focussing on
clarity rather than presenting the detail on the object model. For our
rescue, sphinx reST provide :attr: for attribute and :meth: for
methods. :)


-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Jun 27, 2011 at 7:52 AM, Terry Reedy <tjreedy at udel.edu> wrote:

+1 to what Terry said.

"Members" is a historical relic that is best replaced by "attributes"
or "data attributes" if we want to explicitly exclude methods for some
reason. "Methods" is a subset of attributes that explicitly excludes
data attributes.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Mon, 27 Jun 2011 11:32:32 +1000,
Nick Coghlan <ncoghlan at gmail.com> a ?crit :


While I know it is technically right, I find it a bit strange to refer to
methods as "attributes". We're describing an API, not the inner working of
the object model. Also, people just discovering Python will probably be a
bit surprised if we start refer to methods as "attributes".

FWIW, I tend to understand "members" as "methods + attributes", which makes
it a nice term to use for that purpose.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 27 June 2011 09:24, Antoine Pitrou <solipsis at pitrou.net> wrote:

+1


+1

Paul.



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 27/06/2011 09:24, Antoine Pitrou wrote:

That is my understanding / use of the terms as well.

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Oleg Broytma
Attributes: []Content: 
On Mon, Jun 27, 2011 at 10:24:28AM +0200, Antoine Pitrou wrote:

   That's my feeling too.

Oleg.
-- 
     Oleg Broytman            http://phdru.name/            phd at phdru.name
           Programmers don't die, they just GOSUB without RETURN.



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Rob Cliff
Attributes: []Content: 
On 27/06/2011 15:08, R. David Murray wrote:
+1.

'function attributes' ?  'def attributes' ?  Or just stick with 'method 
attributes' ?




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 27/06/2011 15:08, R. David Murray wrote:

Well perhaps, but where does the language draw the distinction between 
attributes and "data attributes" as you all them (a term entirely new to 
me)? Only in the descriptor protocol and that term isn't used there 
(data-descriptors and non data-descriptors is terminology used in the 
documentation there).

If you're saying that data attributes isn't clear either (I couldn't 
quite tell from your email) then how *do* we draw a distinction. We 
could talk about instance attributes, non-descriptor class attributes 
and descriptors, but that terminology requires a reasonably advanced 
understanding of the Python data model.

I don't think that "all members, made up of attributes plus methods" is 
hard to understand. That's a great benefit. The fact that you can 
technically treat methods as attributes too is a minor detail.

All the best,

Michael Foord



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 12:08 AM, R. David Murray <rdmurray at bitdance.com> wrote:

Yep, to me "attribute" just means "something that can be accessed
using attribute notation". What it actually *is* is completely up for
grabs at that point.


It's worse than that - the specific meaning of "members" in the
context of Python's history specifically *excludes* methods.

The superset is "attributes" - as noted, the names of the builtins and
magic methods make that terminology quite explicit.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 12:27 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

It has almost no precedent in the Python context and what precedent it
does have is wrong (since it excluded methods).

And no, the fact that methods can be treated as attributes is not a
minor detail. It is *fundamental* to Python's object model that
*methods are not a special case of attribute access*. All attributes
work the same way, it is just the way functions implement the
descriptor protocol that makes instance methods behave the way they
do.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Nick Coghlan writes:

 > And no, the fact that methods can be treated as attributes is not a
 > minor detail. It is *fundamental* to Python's object model that
 > *methods are not a special case of attribute access*.

That's ambiguous.  I assume you mean "just a case of attribute access,
and not special in any way"?




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
Egads.

Back when I wrote

    "Members and methods" should just be "attributes".

I used quotes to specifically indicate that this applied to the phrase
"members and methods", not their separate use.  I guess I wasn't obvious
enough.

The general & Python-historical uses of "members" is unfortunate.

My current position on this is that we should avoid the use of "members",
because either use will confuse a large set of readers.

As Nick points out, these are all attributes, regardless of their
implementation or type of the value.  "Methods" is a convenient and widely
understood term to refer to a general class of attributes, when that actually
matches the meaning.

For non-method or not-necessarily-a-method attributes, I'm inclined to just
stick with calling them attributes at this point.

Even more important, we need to decide what to call them, and add appropriate
words to the glossary.  And then make the documentation match that.


  -Fred

-- 
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"Give me the luxuries of life and I will willingly do without the necessities."
?? --Frank Lloyd Wright



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Tue, 28 Jun 2011 00:36:20 +1000,
Nick Coghlan <ncoghlan at gmail.com> a ?crit :

Uh, and so what?

Again, the stdlib docs are describing APIs from a high-level viewpoint, not
the Python object model. Just because a method is treated like other
attributes, or a function is treated like other callable objects, doesn't
mean we should replace "method" with "attribute", or "function" with
"callable object", or "class" with "instance of the type type".

I'm -1 on such a change anyway.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 27/06/2011 20:22, R. David Murray wrote:

I think part of the problem is also that *some* people's usage of the 
term doesn't match the precise terminology.

It sounds like if I say "an object's attributes" some of us will expect 
that to include the methods and some of us not. I'd say anecdotally that 
when people talk about object attributes collectively they are *not* 
including methods. When they talk about attribute lookup that rightly 
includes everything.

We do have other terms: instance attributes (which do not include 
methods) and class attributes (which technically do - but is also used 
to indicate attributes set on the class rather than the instance but not 
including methods [1]).

A precise term to describe "attributes that are not methods" would still 
be helpful. I guess the closest we have is "non-descriptors", but that 
requires a knowledge of the descriptor protocol for it to be useful.

Making-things-worse-and-not-better'ly yours,

Michael

[1] I'm talking about *usage* of the term here... I guess usage is 
inconsistent anyway, sometimes people will mean to include methods and 
sometimes not.


-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110627/973dcc44/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 27/06/2011 23:18, Terry Reedy wrote:
So the Python 2.2 what's new talks about attributes and methods as 
different things.... Of course the context makes it clear, but this 
mirrors how I use the terms in discussion and how I see others generally 
using them.

Great topic for bikeshedding. :-)

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
R. David Murray wrote:


"Attribute" also encompasses both instance attributes and class 
attributes. Rather than having two different words, we simply qualify 
the word when we need to distinguish them.

Likewise, in the cases where it is important to distinguish methods from 
other attributes, we should qualify the word: data attribute vs method 
attribute.

(I'm not suggesting that we should routinely refer to "method attribute" 
rather than simply method, but only when we wish to emphasize that 
methods are a kind of attribute and not a completely different kind of 
thing.)



-- 
Steven



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 8:46 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

Yep, as David said, "attribute" is legitimately used to mean *both*
"all attributes (i.e. both data attributes and methods)" and "data
attributes (i.e. excluding methods)". In general, context makes it
clear which meaning is intended, and when that isn't the case, more
verbose phrasing such as that in the previous sentence can make it
explicit.

Rather than fighting that convention, we should probably just confront
the ambiguity head on and update
http://docs.python.org/dev/glossary.html#term-attribute to describe
both uses of the term (and add a separate entry for "data attribute",
with a definition which basically says "attributes which are not
methods").

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Mon, Jun 27, 2011 at 8:11 PM, Ben Finney <ben+python at benfinney.id.au> wrote:

The usage of the object determines what we call it then, so what about
"state attributes" in the same vein as "callable attributes" (data vs.
method).  But it would be nice to have the names consistent across the
different contexts.

ABCMeta tests __isabstractmethod__ on each attribute of a class, not
just the methods, rather that __isabstractattribute__.  Perhaps
calling something a method attribute even when it isn't a function is
still okay.  Thus the pair could be "method and data attributes".
"method attribute" is a little redundant but calling it a "function
attribute" seems less consistent

-eric




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 3:05 PM, Ben Finney <ben+python at benfinney.id.au> wrote:

The two terms I've got out of this thread are "callable attributes"
(instance/static/class methods, etc) and "data attributes" (everything
else). Both seem reasonable to me, creating two largely disjoint sets
that together cover all the different kinds of attribute you're likely
to encounter.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Tue, Jun 28, 2011 at 2:33 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

But "callable attributes" aren't the same thing as methods; most are methods,
but not all.  Sometimes, they're data used by the object.  The fact that
data attributes can be callable is irrelevant.


  -Fred

-- 
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"Give me the luxuries of life and I will willingly do without the necessities."
?? --Frank Lloyd Wright



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/06/2011 11:44, Fred Drake wrote:

Added to which there are other descriptors, notably property, that are 
not directly callable but are not provided as normal "data attributes" 
(although the access syntax is the same). Properties are much closer to 
methods as they are implemented on the class and fetched via the 
descriptor protocol. Instead of "data attributes" I prefer the term 
"instance attributes" although that doesn't include "class attributes" 
(or more precisely it doesn't cover "class attributes that aren't 
descriptors").

The problem with "data attributes" is that it doesn't mean *anything*, 
which I suppose is useful for invented terminology, but it means it 
doesn't convey anything precise to those who haven't heard the term 
before. If it becomes widely used then that changes I guess. I'd still 
normally just use "attributes" though...

All the best,

Michael Foord


-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Tue, Jun 28, 2011 at 6:54 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

Given the availability of __getattr__ and __getattribute__, I consider
properties an implementation detail for some attributes.  The fact that
Python code is called on access is only marginally interesting.


I'd read "data attributes" the same as "non-method attributes".  For readers,
calling them "attributes" is typically sufficient.  It's rare to need to
distinguish them from methods.


  -Fred

-- 
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"Give me the luxuries of life and I will willingly do without the necessities."
?? --Frank Lloyd Wright



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/06/2011 12:04, Fred Drake wrote:

Well, they're *usually* implemented as methods and backed by a real 
instance attribute. Usually (but not always) it makes more sense (IME) 
to group them with methods. The fact that they're *accessed* as an 
attribute is the uninteresting detail.

__getattr__ and __getattribute__ are interesting - they allow you to use 
attribute access syntax for things that *aren't* attributes.

I appreciate the fact that the Python data-model means methods are just 
object attributes, but they're not *instance attributes* and sometimes 
you need to make a distinction. (And yes it is helpful if the standard 
terminology leads people into a better understanding of the Python data 
model, but that still doesn't change the need on occasion for 
terminology that doesn't need to be explained whenever it is used.)

Given that how even methods are to be described depends on the context 
(if you're fetching bound methods as objects then it makes perfect sense 
to just talk about them as attributes) it doesn't seem an area amenable 
to one-size-fits-all terminology.


Yeah, this is all a grand bikeshed. I'm not sure I would understand 
"data attributes" unless it was clear from the context. I would wonder 
what qualifies something as "data".

It is an interesting question what terminology we should use in the 
documentation if we need to distinguish them, but I think that is still 
wandering away from the original question that was posed.

All the best,

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/06/2011 12:51, R. David Murray wrote:

I would use instance attributes for members that are held in the 
instance dict (or have specific slots). As this can't be a normal 
"method" (it could be any object including a callable one - but won't be 
a standard method descriptor) it seems uncontroversial. (Or more to the 
point it seems to be *precise* in its meaning in the context of the 
Python data model.)

What do you mean by "instances can have methods as instance attributes"? 
Once you attach a bound method directly to an instance it becomes a 
slightly different beast I think. (On top of which that is pretty rare 
behaviour.)


Hehe, yeah - that's not bad... It's hard to see how it could be 
misunderstood too.

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 8:54 PM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

In this context, precision is an illusion. There is *no* precise
terminology, not only because Python blurs the boundaries by design in
many respects (callable or not, method or not, attribute or not, etc),
but also because *people* use the same words to mean slightly
different things.

The best we can hope for is to encourage the right way of thinking
about the situation, and in that regard you have the complete set of
attributes accessible via an object (i.e. via __getattribute__), some
of which are callables (and may or may not act like instance methods)
and the remainder of which are data attributes (some of which may
incidentally be callable, even if they aren't used that way). How a
*particular* attribute is classified is not an inherent property of
the attribute, but also an artifact of the way it is used by the
application.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/06/2011 13:31, Nick Coghlan wrote:
"instance members" is precise, (whether or not an attribute is fetched 
"from the instance" is one thing that is clear in the python data model 
- there are just lots of other ways of fetching attributes too). 
"instance members" just doesn't cover all the cases where you may want 
to group attributes together though.

However, providing "__getattr__" and "__getattribute__" is not the 
common case and even properties are *usually* backed by a real instance 
attribute. So "instance attributes" as opposed to other types of 
attributes is *often* a useful distinction.

I don't think "data attributes" is clear or precise. Is a property a 
data attribute (well it depends how it is implemented and what it does), 
is a staticmethod a data attribute (no - but then Tres' question - it 
isn't a normal method either so if you define data attribute to mean 
"all non method attributes" then its potentially an open question).

All the best,

Michael Foord



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Michael Foord wrote:


 >>> class C:
...     def method(self, x):
...             return x+1
...
 >>> c = C()
 >>> c.method = types.MethodType(lambda self, x: x+101, c)
 >>> c.method(1)
102

I don't know how rare it is, but it's a useful trick for customising the 
behaviour of instances.


As I see it, there are three dichotomies we sometimes need to make:


(1) Instance attributes vs class (shared) attributes.

Broadly speaking, whether the attribute is in instance.__dict__ or 
type(instance).__dict__.

(2) Computed vs non-computed attributes.

Attributes which are computed by __getattr__ or via the descriptor 
protocol (which includes properties) are all computed attributes; 
everything else is non-computed.

(3) Method attributes (methods) vs non-method/data attributes.

Broadly speaking, methods are callable, non-method (data) attributes are 
not.


The three are orthogonal: e.g. a staticmethod is a method by virtue of 
being callable, computed by virtue of being generated by a descriptor, 
and a class attribute by virtue of existing in the type __dict__ rather 
than the instance __dict__.

Strictly speaking, (3) is not truly a dichotomy, since functions and 
methods are first class-objects in Python. E.g. one may store a function 
as an attribute with the intention of using it as data rather than as a 
method. But that's a moderately obscure corner case, and in my opinion 
it's not worth obscuring the practical distinction between "methods are 
things you call, data are not" for the sake of it. Leave the 
functions-as-data case for a footnote.



-- 
Steven




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/06/2011 13:56, Michael Foord wrote:

Although to be fair "data attributes" mirrors the terminology for 
descriptors where we have "data descriptors" (which have both __set__ 
and __get__) and "non data descriptors" (__get__ only). Unfortunately 
there are also non-non-data decriptors too (only __set__ and / or 
__delete__) so that terminology is at least slightly confusing / 
imprecise [1]... There was a genuine Python "bug" caused by that 
confusion at one point (unfortunately I forget what it was).

All the best,

Michael

[1] From: http://users.rcn.com/python/download/Descriptor.htm

     If an object defines both __get__ and __set__, it is considered a 
data descriptor. Descriptors that only define __get__ are called 
non-data descriptors (they are typically used for methods but other uses 
are possible).




-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/06/2011 14:20, Steven D'Aprano wrote:
Right - that method is an instance attribute.

Technically also via __getattribute__ when overridden.

Yep, useful summary.

Michael




-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 10:56 PM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

"callable attributes" and "data attributes" are purely about the API
exposed by the object in question. If we're going for object model
neutral terminology, that's the only view that makes sense. They're
descriptions about how attributes are intended to be *used* that are
completely silent on the question of how they're *implemented*. So
staticmethod would fall into the first group, while property would
fall into the latter.

differently, so it makes sense to worry about class attributes,
instance attributes, dynamic attributes, etc. (the class vs instance
distinction can also matter to some degree from the usage point of
view, since it affects the scope of any mutable attributes, and the
static vs dynamic distinction can also matter, especially for
introspection purposes).

This goes back to the original point about all of this being highly
context dependent - how you carve up the set of all attributes is
going to change based on what you're trying to explain (e.g. the
distinction between enforced 'data' descriptors, descriptors that
allow shadowing in the instance dict, class attributes that aren't
descriptors at all, instance attributes and dynamic attributes
retrieved via __getattr__ is another way of dividing them)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jun 28, 2011 at 11:27 PM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

Since object.__getattribute__ is the hook that implements the entire
attribute lookup protocol, *all* attributes are technically retrieved
via __getattribute__ (which is why overriding it correctly can be such
a PITA). That's also the hook type() overrides to make class attribute
lookup differ from ordinary instance lookup.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Tue, Jun 28, 2011 at 5:35 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

While the distinction between class-specific attributes and
instance-specific is important, I don't think merging it with the
method/data distinction is as helpful.  The valuable information here
is the expectation of how the attributes are used.  In my mind, that
boils down to data (holds static or dymanic state) and methods (does
something on the class or instance).

-eric




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28/06/2011 16:23, Terry Reedy wrote:

So what is the difference between "Instance method" and "Method" above? 
Is it just that "Method" is broader and includes class methods and bound 
methods?

If anyone said "instance method" to me I would assume they meant bound 
method. (A normal method fetched from an instance.)

All the best,

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Tue, Jun 28, 2011 at 7:20 AM, Steven D'Aprano <steve at pearwood.info> wrote:

For terminology, is it important that data attributes are [usually]
not callable, or is it that being callable is not relevant to their
use as attributes of the class/instance?  The same for "methods".

We have precedent for where the the terminology represents an
expectation rather than a firm constraint (__isabstractmethod__).


+1

The "three dichotomies" is a great way to look at it.

-eric




----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/28/2011 11:35 AM, Michael Foord wrote:


Before 2.2, these were the only methods.


Since 2.2, yes. The current glossary entry starts

"method A function which is defined inside a class body."

This includes 'staticmethods', but as I have said, I think that is a 
mistake. Static methods are functions without special method treatment. 
A class staticmethod function act the same as any other function.
Also, 'defined inside' is not necessary.

I would change the above to "A function that get bound to a instance or 
class when called as an attribute of the instance or class. Methods are 
usually defined inside a class body."


The result of accessing an instance or class method via an instance or 
class. Accessing a static method does not create a bound method. Bound 
methods are usually anonymous and ephemeral, being used for one call and 
then deleted.


Instance methods are the 'permanent' class function attributes, not the 
ephemeral object that implements a.b(c). Bound methods would be an 
implementation detail, except that a.b has to evaluate to something and 
saving bound methods is quite handy when calling a method or methods 
repeatedly on the same instance.

Functools.partial is a generalization of bound methods, which were 
Python's first (special-case) implementation of the partial function 
idea. Leaving out implementation details, if b is a function attribute 
of type(a), 'a.b' is an abbreviated way of writing 
'functools.partial(type(a).b,a)' (and yes, I have tested an example of 
this).


---
Terry Jan Reedy



----------------------------------------
Subject:
[Python-Dev] Issue10403 - using 'attributes' instead of members
 in documentation
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Jun 28, 2011 at 06:28, Nick Coghlan <ncoghlan at gmail.com> wrote:


These two terms also have the benefit of being understandable by non Python
programmers. Since we can't rely on people read the glossary or any other
specific doc before reading the stdlib docs we need to make sure that the
terminology that we use can be understood by newbies. Thus I like data and
callable attributes since it makes sense even if you come from another
programming language since you can easily reason it out.

And now I am done adding my graffiti to the shed.

-Brett


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110628/1a3fb3f7/attachment.html>

