
============================================================================
Subject: [Python-Dev] [ANN] Python 2.5.6 Release Candidate 1
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [ANN] Python 2.5.6 Release Candidate 1
----------------------------------------
Author: =?ISO-8859-15?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On behalf of the Python development team and the Python community, I'm
happy to announce the release candidate 1 of Python 2.5.6.

This is a source-only release that only includes security fixes. The
last full bug-fix release of Python 2.5 was Python 2.5.4. Users are
encouraged to upgrade to the latest release of Python 2.7 (which is
2.7.1 at this point).

This releases fixes issues with the urllib, urllib2, SimpleHTTPServer,
and audiop modules. See the release notes at the website (also
available as Misc/NEWS in the source distribution) for details of bugs
fixed.

For more information on Python 2.5.6, including download links for
various platforms, release notes, and known issues, please see:

    http://www.python.org/2.5.6

Highlights of the previous major Python releases are available from
the Python 2.5 page, at

    http://www.python.org/2.5/highlights.html

Enjoy this release,
Martin

Martin v. Loewis
martin at v.loewis.de
Python Release Manager
(on behalf of the entire python-dev team)



----------------------------------------
Subject:
[Python-Dev] [ANN] Python 2.5.6 Release Candidate 1
----------------------------------------
Author: Leo Ja
Attributes: []Content: 
Hi,

I think the release date of 2.5.6c1 should be 17-Apr-2011, instead of
17-Apr-2010
http://www.python.org/download/releases/2.5.6/NEWS.txt

On Mon, Apr 18, 2011 at 5:57 AM, "Martin v. L?wis" <martin at v.loewis.de> wrote:


--
Best Regards,
Leo Jay



----------------------------------------
Subject:
[Python-Dev] [ANN] Python 2.5.6 Release Candidate 1
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

Thanks, fixed.

Martin

