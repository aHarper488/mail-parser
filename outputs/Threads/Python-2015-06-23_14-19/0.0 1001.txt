
============================================================================
Subject: [Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 


On 11/23/2010 5:43 PM, ?ric Araujo wrote:

I used Notepad to edit the file, TortoiseSvn to commit, the same as I 
did for #9222, rev86702, Lib\idlelib\IOBinding.py, yesterday.
If the latter is OK, perhaps *.py gets filtered better than misc. text 
files. I believe I have the config as specified in dev/faq.

[miscellany]
enable-auto-props = yes

[auto-props]
* = svn:eol-style=native
*.c = svn:keywords=Id
*.h = svn:keywords=Id
*.py = svn:keywords=Id
*.txt = svn:keywords=Author Date Id Revision

Terry




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Nov 23, 2010 at 15:07, Terry Reedy <tjreedy at udel.edu> wrote:

Adding the BOM will be an editor thing, not a svn thing. Doing a
Google search for [ms notepad bom] shows that Notepad did the
"helpful", invisible edit.

-Brett





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 11/24/2010 2:04 PM, Brett Cannon wrote:


So I presume it did the same with IOBinding.py. Does *.py get filtered 
is a way that could be extended to no-extention files? Do *.txt files 
get BOM filtered off? Should all text files in repository have some 
extension (default .txt)?

More to the point, can better filtering be added to the new hg 
repository? Or can a local Windows hg setup have such filtering on local 
commits before pushing?

I know now that I could always edit with IDLE's editor, but it is a lot 
easier to right click and select edit than it is to run thru the 
directory tree in an open dialog. And of course, since the pseudo-BOM 
addition is undocumented within notepad itself, and probably other 
editors, it is easy to not know.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 24.11.2010 20:25, schrieb Terry Reedy:

Of course it can; it's just a matter of writing the respective hooks.
What we *can* do in any case is to check for UTF-8 "BOMs" server-side
in the whitespace checking hook.


It should show up as an invisible change in the first line of a file when you
look at a "svn diff".  (It is a very good practice to look at a diff before
committing anyway.)

Georg





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 11/24/2010 3:04 PM, Georg Brandl wrote:



It does show up, and yes I agree. That should be in dev/faq if not already

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 11/24/2010 5:13 PM, "Martin v. L?wis" wrote:

Or it somehow got removed from the .py file. I tried with another .py 
file (and reverted!) and the diff showed the invisible change to the 
first line that Georg predicted.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86720 -
	python/branches/py3k/Misc/ACKS
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Nov 25, 2010 at 5:25 AM, Terry Reedy <tjreedy at udel.edu> wrote:

If you want a decent free text editor on Windows, the open source
Notepad++ does a very nice job. It also adds an "Edit with Notepad++"
to the explorer context menu :)


As far as the implicit BOM addition itself goes, reindent.py and
reindent-rst.py could probably be updated to check for it, but the
miscellaneous files (like ACKS) are likely to continue to need manual
checks.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

