
============================================================================
Subject: [Python-Dev] Dev In a Box: Pumped for a better UX (with video)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Dev In a Box: Pumped for a better UX (with video)
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Hi,

If you don't know, Dev In a Box is "everything you need to contribute to
Python in under 700 MB".  I've patched it up to the latest standards
of colorless console user interfaces and uploaded a video of the process
for you to enjoy.

http://www.youtube.com/watch?v=jbJcI9MnO_c

This tool can be greatly improved to provide entrypoint for other healthy
activities. Like improving docs by editing, comparing, building and sending
patches for review. Specialized menus can greatly help with automating
common tasks, which are not limited by sources fetching.


https://bitbucket.org/techtonik/devinabox
-- 
anatoly t.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120203/dab47934/attachment.html>

