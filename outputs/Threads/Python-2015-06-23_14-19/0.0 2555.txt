
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Enhance
 Py_ARRAY_LENGTH(): fail at build time if the argument is not an array
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Enhance
 Py_ARRAY_LENGTH(): fail at build time if the argument is not an array
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/9/28 victor.stinner <python-checkins at python.org>:

Do we really need a new file? Why not pyport.h where other compiler stuff goes?


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Enhance
 Py_ARRAY_LENGTH(): fail at build time if the argument is not an array
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
On Wed, Sep 28, 2011 at 8:07 PM, Benjamin Peterson <benjamin at python.org> wrote:

...


I would expect pyport to contain only system-specific macros.  These
seem more universal.

-jJ



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Enhance
 Py_ARRAY_LENGTH(): fail at build time if the argument is not an array
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Thu, Sep 29, 2011 at 2:45 AM, Victor Stinner
<victor.stinner at haypocalc.com> wrote:

Not sure about the other two, but Py_ARITHMETIC_RIGHT_SHIFT is
definitely platform dependent, which is why it's in pyport.h in the
first place.

Mark

