
============================================================================
Subject: [Python-Dev] 2.7 releases
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: Thomas Helle
Attributes: []Content: 
Will there be more 2.7 bugfix releases, and when the next one?

In other words; if I submit a patch and it is accepted, can I
expect that patch be committed also to the 2.7 branch?

Thanks,
Thomas

Been a long time that I've been here - but still using Python (2.7 now)
more and more...




----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Thu, Jul 26, 2012 at 10:56 AM, Thomas Heller <theller at ctypes.org> wrote:

We are still back-porting bug fixes to 2.7 branch. Which means that yes, we
may definitely see a release.
Benjamin might be right person to answer the question on when.

Thanks,
Senthil
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120726/4a1d99cb/attachment.html>



----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

Yes.


That's up for Benjamin to decide. My view is that one bugfix
release every year is more than enough.


Don't you have commit rights still?

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: Thomas Helle
Attributes: []Content: 
Am 26.07.2012 20:16, schrieb martin at v.loewis.de:

Ok.  I expect we will still be using 2.7 next year in my company.


I dont't know.  IIRC, I have asked for them to be retracted some years
ago.  Anyway, I do know nearly nothing about hg and don't have time to
learn it.

So, I have uploaded a patch and asked for review (since I'm not 1000%
sure that it is absolutely correct):

http://bugs.python.org/issue15459

Thomas





----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 7/26/2012 2:50 PM, Thomas Heller wrote:


The tracker thinks you do. That is what the Python logo next to your 
name means.


I started from nothing too. TortoiseHG and its GUI Workbench makes the 
basics pretty easy.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: Meador Ing
Attributes: []Content: 
On Thu, Jul 26, 2012 at 1:50 PM, Thomas Heller <theller at ctypes.org> wrote:


I'll take a look at this in the next few days.

-- Meador



----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/7/26 Thomas Heller <theller at ctypes.org>:

Probably late fall or early 2013.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] 2.7 releases
----------------------------------------
Author: MRA
Attributes: []Content: 
On 27/07/2012 20:12, Benjamin Peterson wrote:
Or, for those in the southern hemisphere, late spring or early 2013.

