
============================================================================
Subject: [Python-Dev] cpython: issue9584: Add {} list expansion to glob.
 Original patch by Mathieu Bridon
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: issue9584: Add {} list expansion to glob.
 Original patch by Mathieu Bridon
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 11/06/2012 02:56 PM, tim.golden wrote:

Needs a versionchanged.

In any case, brace expansion is not part of globbing (see the bash or zsh
manuals) because it does not generate valid file names, and it is a non-POSIX
expansion of some shells.  Are you sure it should be put into the glob module?
(Not speaking of the backward incompatibility it creates.)

Georg




----------------------------------------
Subject:
[Python-Dev] cpython: issue9584: Add {} list expansion to glob.
 Original patch by Mathieu Bridon
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 11/08/2012 09:43 PM, Georg Brandl wrote:

Sorry, just saw the reversion.  Never mind, my concerns have been addressed.

Georg




----------------------------------------
Subject:
[Python-Dev] cpython: issue9584: Add {} list expansion to glob.
 Original patch by Mathieu Bridon
----------------------------------------
Author: Tim Golde
Attributes: []Content: 
On 08/11/2012 20:43, Georg Brandl wrote:

I backed it out very soon afterwards, Georg. It had had some (quite a 
bit of) discussion on the issue, but I'd messed up the patch somehow and 
the backwards compat issue was raised pretty much immediately by Serhiy. 
So I pulled the commit.

TJG

Insofar as

