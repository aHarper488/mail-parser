
============================================================================
Subject: [Python-Dev] Rename time.steady(strict=True) to time.monotonic()?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Rename time.steady(strict=True) to time.monotonic()?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

time.steady(strict=True) looks to be confusing for most people, some
of them don't understand the purpose of the flag and others don't like
a flag changing the behaviour of the function.

I propose to replace time.steady(strict=True) by time.monotonic().
That would avoid the need of an ugly NotImplementedError: if the OS
has no monotonic clock, time.monotonic() will just not exist.

So we will have:

- time.time(): realtime, can be adjusted by the system administrator
(manually) or automatically by NTP
- time.clock(): monotonic clock on Windows, CPU time on UNIX
- time.monotonic(): monotonic clock, its speed may or may not be
adjusted by NTP but it only goes forward, may raise an OSError
- time.steady(): monotonic clock or the realtime clock, depending on
what is available on the platform (use monotonic in priority). may be
adjusted by NTP or the system administrator, may go backward.

time.steady() is something like:

try:
  return time.monotonic()
except (NotImplementError, OSError):
  return time.time()

time.time(), time.clock(), time.steady() are always available, whereas
time.monotonic() will not be available on some platforms.

Victor



----------------------------------------
Subject:
[Python-Dev] Rename time.steady(strict=True) to time.monotonic()?
----------------------------------------
Author: Jim J. Jewet
Attributes: []Content: 


In http://mail.python.org/pipermail/python-dev/2012-March/118024.html
Steven D'Aprano wrote:


It is best-effort for steady, but putting "best" in the name would
be an attractive nuisance.



That would still be worth doing.  But I think the main point is
that the clock *should* be monotonic, and *should* be as precise
as possible.

Given that it returns seconds elapsed (since an undefined start),
perhaps it should be

    time.seconds()

or even

    time.counter()

-jJ

-- 

If there are still threading problems with my replies, please 
email me with details, so that I can try to resolve them.  -jJ


