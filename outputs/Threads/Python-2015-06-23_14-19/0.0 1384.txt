
============================================================================
Subject: [Python-Dev] Tip for hg merge
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Tip for hg merge
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi,


Here?s a useful tip: instead of merging pulled changesets with your
branch, do the reverse.  That is:

$ hg pull
$ hg heads .  # get only heads for the checked-out branch
$ hg up other-head
$ hg merge

Now instead of merging unknown code into your checkout, you will merge
the code added by your unpushed changesets to the other code.  If you?re
using a three-way file merge tool, it is your code that will be in the
?other? pane, not the unknown code.

Regards

