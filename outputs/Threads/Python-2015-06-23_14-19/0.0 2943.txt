
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (2.6): ensure no one
 tries to hash things before the random seed is found
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.6): ensure no one
 tries to hash things before the random seed is found
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Feb 22, 2012 at 2:24 AM, benjamin.peterson
<python-checkins at python.org> wrote:

Won't this trigger in the -Wd case that led to the PyStr_Fini workaround?

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.6): ensure no one
 tries to hash things before the random seed is found
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Feb 22, 2012 at 8:49 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Never mind, just saw the later series of checkins that fixed it.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

