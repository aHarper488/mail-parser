
============================================================================
Subject: [Python-Dev] bbreport
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] bbreport
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

Ezio and Florent are developing a tool called bbreport to collect buildbot 
results and generate short reports to the command line. It's possible to 
filter results by Python branch, builder name, etc. I send patches to link 
failed tests to existing issues to see quickly known failures vs new failures. 
This tool becomes really useful to analyze buildbot results!

http://code.google.com/p/bbreport/

bbreport requires Python trunk (2.7) and color output only works on UNIX/BSD 
OS (ie. not Windows).

-- 
Victor Stinner
http://www.haypocalc.com/



----------------------------------------
Subject:
[Python-Dev] bbreport
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Sat, Apr 17, 2010 at 7:41 PM, Victor Stinner
<victor.stinner at haypocalc.com> wrote:

Seconded.  I've been using this for a few days, and found it
especially useful to be able to get a quick summary of exactly *which*
tests are failing on the various buildbots.


Does it really need trunk?  I've been running it under 2.6 without
problems, but I probably haven't explored all the options fully.

Mark



----------------------------------------
Subject:
[Python-Dev] bbreport
----------------------------------------
Author: Florent Xiclun
Attributes: []Content: 

Now it works on 2.6 (or 2.5+pysqlite).

Regards,

-- 
Florent



----------------------------------------
Subject:
[Python-Dev] bbreport
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le samedi 17 avril 2010 20:41:10, Victor Stinner a ?crit :

I realized that most issues come from Windows and Mac. Can't we just turn off 
these buildbots?

-- 
Victor Stinner
http://www.haypocalc.com/



----------------------------------------
Subject:
[Python-Dev] bbreport
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Victor Stinner wrote:

o.O?

Remember there's no tone of voice in email... smileys matter when joking :)

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

