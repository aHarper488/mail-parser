
============================================================================
Subject: [Python-Dev] Generating "canonical argument call" from `getargspec`
 and `getcallargs` results
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Generating "canonical argument call" from `getargspec`
 and `getcallargs` results
----------------------------------------
Author: cool-R
Attributes: []Content: 
I was happy to see the new `getcallargs` function in the `inspect` module.
But there's one thing I want to do that's related to it, and maybe this was
implemented somewhere or someone can give me some pointers about it.

I want to have a function that takes the results of `getargspec` and
`getcallargs` for a function and a set of arguments, and outputs the
"canonical argument call" that would generate these results.

For example, let's say I have a function `f`:

    def f(a, b, c=7, **kwargs):
        pass

This is its argument spec:

    >>> inspect.getargspec(f)
    ArgSpec(args=['a', 'b', 'c'], varargs=None, keywords='kwargs',
defaults=(7,))

Now, different argument combinations can generate the same results
`getcallargs` result

    >>> inspect.getcallargs(f, 1, 2, 7, meow='grrr') # Like calling f(1, 2,
7, meow='grrr')
    {'a': 1, 'c': 7, 'b': 2, 'kwargs': {'meow': 'grrr'}}

    >>> inspect.getcallargs(f, 1, 2, meow='grrr') # Like calling f(1, 2,
meow='grrr')
    {'a': 1, 'c': 7, 'b': 2, 'kwargs': {'meow': 'grrr'}}

    >>> inspect.getcallargs(f, 1, b=2, c=7, meow='grrr') # Like calling f(1,
b=2, c=7, meow='grrr')
    {'a': 1, 'c': 7, 'b': 2, 'kwargs': {'meow': 'grrr'}}

What I would like to have is one canonical argument combination. I assume
that the best way to define the canonical form is "The smallest number of
arguments, and specifying as few keyword arguments as possible." According
to that definition, the canonical argument call in the above example would
be the second one, `f(1, 2, meow='grrr')`.

So I want a function that for a given function and argument call, returns
the canonical argument call, which generates the same `getcallargs` result
as the given argument call.

The reason I want this is to make it easier to organize a set of argument
calls. (I'm doing an application in which the user maintains a set of
argument profiles that he can choose from, and having to deal with only the
canonical form will prevent some annoyances.)

Did anyone implement something like this? Does anyone have ideas? And is
this something that would be of interest to the standard library?


Best Wishes,
Ram Rachum.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100824/97f3913f/attachment.html>

