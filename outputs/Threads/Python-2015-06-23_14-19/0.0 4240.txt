
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #18081:
 Workaround "./python -m test_idle test_logging" failure
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #18081:
 Workaround "./python -m test_idle test_logging" failure
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
This fix is still not correct as warnings.formatwarning should not be
overridden, only showwarning can be.
On Jun 24, 2013 6:18 PM, "victor.stinner" <python-checkins at python.org>
wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130625/28f21240/attachment.html>

