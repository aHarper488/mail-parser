
============================================================================
Subject: [Python-Dev] Proposal: make float.__str__ identical
 to	float__repr__ in Python 3.2
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Proposal: make float.__str__ identical
 to	float__repr__ in Python 3.2
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 29/07/2010 19:47, Mark Dickinson wrote:
+1

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.



