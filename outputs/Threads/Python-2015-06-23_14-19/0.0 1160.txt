
============================================================================
Subject: [Python-Dev] test_grp regression test fails with NIS entries present
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] test_grp regression test fails with NIS entries present
----------------------------------------
Author: Bobby Impolloni
Attributes: []Content: 
The regression tests for py3k (or, I think, any branch) fail on one of
my machines because test_grp chokes if /etc/group contains a "+" line,
which is a directive to pull information from NIS.

The test enumerates all entries in /etc/group using grp.getgrall() and
verifies that it can look up each entry by name using grp.getgrnam().
The current behavior of grp.getgrall() is to return entries whose
names start with plus or minus signs (NIS-related lines) as if they
were regular entries. The result is that the test tries to look up the
name "+" and fails because no entry is found.

It turns out that a bug on this issue has existed since 2003:
http://bugs.python.org/issue775964
The bug originally indicated that the problem is specific to Red Hat,
but that is not the case because I ran into it on Debian Squeeze.
According to a comment on the bug, this syntax in the group file has
been deprecated for a long time, which is why the issue rarely comes
up.

I believe the right thing to do at this point is to keep the behavior
of grp.getgrall(), but document that it will return NIS-related lines
along with regular entries and to modify the test to not try to look
up those entries with grp.getgrnam().

I've attached a patch to the bug that makes these changes and results
in the test passing. Is it possible that someone can review/ checkin
the patch? Thanks.

