
============================================================================
Subject: [Python-Dev] Fwd: Fixing zipfile.BadZipfile to zipfile.BadZipFile
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Fwd: Fixing zipfile.BadZipfile to zipfile.BadZipFile
----------------------------------------
Author: =?UTF-8?Q?Bo=C5=A1tjan_Mejak?
Attributes: []Content: 
Forwarded conversation
Subject: Fixing zipfile.BadZipfile to zipfile.BadZipFile
------------------------

From: *Bo?tjan Mejak* <bostjan.mejak at gmail.com>
Date: Fri, Oct 15, 2010 at 11:02 PM
To: python-dev at python.org


I am very glad you're reorganizing the Standard Library. Thumbs up! I
hope everything will comply to PEP 8 after you're done.

Since you're reorganizing, I have my own contribution. I have attached
a patch. The issue7351 <http://bugs.python.org/issue7351> was not
accepted at the time, so I hope you'll accept this fix now.

My point is that every class name in the module zipfile is like this:
- LargeZipFile
- ZipFile
- PyZipFile
.

.

.

So apply my patch to make the class name BadZipfile consistent to
other class names in the zipfile module and let it be named
BadZipFile. Thank you.



Best regards,

Bo?tjan Mejak


----------
From: *?ric Araujo* <merwok at netwok.org>
Date: Sun, Oct 17, 2010 at 11:00 PM
To: python-dev at python.org
Cc: Bo?tjan Mejak <bostjan.mejak at gmail.com>


Hello

(A bit of context: The original message comes from bug #2775, ?Implement
PEP 3108?, a meta-bug tracking stdlib reorganization for py3k.)
You may have missed the timeline: Most of the PEP 3108 changes have been
done before the first 3.x release went out.  Now that we have 3.1 out as
a stable and supported, we cannot reorganize and break compatibility
anymore.

(A note about PEP 8 compliance: Module names have been mostly fixed, but
not all function/method names, for example in logging and unittest.  If
I recall correctly, readability did not seem to make all the rewrites
worth it.)
I?ve just re-read the answers there and they are still valid.

Ezio and me: ?Your patch need to include an alias (BadZipfile =
BadZipFile) to preserve compatibility with old pickles, as explains
msg95477.?

Antoine: ?I don't think changing it for the sake of aesthetics is a good
deal given that many existing programs will have to be converted to the
new spelling.?

Regards


----------
From: *Bo?tjan Mejak* <bostjan.mejak at gmail.com>
Date: Mon, Oct 18, 2010 at 9:25 AM
To: ?ric Araujo <merwok at netwok.org>


Shoot. Well, too bad. I thought Python is all about readability, but I think
you developers don't take it very seriously. If I was a developer, I would
certainly go to the trouble of all the rewrittes of
package/module/class/method/function names that do not comply to PEP 8 and
have them done by the time the first sub-version of Python 3 would be
released.

----------
From: *?ric Araujo* <merwok at netwok.org>
Date: Mon, Oct 18, 2010 at 2:14 PM
To: Bo?tjan Mejak <bostjan.mejak at gmail.com>


Hi

Could you repost your message to the mailing list, so that other people
can see it and eventually react?  Thanks.

(P.S. please make sure not to top-post on the mailing list)

Regards


----------
From: *?ric Araujo* <merwok at netwok.org>
Date: Mon, Oct 18, 2010 at 2:45 PM
To: python-dev at python.org


Hello

[Sorry if this comes twice, connection errors here]
_______________________________________________
Python-Dev mailing list
Python-Dev at python.org
http://mail.python.org/mailman/listinfo/python-dev
Unsubscribe:
http://mail.python.org/mailman/options/python-dev/bostjan.mejak%40gmail.com

----------
From: *Nick Coghlan* <ncoghlan at gmail.com>
Date: Mon, Oct 18, 2010 at 3:13 PM
To: ?ric Araujo <merwok at netwok.org>
Cc: python-dev at python.org


Correct. We went through this for one module that I recall (threading)
and that was annoying enough that we mostly left things alone after
that unless they were truly obnoxious.

For threading we were able to clean a lot of things up in the process
(such as adding properties where appropriate), but even so, we still
made sure all the old names continued to work.

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia

----------
From: *Bo?tjan Mejak* <bostjan.mejak at gmail.com>
Date: Mon, Oct 18, 2010 at 4:36 PM
To: Nick Coghlan <ncoghlan at gmail.com>


Then please make an alias for this custom BadZipfile exception class.
Add  BadZipfile = BadZipFile  in the zipfile module.


----------
From: *Bo?tjan Mejak* <bostjan.mejak at gmail.com>
Date: Tue, Oct 19, 2010 at 10:34 PM
To: Nick Coghlan <ncoghlan at gmail.com>


Sorry, let me correct myself:
 Add  BadZipFile = BadZipfile in the zipfile module.

Please add the above assignment statement in the right place to the zipfile
module and create an alias. Thank you.

----------
From: *Bo?tjan Mejak* <bostjan.mejak at gmail.com>
Date: Wed, Oct 20, 2010 at 11:13 PM
To: Nick Coghlan <ncoghlan at gmail.com>


Ah, leave it as is. If in the future you will be able to rename BadZipfile
to BadZipFile, please do.

----------
From: *?ric Araujo* <merwok at netwok.org>
Date: Wed, Oct 27, 2010 at 6:26 PM
To: python-dev <python-dev at python.org>
Cc: Bo?tjan Mejak <bostjan.mejak at gmail.com>


[Reply received off-list quoted here]

Le 18/10/2010 09:25, Bo?tjan Mejak a ?crit :
Readability has to be balanced with other important things:
maintainability, discoverability, compatibility, rapidity and other
things ending in -ity.  I?m personally glad that readability gets a huge
place in the language definition itself (no braces, standard
indentation), I follow 98 % of PEP 8 where I can, and have learned to
accept that sometimes, a piece of code in the standard lib won?t get
more readable.  Still beats some languages that I won?t name because
dissing Java is too easy.  <wink>
You are free to break compat in your code but it python-dev has a duty
to its users.  Renames in threading have been deemed a good thing,
renames in unittest not.  You can read the python-3000 archives to get
an idea of the years of work and thousands of messages that went into
py3k.  At some point, a release had to be done.  Now that there is a
stable release in the 3.x line, compatibility rules apply.

Practicality beats purity; now is better than never.

Regards


----------
From: *Bo?tjan Mejak* <bostjan.mejak at gmail.com>
Date: Wed, Oct 27, 2010 at 7:13 PM
To: ?ric Araujo <merwok at netwok.org>


Since Python 3.2 accepts feature requests, take this fix as a feature
request. Please forget about preserving the compatibility with old pickles.

----------
From: *?ric Araujo* <merwok at netwok.org>
Date: Wed, Oct 27, 2010 at 7:16 PM
To: Bo?tjan Mejak <bostjan.mejak at gmail.com>


Again, please send a copy to your messages to the mailing list (choose
?reply to all? or ?reply to list?), and don?t leave the whole message
after your reply.  Thanks.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101027/893799a1/attachment-0001.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: zipfile-patch.diff
Type: application/octet-stream
Size: 3786 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101027/893799a1/attachment-0001.obj>

