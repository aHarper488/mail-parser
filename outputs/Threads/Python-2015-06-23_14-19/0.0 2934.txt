
============================================================================
Subject: [Python-Dev] hash randomization in the 2.6 branch
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] hash randomization in the 2.6 branch
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
I've just committed a back port of issue 13703, the hash randomization patch,
to the Python 2.6 branch.  I have left the forward porting of this to Python
2.7 to Benjamin.

test_json will fail with randomization enabled since there is a sort order
dependency in the __init__.py doctest.  I'm not going to fix this (it can't be
fixed in the same way 2.7 can), but I'd gladly accept a patch if it's not too
nasty.  If not, then it doesn't bother me because we previously agreed that it
is not a showstopper for the tests to pass in Python 2.6 with randomization
enabled.

Please however, do test the Python 2.6 branch thoroughly, both with and
without randomization.  We'll be coordinating release candidates between all
affected branches fairly soon.

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120220/62963aaf/attachment.pgp>

