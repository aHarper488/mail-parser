
============================================================================
Subject: [Python-Dev] bugs.python.org not responding (Was: rlcompleter
	-- auto-complete dictionary keys (+ tests))
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] bugs.python.org not responding (Was: rlcompleter
	-- auto-complete dictionary keys (+ tests))
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 07 Nov 2010 09:30:17 -0800, Bobby Impollonia <bobbyi at gmail.com> wrote:

The hosting company working on the problem, which seems to be a hardware
issue.  Hopefully be resolved soon.

FYI bugs.python.org and www.python.org are different machines, and
in fact the two machines are not even hosted at the same location.

Valery,  I would advise you to submit the patch to bugs.python.org when
it comes back up.  Patches posted to this mailing list will in general
just get forgotten.

--
R. David Murray                                      www.bitdance.com

