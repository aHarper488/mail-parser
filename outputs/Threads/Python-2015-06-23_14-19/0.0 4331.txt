
============================================================================
Subject: [Python-Dev] Google Summer of Code - Organization Deadline
	Approaching - March 29
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Google Summer of Code - Organization Deadline
	Approaching - March 29
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
Just an FYI that there are under 3 days to apply to Google Summer of
Code for mentoring organizations:
http://www.google-melange.com/gsoc/homepage/google/gsoc2013. The
student application deadline is later on in May.

If you run a project that is interested in applying under the Python
umbrella organization, contact Terri Oda at terri at zone12.com

Is anyone here interested in leading CPython through GSOC? Anyone have
potential students to get involved, or interested in being a mentor?

