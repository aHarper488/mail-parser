
============================================================================
Subject: [Python-Dev] PEP 385: Auditing
Post Count: 11
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
I recently set up a Mercurial hosting solution myself, and noticed that
there is no audit trail of who had been writing to the "master" clone.
There are commit messages, but they could be fake (even misleading to a
different committer).

The threat I'm concerned about is that of a stolen SSH key. If that is
abused to push suspicious changes into the repository, it is really
difficult to find out whose key had been used.

The solution I came up with is to define an "incoming" hook on the
repository which will log the SSH user along with the pack ID of the
pack being pushed.

I'd like to propose that a similar hook is installed on repositories
hosted at hg.python.org (unless Mercurial offers something better
already). Whether or not this log should be publicly visible can be
debated; IMO it would be sufficient if only sysadmins can inspect it in
case of doubt.

Alterntively, the email notification sent to python-checkins could could
report who the pusher was.

Dirkjan: if you agree to such a strategy, please mention that in the PEP.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Martin v. L?wis <martin <at> v.loewis.de> writes:

This sounds reasonable, assuming it doesn't disclose any private information.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Rafael Villar Burk
Attributes: []Content: 
Antoine Pitrou <solipsis <at> pitrou.net> writes:


There are already made solutions for that, as the pushlog hooks used by Mozilla,
OpenJDK and others.

Mozilla's pushlog can be seen here:

http://hg.mozilla.org/mozilla-central/pushloghtml

And its code is avaliable here:
http://hg.mozilla.org/users/bsmedberg_mozilla.com/hgpoller/file/tip/pushlog-feed.py

Dirkjan is its author, so I suppose he was already thinking about having a
similar hook for Python repos.

Regards,

Rafael




----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

This seems to just be the code that generates the feed, out of a
database pushlog2.db that somehow must be created. So where is the code
to actually fill that database?

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Rafael Villar Burke (Pach
Attributes: []Content: 


On 13/02/2010 15:25, "Martin v. L?wis" wrote:
There's some more content here: 
http://hg.mozilla.org/users/bsmedberg_mozilla.com/hgpoller/file/tip
But I don't use it myself, just knew about its existance. Surely Dirkjan 
can make all the pieces fit nicely :).

Rafael



----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Rafael Villar Burke (Pach
Attributes: []Content: 
On 13/02/2010 16:03, Rafael Villar Burke (Pachi) wrote:
The hook code looks like it's here: 
http://hg.mozilla.org/users/bsmedberg_mozilla.com/hghooks/file/tip
The previous repository link is the hgwebdir integration code.

Regards,

Rafael



----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Sat, Feb 13, 2010 at 12:53, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Having a pushlog and/or including the pusher in the email sounds like
a good idea, I'll add something to that effect to the PEP. I slightly
prefer adding it to the commit email because it would seem to require
less infrastructure, and it can be handy at times to know who pushed
something right off the bat.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 13.02.2010 13:19, schrieb Antoine Pitrou:

How could it disclose more than the SVN hook does today (i.e. who is working
on the repo right now)?

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.




----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 13.02.2010 18:52, schrieb Dirkjan Ochtman:

+1.

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.




----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Georg Brandl wrote:

It could reveal email addresses, for example, which in turn would
attract spammers. However, I assume that Antoine was bringing up privacy
just as a general concern, and didn't really expect any specific issue.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 385: Auditing
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Martin v. L?wis <martin <at> v.loewis.de> writes:
could
information.
working

That's right. Thanks for de-obfuscating me :)

Regards

Antoine.


