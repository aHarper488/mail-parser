
============================================================================
Subject: [Python-Dev] packaging was not getting installed
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] packaging was not getting installed
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
I just fixed Makefile.pre.in to install the packaging directory and all its
subdirs.  Without this `python3.3 -c 'import packaging'` fails from the
installation target directory.  I'm not sure the Fellowship intends for every
subdir to get installed, so please double check.  I just added everything that
came from `find Lib/packaging -type d`.

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110614/1ef6ad3b/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] packaging was not getting installed
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <20110614165414.340f2138 at neurotica.wooz.org>,
 Barry Warsaw <barry at python.org> wrote:

http://bugs.python.org/issue12313

-- 
 Ned Deily,
 nad at acm.org


