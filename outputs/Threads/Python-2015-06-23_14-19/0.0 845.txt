
============================================================================
Subject: [Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 20/05/2010 00:42, Giampaolo Rodol? wrote:

+1 - I've seen many newbies confused by this error and I sometimes do a 
double-take myself.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
----------------------------------------
Author: John Arbash Meine
Attributes: []Content: 
Giampaolo Rodol? wrote:

Because you wouldn't want to have

A.echo()

Say that it takes 1 argument and (-1 given) ?

John
=:->




----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
John Arbash Meinel wrote:


Something like "1 argument in addition to 'self'" would be
reasonably clear and would cover both situations.

+1 on fixing this from me, too. Even when you understand
exactly what's going on, it's annoying having to make the
mental adjustment every time.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 20/05/2010 10:49, Giampaolo Rodol? wrote:

Although the pattern of calling an unbound method with an instance as 
the first argument (self) is not uncommon - and if you're doing 
Class.method() that not only looks like what you're doing but is also an 
accurate error message.

I would also expect that accidentally calling unbound methods is a much 
less common error than calling bound methods with the wrong number of 
arguments...

All the best,

Michael Foord



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/20/2010 4:02 AM, Floris Bruynooghe wrote:


In 3.x, there are no unbound method objects, just functions. But that 
should make the difference *easier* to detect, as bound/unbound method 
objects were actually the same class, differing only in an attribute.

I notice
 >>> list.append()
Traceback (most recent call last):
   File "<pyshell#0>", line 1, in <module>
     list.append()
TypeError: descriptor 'append' of 'list' object needs an argument

So here the message is specific to the type of the callable.




----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Ben Finney wrote:


That's true, but the use of the word 'self' here isn't meant
to refer to the name of a parameter. The message is aimed at
the caller of the function, who doesn't necessarily know or
care what the parameter is actually called.

The important thing is that it represents the object the method
is being called for, and 'self' is the traditional term used
when talking about that. I can't think of anything that would
be more accurate without being excessively verbose or pedantic.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message when
 passing the wrong number of arguments to a method
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Giampaolo Rodol? wrote:


It's hard to see how this could be improved. If you had been
intending to call an unbound method, the comment about arguments
would have been relevant -- you were supposed to supply one
but didn't. Python is unable to read your mind and figure out
that you really meant something else altogether.

BTW, unbound methods no longer exist in Py3, so you would get
the standard message about incorrect number of arguments
instead. Not sure whether that's better or worse in this
situation.

-- 
Greg

