
============================================================================
Subject: [Python-Dev] cpython (2.7): #16004: Add `make touch`.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #16004: Add `make touch`.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 11 Mar 2013 08:14:24 +0100 (CET)
ezio.melotti <python-checkins at python.org> wrote:

Shouldn't that be mentioned / explained / documented somewhere?
It doesn't look obvious in which circumstances it could be useful.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #16004: Add `make touch`.
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
Hi,

On Mon, Mar 11, 2013 at 3:28 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

It will be documented in http://bugs.python.org/issue15964
(SyntaxError in asdl when building 2.7 with system Python 3).

Best Regards,
Ezio Melotti


