
============================================================================
Subject: [Python-Dev] Why does TemporaryDirectory not wait for `__enter__`?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Why does TemporaryDirectory not wait for `__enter__`?
----------------------------------------
Author: cool-R
Attributes: []Content: 
Hello,

I noticed that the `TemporaryDirectory` context manager creates the folder
on `__init__` rather than on `__enter__`, resulting in complexity, bugs, and
hackarounds in `__del__`. I assume there's a good reason for this decision.
What is it?


Thanks,
Ram.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110226/04010e8d/attachment.html>

