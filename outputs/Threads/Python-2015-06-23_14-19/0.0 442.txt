
============================================================================
Subject: [Python-Dev] Mercurial repository for Python benchmarks
Post Count: 17
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Collin Winte
Attributes: []Content: 
Hey Dirkjan,

Would it be possible for us to get a Mercurial repository on
python.org for the Unladen Swallow benchmarks? Maciej and I would like
to move the benchmark suite out of Unladen Swallow and into
python.org, where all implementations can share it and contribute to
it. PyPy has been adding some benchmarks to their copy of the Unladen
benchmarks, and we'd like to have as well, and Mercurial seems to be
an ideal solution to this.

Thanks,
Collin Winter



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
Hi Collin (and others),

On Sun, Feb 21, 2010 at 15:28, Collin Winter <collinwinter at google.com> wrote:

Just a repository on hg.python.org?

Sounds good to me. Are you staying for the sprints? We'll just do it.
(Might need to figure out some hooks we want to put up with it.)

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Collin Winte
Attributes: []Content: 
On Sun, Feb 21, 2010 at 3:31 PM, Dirkjan Ochtman <djc.ochtman at gmail.com> wrote:

Yep, that's all we want. I'll be around for the sprints through
Tuesday, sitting at the Unladen Swallow sprint.

Collin Winter



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Daniel Stutzbac
Attributes: []Content: 
On Sun, Feb 21, 2010 at 2:28 PM, Collin Winter <collinwinter at google.com>wrote:


If and when you have a benchmark repository set up, could you announce it
via a reply to this thread?  I'd like to check it out.
--
Daniel Stutzbach, Ph.D.
President, Stutzbach Enterprises, LLC <http://stutzbachenterprises.com>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100221/4d67db54/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
Hello.

We probably also need some people, besides CPython devs having some
access to it (like me).

Cheers,
fijal

On Sun, Feb 21, 2010 at 4:51 PM, Daniel Stutzbach
<daniel at stutzbachenterprises.com> wrote:



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Sun, Feb 21, 2010 at 21:31, Maciej Fijalkowski <fijall at gmail.com> wrote:

Right. I've setup a public repository on hg.python.org:

http://hg.python.org/benchmarks/

Right now, I still need to have Martin change some configuration so I
will be able to set up push access for people other than me, so it's
pull-only for now. I've already sent an email to Martin to help me get
this fixed, so it should be fixed soon. Let me know if there are any
issues.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Collin Winte
Attributes: []Content: 
Hey Daniel,

On Sun, Feb 21, 2010 at 4:51 PM, Daniel Stutzbach
<daniel at stutzbachenterprises.com> wrote:

Will do.

In the meantime, you can read
http://code.google.com/p/unladen-swallow/wiki/Benchmarks to find out
how to check out the current draft of the benchmarks, as well as which
benchmarks are currently included.

Thanks,
Collin Winter



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/2/21 Dirkjan Ochtman <djc.ochtman at gmail.com>:

I think we should probably develop a policy about hg.python.org repos
before we start handing them out. Who will be able to have a repo on
hg.python.org? What kinds of projects?


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Sun, Feb 21, 2010 at 22:21, Benjamin Peterson <benjamin at python.org> wrote:

I'd be happy to host stuff for people who are already Python
committers, and limit it to stuff that would otherwise live somewhere
in Python's svn repository.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/2/21 Dirkjan Ochtman <dirkjan at ochtman.nl>:

+1 Sounds like a good starting place.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Benjamin Peterson wrote:

This is pretty much the same approach we use for creating subdirectories
of /sandbox on the SVN side so it sounds reasonable to me too.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Collin Winte
Attributes: []Content: 
On Sun, Feb 21, 2010 at 9:43 PM, Collin Winter <collinwinter at google.com> wrote:

The benchmarks repository is now available at
http://hg.python.org/benchmarks/. It contains all the benchmarks that
the Unladen Swallow svn repository contains, including the beginnings
of a README.txt that describes the available benchmarks and a
quick-start guide for running perf.py (the main interface to the
benchmarks). This will eventually contain all the information from
http://code.google.com/p/unladen-swallow/wiki/Benchmarks, as well as
guidelines on how to write good benchmarks.

If you have svn commit access, you should be able to run `hg clone
ssh://hg at hg.python.org/repos/benchmarks`. I'm not sure how to get
read-only access; Dirkjan can comment on that.

Still todo:
- Replace the static snapshots of 2to3, Mercurial and other hg-based
projects with clones of the respective repositories.
- Fix the 2to3 and nbody benchmarks to work with Python 2.5 for Jython and PyPy.
- Import some of the benchmarks PyPy has been using.

Any access problems with the hg repo should be directed to Dirkjan.
Thanks so much for getting the repo set up so fast!

Thanks,
Collin Winter



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Thomas Wouter
Attributes: []Content: 
On Mon, Feb 22, 2010 at 21:17, Collin Winter <collinwinter at google.com>wrote:


It would be http://hg.python.org/benchmarks (http, not ssh; no username; no
'/repos' toplevel directory.)





-- 
Thomas Wouters <thomas at python.org>

Hi! I'm a .signature virus! copy me into your .signature file to help me
spread!
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100222/68ccef73/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Mon, Feb 22, 2010 at 15:42, Thomas Wouters <thomas at python.org> wrote:

Correct.

Another todo is to get commit mails; I'm currently working on that.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Collin Winte
Attributes: []Content: 
On Mon, Feb 22, 2010 at 3:17 PM, Collin Winter <collinwinter at google.com> wrote:

We now have a "Benchmarks" component in the bug tracker. Suggestions
for new benchmarks, feature requests for perf.py, and bugs in existing
benchmarks should be reported under that component.

Thanks,
Collin Winter



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Wed, Feb 24, 2010 at 12:12 PM, Dave Fugate <dfugate at microsoft.com> wrote:

run all of them on nightly run for example (we as in PyPy). Are you up
to adhering to perf.py expectation scheme? (The benchmark being
runnable by perf.py)

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Mercurial repository for Python benchmarks
----------------------------------------
Author: Dave Fugat
Attributes: []Content: 
perf.py - I'll look into this.  At this point we'll need to refactor them any ways as there are a few dependencies on internal Microsoft stuff the IronPython Team didn't create.

Thanks,

Dave

-----Original Message-----
From: Maciej Fijalkowski [mailto:fijall at gmail.com] 
Sent: Wednesday, February 24, 2010 10:51 AM
To: Dave Fugate
Cc: python-dev at python.org
Subject: Re: [Python-Dev] Mercurial repository for Python benchmarks

On Wed, Feb 24, 2010 at 12:12 PM, Dave Fugate <dfugate at microsoft.com> wrote:

From my perspective the more we have there the better. We might not
run all of them on nightly run for example (we as in PyPy). Are you up
to adhering to perf.py expectation scheme? (The benchmark being
runnable by perf.py)

Cheers,
fijal


