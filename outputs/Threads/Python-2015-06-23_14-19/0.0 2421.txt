
============================================================================
Subject: [Python-Dev] Python Core Tools
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python Core Tools
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Hello,

I've stumbled upon Dave Beazley's article [1] about trying ancient GIL
removal patch at
http://dabeaz.blogspot.com/2011/08/inside-look-at-gil-removal-patch-of.html
and looking at the output of Python dis module thought that it would
be cool if there were tools to inspect, explain and play with Python
bytecode. Little visual assembler, that shows bytecode and disassembly
side by side and annotates the listing with useful hints (like
interpreter code optimization decisions). That will greatly help many
new people understand how Python works and explain complicated stuff
like GIL and stackless by copy/pasting pictures from there. PyPy has a
tool named 'jitviewer' [2] that may be that I am looking for, but the
demo is offline.

But even without this tool I know that speakers at conferences create
various useful scripts to gather interesting stats and visualize
Python internals. I can name at least 'dev in a box' [3] project to
get people started quickly with development, but I am sure there many
others exist. Can you remember any tools that can be useful in Python
core development? Maybe you use one every day? I'd like to compile a
list of such tools and put in to Wiki to allow people have some fun
with Python development without the knowledge of C.


1. http://dabeaz.blogspot.com/2011/08/inside-look-at-gil-removal-patch-of.html
2. http://morepypy.blogspot.com/2011/08/visualization-of-jitted-code.html
3. http://hg.python.org/devinabox/
--
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Python Core Tools
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Sun, Oct 2, 2011 at 5:02 AM, anatoly techtonik <techtonik at gmail.com> wrote:

I put demo back online.

https://bitbucket.org/pypy/pypy/src/59460302c713/lib_pypy/disassembler.py

this might be of interest. It's like dis module except it creates
objects instead of printing them




----------------------------------------
Subject:
[Python-Dev] Python Core Tools
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Sun, Oct 2, 2011 at 8:05 AM, Maciej Fijalkowski <fijall at gmail.com> wrote:

It's just that SimpleHTTPServer doesn't quite survive slashdot effect.
Where do I fill a bug report :)

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Python Core Tools
----------------------------------------
Author: renau
Attributes: []Content: 
Maciej Fijalkowski <fijall <at> gmail.com> writes:


I think that Issue11816 (under review) aims at extending the dis module in a
similar direction:
Refactor the dis module to provide better building blocks for bytecode analysis
http://bugs.python.org/issue11816

renaud



