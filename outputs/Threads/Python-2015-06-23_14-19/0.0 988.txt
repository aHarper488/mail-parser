
============================================================================
Subject: [Python-Dev] Re-enable warnings in regrtest and/or unittest
Post Count: 11
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
I would like to re-enable by default warnings for regrtest and/or unittest.

The reasons are:
   1) these tools are used mainly by developers and they (should) care 
about warnings;
   2) developers won't have to remember that warning are silenced and 
how to enable them manually;
   3) developers won't have to enable them manually every time they run 
the tests;
   4) some developers are not even aware that warnings have been 
silenced and might not notice things like DeprecationWarnings until the 
function/method/class/etc gets removed and breaks their code;
   5) another developer tool -- the --with-pydebug flag -- already 
re-enables warnings when it's used;

If this is fixed in unittest it won't be necessary to patch regrtest.
If it's fixed in regrtest only the core developers will benefit from this.

This could be fixed checking if any warning flags (-Wx) are passed to 
python.
If no flags are passed the default will be -Wd, otherwise the behavior 
will be the one specified by the flag.
This will allow developers to use `python -Wi` to ignore errors explicitly.

Best Regards,
Ezio Melotti



----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: =?UTF-8?B?xYF1a2FzeiBMYW5nYQ==?
Attributes: []Content: 
Am 22.11.2010 18:14, schrieb Ezio Melotti:

+1

Especially in regrtest it could help manage stdlib quality (currently we 
have a horde of ResourceWarnings, zipfile mostly). I would even be +1 on 
making warnings errors for regrtest but that seems to be unpopular on 
#python-dev.

Best regards,
?ukasz Langa



----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 22/11/2010 17:35, ?ukasz Langa wrote:

Enabling it for regrtest makes sense. For unittest I still think it is a 
choice that should be left to developers.

Michael



-- 

http://www.voidspace.org.uk/

READ CAREFULLY. By accepting and reading this email you agree,
on behalf of your employer, to release me from all obligations
and waivers arising from any and all NON-NEGOTIATED agreements,
licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap,
confidentiality, non-disclosure, non-compete and acceptable use
policies (?BOGUS AGREEMENTS?) that I have entered into with your
employer, its partners, licensors, agents and assigns, in
perpetuity, without prejudice to my ongoing rights and privileges.
You further represent that you have the authority to release me
from any BOGUS AGREEMENTS on behalf of your employer.




----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
On 22/11/2010 19.45, Michael Foord wrote:

As I said on IRC I think it makes sense to turn them into errors once we 
fixed/silenced all the ones that we have now. That would help keeping 
the number of warning to 0.


If we consider that most of the developers want to see them, I'd prefer 
to have the warnings by default rather than having to use -Wd explicitly 
every time I run the tests (keep in mind that many developers out there 
don't even know/remember that now they should use -Wd).






----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Nov 22, 2010 at 10:58, Ezio Melotti <ezio.melotti at gmail.com> wrote:

I agree.


The problem with that is it means developers who switch to Python 3.2
or whatever are suddenly going to have their tests fail until they
update their code to turn the warnings off. Then again, if we make the
switch for this dead simple to add and backwards-compatible so that
turning them off doesn't trigger an error in older versions then I am
all for turning warnings on by default.

Another approach is to have unittest's runner, when run in verbose
mode, print out what the warnings filter is set to so developers are
aware that they are silencing warnings.

-Brett




----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Nov 22, 2010 at 11:24 AM, Brett Cannon <brett at python.org> wrote:

That sounds like a feature to me... :-)

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 22/11/2010 21:08, Guido van Rossum wrote:
I think Ezio was suggesting just turning warnings on by default when 
unittest is run, not turning them into errors. Ezio is suggesting that 
developers could explicitly turn warnings off again, but when you use 
the default test runner warnings would be shown. His logic is that 
warnings are for developers, and so are tests...

Michael

-- 

http://www.voidspace.org.uk/

READ CAREFULLY. By accepting and reading this email you agree,
on behalf of your employer, to release me from all obligations
and waivers arising from any and all NON-NEGOTIATED agreements,
licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap,
confidentiality, non-disclosure, non-compete and acceptable use
policies (?BOGUS AGREEMENTS?) that I have entered into with your
employer, its partners, licensors, agents and assigns, in
perpetuity, without prejudice to my ongoing rights and privileges.
You further represent that you have the authority to release me
from any BOGUS AGREEMENTS on behalf of your employer.




----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Nov 22, 2010 at 13:08, Guido van Rossum <guido at python.org> wrote:

=) I meant update their tests with the switch to turn off the
warnings, not update to make the warnings properly disappear.

I guess it's a question of whether it will be errors by default or
simply output the warning. I can get behind printing the warnings by
default and adding a switch to make them errors or off otherwise.

-Brett




----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: =?utf-8?Q?=C5=81ukasz_Langa?
Attributes: []Content: 
Wiadomo?? napisana przez Michael Foord w dniu 2010-11-22, o godz. 23:01:


Then again, he is not against the idea to turn those warnings into errors, at least for regrtest.

If you agree to do that for regrtest I will clean up the tests for warnings. Already did that for zipfile so it doesn't raise ResourceWarnings anymore. I just need to correct multiprocessing and xmlrpc ResourceWarnings, silence some DeprecationWarnings in the tests and we're all set. Ah, I see a couple more with -uall but nothing scary.

Anyway, I find warnings as errors in regrtest a welcome feature. Let's make it happen :)

-- 
Best regards,
?ukasz Langa
tel. +48 791 080 144
WWW http://lukasz.langa.pl/

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101123/e6554e6f/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Nadeem Vawd
Attributes: []Content: 
2010/11/23 ?ukasz Langa <lukasz at langa.pl>:

There are also some in test_socket - I've submitted a patch on
Roundup: http://bugs.python.org/issue10512

Looking at the multiprocessing warnings, they seem to be caused by
leaks in the underlying package, unlike xmlrpc and socket, where it's
just a matter of the test code neglecting to close the connection.  So
+1 to:


Nadeem



----------------------------------------
Subject:
[Python-Dev] Re-enable warnings in regrtest and/or unittest
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Nov 23, 2010 at 8:01 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

Having at least the default test runner change the default warnings
behaviour to -Wd (while still respecting sys.warnoptions) sounds like
a good idea. That way users won't see the warnings (as intended with
that change), but developers are less likely to get nasty surprises
when things break in future releases (which was one of our major
concerns when we made the decision to change the default handling of
DeprecationWarning). A similar change may be appropriate for doctest
as well.

Printing out the list of suppressed warnings in verbose mode may also be useful.

A blanket -We is unlikely to work for the test suite, since generating
warnings on some platforms is expected behaviour (e.g. due to the
ongoing argument between multiprocessing and FreeBSD as to the
appropriate behaviour of semaphores). However, we may be able to get
to the point where it is run that way by default and then affected
tests use check_warnings() to alter the filter configuration
(something that many such affected tests already do).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

