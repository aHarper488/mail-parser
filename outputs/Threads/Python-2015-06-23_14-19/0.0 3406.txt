
============================================================================
Subject: [Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 03/15/2012 01:17 AM, victor.stinner wrote:

This is not clear to me.  Why wouldn't it raise OSError on error even with
strict=False?  Please clarify which exception is raised in which case.

Georg




----------------------------------------
Subject:
[Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 03/17/2012 09:47 PM, Michael Foord wrote:

So errors are ignored when strict is false?

Georg




----------------------------------------
Subject:
[Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
On 3/17/2012 4:47 PM, Michael Foord wrote:


I have to agree with Georg. Looking at the code, it appears OSError can
be raised with both strict=True and strict=False (since floattime() can
raise OSError). The text needs to make it clear OSError can always be
raised.

I also think "By default, if strict is False" confuses things. If
there's a default behavior with strict=False, what's the non-default
behavior? I suggest dropping "By default".

Eric.



----------------------------------------
Subject:
[Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

This is an old bug in floattime(): I opened the issue #14368 to remove
the unused exception. In practice, it never happens (or it is *very*
unlikely today). IMO it's a bug in floattime().


I agree, I replaced it by "By default,".

Victor



----------------------------------------
Subject:
[Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

Said differently: time.steady(strict=True) is always monotonic (*),
whereas time.steady() may or may not be monotonic, depending on what
is avaiable.

time.steady() is a best-effort steady clock.

(*) time.steady(strict=True) relies on the OS monotonic clock. If the
OS provides a "not really monotonic" clock, Python cannot do better.
For example, clock_gettime(CLOCK_MONOTONIC) speed can be adjusted by
NTP on Linux. Python tries to use clock_gettime(CLOCK_MONOTONIC_RAW)
which doesn't have this issue.

Victor



----------------------------------------
Subject:
[Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
I believe we should make a monotonic_time method that assures monotonicity
and be done with it. Forward steadiness can not be guaranteed. No
parameters.
On Mar 20, 2012 2:56 PM, "Steven D&apos;Aprano" <steve at pearwood.info> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120320/08533aa7/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython: Issue #10278: Add an optional strict
 argument to time.steady(), False by default
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

That's why the function name was changed from time.monotonic() to
time.steady(strict=True). If you want to change something, you should
change the documentation to list OS limitations.


Correct, most Python modules exposing OS functions are thin wrappers
and don't add any magic. When we need a higher level API, we write a
new module: like shutil enhancing the os module.

Victor

