
============================================================================
Subject: [Python-Dev] Enhanced tracker privileges for dangerjim to
	do?triage.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to
	do?triage.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
A.M. Kuchling <amk <at> amk.ca> writes:

I'd say that we usually notice them, since we process their patches or read
their reviews and comments. Unless perhaps they're called "John Smith" or "Jean
Dupont" (if French).

Regards

Antoine.



