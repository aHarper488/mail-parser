
============================================================================
Subject: [Python-Dev] Can Python implementations reject semantically
 invalid expressions?
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Can Python implementations reject semantically
 invalid expressions?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I'd say "no". Any syntactically correct module should start executing,
and type errors are only a runtime concept.

If you were to reject code at startup more restrictively, you might
end up rejecting the standard library, as it contains syntax errors
in code that isn't being imported normally (test/badsyntax*).

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Can Python implementations reject semantically
 invalid expressions?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Stefan Behnel wrote:


It would have to be

    raise TypeError("Exactly the message that would have been produced at run time")

That might be acceptable, but then you have to ask, is it really
worth performing this optimisation? The overhead of raising and
handling the exception is likely to completely swamp that of
executing the original code.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Can Python implementations reject semantically
 invalid expressions?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Craig Citro wrote:


If producing different bytecode were considered a reason
against performing an optimisation, then no code optimisations
would be permissible at all!

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Can Python implementations reject semantically
 invalid expressions?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Steven D'Aprano wrote:

That might break code that was deliberately trying to raise
an exception. Sometimes you see things like

   try:
     1/0
   except Exception, e:
     ...

Usually this kind of thing is only done in test code or
illustrative snippets, but even so, it should work as
expected.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Can Python implementations reject semantically
 invalid expressions?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Craig Citro wrote:


They might not intend to execute the code at all -- e.g.
they may want to pass the compiled code to dis() to find
out what bytecode gets generated. Having it refuse to
compile would be annoying in that case.

-- 
Greg

