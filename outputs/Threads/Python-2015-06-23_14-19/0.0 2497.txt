
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue 13227: Option to
 make the lru_cache() type specific (suggested by Andrew
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue 13227: Option to
 make the lru_cache() type specific (suggested by Andrew
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Oct 21, 2011 at 1:57 AM, raymond.hettinger
<python-checkins at python.org> wrote:

I've been pondering this one a bit since reviewing it on the tracker,
and I'm wondering if we have the default behaviour the wrong way
around.

For "typed=True":
  - never results in accidental type coercion and potentially wrong
answers* (see below)
  - cache uses additional memory (each entry is larger, more entries
may be stored)
  - additional cache misses
  - differs from current behaviour

For "typed=False" (current default):
  - matches current (pre-3.3) behaviour
  - can lead to accidental type coercion and incorrect answers

I only just realised this morning that the existing (untyped) caching
behaviour can give answers that are *numerically* wrong, not just of
the wrong type. This becomes clear once we use division as our test
operation rather than multiplication and bring Decimal into the mix:

... def divide(x, y):
...     return x / y
...
1.1111111111111112
Decimal('1.111111111111111111111111111')
1.1111111111111112
1.1111111111111112

At the very least, I think lru_cache should default to typed behaviour
in 3.3, with people being able to explicitly switch it off as a cache
optimisation technique when they know it doesn't matter. You could
even make the case that making the cache type aware under the hood in
3.2 would be a bug fix, and the only new feature in 3.3 would be the
ability to switch off the type awareness to save memory.

Regards,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

