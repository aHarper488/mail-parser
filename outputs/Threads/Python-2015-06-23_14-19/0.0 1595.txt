
============================================================================
Subject: [Python-Dev] [Python-checkins] r88395 -
 python/branches/py3k/Lib/asyncore.py
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88395 -
 python/branches/py3k/Lib/asyncore.py
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le vendredi 11 f?vrier 2011 ? 14:52 +0100, Giampaolo Rodol? a ?crit :

dispatcher.closing is a public attribute: some programs my rely on it. I
checked mine: it uses "connected", but not closing :-)

I think that it will be fine for Python 3.3, but not for 3.2 (too late).
And you should document your change, because it is the public API.

Victor




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88395 -
 python/branches/py3k/Lib/asyncore.py
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Nick Coghlan wrote:


So maybe it's time to design a new module with a better API
and deprecate the old one?

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88395 -
 python/branches/py3k/Lib/asyncore.py
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Antoine Pitrou wrote:


I was thinking of something lighter-weight than that.

-- 
Greg




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88395 -
 python/branches/py3k/Lib/asyncore.py
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 


Oh, I agree with you. -1 on any new externally maintained library.


Of course. asyncore's problem is not that its a maintenance burden, it's
that it's really subpar compared to everything else out there.
That said, Giampaolo has committed to taking it forward, so perhaps the
3.3 version of asyncore will be much (?) better.


Well, of course. Or at least that's the theory. In practice, the algebra
of open source licenses is quite well-known and non-copyleft code
usually can be combined freely without any worries.
(and do you think the zlib authors signed a contributor agreement for
inclusion in Python distributions? :-))


"IP management process"? What is that horrible jargon supposed to
mean? :)
I don't think the Twisted people are into legalese, and I've never
signed an agreement when contributing (admittedly little) code to
Twisted. They did relicense Twisted once (from LGPL to MIT-like), but
that probably means they asked every past contributor.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88395 -
 python/branches/py3k/Lib/asyncore.py
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
exarkun at twistedmatrix.com wrote:

I just had a look at the docs for Twisted Core, and it lists
10 sub-modules. The only one that really looks "core" to me
is twisted.internet. Drilling into that reveals another
39 public sub-sub-modules and 10 private ones.

Sorry, but you'll have to chop it back quite a bit more than
that before it's focused enough to be a stlib module, I think.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88395 -
 python/branches/py3k/Lib/asyncore.py
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Giampaolo Rodol? wrote:


My thoughts exactly -- from a bird's eye view, Twisted appears
to be very far from simple. While there may be some good ideas
to adopt from it, I suspect that finding them will require just
as much careful thought as designing an API from scratch.

-- 
Greg

