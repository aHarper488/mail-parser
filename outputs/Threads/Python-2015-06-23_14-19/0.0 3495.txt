
============================================================================
Subject: [Python-Dev] [Python-checkins] peps: Update PEP 1 to better
 reflect current practice
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Update PEP 1 to better
 reflect current practice
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
Thanks for doing this update Nick.  I have just a few comments.

On May 05, 2012, at 02:57 PM, nick.coghlan wrote:


While I certainly don't mind (in fact, prefer) those with commit privileges to
just go ahead and commit their PEP to the repo, I'd like for there to be
*some* communication with the PEP editors first.  E.g. sanity checks on the
basic format or idea (was this discussed on python-ideas first?), or
reservation of PEP numbers.

When you do contact the PEP editors, please also specify whether you have
commit privileges or not.  It's too hard to remember or know who has those
rights, and too much hassle to look them up. ;)

OTOH, I'm also happy to adopt an EAFP style rather than LBYL, so that the PEP
editors can re-assign numbers or whatever after the fact.  We've done this in
a few cases, and it's never been that much of a problem.

Still, core developers needn't block (for too long) on the PEP editors.


I'd reword this to something like the following:

    The final authority for the PEP approval is the BDFL.  However, the BDFL
    may delegate the final approval authority to a "PEP czar" for that PEP.
    This happens most frequently with PEPs where the BDFL has granted approval
    in principle for *something* to be done, and in agreement with the general
    proposals of the PEP, but there are details that need to be worked out
    before the final PEP can be approved.  When an `PEP-Czar` header must be
    added to the PEP to record this delegation.  The format of this header is
    the same as the `Author` header.

This leave out the whole self-nomination text, which I think isn't very
relevant to the official addition of the czar role (sadly, no clever bacronym
has come to mind, and BDFOP hasn't really taken off ;).


Or just run "make" on systems that have that handy convenience. :)

Cheers,
-Barry

(Nick, if you agree with these changes, please just go ahead and make them.)



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Update PEP 1 to better
 reflect current practice
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 05, 2012, at 12:56 PM, Barry Warsaw wrote:


s/When an/A/

-Barry



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Update PEP 1 to better
 reflect current practice
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 06, 2012, at 03:08 PM, Nick Coghlan wrote:


The diff looks good, thanks.

-Barry

