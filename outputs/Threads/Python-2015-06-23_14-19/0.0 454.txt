
============================================================================
Subject: [Python-Dev] contributor to committer
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Florent Xiclun
Attributes: []Content: 
Hello,

I am a semi-regular contributor for Python: I have contributed many patches
since end of last year, some of them were reviewed by Antoine.
Lately, he suggested that I should apply for commit rights.

Some of the accepted patches:
 - Fix refleaks in py3k branch (#5596)
 - Extend stringlib fastsearch for various methods of bytes, bytearray
   and unicode objects (#7462 and #7622)
 - Various documentation patches, including FAQ

Recently, I worked with Ezio to fix some tests and to silence py3k warnings
in the test suite (#7092). And I am in touch with Fredrik to release
ElementTree 1.3 and port it to Python 2.7 (#6472).

As a final note, I would like to highlight a small script in the same spirit
as Pyflakes: pep8.py
I've contributed few patches for the version 0.5, released a week ago:
http://pypi.python.org/pypi/pep8/

-- 
Florent Xicluna





----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Wed, 24 Feb 2010 12:13:10 +0000, Florent Xicluna <florent.xicluna at gmail.com> wrote:

+1

--David



----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Alexandre Vassalott
Attributes: []Content: 
On Wed, Feb 24, 2010 at 7:13 AM, Florent Xicluna
<florent.xicluna at gmail.com> wrote:

+1

-- Alexandre



----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 

Another +1. :)

-- 
Senthil
Every living thing wants to survive.
		-- Spock, "The Ultimate Computer", stardate 4731.3



----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Wed, 24 Feb 2010 12:13:10 +0000, Florent Xicluna a ?crit?:

Semi-regular is quite humble. You have been cranking out patches at a 
higher frequency than almost any of us in the last 3 months. We are 
exhausted of reviewing and (most of the time) committing your patches :)
(fortunately, your work happens to be of consistently good quality)

Regards

Antoine.




----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Wed, Feb 24, 2010 at 18:48, Antoine Pitrou <solipsis at pitrou.net> wrote:

Sometimes it seems like half of the tracker updates are Florent's.
Semi-regular is quite the understatement.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100224/757cb4c3/attachment.html>



----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Florent Xicluna <florent.xicluna <at> gmail.com> writes:


+1

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Antoine Pitrou wrote:

Agreed, I'd been thinking this may be deserved based on the number of
"patch by Florent Xicluna" commit messages I had seen go by on the
checkins list.

The usual caveats apply though:
	- don't get carried away with the privileges
	- even core devs still put patches on the tracker sometimes
	- if in doubt, ask for advice on python-dev (or IRC)
	- make sure to subscribe to python-checkins

The last point covers the fact that most checkin messages will get an
after-the-fact review from other developers and those comments usually
go straight to the checkins list.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] contributor to committer
----------------------------------------
Author: Florent Xiclun
Attributes: []Content: 
Hello,


Thanks all, for your warm welcome.


Usually I tend to be cautious.

-- 
Florent



