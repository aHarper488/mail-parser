
============================================================================
Subject: [Python-Dev] Issue #8863 adds a new
	PYTHONNOFAULTHANDLER?environment variable
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Issue #8863 adds a new
	PYTHONNOFAULTHANDLER?environment variable
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Mon, 20 Dec 2010 15:55:57 +0100, Stefan Krah <stefan at bytereef.org> wrote:

How is 'line 29 in g' not more precise than 'Segmentation fault'?

Now imagine that this error is being produced from a 20,000 line
application program bundle...  

--
R. David Murray                                      www.bitdance.com

