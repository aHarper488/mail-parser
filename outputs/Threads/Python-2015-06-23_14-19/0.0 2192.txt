
============================================================================
Subject: [Python-Dev] Suggestion on back-porting - a getpass issue.
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Suggestion on back-porting - a getpass issue.
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
The issue is this:

http://bugs.python.org/issue11236
getpass.getpass does not respond to ctrl-c or ctrl-z


Python 2.5 had a behavior when a user pressed CTRL-C at the getpass
prompt, it would raise a KeyBoardInterrupt and CTRL-Z would background
it.

Python 2.6 onwards this behavior got changed and KeyBoardInterrupt
Exception is not raised and nor is CTRL-Z backgrounding behavior
observed.

Users have complained about this change in behavior and want the old
behavior back. Their reasoning for wanting 2.5 behavior seems fine.

The tracker link gives the details to the discussion.

Now, if we fix it in 3.3 (and document the change), should  it be
back-ported to other versions (3.2,3.1,2.7)?

We may end up in a state where only 2.6 has a unique behavior with
respect handling CTRL-C and CTRL-Z on getpass call.

If you have any strong opinion that it should not be back-ported,
please share.

-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] Suggestion on back-porting - a getpass issue.
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Senthil Kumaran wrote:


Could this have been deliberate so that people can
put control characters in their passwords?

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Suggestion on back-porting - a getpass issue.
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
Greg Ewing wrote:

I don't think so. There are discussions in the internet which don't
favor use of control character in password. And anyway, it was not
intention to introduce control characters in password, for the change
in 2.6.

-- 
Senthil

