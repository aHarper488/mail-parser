
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.3): check the return
 value of new_string() (closes #18470)
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): check the return
 value of new_string() (closes #18470)
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2013/7/16 benjamin.peterson <python-checkins at python.org>:

Visual Studio does support instructions between declarations, and so
this changeset broke Windows buildbots.

Should we add the "-Werror=declaration-after-statement" compiler flag
to the 3.3 branch? (in debug mode?)

Victor



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): check the return
 value of new_string() (closes #18470)
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/7/15 Victor Stinner <victor.stinner at gmail.com>:

Fixed.


We've lived with this limitation for years. I'm sure we can wait until
3.4 becomes maintenance. :)


--
Regards,
Benjamin

