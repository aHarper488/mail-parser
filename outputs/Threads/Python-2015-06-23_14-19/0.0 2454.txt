
============================================================================
Subject: [Python-Dev] Status of the built-in virtualenv functionality in
	3.3
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 06.10.2011 16:12, schrieb ?ric Araujo:

How about clutch? A virtualenv is a clutch of Python eggs, all ready to
hatch. (Pun intended). :)

Christian




----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Thu, Oct 6, 2011 at 09:12, ?ric Araujo <merwok at netwok.org> wrote:

How about we just drop the "virtual" part of the name and make it "env"?

(or something non-virtual)



----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 6 Oct 2011 10:06:17 -0500
Brian Curtin <brian.curtin at gmail.com> wrote:

"pythonenv"?





----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Thu, Oct 6, 2011 at 10:46, ?ric Araujo <merwok at netwok.org> wrote:

develop? devenv?



----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 6 Oct 2011 12:02:05 -0400
Barry Warsaw <barry at python.org> wrote:

`python -m sandbox` ?






----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Carl Meye
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Hi ?ric,

Vinay is more up to date than I am on the current status of the
implementation. I need to update the PEP draft we worked on last spring
and get it posted (the WIP is at
https://bitbucket.org/carljm/pythonv-pep but is out of date with the
latest implementation work).

On 10/06/2011 08:12 AM, ?ric Araujo wrote:

What about "venv"? It's short, it's already commonly used colloquially
to refer to virtualenv so it makes an accurate and unambiguous mental
association, but AFAIK it is currently unused as a script or module name.

Carl
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk6N0+QACgkQ8W4rlRKtE2fCOwCg1YOWcMCZH6HOdyKepcQG3RgB
T48AoIIqol+sUpOAFI+4HJH/dAdX5Xwm
=DLjq
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
----- Original Message -----


The pythonv branch is pretty much up to date with the default branch (3.3). I regularly merge with default and post the results of running the Python regression suite in a virtual environment - in fact I'm running such a test right now :-). The test results and a screencast are linked from the project's BitBucket page at

https://bitbucket.org/vinay.sajip/pythonv/


I've made changes to get packaging to work well with virtualenv, some of which I raised as packaging issues on the tracker. In some cases, I've fixed them in the pythonv branch. The last bug is a problem with test_user_similar which has an existing issue (#9100) in a non-virtual environment. This is not a show-stopper, but I'm not really sure how the user scheme is supposed to work in venvs: perhaps Carl has a view on this. BTW there have been intermittent failures in test_packaging, too but they've generally been fixed by changes in core).


In terms of design issues, it would be useful if someone (apart from me that is) could look at how the pythonv fork differs from the core and comment on any issues they find. (Much of the change can be summed up as replacing occurrences of "sys.prefix" with "sys.getattr('site_prefix', sys.prefix)".)

BitBucket makes this type of comparison fairly easy to do; I'm not sure if there's a lot of value in adding a patch on the tracker for Rietveld review, until (and if) the PEP is accepted.


Re. distribute: At the moment the pythonv branch downloads a private version of distribute. The only significant difference from the vanilla distribute is the use of sys.site_prefix and sys.site_exec_prefix (falling back to sys.prefix and sys.exec_prefix if we're not running distribute in a virtual env, so it's backward compatible - but I didn't ask anyone in the packaging/distribute team to port this small change across. The only reference to sys.site_prefix is this code in setuptools/command/easy_install.py:

if hasattr(sys, 'site_prefix'):
??? prefixes = [sys.site_prefix]
else:
??? prefixes = [sys.prefix]
??? if sys.exec_prefix != sys.prefix:
??????? prefixes.append(sys.exec_prefix)

If this were ported to distribute, that would be nice :-)

I think the plan is to remove the distribute-downloading functionality from the stdlib. However, I am working on a companion project, "nemo", which will have this functionality and in addition provides virtualenvwrapper-like functionality for Linux, Mac and Windows. (This is based on the stdlib API, is WIP, not released yet, though shown in the screencast I mentioned earlier).


I'm OK with Carl's suggestion of "venv", and prefer it to Brian's suggestion of "env".

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
+1 for env or sandbox or something else with "box" in it.

pythonbox? envbox? boxenv?



----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: PJ Eb
Attributes: []Content: 
On Thu, Oct 6, 2011 at 12:02 PM, Barry Warsaw <barry at python.org> wrote:


Actually, it was pretty much for this exact purpose -- i.e. it was the idea
of a virtual environment.  Ian just implemented it first, with some
different ideas about configuration and activation.  Since this is basically
the replacement for that, I don't have any objection to using the term here.
 (In my vision, "nest" was also the name of a package management tool for
creating such nests and manipulating their contents, though.)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111006/3593bd04/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 10/06/11 18:02, Barry Warsaw wrote:

Hmm, with proper interpreter support I don't see what would be so "virtual"
about it anymore.

Georg




----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 6 October 2011 17:02, Barry Warsaw <barry at python.org> wrote:

No problem with a wrapper, but the nice thing about the -m form is
that it's portable. On Unix, shell script wrappers are pretty portable
(no idea if C-shell users would agree...) On Windows, though, there
are all sorts of problems. BAT files don't nest, so you end up having
to use atrocities like "CALL virtualenv" within a batch file.
Powershell users prefer .ps1 files. The only common form is an EXE,
but nobody really likes having to use a compiled form every time.

Paul.



----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
?ric Araujo <merwok <at> netwok.org> writes:



Another possible name would be "isolate":

python -m isolate /project/env

doesn't look too bad. There's no eponymous package on PyPI, and note also that
in addition to the common usage of isolate as a verb, it's also a noun with an
appropriate meaning in this context.

Regards,

Vinay Sajip







----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Oct 6, 2011 at 12:50 PM, Barry Warsaw <barry at python.org> wrote:

sandbox is a bit close to Victor's pysandbox for restricted execution
environments.

'nest' would probably work, although I don't recall the 'egg'
nomenclature featuring heavily in the current zipimport or packaging
docs, so it may be a little obscure.

'pyenv' is another possible colour for the shed, although a quick
Google search suggests that may have few name clash problems.

'appenv' would be yet another colour, since that focuses on the idea
of 'environment per application'.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Status of the built-in virtualenv functionality in
	3.3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Oct 6, 2011 at 3:06 PM, Barry Warsaw <barry at python.org> wrote:

Yeah, I meant to say that 'venv' also sounded like a reasonable choice
to me (for the reasons Carl listed).

Chers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

