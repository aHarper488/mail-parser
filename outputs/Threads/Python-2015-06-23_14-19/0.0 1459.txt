
============================================================================
Subject: [Python-Dev] cpython (3.2): NUL -> NULL
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.2): NUL -> NULL
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 18 Aug 2011 17:49:28 +0200
benjamin.peterson <python-checkins at python.org> wrote:

Are you sure? IIRC, NUL is the little name of ASCII character 0
(while NULL would be the NULL pointer).

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (3.2): NUL -> NULL
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
On 08/18/2011 02:19 PM, Antoine Pitrou wrote:

That's my understanding, too.

Eric.



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): NUL -> NULL
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/8/18 Antoine Pitrou <solipsis at pitrou.net>:

NUL is the abbreviation of the "Null character".


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): NUL -> NULL
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Antoine Pitrou <solipsis at pitrou.net> wrote:

Yes, that's the traditional name. I was surprised that the C99 standard uses
"null character" in almost all cases. Example:

"The construction '\0' is commonly used to represent the null character."


So I think it should be either NUL or "null character" with the lower
case spelling.


Stefan Krah

 



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): NUL -> NULL
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Aug 19, 2011 at 4:51 AM, Stefan Krah <stefan at bytereef.org> wrote:

+1

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): NUL -> NULL
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 18Aug2011 20:51, Stefan Krah <stefan at bytereef.org> wrote:
| Antoine Pitrou <solipsis at pitrou.net> wrote:
| > On Thu, 18 Aug 2011 17:49:28 +0200
| > benjamin.peterson <python-checkins at python.org> wrote:
| > > -        PyErr_SetString(PyExc_TypeError, "embedded NUL character");
| > > +        PyErr_SetString(PyExc_TypeError, "embedded NULL character");
| > 
| > Are you sure? IIRC, NUL is the little name of ASCII character 0
| > (while NULL would be the NULL pointer).
| 
| Yes, that's the traditional name. I was surprised that the C99 standard uses
| "null character" in almost all cases. Example:
| 
| "The construction '\0' is commonly used to represent the null character."
| 
| So I think it should be either NUL or "null character" with the lower
| case spelling.

+1 from me, too.
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

I like to keep an open mind, but not so open my brains fall out.
        - New York Times Chairman Arthur Sulzberger

