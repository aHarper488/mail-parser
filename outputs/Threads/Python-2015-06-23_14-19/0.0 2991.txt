
============================================================================
Subject: [Python-Dev]  Hash collision security issue (now public)
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]  Hash collision security issue (now public)
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
Steven D'Aprano (in
<http://mail.python.org/pipermail/python-dev/2011-December/115162.html>)
wrote:


No.  I really mean when the C code is initially compiled to produce an
python executable.

The only reason we're worrying about this is that an adversary may
force worst-case performance.  If the python instance isn't a server,
or at least isn't exposed to untrusted clients, then even a single
extra "if" test is unjustified overhead.  Adding overhead to every
string hash or every dict lookup is bad.

That said, adding some overhead (only) to dict lookups *that already
hit half a dozen consecutive collisions* probably is reasonable,
because that won't happen very often with normal data.  (6 collisions
can't happen at all unless there are already at least 6 entries, so
small dicts are safe; with at least 1/3 of the slots empty, it should
happen only 1/729 for worst-size larger dicts.)

-jJ



----------------------------------------
Subject:
[Python-Dev]  Hash collision security issue (now public)
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
Paul McMillan in
<http://mail.python.org/pipermail/python-dev/2012-January/115183.html>
wrote:



If the new hash algorithm doesn't kick in before, say, 32 characters,
then most currently hashed strings will not be affected.  And if the
attacker has to add 32 characters to every key, it reduces the "this
can be done with only N bytes uploaded" risk.  (The same logic
would apply to even longer prefixes, except that an attacker might
more easily find short-enough strings that collide.)


Given that the modulo is always 2^N, how is that different?

-jJ



----------------------------------------
Subject:
[Python-Dev]  Hash collision security issue (now public)
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
Victor Stinner wrote in
<http://mail.python.org/pipermail/python-dev/2012-January/115198.html>


(1)  Is it common to hash non-string input?  Because generating integers
that collide for certain dict sizes is pretty easy...

(2)  Would it make sense for traceback printing to sort dict keys?  (Any site
worried about this issue should already be hiding tracebacks from untrusted
clients, but the cost of this extra protection may be pretty small, given that
tracebacks shouldn't be printed all that often in the first place.)

(3)  Should the docs for json.encoder.JSONEncoder suggest sort_keys=True?

-jJ



----------------------------------------
Subject:
[Python-Dev]  Hash collision security issue (now public)
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
In http://mail.python.org/pipermail/python-dev/2012-January/115350.html,
Mark Shannon wrote:


(1)  Is it established that this (a single initial add, with no
per-loop operations) would be sufficient?

I thought that was in the gray area of "We don't yet have a known
attack, but there are clearly safer options."

(2)  Even if the direct cost (fetch and add) were free, it might be
expensive in practice.  The current hash function is designed to send
"similar" strings (and similar numbers) to similar hashes.

(2a)  That guarantees they won't (initially) collide, even in very small dicts.
(2b)  It keeps them nearby, which has an effect on cache hits.   The
exact effect (and even direction) would of course depend on the
workload, which makes me distrust micro-benchmarks.

If this were a problem in practice, I could understand accepting a
little slowdown as the price of safety, but ... it isn't.  Even in
theory, the only way to trigger this is to take unreasonable amounts
of user input and turn it directly into an unreasonable number of keys
(as opposed to values, or list elements) placed in the same dict (as
opposed to a series of smaller dicts).

-jJ



----------------------------------------
Subject:
[Python-Dev]  Hash collision security issue (now public)
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
In http://mail.python.org/pipermail/python-dev/2012-January/115368.html
Stefan Behnel wrote:


They SHOULD NOT represent the same content; comparing two strings
currently requires converting them to canonical form, which means the
smallest format (of those three) that works.

If it can be represented in PyUnicode_1BYTE_KIND, then representations
using PyUnicode_2BYTE_KIND or PyUnicode_4BYTE_KIND don't count as
canonical, won't be created by Python itself, and already compare
unequal according to both PyUnicode_RichCompare and stringlib/eq.h (a
shortcut used by dicts).

That said, I don't think smallest-format is actually enforced with
anything stronger than comments (such as in unicodeobject.h struct
PyASCIIObject) and asserts (mostly calling
_PyUnicode_CheckConsistency).  I don't have any insight on how
prevalent non-conforming strings will be in practice, or whether
supporting their equality will be required as a bugfix.

-jJ

