
============================================================================
Subject: [Python-Dev] Please revert autofolding of tracker edit form
Post Count: 41
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 3/31/2011 9:52 AM, Terry Reedy wrote:

+1.   Or +0 reverse time sequence the messages.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110331/be991103/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 31 Mar 2011 23:59:36 +0200, =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?= <martin at v.loewis.de> wrote:

I do.

--
R. David Murray           http://www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Glenn Linderman wrote:

-1 on reverse time sequence of messages -- no top posting!  ;)

~Ethan~



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Mar 31, 2011, at 9:52 AM, Terry Reedy wrote:


While that sounds logical, I think it will be a usability problem.  If someone doesn't see a the comment box immediately, they may not know to scroll down past dozens of messages to find it.

Rather that being trial-and-error amateur web page designers, it would be better to follow proven models.  All of the following have the comment box at the top and the messages in reverse chronological order:

* http://news.ycombinator.com/item?id=2393587  
* http://digg.com/news/entertainment/top_12_game_shows_of_all_time
* https://twitter.com/


Raymond
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110331/6907555f/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/3/31 Raymond Hettinger <raymond.hettinger at gmail.com>:

Please no reverse chronological order! Every bug tracker I know which
isn't underconfigured roundup uses chronological order.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Westley =?ISO-8859-1?Q?Mart=EDnez?
Attributes: []Content: 
On Thu, 2011-03-31 at 16:27 -0700, Raymond Hettinger wrote:

How 'bout no? YouTube uses this and it's horrid and unnatural, and
bulletin boards have been using chronological order for whiles with
great success. Reverse chronological order has a niche for feeds,
updates, whatever you want to call it, but when it comes to following a
discussion it's much easier to start with the first word.




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Mar 31, 2011, at 5:02 PM, Westley Mart?nez wrote:

Perhaps the most important part is that the comment box goes at the top.


Raymond




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/31/2011 7:27 PM, Raymond Hettinger wrote:

Even though such is standard for the majority of web fora?


I really hate that since it means scrolling down before reading, and 
because this is unusually, so by habit I start reading at the top.

In my experience, reverse is maybe 20% of sites I have visited. Forward: 
guido's blog, and I presume others at site; ars technica, stackoverflow, 
slashdot, most or all php-based web fora, ....

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 31 Mar 2011 23:59:36 +0200
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Sure, but it's far removed from where the rest of the issue is
displayed.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 31 Mar 2011 12:52:23 -0400
Terry Reedy <tjreedy at udel.edu> wrote:

Let's say that by using non-metric units you have already lost me,
sorry.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Raymond Hettinger wrote:

Are there cases where someone should be posting new comments who 
/hasn't/ read the existing comments?  I would hope that new comments 
would come only after reading what has already transpired -- in which 
case one would find the comment box when one runs out of previous 
comments to read.

~Ethan~



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Ethan Furman writes:

 > -1 on reverse time sequence of messages -- no top posting!  ;)

I'd really like this to be a browse-time option, and for bonus points,
it should be "sticky".  For issues I'm not familiar with, I want to
read in more or less chronological order.  For issues I *am* familiar
with, I want reverse chronological order.



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    >> I would like to try putting the comment box after the last (most
    >> recent) comment, as that is the message one most ofter responds
    >> to. Having to now scroll up and down between comment box and last
    >> message(s) is often of a nuisance.

    Raymond> While that sounds logical, I think it will be a usability
    Raymond> problem.  If someone doesn't see a the comment box immediately,
    Raymond> they may not know to scroll down past dozens of messages to
    Raymond> find it.

For me, the comment box is never available immediately.  I always have to
scroll.  After the first two sections have been filled in, most of that
information is static, excepting the nosy list changes and the occasional
change of state.  Almost all the action is going to be in the comments,
review buttons and patches which you always have to scroll to see.  That's
one reason I asked for the collapsible expanders.

If nothing else, You could make it easy to jump to the comments or the list
of patches with a link near the top of the page labelled "Jump to comments"
(or similar) which links to an anchor further down the page.

    Raymond> Rather that being trial-and-error amateur web page designers,
    Raymond> it would be better to follow proven models. 

I don't know who here uses Chrome, but if you have access to it, take a look
at the bookmark manager.  While it doesn't have what I had in mind at the
leaf level (I'd like the leaves to expand under their parents, not in a
separate pane), it does use expanders to reveal different amounts of detail.
It's a model many other "directory traversal" GUIs use.  Admittedly, we have
a bit flatter hierarchy, but the leaves are huge.  See attached.  (Apologies
for the image size.  Who would have thought such a modest image would be so
hard to compress?)

Skip

-------------- next part --------------
A non-text attachment was scrubbed...
Name: expanders.jpg
Type: image/jpeg
Size: 26587 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110331/24197c02/attachment.jpg>



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Terry> Even though such is standard for the majority of web fora?

I participate in a couple different web forums (914s and swimming).  Both
present their topics in chronological order and provide a link at the top of
every page which jumps the user to the first unread message (no matter how
many pages there are in the thread or where you happen to be at the moment).

Reverse chronological order is a nightmare for anybody trying to bring
themselves up-to-speed for the first time on a long discussion.  In my mind,
the only place where reverse order makes sense is in cases where messages
rapidly become less important as they age.  Think following the Japanese
earthquake/tsunami aftermath.

Skip



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Antoine> Let's say that by using non-metric units you have already lost
    Antoine> me, sorry.

Wouldn't it be cool if you could feed the text to Google Translator and have
it not only translate the English to bad French, but translate the units to
metric (hopefully with more accuracy than the language conversion)? :-)

Skip



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Ethan> Are there cases where someone should be posting new comments who
    Ethan> /hasn't/ read the existing comments?  I would hope that new
    Ethan> comments would come only after reading what has already
    Ethan> transpired -- in which case one would find the comment box when
    Ethan> one runs out of previous comments to read.

Again, drawing on the forum model, the new message box is always at the
bottom (of each page) in my experience.

Skip



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 3/31/2011 5:22 PM, Raymond Hettinger wrote:

As long as it is adjacent to the last comment, that would be fine.

(said while tongue removing the last bit of supper from the space 
outside the teeth)



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Westley =?ISO-8859-1?Q?Mart=EDnez?
Attributes: []Content: 
On Thu, 2011-03-31 at 21:18 -0500, skip at pobox.com wrote:

Exactly; my blog is in reverse chronological order because it's more of
a news bulletin and less of a discussion thread.  As for the comment
box, why not have it at the top AND bottom.  The top could have the
entire form and the bottom could have just a small quick-reply box, or
perhaps a back to top button (which probably exists already).




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Westley =?ISO-8859-1?Q?Mart=EDnez?
Attributes: []Content: 
On Thu, 2011-03-31 at 21:21 -0500, skip at pobox.com wrote:

It'd be accurate enough for most cases, but still limited by double
precision floating-point math.




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/31/2011 8:26 PM, Antoine Pitrou wrote:

My bad. Im science context I have always used S.I units, and wish U.S. 
would switch. Just forgot here. Multiply everything by 2.4 for cm.

Screen 43 cm wide, Left column 2.2 + 6 + 2.4, perhaps shrink to .6 + 4.8 
+ .6. Add right column with same. Comment box is 21 cm. Message boxes 
wider, could lose 2.7.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 01.04.2011 06:02, schrieb Terry Reedy:

Or by 2.54, if you're using SI cm :)

Georg




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 4/1/2011 6:44 AM, Georg Brandl wrote:

Then its a good thing I did the conversions with a dual scale ruler ;-).
So the number were accurate.
I envy all of you who only have to learn and use one relatively sensible 
unit system.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 4/1/2011 9:08 AM, Terry Reedy wrote:

Me too.  But anyone that calls themselves a programmer should be able to 
realize that the numbers are proportional and Google happily finds 
online conversion calculators.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110401/fd700f97/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Dj Gilcreas
Attributes: []Content: 
How about something like
http://andurin.com/python-issue-tracker/issue5863.htm but with proper
click to expand js not css hover expansion since the pure css solution
gets a little jumpy.

Dj Gilcrease
?____
( | ? ? \ ?o ? ?() ? | ?o ?|`|
? | ? ? ?| ? ? ?/`\_/| ? ? ?| | ? ,__ ? ,_, ? ,_, ? __, ? ?, ? ,_,
_| ? ? ?| | ? ?/ ? ? ?| ?| ? |/ ? / ? ? ?/ ? | ? |_/ ?/ ? ?| ? / \_|_/
(/\___/ ?|/ ?/(__,/ ?|_/|__/\___/ ? ?|_/|__/\__/|_/\,/ ?|__/
? ? ? ? ?/|
? ? ? ? ?\|



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Dj> How about something like
    Dj> http://andurin.com/python-issue-tracker/issue5863.htm but with
    Dj> proper click to expand js not css hover expansion since the pure css
    Dj> solution gets a little jumpy.

That's part of it.  Note the files list as well:

    bz2module-v1.diff       nvawda, 2011-01-24 20:15        ...
    bz2module-v2.diff       nvawda, 2011-01-25 17:07        
    bz2-v3.diff             nvawda, 2011-01-30 14:12        
    bz2-doc.diff            nvawda, 2011-02-05 20:31        
    bz2-v3b.diff            nvawda, 2011-02-08 21:57        
    bz2-v4.diff             nvawda, 2011-03-20 19:15                     
    bz2-v4-doc.diff         nvawda, 2011-03-20 19:18                        
    bz2-v5.diff             nvawda, 2011-04-02 07:34        
    bz2-v5-doc.diff         nvawda, 2011-04-02 07:38                        
    bz2-v6.diff             nvawda, 2011-04-02 18:14                        
    bz2-v6-doc.diff         nvawda, 2011-04-02 18:14        

It looks like there are seven versions of the bz2 patch and four versions of
the doc patch.  I think this list should be collapsed as well (ignoring that
the author seems to have changed his mind about the name of the patch):

    bz2.diff >       nvawda, 2011-04-02 18:14
    bz2-doc.diff >   nvawda, 2011-04-02 18:14

expanding the patch proper:

    bz2.diff v       nvawda, 2011-04-02 18:14
      bz2.diff       nvawda, 2011-04-02 07:34
      bz2.diff       nvawda, 2011-03-20 19:15
      ...
    bz2-doc.diff >   nvawda, 2011-04-02 18:14

Skip



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
The tracker was recently changed so that when I click on a link to a 
tracker page, the page is properly displayed, but then a fraction of a 
second it blinks and redisplays with the edit form hidden. This is so 
obnoxious to me that I no longer want to visit the tracker. Then I have 
to find and click the button to get back the edit form that I nearly 
always want to see, as I often make changes. All this to compress the 
page by half a screen, which makes almost no difference once one grabs 
the scrollbar anyway.

If someone actually considers this a desired feature, after using it, 
then please add a field on the profile page to select autofolding or 
not. Also, there should be a button to fold as well as one to unfold.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Mar 31, 2011 at 7:35 AM, Terry Reedy <tjreedy at udel.edu> wrote:

Interesting - it comes straight up with the folded screen for me, no
flickering at all.

It may depend on how a given browser handles the scripts involved.


Skip objected to the amount of noise at the top of the tracker screen,
so I assume whoever added the feature was doing so in response to that
concern. Since it doesn't flicker for me, I actually found it to be an
elegant solution.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Mar 30, 2011, at 2:35 PM, Terry Reedy wrote:


Same thing happening here (Google Chrome browser running on Snow Leopard).
I also find it weird and irritating.



+1


Raymond



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Simon Cros
Attributes: []Content: 
On Thu, Mar 31, 2011 at 12:54 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

I didn't get any flicker either and my first impression of the change
was also positive -- I usually skip straight to the comments the first
time I visit an issue.

Schiavo
Simon



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 31 Mar 2011 08:54:41 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

It has flickered occasionally here (Firefox 4).


Or how fast they get loaded compared to the main HTML, which can then
interact with some redraw timers in your browser or whatever else.


There's a lot of "noise" but that noise is useful. I find the
natural language summary to be much too terse and doesn't make it easy
to visualize said information as opposed to the form fields.
What's more, it lacks the most important: the issue title.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/30/2011 7:32 PM, Antoine Pitrou wrote:


Yes, there is a good reason why database records are routinely displayed 
in labeled and located fields rather than in variable length natural 
language sentences with a monochrome font. Form letters, of course, are 
an exception.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 31 Mar 2011 08:54:41 +1000, Nick Coghlan <ncoghlan at gmail.com> wrote:

Flicker or not, I don't like it myself.  I've turned off javascript on
bugs.python.org in my browser, and now it doesn't bother me anymore.
But I don't think that is a good long term solution.  Making it optional
based on a setting in the user profile might be OK.

--
R. David Murray           http://www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Wed, Mar 30, 2011 at 10:08 PM, Terry Reedy <tjreedy at udel.edu> wrote:

Yep.

While I'm fine with folding away some of the verbose fields, the
current implementation is jarring.  I also get the flicker before the
fold happens, but the sentence describing status doesn't seem like a
good idea for regular users.  I would expect only the "second box"
from the top to get folded.

Making folding a per-user setting would be appropriate.


-- 
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"Give me the luxuries of life and I will willingly do without the necessities."
?? --Frank Lloyd Wright



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 
Please see this thread in the tracker-discuss list:

    http://mail.python.org/pipermail/tracker-discuss/2011-March/thread.html

Skip



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    skip> Please see this thread in the tracker-discuss list:
    skip>     http://mail.python.org/pipermail/tracker-discuss/2011-March/thread.html

That didn't read right.  I meant see that thread to see other discussion
about the folding topic, not that it would necessary convince you all to
change your minds.

I'd still like to see some folding, but more selective and with closer
attention paid to the current stage of the issue.

Skip



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 31 Mar 2011 06:20:03 -0500
skip at pobox.com wrote:

Agreed that some folding could be beneficial, but with a better UI
(no flickering, use of a jQuery-alike recommended). Also, field-based
visualization of tracker properties should be retained in the
"minimized" form, instead of natural language.

It would be nice if someone with UI design experience was interested in
maintaining/improving the tracker.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Mar 31, 2011 at 10:16 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

The challenge is the same as with any UI designer involvement in open
source stuff though: they really need to be given the freedom to do
proper workflow analysis and come up with something that *works*, but
in practice things tend to get derailed into endless bikeshed
arguments, since *everyone* has an opinion on the UI design of the
tools they have to use.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le jeudi 31 mars 2011 ? 23:48 +1000, Nick Coghlan a ?crit :

Well, obviously they have, since they are users and are directly
impacted by any changes.
The line this draws is between clean-sheet design and iterative
improvement. Clearly it would be difficult to try to sell us a wholesale
change in how the issue tracker organizes things.

(AFAIK, Roundup itself comes from a platonic, clean-sheet design of an
ideal bug tracker: we had to customize it a lot)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/31/2011 9:59 AM, Antoine Pitrou wrote:

Here is my proposal for a redesign based on an analysis of my usage ;-).
I have a 1600x1050 (or thereabouts), 20" (measured) diagonal, 17" across 
screen.

The left column has a 7/8" margin, 2 3/8" text area, and 1" gutter. 
These could be shrunk to say, 1/4, 2, 1/4, saving 1 3/8".
The comment box is 8 1/2", message boxes are wider, but the extra width 
is not used if one uses hard returns in the comment box. In any case, 
the message boxes could be narrowed by 1 1/8".
This would allow a right column of 1/4+2+1/4".

Except for title, comment, and file boxes, we then stack the 
classification and process boxes in the right column.
The nosy box would be last so it can list one name per line.
It would end with an [ add  me ] button. It should also have a box for 
adding people (as with Assigned To). Having to look up tracker names in 
Assigned To and retype exactly in the nosy list is an error-prone 
nuisance. Putting all these boxes to the side leaves them visible for 
those who want them but out of the way and ignorable for those who do not.

I would be nice is both the left and right columns had buttons to hide 
and show. This would help people accessing issues from smaller screens, 
as with a netbook.

The title is not really classification and belongs at the very top. It 
could even go in the 1 1/8" top bar, which is currently mostly empty, 
along with the issue number.

I would like to try putting the comment box after the last (most recent) 
comment, as that is the message one most ofter responds to. Having to 
now scroll up and down between comment box and last message(s) is often 
of a nuisance.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
Hi,

On 31/03/2011 19.52, Terry Reedy wrote:

You can add your proposal to 
http://wiki.python.org/moin/DesiredTrackerFeatures

There you can find several suggestions and discussions about possible 
changes to the tracker (it's similar to python-ideas ML, but it's about 
the tracker and it's on a wiki page).

Bugs and concrete suggestions should be submitted to the meta-tracker 
(http://psf.upfronthosting.co.za/roundup/meta/) or discussed on the 
tracker-discuss mailing list.


You can also use the "(list)" link to open a popup and search for users 
to add to the nosy instead of copying them from the "Assigned To" list 
by hand.  (I know that popup are not the most usable thing and that the 
search has some limitation (e.g. it's case sensitive), but that's the 
best we have right now, and works fairly well for me.)

Also one of the design goal is to keep the interface as simple as 
possible, so we try to avoid adding more things unless they are really 
useful.  Having an expanded list of names doesn't sound too useful to me.


This summer I plan to participate again to GSOC and work on the bug 
tracker (if my proposal gets accepted).  There are already a few things 
I want to improve and I also have some patches ready that just need to 
be updated and applied.

In the meanwhile you can use 
http://wiki.python.org/moin/DesiredTrackerFeatures to list the things 
that you would like to see, so that they don't get lost in the archives 
of python-dev.

Best Regards,
Ezio Melotti



----------------------------------------
Subject:
[Python-Dev] Please revert autofolding of tracker edit form
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Notice that the issue title was always there, in your browser's title
bar (unless you have a web browser that doesn't display the page title).

Regards,
Martin

