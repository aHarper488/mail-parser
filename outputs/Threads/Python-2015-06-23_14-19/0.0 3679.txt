
============================================================================
Subject: [Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
As discussed here, the python 2.5 binary distributed by Apple on mountain
lion is broken. Could someone file an official complaint? This is really
bad...

--Guido


-- 
--Guido van Rossum (python.org/~guido)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121001/e3c5f7a8/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Forgot the link...
http://code.google.com/p/googleappengine/issues/detail?id=7885

On Monday, October 1, 2012, Guido van Rossum wrote:



-- 
--Guido van Rossum (python.org/~guido)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121001/11257586/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article 
<CAP7+vJLYBgPCmKNT5UVzQ9Na3qBdumE9CmJ5PL6yZqR75AyAnw at mail.gmail.com>,
 Guido van Rossum <guido at python.org> wrote:


I've filed a bug against 10.8 python2.5.   The 10.8 versions of Apple's 
pythons are compile with clang and we did see some sign extension issues 
with ctypes.  The 10.7 version of Apple's python2.5 is compiled with 
llvm-gcc and handles 2**31 correctly.

Unfortunately, AFAIK, only the submitter (i.e. me) can see the bug 
report (problem id 12411585).  I'll report back when I hear something.

-- 
 Ned Deily,
 nad at acm.org




----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Ned Deily <nad at acm.org> wrote:

Yes, this looks like http://bugs.python.org/issue11149 .


Stefan Krah





----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <20121002073135.GA26567 at sleipnir.bytereef.org>,
 Stefan Krah <stefan at bytereef.org> wrote:

Ah, right, thanks.  I've updated the Apple issue accordingly.

-- 
 Ned Deily,
 nad at acm.org




----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <nad-B4E67A.00475902102012 at news.gmane.org>,
 Ned Deily <nad at acm.org> wrote:

Update: the bug I filed has been closed as a duplicate of #11932488 
which apparently at the moment is still open.  No other information is 
available.

-- 
 Ned Deily,
 nad at acm.org




----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Oct 5, 2012 at 4:45 PM, Ned Deily <nad at acm.org> wrote:

Thanks Ned! Is there any way that I could see that bug myself and
attach myself to updates? Otherwise, can you keep us here appraised of
developments (even if Apple decides not to fix it)?

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article 
<CAP7+vJKeXqYyvfwipOR+7yTdtUs2eDhvybV3tyZb3ZkX6hta5g at mail.gmail.com>,
 Guido van Rossum <guido at python.org> wrote:

I don't think there's any way to see any bug other than ones you have 
submitted yourself.  All I can see is that the bug I submitted is closed 
as a duplicate and now has a Related Problem section that only gives the 
other incident number and its status (Open).  I can't view anything else 
about that other incident.  I don't know if I'll get an email update if 
its status changes.  I'll keep an eye on mine and perhaps ask for a 
status update if nothing changes in a few weeks.

-- 
 Ned Deily,
 nad at acm.org




----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Thanks, Ned. Do you think it would be worth it to use our contacts at Apple
to raise the priority of this embarrassment?

On Saturday, October 6, 2012, Ned Deily wrote:



-- 
--Guido van Rossum (python.org/~guido)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121006/a055f4c6/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Bad python 2.5 build on OSX 10.8 mountain lion
----------------------------------------
Author: Ned Deil
Attributes: []Content: 

On Oct 6, 2012, at 20:47 , Guido van Rossum <guido at python.org> wrote:

It might help if anyone wants to try.  On the other hand, Python 2.5 is probably not the highest priority for Apple since they also ship Python 2.6 and 2.7 with 10.8, neither of which have this problem.  I'll see if I can get an official status update on mine (#12411585).

--
  Ned Deily
  nad at acm.org -- []



