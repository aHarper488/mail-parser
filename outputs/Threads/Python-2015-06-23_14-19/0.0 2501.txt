
============================================================================
Subject: [Python-Dev] AUTO: Jon K Peck is out of the office (returning
	10/27/2011)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] AUTO: Jon K Peck is out of the office (returning
	10/27/2011)
----------------------------------------
Author: Jon K Pec
Attributes: []Content: 


I am out of the office until 10/27/2011.

I am out of the office attending the IBM IOD conference.  I will be back in
the office on Friday, 10/28.  My email responses will be delayed during
that period.


Note: This is an automated response to your message  "Python-Dev Digest,
Vol 99, Issue 45" sent on 10/23/11 4:00:03.

This is the only notification you will receive while this person is away.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111023/b2bb1c9e/attachment.html>

