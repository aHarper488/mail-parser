
============================================================================
Subject: [Python-Dev] [Python-checkins] r86264 -
	python/branches/release27-maint/Lib/distutils/sysconfig.py
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86264 -
	python/branches/release27-maint/Lib/distutils/sysconfig.py
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Yes, I asked myself the same question.  I had done the svnmerge from
py3k, and when I saw that only one change was left, I wondered whether I
should commit or revert.

This was a private function used on an unsupported platform, this should
do no harm.  We?ve been bitten by ?should do no harm? before though, so
I am ready to revert this change (and learn from this :)

Regards




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86264 -
	python/branches/release27-maint/Lib/distutils/sysconfig.py
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

Do as you like. I won't insist on it being reverted.

It's rather a matter of agreeing when moving forward: IMO, mere style
changes, code cleanup etc shouldn't be applied to the bug fix branches,
as their only purpose is to provide bug fixes for existing users.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86264 -
	python/branches/release27-maint/Lib/distutils/sysconfig.py
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 11/6/2010 12:33 PM, "Martin v. L?wis" wrote:

The omission of the deletion from the 5/5 revision was a bug in that 
revision. If the removal of OS9 support was documented (announced), 
which I presume it was, then one could consider any visible trace 
remaining to be a bug.

Perhaps the policy on code cleanup should be a bit more liberal for 2.7 
*because* it will be maintained for several years and *because* there is 
no newer 2.x branch to apply changes to. If I were going to maintain 2.7 
for several years, I would want to have the benefit of gradual 
improvements that make maintainance easier.

Applying such a cleanup to 3.1, say, is less necessary because a) the 
code will soon be end-of-lifed and not maintained much and b) it can be 
applied to the newer (3.2) branch and benefit that and all future 
releases thereafter.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86264 -
	python/branches/release27-maint/Lib/distutils/sysconfig.py
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
[Martin]

FTR, it was documented in PEP 11 as removed in 2.4 (but not in 2.4?s
whatsnew).


It?s known that people do modify distutils.sysconfig._config_vars, a
private dictionary; I can imagine some really contrived example of code
using _init_mac, the function I removed, to set sysconfig values for Mac
OS 9 in 2.7 code.  1% chance, I guess.


I don?t think Terry was suggesting breakages, just other kinds of
cleanup.  In this particular case, I think now that I should have
followed distutils policy (which is less liberal that the rest of the
stdlib).  If there are no arguments against it this week, I will revert
the commit.

Regards


