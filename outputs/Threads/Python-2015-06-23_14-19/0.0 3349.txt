
============================================================================
Subject: [Python-Dev] importlib cleared for merging into default!
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] importlib cleared for merging into default!
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
At the language summit today I got clearance to merge my importlib
bootstrap branch (http://hg.python.org/sandbox/bcannon#bootstrap_importlib)
thanks to performance being about 5% slower using the normal_startup
(which, as Thomas Wouters said, is less than the difference of using the
newest gcc in some benchmarking he recently did). Obviously thanks to
everyone who has helped out with making this happen over the years!

So, where does that leave us? First is getting full code review and sign
off from somebody (http://bugs.python.org/issue2377 is the issue tracking
this). Once I have that I will do the merge and then go through the tracker
to update issues to see if they still apply or need to be re-targeted for
importlib.

Once importlib has been merged there is some stuff that needs to happen. I
will first strip imp down to what has to be in import.c (e.g. importing a
built-in module), rename it _imp, and then re-implement/extend whatever is
needed in an imp.py module. This will allow for much of the C code left in
import.c to go away (i.e. imp.find_module() is the reason the finder code
in import.c has not been removed yet). It will also alleviate the other VMs
from having to implement all of imp from scratch.

Once importlib is in I will also do some proposing on how to undo some
import design decisions that were caused because it was all C code (e.g.
implicit stuff like a sys.meta_path entry that handles
sys.path/sys.path_importer_cache/sys.path_hooks, the meaning of None in
sys.path_importer_cache). Everyone at the language summit was supportive of
this stuff and basically agreed to it but wanted it as a separate step from
the merge to get everything moving faster.

What can be done in parallel/while waiting is exposing more of importlib's
innards. This ties into some of the proposals I will be making once the
merge occurs.

Everything else I have in mind is long term stdlib cleanup using importlib,
but that is not important now.

IOW, someone please review my bootstrap_importlib branch so I can merge it.
=)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120307/2fde2777/attachment.html>



----------------------------------------
Subject:
[Python-Dev] importlib cleared for merging into default!
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 7 March 2012 23:05, Brett Cannon <brett at python.org> wrote:

Yay! Congratulations for getting this done. When I first co-authored
PEP 302 I never realised what a long road it would be from there to
here. Thanks for making it happen.

Paul.

