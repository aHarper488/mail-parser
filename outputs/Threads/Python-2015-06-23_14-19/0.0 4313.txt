
============================================================================
Subject: [Python-Dev] A 'common' respository? (was Re: IDLE in the
	stdlib)
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] A 'common' respository? (was Re: IDLE in the
	stdlib)
----------------------------------------
Author: Philip Jame
Attributes: []Content: 
I hope I'm not coming across as pedantic, because I think you have some
good arguments listed above, but shouldn't discussion like this go in
python-ideas rather than python-dev? I'm very new to these lists, so
forgive me if I'm stepping on any toes, I'm just trying to grok what kind
of content should go in each list.

PJJ
http://philipjohnjames.com


On Wed, Mar 20, 2013 at 7:30 PM, Terry Reedy <tjreedy at udel.edu> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130320/2836720a/attachment.html>



----------------------------------------
Subject:
[Python-Dev] A 'common' respository? (was Re: IDLE in the
	stdlib)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/21/2013 2:06 AM, Philip James wrote:

Normally yes. But since this is a counter-proposal or an alternate 
proposal to proposals already made in this thread, it belongs as an 
offshoot of this thread. This thread in turn is a continuation of 
similar threads here in the past, and involves some people who are less 
active in python-ideas. Also, it is more about technical development 
matters than about future *language* changes.

 > I'm very new to these lists, so

You are not stepping on my toes, and that is a good question to think about.

-- 
Terry Jan Reedy


