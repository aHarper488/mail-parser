
============================================================================
Subject: [Python-Dev] PEP 8 misnaming
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 8 misnaming
----------------------------------------
Author: Facundo Batist
Attributes: []Content: 
Hello!

In the "Maximum Line Length" section of PEP 8 it says:

    "The preferred place to break around a binary operator is *after*
the operator, not before it."

And after that is an example (trimmed here):

            if (width == 0 and height == 0 and
                color == 'red' and emphasis == 'strong' or
                highlight > 100):
                raise ValueError("sorry, you lose")

In the example the line is broken after the 'and' or 'or' *keywords*,
not after the '==' *operator* (which is the nice way of doing it).

Maybe the sentence above is misleading?

Thanks!

-- 
.? ? Facundo

Blog: http://www.taniquetil.com.ar/plog/
PyAr: http://www.python.org/ar/



----------------------------------------
Subject:
[Python-Dev] PEP 8 misnaming
----------------------------------------
Author: Geoffrey Spea
Attributes: []Content: 
On Wed, Mar 14, 2012 at 7:21 AM, Facundo Batista
<facundobatista at gmail.com> wrote:

'and' and 'or' are both binary logical operators. The fact that they
are keywords is irrelevant; the sentence isn't misleading.



----------------------------------------
Subject:
[Python-Dev] PEP 8 misnaming
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Facundo Batista <facundobatista at gmail.com> writes:


?and? and ?or? are binary operators (that also happen to be keywords).
The description is accurate and IMO not misleading.


?1. The lower-priority binding operator is the better place to break the
line. The binary logical operators bind at lower priority than the
equality operator.

-- 
 \           ?If you do not trust the source do not use this program.? |
  `\                                ?Microsoft Vista security dialogue |
_o__)                                                                  |
Ben Finney


