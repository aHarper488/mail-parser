
============================================================================
Subject: [Python-Dev] [Python-checkins] r88729 -
	python/branches/py3k/Modules/posixmodule.c
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88729 -
	python/branches/py3k/Modules/posixmodule.c
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 4, 2011 at 2:10 AM, giampaolo.rodola
<python-checkins at python.org> wrote:

NEWS entry and a new name in ACKS?

(the query regarding the lack of a NEWS entry applies to your other
recent commits as well).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88729 -
	python/branches/py3k/Modules/posixmodule.c
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 4, 2011 at 10:33 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Oops, one of them did have an entry, and a second was just a tweak to
the first one. So the NEWS query only applies to this one and the NNTP
change.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88729 -
	python/branches/py3k/Modules/posixmodule.c
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 
Thanks.
I'll try to remember ACKS and NEWS in the future. =)
Fixed in r88744 and r88745.


--- Giampaolo
http://code.google.com/p/pyftpdlib/
http://code.google.com/p/psutil/

2011/3/4 Nick Coghlan <ncoghlan at gmail.com>:

