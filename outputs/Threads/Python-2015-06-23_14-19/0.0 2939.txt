
============================================================================
Subject: [Python-Dev] hash randomization in 3.3
Post Count: 22
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

Shouldn't it be enabled by default in 3.3?
It's currently disabled.

$ ./python -c "print(hash('aa'))"
12416074593111936
[44297 refs]
$ ./python -c "print(hash('aa'))"
12416074593111936
[44297 refs]

Thanks

Antoine.





----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/2/21 Antoine Pitrou <solipsis at pitrou.net>:

Should you be able to disable it?


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 21 Feb 2012 14:58:41 -0500
Benjamin Peterson <benjamin at python.org> wrote:

PYTHONHASHSEED=0 should disable it.  Do we also need a command-line
option?

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/2/21 Antoine Pitrou <solipsis at pitrou.net>:

I don't think so. I was just wondering if we should force people to use it.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Feb 21, 2012, at 02:58 PM, Benjamin Peterson wrote:


Yes.


No, but you should be able to provide a seed.

-Barry



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 2/21/2012 11:58 AM, Benjamin Peterson wrote:

Yes, absolutely.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120221/3c9615f3/attachment.html>



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Feb 21, 2012 at 15:05, Barry Warsaw <barry at python.org> wrote:



I think that's inviting trouble if you can provide the seed. It leads to a
false sense of security in that providing some seed secures them instead of
just making it a tad harder for the attack. And it won't help with keeping
compatibility with Python 2.7 installations that don't have randomization
turned on by default. If we are going to allow people to turn this off then
it should be basically the inverse of the default under Python 2.7 and no
more.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120221/d6253bcc/attachment.html>



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/2/21 Antoine Pitrou <solipsis at pitrou.net>:

I've now enabled it by default in 3.3.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Xavier More
Attributes: []Content: 
On 2012-02-21, at 21:24 , Brett Cannon wrote:

I might have misunderstood something, but wouldn't providing a seed always 
make it *easier* for the attacker, compared to a randomized hash?




----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Feb 21, 2012 at 15:58, Xavier Morel <python-dev at masklinn.net> wrote:


Yes, that was what I was trying to convey.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120221/3e6670a1/attachment.html>



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Feb 21, 2012, at 09:58 PM, Xavier Morel wrote:


I don't think so.  You'd have to somehow coerce the sys.hash_seed out of the
process.  Not impossible perhaps, but unlikely unless the application isn't
written well and leaks that information (which is not Python's fault).

Plus, with randomization enabled, that won't help you much past the current
invocation of Python.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120221/58d5884c/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 21.02.2012 20:59, schrieb Antoine Pitrou:

On the contrary. PYTHONHASHSEED should go in 3.3, as should any
facility to disable or otherwise fix the seed.

Regards,
martin



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Why exactly is that?

We should take an attitude that Python hash values
are completely arbitrary and can change at any point
without notice. The only strict requirement should be
that hashing must be consistent with equality; everything
else should be an implementation detail.

With that attitude, supporting explicit seeds is counter-productive.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 21 Feb 2012 22:51:48 +0100
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Being able to reproduce exact output is useful to chase sporadic test
failures (as with the --randseed option to regrtest).

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Feb 22, 2012 at 8:07 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

I'm with Antoine here - being able to force a particular seed still
matters for testing purposes. However, the documentation of the option
may need to be updated for 3.3 to emphasise that it should only be
used to reproduce sporadic failures. Using it to work around
applications that can't cope with randomised hashes would be rather
ill-advised.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

In the tracker, someone proposed that the option is necessary to synchronize
the seed across processes in a cluster. I'm sure people will use it for that
if they can.

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Feb 22, 2012 at 3:20 PM,  <martin at v.loewis.de> wrote:

Yeah, that use case sounds reasonable, too. Another example is that,
even within a machine, if two processes are using shared memory rather
than serialised IPC, synchronising the hashes may be necessary. The
key point is that there *are* valid use cases for forcing a particular
seed, so we shouldn't take that ability away.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Brett Cannon writes:

 > I think that's inviting trouble if you can provide the seed. It leads to a
 > false sense of security

I thought the point of providing the seed was for reproducability of
tests and the like?

As for "false sense", can't we document this and chalk up hubristic
behavior to "consenting adults"?



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Feb 22, 2012, at 09:04 PM, Stephen J. Turnbull wrote:


+1

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120222/a32e4b81/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 22 Feb 2012 12:59:33 -0500
Barry Warsaw <barry at python.org> wrote:


How is it a "false sense of security" at all? It's the same as
setting a private secret for e.g. session cookies in Web applications.
As long as you don't leak the seed, it's (should be) secure.

(the only hypothetical issue being with Victor's choice of an LCG
pseudo-random generator to generate the secret from the seed)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 2/22/2012 1:57 AM, Nick Coghlan wrote:


When we document the option to set the seed, we could mention that 
synchronization of processes that share data is the main intended use.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] hash randomization in 3.3
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Antoine Pitrou writes:

 > How is it a "false sense of security" at all? It's the same as
 > setting a private secret for e.g. session cookies in Web applications.
 > As long as you don't leak the seed, it's (should be) secure.

That's true.  The problem is, the precondition that you won't leak the
seed is all too often false.  If a user takes advantage of the ability
to set the seed, she can leak it, or a coworker (or a virus) can steal
it from her source or keystroke logging, etc.

And it's not the same, at least not for a highly secure application.
In high-quality security, session keys are generated for each session
(and changed frequently); the user doesn't know them (of course, he
can always find out if he really wants to know, and sometimes that's
necessary -- Hello, Debian OpenSSH maintainer!), and so can't leak
them.


