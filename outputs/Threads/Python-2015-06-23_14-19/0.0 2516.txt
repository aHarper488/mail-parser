
============================================================================
Subject: [Python-Dev] cpython (merge 3.2 -> default): Fix the return
 value of set_discard (issue #10519)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (merge 3.2 -> default): Fix the return
 value of set_discard (issue #10519)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 30 Oct 2011 13:38:35 +0100
petri.lehtinen <python-checkins at python.org> wrote:


I get the following compiler warning here:

Objects/setobject.c: In function ?set_discard?:
Objects/setobject.c:1909:24: attention : unused variable ?result?

Regards

Antoine.



