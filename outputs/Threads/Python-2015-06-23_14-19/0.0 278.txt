
============================================================================
Subject: [Python-Dev] [Python-checkins] r87070 -
	python/branches/py3k/Lib/test/test_shutil.py
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87070 -
	python/branches/py3k/Lib/test/test_shutil.py
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Sat, Dec 4, 2010 at 20:04, hirokazu.yamamoto
<python-checkins at python.org>wrote:


I created #10540 for this issue, but the patch I have on there is just a bad
hack. I need to fix os.path.samefile for hard links, which might be easier
if we keep st_ino data in stat structures on Windows.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101204/f2ab10c2/attachment.html>

