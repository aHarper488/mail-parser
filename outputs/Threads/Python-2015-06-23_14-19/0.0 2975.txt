
============================================================================
Subject: [Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
Post Count: 12
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 26 Feb 2012 19:50:10 +0100
martin at v.loewis.de wrote:

It would be nice to call it something else than "printf-style
formatting". While it is certainly modelled on printf(), knowledge of C
or printf is not required to understand %-style formatting, nor even to
appreciate it.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

+1. The section is already titled "old string formatting operations" so if
this name is acceptable it should be reused. If it's not, it should then be
consistently changed everywhere.

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120226/3ee6fc96/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Mon, Feb 27, 2012 at 5:54 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

-1. Calling it "printf-style" ties it in with its origins just as the
term "regex" does for the 're' module. There are printf-derived
features in quite a few high level languages; they may differ somewhat
(Pike's sprintf() can do columnar displays; PHP's vsprintf takes an
array, not some weird and mythical varargs token), but in their basics
they will be similar. The name is worth keeping.

Chris Angelico



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Feb 27, 2012 at 5:23 AM, Eli Bendersky <eliben at gmail.com> wrote:

I deliberately chose printf-style as being value neutral (whereas
old-style vs new-style carries a heavier recommendation that you
should be using the new one). Sure you don't need to know printf to
understand it, but it needs *some* kind of name, and "printf-style"
acknowledges its roots. Another value-neutral term is "mod-style",
which describes how it is invoked (and I believe we do use that in a
few places already).

I didn't actually expect that paragraph to be incorporated wholesale
into the docs - it was intended as a discussion starter, not a
finished product. Aside from the last two sentences, the other big
problem with it is that print-style formatting *does* support
formatting arbitrary objects, they're just forced to go through type
coercions whereas .format() allows objects to define their own
formatting specifiers (such as datetime with strftime strings).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 27Feb2012 07:13, Nick Coghlan <ncoghlan at gmail.com> wrote:
| On Mon, Feb 27, 2012 at 5:23 AM, Eli Bendersky <eliben at gmail.com> wrote:
| >> It would be nice to call it something else than "printf-style
| >> formatting". While it is certainly modelled on printf(), knowledge of C
| >> or printf is not required to understand %-style formatting, nor even to
| >> appreciate it.
| >
| >
| > +1. The section is already titled "old string formatting operations" so if
| > this name is acceptable it should be reused. If it's not, it should then be
| > consistently changed everywhere.
| 
| I deliberately chose printf-style as being value neutral (whereas
| old-style vs new-style carries a heavier recommendation that you
| should be using the new one). Sure you don't need to know printf to
| understand it, but it needs *some* kind of name, and "printf-style"
| acknowledges its roots.

+1 here from me too: it _is_ printf in roots and several format
specifiers (%d, %s etc). If you know printf you _immediately_ know a lot
about what you can expect, and if you don't you know know a little about
its roots.

| Another value-neutral term is "mod-style",
| which describes how it is invoked (and I believe we do use that in a
| few places already).

A -1 on "mod-style" from me. While it does use the "%" operator symbol, in no
other way is it like the "mod" arithmetic operation.

I think docs _should_ occasionally hint at preferred approaches. The
new new formatting is a deliberate Python change. Without some
rationale/editorial it flies in the face of the "one obvious way to do
things" notion. It shouldn't be overdone, but neither should it be
absent.

Cheers,
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

Ignorance is preferable to error; and he is less remote from the truth
who believes nothing, than he who believes what is wrong.
        - Thomas Jefferson



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 02/26/2012 10:13 PM, Nick Coghlan wrote:

I've seen "percent-formatting", which is neutral, accurate and doesn't
require any previous knowledge.  (The new one could be "format-formatting"
then, which is a tad awkward. :)

Georg




----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Ah, thanks, I knew there was another term that had a new-style counterpart:
percent formatting vs brace formatting.

--
Sent from my phone, thus the relative brevity :)
On Feb 27, 2012 7:53 AM, "Georg Brandl" <g.brandl at gmx.net> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120227/a2958869/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 2/26/2012 5:38 PM, Nick Coghlan wrote:

Hooray!
Exact parallel and value-neutral.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Sun, Feb 26, 2012 at 3:14 PM, Terry Reedy <tjreedy at udel.edu> wrote:

Can we stop it with the "political correctness" already? The old style
is best named printf-style formatting because that's the origin of the
format language, and there are many other programming languages that
support the same formatting language (with minor variations). I care
less about what we call the new style -- "new style" or "format
method" both work for me.

I also would like to suggest that, even if the reality is that we
can't deprecate it today, *eventually*, at *some* *distant* point in
the future we ought to start deprecating printf-style formatting -- it
really does have a couple of nasty traps that keep catching people
unawares. In the mean time it doesn't hurt to use terms that make
people ever so slightly uneasy with using the old style for new code,
while also committing to not throwing it out until Python 4 comes
around.

That said, for consistency's sake, if you add formatting code to an
existing module that uses the old style, please stick to the old
style. And to avoid disasters, also please don't go on a library-wide
rampage of wholesale conversions. The time to start using the new
formatting is when writing new modules or packages, or possibly when
doing a major refactoring/upgrade of an existing module or package.

One thing I'd like to see happening regardless is support for
new-style formatting in the logging module. It's a little tricky to
think how that would work, alas -- should this be a property of the
logger or of the call?

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 26Feb2012 15:33, Guido van Rossum <guido at python.org> wrote:
| One thing I'd like to see happening regardless is support for
| new-style formatting in the logging module. It's a little tricky to
| think how that would work, alas -- should this be a property of the
| logger or of the call?

Surely the call? The caller doesn't necessarily know anything about the
loggers in play, so if a logger sprouts different message formating
syntax the caller is hosed.
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

A Master is someone who started before you did. - Gary Zukav



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Mark Lawrenc
Attributes: []Content: 
On 26/02/2012 23:33, Guido van Rossum wrote:

Just thinking out loud that a tool along the lines of 2to3 aimed 
specifically at changing string formatting would be some encouragement 
for people to switch.  Maybe a project for someone being looked after on 
the mentors ml?

-- 
Cheers.

Mark Lawrence.




----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Issue #14123: Explicitly mention
 that old style % string formatting has caveats
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 
On 02/26/2012 03:33 PM, Guido van Rossum wrote:

There already is some support.  logging.Formatter objects can be 
initialized with a "style" parameter, making this a property of the 
logger.  (New in 3.2.)

    http://docs.python.org/py3k/library/logging.html#formatter-objects

Is that what you had in mind?


//arry/

