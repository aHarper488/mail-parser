
============================================================================
Subject: [Python-Dev] Platform extension for distutils on
 other	interpreters than CPython
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Platform extension for distutils on
 other	interpreters than CPython
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Tarek Ziad? wrote:

You might want to try and catch up with Christian Heimes. He was going
to write a PEP to add one, but must have been caught up with other
things. See the thread starting at:
http://mail.python.org/pipermail/python-dev/2009-October/092893.html

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

