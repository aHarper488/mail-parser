
============================================================================
Subject: [Python-Dev] (Non-) use of weakref.WeakSet in concurrent.futures
 (was Re: futures API)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] (Non-) use of weakref.WeakSet in concurrent.futures
 (was Re: futures API)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Dec 11, 2010 at 6:07 AM, Brian Quinlan <brian at sweetapp.com> wrote:

Getting away from the flaws in the test code in this thread, is there
any particular reason futures is rolling its own variant of
weakref.WeakSet for the _thread_references cleanup sets? It seems to
me that using WeakSet would mean you could get rid of
_remove_dead_thread_references altogether (the implicit callbacks
would handle that part), and then a set() call in _python_exit would
give you concrete references to work with for cleanup purposes.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

