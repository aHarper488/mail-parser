
============================================================================
Subject: [Python-Dev] configparser: `__name__` special key is mad
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] configparser: `__name__` special key is mad
----------------------------------------
Author: =?utf-8?Q?=C5=81ukasz_Langa?
Attributes: []Content: 
Hello guys,
I want to sum up all strange things about the behaviour of `__name__`, a special key present in every section of a parser instance.

Hopefully it will be removed at once or in the worst case oficially deprecated and removed in 3.4.

1. There is a special `__name__` key in every section.
2. Except for the DEFAULTSECT.
3. `__name__` key is set for every section read from a file.
4. and not when adding by `add_section()`.
5. if `__name__` does exist, it's not visible in `parser.options('section')`
6. but it is here: `parser.has_option('section', '__name__') == True`
7. and can be obtained by `parser.get('section', '__name__')`
8. and can be changed by `parser.set('section', '__name__', 'ANY VALUE')`
9. and can be removed by `parser.remove_option('section', '__name__')`
10. even if the value is changed by `parser.set()`, it won't be written back to a file with `parser.write()`

All this looks like a feature that was not particularly complete and well defined when it was first created. Or possibly, it became rotten with time and now nobody is using it anyway. That way or the other, I couldn't come up with a valid use case for `__name__` with the current implementation. It doesn't serve any internal purpose and the *only* way you can actually get it is to `parser.get('section', '__name__')` which returns 'section' anyway. About as useless as it gets. Of course, one can go and take the internal parser._sections data structure of the parser but that's evil.

If I were to say what to do with all this, I'd simply remove all mentions of a special `__name__` key in configparser.py. I know that your default answer for that kind of idea is "no, backwards compatibility for the win". And I agree with that. In this case however we have a concept that is so broken that you can't actually use it.

Many of you are here for quite a long time, maybe someone can shed some light on this. Maybe I'm missing something.

-- 
Best regards,
?ukasz Langa
tel. +48 791 080 144
WWW http://lukasz.langa.pl/

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100730/41ec6b84/attachment.html>

