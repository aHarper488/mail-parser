
============================================================================
Subject: [Python-Dev] Thank yous
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
Now that Python 2.7 is out, I'd like to thank a few of the people who
made it possible:

- Martin for building Windows Installers to my schedule, maintaining
the tracker and PyPi, and providing lots of guidance and advice.
- Ronald for building Mac installers.
- Tarek for picking up the reins of distutils.
- Jesse for being responsive to last minute multiprocessing bugs.
- Georg for keeping the documentation excellent and numerous bug fixes.
- Victor for fixing obscure unicode bugs and segfaults.
- Ezio for plugging unicode holes and fixing docs.
- Alexander Belopolsky for taking up datetime.
- R. David Murray for picking up the email module.
- Alexandre Vassalotti for various backports.
- Mark for oiling Python's mathematical gears and explaining floating
point to me.
- Antoine for backporting the improved io module, memoryviews,
adopting the ssl module, and numerous bug fixes.
- Barry for mentoring me in the release process and testing out release.py.
- Dirkjan for always getting us closer to Mercurial migration.
- Andrew for writing "What's New in Python 2.7?" That's a lot of
changes to keep up with.
- And, of course, all of the people who reported bugs or submitted patches.


Thank you once again,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Sun, Jul 4, 2010 at 12:02 PM, Benjamin Peterson <benjamin at python.org> wrote:
..

I am honored that my contributions have been noticed, but would not be
able to contribute as much without support from Mark Dickinson.



----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 4 July 2010 17:02, Benjamin Peterson <benjamin at python.org> wrote:

And not forgetting Benjamin himself for managing the whole thing!

Paul.



----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
On Sun, Jul 4, 2010 at 7:16 PM, Paul Moore <p.f.moore at gmail.com> wrote:

+1. Thanks a lot for your hard work



----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Sun, Jul 4, 2010 at 1:26 PM, Tarek Ziad? <ziade.tarek at gmail.com> wrote:

Seriously Benjamin, you've done a great job.



----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 04.07.2010 22:45, schrieb Jesse Noller:

Totally!  *cheer*

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.




----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Sun, Jul 4, 2010 at 9:45 PM, Jesse Noller <jnoller at gmail.com> wrote:

+1.  A fantastic job.

Mark



----------------------------------------
Subject:
[Python-Dev] Thank yous
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Mon, Jul 05, 2010 at 08:46:57AM +0100, Mark Dickinson wrote:

Yeah, there is lot to learn from you in the way you lead the release.

Thank you!
Senthil

