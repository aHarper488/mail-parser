
============================================================================
Subject: [Python-Dev] RFC: PEP 445: Add new APIs to customize
	Python	memory allocators
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] RFC: PEP 445: Add new APIs to customize
	Python	memory allocators
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
Oh, it should be public, in my opinion.
We do exactly that when we embed python into UnrealEngine.  We keep pythons internal PyObject_Mem allocator, but have it ask UnrealEngine for its arenas.  That way, we can still keep track of python's memory usage from with the larger application, even if the granularity of memory is now on an "arena" level, rather than individual allocs.

K




