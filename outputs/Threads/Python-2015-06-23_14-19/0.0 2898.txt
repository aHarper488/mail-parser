
============================================================================
Subject: [Python-Dev] http://pythonmentors.com/
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Mark Lawrenc
Attributes: []Content: 
Hi all,

I'd never heard of this until some Dutch geezer whose name I'm now 
forgotten pointed me to it.  Had I known about it a couple of years ago 
it would have saved a lot of people a lot of grief.  Please could it be 
given a bit of publicity.

-- 
Cheers.

Mark Lawrence.

p.s. The Dutch geezer in question competes with Dr. Who for having the 
bast time travelling machine :)




----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
I've been trying to publicize it on twitter, my blog, google plus and elsewhere.  

help welcome. 


On Friday, February 10, 2012 at 8:27 PM, Mark Lawrence wrote:







----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Sat, Feb 11, 2012 at 03:38, Jesse Noller <jnoller at gmail.com> wrote:

It also appears in the first paragraph of "Contributing" in the dev
guide - which is pointed to by the main page at python.org (Core
Development link).

Mark, do you have a concrete idea of how it can be made more prominent?

Eli



----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Feb 11, 2012 at 1:14 PM, Eli Bendersky <eliben at gmail.com> wrote:

Mark didn't know about it because the core-mentorship list didn't
exist yet in the timeframe he's talking about :)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Sat, Feb 11, 2012 at 08:27, Nick Coghlan <ncoghlan at gmail.com> wrote:

Yes, but he *now* asks to give it more publicity. Hence my question.

Eli



----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Mark Lawrenc
Attributes: []Content: 
On 11/02/2012 03:14, Eli Bendersky wrote:

Eli, quite frankly no :(

The stock answer "put it on the main page at python.org" if actually 
followed up in all cases would result in something unreadable, as the 
page would be too noisy and displayed in something like Palatino size 1 
(if there is such a thing).

I'm just crossing my fingers and hoping that someone with far more than 
my own miniscule imagination can come up with a sensible suggestion.

-- 
Cheers.

Mark Lawrence.




----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

Well, I think the situation is pretty good now. If one goes to
python.org and is interested in contributing, clicking on the "Core
Development" link is a sensible step, right? It then leads to the
Devguide, which is a relatively new thing. Reading that opening page
of the devguide which is linked to from "Core Development" should give
aspiring contributors all the information they need, including a link
to the mentorship site.

Eli



----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Eli Bendersky writes:

 > Well, I think the situation is pretty good now.

I agree, but improvement is always possible.

 > If one goes to python.org and is interested in contributing,
 > clicking on the "Core Development" link is a sensible step, right? 

Maybe.  But a lot of the "Core Dev" links I've seen are maintained by
and for incumbent core devs, and are somewhat intimidating for new
users and developers.

How about moving the About | Getting Started link to the top level and
giving it a set of links like

- Standalone Scripting
- Extending Apps Using Python as Extension Language
- Developing Apps in Python (this is what I think of when I read the
  current Getting Started with Python page, FWIW YMMV)
- Developing Library Modules to Extend Python
- Developing Python (there's work you can do, too!)

This is similar to the existing About | Getting Started page, but more
accessible (in the sense of being an index, rather than a more verbose
description containing scattered links).  I think most of them can go
to existing pages.





----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 11/02/2012 12:00, Eli Bendersky a ?crit :

Maybe, depending on your knowledge of jargon.  How about rewording that
link to ?Contributing??

Regards



----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Wed, Feb 22, 2012 at 18:21, ?ric Araujo <merwok at netwok.org> wrote:

If you want to contribute to development, I think you'll know that a
link about development is relevant. If you want to contribute money, a
contribute link about development means you have to try again to give
away your money.



----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Brian Curtin writes:

 > If you want to contribute to development, I think you'll know that a
 > link about development is relevant.

For values of "you" in "experienced programmers", yes.  But
translators and tech writers don't consider what they do to be
"development."




----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Thu, Feb 23, 2012 at 01:15, Stephen J. Turnbull <stephen at xemacs.org> wrote:

I don't know what this is saying, but I'll guess it's some suggestion
that we should still name the link "Contributing". Keep in mind that
the current "Core Development" link on the front page goes directly to
http://docs.python.org/devguide/ -- getting this page in people's
hands earlier is a Good Thing. However, this is not a correct link
from something named "Contributing".

It would have to say "Contributing Code", but then it leaves out docs
and translations and our resident spelling bee contestants. Paint the
bike shed any way you want except the plain "Contributing" color,
please.



----------------------------------------
Subject:
[Python-Dev] http://pythonmentors.com/
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Brian Curtin writes:
 > On Thu, Feb 23, 2012 at 01:15, Stephen J. Turnbull <stephen at xemacs.org> wrote:
 > > Brian Curtin writes:
 > >
 > > ?> If you want to contribute to development, I think you'll know that a
 > > ?> link about development is relevant.
 > >
 > > For values of "you" in "experienced programmers", yes. ?But
 > > translators and tech writers don't consider what they do to be
 > > "development."
 > 
 > I don't know what this is saying, but I'll guess it's some suggestion
 > that we should still name the link "Contributing".

No, it's saying that there are a lot of potential contributors for
whom "Core Development" is pretty obviously not where they want to
go.  There should be a link that is the obvious place for them to go,
and there currently isn't one.

I don't have a problem with the presence of a "core development" link
that goes to the devguide.  I do have a problem with failing to invite
people who are not at present interested in contributing code or money
to contribute what they have.  The next question is how many links do
we want in that sidebar; I think there may already be too many.  But
I'm not a web designer to have a strong opinion on that.

