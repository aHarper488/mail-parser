
============================================================================
Subject: [Python-Dev] urllib bug in Python 3.2.1?
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: MRA
Attributes: []Content: 
Someone over at StackOverflow has a problem with urlopen in Python 3.2.1:

 
http://stackoverflow.com/questions/6892573/problem-with-urlopen/6892843#6892843

This is the code:

     from urllib.request import urlopen
     f = 
urlopen('http://online.wsj.com/mdc/public/page/2_3020-tips.html?mod=topnav_2_3000')
     page = f.read()
     f.close()

With Python 3.1 and Python 3.2.1 it works OK, but with Python 3.2.1 the
read returns an empty string (I checked it myself).



----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <4E35DC94.2090208 at mrabarnett.plus.com>,
 MRAB <python at mrabarnett.plus.com> wrote:

http://bugs.python.org/issue12576

-- 
 Ned Deily,
 nad at acm.org




----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

The bug is now fixed. Can you release a Python 3.2.2, maybe only with 
this fix?

Victor



----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/8/2011 4:26 PM, Victor Stinner wrote:

Any new release should also have
http://bugs.python.org/issue12540
which fixes another bad regression.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 09.08.2011 01:35, schrieb Terry Reedy:

I can certainly release a version with these two fixes.  Question is, should
we call it 3.2.2, or 3.2.1.1 (3.2.1p1)?

Georg




----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/9/2011 2:02 AM, Georg Brandl wrote:

I believe precedent and practicality say 3.2.2. How much more, if 
anything is up to you. The important question is whether Martin is 
willing to do a Windows installer, as 12540 only affects Windows.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Arfrever Frehtes Taifersar Arahesi
Attributes: []Content: 
2011-08-09 08:02:45 Georg Brandl napisa?(a):

I would suggest that a normal release with all changes committed on 3.2 branch be created.

-- 
Arfrever Frehtes Taifersar Arahesis
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: This is a digitally signed message part.
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110809/21ec4e00/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Aug 09, 2011, at 08:02 AM, Georg Brandl wrote:


Definitely 3.2.2.

-Barry

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110809/e602443f/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] urllib bug in Python 3.2.1?
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 09.08.2011 16:25, schrieb Barry Warsaw:

OK, 3.2.2 it is.  I will have to have a closer look at the other changes in
the branch to decide if it'll be a single(double)-fix only release.

Schedule would be roughly as follows: rc1 this Friday/Saturday, then I'm on
vacation for a little more than one week, so final would be the weekend of
27/28 August.

Georg


