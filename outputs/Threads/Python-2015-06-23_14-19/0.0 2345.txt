
============================================================================
Subject: [Python-Dev] Python 3 optimizations, continued, continued again...
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 3 optimizations, continued, continued again...
----------------------------------------
Author: stefan brunthale
Attributes: []Content: 
Hi guys,

while there is at least some interest in incorporating my
optimizations, response has still been low. I figure that the changes
are probably too much for a single big incorporation step. On a recent
flight, I thought about cutting it down to make it more easily
digestible. The basic idea is to remove the optimized interpreter
dispatch loop and advanced instruction format and use the existing
ones. Currently (rev. ca8a0dfb2176), opcode.h uses 109 of potentially
available 255 instructions using the current instruction format.
Hence, up to 149 instruction opcodes could be given to optimized
instruction derivatives. Consequently, a possible change would require
to change:
  a) opcode.h to add new instruction opcodes,
  b) ceval.c to include the new instruction opcodes in PyEval_EvalFrameEx,
  c) abstract.c, object.c (possible other files) to add the
quickening/rewriting function calls.

If this is more interesting, I could start evaluating which
instruction opcodes should be allocated to which derivatives to get
the biggest benefit. This is a lot easier to implement (because I can
re-use the existing instruction implementations) and can easily be
made to be conditionally compile-able, similar to the computed-gotos
option. Since the changes are minimal it is also simpler to understand
and deal with for everybody else, too. On the "downside", however, not
all optimizations are possible and/or make sense in the given limit of
instructions (no data-object inlining and no reference-count
elimination.)

How does that sound?

Have a nice day,
--stefan

