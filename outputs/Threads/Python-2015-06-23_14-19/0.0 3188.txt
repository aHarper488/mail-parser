
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Eric Snow's
 implementation of PEP 421.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Eric Snow's
 implementation of PEP 421.
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
Thanks for the second set of eyes, Brett.

On Jun 04, 2012, at 10:16 AM, Brett Cannon wrote:


I actually rewrote this section a bit:

   An object containing information about the implementation of the
   currently running Python interpreter.  The following attributes are
   required to exist in all Python implementations.


Here's how I rewrote it:

   :data:`sys.implementation` may contain additional attributes specific to
   the Python implementation.  These non-standard attributes must start with
   an underscore, and are not described here.  Regardless of its contents,
   :data:`sys.implementation` will not change during a run of the interpreter,
   nor between implementation versions.  (It may change between Python
   language versions, however.)  See `PEP 421` for more information.


Yes, PEP 421 guarantees them to be lower cased.

   *name* is the implementation's identifier, e.g. ``'cpython'``.  The actual
   string is defined by the Python implementation, but it is guaranteed to be
   lower case.


Fixed.


I'm not sure, but it seems to result in a cross-reference, and I see tildes
used elsewhere, so I guess it's some reST/docutils magic.  I left this one in
there.

Cheers,
-Barry



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Eric Snow's
 implementation of PEP 421.
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jun 04, 2012, at 11:39 AM, Brett Cannon wrote:


Good idea.  Done.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120604/690cd809/attachment.pgp>

