
============================================================================
Subject: [Python-Dev] Inconsistent script/console behaviour
Post Count: 17
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Sat, Sep 24, 2011 at 11:27 AM, Georg Brandl <g.brandl at gmx.net> wrote:



I've just hit this UX bug once more, but now I more prepared. Despite
Guido's proposal to move into python-ideas, I continue discussion here,
because:

1. It is not a proposal, but a defect (well, you may argue, but please,
don't)
2. This thread has a history of analysis of what's going wrong in console
3. This thread also has developer's decision that answers the question
    "why it's so wrong?" and "why it can't/won't be fixed"
4. Yesterday I've heard from a Java person that Python is hard to pick up
    and remembered how I struggled with indentation myself trying to
    'learn by example' in console

Right now I am trying to cope with point (3.). To summarize, let's speak
code
that is copy/pasted into console. Two things that will make me happy if they
behave consistently in console from .py file:

---ex1---
def some():
    print "XXX"
some()
---/ex1---

--ex1.output--
[ex1.py]
XXX
[console]
  File "<stdin>", line 3
    some()
       ^
SyntaxError: invalid syntax
--/ex1.output--


--ex2--
def some():
pass
--/ex2--

--ex2.output--
[ex2.py]
  File "./ex2.py", line 2
    pass
       ^
IndentationError: expected an indented block
[console]
  File "<stdin>", line 2
    pass
       ^
IndentationError: expected an indented block
--/ex2.output--


The second example already works as expected. Why it is not possible to fix
ex1? Guido said:


But the fix doesn't require changing the way interactive console treats
blank lines at all. It only requires to finish current block when a
dedented line is encountered and not throwing obviously confusing
SyntaxError. At the very least it should not say it is SyntaxError, because
the code is pretty valid Python code. If it appears to be invalid "Python
Console code" - the error message should say that explicitly. That would be
a correct user-friendly fix for this UX issue, but I'd still like the
behavior to be fixed - i.e. "allow dedented lines end current block in
console without SyntaxError". Right now I don't see the reasons why it is
not possible.

Please speak code when replying about use cases/examples that will be
broken - I didn't quite get the problem with "global scope if" statements.
-- 
anatoly t.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111215/3c72d25e/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 
Il 15 dicembre 2011 09:58, anatoly techtonik <techtonik at gmail.com> ha scritto:

You can't copy/paste multiline scripts into system shell either,
unless you append "\".
It's likely that similar problems exists in a lot of other interactive
shells (ruby?).
And that makes sense to me, because they are supposed to be used interactively.
It might be good to change this? Maybe.
Is the current behavior objectively wrong? No, in my opinion.

--- Giampaolo
http://code.google.com/p/pyftpdlib/
http://code.google.com/p/psutil/



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/15/2011 3:58 AM, anatoly techtonik wrote:


You state a controversial opinion as a fact and then request that others 
not discuss it. To me, this is a somewhat obnoxious hit-and-run tactic. 
If you do not want the point discussed, don't bring it up.

Anyway, I will follow your request and not argue. Since that opinion is 
a central point, not discussing it does not leave much to say.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Fernando Pere
Attributes: []Content: 
On Fri, 23 Sep 2011 16:32:30 -0700, Guido van Rossum wrote:


Apology for the advertising, but if the OP is really interested in that 
kind of behavior, then instead of asking for making the default shell more 
complex, he can use ipython which supports what he's looking for:

In [5]: def some():
   ...:   print 'xxx'
   ...: some()
   ...: 
xxx

and even blank lines inside functions (albeit only in certain locations):

In [6]: def some():
   ...: 
   ...:   print 'xxx'
   ...: some()
   ...: 
xxx


Now, the dances we have to do in ipython to achieve that are much more 
complex than what would be reasonable to have in the default '>>>' python 
shell, which should remain simple, light and robust.  But ipython is a 
simple install for someone who wants fancier features for interactive work.

Cheers,

f




----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Fernando Perez writes:

 > Apology for the advertising,

If there's any apologizing to be done, it's on Anatoly's part.  Your
post was short, to the point, information-packed, and should put a big
fat open-centered ideographic full stop period to this thread.




----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Mon, Dec 19, 2011 at 7:47 AM, Stephen J. Turnbull <stephen at xemacs.org>wrote:



Fernando clearly showed that IPython rocks, because CPython suxx. I don't
think anybody should apologize for the intention to fix this by enhancing
CPython, so as a python-dev subscriber you should be ashamed of yourself
for this proposal already. ;)


Thanks everyone else for explaining the problem with current
implementation. I'll post a follow-up as soon as I have a time to wrap my
head around the details and see for myself why the IPython solution is so
hard to implement.
-- 
anatoly t.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111220/15b6fbde/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
anatoly techtonik writes:

 > Fernando clearly showed that IPython rocks, because CPython suxx.

<sigh/>

No, IPython rocks because it focuses on doing one thing well:
providing an interactive environment that takes advantage of the many
features that Python provides in support.  CPython should do the same:
specifically, focus on the *language* that we all consider excellent
but still can be improved, and on the (still) leading implementation
of the language and the stdlib.[1]

 > so as a python-dev subscriber you should be ashamed of yourself for
 > this proposal already. ;)

ROTFLMAO!  No, I still think you're making an awfully big deal of
something that doesn't need fixing, and I wish you would stop.

Footnotes: 
[1]  Note that this *is* *one* task, because CPython has chosen a
definition of "language excellence" that includes prototype
implementation of proposed language features and "batteries
included".




----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Currently if you work in console and define a function and then
immediately call it - it will fail with SyntaxError.
For example, copy paste this completely valid Python script into console:

def some():
  print "XXX"
some()

There is an issue for that that was just closed by Eric. However, I'd
like to know if there are people here that agree that if you paste a
valid Python script into console - it should work without changes.
--
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Sep 23, 2011 at 4:25 PM, anatoly techtonik <techtonik at gmail.com> wrote:

You can't fix this without completely changing the way the interactive
console treats blank lines. None that it's not just that a blank line
is required after a function definition -- you also *can't* have a
blank line *inside* a function definition.

The interactive console is optimized for people entering code by
typing, not by copying and pasting large gobs of text.

If you think you can have it both, show us the code.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Yuval Greenfiel
Attributes: []Content: 
I agree that it should and it doesn't. I also recall that not having empty
lines between function/class definitions can cause indentation errors when
pasting to the console on my windows machine.

--Yuval
On Sep 23, 2011 7:26 PM, "anatoly techtonik" <techtonik at gmail.com> wrote:
http://mail.python.org/mailman/options/python-dev/ubershmekel%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110923/c23de6ab/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 9/23/2011 7:25 PM, anatoly techtonik wrote:

For this kind of multi-line, multi-statemenmt pasting, open an IDLE edit 
window for tem.py (my name) or such, paste, run with F5. I have found 
that this works for me than direct pasting.

A interactive lisp interpreter can detect end-of-statement without a 
blank line by matching a closing paren to the open paren that starts 
every expression.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Fri, Sep 23, 2011 at 18:49, Terry Reedy <tjreedy at udel.edu> wrote:

Braces-loving programmers around the world are feverishly writing a
PEP as we speak.



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 24.09.2011 01:32, schrieb Guido van Rossum:

While the former could be changed (I think), the latter certainly cannot.
So it's probably not worth changing established behavior.

Georg




----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Yuval Greenfiel
Attributes: []Content: 
Could you elaborate on what would be wrong if function definitions ended
only after an explicitly less indented line? The only problem that comes to
mind is global scope "if" statements that wouldn't execute when expected (we
actually might need to terminate them with a dedented "pass").
On Sep 24, 2011 4:26 AM, "Georg Brandl" <g.brandl at gmx.net> wrote:
wrote:
console:
http://mail.python.org/mailman/options/python-dev/ubershmekel%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110924/fb5dabec/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
You're right that in principle for function definitions there is no ambiguity.
But you also presented the downfall of that proposal: all multi-clause
statements will still need an explicit way of termination, and of course the
"pass" would be exceedingly ugly, not to mention much more confusing than the
current way.

Georg

Am 24.09.2011 11:53, schrieb Yuval Greenfield:







----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
I see a lot of flawed "proposals". This is clearly a python-ideas
discussion. (Anatoly, take note -- please post your new gripe there.)

In the mean time, there's a reasonable work-around if you have to
copy/paste a large block of formatted code:

   .
   .
   .
<put anything you like here>
   .
   .
   .
''')

The only thing that you can't put in there is a triple-quoted string
using single quotes.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Inconsistent script/console behaviour
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
On 24/09/2011 00:32, Guido van Rossum wrote:

Anatoly wants ipython's new qtconsole.

This "does the right thing" because it's a GUI app and so can manipulate 
the content on paste...

Not sure if you can do that in a console app...

cheers,

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk

