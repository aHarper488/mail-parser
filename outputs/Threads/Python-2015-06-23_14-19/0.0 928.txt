
============================================================================
Subject: [Python-Dev] [Python-checkins] r86327 - in
 python/branches/py3k: Doc/includes/email-mime.py
 Doc/includes/email-simple.py	Doc/library/smtplib.rst Doc/whatsnew/3.2.rst
 Lib/smtplib.py Lib/test/test_smtplib.py Misc/NEWS
In-Reply-To: <20101108171513.F0642FC70@mail.python.org>
References: <20101108171513.F0642FC70@mail.python.org>
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86327 - in
 python/branches/py3k: Doc/includes/email-mime.py
 Doc/includes/email-simple.py	Doc/library/smtplib.rst Doc/whatsnew/3.2.rst
 Lib/smtplib.py Lib/test/test_smtplib.py Misc/NEWS
In-Reply-To: <20101108171513.F0642FC70@mail.python.org>
References: <20101108171513.F0642FC70@mail.python.org>
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

If I?m not mistaken, you?re giving a message object to a method that
only accepts str or bytes.  That line should read s.send_message(msg).

Regards


