
============================================================================
Subject: [Python-Dev] Bug in generator if the generator in created in a C
	thread
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Bug in generator if the generator in created in a C
	thread
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

bugs.python.org is down so I'm reporting the bug here :-)

We have a crash in our product when tracing is enabled by
sys.settrace() and threading.settrace(). If a Python generator is
created in a C thread, calling the generator later in another thread
may crash if Python tracing is enabled.

 - the C thread calls PyGILState_Ensure() which creates a temporary
Python thread state
 - a generator is created, the generator has a reference to a Python
frame which keeps a reference to the temporary Python thread state
 - the C thread calls PyGILState_Releases() which destroys the
temporary Python thread state
 - when the generator is called later in another thread, call_trace()
reads the Python thread state from the generator frame, which is the
destroyed frame => it does crash on a pointer dereference if the
memory was reallocated (by malloc()) and the data were erased

To reproduce the crash, unpack the attached
generator_frame_bug.tar.gz, compile the C module using "python
setup.py build" and then run "PYTHONPATH=$(ls -d build/lib*/) python
test.py" (or just "python test.py if you installed the _test module).
You may need to use Valgrind to see the error, or call memset(tstate,
0xFF, sizeof(*tstate)) before free(tstate) in tstate_delete_common().

Calling the generator should update its reference to the Python state
thread in its frame. The generator may also clears frame->f_tstate (to
detect bugs earlier), as it does for frame->f_back (to avoid a
reference cycle). Attached patch implements this fix for Python 3.3.

Victor
-------------- next part --------------
A non-text attachment was scrubbed...
Name: generator_frame_bug.tar.gz
Type: application/x-gzip
Size: 1747 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120328/3a5d2c3c/attachment.bin>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: generator.patch
Type: text/x-patch
Size: 789 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120328/3a5d2c3c/attachment-0001.bin>

