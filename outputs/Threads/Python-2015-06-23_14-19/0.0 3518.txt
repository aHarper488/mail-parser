
============================================================================
Subject: [Python-Dev] Exception and ABCs / issue #12029
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Exception and ABCs / issue #12029
----------------------------------------
Author: =?UTF-8?Q?George=2DCristian_B=C3=AErzan?
Attributes: []Content: 
As per http://bugs.python.org/issue12029 , ABC registration cannot be
used for exceptions. This was introduced in a commit that fixed a
recursion limit problem back in 2008
(http://hg.python.org/cpython/rev/d6e86a96f9b3/#l8.10). This was later
fixed in a different way and improved upon in the 2.x branch in
http://hg.python.org/cpython/rev/7e86fa255fc2 and
http://hg.python.org/cpython/rev/57de1ad15c54 respectively.

Applying the fix from the 2.x branch for doesn't make any tests fail,
and it fixes the problem described in the bug report. There are,
however, two questions about this:

* Is this a feature, or a bug? I would say that it's a bug, but even
if it's not, it has to be documented, since one generally assumes that
it will work.
* Even so, is it worth fixing, considering the limited use cases for
it? This slows exception type checking 3 times. I added a new test to
pybench:

before:
       TryRaiseExceptClass:     25ms     25ms    0.39us    0.216ms
after:
       TryRaiseExceptException:     31ms     31ms    0.48us    0.214ms

However, that doesn't tell the whole story, since there's overhead
from raising the exception. In order to find out how much actually
checking slows down the checking, I ran three timeits, with the
following code:

1)
try: raise ValueError()
except NameError: pass
except NameError: pass
except ValueError: pass

2)
try: raise ValueError()
except NameError: pass
except ValueError: pass

3)
try: raise ValueError()
except ValueError: pass

Times are in ms:
       before      after
1       528.69      825.38
2       473.73      653.39
3       416.29      496.80
avgdiff  56.23      164.29

The numbers don't change significantly for more exception tests.

-- 
George-Cristian B?rzan



----------------------------------------
Subject:
[Python-Dev] Exception and ABCs / issue #12029
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Thanks for bringing this up. I've added my opinion to the tracker
issue -- I think it's a bug and should be fixed. We should have a
uniform way of checking for issubclass/isinstance.

--Guido

On Fri, May 11, 2012 at 3:33 AM, George-Cristian B?rzan
<gcbirzan at gmail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)

