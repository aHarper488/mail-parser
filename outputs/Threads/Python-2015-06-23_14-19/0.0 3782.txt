
============================================================================
Subject: [Python-Dev] TypeError: f() missing 1 required positional
	argument: 'x'
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
	argument: 'x'
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 20 Sep 2012 10:12:04 -0400
Benjamin Peterson <benjamin at python.org> wrote:

But since the error message gives the name of the parameter, there
doesn't seem to be a point to add that it's "positional": it can be
trivially deduced from the function signature.

Regards

Antoine.


-- 
Software development and contracting: http://pro.pitrou.net





----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
	argument: 'x'
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 9/20/2012 7:56 AM, Mark Dickinson wrote:

and optional params may or may not have an overt default object


Sometimes position is required, sometimes keyword is required, and 
usually both are allowed.


Moreover, all six combinations of passing mode and requirement are 
possible, although for Python functions, some combinations require setup 
code in addition to the header. Built-in print accepts an indefinite 
number of optional no-default position-only args followed by up to three 
optional defaulted keyword-only args. print() emits the default end='\n'.


I have strongly suggested that the docs not adds to the confusion in at 
least one tracker discussion.


For this example that would be sufficient, but your later message shows 
that we need a one-word abbreviations for positional-or-keyword: either 
something indicating that its default nature -- 'normal', 'standard', 
'flexible', 'usual', 'typical' -- or something indicating its dual 
nature (possibly coined or metaphorical -- 'pos-key', 'bi-mode', 
'dual-mode', 'Janus-like'.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
	argument: 'x'
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 9/20/2012 10:12 AM, Benjamin Peterson wrote:

They are positional-or-keyword without defaults.


Positional-or-keyword and positional-only also need to be distinguished.
'Positional' is ambiguous. One problem for standardized error messages 
is the the header info does not always tell the complete story.


I gave several suggestions for 'positional-or-keyword' in my response to 
Mark.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
	argument: 'x'
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 9/20/2012 11:52 AM, Guido van Rossum wrote:


One standard usage (and mine) is that parameters are the (local) names 
that arguments get bound to. I *believe* that Knuth used this also, but 
I cannot find a reference. Here is the CS part of
https://en.wikipedia.org/wiki/Parameters
See the last sentence.

"Computer science
Main article: Parameter (computer science)

When the terms formal parameter and actual parameter are used, they 
generally correspond with the definitions used in computer science. In 
the definition of a function such as

     f(x) = x + 2,

x is a formal parameter. When the function is used as in

     y = f(3) + 5 or just the value of f(3),

3 is the actual parameter value that is substituted for the formal 
parameter in the function definition. These concepts are discussed in a 
more precise way in functional programming and its foundational 
disciplines, lambda calculus and combinatory logic.

In computing, parameters are often called arguments, and the two words 
are used interchangeably. However, some computer languages such as C 
define argument to mean actual parameter (i.e., the value), and 
parameter to mean formal parameter."

-- 
Terry Jan Reedy


