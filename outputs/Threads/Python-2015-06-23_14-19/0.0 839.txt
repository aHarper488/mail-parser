
============================================================================
Subject: [Python-Dev] Python versions for Ubuntu 10.10 (Maverick Meerkat)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python versions for Ubuntu 10.10 (Maverick Meerkat)
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
I just wanted to let the python-dev community know about some tracks we had at
the recently concluded Ubuntu Developer Summit in Brussels.  Among the several
Python-related discussions, we talked about what versions of Python will be
supported and default in the next version of Ubuntu (10.10, code name Maverick
Meerkat, to be released in October).

If you're interested in following and participating in this discussion, I've
started a wiki page as the central place to collect information:

https://wiki.ubuntu.com/MaverickMeerkat/TechnicalOverview/Python

While we don't have consensus yet, and we have a lot of footwork to do before
we can make a decision, my goal is to include Python 2.6, 2.7, and 3.2 (beta)
in Ubuntu 10.10.  I'd like to make Python 2.7 the default if possible, and we
will need to get pre-approval to do stable release upgrades to Python 3.2 to
track the move to its final release, which will happen after Ubuntu 10.10. is
released.

It seems that most discussions are happening on the debian-python mailing list
right now.

they-don't-call-it-maverick-for-nuthin'-ly y'rs,
-Barry

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100518/2beab8fa/attachment.pgp>

