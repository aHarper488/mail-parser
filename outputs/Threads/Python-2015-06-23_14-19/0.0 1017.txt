
============================================================================
Subject: [Python-Dev] [Python-checkins] r86750
	-	python/branches/py3k/Demo/curses/life.py
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86750
	-	python/branches/py3k/Demo/curses/life.py
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 27/11/2010 12:22, Nick Coghlan wrote:

I *thought* that the Python policy was that English speakers wrote 
documentation in English and American speakers wrote documentation in 
American and that we *don't* insist on US spellings in the Python 
documentation?

Michael




-- 

http://www.voidspace.org.uk/

READ CAREFULLY. By accepting and reading this email you agree,
on behalf of your employer, to release me from all obligations
and waivers arising from any and all NON-NEGOTIATED agreements,
licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap,
confidentiality, non-disclosure, non-compete and acceptable use
policies (?BOGUS AGREEMENTS?) that I have entered into with your
employer, its partners, licensors, agents and assigns, in
perpetuity, without prejudice to my ongoing rights and privileges.
You further represent that you have the authority to release me
from any BOGUS AGREEMENTS on behalf of your employer.


