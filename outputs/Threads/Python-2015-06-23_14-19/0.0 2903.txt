
============================================================================
Subject: [Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
Post Count: 24
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

On Sun, 12 Feb 2012 19:04:30 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

Why hard links? Symlinks are much more introspectable. When looking at
a hard link I have no easy way to know it's the same as whatever other
file in the same directory.

I also don't understand this mention:

?The make install command in the CPython 3.x series will similarly
install the python3.x, idle3.x, pydoc3.x, and python3.x-config binaries
(with appropriate x), and python3, idle3, pydoc3, and python3-config as
hard links. This feature will first appear in CPython 3.3.?

This feature actually exists in 3.2 (but with a symlink, fortunately):

$ ls -la  ~/opt/bin/pydoc3
lrwxrwxrwx 1 antoine antoine 8 oct.  15 21:24 /home/antoine/opt/bin/pydoc3 -> pydoc3.2*


Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

There actually *is* an easy way, in regular ls: look at the link count.
It comes out of ls -l by default, and if it's >1, there will be an
identical file.

I agree with the question, though: this needs to be justified (but
there may well be a justification).


If you look at the patch, you'll notice that the only change is to
make the links hard links.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le dimanche 12 f?vrier 2012 ? 16:52 +0100, "Martin v. L?wis" a ?crit :

This doesn't tell me which file it is, which is practically useless if I
have both python3.3 and python3.2 in that directory.


This begs the question: why?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 

You can use 'ls -i' to print the inode, or you could use find's
'samefile' option.
But this is definitely not as straightforward as a it would be for a
symlink, and I'm also curious to know the reason behind this choice.



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 12.02.2012 17:04, schrieb Antoine Pitrou:

Well, you didn't ask for that, it does "to know it's the same as
whatever other file" nicely :-)

As Charles-Fran?ois explains, you can use ls -i for that, which isn't
that easy, but still straight-forward.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 12Feb2012 18:57, "Martin v. L?wis" <martin at v.loewis.de> wrote:
| Am 12.02.2012 17:04, schrieb Antoine Pitrou:
| > Le dimanche 12 f?vrier 2012 ? 16:52 +0100, "Martin v. L?wis" a ?crit :
| >>> Why hard links? Symlinks are much more introspectable. When looking at
| >>> a hard link I have no easy way to know it's the same as whatever other
| >>> file in the same directory.
| >>
| >> There actually *is* an easy way, in regular ls: look at the link count.
| >> It comes out of ls -l by default, and if it's >1, there will be an
| >> identical file.

Yeah! Somewhere... :-(

| > This doesn't tell me which file it is
| 
| Well, you didn't ask for that, it does "to know it's the same as
| whatever other file" nicely :-)

Sure, at the OS level. Not much use for _inspection_.

| As Charles-Fran?ois explains, you can use ls -i for that, which isn't
| that easy, but still straight-forward.

If the hardlink is nearby. Of course in this example it (almost
certainly?) is, but it needn't be. A symlink is a much better solution
to this problem because:

  - usability - "ls -l" shows it to the user by default

  - practicality:

      With a symlink, to find out what it attaches to you examine the
      symlink.
      
      With a hardlink you first examine a fairly opaque numeric attribute of
      "python2", and _then_ you examine every other filename on the system!
      Admittedly starting with "python2.*" in the same directory, but in
      principle in other places. Arbitrary other places.

IMO a symlink is far and away the better choice in this situation.

Cheers,
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

I need your clothes, your boots, and your motorcycle.
        - Arnold Schwarzenegger, Terminator 2



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Feb 13, 2012 at 6:42 AM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Kerrick did post a rationale in the last thread [1], but it never made
it into the PEP itself. The relevant comment:

==========
Also, I updated the PEP with the clarification that commands like
python3 should be hard links (because they'll be invoked from code and
are more efficient; also, hard links are just as flexible as symlinks
here), while commands like python should be soft links (because this
makes it clear to sysadmins that they can be "switched", and it's
needed for flexibility if python3 changes). This really doesn't
matter, but can we keep it this way unless there are serious
objections?
==========

I think Antoine makes a good point about ease of introspection when
you have multiple versions in the same series installed, so I'd be
fine with:
- updating the PEP recommendation to say that either form of link is
fine (with hard links marginally faster, but harder to introspect)
- noting that python.org releases will consistently use symlinks for
easier introspection via "ls -l"
- updating Makefile.pre.in to ensure that we really do consistently use symlinks

This does mean that launching Python may involve a slightly longer
symlink chain in some cases (python -> python2 -> python2.7), but the
impact of that is always going to be utterly dwarfed by other startup
costs.

[1] http://mail.python.org/pipermail/python-dev/2011-July/112322.html

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Sounds fine to me as well. When you update the PEP, please also update
the <TBD> mark with the actual issue number (or add it to the References).

For the patch, it seems that one open issue is OSX support, although
I'm unsure what exactly the issue is.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Feb 13, 2012, at 12:31 PM, Nick Coghlan wrote:


+1, and +1 for the PEP to be accepted.


Agreed about startup times.  However, does the symlink chain have to go in
this order?  Couldn't python -> python2.7 and python2 -> python2.7?  OTOH, I
seriously doubt removing one level of symlink chasing will have any noticeable
effect on startup times.

One other thing I'd like to see the PEP address is a possible migration
strategy to python->python3.  Even if that strategy is "don't do it, man!".
IOW, can a distribution change the 'python' symlink once it's pointed to
python2?  What is the criteria for that?  Is it up to a distribution?  Will
the PEP get updated when our collective wisdom says its time to change the
default?  etc.

Also, if Python 2.7 is being changed to add this feature, why can't Python 3.2
also be changed?  (And if there's a good reason for not doing it there, that
should be added to the PEP.)

Cheers,
-Barry



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Feb 14, 2012 at 7:07 AM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Hmm, the PEP builder on python.org may need a kick. I added the
tracker reference before starting this thread
(http://hg.python.org/peps/rev/78b94f8648fa), but didn't comment on it
since I expected the site to update in fairly short order.


I don't know either, but I'll take Ned's word for it if he says
there's something more he needs to do to make it work in the OS X
binaries.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Feb 14, 2012 at 8:08 AM, Barry Warsaw <barry at python.org> wrote:

I considered that, but thought it would be odd to make people
double-key a manual default version change within a series. It seemed
more logical to have "python" as a binary "python2/3" switch and then
have the python2/3 symlinks choose which version is the default for
that series. (I'll add that rationale to the PEP, though)


I have no idea, and I'm not going to open that can of worms for this
PEP. We need to say something about the executable aliases so that
people can eventually write cross-platform python2 shebang lines, but
how particular distros actually manage the transition is going to
depend more on their infrastructure and community than it is anything
to do with us.


Because Python 3.2 already installs itself as python3 and doesn't
touch the python symlink. Aside from potentially cleaning up the
choice of symlinks vs hardlinks in a couple of cases, the PEP really
doesn't alter Python 3 deployment at all.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Feb 14, 2012, at 12:38 PM, Nick Coghlan wrote:


Then I think all the PEP needs to say is that it is explicitly up to the
distros to determine if, when, where, and how they transition.  I.e. take it
off of python-dev's plate.

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120214/777e340f/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Feb 15, 2012 at 12:44 AM, Barry Warsaw <barry at python.org> wrote:

Yeah, good idea.

I'll also add an explicit link to the announcement of the Arch Linux
transition [1] that precipitated this PEP.

[1] https://www.archlinux.org/news/python-is-now-python-3/

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Does this need a pronouncement? Worrying about the speed of symlinks
seems silly, and exactly how the links are created (hard or soft,
chaining or direct) should be up to the distro; our own Makefile
should create chaining symlinks just so the mechanism is clear.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Feb 15, 2012, at 09:20 AM, Guido van Rossum wrote:


Works for me.
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120215/b96cbe8d/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Neil Schemenaue
Attributes: []Content: 
Guido van Rossum <guido at python.org> wrote:

I agree.  I wonder if a hard-link was used for legacy reasons.  Some
very old versions of Unix didn't have symlinks.  It looks like it
was introduced in BSD 4.2, released in 1983.  That seems a long time
before the birth of Python but perhaps some SysV systems were around
that didn't have it.  Also, maybe speed was more of a concern at
that time.  In any case, those days are long, long gone.

  Neil




----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
+1 for using symlinks where possible. In deploying Python to different
operating systems and filesystems I've often had to run a script to "fix"
the hardlinking done by make install because the deployment mechanism or
system couldn't be trusted to do the right thing with respect to minimising
installation size. Symlinks are total win when disk use is a concern, and
make intent clear. I'm not aware of any mainstream systems that don't
support them.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120216/7b9a2d08/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Wed, Feb 15, 2012 at 5:26 PM, Neil Schemenauer <nas at arctrix.com> wrote:

Actually I remember what was my motivation at the time (somewhere
between 1995-1999 I think) that I decided to use a hard link. It was
some trick whereby if you ran "make install" the target binary, e.g.
"python1.3", was removed and then overwritten in such a way that code
which was running it via "python" (a hard link to python1.3) would not
be disturbed. Then a new hard link would be created atomically.

But it was too clever, and it's long been replaced with a symlink.

Anyway, I don't think anyone is objecting against the PEP allowing symlinks now.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Feb 16, 2012 at 12:06 PM, Guido van Rossum <guido at python.org> wrote:

Yeah, the onus is just back on me to do the final updates to the PEP
and patch based on the discussion in this thread. Unless life
unexpectedly intervenes, I expect that to happen on Saturday (my
time).

After that, the only further work is for Ned to supply whatever
updates he needs to bring the 2.7 Mac OS X installers into line with
the new naming scheme.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Where exactly in the Makefile is that reflected? ISTM that the current
patch already covers that, since the framwork* targets are not concerned
with the bin directory.


What is the "framework bin directory"? The links are proposed for
/usr/local/bin resp. /usr/bin. The proposed patch already manages
these links across releases (the most recent install wins).

If you are concerned about multiple feature releases: this is not an
issue, since the links are just proposed for Python 2.7 (distributions
may also add them for 2.6 and earlier, but we are not going to make
a release in that direction).

It may be that the PEP becomes irrelevant before it is widely accepted:
if the sole remaining Python 2 version is 2.7, users may just as well
refer to python2 as python2.7.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Feb 16, 2012 at 8:01 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

My hope is that a clear signal from us supporting a python2 symlink
for cross-distro compatibility will encourage the commercial distros
to add such a link to their 2.6 based variants (e.g. anything with an
explicit python2.7 reference won't run by default on RHEL6, or
rebuilds based on that).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Feb 15, 2012 at 12:44 AM, Barry Warsaw <barry at python.org> wrote:

It turns out I'd forgotten what was in the PEP - the Notes section
already contained a lot of suggestions along those lines. I changed
the title of the section to "Migration Notes", but tried to make it
clear that those *aren't* consensus recommendations, just ideas
distros may want to think about when considering making the switch.

The updated version is live on python.org:
http://www.python.org/dev/peps/pep-0394/

I didn't end up giving an explicit rationale for the choice to use a
symlink chain, since it really isn't that important to the main
purpose of the PEP (i.e. encouraging distros to make sure "python2" is
on the system path somewhere).

Once MvL or Guido give the nod to the latest version, I'll bump it up
to approved.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
I'm away from the source for the next 36 hours.  I'll reply with patches by Saturday morning.
___
  Ned Deily
  nad at acm.org  --  []

 ..... Original Message .......
On Thu, 16 Feb 2012 11:01:39 +0100 ""Martin v. L?wis"" <martin at v.loewis.de> wrote:




----------------------------------------
Subject:
[Python-Dev] PEP 394 request for pronouncement (python2 symlink
 in *nix systems)
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Feb 16, 2012, at 09:54 PM, Nick Coghlan wrote:


That section looks great Nick, thanks.

I have one very minor quibble left.  In many places the PEP says something
like:

    "For the time being, it is recommended that python should refer to python2
    (however, some distributions have already chosen otherwise; see the
    Rationale and Migration Notes below)."

which implies that we may change our recommendation, but never quite says what
the mechanism is for us to do that.

You could change the status of this PEP from Draft to Active, which perhaps
implies a little more strongly that this PEP will be updated should our
recommendation ever change.  I suspect it won't though (or at least won't any
time soon).

If you mark the PEP as Final, we still have the option of updating the PEP
some time later to reflect new recommendations.  It might be worth a quick
sentence to that effect in the PEP.

As I say though, this is a very minor quibble, so just DTRT.  +1 and thanks
for your great work on it.

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120216/146e7a1d/attachment-0001.pgp>

