
============================================================================
Subject: [Python-Dev] importlib.find_loader
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] importlib.find_loader
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Yep, looks like a bug in the bootstrapping, failing to set __loader__
properly.
However, I also think the current handling of the "no __loader__ attribute"
case is a separate bug - since we generally aim to tolerate non-modules in
sys.modules (albeit somewhat grudgingly), importlib should probably be
using "getattr(mod, '__loader__', None)" rather than assuming the attribute
will always be present.

Cheers,
Nick.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130201/f8990254/attachment.html>



----------------------------------------
Subject:
[Python-Dev] importlib.find_loader
----------------------------------------
Author: Thomas Helle
Attributes: []Content: 
Am 01.02.2013 01:42, schrieb Nick Coghlan:

It also has the effect that reload does not work:

Type "help", "copyright", "credits" or "license" for more information.
 >>> import imp
 >>> import math
 >>> imp.reload(math)
<module 'math' (built-in)>
 >>> import signal
 >>> imp.reload(signal)
Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   File "C:\Python33-64\lib\imp.py", line 252, in reload
     return module.__loader__.load_module(name)
AttributeError: 'module' object has no attribute '__loader__'
 >>>


Thomas





----------------------------------------
Subject:
[Python-Dev] importlib.find_loader
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
Bugs #17098 and #17099 filed.


On Fri, Feb 1, 2013 at 1:56 AM, Thomas Heller <theller at ctypes.org> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130201/1b9c8546/attachment.html>



----------------------------------------
Subject:
[Python-Dev] importlib.find_loader
----------------------------------------
Author: Thomas Helle
Attributes: []Content: 
In Python3.3, I thought that every loaded module has a __loader__
attribute. Apparently this is not the case for a few builtin modules:

Python 3.3.0 (v3.3.0:bd8afb90ebf2, Sep 29 2012, 10:57:17) [MSC v.1600 64 
bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
 >>> import importlib
 >>>
 >>> def bug():
...     for name in list(sys.modules.keys()):
...         try:
...             importlib.find_loader(name)
...         except Exception as details:
...             print(name, type(details), details)
...
 >>> bug()
_frozen_importlib <class 'AttributeError'> 'module' object has no 
attribute '__loader__'
builtins <class 'AttributeError'> 'module' object has no attribute 
'__loader__'
signal <class 'AttributeError'> 'module' object has no attribute 
'__loader__'
importlib._bootstrap <class 'AttributeError'> 'module' object has no 
attribute '__loader__'
 >>>


However, the importlib docs talk about a ValueError instead of the 
AttributeError that I see above:

 > is returned (unless the loader would be None, in which case
 > ValueError is raised).

Is this a bug?

Thomas


