
============================================================================
Subject: [Python-Dev] Triagers and checkin access to the devguide repository
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Triagers and checkin access to the devguide repository
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Given that some of the dev guide docs cover triaging and other aspects
of managing issues on the tracker, does it make sense to offer
devguide checkin access to triagers that want it?

Regards,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

