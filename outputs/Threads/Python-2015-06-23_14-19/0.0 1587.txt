
============================================================================
Subject: [Python-Dev] devguide: Fix a silly statement.
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devguide: Fix a silly statement.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 09.02.2011 23:58, schrieb brett.cannon:

This statement is still somewhat silly, as a) you get a clone, not a checkout
and b) it is not read only in any way: you can commit just fine.  The only
difference will be the entry in .hg/hgrc pointing the default repo to something
you can't push to.

Skimming through, the whole section "Checking out the code" is still way too
SVN-point of viewy (e.g. you always get all branches anyway).

Georg




----------------------------------------
Subject:
[Python-Dev] devguide: Fix a silly statement.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Feb 9, 2011 at 23:10, Georg Brandl <g.brandl at gmx.net> wrote:

I'll take another pass, but do realize this needs to be something that
can easily be understood by someone who has never used hg before, so I
can't get too technically accurate while ignoring a possible base
ignorance of hg and DVCSs as a whole.



----------------------------------------
Subject:
[Python-Dev] devguide: Fix a silly statement.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 10.02.2011 19:27, schrieb Brett Cannon:

Well, it's no good to keep using CVCS terms and mislead users.  That the
"checkout" is not a checkout but a full repository is about the most important
fact about a hg (or any DVCS) clone.

Georg




----------------------------------------
Subject:
[Python-Dev] devguide: Fix a silly statement.
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Thu, Feb 10, 2011 at 10:08 PM, Georg Brandl <g.brandl at gmx.net> wrote:

+1
--
anatoly t.



----------------------------------------
Subject:
[Python-Dev] devguide: Fix a silly statement.
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

Well, to really use the Mercurial terms, what you have when you get
stuff from a remote server to your disk is a clone, which contains a
full repository and may contain a working copy (also called checkout).

IOW, ?check out? is used with Mercurial, as a synonym for ?update?, an
operation from the (local) repo to the working directory; the
CVCS-inspired mistake is to use that to refer to an operation from a
remote server to the local disk (?clone?, ?pull?).  HTH

Regards



----------------------------------------
Subject:
[Python-Dev] devguide: Fix a silly statement.
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Wed, Feb 16, 2011 at 1:05 PM, ?ric Araujo <merwok at netwok.org> wrote:

And that's the proper way to describe this.


Exactly.
--
anatoly t.

