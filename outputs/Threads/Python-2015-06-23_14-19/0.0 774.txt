
============================================================================
Subject: [Python-Dev] [Python-checkins] r79397 - in python/trunk:
	Doc/c-api/capsule.rst Doc/c-api/cobject.rst
	Doc/c-api/concrete.rst Doc/data/refcounts.dat
	Doc/extending/extending.rst Include/Python.h
	Include/cStringIO.h Include/cobject.h Include/datetime
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r79397 - in python/trunk:
	Doc/c-api/capsule.rst Doc/c-api/cobject.rst
	Doc/c-api/concrete.rst Doc/data/refcounts.dat
	Doc/extending/extending.rst Include/Python.h
	Include/cStringIO.h Include/cobject.h Include/datetime
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Thu, Mar 25, 2010 at 2:16 PM, Larry Hastings <larry at hastings.org> wrote:

This has always been a point of contention.  I'm not even sure what
the current official position is.


  -Fred

-- 
Fred L. Drake, Jr.    <fdrake at gmail.com>
"Chaos is the score upon which reality is written." --Henry Miller



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r79397 - in python/trunk:
	Doc/c-api/capsule.rst Doc/c-api/cobject.rst
	Doc/c-api/concrete.rst Doc/data/refcounts.dat
	Doc/extending/extending.rst Include/Python.h
	Include/cStringIO.h Include/cobject.h Include/datetime
----------------------------------------
Author: Reid Kleckne
Attributes: []Content: 
On Sun, Mar 28, 2010 at 3:25 PM, Larry Hastings <larry at hastings.org> wrote:

I'm curious what is considered reasonable/unreasonable breakage.  If
this breakage just requires a recompile, then doesn't it just
introduce an ABI incompatibility?  Aren't those allowed every minor
(point) release?  Or do people believe that this is more than an ABI
change?

Reid

