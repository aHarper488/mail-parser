
============================================================================
Subject: [Python-Dev] this is why we shouldn't call it a "monotonic clock"
 (was: PEP 418 is too divisive and confusing and should be postponed)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] this is why we shouldn't call it a "monotonic clock"
 (was: PEP 418 is too divisive and confusing and should be postponed)
----------------------------------------
Author: Zooko Wilcox-O'Hear
Attributes: []Content: 
On Thu, Apr 5, 2012 at 7:14 PM, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

Yep. As far as I can tell, nobody has a use for an unsteady, monotonic clock.

There seem to be two groups of people:

1. Those who think that "monotonic clock" means a clock that never
goes backwards. These people are in the majority. After all, that's
what the word "monotonic" means ? . However, a clock which guarantees
*only* this is useless.

2. Those who think that "monotonic clock" means a clock that never
jumps, and that runs at a rate approximating the rate of real time.
This is a very useful kind of clock to have! It is what C++ now calls
a "steady clock". It is what all the major operating systems provide.

The people in class 1 are more correct, technically, and far more
numerous, but the concept from 1 is a useless concept that should be
forgotten.

So before proceeding, we should mutually agree that we have no
interest in implementing a clock of type 1. It wouldn't serve anyone's
use case (correct me if I'm wrong!) and the major operating systems
don't offer such a thing anyway.

Then, if we all agree to stop thinking about that first concept, then
we need to agree whether we're all going to use the word "monotonic
clock" to refer to the second concept, or if we're going to use a
different word (such as "steady clock") to refer to the second
concept. I would prefer the latter, as it will relieve us of the need
to repeatedly explain to newcomers: "That word doesn't mean what you
think it means.".

The main reason to use the word "monotonic clock" to refer to the
second concept is that POSIX does so, but since Mac OS X, Solaris,
Windows, and C++ have all avoided following POSIX's mistake, I think
Python should too.

Regards,

Zooko

? http://mathworld.wolfram.com/MonotonicSequence.html

