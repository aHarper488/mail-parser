
============================================================================
Subject: [Python-Dev] EuroPython 2012 Language Summit Is In JEOPARDY *gasp*
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] EuroPython 2012 Language Summit Is In JEOPARDY *gasp*
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 


So far I've had exactly four reservations for the Language Summit at 
EuroPython 2012.  One of them is Guido--but he's threatening to skip it 
if we don't get more people and just go to the sprints.

Also, honestly I have next-to-nothing on the docket.  At this point 
we've just hit feature freeze for Python 3.3--or at least that's what 
people tell me. In general, anyone who wanted to get a language change 
in have already either just succeeded or just failed.  So after a flurry 
of activity I feel like we've hit a quiet period.  I mean, the trunk for 
3.4 won't even be open for a month or two.

So: if you're a core developer, and you're interested in attending the 
Language Summit in Florence on Saturday, please email me your RSVP.  
Also, if you have suggestions for things we should discuss, send those 
along too.  Please send all these emails directly to me, *off-list*, 
tonight or Thursday.

I'll send another email on Friday morning decreeing the fate of the 
Language Summit.

Thanks!


//arry/

p.s. I'm assured the room we'd have for the Language Summit has 
excellent air conditioning.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120704/db89d289/attachment.html>

