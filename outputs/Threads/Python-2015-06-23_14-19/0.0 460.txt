
============================================================================
Subject: [Python-Dev] Pickling named tuples on IronPython
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Pickling named tuples on IronPython
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
Hello Raymond,

Named tuples have compatibility code to enable them to work on 
IronPython without frame support, but unfortunately this doesn't allow 
pickling / unpickling of named tuples.

One fix is to manually set __module__ on the named tuples once created, 
but I wonder if it would be possible to change the API to better support 
this - perhaps a default __module__ or providing an optional argument to 
specify it at creation time?

Michael

-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.



