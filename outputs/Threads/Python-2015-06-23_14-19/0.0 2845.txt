
============================================================================
Subject: [Python-Dev] PYTHONPATH processing change from 2.6 to 2.7 and Mac
	bundle builder problems
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PYTHONPATH processing change from 2.6 to 2.7 and Mac
	bundle builder problems
----------------------------------------
Author: Barry Scot
Attributes: []Content: 
I'm trying to track down why bundlebuilder no longer works with python 2.7
to create runnable Mac OS X apps.

I have got as far as seeing that imports of modules are failing.

What I see is that sys.path does not contain all the elements from the
PYTHONPATH variable.

No matter what I put in PYTHONPATH only the first element is in sys.path.

In detail here is what I'm using to test this:

$ export PYTHONHOME=/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources
$ export PYTHONPATH=/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources:/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/Modules.zip

$ python2.7 -c "import sys;print sys.path"
['', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources']

$ python2.6 -c "import sys;print sys.path"
'import site' failed; use -v for traceback
['', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/Modules.zip', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python26.zip', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python2.6/', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python2.6/plat-darwin', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python2.6/plat-mac', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python2.6/plat-mac/lib-scriptpackages', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python2.6/lib-tk', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python2.6/lib-old', '/Users/barry/wc/svn/pysvn/WorkBench/Kit/MacOSX/tmp/pysvn_workbench_svn178-1.6.6-0-x86_64/WorkBench.app/Contents/Resources/lib/python2.6/lib-dynload']

Any insight into what has changed and what might need changing in bundlebuilder would be appreciated.

Barry


