
============================================================================
Subject: [Python-Dev] Omit Py_buffer struct from Stable ABI for Python 3.2?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Omit Py_buffer struct from Stable ABI for Python 3.2?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Currently [1], the implementation and the documentation for PEP 3118's
Py_buffer struct don't line up (there's an extra field in the
implementation that the PEP doesn't mention).

Accordingly, Mark and I think it may be a good idea to leave this
structure (and possibly related APIs) out of the stable ABI for the
3.2 release. I don't *think* it needs changing, but I'm not 100%
certain until we finish working through the problem and realign the
implementation and documentation. Applications and extension modules
that use this interface would still work - they would just have to
wait until 3.3 before they could consider migrating to the stable ABI.

Regards,
Nick.

[1] http://bugs.python.org/issue10181

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

