
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Fix email post-commit
	review comments.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Fix email post-commit
	review comments.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Apr 18, 2012 at 11:31 PM, brian.curtin
<python-checkins at python.org> wrote:

A slightly more traditional way to write that would be:

    name = Py_None;
    Py_INCREF(name);


Ditto.


However, *this* looks a lot more suspicious... accidental commit of
debugging code?

(if not for spotting this last problem, I wouldn't have even mentioned
the first two)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Fix email post-commit
	review comments.
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
We're seeing segfuilts on the buildbots now.  Example:

http://www.python.org/dev/buildbot/all/builders/x86%20Ubuntu%20Shared%203.x/builds/5715

On Wed, 18 Apr 2012 23:39:34 +1000, Nick Coghlan <ncoghlan at gmail.com> wrote:

