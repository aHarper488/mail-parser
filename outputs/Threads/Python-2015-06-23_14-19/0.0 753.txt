
============================================================================
Subject: [Python-Dev] Tracker opened/closed issue list: missing?
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Tracker opened/closed issue list: missing?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
I did not receive the usual Friday tracker post on issues opened and 
closed during the past week.
Did anyone else?
I am reading via gmane.

Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Tracker opened/closed issue list: missing?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Sat, Mar 20, 2010 at 11:02, Terry Reedy <tjreedy at udel.edu> wrote:


I actually haven't seen that mail in a few weeks (subscribed to the list via
my gmail account).
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100320/d7ce67ef/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Tracker opened/closed issue list: missing?
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sat, 20 Mar 2010 11:11:17 -0600, Brian Curtin <brian.curtin at gmail.com> wrote:

Darn.  I bet I broke it when I added the 'languishing' status.  I even
thought about that at the time but didn't follow up on it.  I'll see
if I can figure out where the script lives.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Tracker opened/closed issue list: missing?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 


My guess is that the Debian upgrade killed it.

Regards,
Martin


