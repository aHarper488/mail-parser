
============================================================================
Subject: [Python-Dev] Vagaries of "their" in English (was Re: Support the
 /usr/bin/python2 symlink upstream)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Vagaries of "their" in English (was Re: Support the
 /usr/bin/python2 symlink upstream)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Mar 5, 2011 at 10:40 AM, Guido van Rossum <guido at python.org> wrote:

If anyone wants to further explore this question, the Stack Exchange
on English usage is a decent place to start:
http://english.stackexchange.com/questions/192/is-it-correct-to-use-their-instead-of-his-or-her

What it boils down to is that "their" is the least bad of all of the
available options.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

