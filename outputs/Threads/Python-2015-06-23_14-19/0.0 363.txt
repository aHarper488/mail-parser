
============================================================================
Subject: [Python-Dev] [Python-checkins] r87445 -
	python/branches/py3k/Lib/numbers.py
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87445 -
	python/branches/py3k/Lib/numbers.py
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Dec 24, 2010 at 4:41 AM, eric.araujo <python-checkins at python.org> wrote:

Yes, there is, it just isn't a builtin - it lives in the operator module.


Changing the docstring to say "operator.index(self)" would be the
clearest solution here. (Choosing to accept arbitrary index objects as
integer equivalents is up to the object being indexed, just like
interpreting slices is - a dict, for example, will never invoke
__index__ methods).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87445 -
	python/branches/py3k/Lib/numbers.py
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 24/12/2010 02:08, Nick Coghlan a ?crit :
Defining object.__index__ with operator.index seems pretty circular to
me :)  http://docs.python.org/dev/reference/datamodel#object.__index__
does it, but it should be fixed IMO.  The difference between __int__ and
__index__ is also not clear.

I disagree.  __add__ is documented as implementing +, not operator.add.

I honestly don?t know what the best fix is.  We could copy the doc from
datamodel (?called whenever Python needs an integer object (such as in
slicing, or in the built-in bin(), hex() and oct() functions)?).  I?ve
been told on IRC to let Mark Dickison decide how to fix the docstrings
in the numbers module (deleting them being of course an option: magic
methods are documented in the language reference, they don?t need
docstrings).

Regards




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87445 -
	python/branches/py3k/Lib/numbers.py
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Dec 27, 2010 at 3:13 AM, ?ric Araujo <merwok at netwok.org> wrote:

Yes, the definition in the language reference could definitely be
improved to mention the semantics first, and then reference
operator.index second.

Possible wording "Indicates to the Python interpreter that the object
is semantically equivalent to the returned integer, rather than merely
supporting a possibly lossy coercion to an integer (i.e. as the
__int__ method allows for types like float and decimal.Decimal). This
allows non-builtin objects to be used as sequence indices, elements of
a slice definition, multiplies in sequence repetition, etc. Can be
invoked explicitly from Python code via operator.index()"

Removing the circularity from the definitions of __index__ and
operator.index doesn't have a great deal to do with the docstrings in
numbers.py, though.


That's because "+" is the idiomatic spelling. operator.index *is* the
Python level spelling of obj.__index__() - there is no other way to
spell it (other than calling the method explicitly, which is subtly
different).


Indeed. However, as a reference module for the numeric tower, it makes
a certain amount of sense to keep the docstrings in this particular
case.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87445 -
	python/branches/py3k/Lib/numbers.py
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/26/2010 7:01 PM, Nick Coghlan wrote:


If that is the intent of __index__, the doc should say so more clearly. 
That clarification would change my answer to your question about range.

 > (i.e. as the

It is both related and needed though. IE, it is hard to answer questions 
about what to to with .index if the intended meaning of .index is not 
very clear ;-).

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87445 -
	python/branches/py3k/Lib/numbers.py
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Fri, Dec 24, 2010 at 1:08 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Agreed.  Certainly "someobject[self]" isn't right.

(There's also a question about whether __index__ should really be
defaulting to int, but that's another issue...)

Mark

