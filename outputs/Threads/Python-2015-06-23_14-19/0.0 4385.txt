
============================================================================
Subject: [Python-Dev] PEP 435 doesn't help with bitfields [Was: Re: PEP 435
 - ref impl disc 2]
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 435 doesn't help with bitfields [Was: Re: PEP 435
 - ref impl disc 2]
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 5/13/2013 7:36 PM, Ethan Furman wrote:

There's been some talk of Enum-ing constants in the Socket library... 
I'm no socket programmer, so I'd have to go read the APIs to know if any 
of them are for bitfields which are typically combined together with | 
or + being the typical operators... and which would convert them to 
plain integers, and lose the reporting by name.  That's the problem I 
see with IntEnum used for bitfields.

For simple selection of choices, one choice per parameter, Enum will be 
great. But for bitfields, it is lacking.

Sorry if this sounds repetitious, but all the other times I've mentioned 
it, it has been in a big discussion of other stuff too.

Glenn
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130513/b9243ee3/attachment-0001.html>

