
============================================================================
Subject: [Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
Post Count: 25
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Jul 20, 2011 at 1:58 PM, P.J. Eby <pje at telecommunity.com> wrote:

I pushed this version up to the PEPs repo, so it now has a number
(402) and can be read in prettier HTML format:
http://www.python.org/dev/peps/pep-0402/

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 7/19/2011 8:58 PM, P.J. Eby wrote:

When I read about creating __path__ from sys.path, I immediately thought 
of the issue of programs that extend sys.path, and the above is the 
"workaround" for such programs.  but it requires such programs to do 
work, and there are a lot of such programs (I, a relative newbie, have 
had to write some).  As it turns out, I can't think of a situation where 
I have extended sys.path that would result in a problem for fancy 
namespace packages, because so far I've only written modules, not 
packages, and only modules are on the paths that I add to sys.path.  But 
that does not make for a general solution.

Is there some way to create a new __path__ that would reflect the fact 
that it has been dynamically created, rather than set from __init__.py, 
and then when it is referenced, calculate (and cache?) a new value of 
__path__ to actually search?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110720/710f4c6c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
On 07/20/2011 08:57 AM, P.J. Eby wrote:

I think we're back to normal PEP numbering. PEP 382 was also 3.x only.

Eric.



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: R. David Murra
Attributes: []Content: 

On Tue, 19 Jul 2011 23:58:55 -0400, "P.J. Eby" <pje at telecommunity.com> wrote:

In general the simplicity of the proposed mechanism and implementation
is attractive.  However, this bit of discussion struck me as sending the
wrong message.  We don't *want* something like the CPAN module
hierarchy.  I prefer to keep things as flat as practical.  Namespace
packages clearly have utility, but please let's not descend into
java-esq package hierarchies.

--
R. David Murray           http://www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Neal Becke
Attributes: []Content: 
I wonder if this fixes the long-standing issue in OS vendor's distributions.  In 
Fedora, for example, there is both arch-specific and non-arch directories: 
/usr/lib/python2.7 + /usr/lib64/python2.7, for example.  Pure python goes into 
/usr/lib/python2.7, and code including binaries goes into /usr/lib64/python2.7.  
But if a package has both, it all has to go into /usr/lib64/python2.7, because 
the current loader can't find pieces in 2 different directories.

You can't have both /usr/lib/python2.7/site-packages/foo and 
/usr/lib64/python2.7/site-packages/foo.

So if this PEP will allow pieces of foo to be found in 2 different places, that 
would be helpful, IMO.





----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Jeff Hard
Attributes: []Content: 
On Tue, Jul 19, 2011 at 8:58 PM, P.J. Eby <pje at telecommunity.com> wrote:

This part worries me slightly. Imagine a program as such:

datagen.py
json/foo.js
json/bar.js

datagen.py uses the files in json/ to generate sample data for a
database. In datagen.py is the following code:

try:
    import json
except ImportError:
    import simplejson as json

Currently, this works just fine, but if will break (as I understand
it) under the PEP because the json directory will become a virtual
package and no ImportError will be raised. Is there a mitigation for
this in the PEP that I've missed?


It may only be annoying, but it's still a breaking change, and a
subtle one at that. Checking __version__ is of course possible, but
it's never been necessary before, so it's unlikely there's much code
that does it. It also makes the fallback code significantly less neat.

- Jeff



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Eri
Attributes: []Content: 
On Wed, Jul 20, 2011 at 11:56 AM, Jeff Hardy <jdhardy at gmail.com> wrote:

This problem was brought up a few times on import-sig, but I don't
think a solution was ever decided on.

The best solution I can think of would be to have a way for a module
to mark itself as "finalized" (I'm not sure if that's the best
term--just the first that popped into my head).  This would prevent
its __path__ from being created or extended in any way.  For example,
if the json module contains `__finalized__ = True` or something of the
like, any `import json.foo` would immediately fail.

Of course, this would put all the onus on the json module to solve
this problem, and other modules might actually wish to be extendable
into packages, in which case you'd still have this problem.  In that
case there would need to be a way to mark a directory as not
containing importable code.  Not sure what the best approach to that
would be, especially since one of the goals of this PEP seems to be to
avoid marker files.

Erik



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Wed, Jul 20, 2011 at 11:04 AM, P.J. Eby <pje at telecommunity.com> wrote:

(I'm guessing you meant sys.modules in that last sentence.)

This is a really nice solution.  So a virtual package is not imported
until a submodule of the virtual package is successfully imported
(except for direct import of pure virtual packages).  It seems like
sys.virtual_packages should be populated even during a failed
submodule import.  Is that right?

Also, it makes sense that the above applies to all virtual packages,
not just pure ones.


It wouldn't be that hard to disallow their direct import entirely, but
still allow the indirect import when successfully importing a
submodule.  However, that would effectively imply that the import of
submodules of the virtual package will also fail.  In other words, it
may be a source of confusion if a package can't be imported but its
submodule can.

There is one remaining difference between the two types of virtual
packages that's derived from allowing direct import of pure virtual
packages.

When a pure virtual package is directly imported, a new [empty] module
is created and its __path__ is set to the matching value in
sys.virtual_packages.  However, an "impure" virtual package is not
created upon direct import, and its __path__ is not updated until a
submodule import is attempted.  Even the sys.virtual_packages entry is
not generated until the submodule attempt, since the virtual package
mechanism doesn't kick in until the point that an ImportError is
currently raised.

This isn't that big a deal, but it would be the one behavioral
difference between the two kinds of virtual packages.  So either leave
that one difference, disallow direct import of pure virtual packages,
or attempt to make virtual packages for all non-package imports.  That
last one would impose the virtual package overhead on many more
imports so it is probably too impractical.  I'm fine with leaving the
one difference.


(Guessing you meant sys.virtual_packages.)

Agreed.

FYI, last night I started on an importlib-based implementation for the
PEP and the above solution would be really easy to incorporate.

-eric




----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 7/20/2011 1:04 PM, P.J. Eby wrote:


While reading the PEP, I worried about this standard usage too but 
missed the scenario you imagined. Good catch.


If one actually wants to create a bare-as-possible empty module, one can 
do that now either with a directory containing an empty __init__.py or, 
even cleaner, imp.new_module. So there is no need for the new mechanism 
to ever duplicate either ;-). So +1 on improving back-compatibility.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Wed, Jul 20, 2011 at 2:44 PM, P.J. Eby <pje at telecommunity.com> wrote:

Good point, though I was talking about direct imports of pure virtual
packages (which you've indicated are disallowed by the current draft).


I meant that if the submodule import fails in the "impure" case, the
existing module does not end up with a __path__.


I see what you mean.  That case is probably more important than the
case of having a package that fails to import but submodules of the
package that succeed.

<snip>


Exactly.  That's part of why the importlib approach is so appealing to
me.  Brett really did a nice job.

-eric



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 7/20/2011 6:05 AM, P.J. Eby wrote:

Sure.  But there are a lot of things already imported by Python itself, 
and if this mechanism gets used in the stdlib, a program wouldn't know 
whether it is safe or not, to not bother with the 
pkgutil.extend_virtual_paths() call or not.

Plus, that requires importing pkgutil, which isn't necessarily done by 
every program that extends the sys.path ("import sys" is sufficient at 
present).

Plus, if some 3rd party packages are imported before sys.path is 
extended, the knowledge of how they are implement is required to make a 
choice about whether it is needed to import pkgutil and call 
extend_virtual_paths  or not.

So I am still left with my original question:


I think I would have to write

sys.path.append('foo')
import pkgutil
pkgutil.extend_virtual_paths('foo')

or I'd get an error.

And, in the absence of knowing (because I didn't write them) whether any 
of the packages I imported before extending sys.path are virtual 
packages or not, I would have to do this every time I extend sys.path.  
And so it becomes a burden on writing programs.

If the code is so boilerplate as you describe, should sys.path become an 
object that acts like a list, instead of a list, and have its append 
method automatically do the pkgutil.extend_virtual_paths for me?  Then I 
wouldn't have to worry about whether any of the packages I imported were 
virtual packages or not.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110720/c9adcddd/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 7/20/2011 4:03 PM, P.J. Eby wrote:

So that is a burden on every program.  Documentation would help, but it 
certainly makes updating sys.path much more complex -- 3 lines (counting 
import of pkgutil) instead of one, and the complexity of understanding 
why there is a need for it, when in simple cases the single line works 
fine, but it would be bug prone to have both ways.



 From what you said, it would complicate the solution for complex 
packaging tasks, but would return simple extensions of sys.path to being 
simple again.  Sounds like a good tradeoff, but I'll leave that to you 
and other more knowledgeable people to figure out the details and 
implementation... I snipped the explanation, because it is beyond my 
present knowledge base.


Please consider it.  I think your initial proposal solves some problems, 
but a version that doesn't complicate the normal, simple, extension of 
sys.path would be a much better solution, so I am happy to hear that you 
have ideas in that regard.  Hopefully, they don't complicate things too 
much more.  So far, I haven't gotten my head around packages as they 
presently exist (this __init__.py stuff seems much more complex than the 
simplicity of Perl imports that I was used to, although I certainly like 
many things about Python better than Perl, and have switched 
whole-heartedly, although I  still have a fair bit of Perl code to port 
in the fullness of time).  I think your proposal here, although 
maintaining some amount of backward-compatibility may require complexity 
of implementation, can simplify the requirements for creating new 
packages, to the extent I understand it.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110720/d28ac334/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Jul 21, 2011 at 9:03 AM, P.J. Eby <pje at telecommunity.com> wrote:

Setting __path__ to a sentinel value (imp.VirtualPath?) would break
less code, as hasattr(mod, '__path__') checks would still work.

Even better would be for these (and sys.path) to be list subclasses
that did the right thing under the hood as Glenn suggested. Code that
*replaces* rather than modifies these attributes would still
potentially break virtual packages, but code that modifies them in
place would do the right thing automatically. (Note that all code that
manipulates sys.path and __path__ attributes requires explicit calls
to correctly support current namespace package mechanisms, so this
would actually be an improvement on the status quo rather than making
anything worse).

I'll note that this kind of thing is one of the key reasons the import
state should some day move to a real class - state coherency is one of
the major use cases for the descriptor protocol, which is unavailable
when interdependent state is stored as module attributes. (Don't
worry, that day is a very long way away, if it ever happens at all)


Trying to change how packages are identified at the Python level makes
PEP 382 sound positively appealing. __path__ needs to stay :)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Wed, Jul 20, 2011 at 7:52 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

+1 as a solution to the problem Glenn brought up.  However, I'm still
not clear on how much code out there changes sys.path in the offending
way, forcing the need to provide a more implicit solution in this PEP
than extend_virtual_paths().  And in cases where sys.path *is*
changed, and it impacts some virtual package, how many places is that
going to happen in one project?  My guess is not many (and so not many
"boilerplate" calls).  Is it worth adding implicit __path__ updates
for that use case, rather than just the extend_virtual_paths()
function?

As an aside, my first reaction to Glenn's suggestion was "that would
be cool".  Would it be a pursuable option?  We can take this over to
import-sig if it is.

-eric



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Jul 21, 2011 at 11:20 PM, P.J. Eby <pje at telecommunity.com> wrote:

A no-indexing tuple wrapper for virtual package __path__ values that
automatically updates itself in response to parent path modifications
sounds good to me (errors shall not pass silently, etc). This also
allows virtual packages to be indicated clearly just through the type
of their __path__ attribute rather than having to look them up in the
import state.

I still like the idea of keeping sys.virtual_packages as a dict
mapping to the path values, though - it makes it easier to debug
erroneous __path__ replacement in virtual packages by checking
"pkg.__path__ is sys.virtual_package_paths[pkg.__name__]"

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 19 Jul 2011 23:58:55 -0400
"P.J. Eby" <pje at telecommunity.com> wrote:

I have a question.

If I have (on sys.path) a module "x.py" containing, say:

    y = 5

and (also on sys.path), a directory "x" containing a "y.py" module.

What is "from x import y" supposed to do?

(currently, it would bind "y" to its value in x.py)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Jul 22, 2011 at 9:35 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

It would behave the same as it does today: the imported value of 'y' would be 5.

Virtual packages only kick in if an import would otherwise fail.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 7/21/2011 5:00 PM, Antoine Pitrou wrote:

If I have (on sys.path), a directory "x" containing a "y.py" module, and 
later (on sys.path), another directory "x" containing a "y.py" module, 
what is "from x import y" supposed to do?

OR

If I have (on sys.path), a module "x.py" containing, say:

    y = 5

and later (on sys.path), another module "x.py" containing, say:

    y = 6

what is "from x import y" supposed to do?


I guess I don't see how this new proposal makes anything more confusing 
than it already is?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110721/1c4d6a31/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 21 Jul 2011 17:31:04 -0700
Glenn Linderman <v+python at g.nevcal.com> wrote:

It does. In your two examples, the "x.py" files (or the "x" directories)
live in two different base directories; imports are then resolved in
sys.path order, which is expected and intuitive.

However, you can have a "x.py" file and a "x" directory *in the same
base directory which is present in sys.path*, meaning sys.path can't
help disambiguate in this case.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 7/21/2011 5:38 PM, Antoine Pitrou wrote:

Ah yes.  It means there has to be one more rule for disambiguation, 
which Nick supplied.  Your case wasn't clear to me from your first 
description, however.  As long as there is an ordering, and it is 
documented, it is not particularly confusing, however.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110721/9b289a7d/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Jul 22, 2011 at 10:00 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

I don't see how it is any more confusing than any other form of module
shadowing.

For backwards compatibility reasons, the precedence model will be:

1. Modules and self-contained packages that can satisfy the import
request are checked for first (along the whole length of sys.path).
2. If that fails, the virtual package mechanism is checked

PEP 402 eliminates some cases of package shadowing by making
__init__.py files optional, so your scenario will actually *work*, so
long as the submodule name doesn't conflict with a module attribute.

*Today* if you have:

x.py
x.pyd
x.so
x/__init__.py

in the same sys.path directory, x.py wins (search order is controlled
by the internal order of checks within the import system - and source
files are first on that list).

With PEP 302, x.py still wins, but the submodules within the x
directory become accessible so long as they don't conflict with
*actual* attributes set in the x module.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Jul 22, 2011 at 10:53 AM, Glenn Linderman <v+python at g.nevcal.com> wrote:

The genuinely confusing part is that x.py still takes precedence, even
if it appears on sys.path *after* x/y.py.

However, we're forced into that behaviour by backwards compatibility
requirements. The alternative of allowing x/y.py to take precedence
has been rejected on those grounds more than once.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Jul 22, 2011 at 11:04 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

It's still an improvement on current Python. There a submodule can be
shadowed uselessly by something that doesn't even exist. For example:

x.py <-- No 'y' attribute
x/__init__.py <-- not needed in PEP 402
x/y.py

from x import y  <-- ImportError now, but would work in PEP 402

However, this does highlight an interesting corner case not yet
covered by the PEP: when building a virtual path to add to an existing
module, what do we do with directories that contain __init__.py[co]
files?

1. Ignore the entire directory (i.e leave it out of the created path)?
(always emit ImportWarning)
2. Ignore the file and add the directory to the created path anyway?
(never emit ImportWarning)
3. Ignore the file and add the directory to the created path anyway?
(emit ImportWarning if __init__.py is not empty)
4. Ignore the file only if it is empty, otherwise ignore the whole
directory? (emit ImportWarning if __init__.py is not empty)
5. Execute the file in the namespace of the existing module?

I suspect option 1 will lead to the fewest quirks, since it preserves
current shadowing behaviour for modules and self-contained packages.

Cheers,
Nick.


-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
Hi,
sorry for nitpicking, but...

On Wed, Jul 20, 2011 at 05:58, P.J. Eby <pje at telecommunity.com> wrote:
...

the '\' should be removed, right?

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Draft PEP: "Simplified Package Layout and
	Partitioning"
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
On Sat, Jul 30, 2011 at 14:57, ?ric Araujo <merwok at netwok.org> wrote:

Gaah, sorry for the noise then! (but at least I learnt a new thing!)

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi

