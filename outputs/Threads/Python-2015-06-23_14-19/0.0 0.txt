
============================================================================
Subject: [Python-Dev] trunk doctests fail to execute with 2.7 alpha
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] trunk doctests fail to execute with 2.7 alpha
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
I can not compile Python itself, so I use Alpha version to run tests
in trunk. Recent update broke successfully running tests. Any hints
why this happened and how to fix them back?


doctest (doctest) ... 66 tests with zero failures
Traceback (most recent call last):
  File "test\test_doctest.py", line 2492, in <module>
    test_main()
  File "test\test_doctest.py", line 2474, in test_main
    with test_support.check_warnings(*deprecations):
  File "C:\~env\Python27\lib\contextlib.py", line 84, in helper
    return GeneratorContextManager(func(*args, **kwds))
TypeError: check_warnings() takes no arguments (1 given)

-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] trunk doctests fail to execute with 2.7 alpha
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 01/04/2010 10:05, anatoly techtonik wrote:

Building Python on Windows can be done with free tools, so it should be 
possible for you to build Python.

See the instructions here:

http://python.org/dev/faq/#id8


When I run this test with a freshly built Python I get the following:

:\compile\python-trunk\PCbuild
 > python_d.exe ..\Lib\test\test_doctest.py
doctest (doctest) ... 66 tests with zero failures
doctest (test.test_doctest) ... 428 tests with zero failures
[42795 refs]

All the best,


Michael Foord

-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] trunk doctests fail to execute with 2.7 alpha
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Thanks. I've copied test/test_support.py form Lib into 2.7 alpha
directory and it seems to work.
Although it doesn't seem good to me to mix test support library with
tests themselves.
-- 
anatoly t.



On Thu, Apr 1, 2010 at 2:18 PM, Michael Foord <fuzzyman at voidspace.org.uk> wrote:



----------------------------------------
Subject:
[Python-Dev] trunk doctests fail to execute with 2.7 alpha
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 01/04/2010 13:15, anatoly techtonik wrote:
What do you mean by "it doesn't seem good to me to mix test support 
library with tests themselves"? Do you mean to have it in the same 
directory - where would you put it? It isn't *meant* to be a public 
library, it exists only to support the test framework. In Python 3 it 
has been renamed support.py, but lives in the same location.

Michael

-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] trunk doctests fail to execute with 2.7 alpha
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Thu, Apr 1, 2010 at 3:59 PM, Michael Foord <fuzzyman at voidspace.org.uk> wrote:

I mean that usually testing tools/libraries are separated from tests
itself, as well as data. test_support.py is not only located in the
same directory - it is even named in the same way. The test directory
looks like a mess with all these aux data files. But it is hard to
estimate if it would be worthy to separate testing framework from
tests and test data. It may happen that writing and debugging tests
become harder, because Python is not locked into some specific usage
domain.

-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] trunk doctests fail to execute with 2.7 alpha
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
anatoly techtonik wrote:

Tests start with test_*, the support files don't. The only odd one out
was test_support, and that has been fixed for 3.x.

The generalised test frameworks (unittest, doctest) do live in the
standard library. It's only the stuff specific to *our* unit tests that
lives in the test directory (and certainly, things from that directory
will sometimes get generalised and moved to the standard library -
that's how warning.catch_warnings was created).

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

