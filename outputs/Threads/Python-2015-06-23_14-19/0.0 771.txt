
============================================================================
Subject: [Python-Dev] [Python-checkins] r79397 -
 in	python/trunk:	Doc/c-api/capsule.rst
 Doc/c-api/cobject.rst	Doc/c-api/concrete.rst	Doc/data/refcounts.dat	Doc/extending/extending.rst
 Include/Python.h	Include/cStringIO.h	Include/cobject.h	Include/datetime.h
 Include/py_curses.h	Include/pycapsule.h	Include/pyexpat.h	Include/ucnhash.h
 Lib/test/test_sys.py	Makefile.pre.in	Misc/NEWS	Modules/_ctypes/callproc.c
 Modules/_ctypes/cfield.c	Modules/_ctypes/ctypes.h	Modules/_cursesmodule.c
 Modules/_elementtree.c	Modules/_testcapimodule.c	Modules/cStringIO.c	Modules/cjkcodecs/cjkcodecs.h
 Modules/cjkcodecs/multibytecodec.c	Modules/cjkcodecs/multibytecodec.h	Modules/datetimemodule.c
 Modules/pyexpat.c	Modules/socketmodule.c	Modules/socketmodule.h	Modules/unicodedata.c
 Objects/capsule.c	Objects/object.c	Objects/unicodeobject.c	PC/VS7.1/pythoncore.vcproj
 PC/VS8.0/pythoncore.vcproj	PC/os2emx/python27.def	PC/os2vacpp/python.def
 Python/compile.c Python/getargs.c
In-Reply-To: <4BAB9FAE.1070206@hastings.org>
References: <20100325005454.B1AC5FCCD@mail.python.org>	<4BAB2882.6070508@egenix.com>
	<4BAB9FAE.1070206@hastings.org>
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r79397 -
 in	python/trunk:	Doc/c-api/capsule.rst
 Doc/c-api/cobject.rst	Doc/c-api/concrete.rst	Doc/data/refcounts.dat	Doc/extending/extending.rst
 Include/Python.h	Include/cStringIO.h	Include/cobject.h	Include/datetime.h
 Include/py_curses.h	Include/pycapsule.h	Include/pyexpat.h	Include/ucnhash.h
 Lib/test/test_sys.py	Makefile.pre.in	Misc/NEWS	Modules/_ctypes/callproc.c
 Modules/_ctypes/cfield.c	Modules/_ctypes/ctypes.h	Modules/_cursesmodule.c
 Modules/_elementtree.c	Modules/_testcapimodule.c	Modules/cStringIO.c	Modules/cjkcodecs/cjkcodecs.h
 Modules/cjkcodecs/multibytecodec.c	Modules/cjkcodecs/multibytecodec.h	Modules/datetimemodule.c
 Modules/pyexpat.c	Modules/socketmodule.c	Modules/socketmodule.h	Modules/unicodedata.c
 Objects/capsule.c	Objects/object.c	Objects/unicodeobject.c	PC/VS7.1/pythoncore.vcproj
 PC/VS8.0/pythoncore.vcproj	PC/os2emx/python27.def	PC/os2vacpp/python.def
 Python/compile.c Python/getargs.c
In-Reply-To: <4BAB9FAE.1070206@hastings.org>
References: <20100325005454.B1AC5FCCD@mail.python.org>	<4BAB2882.6070508@egenix.com>
	<4BAB9FAE.1070206@hastings.org>
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
Larry Hastings wrote:

More later, have to run. For now, I'm with Antoine on this one:

 * adding support for PyCapsules to 2.7 is fine.

 * exposing the various module C APIs as PyCapsules in addition to
   the existing PyCObject C APIS is fines as well and indeed a good
   idea, since it makes porting 2.7 applications to 3.1 easier.

 * adding more C APIs using PyCapsules in Python 2.7 is fine as well,
   e.g. for the decimal module.

 * removing the PyCObject C APIs is no option in Python 2.7.

Please remember that you're dealing with Python 2.7. Major backwards
incompatible changes are not allowed in that branch and you're pretty
much breaking all 3rd party modules that have just started using e.g
the datetime module C API.

-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Mar 25 2010)
________________________________________________________________________

::: Try our new mxODBC.Connect Python Database Interface for free ! ::::


   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r79397 -
 in	python/trunk:	Doc/c-api/capsule.rst
 Doc/c-api/cobject.rst	Doc/c-api/concrete.rst	Doc/data/refcounts.dat	Doc/extending/extending.rst
 Include/Python.h	Include/cStringIO.h	Include/cobject.h	Include/datetime.h
 Include/py_curses.h	Include/pycapsule.h	Include/pyexpat.h	Include/ucnhash.h
 Lib/test/test_sys.py	Makefile.pre.in	Misc/NEWS	Modules/_ctypes/callproc.c
 Modules/_ctypes/cfield.c	Modules/_ctypes/ctypes.h	Modules/_cursesmodule.c
 Modules/_elementtree.c	Modules/_testcapimodule.c	Modules/cStringIO.c	Modules/cjkcodecs/cjkcodecs.h
 Modules/cjkcodecs/multibytecodec.c	Modules/cjkcodecs/multibytecodec.h	Modules/datetimemodule.c
 Modules/pyexpat.c	Modules/socketmodule.c	Modules/socketmodule.h	Modules/unicodedata.c
 Objects/capsule.c	Objects/object.c	Objects/unicodeobject.c	PC/VS7.1/pythoncore.vcproj
 PC/VS8.0/pythoncore.vcproj	PC/os2emx/python27.def	PC/os2vacpp/python.def
 Python/compile.c Python/getargs.c
In-Reply-To: <4BAB9FAE.1070206@hastings.org>
References: <20100325005454.B1AC5FCCD@mail.python.org>	<4BAB2882.6070508@egenix.com>
	<4BAB9FAE.1070206@hastings.org>
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
Larry Hastings wrote:

Just as reminder of the process we have in place for such changes:
Please discuss any major breakage on python-dev before checking in
the patch.


That's good, but then again: deliberate wrong use of APIs will
always cause crashes and at least I don't know of any report about
PyCObjects posing a problem in all their years of existence.


Sure, but forcing them is not a good idea, even less so if you can easily
expose both a PyCObject and PyCapsule interface to the same C API.


Please remember that PyCObjects are not only used internally
in CPython, but also in other 3rd party modules to expose C APIs
and those will generally have to support more than just the latest
Python release.

If you deprecate those C APIs, the process will at least have to cover
one more release, i.e. run through the whole deprecation process:

1. pending deprecation (2.7)
2. deprecation (2.8)
3. removal (2.9)

I think it's better to add a -3 warning when using PyCObjects in
2.7.


I know about those macros... I introduced that idea with mxDateTime
and then added the same logic in CPython in a couple of places
a long while ago, e.g. socketmodule.h. IIRC, Jim Fulton added
PyCObjects for exchanging module C APIs a few years before that.

Later on PyCObject_Import() was added to simplify the C API
import a bit.

A recompile is certainly a possibility to have 3rd party
modules switch to the capsule interfaces of the stdlib
modules, but we should still be more careful about this.

Wouldn't it be possible to have PyCObject_AsVoidPtr() et al.
work with PyCapsules as well ?

After all, the caller is only interested in the pointer and
doesn't really care about what object was used to wrap it.


True, but we've been happy with this vulnerability for years, just
as we've been happy with the fact that it's easy to crash the
VM by passing hand-crafted byte-code to it, or using ctypes to
call an OS function with the wrong parameters, etc.

Like I said: there are many ways to deliberately crash Python.

We don't have a concept of interface signatures in Python,
it's mostly based on trust.


This should be manageable with some aliasing.


That's a good idea. Yes, playing with fire is unsafe, but fire is
useful in a lot of places as well :-)


I'm not sure whether that would worth the effort. 3rd party modules
for Python 2.x are likely not going to use them, since they typically
have to support more than just Python 2.7.

E.g. look at Plone - they are still running on Python 2.4 and use
lots of 3rd party modules. It took Zope years to switch from 2.4 to
2.6. You have the same situation with other large systems, whether
OSS or not.

If a required 3rd party module would suddenly only support Python 2.7,
developers would either have to find a replacement that (still) works
for them (rather unlikely), or try to port their whole app to 2.7,
which often enough is not easily possible due to other restrictions.

This aspect is often not considered when discussing such backwards
incompatible changes - at least not in the early stages. Fortunately,
we have so far - and most of the time - found a way to keep everyone
happy.


See above. That's still an option as well, but I agree that either
making PyCObjects compatible with PyCapsules (via changes to
the PyCObject functions) or having them exist side-by-side is
a better option.

-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Mar 26 2010)
________________________________________________________________________

::: Try our new mxODBC.Connect Python Database Interface for free ! ::::


   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/

