
============================================================================
Subject: [Python-Dev] PEP 379 Python launcher for Windows - behaviour
	for #!/usr/bin/env python line is wrong
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 379 Python launcher for Windows - behaviour
	for #!/usr/bin/env python line is wrong
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Paul Moore <p.f.moore <at> gmail.com> writes:


Overall I think it's the right result. There's one other compatibility
clarification: at the moment, as allowed by the PEP, you can have launcher
flags in a line that starts with #!/usr/bin/env python, for example

#!/usr/bin/env python3.2-32 -u

In such a case the launcher would use the 3.2-32 suffix to indicate that
32-bit Python 3.2 is wanted, and pass the -u to the launched executable. I
assume that this behaviour should continue, and the Posix-compatible behaviour
being proposed should only apply for lines that contain

"#!/usr/bin/env python"

followed by whitespace.

Also, since we're making a backwards incompatible change, do people feel that
it needs to be switched on only in the presence of e.g. an environment variable
such as PYLAUNCH_SEARCHPATH, or should we just change the default behaviour
now and risk breaking user scripts which may rely on the current behaviour?

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] PEP 379 Python launcher for Windows - behaviour
	for #!/usr/bin/env python line is wrong
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Paul Moore <p.f.moore <at> gmail.com> writes:


I've taken a quick look at it, but I probably won't be able to make any
changes until the near the end of the coming week. Feel free to have a go;
the place to make changes will be near the call 

is_virt = parse_shebang(...)
...
if (!is_virt) {
   ...
}
else {
  /* In here is where the new logic will probably go. */
}

Also, the #define SEARCH_PATH needs to be uncommented to include the
find_on_path function. It also enables searching the path for customised
commands.

Regards,

Vinay




----------------------------------------
Subject:
[Python-Dev] PEP 379 Python launcher for Windows - behaviour
	for #!/usr/bin/env python line is wrong
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 



Hi Paul,

Sorry I haven't had a chance yet - real life is very busy at the moment. A pull request against the pypa version is fine, and I will get to it soon - thanks for your patience.

Regards,

Vinay Sajip


