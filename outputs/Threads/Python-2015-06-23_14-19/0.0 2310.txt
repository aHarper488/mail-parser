
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Avoid useless "++" at
	the end of functions
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Avoid useless "++" at
	the end of functions
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/26/2011 10:34 AM, Ronald Oussoren wrote:



Lets assume that the function currently does what it is supposed to do, 
as verified by tests. Then adding an unneeded increment in case the 
function is redefined in the future so that it needs more code strikes 
me as YAGNI. Certainly, reading it today with an unused increment 
suggests to me that something is missing that would use the incremented 
value. This strike me as different from adding a comma at the end of a 
Python sequence display.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Avoid useless "++" at
	the end of functions
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
So, given the discussions about this change, can you please revert it,
Victor?

Eric.

On 05/26/2011 08:07 AM, victor.stinner wrote:


