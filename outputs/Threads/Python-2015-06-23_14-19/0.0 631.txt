
============================================================================
Subject: [Python-Dev] Distutils reverted in py3k
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Distutils reverted in py3k
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Hello

As decided during the summit, I've reverted Distutils in the py3k
branch, to its release3.1-maint state. This was already done in 2.7.

I will only work on bugfixes for now on for distutils. Everything new
is done in distutils2. So if you have a feature request, use the
distutils2 component in the tracker.

Let me know if you have/see any problem this revert created.

Regards
Tarek

-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] Distutils reverted in py3k
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Note that I'll revert Doc/distutils as well, but I need to check first
with Ronald a few Mac OS X points.



----------------------------------------
Subject:
[Python-Dev] Distutils reverted in py3k
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 22 Jul, 2010, at 14:01, Tarek Ziad? wrote:


Not just a few point, the revert completely broke universal builds on OSX. I'm working on a fix.

Ronald


-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 3567 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100723/0eca8382/attachment-0001.bin>



----------------------------------------
Subject:
[Python-Dev] Distutils reverted in py3k
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 23 Jul, 2010, at 10:12, Ronald Oussoren wrote:


The build seems to work again, my fix is in r83066. There are a couple of test failures left, but those don't seem to be related to distutils.

Ronald

-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 3567 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100723/2c6fecab/attachment.bin>



----------------------------------------
Subject:
[Python-Dev] Distutils reverted in py3k
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
2010/7/23 Ronald Oussoren <ronaldoussoren at mac.com>:

mmm sorry about that... I though you backported everything in 3.1


Thanks.  The next task zould be to check what 3,2 doc should be kept
before Docs/distutils is reverted

Regards
Tarek

-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] Distutils reverted in py3k
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 23 Jul, 2010, at 10:47, Tarek Ziad? wrote:


3.1 is probably broken as well due to the recent move to setting PY_CFLAGS instead of CFLAGS.  That is next on my list.

Ronald



-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 3567 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100723/590bd31f/attachment.bin>

