
============================================================================
Subject: [Python-Dev] [Python-checkins] r85739 - in
 python/branches/issue4388/Lib/test: script_helper.py test_cmd_line.py
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r85739 - in
 python/branches/issue4388/Lib/test: script_helper.py test_cmd_line.py
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Oct 20, 2010 at 10:06 PM, victor.stinner
<python-checkins at python.org> wrote:

Isn't that "env=env" argument meant to be down in the call to
assert_python_ok, not in the print call? (your next checkin didn't
change this)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

