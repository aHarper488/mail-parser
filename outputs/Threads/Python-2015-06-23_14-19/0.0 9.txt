
============================================================================
Subject: [Python-Dev] 5 : 1
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] 5 : 1
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
I reviewed 5 issues and want to see http://bugs.python.org/issue7585
committed to Python 2.7


http://bugs.python.org/issue7443
test.support.unlink issue on Windows platform

 i race condition between os.unlink(), TortoiseSVN and os.open()
 - reporter changed OS and can't confirm bug anymore,
   need somebody with Windows and TortoiseSVN installed

http://bugs.python.org/issue6703
cross platform failure and silly test in doctest

 i non-crossplatform check for absolute path when 'module_relative' is True
 - added patch
 - need testcases

http://bugs.python.org/issue1659
Tests needing network flag?

 i skip tests that require network if network is not available
 - added patch
 - it is recommended to split issue to count skipped tests exactly and to
   probe which tests marked as 'require network' are actually don't

http://bugs.python.org/issue2810
_winreg.EnumValue fails when the registry data includes multibyte
unicode characters

 i WindowsError: [Error 234] More data is available when working with _winreg
 - original issue assumed that it is caused by multibyte string, however this
   can not be confirmed anymore
 - exception can be thrown when operating over long keys, this should be
   tested with WinAPI over ctypes, I propose to open new bug for long keys
 - another bug could be opened - it seems impossible to get values of Unicode
   keys with _winreg

http://bugs.python.org/issue3778
python uninstaller leave registry entries

 - confirmed for 2.6.5
 - need windows installer expert to create patch for cleaning empty
registry keys


-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] 5 : 1
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 02 Apr 2010 16:05:28 +0300, anatoly techtonik <techtonik at gmail.com> wrote:

It may be a little too close to the feature freeze to get this reviewed
and in to 2.7.  I'll take a look at it tonight (I'm in GMT -4) if I can
(and I think I can).

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] 5 : 1
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
R. David Murray <rdmurray <at> bitdance.com> writes:
gmail.com> wrote:

This looks like a bug fix, not a new feature.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] 5 : 1
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 02 Apr 2010 18:06:28 -0000, Antoine Pitrou wrote:

Well, yes, but it is also a behavior change.  Is that OK for a Beta?

--David



----------------------------------------
Subject:
[Python-Dev] 5 : 1
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
R. David Murray <rdmurray <at> bitdance.com> writes:

As soon as we consider the old behaviour a bug, I'd say yes.
Besides, this is the first beta, things get stricter afterwards.

Regards

Antoine.



