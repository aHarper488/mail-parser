
============================================================================
Subject: [Python-Dev] cpython (3.3): ctypes: AIX needs an explicit
 #include <alloca.h> to get alloca()
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.3): ctypes: AIX needs an explicit
 #include <alloca.h> to get alloca()
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 18.06.2013 12:56, schrieb Jeremy Kloth:

I have added HAVE_ALLOCA_H to configure:

http://hg.python.org/cpython/rev/7b6ae19dd116

Christian



----------------------------------------
Subject:
[Python-Dev] cpython (3.3): ctypes: AIX needs an explicit
 #include <alloca.h> to get alloca()
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2013/6/18 Christian Heimes <christian at python.org>:

Oh really? Portability is complex :-) I only tested on Linux, but I
expected this header to be part of the C standard...


Cool, thanks.

Victor



----------------------------------------
Subject:
[Python-Dev] cpython (3.3): ctypes: AIX needs an explicit
 #include <alloca.h> to get alloca()
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 18.06.2013 13:32, schrieb Victor Stinner:

It's neither C99 nor POSIX.1-2001. I guess it's just too fragile and not
portable enough. http://c-faq.com/malloc/alloca.html


You are welcome. alloca() is documented to require <alloca.h>. It merely
works with GCC because the compiler translates the function call to
inline code.

Christian


