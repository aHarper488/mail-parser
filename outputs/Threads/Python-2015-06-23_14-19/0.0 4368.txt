
============================================================================
Subject: [Python-Dev] Call for testing: generator finalization
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Call for testing: generator finalization
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

In http://bugs.python.org/issue17807 I've committed a patch to allow
generator finalization (execution of "finally" blocks) even when a
generator is part of a reference cycle. If you have some workload
which is known for problems with generator finalization (or otherwise
makes a heavy use of generators), it would nice to have some feedback on
this change.

(the commit is only on the default branch)

Regards

Antoine.




