
============================================================================
Subject: [Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
Post Count: 12
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Skip Montanar
Attributes: []Content: 

Where I can, I do, however I often find it difficult to come up with a
one-liner, especially one that mentions the parameters to functions.
If the one-line rule is going to be violated, I go whole hog and don't
bother with the blank line either.  (If I'm going to punch a hole in a
rule, I might as well create one big enough to comfortably walk
through.)

Skip



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
On 06/26/2013 07:08 PM, Skip Montanaro wrote:

Why do the parameters have to be in the summary?  You could do them better justice in the following paragraphs.

--
~Ethan~



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Wed, 26 Jun 2013 18:56:10 -0700,
Guido van Rossum <guido at python.org> a ?crit :

I don't always find it easy to summarize a function in one line.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Thu, Jun 27, 2013 at 1:21 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Neither do I. But I always manage to do it anyway. My reasoning is,
you can always leave out more detail and add it to the subsequent
paragraph.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 
On 06/26/2013 08:56 PM, Guido van Rossum wrote:

Argument Clinic could conceivably enforce this.  It could mandate that 
the first paragraph of the function docstring contain exactly one 
sentence (must end in a period, all embedded periods cannot be followed by
whitespace).  This would make some things nicer; I could automatically 
insert the per-parameter docstrings in after the summary.

Should it?


//arry/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130627/721fe993/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Yes on one line, capitalized, period. No on single sentence.

--Guido van Rossum (sent from Android phone)
On Jun 27, 2013 8:17 AM, "Larry Hastings" <larry at hastings.org> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130627/65ec1b82/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/27/2013 11:57 AM, Guido van Rossum wrote:

Complete and correct docstrings are somewhat rare in idlelib.
About half are missing.
Single lines typically omit the period.
Multiple lines often omit the blank line after the first.

I will take your reminder as permission to make changes as I review the 
code. I wish there were more good summary lines already.

My understanding is that SubclassTests and test_methods should not have 
docstrings.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
It was never my intention to enforce that everything has a docstring.
Just that if it does, it looks good.

On Thu, Jun 27, 2013 at 9:40 AM, Terry Reedy <tjreedy at udel.edu> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/27/2013 12:57 PM, Guido van Rossum wrote:

Ok, I won't add them when a function's name actually makes what it does 
obvious. But when I have to spend at least a few minutes reading the 
body to make sense of it, I will try to add the summary line that I wish 
had been there already.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Jun 27, 2013 at 10:20 AM, Guido van Rossum <guido at python.org> wrote:



+1

If you cannot summarize what your function does in one line, chances are it
is time to split your function, not the summary line.  Ideally, the name of
the function should already give a good idea of what it does.  A summary
line can just rephrase the same in a complete sentence.

I took a quick look at the stdlib and in many cases a summary line is
already there.  It is just the issue of formatting.

 250<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/suite.py#l250>
class _ErrorHolder(object):
251<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/suite.py#l251>
"""
252<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/suite.py#l252>
Placeholder
for a TestCase inside a result. As far as a TestResult
253<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/suite.py#l253>
is
concerned, this looks exactly like a unit test. Used to insert
254<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/suite.py#l254>
arbitrary
errors into a test suite run.
255<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/suite.py#l255>
"""
*http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/suite.py#l250*

81 <http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/main.py#l81>
class TestProgram(object):
82 <http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/main.py#l82>
"""A
command-line program that runs a set of tests; this is primarily
83 <http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/main.py#l83>
for
making test modules conveniently executable.
84 <http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/main.py#l84>
"""
*http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/main.py#l81*

109<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l109>
@failfast
110<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l110>
def addError(self, test, err):
111<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l111>
"""Called
when an error has occurred. 'err' is a tuple of values as
112<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l112>
returned
by sys.exc_info().
113<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l113>
"""
114<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l114>
self.errors.append((test, self._exc_info_to_string(err, test)))
115<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l115>
self._mirrorOutput = True
116<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l116>
117<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l117>
@failfast
118<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l118>
def addFailure(self, test, err):
119<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l119>
"""Called
when an error has occurred. 'err' is a tuple of values as
120<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l120>
returned
by sys.exc_info()."""
121<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l121>
self.failures.append((test, self._exc_info_to_string(err, test)))
122<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l122>
self._mirrorOutput = True
123<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l123>
124<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l124>
@failfast
125<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l125>
def addSubTest(self, test, subtest, err):
126<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l126>
"""Called
at the end of a subtest.
127<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l127>
'err'
is None if the subtest ended successfully, otherwise it's a
128<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l128>
tuple
of values as returned by sys.exc_info().
129<http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l129>
"""
*http://hg.python.org/cpython/file/44f455e6163d/Lib/unittest/result.py#l109*
*
*
589 <http://hg.python.org/cpython/file/44f455e6163d/Lib/pdb.py#l589> def
do_break(self, arg, temporary = 0):
590 <http://hg.python.org/cpython/file/44f455e6163d/Lib/pdb.py#l590> """b(reak)
[ ([filename:]lineno | function) [, condition] ]
591 <http://hg.python.org/cpython/file/44f455e6163d/Lib/pdb.py#l591> Without
argument, list all breaks.
*http://hg.python.org/cpython/file/44f455e6163d/Lib/pdb.py#l589*
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130627/e14af801/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Terry Reedy writes:

 > Ok, I won't add them when a function's name actually makes what it does 
 > obvious. But when I have to spend at least a few minutes reading the 
 > body to make sense of it, I will try to add the summary line that I wish 
 > had been there already.

+1  That's a *great* rule-of-thumb!



----------------------------------------
Subject:
[Python-Dev] Reminder: an oft-forgotten rule about docstring
 formatting (PEP 257)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/26/2013 9:56 PM, Guido van Rossum wrote:

fileinput has docstrings like

     """
     Return the file number of the current file. When no file is currently
     opened, returns -1.
     """
and, perhaps neater,
     """
     Return the name of the file currently being read.
     Before the first line has been read, returns None.
     """

 From the above, I presume these should become

     """
     Return the file number of the current file.

     When no file is currently opened, returns -1.
     """
and
     """
     Return the name of the file currently being read.

     Before the first line has been read, returns None.
     """

-- 
Terry Jan Reedy


