
============================================================================
Subject: [Python-Dev] Tracker reviews workflow and flags
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
I want to push some of my patches before 2.7 and use 5-1 rule for
that, but I can't come up with any review workflow other than mailing
status of my comments to the issues here. I can't mark issues in any
way. How about giving users ability to set flags or keywords? Maybe
entering a separate field like "Review status"?

Real world example with issue8151. It is an issue with a trivial patch
in it. Everything what is needed is to dispatch it to stable `commit
queue` and port to trunk. It is not 'easy' - it is 'trivial', but I
have no means to mark it as 'easy' either, so even this trivial fix
lies in tracker for three days waiting for review.

About 'easy' flag:
"6	easy	This is an easy task (e.g. suitable for GHOP or bug day beginners)"
It seems that it is for the issue that requires a patch, but if the
patch is already there and nobody wants to commit it - what should I
do?


Finally, review queue proposal.
To follow 5-1 rule, I need to review 5 issues, before mine is
reviewed, so I need some help from tracker.
1. I lack expertise in certain areas (C/C++, Mac OS, etc.), so I would
like to hide some issues under "Needs review" query, so they won't
bother me (if too many people marked issues in this way - the stats
may become a good reason for highly professional bug party)
2. I need ability to set "Needs review" flag back. Some issues were
once reviewed, but since then they've got modified and need further
comments.  The need the review again. That means pushed back _into the
end_ of patch queue.
3. Setting bug dependency by users. Often nobody wants to care about
issue, because it is just too long, there is much irrelevant comments
and nobody has time to read it. We don't have personal digg-like
comment filters to cope with the amount of text to be brain-loaded.
But it is possible to refactor such issue thread into separate
digestable issue/issues.


P.S.  About personal comment filters if anybody is interested in that.
Digg-like +1, -1 are good for voting, but for filtering it would be
nice to: 1. have several filters for different aspects of the thread,
2. have JS filter by marking individual phrases in comments and adding
ranges to filter using jquery / ajax

This way reviews will be more fun and easy.
-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Fri, Mar 19, 2010 at 03:09, anatoly techtonik <techtonik at gmail.com>wrote:


We already have "Patch Review" as a stage, and "needs review" as a flag,
which I feel is more than enough. If you don't have the privileges to modify
an issue, you can always comment on the issue with something like "can this
issue be set to the patch review stage?" Someone will probably take a look
at it and set it accordingly.

Real world example with issue8151. It is an issue with a trivial patch

Given that we have hundreds of issues with patches waiting for review, three
days isn't so bad :) As with any project, there are only so many people and
so much time to get the work done.



Post a comment on the issue asking what the status is. If an approved patch
has been sitting for a few weeks, make a comment. If it has been there for a
few days, I'd let it slide but keep it on your radar.



There already exists a component field which has Macintosh as an option, so
you could filter on that. Same goes for Windows.

Drilling down to the language(s) involved in the issue feels like just
another step that would get limited use. I do see what you are saying,
though, because I'm sure it's frustrating to want to look out there for a
nice pure-Python issue, then the first 10 issues you click on require C code
(or vice versa).



I don't think the "needs review" flag gets unset, but if it has been unset
manually and you've made changes, you can ask for another review. If you can
get the changes made quickly, you might be able to get the previous reviewer
to look at it again while it's still fresh in their mind.



I think that would take away from the goal of fixing the issue. Even some -1
comments could contain helpful information, but you might have to explicitly
click on that hidden comment to find out.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100319/3c75a976/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 19 Mar 2010 10:09:16 +0200, anatoly techtonik <techtonik at gmail.com> wrote:

With our current workflow, mailing the status of your reviews here is
pretty much the only option.  ("I've reviewed issues a, b, c, d, and e,
could someone please review y?").


Three days?  That's nothing :)


If an issue has a patch the 'easy' flag doesn't really apply any more,
but it doesn't always get deleted, either.

Issues that have a patch should get set to "patch review" stage, unless
tests are missing, in which case it should get set to "needs test".

Making changes like that (and also updating keywords) requires tracker
privileges, but we give those out more freely than we do commit privs.
You could try hanging out on #python-dev on Freenode, where there will
(usually) be people who can respond to your requests to update the status
of tracker issues.  Make enough good requests and we'll get you tracker
privs so you can do it yourself.  ("Enough" is probably a pretty small
number, by the way :)


To avoid issues involving C coding, try restricting your search to
'Library' issues.  There is C code in some library modules, but less so
than in the core ;).  It would be nice if you could search on more than
one component, but currently the interface doesn't allow that.  (You might
be able to do it by hand constructing a query string, I haven't checked.)


I'm not sure exactly what the 'needs review' keyword is for, frankly :)
Talking about pushing things back into the end of the patch queue makes
the assumption that there *is* a queue.  There isn't, there's just the
set of open issues with patches that committers look at when they can or
when they have an itch to scratch.  Making a comment on an issue (such
as doing a review) generates an email to everyone on the nosy list and
to the python-bugs list (for those people who are subscribed), and thus
in a fashion brings the issue to the 'top of the pile' in some sense and
for a short period.  Often things happen to the issue when a comment is
posted, but not always.  As with everything else, it depends on whether
anyone has both the time and the interest to take action.


-1 on comment filtering.  The text in the issue is the history of the
issue.  Reading it is part of doing an issue review.  I can't imagine that
the effort of manually blocking certain comments so that you didn't see
them when later looking at the issue again would be worth it.  It seems
to me you'd be much more likely to accidentally block something you should
be reading than you would be to make the issue usefully more readable.

If an issue needs to be refactored, new issues can be opened with
pointers to the old issue, and pointers added to the old issue to the new.
This can be done by anyone just by saying, eg: 'see issue 1234' (roundup
turns that into a link), and you can also suggest that the bug be added
to the dependency list by someone who can, if that is appropriate.
This happens fairly regularly with complex issues, or issues where we
discover more than one bug is involved.  Of course, it doesn't always
happen when it should.

The roundup code lives in the SVN repository, and you can set up your own
instance of the tracker to experiment with changes.  Patches are welcome,
although we have a few in the meta-tracker[1] already that haven't been
reviewed.  Currently Martin is pretty much the only one doing code-level
tracker work, and while I hope to get involved in that at some point it
hasn't happened yet.  So if you want to contribute some time in that
area, that would be great.  You could join the tracker-discuss list
(though it is very low volume).

[1] The 'report tracker problems' link on the lower left, if you
haven't noticed it.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: A.M. Kuchlin
Attributes: []Content: 
On Fri, Mar 19, 2010 at 10:22:05AM -0400, R. David Murray wrote:

I think Anatoly has a point, though; there are issues where the people
discussing the fix have come to a consensus, but none of them are
committers, or the committers involved are too busy.  There's a patch
ready to apply, but no committer knows about it.


Actually Library should pretty much always be pure-Python.  Issues
related to C modules such as socket are classified under the
'Extension modules' component.  Of course, sometimes the ultimate
cause will migrate (a bug in SocketServer.py is traced to a flaw in
socket, for example).

--amk



----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/19/2010 10:22 AM, R. David Murray wrote:


That should be 'see issue #1234' to get the autolink.
 From http://wiki.python.org/moin/TrackerDocs/

The tracker converts some specially formatted words in messages into 
links. The list includes
       "#<number>" links to the issue <number>
       "msg<number>" links to the message <number>
       "r<number>, "rev<number>", "revision <number>" links to 
svn.python.org displaying the checked in changes.




----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: Zvezdan Petkovi
Attributes: []Content: 

On Mar 19, 2010, at 1:15 PM, Terry Reedy wrote:


The list in the docs is not complete then.
David is right.  The words 'issue 1234' will become a link in roundup automatically.  As well as 'issue1234'.

	Zvezdan




----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 19 Mar 2010 14:09:28 -0400, Zvezdan Petkovic <zvezdan at zope.com> wrote:

I've updated the wiki page.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 19 Mar 2010 12:39:58 -0400, "A.M. Kuchling" <amk at amk.ca> wrote:

Yes, this is a general problem with the workflow.

This is one reason for the creation of the 'committers.rst' file in py3k:
submitters can find committers there to add to the nosy list of issues
that are ready for commit, so that they do notice.

However, as you note, sometimes 'ready' bugs sit in the tracker for
a while even when one or more committers is already nosy.  As someone
else said, that's the nature of a volunteer driven process.  The best
solution as far as I know is to expand the pool of volunteers, so I
hope Anatoloy is inspired to continue to contribute so he can eventually
become a committer!


Good point.  Or at least, that's the theory.  In practice bugs sometimes
get miscategorized.  So, Anatoly, if you find bugs like that you can ask that
they be properly recategorized.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
I am generally happy with the tracker workflow and in my experience,
issues that are lingering are lingering for a good reason.

I've decided to add my two cents to this thread because I've just had
a negative experience with how issue 8154 and my input to it were
handled.

I will not go into details here beyond referring to
http://bugs.python.org/issue8154, but if you follow the link, you'll
see that there was not a consensus on how the issue should be
addressed and even whether or not it was a bug.  Nevertheless the
patch was committed both to the trunk and to 2.6 without any answer to
my concerns and without  even an rNNN link to the committed revision.

I think it would be nice if committers would not cut the discussion
short without at least a note explaining their decision.



----------------------------------------
Subject:
[Python-Dev] Tracker reviews workflow and flags
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 19 Mar 2010 23:20:52 -0400, Alexander Belopolsky <alexander.belopolsky at gmail.com> wrote:

I think the reason this happened in this bug is that it *appeared*
as though the same problem had been encountered and already fixed in
python3, and all that was needed was a backport.  We were groping our
way toward deciding that that wasn't actually the case when Matthias
committed the patch.  Possibly my fault, since I said that I agreed that
it should be fixed.  As I've now noted on the tracker (and in the Ubuntu
tracker just for good measure), I don't think that commit was
appropriate, at least for 2.6, and probably not for 2.7 either.

I also hope that Matthias will put in commit numbers in the future,
since it is very helpful.  Even better, I hope that we can automate this
after the switch to Mercurial (but someone will need to write the code
to do it, of course...)

--David

