
============================================================================
Subject: [Python-Dev] how to decide on a Python 3 design for wsgiref
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] how to decide on a Python 3 design for wsgiref
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
Both the RM and BDFL agree that Python 3.2b1 should be held up until
we settle this wsgi matter. That makes it a question of how to settle
it.

Thinking out loud here to keep this discussion focused, I say we give
a deadline for PEPs to be submitted by October 15th. We then choose
two PEP dictators to make a call by November 1, get wsgiref cleaned up
ASAP, and get Python 3.2b1 out the door immediately thereafter. If
web-sig manages to come to an agreement as a whole before then we can
skip the PEPs, but if they have not managed to do this already then it
probably is not going to suddenly happen now under threat of
python-dev making the call for them by blessing a new wsgiref
implementation (happy to be proven wrong, though).



----------------------------------------
Subject:
[Python-Dev] how to decide on a Python 3 design for wsgiref
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Wed, Sep 15, 2010 at 4:46 PM, Brett Cannon <brett at python.org> wrote:

There's not just wsgiref; there's the possibility that other core and
standard library changes may be needed. For example, the discussion
from back in june:

http://mail.python.org/pipermail/python-dev/2010-June/100733.html

(that thread goes on for awhile)

I'd personally like to see a plan which provides a path for both
wsgiref, WSGI itself in python3 and the related changes to core which
might be predicated, that way we can have some level of confidence
we're not making the same misstep we made in the first place, making
it so painful for the web frameworks/gateways/etc.

jesse



----------------------------------------
Subject:
[Python-Dev] how to decide on a Python 3 design for wsgiref
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Wed, Sep 15, 2010 at 22:46, Brett Cannon <brett at python.org> wrote:

I think that's a very good goal. Given all the times it's come up on
the Web-SIG list (I even tried my hand at it once) and a consensus has
failed to form, it seems pretty important that we force some kind of
breakthrough.

One problem: Graham Dumpleton, who is certainly one of the more
knowledgeable people on this subject, will be on vacation "from late
September". It looks like he's gone at least until half October. It
would be a pity if we force a decision without his input.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] how to decide on a Python 3 design for wsgiref
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Sep 15, 2010 at 14:41, Dirkjan Ochtman <dirkjan at ochtman.nl> wrote:

It can get shifted to November 1 if needed. I just don't want it to
drag on forever as it will keep postponing Python 3.2.



----------------------------------------
Subject:
[Python-Dev] how to decide on a Python 3 design for wsgiref
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Wed, 2010-09-15 at 13:46 -0700, Brett Cannon wrote:

FWIW, I've submitted http://github.com/mcdonc/web3/blob/master/web3.rst
as a PEP (by sending a mail to peps at python.org).

- C




