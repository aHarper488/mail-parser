
============================================================================
Subject: [Python-Dev] cpython: Issue #15177: Added dir_fd parameter to
 os.fwalk().
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #15177: Added dir_fd parameter to
 os.fwalk().
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jun 25, 2012, at 02:17 PM, Antoine Pitrou wrote:


Wouldn't this be considered a new feature added past the beta feature freeze?
I didn't see any explicit permission from the 3.3 RM in the tracker issues for
this commit.  I don't read Georg's comment in msg163937 as providing that
permission.  Please either revert or have Georg approve the patch in the
tracker.

-Barry



----------------------------------------
Subject:
[Python-Dev] cpython: Issue #15177: Added dir_fd parameter to
 os.fwalk().
----------------------------------------
Author: Georg Brand
Attributes: []Content: 

-------- Original-Nachricht --------


How do you know it was rushed?  There is plenty time for testing during the beta period.


Relax.  It was with my permission.  IMO it is quite a logical extension of fwalk using the new features supporting dir_fd -- the whole point of fwalk() is working with directory fds instead of names.

cheers,
Georg
-- 
NEU: FreePhone 3-fach-Flat mit kostenlosem Smartphone!                                  
Jetzt informieren: http://mobile.1und1.de/?ac=OM.PW.PW003K20328T7073a

