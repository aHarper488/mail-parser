
============================================================================
Subject: [Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
Post Count: 42
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 12 Apr 2013 05:55:00 -0700, Eli Bendersky <eliben at gmail.com> wrote:

This looks great.  There's just one bit I don't understand.  I'm sure
it was discussed in the python-ideas thread, but the discussion of it
in the PEP does not provide any motivation for the decision.


[...]


This is the part that I don't understand.  Enums *clearly* have an
ordering, since the iteration order is defined and stable.  Why should
I not be allowed to compare values from the same Enum type?  There are
certainly use cases where that is very useful.

To give you a concrete use case: consider response codes from a client
server application constructed the way typical internet protocols are.
We might have:

    class MyAppCode(Enum):

        ok = 200
        command_complete = 201
        status_response = 202
        help_response = 203
        service_ready = 204
        signoff_accepted = 205

        temporary_failure = 400
        service_not_available = 401
        server_error = 402
        out_of_resources = 403

        error = 500
        syntax_error = 501
        command_not_implemented = 502
        access_denied = 503
        resource_not_found = 504


It can be quite handy to be able to say things like:

    code = myapp.operation(opstring)
    if MyAppCode.temporary_failure < code < MyAppCode.error:
        myframework.requeue(opstring, code=code)
        return False
    elif code > MyAppCode.error:
        raise AppError(code)
    ....

In talking to an existing internet protocol it would be natural to use
IntEnum and this issue would not arise, but I have recently worked on
an application that had *exactly* the above sort of enumeration used
internally, when it would have been totally appropriate to use Enum rather
than IntEnum.  The ap has several places where an ordered comparison
against the enum is used to check if a code is in the error range or not.

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Lele Gaifa
Attributes: []Content: 
Eli Bendersky <eliben at gmail.com> writes:


I'm not a native speaker and I found the above difficult to parse: is
there anything missing between "nor do they" and "and hence may exist"?

ciao, lele.
-- 
nickname: Lele Gaifax | Quando vivr? di quello che ho pensato ieri
real: Emanuele Gaifas | comincer? ad aver paura di chi mi copia.
lele at metapensiero.it  |                 -- Fortunato Depero, 1929.




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 12 Apr 2013 10:19:29 -0400, Davis Silverman <sinistersnare at gmail.com> wrote:

That's why I included the bit about iterating the values.  The ordering
*is* defined.  I find it much more surprising for that ordering to
be inaccessible via the comparison operators.

I think either the iteration order should be undefined (like a set
or dict), or the comparison operations should work.  I'd prefer
the latter, because of the use case I outlined.

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Terry Jan Reed
Attributes: []Content: 
On 4/12/2013 9:53 AM, Lele Gaifax wrote:

I am a native speaker and also do not understand the above quote.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 12 Apr 2013 10:50:44 -0400, Barry Warsaw <barry at python.org> wrote:

OK.


To take advantage of their incommensurability with other Enums.  It's
not a big deal, though; I'm more concerned that the API be internally
consistent.  I presume that one could always define an Enum subclass
and provide comparison methods if desired :)

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 12 Apr 2013 11:02:54 -0400, Barry Warsaw <barry at python.org> wrote:

I think TypeError is more consistent with the rest of Python:

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unorderable types: object() < object()

You get that automatically if you return NotImplemented from the
comparison methods.  I don't think you should be explicitly raising
NotImplemented.

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Russell E. Owe
Attributes: []Content: 
In article 
<CAP7+vJLUO0B0y+=Jcg8D=jQ3mggSAm1tb9ZzttO7nCrKE6Mh6Q at mail.gmail.com>,
 Guido van Rossum <guido at python.org> wrote:


I, too, would strongly prefer to see ordering within an enum. I use 
home-made enums heavily in my code and find ordering comparisons useful 
there.

Using intEnum is certainly doable, but that opens up the door to 
comparing values from different Enums, which is not something I'd want 
to allow.

I don't understand why order tests are seen as a bad thing, as long as 
the values have order (as they will in the common cases of string and 
int values). It seems the code must go out of its way to prohibit such 
tests.

In any case, I'm very pleased to have this library. it will get rid of 
much boilerplate in my code. It seems very well designed, and I'm really 
glad to see it supports subclassing to add values!

-- Russell




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 12 Apr 2013 15:33:02 -0400, Barry Warsaw <barry at python.org> wrote:

You are right, the problem of comparison of disparate types makes ordering
a non-starter.  But by the same token that means you are going to have to
be consistent and give up on having a sorted iteration and a stable repr:

...    aa = 1
...    bb = 2
...    cc = 'hi'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "./enum.py", line 103, in __repr__
    for k in sorted(cls._enums)))
TypeError: unorderable types: str() < int()

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 12 Apr 2013 14:06:55 -0700, Eli Bendersky <eliben at gmail.com> wrote:

I'm sure someone will come up with one :)

But seriously, even if you require all values to be of the same type,
that doesn't solve the sorting problem:

...    aa = object()
...    bb = object()
... 
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "./enum.py", line 103, in __repr__
    for k in sorted(cls._enums)))
TypeError: unorderable types: object() < object()
    
Now, you could *further* require that the type of enum values be
sortable....but that point you really have no excuse for not allowing
enum values to be compared :)

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 12 Apr 2013 18:35:58 -0400, Barry Warsaw <barry at python.org> wrote:

That's fine with me.  I'm just requesting that it be self-consistent.

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 12.04.13 15:55, Eli Bendersky wrote:

This is unnecessary because enumerations are iterable. 
Colors.__members__ is equal to [v.name for v in Colors] and the latter 
looks more preferable, because it does not use the magic method.


Does the enumeration's repr() use str() or repr() for the enumeration 
values? And same question for the enumeration's str().


How to get the enumeration value by its value?


It's unexpected if values of the enumeration values have the natural 
order. And values of the enumeration values *should be* comparable 
("Iteration is defined as the sorted order of the item values").


There is some ambiguity in the term "enumeration values". On the one 
hand, it's the singleton instances of the enumeration class (Colors.red, 
Colors.gree, Colors.blue), and on the other hand it is their values (1, 
2, 3).


Should enumeration values be hashable?

At least they should be comparable ("Iteration is defined as the sorted 
order of the item values").


What is ``isinstance(Shape.circle, int)``? Does PyLong_Check() return 
true for ``IntEnum`` values?


This does not apply to marshalling, I suppose? Perhaps this is worth to 
mention explicitly. There may be some errors of incompatibility.


Why the enumeration starts from 1? It is not consistent with namedtuple, 
in which indices are zero-based, and I believe that in most practical 
cases the enumeration integer values are zero-based.


The Python standard library has many places where named integer 
constants used as bitmasks (i.e. os.O_CREAT | os.O_WRONLY | os.O_TRUNC, 
select.POLLIN | select.POLLPRI, re.IGNORECASE | re.ASCII). The proposed 
PEP is not applicable to these cases. Whether it is planned expansion of 
Enum or additional EnumSet class to aid in these cases?





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 13.04.13 15:43, Eli Bendersky wrote:


May be use "enumeration items" or "enumeration members" if instances of 
the enumeration class have in mind? And left "enumeration names" and 
"enumeration values" for sets of corresponding attributes (.name and 
.value) of instances.


I think this requirements (hashability and comparability (for repr() and 
iteration)) should be mentioned explicitly.


But IntEnum is useless in such cases because a resulting mask will be an 
integer an will lost its convenient printable representation. There is 
almost no benefit of IntEnum before int constant.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 13.04.13 03:13, Glenn Linderman wrote:

For current flufl.enum implementations this requires values to be 
hashable. An alternative implementation can use comparability (which 
already required for repr() and iteration).





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sat, 20 Apr 2013 14:10:32 -0400, Barry Warsaw <barry at python.org> wrote:

I think definition order would be much better, but if we can't have that,
this is probably better than value order for non-int.

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 21 Apr 2013 08:34:39 +1000, Tim Delaney <timothy.c.delaney at gmail.com> wrote:

It seems strange to limit a new Python3 feature to the Python2 feature
set.  Just saying :)

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Rurp
Attributes: []Content: 
On 04/20/2013 01:42 PM, Barry Warsaw wrote:> On Apr 13, 2013, at 12:51 PM, Steven D'Aprano wrote:

Composite keys have been part of relational databases from
their inception.  If you want to store an enumeration value
in a database when non-unique values are possible, you can 
do so simply by storing the name, value pair; i.e. use two 
columns instead of one.  Of course this does not preclude 
storing just the value when you know they will be unique.
But it is not true that unique values are *required* for 
storing enumeration values in a database.




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Rurp
Attributes: []Content: 
On 04/20/2013 10:55 PM, Rurpy wrote:

I should have added that allowing mixed types for values (e.g.
as discussed in 
 http://mail.python.org/pipermail/python-dev/2013-April/125322.html)
is far more problematic for database storage than non-unique
values are.  Nearly all databases (Sqlite being an exception)
don't allow different types in a column.  (Not sure if mixed
types is still an open issue or not...)




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 21 Apr 2013 20:28:16 -0400, Barry Warsaw <barry at python.org> wrote:

I didn't read Nick's message as being about a use case.  The key word in
the sentence above was "complaining" :)

Regardless of the specific values involved, it is pretty much guaranteed
that if anything other than definition order is used we *will* get bug
reports/enhancement requests to fix it, on a regular basis.  We can choose
to live with that, but we should admit that it will will happen :)

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

Le Fri, 12 Apr 2013 05:55:00 -0700,
Eli Bendersky <eliben at gmail.com> a ?crit :

I'm having a problem with the proposed implementation. I haven't found
any mention of it, so apologies if this has already been discussed:







----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

(sorry for the previous message attempt - my mouse pointer hit the send
button before I was finished with it)


Le Fri, 12 Apr 2013 05:55:00 -0700,
Eli Bendersky <eliben at gmail.com> a ?crit :

I'm having a problem with the proposed implementation. I haven't found
any mention of it, so apologies if this has already been discussed:

...  a = 1
...  b = 2
... 
<class 'flufl.enum._enum.EnumValue'>
False
False


It would really be better if instances were actual instances of the
class, IMO.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 23 Apr 2013 15:44:58 +0200, Antoine Pitrou <solipsis at pitrou.net> wrote:

The first False looks correct to me, I would not expect an enum value to
be an instance of the class that holds it as an attribute.  The second
certainly looks odd, but what does it even mean to have an instance of
an Enum class?

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Tue, 23 Apr 2013 10:24:18 -0400,
"R. David Murray" <rdmurray at bitdance.com> a ?crit :

Perhaps I should have added some context:

...  a = 1
...  b = 2
... 
<EnumValue: C.a [value=1]>
<EnumValue: C.a [value=1]>


It is simply the same as a __getattr__ call.

That said, I don't see why it wouldn't make sense for an enum value to
be an instance of that class. It can be useful to write
`isinstance(value, MyEnumClass)`. Also, any debug facility which has a
preference for writing out class names would produce a better output
than the generic "EnumValue".

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 23 Apr 2013 16:33:02 +0200, Antoine Pitrou <solipsis at pitrou.net> wrote:

Ah.  I'd be looking for a bug every time I saw isinstance(value,
myEnumClass).  A better class name for values and an API for
getting that class from the EnumClass would be nice, though.
(So that you could write "isinstance(value, MyEnumClass.ValueClass)",
say.)

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Wed, 24 Apr 2013 00:22:40 +1000,
Nick Coghlan <ncoghlan at gmail.com> a ?crit :

You can work it around by making __doc__ a descriptor that behaves
differently when called on a class or an instance. There is a slight
metaclass complication because __doc__ is not writable on a class (but
I suppose enums are already using a metaclass, so it's not much of an
issue):


class docwrapper:
    def __init__(self, class_doc, instance_doc_func):
        self.class_doc = class_doc
        self.instance_doc_func = instance_doc_func

    def __get__(self, instance, owner):
        if instance is None:
            return self.class_doc
        else:
            return self.instance_doc_func(instance)

def instancedocwrapper(instance_doc_func):
    class metaclass(type):
        def __new__(meta, name, bases, dct):
            dct['__doc__'] = docwrapper(dct['__doc__'],
                                        instance_doc_func)
            return type.__new__(meta, name, bases, dct)
    return metaclass

class D(metaclass=instancedocwrapper(
           lambda self: "My instance:{}".format(self.x))):
    """My beautiful, documented class."""
    def __init__(self, x):
        self.x = x

class E(D):
    """My magnificent subclass."""

print("class doc:", D.__doc__)
print("subclass doc:", E.__doc__)
print("instance doc:", E(5).__doc__)


Note that the builtin help() function always displays the class's
__doc__, even when called on an instance which has its own __doc__.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 23 Apr 2013 08:11:06 -0700, Guido van Rossum <guido at python.org> wrote:

Well, I guess I can wrap my head around it :)  An Enum is an odd duck
anyway, which I suppose is one of the things that makes it worth adding.

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 23 Apr 2013 08:44:21 -0700, Guido van Rossum <guido at python.org> wrote:

I was alluding to the fact that in Python we usually work with instances
of classes (not always, I know, but still...), but with Enum we are
really using the class as a first level object.  Given that, breaking my
(questionable?) intuition about isinstance should not be unexpected.
And by that last phrase I meant to refer to the fact that getting this
right is obviously non-trivial, which is one of the things that makes
it worth adding as a Python feature.

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Philip Jenve
Attributes: []Content: 

On Apr 23, 2013, at 8:11 AM, Guido van Rossum wrote:


Furthermore, you could define methods/fields on enum values, like Java's enums.

E.g.: Errno.EBADF.strerror() -> 'Bad file descriptor'

Easily adapt to additional fields:

class Protocol(Enum):

  HOPOPT = 0, "IPv6 Hop-by-Hop Option"
  ICMP = 1, "Internet Control Message"
  IGMP = 2, "Internet Group Management"

  @property
  def value(self):
      return self._value[0]

  @property
  def description(self):
    return self._value[1]

--
Philip Jenvey



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Wed, 24 Apr 2013 11:37:16 +1200, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

Obviously they don't need to be, since people have discussed how to
implement this.

But I am primarily a Python programmer, so my intuition is based on my
Python experience, not on what other languages do.

Given:

    MyEnum(Enum):
       a = 1
       b = 2

I understood that 'a' and 'b' are class attributes, where the int value
had been transformed into instances of a (separate) value class rather
than being ints.  The fact that there was a specific value class
had been discussed.

If 'a' is now an instance of MyEnum, then I would expect that:

    MyEnum.a.b

would be valid (b being an attribute of the MyEnum class which should
therefore be accessible from an instance of that class).  That seems
a bit odd, and based on my Python-only mindset, I saw no particular
reason why an enum value *should* be instance of the enum class, since
it would lead to that oddity.  (Well, I'm not sure I was concious of
that *particular* oddity, but in general it seemed like an odd thing to
have class attributes of a class be instances of that class when the set
of class attributes was the most important thing about that class...).
It seemed more natural for the values to be instances of a separate
value class.

Now, given that I was viewing the Enum as being a collection of attributes
whose values were instances of a different class, what would it mean
to create an instance of the Enum class itself?  You'd have an instance
with access to those class attributes...but the getitem wouldn't work,
because that's on the metaclass.  You'd really want the Enum class to
be a singleton...the important thing was that it was an instance of the
metaclass, its instances would be pretty useless.

I don't have any *problem* with enum values being instances of the class.
If you make the attribute values instances of the enum class, then
yes instances of enum class have a meaning.  And then having attributes
of the class be instances of the class makes perfect sense in
hindsight. 

It's just not where *my* Python-only intuition, or my understanding of
the discussion, led me.

I feel like I'm revealing my ignorance or stupidity here, but what the
heck, that's what was going through my brain and I might as well own up
to it :).

--David



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Fri, 12 Apr 2013 05:55:00 -0700,
Eli Bendersky <eliben at gmail.com> a ?crit :

The PEP should mention how to get an enum from its raw value:

    >>> Colors[1]
    <EnumValue: Colors.red [value=1]>

or:

    >>> Colors(1)
    <EnumValue: Colors.red [value=1]>

It would perhaps be nice to have a .get() method that return None if the
raw value is unknown:

    >>> Colors(42)
    ...
    ValueError: 42
    >>> Colors.get(42)
    >>> 

Regards

Anroine.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Thu, 25 Apr 2013 05:21:50 -0700,
Eli Bendersky <eliben at gmail.com> a ?crit :

Well, it works in latest flufl.enum. Is there a difference between the
PEP and the implementation.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 04/25/2013 12:39 PM, Ethan Furman wrote:

Animals is *not* a class -- it just uses the class syntax as a convenient
way to set up the names used to construct the new type.  (This subtlety
is why the metaclass hook is reputed to make peoples' brains explode).


Not really.  Normal classes, when called, give you a new instance:  they
don't look up existing instances.



Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)
Comment: Using GnuPG with undefined - http://www.enigmail.net/

iEYEARECAAYFAlF5fa4ACgkQ+gerLs4ltQ7FSwCgzhcoXonMO/7W+xYMpM4EvtTj
nPIAnAkHtWxFMaU3dqfFUclNQkUcJ2FZ
=C0/7
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 25 Apr 2013 14:37:29 -0700, Ethan Furman <ethan at stoneleaf.us> wrote:

I haven't cared much about this particular bikeshed, but I find this a
somewhat compelling argument.  I'm working on a system that depends on
exactly this standard behavior of (most?) built in types in Python: if
you pass an instance or something that can be converted to an instance
to the type constructor, you get back an instance.  If Enums break that
paradigm(*), someone would have to write a custom class that provided
that behavior in order to use Enums with my system.  I wouldn't say that
was a show stopper, especially since my system may never go anywhere :),
but it certainly is an exemplar of the issue Eli is talking about.

--David

(*) Hmm.  NoneType(None) is still an error.



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
26.04.13 11:00, Greg Ewing ???????(??):

This is why enums are not subclassable in other languages (i.e. Java).




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
26.04.13 18:50, Larry Hastings ???????(??):

This example requires more than features discussed here. It requires an 
enum constructor.

class Planet(Enum):
     MERCURY = Planet(3.303e+23, 2.4397e6)
     VENUS   = Planet(4.869e+24, 6.0518e6)
     EARTH   = Planet(5.976e+24, 6.37814e6)
     MARS    = Planet(6.421e+23, 3.3972e6)
     JUPITER = Planet(1.9e+27,   7.1492e7)
     SATURN  = Planet(5.688e+26, 6.0268e7)
     URANUS  = Planet(8.686e+25, 2.5559e7)
     NEPTUNE = Planet(1.024e+26, 2.4746e7)

     def __init__(self, mass, radius):
         self.mass = mass     # in kilograms
         self.radius = radius # in meters

     @property
     def surfaceGravity(self):
         # universal gravitational constant  (m3 kg-1 s-2)
         G = 6.67300E-11
         return G * self.mass / (self.radius * self.radius)

     def surfaceWeight(self, otherMass):
         return otherMass * self.surfaceGravity

This can't work because the name Planet in the class definition is not 
defined.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
26.04.13 11:00, Greg Ewing ???????(??):

I propose do not use an inheritance for extending enums, but use an import.

class Colors(Enum):
   red = 1
   green = 2
   blue = 3

class MoreColors(Enum):
   from Colors import *
   cyan = 4
   magenta = 5
   yellow = 6

An inheritance we can use to limit a type of values.

class Colors(int, Enum): # only int values
   red = 1
   green = 2
   blue = 3

Colors.viridity = green





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
26.04.13 05:13, Nick Coghlan ???????(??):

What if use mixins? Shouldn't it work without magic?

class ColorMethods:

   def wave(self, n=1):
     for _ in range(n):
       print('Waving', self)

class Color(ColorMethods, Enum):

   red = 1
   white = 2
   blue = 3
   orange = 4





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
Thank you for your answers, Barry. Eli already answered me most of my 
questions.

20.04.13 22:18, Barry Warsaw ???????(??):

Yes, values can have different reprs and strs (but ints haven't). What 
of them uses repr of an enumeration item? I.e. what is str(Noises): 
'<Noises {dog: bark}>' or '<Noises {dog: "bark"}>'?

class Noises(Enum)
     dog = 'bark'

flufl.enum uses str(), but is it intentional? If yes, than it should be 
specified in the PEP.


Eli and you have missed my first question. Should enumeration values be 
hashable? If yes (flufl.enum requires hashability), then this should be 
specified in the PEP. If no, then how you implements __getitem__? You 
can use binary search (but values can be noncomparable) or linear search 
which is not efficient.





----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Nikolaus Rat
Attributes: []Content: 
Steven D'Aprano <steve at pearwood.info> writes:


In this case, wouldn't it be nicer to "decorate" those attributes that
are meant to be enum values? I think having to use the class keyword to
define something that really doesn't behave like an ordinary class is
pretty confusing, and the following syntax would be a lot easier to
understand at first sight:

class Insect(enum.Enum):
    ant = enum.EnumValue(1)
    bee = enum.EnumValue(2)

    @classmethod
    def spam(cls, args):
         pass

    def ham(self, args):
         pass


Obviously, EnumValue() would automatically assign a suitable number.


Best,

   -Nikolaus

-- 
 ?Time flies like an arrow, fruit flies like a Banana.?

  PGP fingerprint: 5B93 61F8 4EA2 E279 ABF6  02CF A9AD B7F8 AE4E 425C




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Philip Jenve
Attributes: []Content: 

On Apr 27, 2013, at 2:57 PM, Guido van Rossum wrote:


Call me crazy, but might I suggest:

class Planet(Enum, values='MERCURY VENUS EARTH'):
    """Planets of the Solar System"""

I've always wanted something similar for namedtuples, such as:

class Point(NamedTuple, field_names='x y z'):
    """Planet location with Sun as etc"""

Especially when the common idiom for specifying namedtuples w/ docstrings looks similar but leaves a lot to be desired w/ the required duplication of the name:

class Point(namedtuple('Point', 'x y z')):
    """Planet location with Sun as etc"""

(Raymond's even endorsed the former):

http://bugs.python.org/msg111722

--
Philip Jenvey



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Nikolaus Rat
Attributes: []Content: 
Steven D'Aprano <steve at pearwood.info> writes:
[...]


However, without knowing that the MetaEnum metaclass will do some magic
here, there's no way to know that there's anything special about red,
blue and green. So I think there's actually a lot of implicit stuff
happening here.

In contrast,

class Example(metaclass=MetaEnum):
     red = EnumValue(1)
     blue = EnumValue(2)
     green = EnumValue(lambda: 'good lord, even functions can be
     enums!')
     
     def __init__(self, count=3):
         self.count = count

     def spam(self):
         return self.count * self.food

Makes it very clear that red, blue will not be attributes of type int,
even if one has never heard of Enums or metaclasses before.

I don't think this syntax is making the common case hard. By the same
logic, you'd need to introduce C-style i++ postincrement because having
just "i += x" makes the common case with x=1 "hard" as well.


Best,

   -Nikolaus

-- 
 ?Time flies like an arrow, fruit flies like a Banana.?

  PGP fingerprint: 5B93 61F8 4EA2 E279 ABF6  02CF A9AD B7F8 AE4E 425C




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Nikolaus Rat
Attributes: []Content: 
Guido van Rossum <guido at python.org> writes:

I think this is actually a big advantage. It makes it obvious that
something special is going on without having to know that IntEnum uses a
special metaclass.


Best,

   -Nikolaus

-- 
 ?Time flies like an arrow, fruit flies like a Banana.?

  PGP fingerprint: 5B93 61F8 4EA2 E279 ABF6  02CF A9AD B7F8 AE4E 425C




----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the Python
	standard library
----------------------------------------
Author: Philip Jenve
Attributes: []Content: 

On Apr 27, 2013, at 6:09 PM, Guido van Rossum wrote:


So does the convenience API outlined in the PEP, so this is just an alternative to that.

--
Philip Jenvey


