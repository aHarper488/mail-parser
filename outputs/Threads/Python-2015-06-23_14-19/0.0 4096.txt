
============================================================================
Subject: [Python-Dev] PEP 446: Add new parameters to configure the
	inherance of files and for non-blocking sockets
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 446: Add new parameters to configure the
	inherance of files and for non-blocking sockets
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 4 Jul, 2013, at 13:19, Victor Stinner <victor.stinner at gmail.com> wrote:


I don't understand your reasoning, that is what has GNU libc to do with adding "e" mode to io.open?

BTW. I have no particular fondness for an "e" flag, adding a clo_exec flag would be fine and I'm just curious.

Ronald




----------------------------------------
Subject:
[Python-Dev] PEP 446: Add new parameters to configure the
	inherance of files and for non-blocking sockets
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 6 Jul, 2013, at 14:04, Victor Stinner <victor.stinner at gmail.com> wrote:


I guess he means that O_NONBLOCK doesn't actually do anything for regular
files, regular files are always ready for I/O as far as select et. al. are
concerned and I/O will block when data has to be read from disk (or in 
the case of a network filesystem, from another machine).

Ronald


