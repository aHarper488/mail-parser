
============================================================================
Subject: [Python-Dev] configuring the buildbot to skip some tests?
Post Count: 23
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
I've got parc-tiger-1 up and running again.  It's failing on test_tk,
which makes sense, because it's running as a background twisted process,
and thus can't access the window server.  I should configure that out.

I'm looking for documentation on how to configure the build slave so
that it skips this test.

Bill



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Bill Janssen wrote:

It doesn't really make sense. It should skip the test, instead of
failing it. I.e. aborting the Python process is definitely not a good
response.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Bill Janssen wrote:

It may be better to try to detect the "no window server" case and skip
it in the test itself rather than in the build slave configuration.

Cheers,
Nick.


-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: exarkun at twistedmatrix.co
Attributes: []Content: 
On 03:17 am, janssen at parc.com wrote:

You can run it in an xvfb.

Jean-Paul



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Even better would be if Python wouldn't crash when you try to run Tk
commands without a window server. Instead of aborting Python, that
should raise an exception (which can then be detected as a test skip).

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
exarkun at twistedmatrix.com wrote:

But that's beside the point! The slave configuration detected a bug in
Python, so rather than working around the bug by modifying the slave
configuration, the bug should get fixed.

Of course, the slave is then useless until somebody contributes such a fix.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 13 May 2010 15:43, "Martin v. L?wis" <martin at v.loewis.de> wrote:

That's the sad part. If there was a means of temporarily marking the
test on a particular slave as a known issue, it would avoid a single
bug rendering a buildslave useless...

(Having said that, a similar situation with my buildslave prompted me
to spend the time fixing the bug so I didn't have to keep restarting
the slave, so maybe it's a good thing after all :-))

Paul.



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Indeed. More generally, I'd question the point of automated testing if
people try to work around serious problems rather than fixing them. And
an interpreter crash in the test suite *is* a serious problem, IMO.

Of course, it may turn out to be an unfixable Tcl or Apple bug, in which
case working around would become more interesting.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
Martin v. L?wis <martin at v.loewis.de> wrote:


Yes, you're right.  It's a bug in the test.

Bill



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Glyph Lefkowit
Attributes: []Content: 

On May 13, 2010, at 9:41 AM, exarkun at twistedmatrix.com wrote:


See <http://www.python.org/dev/buildbot/builders/PPC%20Tiger%20trunk/builds/7/steps/test/logs/stdio>: this isn't an X server that he's talking about, it's "WindowServer", the OS X windowing system, so Xvfb won't help.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100513/70decf26/attachment.html>



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
exarkun at twistedmatrix.com wrote:


I don't think that would work -- it's a Mac.

Bill



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Bill Janssen wrote:

No, I'd say it's even deeper, in the Tcl integration.

There shouldn't be a way to cause an interpreter abort, except by
calling os.abort().

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 13 May, 2010, at 20:41, Martin v. L?wis wrote:


This is a bug in Tk: 

Thu May 13 20:45:13 Rivendell.local python[84887] <Error>: kCGErrorFailure: Set a breakpoint @ CGErrorBreakpoint() to catch errors as they are logged.
_RegisterApplication(), FAILED TO establish the default connection to the WindowServer, _CGSDefaultConnection() is NULL.
2010-05-13 20:45:16.762 Python[84887:d07] Error (1002) creating CGSWindow
2010-05-13 20:45:16.955 Python[84887:d07] *** Terminating app due to uncaught exception 'NSInternalInconsistencyException', reason: 'Error (1002) creating CGSWindow'
*** Call stack at first throw:
(
	0   CoreFoundation                      0x00007fff85e31d24 __exceptionPreprocess + 180
	1   libobjc.A.dylib                     0x00007fff860000f3 objc_exception_throw + 45
	2   CoreFoundation                      0x00007fff85e31b47 +[NSException raise:format:arguments:] + 103
	3   CoreFoundation                      0x00007fff85e31ad4 +[NSException raise:format:] + 148
	4   AppKit                              0x00007fff84614aba _NSCreateWindowWithOpaqueShape2 + 473
	5   AppKit                              0x00007fff845a9055 -[NSWindow _commonAwake] + 1214
	6   AppKit                              0x00007fff845c6d3d -[NSWindow _makeKeyRegardlessOfVisibility] + 96
	7   AppKit                              0x00007fff845c6cb2 -[NSWindow makeKeyAndOrderFront:] + 24
	8   Tk                                  0x000000010075b86c XMapWindow + 155
	9   Tk                                  0x00000001006ca6d0 Tk_MapWindow + 89
	10  Tk                                  0x00000001006d35e6 TkToplevelWindowForCommand + 2658
	11  Tcl                                 0x00000001006300d3 TclServiceIdle + 76
	12  Tcl                                 0x00000001006162ce Tcl_DoOneEvent + 329
	13  _tkinter.so                         0x0000000100595683 Tkapp_CallDeallocArgs + 277
	14  readline.so                         0x00000001001f1f9a initreadline + 1280
	15  Python                              0x00000001000076a1 PyOS_Readline + 239
	16  Python                              0x0000000100008a57 PyTokenizer_FromString + 1322
	17  Python                              0x00000001000090a0 PyTokenizer_Get + 154
	18  Python                              0x0000000100005698 PyParser_AddToken + 1018
	19  Python                              0x00000001000a2320 PyParser_ASTFromFile + 146
	20  Python                              0x00000001000a443f PyRun_InteractiveOneFlags + 345
	21  Python                              0x00000001000a4615 PyRun_InteractiveLoopFlags + 206
	22  Python                              0x00000001000a4685 PyRun_AnyFileExFlags + 76
	23  Python                              0x00000001000b0286 Py_Main + 2718
	24  Python                              0x0000000100000e6c start + 52
	25  ???                                 0x0000000000000001 0x0 + 1
)
terminate called after throwing an instance of 'NSException'
Abort trap


This is running /usr/bin/python in a session as a user that doesn't have access to the GUI.  The text above says that there is an uncaught ObjC exception, caused by the lack of a connection to the window server. Tk should have converted that to its own style of errors but didn't.

Bill: could you please file an issue for this in the python tracker, it should be possible to add a workaround for this to the Tkinter extension.

Ronald

-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 3567 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100513/a4698956/attachment-0001.bin>



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 


That makes sense to me; thanks for investigating it.

If we are kind, we could also file a Tk bug, then.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
Martin v. L?wis <martin at v.loewis.de> wrote:


There are some cases where the OS X security mechanism interprets an
invalid attempt to connect to the window server as a privilege
violation, and just terminates the process.  I've seen that before.

This is basically a resource issue in the testing framework, too, like
having network access.  The problem is, for lot of external libraries,
it's not clear just what resources they assume are available.  If we
believe that Tk doesn't intend to require access to the window server,
it's a Tk bug.  If we know that it does, it's a resource issue.

Bill



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
Martin v. L?wis <martin at v.loewis.de> wrote:


Yes, I agree.  It's an undesirable design bug in the Apple OS, IMO.
When some Apple libraries ask for some things that they're not allowed
to have, the library calls abort() instead of signalling an error.

Google for "FAILED TO GET ASN FROM CORESERVICES" sometime.

Bill



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Martin v. L?wis wrote:

Yes, when I commented I didn't realise that "failing" in this case
actually meant "crashing" :P

Regards,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Vincent Davi
Attributes: []Content: 
Not to interrupt you  you conversation but I am interested in setting
up a buildbot on one of my Macs. Is there any documentations or advise
that is different from that of a linux machine?  Any advise would be
appreciated.
Thanks
Vincent


On Thu, May 13, 2010 at 3:44 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Vincent Davis wrote:

This is a little bit out of context: what exactly do you want to set up?
A buildbot master, or a buildbot slave? For running what tests?

Buildbot is available from http://buildbot.net/trac

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
Vincent Davis <vincent at vincentdavis.net> wrote:


Assuming you mean "set up a build slave for Python testing"...

Here's what I did.  I was installing on a Tiger machine, which comes
with Python 2.3.5, so the first thing I did was to install Xcode 2.5,
the latest release for Tiger.  I also needed Subversion, so I downloaded
and installed that.  Later versions of OS X Xcode include Subversion, so
you won't need to do it separately if you're using those.

I then installed Python 2.6.5 from python.org, which winds up in
/usr/local/bin, and modified my PATH to put /usr/local/bin on it.  I
downloaded the source bundles for Twisted, zope.interface, and buildbot,
and installed them in that order -- "sudo python setup.py install" works
fine for each of them.

After that, I followed the instructions on the Wiki at
http://wiki.python.org/moin/BuildBot.  Create a buildbot account, make
sure /usr/local/bin is on the PATH of the buildbot account, and issue
the commands shown there.

Bill



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Vincent Davi
Attributes: []Content: 
On Fri, May 14, 2010 at 10:15 AM, Bill Janssen <janssen at parc.com> wrote:



Yes thanks this is what I was thinking "set up a build slave for Python
testing", No reason this would not work on a leopard 10.6 machine?

Thanks

*Vincent Davis
720-301-3003 *
vincent at vincentdavis.net
 my blog <http://vincentdavis.net> |
LinkedIn<http://www.linkedin.com/in/vincentdavis>
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100514/f501ef1d/attachment.html>



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

In my experience, it is mandatory that the slave admin has really good
understanding of Python, and of the operating system that the slave runs
on. Otherwise, the slave will be down most of the time, and just not
function correctly; it was then a waste of time to set it up in the
first place.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] configuring the buildbot to skip some tests?
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
Ronald Oussoren <ronaldoussoren at mac.com> wrote:


http://bugs.python.org/issue8716


That would be good.

Bill

