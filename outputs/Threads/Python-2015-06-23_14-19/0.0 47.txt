
============================================================================
Subject: [Python-Dev] [Python-checkins] r80025 - peps/trunk/pep-3147.txt
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r80025 - peps/trunk/pep-3147.txt
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
barry.warsaw wrote:

What (if anything) should we set __cached__ to in __main__?

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r80025 - peps/trunk/pep-3147.txt
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 13, 2010, at 11:13 PM, Nick Coghlan wrote:


Good catch.  Right now (in my current branch) it is undefined.  It should be
None.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100413/342c1439/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r80025 - peps/trunk/pep-3147.txt
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Barry Warsaw wrote:

Sounds reasonable. I ask because the various functions in runpy will
also need to cover setting that value properly.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r80025 - peps/trunk/pep-3147.txt
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 14, 2010, at 12:17 AM, Nick Coghlan wrote:


Right.  I'm looking at runpy now, but am less familiar with this code.  Right
now I have anything executed with -m as setting __cached__ to None, which
seems right in the simple case because the -m <module> doesn't appear to get
byte compiled.  Is that correct?

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100413/d21d308b/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r80025 - peps/trunk/pep-3147.txt
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Barry Warsaw wrote:

Yeah, the only time it uses byte-compiled files is if the original
source is missing. Setting __cached__ to None for that case as well
sounds like a reasonable starting point.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r80025 - peps/trunk/pep-3147.txt
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 14, 2010, at 07:04 AM, Nick Coghlan wrote:


Cool, thanks.
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100414/3514a106/attachment.pgp>

