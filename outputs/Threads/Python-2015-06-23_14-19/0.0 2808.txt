
============================================================================
Subject: [Python-Dev] Downloads page: Which version of Python should be
 listed first?
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Downloads page: Which version of Python should be
 listed first?
----------------------------------------
Author: Ross Lagerwal
Attributes: []Content: 
On Fri, Dec 14, 2012 at 07:57:52AM +1100, Chris Angelico wrote:

I would say listing 3.3 as the recommended version to use is a good
thing, especially as distros like Ubuntu and Fedora transition to Python
3. It also makes sense, given that the docs default to 3.3.

-- 
Ross Lagerwall



----------------------------------------
Subject:
[Python-Dev] Downloads page: Which version of Python should be
 listed first?
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Thu, Dec 13, 2012 at 1:57 PM, Chris Angelico <rosuav at gmail.com> wrote:

Nope:

http://py3ksupport.appspot.com/
http://python3wos.appspot.com/ (plone and zope skew the results)

-eric



----------------------------------------
Subject:
[Python-Dev] Downloads page: Which version of Python should be
 listed first?
----------------------------------------
Author: Devin Jeanpierr
Attributes: []Content: 
On Thu, Dec 13, 2012 at 9:38 PM, Eric Snow <ericsnowcurrently at gmail.com> wrote:

Until those numbers hit 100%, or until projects start dropping support
for Python 2.x, the statement would still be true.

-- Devin



----------------------------------------
Subject:
[Python-Dev] Downloads page: Which version of Python should be
 listed first?
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Sat, Dec 15, 2012 at 12:31 PM, Devin Jeanpierre
<jeanpierreda at gmail.com> wrote:

Not necessarily dropping; all you need is for new projects to not
bother supporting 2.x and the statement can become false.

ChrisA



----------------------------------------
Subject:
[Python-Dev] Downloads page: Which version of Python should be
 listed first?
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Devin Jeanpierre writes:

 > Until those numbers hit 100%, or until projects start dropping support
 > for Python 2.x, the statement would still be true.

This is simply not true.  Once the numbers hit somewhere in the
neighborhood of 50%, the network effects (the need to "connect" to the
more progressive projects) are going to rule, and "most projects" will
face strong pressure to adapt (which means providing Python 3 support,
*not* "dropping Python 2").  On the other hand, some projects will
never bother because they're completely standalone: you won't ever
reach 100%, and even 90% may not be necessary for the ecology to be
considered "fully adapted to Python 3".


