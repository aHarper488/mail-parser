
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: test.support: add
 requires_mac_ver() function
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: test.support: add
 requires_mac_ver() function
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
Hi,

On 01/06/2011 13.28, victor.stinner wrote:

I would expect this to be a decorator, similar to requires_IEEE_754 and 
requires_zlib.


For consistency, this should be a decorator too, possibly named 
requires_linux_ver().
A requires_windows(_ver) sounds like a useful addition too, given the 
number of tests that are specific to Windows.


This should be requires_mac_ver


Best Regards,
Ezio Melotti

