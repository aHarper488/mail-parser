
============================================================================
Subject: [Python-Dev] possible bug in distutils (Mingw32CCompiler)?
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] possible bug in distutils (Mingw32CCompiler)?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 

Mingw32CCompiler in cygwincompiler.py emits the symbol -mno-cygwin.

This is used to make Cygwin's gcc behave as mingw. As of gcc 4.6 it is 
not recognized by the mingw gcc compiler itself, and causes as crash. It 
should be removed because it is never needed for mingw (in any version), 
only for cross-compilation to mingw from other gcc versions.

Instead, those who use CygwinCCompiler or Linux GCC to "cross-compile" 
to plain Win32 can set -mno-cygwin manually. It also means -mcygwin 
should be removed from the output of CygwinCCompiler.

I think...


Sturla






----------------------------------------
Subject:
[Python-Dev] possible bug in distutils (Mingw32CCompiler)?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Thu, May 24, 2012 at 7:03 AM, Sturla Molden <sturla at molden.no> wrote:

Please report bugs to http://bugs.python.org so they don't get lost in
email. The relevant people will be notified or assigned if a bug is
entered.



----------------------------------------
Subject:
[Python-Dev] possible bug in distutils (Mingw32CCompiler)?
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 24 May 2012 08:45:30 -0500, Brian Curtin <brian at python.org> wrote:

It was already reported by someone else:

http://bugs.python.org/issue12641

--David

