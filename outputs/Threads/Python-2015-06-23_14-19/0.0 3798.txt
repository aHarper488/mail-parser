
============================================================================
Subject: [Python-Dev] =?utf-8?b?IkRlY2ltYWwoMikgIT0gZmxvYXQoMikiPz8/?=
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] =?utf-8?b?IkRlY2ltYWwoMikgIT0gZmxvYXQoMikiPz8/?=
----------------------------------------
Author: Jan Kaliszewsk
Attributes: []Content: 
Hello,

In http://docs.python.org/release/3.2.3/reference/expressions.html#in 
we read: "[...] This can create the illusion of non-transitivity between 
supported cross-type comparisons and unsupported comparisons. For 
example, Decimal(2) == 2 and 2 == float(2) but Decimal(2) != float(2)."

(The same is in the 3.3 docs).

But:

     Python 3.2.3 (default, Sep 10 2012, 18:14:40)
     [GCC 4.6.3] on linux2
     Type "help", "copyright", "credits" or "license" for more 
information.
     >>> import decimal
     >>> decimal.Decimal(2) == float(2)
     True

Is it a bug in the docs or in Python itself? (I checked that in 3.2, 
but it may be true for 3.3 as well)

Regards.
*j


