
============================================================================
Subject: [Python-Dev] [Python-checkins] r79397 -
 in	python/trunk:	Doc/c-api/capsule.rst
 Doc/c-api/cobject.rst	Doc/c-api/concrete.rst	Doc/data/refcounts.dat	Doc/extending/extending.rst
 Include/Python.h	Include/cStringIO.h	Include/cobject.h	Include/datetime.h
 Include/py_curses.h	Include/pycapsule.h	Include/pyexpat.h	Include/ucnhash.h
 Lib/test/test_sys.py	Makefile.pre.in	Misc/NEWS	Modules/_ctypes/callproc.c
 Modules/_ctypes/cfield.c	Modules/_ctypes/ctypes.h	Modules/_cursesmodule.c
 Modules/_elementtree.c	Modules/_testcapimodule.c	Modules/cStringIO.c	Modules/cjkcodecs/cjkcodecs.h
 Modules/cjkcodecs/multibytecodec.c	Modules/cjkcodecs/multibytecodec.h	Modules/datetimemodule.c
 Modules/pyexpat.c	Modules/socketmodule.c	Modules/socketmodule.h	Modules/unicodedata.c
 Objects/capsule.c	Objects/object.c	Objects/unicodeobject.c	PC/VS7.1/pythoncore.vcproj
 PC/VS8.0/pythoncore.vcproj	PC/os2emx/python27.def	PC/os2vacpp/python.def
 Python/compile.c Python/getargs.c
In-Reply-To: <4BAC7C48.3080600@egenix.com>
References: <20100325005454.B1AC5FCCD@mail.python.org>	<4BAB2882.6070508@egenix.com>
	<4BAB9FAE.1070206@hastings.org> <4BAC7C48.3080600@egenix.com>
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r79397 -
 in	python/trunk:	Doc/c-api/capsule.rst
 Doc/c-api/cobject.rst	Doc/c-api/concrete.rst	Doc/data/refcounts.dat	Doc/extending/extending.rst
 Include/Python.h	Include/cStringIO.h	Include/cobject.h	Include/datetime.h
 Include/py_curses.h	Include/pycapsule.h	Include/pyexpat.h	Include/ucnhash.h
 Lib/test/test_sys.py	Makefile.pre.in	Misc/NEWS	Modules/_ctypes/callproc.c
 Modules/_ctypes/cfield.c	Modules/_ctypes/ctypes.h	Modules/_cursesmodule.c
 Modules/_elementtree.c	Modules/_testcapimodule.c	Modules/cStringIO.c	Modules/cjkcodecs/cjkcodecs.h
 Modules/cjkcodecs/multibytecodec.c	Modules/cjkcodecs/multibytecodec.h	Modules/datetimemodule.c
 Modules/pyexpat.c	Modules/socketmodule.c	Modules/socketmodule.h	Modules/unicodedata.c
 Objects/capsule.c	Objects/object.c	Objects/unicodeobject.c	PC/VS7.1/pythoncore.vcproj
 PC/VS8.0/pythoncore.vcproj	PC/os2emx/python27.def	PC/os2vacpp/python.def
 Python/compile.c Python/getargs.c
In-Reply-To: <4BAC7C48.3080600@egenix.com>
References: <20100325005454.B1AC5FCCD@mail.python.org>	<4BAB2882.6070508@egenix.com>
	<4BAB9FAE.1070206@hastings.org> <4BAC7C48.3080600@egenix.com>
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 


Well, it's been a couple of days, and nobody has stepped forward to 
speak in favor of my aggressive PyCapsule schedule.  So I will bow to 
the wishes of those who have spoken up, and scale it back.  
Specifically, I propose writing a patch that will:

    * allow the CObject API to (unsafely) dereference a capsule
    * add support for the new files added by capsule to the Visual
      Studio builds (fixes an unrelated second complaint about my checkin)
    * add a -3 warning for calls to CObject
    * add a PendingDeprecation warning to CObject


I've gotten in the habit of doing code reviews at work, and I now 
greatly prefer working that way.  Can I get a volunteer to review the 
patch when it's ready?   Please email me directly.  Thanks!


M.-A. Lemburg wrote:

I'm aware this is a good idea.  I simply didn't consider this a major 
breakage.  Recompiling against the 2.7 header files fixes it for 
everybody.  (Except external users of pyexpat, if any exist.  Google 
doesn't show any, though this is not proof that they don't exist.)

If you suggest that any breakage with previous versions is worth 
mentioning, no matter how small, then I'll remember that in the future.  
Certainly the Python community has had many thrilling and dynamic 
conversations over minutae, so I guess it wouldn't be that surprising if 
this were true.



I'm surprised you find that acceptable.  I thought the community was in 
agreement: our goal is to make the interpreter uncrashable from pure Python.

I would describe ctypes as "impure" Python.  If you wanted to allow 
untrusted developers to run code through a Python interpreter, you'd 
harden it: take away ctypes, and the ability to feed in arbitrary 
bytecode, and reduce the stack limit.  You probably wouldn't take away 
"datetime", "cStringIO", and "cPickle"--yet I can crash 2.6 using those.



The point of these macros would be to support older versions of Python.  
The macros would boil down to either PyCapsule or CObject calls, 
depending on what version of Python you were compiling against.  For 
Python 2.6 and before it would alway use CObjects.  One set of macros 
would switch to PyCapsule when building against 2.7; another would use 
CObject permanently in 2.x.  (Or maybe it should switch in 2.8?  2.9?  
But now we're in the realm of speculative fiction.)

I'd be happy to write these if there was demand, but my sneaking 
suspicion is that nobody would use them.  If you're genuinely interested 
in these macros please email me privately.


/larry/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100328/d888c7a6/attachment-0001.html>

