
============================================================================
Subject: [Python-Dev] Style guide for FAQs?
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Style guide for FAQs?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

I notice that some FAQs are not only outdated but seem to favour a
writing style that's quite lengthy and full of anecdotal details.
It seems to me that there is value in giving terse answers in FAQs (we
have - or should have - reference documentation where things are
explained in more detail).

One primary example is the performance question:
file:///home/antoine/cpython/32/Doc/build/html/faq/programming.html#my-program-is-too-slow-how-do-i-speed-it-up

It mixes a couple of generalities with incredibly specific suggestions
such as early binding of methods or use of default argument values to
fold constants. I think a beginner reading this entry won't get any
meaningful information out of it.

Any advice on whether it's ok to hack and slash into the fat? :)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Style guide for FAQs?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 3 Dec 2011 21:39:03 +0100
Antoine Pitrou <solipsis at pitrou.net> wrote:

Woohoo. This should of course be:
http://docs.python.org/dev/faq/programming.html#my-program-is-too-slow-how-do-i-speed-it-up

cheers

Antoine.





----------------------------------------
Subject:
[Python-Dev] Style guide for FAQs?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/3/2011 3:58 PM, Antoine Pitrou wrote:

That looks like a mini-howto ;-),
rather than a FAQ entry.

The changes you have made so far have looked good to me.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Style guide for FAQs?
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 04.12.2011 03:55, schrieb Terry Reedy:

Definitely.

Georg


