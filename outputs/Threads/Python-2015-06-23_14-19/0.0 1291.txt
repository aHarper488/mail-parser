
============================================================================
Subject: [Python-Dev] faulthandler is now part of Python 3.3
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] faulthandler is now part of Python 3.3
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Victor Stinner wrote:


I apologize -- I'm not going to have time to test this myself, and I'm 
really curious to know if it works:

Issue11603 describes a problem where Python either hangs or crashes 
depending on Python version/OS version... does the faulthandler work for 
this problem?

Remodeling-at-home-and-swamped-at-work-ly yours,
~Ethan~



----------------------------------------
Subject:
[Python-Dev] faulthandler is now part of Python 3.3
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

I pushed my faulthandler module into the default branch (Python 3.3).
Since one week, I fixed a lot of bugs (platform issues), improved the
tests and Antoine wrote a new implementation of dump_backtraces_later()
using a thread (instead of SIGALRM+alarm()). It should now work on all
platforms (but register() is not available on Windows).

Use "python -X faulthandler" or "PYTHONFAULTHANDLER=1 python" to install
the fault handler at startup (catch segfaults and other fatal errors).

You can also register a signal (e.g. SIGUSR1) to dump the traceback on
this signal.

The latest added feature is to be able to the dump the traceback after a
timeout and exit the process: we may use it on regrtest.py to learn more
about test_multiprocess and test_threadsignals hangs. Issue #11393 has a
patch implementing this issue: add --timeout option to regrtest.py. You
can also just dump the traceback after the timeout without exiting.

Py_FatalError() always print the Python traceback (except if an
exception was raised: print the exception with its traceback).

For more information, read the doc:
http://docs.python.org/dev/library/faulthandler.html

Please tell me if you have any issue related to faulthandler.

--

If you get "undefined reference to `_PyFaulthandler_Init'" compiler
error, copy Modules/Setup.dist to Modules/Setup (cp Modules/Setup.dist
Modules/Setup).

test_faulthandler hangs on AMD64 Gentoo Wide 3.x and AMD64 OpenIndiana
3.x. It looks to be related to the stack overflow test (the stack is
maybe not limited on these buildbots?). I have a patch, but I cannot
test it because these buildbots are dead (oops, sorry!).

Most buildbots are red because a regression in test_logging (since 2
days): I disabled temporary the test (issue #11557), I hope that the
situation will be better in a few hours.

Thank you Antoine for your reviews!

Victor




----------------------------------------
Subject:
[Python-Dev] faulthandler is now part of Python 3.3
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 03/30/2011 09:54 PM, Victor Stinner wrote:

Thanks very much for you hard work on this cool new feature.


Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk2Ud/gACgkQ+gerLs4ltQ6BVACgo822OajfnxbVQInroX8q5L7B
wX0AoMpEJLNk0ffEBJs+C2CDXiaIz+yf
=rf2C
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] faulthandler is now part of Python 3.3
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/3/31 Tres Seaver <tseaver at palladion.com>:

Furthermore, let's hope we don't have to use it much! :)



-- 
Regards,
Benjamin

