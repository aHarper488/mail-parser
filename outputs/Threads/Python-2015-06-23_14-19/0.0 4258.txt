
============================================================================
Subject: [Python-Dev] Disabling string interning for null and single-char
	causes segfaults
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Disabling string interning for null and single-char
	causes segfaults
----------------------------------------
Author: Stefan Bucu
Attributes: []Content: 
Hi,

I'm working on an automated bug finding tool that I'm trying to apply on
the Python interpreter code (version 2.7.3). Because of early prototype
limitations, I needed to disable string interning in stringobject.c. More
precisely, I modified the PyString_FromStringAndSize and
PyString_FromString to no longer check for the null and single-char cases,
and create instead a new string every time (I can send the patch if needed).

However, after applying this modification, when running "make test" I get a
segfault in the test___all__ test case.

Before digging deeper into the issue, I wanted to ask here if there are any
implicit assumptions about string identity and interning throughout the
interpreter implementation. For instance, are two single-char strings
having the same content supposed to be identical objects?

I'm assuming that it's either this, or some refcount bug in the interpreter
that manifests only when certain strings are no longer interned and thus
have a higher chance to get low refcount values.

Thanks a lot,
Stefan Bucur
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130301/c86e3b52/attachment.html>

