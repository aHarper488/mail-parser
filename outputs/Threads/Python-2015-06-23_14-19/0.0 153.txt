
============================================================================
Subject: [Python-Dev] Postponing 2.6.6rc1
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Postponing 2.6.6rc1
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
For several reasons, I'm postponing 2.6.6rc1 for one day.  Ezio has been doing
a lot of great work on the test suite, but there are still a few things to
fix.  On top of that, bugs.python.org crashed and we're waiting for our
hosting company to wake up and reboot it.

We'll try again, same time tomorrow: 2200 UTC.  Come join us on #python-dev if
you want to watch the circus. :)

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100802/d19e1c1e/attachment.pgp>

