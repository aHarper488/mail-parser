
============================================================================
Subject: [Python-Dev] Python 3.3 cannot import BeautifulSoup but Python
 3.2 can
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 3.3 cannot import BeautifulSoup but Python
 3.2 can
----------------------------------------
Author: Oleg Broytma
Attributes: []Content: 
On Mon, May 07, 2012 at 04:42:50PM -0400, "Edward C. Jones" <edcjones at comcast.net> wrote:

   Could it be bs4 is installed in python3.2-specific path and hence it's
not in python3.3 sys.path?

Oleg.
-- 
     Oleg Broytman            http://phdru.name/            phd at phdru.name
           Programmers don't die, they just GOSUB without RETURN.



----------------------------------------
Subject:
[Python-Dev] Python 3.3 cannot import BeautifulSoup but Python
 3.2 can
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 07, 2012, at 04:42 PM, Edward C. Jones wrote:


Remember that Debian installs its system packages into dist-packages not
site-packages.  This is a Debian delta from upstream.

http://wiki.debian.org/Python

Cheers,
-Barry


