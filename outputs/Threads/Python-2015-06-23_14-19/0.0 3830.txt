
============================================================================
Subject: [Python-Dev] cpython (3.3): don't run frame if it has no stack
 (closes #17669)
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.3): don't run frame if it has no stack
 (closes #17669)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 10 Apr 2013 23:01:46 +0200 (CEST)
benjamin.peterson <python-checkins at python.org> wrote:

Wouldn't it be better with a test?

Regards

Antoine.









----------------------------------------
Subject:
[Python-Dev] cpython (3.3): don't run frame if it has no stack
 (closes #17669)
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/4/10 Antoine Pitrou <solipsis at pitrou.net>:

Yes, if I could come up with a non-fragile one.

--
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] cpython (3.3): don't run frame if it has no stack
 (closes #17669)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 11 Apr 2013 07:49, "Antoine Pitrou" <solipsis at pitrou.net> wrote:

Benjamin said much the same thing on the issue, but persuading the
interpreter to create a frame without a stack that then gets exposed to
this code path isn't straightforward :P

Cheers,
Nick.

yield from.
function.
*)PyBytes_AS_STRING(bytecode);
http://mail.python.org/mailman/options/python-dev/ncoghlan%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130411/88027645/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython (3.3): don't run frame if it has no stack
 (closes #17669)
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Thu, Apr 11, 2013 at 12:28 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Maybe it's worth understanding in which circumstances this holds?
Please write a test, we'll run into similar issue at some point Im'm
sure.

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] cpython (3.3): don't run frame if it has no stack
 (closes #17669)
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/4/11 Maciej Fijalkowski <fijall at gmail.com>:

Probably not. It's related to cyclic GC.



--
Regards,
Benjamin

