
============================================================================
Subject: [Python-Dev] test_hashlib
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] test_hashlib
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
I was hit by this today.

in test_hashlib.py there is this:



def test_unknown_hash(self):

    self.assertRaises(ValueError, hashlib.new, 'spam spam spam spam spam')

    self.assertRaises(TypeError, hashlib.new, 1)



but in hashlib.py, there is this code:



except ImportError:

    pass # no extension module, this hash is unsupported.

raise ValueError('unsupported hash type %s' % name)





The code will raise ValueError when int(1) is passed in, but the unittests expect a TypeError.

So, which is correct?



K
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120721/62747a7b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] test_hashlib
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 21 Jul 2012 21:29:50 +0000
Kristj?n Valur J?nsson <kristjan at ccpgames.com> wrote:

Well, if test_hashlib passes, surely your analysis is wrong, no?


-- 
Software development and contracting: http://pro.pitrou.net





----------------------------------------
Subject:
[Python-Dev] test_hashlib
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
2012/7/21 Antoine Pitrou <solipsis at pitrou.net>:

In the normal case, yes:

TypeError: name must be a string

But if the _hashlib extension module is not available, the python
version is used and ValueError is raised:

ValueError: unsupported hash type 1

-- 
Amaury Forgeot d'Arc



----------------------------------------
Subject:
[Python-Dev] test_hashlib
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
I think I see Kristj?n's point: the pure Python implementation handles
errors differently than the C implementation, so the unittest fails if
the pure Python version is enabled. I imagine this is a general
problem that often occurs when a pure Python version is normally
shadowed by a C extension, unless the unittest is rigged so that it
tests the pure Python version as well as the C version. But it still
remains a matter of judgment whether in a particular case the unittest
is overspecified or the Python version is incorrect. I think that in
this case Kristj?n's hunch is correct, and the pure Python version
needs to be adjusted to pass the test. I also think this is a low
priority issue since it only affects behavior of error cases.

--Guido

On Sat, Jul 21, 2012 at 3:56 PM, Amaury Forgeot d'Arc
<amauryfa at gmail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] test_hashlib
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
Indeed, shame on me for not mentioning this.
I rarely have the full complement of externals available when I'm doing python work, and it struck me that this unitest was failing.
I suppose it should be possible to write unittests that test more than one particular implementation.

K

________________________________________
Fr?: python-dev-bounces+kristjan=ccpgames.com at python.org [python-dev-bounces+kristjan=ccpgames.com at python.org] fyrir h&#246;nd Amaury Forgeot d'Arc [amauryfa at gmail.com]
Sent: 21. j?l? 2012 22:56
To: Antoine Pitrou
Cc: python-dev at python.org
Efni: Re: [Python-Dev] test_hashlib

2012/7/21 Antoine Pitrou <solipsis at pitrou.net>:

In the normal case, yes:

TypeError: name must be a string

But if the _hashlib extension module is not available, the python
version is used and ValueError is raised:

ValueError: unsupported hash type 1

--
Amaury Forgeot d'Arc
_______________________________________________
Python-Dev mailing list
Python-Dev at python.org
http://mail.python.org/mailman/listinfo/python-dev
Unsubscribe: http://mail.python.org/mailman/options/python-dev/kristjan%40ccpgames.com



----------------------------------------
Subject:
[Python-Dev] test_hashlib
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 22 Jul 2012 01:05:35 -0000, =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?= <kristjan at ccpgames.com> wrote:

It is indeed, and we have been moving the unit tests to do exactly
that for the cases where there is a Python implementation as well as
a C implementation.  It seems like it would be appropriate to open an
issue for doing that for hashlib, in addition to one for fixing this
particular issue with the Python version.

--David



----------------------------------------
Subject:
[Python-Dev] test_hashlib
----------------------------------------
Author: Gregory P. Smit
Attributes: []Content: 
Fixed. The TypeError in this nonsense never gonna work use case is now
consistent in 2.7, 3.2 and 3.3.

On Sat, Jul 21, 2012 at 7:10 PM, R. David Murray <rdmurray at bitdance.com>wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120721/1acfb23b/attachment.html>

