
============================================================================
Subject: [Python-Dev]
 =?windows-1252?q?About_resolution_=93accepted=94_on_?=
 =?windows-1252?q?the_tracker?=
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]
 =?windows-1252?q?About_resolution_=93accepted=94_on_?=
 =?windows-1252?q?the_tracker?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 18 Oct 2010 21:31:24 +0200
Georg Brandl <g.brandl at gmx.net> wrote:

Not only, but it generally gets closed too.


I don't like this first stage, it makes it look like we mandate a
proper unit test to proceed with actually writing patches, which is
really not true.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev]
 =?windows-1252?q?About_resolution_=93accepted=94_on_?=
 =?windows-1252?q?the_tracker?=
----------------------------------------
Author: Ron Ada
Attributes: []Content: 


On 10/18/2010 07:07 PM, R. David Murray wrote:


This is about communicating both content and quality, with several
decisions at certain points.  It's not a straight line step by step 
process, which is why it gets confusing if you try to treat it as such. 
Check boxes work well for things like this.

Could something like (or parts of) the following work?  It would have 
assignment and module keywords items as well.


[] boxes can be set or unset by both the bug/feature author and those with
tracker/commit privileges.

{} boxes settable only by those with tracker and commit privileges?


     Title [________________________________]

     Description [_______________________...]


     TYPE:
	[] Bug
             Versions:
                [] 2.5     [] 2.6    [] 2.7
                [] 3.0     [] 3.1    [] 3.2
                     ...
                {} Verified

         [] Feature Request
             For Version [_____]    *Drop down list.

             {} Initial approval    *May be rejected later.
             Requires PEP:
                 {} yes       PEP Number [______]
                 {} no

     STATUS:
	Components:
             [] Code Patch             {} approved
             [] Test patch             {} approved
             [] Document Patch         {} approved
             [] News Entry Patch       {} approved
             [] Final Commit Approval  {} approved

	{} Committed
             Revision #(s) {____________________}
             Notes:{____________________________}

         {} Rejected
             Reason:{___________}  *Drop down list here.

         {} Closed


     ATTENTION REQUESTS:
        [] Request bug verification          *Auto set when new bug.
        [] Request feature initial approval  *Auto set when new.

        [] Request author reassignment
           *Current author unable to work on this.

        Request test patch      [] review  [] approval
        Request code patch      [] review  [] approval
        Request document patch  [] review  [] approval
        Request news patch      [] review  [] approval
        Request final commit    [] review  [] approval

        *More than one attention request can be set at a time.
        *Reviewer clears request if revision is needed or item is ok.

Most changes to any of these should also be documented in the discussion 
area as well.

Ron



----------------------------------------
Subject:
[Python-Dev]
 =?windows-1252?q?About_resolution_=93accepted=94_on_?=
 =?windows-1252?q?the_tracker?=
----------------------------------------
Author: Ron Ada
Attributes: []Content: 


On 10/18/2010 07:07 PM, R. David Murray wrote:


This is about communicating both content and quality, with several
decisions at certain points.  It's not a straight line step by step 
process, which is why it gets confusing if you try to treat it as such. 
Check boxes work well for things like this.

Could something like (or parts of) the following work?  It would have 
assignment and module keywords items as well.


[] boxes can be set or unset by both the bug/feature author and those with
tracker/commit privileges.

{} boxes settable only by those with tracker and commit privileges?


     Title [________________________________]

     Description [_______________________...]


     TYPE:
	[] Bug
             Versions:
                [] 2.5     [] 2.6    [] 2.7
                [] 3.0     [] 3.1    [] 3.2
                     ...
                {} Verified

         [] Feature Request
             For Version [_____]    *Drop down list.

             {} Initial approval    *May be rejected later.
             Requires PEP:
                 {} yes       PEP Number [______]
                 {} no

     STATUS:
	Components:
             [] Code Patch             {} approved
             [] Test patch             {} approved
             [] Document Patch         {} approved
             [] News Entry Patch       {} approved
             [] Final Commit Approval  {} approved

	{} Committed
             Revision #(s) {____________________}
             Notes:{____________________________}

         {} Rejected
             Reason:{___________}  *Drop down list here.

         {} Closed


     ATTENTION REQUESTS:
        [] Request bug verification          *Auto set when new bug.
        [] Request feature initial approval  *Auto set when new.

        [] Request author reassignment
           *Current author unable to work on this.

        Request test patch      [] review  [] approval
        Request code patch      [] review  [] approval
        Request document patch  [] review  [] approval
        Request news patch      [] review  [] approval
        Request final commit    [] review  [] approval

        *More than one attention request can be set at a time.
        *Reviewer clears request if revision is needed or item is ok.

Most changes to any of these should also be documented in the discussion 
area as well.

Ron


