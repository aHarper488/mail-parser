
============================================================================
Subject: [Python-Dev] [Python-checkins] r85934 - in
 python/branches/py3k: Misc/NEWS Modules/socketmodule.c
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r85934 - in
 python/branches/py3k: Misc/NEWS Modules/socketmodule.c
----------------------------------------
Author: Scott Dia
Attributes: []Content: 
On 10/30/2010 4:08 PM, Martin v. L?wis wrote:

Still not quite right. The call to GetComputerNameExW after
ERROR_MORE_DATA (which gives the number of *bytes* needed) still needs
to pass "size/sizeof(wchar_t)" back into GetComputerNameExW since it
wants the number TCHARs. I don't think the +1 is needed either (MSDN
says it already included the null-terminator in the byte count.

-- 
Scott Dial
scott at scottdial.com
scodial at cs.indiana.edu



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r85934 - in
 python/branches/py3k: Misc/NEWS Modules/socketmodule.c
----------------------------------------
Author: Hirokazu Yamamot
Attributes: []Content: 
On 2010/10/30 3:20, martin.v.loewis wrote:

I think size should be in TCHARs, not in bytes. (MSDN says so)
And GetComputerName's signature differs from MSDN. (Maybe should
use GetComputerNameExW again?)




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r85934 - in
 python/branches/py3k: Misc/NEWS Modules/socketmodule.c
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

You are right. So how about this patch?

Index: Modules/socketmodule.c
===================================================================
--- Modules/socketmodule.c      (Revision 85983)
+++ Modules/socketmodule.c      (Arbeitskopie)
@@ -3098,16 +3098,16 @@
        version of the hostname, whereas we need a Unicode string.
        Otherwise, gethostname apparently also returns the DNS name. */
     wchar_t buf[MAX_COMPUTERNAME_LENGTH];
-    DWORD size = sizeof(buf);
+    DWORD size = sizeof(buf)/sizeof(wchar_t);
     if (!GetComputerNameExW(ComputerNamePhysicalDnsHostname, buf, &size)) {
         if (GetLastError() == ERROR_MORE_DATA) {
             /* MSDN says this may occur "because DNS allows longer names */
             PyObject *result = PyUnicode_FromUnicode(NULL, size);
             if (!result)
                 return NULL;
-            if (GetComputerName(ComputerNamePhysicalDnsHostname,
-                                PyUnicode_AS_UNICODE(result),
-                                size+1))
+            if (GetComputerNameExW(ComputerNamePhysicalDnsHostname,
+                                   PyUnicode_AS_UNICODE(result),
+                                   size+1))
                 return result;
             Py_DECREF(result);
         }

Regards,
Martin

