
============================================================================
Subject: [Python-Dev] Changes in html.parser may cause breakage in
 client code
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Changes in html.parser may cause breakage in
 client code
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
Hi,

On 26/04/2012 22.10, Vinay Sajip wrote:

html.parser doesn't use any private _name, so I was considering part of 
the public API only the documented names.  Several methods are marked 
with an "# internal" comment, but that's not visible unless you go read 
the source code.


Django shouldn't override parse_starttag (internal and undocumented), 
but just use handle_starttag (public and documented).
I see two possible reasons why it's overriding parse_starttag:
  1) Django is working around an HTMLParser bug.  In this case the bug 
could have been fixed (leading to the breakage of the now-useless 
workaround), and now you could be able to use the original 
parse_starttag and have the correct result.  If it is indeed working 
around a bug and the bug is still present, you should report it upstream.
  2) Django is implementing an additional feature.  Depending on what 
exactly the code is doing you might want to open a new feature request 
on the bug tracker. For example the original parse_starttag sets a 
self.lasttag attribute with the correct name of the last tag parsed.  
Note however that both parse_starttag and self.lasttag are internal and 
shouldn't be used directly (but lasttag could be exposed and documented 
if people really think that it's useful).


I'm not sure that reverting the regex, deprecate all the exposed 
internal names, and add/use internal _names instead is a good idea at 
this point.  This will cause more breakage, and it would require an 
extensive renaming.  I can add notes to the documentation/docstrings and 
specify what's private and what's not though.
OTOH, if this specific fix is not released yet I can still do something 
to limit/avoid the breakage.

Best Regards,
Ezio Melotti





----------------------------------------
Subject:
[Python-Dev] Changes in html.parser may cause breakage in
 client code
----------------------------------------
Author: Carl Meye
Attributes: []Content: 
On 04/27/2012 08:36 AM, Guido van Rossum wrote:

I committed the relevant code to Django (though I didn't write the 
patch), and I've been following this thread. I have it on my todo list 
to review this code again with Ezio's suggestions in mind. So you can 
consider "the Django folks" contacted.

Carl

