
============================================================================
Subject: [Python-Dev] PEP 399: Pure Python/C Accelerator
 Module	Compatibiilty Requirements
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 399: Pure Python/C Accelerator
 Module	Compatibiilty Requirements
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 05/04/2011 20:57, Raymond Hettinger wrote:

The major problem that pypy had with heapq, aside from semantic 
differences, was (is?) that if you run the tests against the pure-Python 
version (without the C accelerator) then tests *fail*. This means they 
have to patch the CPython tests in order to be able to use the pure 
Python version.

Ensuring that tests run against both (even if there are some unavoidable 
differences like exception types with the tests allowing for both or 
skipping some tests) would at least prevent this happening.

All the best,

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] PEP 399: Pure Python/C Accelerator
 Module	Compatibiilty Requirements
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 06 Apr 2011 15:17:05 +0100
Michael Foord <fuzzyman at voidspace.org.uk> wrote:

Was the tests patch contributed back?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 399: Pure Python/C Accelerator
 Module	Compatibiilty Requirements
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Brett Cannon wrote:

How long does that silence have to last?

I didn't notice a definition of what counts as "100% branch coverage". 
Apologies if I merely failed to notice it, but I think it should be 
explicitly defined.

Presumably it means that any time you have an explicit branch 
(if...elif...else, try...except...else, for...else, etc.) you need a 
test that goes down each branch. But it isn't clear to me whether it's 
sufficient to test each branch in isolation, or whether you need to test 
all combinations.

That is, if you have five branches, A or B, C or D, E or F, G or H, I or 
J, within a single code unit (function? something else?), is it 
sufficient to have at least one test that goes down each of A...J, or do 
you need to explicitly test each of:

A-C-E-G-I
A-C-E-G-J
A-C-E-H-I
A-C-E-H-J
A-C-F-G-I
...
B-D-F-H-J

(10 tests versus 32 tests).

If the latter, this could become impractical *very* fast. But if not, I 
don't see how we can claim 100% coverage when there are code paths which 
are never tested.

At the very least, I think you need to explicitly define what you mean 
by "100% branch coverage". Possibly this will assist in the disagreement 
between you and Antoine re "100% versus "comprehensive" coverage.



-- 
Steven

