
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Closes issue 17947.
 Adds PEP-0435 (Enum, IntEnum) to the stdlib.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Closes issue 17947.
 Adds PEP-0435 (Enum, IntEnum) to the stdlib.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
Ethan, did you forget to run ``hg add`` before committing? If not then why
the heck did we argue over enums for so long if this was all it took to
make everyone happy? =)


On Fri, Jun 14, 2013 at 3:31 AM, ethan.furman <python-checkins at python.org>wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130614/f5ad9abd/attachment.html>

