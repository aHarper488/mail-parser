
============================================================================
Subject: [Python-Dev] [Python-checkins] r83893
	-	python/branches/release27-maint/Misc/ACKS
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r83893
	-	python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 10.08.2010 04:47, schrieb Nick Coghlan:

That's why it says "rough" alphabetical order. Putting it into either
place sounds reasonable - we can just expect people to change it forth
and back.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r83893
	-	python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 10.08.2010 05:49, schrieb Alexander Belopolsky:

People need to recognize that any kind of reference is really irrelevant
here. There is no "right" order that is better than any other "right"
order. I'd personally object to any English language dictionary telling
me how my name sorts in the alphabet.

(and yes, I do think it's "wrong" that it got sorted after Lyngvig -
in Germany, we put the ? as if it was "oe" - unlike the Swedes, which
put the very same letter after the rest of the alphabet. So the
? in Chrigstr?m sorts in a different way than the ? in L?wis. If I move
to Sweden, the file would have to change :-)

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r83893
	-	python/branches/release27-maint/Misc/ACKS
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Benjamin Peterson writes:
 > 2010/8/9 Nick Coghlan <ncoghlan at gmail.com>:
 > > On Tue, Aug 10, 2010 at 2:10 AM, alexander.belopolsky
 > > <python-checkins at python.org> wrote:
 > >> +PS: In the standard Python distribution, this file is encoded
 > >> in UTF-8 +and the list is in rough alphabetical order by last
 > >> names.
 > >>
 > >> ?David Abrahams
 > >> ?Jim Ahlstrom
 > >> @@ -28,6 +29,7 @@
 > >> ??ric Araujo
 > >> ?Jason Asbahr
 > >> ?David Ascher
 > >> +Peter ?strand
 > > From my recollection of the discussion when Peter was added, the
 > > >first
 > > character in his last name actually sorts after Z (despite its
 > > resemblance to an A).
 > This is correct. Don't think of ? as a kind of "A". It's its own
 > letter, which sorts after Z in Swedish.

That's true, but IIRC there are a fairly large number of letters where
different languages collate them in different positions.

Is it worth actually asking appropriate humans to think about this, or
would it be better to use Unicode code point order for simplicity?

