
============================================================================
Subject: [Python-Dev] First post
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] First post
----------------------------------------
Author: Carlos Nepomucen
Attributes: []Content: 
Hi guys! This is my first post on this list.

I'd like have your opinion on how to safely implement WSGI on a production server.

My benchmarks show no performance differences between our PHP and Python environments. I'm using mod_wsgi v3.4 with Apache 2.4.

Is that ok or can it get faster?

Thanks in advance.

Regards,

Carlos 		 	   		  



----------------------------------------
Subject:
[Python-Dev] First post
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Tue, May 14, 2013 at 10:22 AM, Carlos Nepomuceno
<carlosnepomuceno at outlook.com> wrote:

Hi - this list is about the development of Python. For user questions,
python-list is a better place to ask this.



----------------------------------------
Subject:
[Python-Dev] First post
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
On 05/14/2013 08:22 AM, Carlos Nepomuceno wrote:

Hi Carlos!



Unfortunately  this list is for the development /of/ Python, no development /with/ Python.

Try asking again over on the regular Python list:

http://mail.python.org/mailman/listinfo/python-list

--
~Ethan~

