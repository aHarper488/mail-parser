
============================================================================
Subject: [Python-Dev] str(memoryview('abc')) != 'abc' in Python 2.7
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] str(memoryview('abc')) != 'abc' in Python 2.7
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
Is this intended or should I open a bug report for it:

True
False
'<memory at 0x2b2bb6ee26d8>'

I would have expected str(m) == 'abc'.

-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Jul 15 2010)
________________________________________________________________________
2010-07-19: EuroPython 2010, Birmingham, UK                 3 days to go

::: Try our new mxODBC.Connect Python Database Interface for free ! ::::


   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/



----------------------------------------
Subject:
[Python-Dev] str(memoryview('abc')) != 'abc' in Python 2.7
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
I wouldn't assume so - memoryview is meant to eventually support more
than just 1-D views of contiguous memory (see PEP 3118), so that
conversion doesn't seem intuitive to me.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] str(memoryview('abc')) != 'abc' in Python 2.7
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jul 14, 2010, at 3:43 PM, M.-A. Lemburg wrote:


That is also my expectation.

A memoryview object is iterable, so str(memviewobj) should return ''.join(memviewobj).

In your example, that would be:

['a', 'b', 'c']
'abc'


Raymond



----------------------------------------
Subject:
[Python-Dev] str(memoryview('abc')) != 'abc' in Python 2.7
----------------------------------------
Author: M.-A. Lembur
Attributes: []Content: 
Nick Coghlan wrote:

In the example I'm passing in a single dimension contiguous memory
chunk to memoryview(), so in that case I would expect str() of that
memory chunk to return the same value and not the repr() of the
object. buffer() already works like that.

For non-contiguous memory views, the situation is less clear.

AFAIK, non-contiguous views are mostly used to provide a non-copying
view on a subset of multi-dimensional data, e.g. access to a row
or column of a matrix, so the str() should probably return a contiguous
copy of the selected data.

-- 
Marc-Andre Lemburg
eGenix.com

Professional Python Services directly from the Source  (#1, Jul 15 2010)
________________________________________________________________________
2010-07-19: EuroPython 2010, Birmingham, UK                 3 days to go

::: Try our new mxODBC.Connect Python Database Interface for free ! ::::


   eGenix.com Software, Skills and Services GmbH  Pastor-Loeh-Str.48
    D-40764 Langenfeld, Germany. CEO Dipl.-Math. Marc-Andre Lemburg
           Registered at Amtsgericht Duesseldorf: HRB 46611
               http://www.egenix.com/company/contact/



----------------------------------------
Subject:
[Python-Dev] str(memoryview('abc')) != 'abc' in Python 2.7
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 15 Jul 2010 00:43:49 +0200
"M.-A. Lemburg" <mal at egenix.com> wrote:

Well, I think this is intended. str(m) is the human-readable string
representation of the memoryview. In 3.x, you would write bytes(m)
instead to have its bytes contents.
Or, better again, use m.tobytes() in both versions.

Regards

Antoine.



