
============================================================================
Subject: [Python-Dev] Another buildslave - Ubuntu again
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
Hello,

I just got a Ubuntu Server running at my disposal, which could be
connected 24/7 for at least next 3 months.  I am not sure how helpful
it would be to have another buildbot on Ubuntu, but i wanted to play
with it for a while (as I have more comfort with Ubuntu than any other
Unix flavor) before I could change it to cover as OS which is not
already covered by the buildbots.

As instructed here - http://wiki.python.org/moin/BuildBot  could
someone please help create a slavename/slavepasswd on
dinsdale.python.org.
Also, I think the instructions in the wiki could be improved. I was
not able to su - buildbot after installing through package manager. I
shall edit it once I have set it up and running.

Thanks,
Senthil



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Wed, May 2, 2012 at 1:09 PM, Senthil Kumaran <senthil at uthcode.com> wrote:

The page does say: """... create a new user "buildbot" if it doesn't
exist (your package manager might have done it for you)""", but it'd
be nice if it could clarify which are known to do it and which are
known not to, eg "(the Debian and Red Hat package managers will do
this for you)". Or is that too much of a moving target to be worth
trying to specify?

ChrisA



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On 02.05.2012 05:13, Chris Angelico wrote:

I think a buildbot admin should be able to figure out what user the
buildbot to run under himself; if that already is a challenge, it might
be better if he don't run a build slave.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I'm not sure how useful it is to have a build slave which you can't
commit to having for more than 3 months. So I'm -0 on adding this
slave, but it is up to Antoine to decide.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Wed, May 2, 2012 at 1:55 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

I am likely switch to places within 3 months, but I am hoping that
having a 24/7 connected system could provide some experience for
running a dedicated system in the longer run.

Thanks,
Senthil



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
On 02.05.2012 08:07, Senthil Kumaran wrote:

You are talking about experience that you gain, right? Some of the build 
slaves have been connected for many years by now, so "we"
(the buildbot admins) already have plenty experience, which can
be summarized as "Unix good, Windows bad".

I suggest that you can still gain the experience when you are able to
provide a longer-term slave. You are then still free to drop out of
this at any time, so you don't really need to commit to supporting
this for years - but knowing that it likely is only for 3 months
might be too much effort for too little gain.

If you want to learn more about buildbot, I suggest that you also
setup a master on your system. You will have to find one of the hg
pollers as a change source, or additionally setup a local clone with
a post-receive hook which pulls cpython every five minutes or so
through a cron job, and posts changes to the local master.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 2 May 2012 14:07:09 +0800
Senthil Kumaran <senthil at uthcode.com> wrote:

What are the characteristics of your machine? We already have several
Linux x86/x86-64 buildbots... That said, we could also toy with other
build options if someone has a request about that.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 2 May 2012 13:13:15 +1000
Chris Angelico <rosuav at gmail.com> wrote:

That page would probably like a good cleanup. I don't even think
creating an user is required - it's just good practice, and you
probably want that user to have as few privileges as possible.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Wed, May 2, 2012 at 10:54 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:


It is not very unique. It is Intel x86 (32 bit) and 1 GB ram. It is
running Ubuntu Server edition.  Yeah if additional build options (or
additional software configuration options) or some alternative
coverage could be thought off with current config itself, I could do
that.

Thanks,
Senthil



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 3 May 2012 00:25:05 +0800
Senthil Kumaran <senthil at uthcode.com> wrote:

Daily code coverage builds would be nice, but that's probably beyond
what the current infrastructure can offer. It would be nice if someone
wants to investigate that.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Thu, May 3, 2012 at 12:46 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Code coverage buildbots would indeed be good. I could give a try on
this. What kind of infra changes would be required? I presume, it is
the server side that you are referring to.

Thank you,
Senthil



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

That's indeed the motivation. Buildbot slave operators need to
recognize that they are opening their machines to execution of
arbitrary code, even though this could only be abused by committers.

But suppose a committer loses the laptop, which has his SSH key
on it, then anybody getting the key could commit malicious code,
which then gets executed by all build slaves. Of course, it would
be possible to find out whose key has been used (although *not*
from the commit message), and revoke that, but the damage might
already be done.

Regards,
Martin

P.S. Another attack vector is through the master: if somebody
hacks into the machine running the master, they can also compromise
all slaves. Of course, we are trying to make it really hard to
break into python.org.



----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On 04.05.2012 07:21, Senthil Kumaran wrote:

I think the setup could be similar to the daily DMG builder. If the
slave generates a set of HTML files (say), those can be uploaded just
fine. However, we don't have any "make coverage" target currently in
the makefile. So if you contribute that, we could then have it run
daily.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Another buildslave - Ubuntu again
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 4 May 2012 13:21:17 +0800
Senthil Kumaran <senthil at uthcode.com> wrote:

It doesn't *need* to be a buildbot. Just have a cron script somewhere to
run coverage on the test suite every day and published the results
somewhere in a readable format.

Regards

Antoine.

