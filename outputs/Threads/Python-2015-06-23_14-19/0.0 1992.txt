
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.2): Issue #12400:
 runtest() truncates the StringIO stream before a new test
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Issue #12400:
 runtest() truncates the StringIO stream before a new test
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mercredi 29 juin 2011 ? 13:05 -0400, Terry Reedy a ?crit :

Oh crap. I read _pyio source code and .truncate(0) was looking for the
right method to "reset" a StringIO. I tried .truncate(0) in a terminal,
but the zeros ('\x00') are "hidden". sys.stdout.write("\0") doesn't
print anything in my Linux terminal.

Fixed by commit 450209efe272, thank you.

Victor


