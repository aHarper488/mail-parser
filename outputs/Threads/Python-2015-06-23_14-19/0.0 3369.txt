
============================================================================
Subject: [Python-Dev]
	=?utf-8?q?=5BImport-SIG=5D_Where_to_discuss_PEP_382_?=
	=?utf-8?q?vs=2E_PEP_402_=28namespace=09packages=29=3F?=
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?=5BImport-SIG=5D_Where_to_discuss_PEP_382_?=
	=?utf-8?q?vs=2E_PEP_402_=28namespace=09packages=29=3F?=
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
I think restarting the discussion anew here on distutils-sig is appropriate.
-- 
Eric.

Guido van Rossum <guido at python.org> wrote:

Martin has asked me to decide on PEP 382 vs. PEP 402 (namespace
packages) in time for inclusion of the decision in Python 3.3. As
people who attended the language-sig know, I am leaning towards PEP
402 but I admit that at this point I don't have enough information. If
I have questions, should I be asking them on the import-sig or on
python-dev? Is it tolerable if I ask questions even if the answer is
somewhere in the archives? (I spent a lot of time reviewing the
"pitchfork thread",
http://mail.python.org/pipermail/python-dev/2006-April/064400.html,
but that wasn't particularly fruitful, so I'm worried I'd just waste
my time browsing the archives -- if the PEP authors did their jobs
well the PEPs should include summaries of the discussion anyways.)

-- 
--Guido van Rossum (python.org/~guido)
_____________________________________________

Import-SIG mailing list
Import-SIG at python.org
http://mail.python.org/mailman/listinfo/import-sig

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120311/9f53d46e/attachment.html>



----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?=5BImport-SIG=5D_Where_to_discuss_PEP_382_?=
	=?utf-8?q?vs=2E_PEP_402_=28namespace=09packages=29=3F?=
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
And of course I meant import-sig.
-- 
Eric.

"Eric V. Smith" <eric at trueblade.com> wrote:

I think restarting the discussion anew here on distutils-sig is appropriate.
-- 
Eric.

Guido van Rossum <guido at python.org> wrote:

Martin has asked me to decide on PEP 382 vs. PEP 402 (namespace
packages) in time for inclusion of the decision in Python 3.3. As
people who attended the language-sig know, I am leaning towards PEP
402 but I admit that at this point I don't have enough information. If
I have questions, should I be asking them on the import-sig or on
python-dev? Is it tolerable if I ask questions even if the answer is
somewhere in the archives? (I spent a lot of time reviewing the
"pitchfork thread",
http://mail.python.org/pipermail/python-dev/2006-April/064400.html,
but that wasn't particularly fruitful, so I'm worried I'd just waste
my time browsing the archives -- if the PEP authors did their jobs
well the PEPs should include summaries of the discussion anyways.)

-- 
--Guido van Rossum (python.org/~guido)
_____________________________________________

Import-SIG mailing list
Import-SIG at python.org
http://mail.python.org/mailman/listinfo/import-sig

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120311/a3246840/attachment.html>

