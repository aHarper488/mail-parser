
============================================================================
Subject: [Python-Dev] Any script to create the installation pacakge of
 Python 3.3.1 on Windows and *NIX?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Any script to create the installation pacakge of
 Python 3.3.1 on Windows and *NIX?
----------------------------------------
Author: Jianfeng Ma
Attributes: []Content: 
To Python-Dev committers:

I am working on a project to embed a slightly customized Python interpreter in our own software. For easy installation and setup, we want to be able to do the standard Python installation as part of the installation of our product.  So far I have successfully customized and built Python 3.3.1 (including the subprojects) on Windows but I can't find anything in the source distribution to allow me package the binaries/modules etc into a MSI just like the one on the download page on python.org.  So I am asking for information regarding how to package Python build for installation on both Windows and *NIX platforms.  Your help will be greatly appreciated.

Thanks,
Jianfeng
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130509/c735626a/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Any script to create the installation pacakge of
 Python 3.3.1 on Windows and *NIX?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Wed, May 8, 2013 at 7:37 PM, Jianfeng Mao <JMao at rocketsoftware.com> wrote:

See Tools/msi/msi.py for the Windows MSI builder.

