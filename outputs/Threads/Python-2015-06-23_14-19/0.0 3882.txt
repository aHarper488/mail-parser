
============================================================================
Subject: [Python-Dev] enum instances
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Marco Hemmelrat
Attributes: []Content: 
First of all, hi, I'm new to this list.

Following the enum discussions on this list I am kind of confused about 
how enums and their respective instances, i.e. the values, should behave 
in "normal" context.
I apologize beforehand for the mass of "questions" because the following 
contains really many discussed things but they all kinda depend on one 
another, and for terminology which I might've missed.

Considering we have[1]:

     class Color(enum):
         red = 1
         white = 2
         other = "undefined"

     class State(enum):
         idle = 0
         busy = 1
         idling = idle
         ideling = 0

together with the premises:

     1. type(State.busy) == State
     2. type(State) == enum
     3. isinstance(State.idle, State)
     4. State.idle is State.idle

which should mostly be agreed on (if I didn't misinterpret).


How would an enum instance (e.g. State.busy) behave in normal Python 
expressions? Should these instances just wrap their values and provide 
some simple overhead useful for enums?

I'll just note down a few examples of how I think it could work and 
express a few thoughts on them:

     1. State.busy == 1
     2. State.busy == Color.red
     3. int(State.Busy) is 1
     4. isinstance(State.busy, int)
     5. Color.other.startswith("und")
     6. State.busy is not 1
     7. State.busy is not Color.red
     8. State.idle in State
     9. 0 in State # True, False or raise?
     10. State.idling is State.idle
     11. State.idle == State.idling
     12. State.idle is not State.idling

1. & 2.
Considering that enum components contain a value this value should be 
accessable and comperable. If it wasn't then there would be no real need 
to assign a value to it because everything must be compared using the is 
operator anyway. In this case an entirely new syntax could be 
implemented but since I think this is not what we want I'll skip this.

Futhermore, if enum instances should compare equal to their values but 
unequal to each other this creates weird circumstances like:
Color.red == 1 == State.busy -but- Color.red != State.busy == 1

3.
Similar to 1. an enum instance's value /should/ be accessable as an 
expression by itself when needed besides simple operations like 
compatisons. This might be really tricky because the object in question 
is still of type State and some existing uses might break even though I 
can't think of any.

What would certainly be a problem in this case though is that repr() 
would not reveal the value but the actual enum instance. This could be 
problematic when the enum instance's value is not a standard type or 
when repr() and str() differ and repr() is what's needed.

4.
Combines 1. and 2. in class relation. It could be argued that the actual 
value should be an instance of its type subclassed by the enum class 
(State) which is in turn a subclass of enum.

Going further on this it could also allow various things like class 
EnumWithInt(enum, int) which would only allow integers as its enum 
identifiers. This goes slightly into the IntEnum direction but with more 
flexibility.

5.
Related to 3. in that this also allows attribute access to the enum 
instance's value. Considering that the enum instance should already be 
an instance of its value type as well this kinda speaks for itself.

6. & 7.
Obviously, these can't be true even though they compare equal (see 1. 
and 2.).

8.
Check if an enum instance is a member of State. Since in this case the 
same can also be achieved by using isinstance or by comparing the 
instance's type, this is mostly interesting for subclassing enums 
because, but I won't that cover here because it would probably be too much.

9.
Analog to 8. this could either return just work, simply return False 
because this object is obviously not a member of State or raise an 
exception because of a type mismatch (not an enum instance). Also 
interesting for subclassing enums.

10.
See premise 4. on this.

11. & 12.
Even though these compare equal and even if their value object is 
actually the same they themseves should /not/ be the same object. 
Someone needs to find a use case for this though because as of now I can 
only think of this being Pythonic.



So, the most critical part of this would probably be 3. to 7. regarding 
the value types. I'd love to read some thoughts and comments on that.


Regards,
Marco

[1]: http://mail.python.org/pipermail/python-dev/2013-April/125738.html



----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
On 04/29/2013 06:23 PM, Marco Hemmelrath wrote:

1 & 2 are False (would be true if using `.value`)

3 is True

4 & 5 are False (again, need `.value`)

6 is not the correct way to use `is` (should be `==`)

7, 8, 9, 10, 11 are True

12 is False




----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Nikolaus Rat
Attributes: []Content: 
Marco Hemmelrath <marco.hemmelrath at googlemail.com> writes:

State is a class, it just inherits from enum. Thus:

type(State) == type(enum) == type(EnumMetaclass)
issubclass(State, enum) == True


HTH,

   -Nikolaus

-- 
 ?Time flies like an arrow, fruit flies like a Banana.?

  PGP fingerprint: 5B93 61F8 4EA2 E279 ABF6  02CF A9AD B7F8 AE4E 425C




----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 
On 04/29/2013 07:42 PM, Nikolaus Rath wrote:

If you'd tried it, you'd have found that that isn't true.  enum has a 
metaclass, EnumMetaclass.  Thus type(enum) == EnumMetaClass.

That didn't help,


//arry/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130429/a32b36ff/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Nikolaus Rat
Attributes: []Content: 
Larry Hastings <larry at hastings.org> writes:

That is exactly what I wrote above.


Aeh, yes.


Best,

   -Nikolaus

-- 
 ?Time flies like an arrow, fruit flies like a Banana.?

  PGP fingerprint: 5B93 61F8 4EA2 E279 ABF6  02CF A9AD B7F8 AE4E 425C




----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 
On 04/30/2013 07:05 PM, Nikolaus Rath wrote:

type(EnumMetaClass) == type
type(enum) == EnumMetaClass
type(EnumMetaClass) != type(enum)


//arry/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130430/8b1169f1/attachment.html>



----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
On 04/30/2013 07:05 PM, Nikolaus Rath wrote:

Not really.  You wrote

   type(enum) == type(EnumMetaClass)

not

   type(enum) == EnumMetaClass

--
~Ethan~



----------------------------------------
Subject:
[Python-Dev] enum instances
----------------------------------------
Author: Nikolaus Rat
Attributes: []Content: 
On 04/30/2013 07:05 PM, Nikolaus Rath wrote:

Sorry, I must have read what I thought rather than what I wrote. You're
right, what I wrote was wrong.


Best,

   -Nikolaus

-- 
 ?Time flies like an arrow, fruit flies like a Banana.?

  PGP fingerprint: 5B93 61F8 4EA2 E279 ABF6  02CF A9AD B7F8 AE4E 425C

