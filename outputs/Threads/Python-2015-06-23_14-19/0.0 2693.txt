
============================================================================
Subject: [Python-Dev]  time.clock_info() field names
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]  time.clock_info() field names
----------------------------------------
Author: Jim J. Jewet
Attributes: []Content: 


In http://mail.python.org/pipermail/python-dev/2012-April/119134.html
Benjamin Peterson wrote:


I agree with monotonic, but I think it should be "adjustable".

To me, "adjusted" and "is_adjusted" both imply that an adjustment
has already been made; "adjustable" only implies that it is possible.

I do remember concerns (including Stephen J. Turnbull's
<CAL_0O19nmi0+zB+tV8poZDAffNdTnohxo9y5dbw+E2q=9rX9YA at mail.gmail.com> )
that "adjustable" should imply at least a list of past adjustments,
and preferably a way to make an adjustment.

I just think that stating it is adjustable (without saying how, or
whether and when it already happened) is less wrong than claiming it
is already adjusted just in case it might have been.

-jJ

-- 

If there are still threading problems with my replies, please 
email me with details, so that I can try to resolve them.  -jJ


