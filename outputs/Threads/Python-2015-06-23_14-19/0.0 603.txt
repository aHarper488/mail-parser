
============================================================================
Subject: [Python-Dev] more details of list comprehension in tutorial
	than in language reference
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] more details of list comprehension in tutorial
	than in language reference
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 16 Jul 2010 11:32:25 -0500, Peng Yu <pengyu.ut at gmail.com> wrote:

It is not, really.  Documentation issues should be posted as bugs
in the bug tracker under the 'Documentation' component.


The Language Reference is intentionally terse.  If there is important
syntactic/semantic information *missing* then it should be added, but if
it is just lacking in the detail of the explanations, then the tutorial
is the place for those, and you say that is OK.

--
R. David Murray                                      www.bitdance.com

