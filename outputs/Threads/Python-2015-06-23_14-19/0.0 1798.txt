
============================================================================
Subject: [Python-Dev] Speed.Python.org
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Speed.Python.org
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
Now that we have the machine, we need to start working on
collecting/organizing the resources needed to get a shared codespeed
system in place. After speaking with various people, we felt that
overloading codespeed-dev, pypy-dev or python-dev with the discussions
around this would be sub optimal. I've spun up a new mailing list
here:

http://mail.python.org/mailman/listinfo/speed

Those who are interested in working on or contributing to the
speed.python.org project can subscribe there. I personally can not
lead the project, and so I will be looking to the current
speed.pypy.org team, and python-dev contributors for leadership in
this. I got you the hardware and hosting! :)

jesse

