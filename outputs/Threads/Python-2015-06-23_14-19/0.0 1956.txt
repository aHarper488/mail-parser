
============================================================================
Subject: [Python-Dev] Buildbot status web pages
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Buildbot status web pages
----------------------------------------
Author: David Bole
Attributes: []Content: 
I've been receiving 503 errors from the buildbot web status pages
beneath www.python.org/dev/buildbot for a day or two now - is there
perhaps something that needs a bit of a kick-start?

Thanks.

-- David


