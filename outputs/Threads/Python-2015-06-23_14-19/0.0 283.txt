
============================================================================
Subject: [Python-Dev] PEP 11: Dropping support for ten year old systems
Post Count: 28
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?ISO-8859-15?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
I'd like to tighten PEP 11, and declare a policy that systems
older than ten years at the point of a feature release are not
supported anymore by default. Older systems where support is still
maintained need to be explicitly listed in the PEP, along with
the name of the responsible maintainer (I think this would currently
only cover OS2/EMX which is maintained by Andrew MacIntyre).

Support for such old platforms can then be removed from the codebase
immediately, no need to go through a PEP 11 deprecation cycle.

As a consequence, I would then like to remove support for Solaris
versions older than Solaris 8 (released in January 2000, last updated
by Sun in 2004) from the configure script for 3.2b2. A number of other
tests in configure.in could probably also be removed, although I
personally won't touch them before 3.2.

The other major system affected by this would be Windows 2000, for which
we already decided to not support it anymore.

Opinions?

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 05 Dec 2010 22:48:49 +0100
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Seconded!


Is there any 2000-specific code (as opposed to XP-compatible)?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sun, Dec 5, 2010 at 14:14, Antoine Pitrou <solipsis at pitrou.net> wrote:

Thirded!

-Brett




----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Yes: a number of APIs didn't exist in W2k, so we currently use
LoadLibrary/GetProcAddress to call them. These can be replaced with
regular calls. _WINNT (or whatever the macro is called) then needs to
be bumped accordingly.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/5/2010 4:48 PM, "Martin v. L?wis" wrote:

WinXP (released August 2001) should be supported a lot longer than 
another year ;-) . It is still supported and installed on new systems.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

Good catch. Windows XP, according to reports online has somewhere between
50-75% marketshare of all personal computers in the world. The Vista fiasco
has created a large void which is only now starting to get filled with
Windows 7, but it will yet take a long time to dethrone XP.

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101206/4b89ee0f/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Dec 6, 2010 at 7:48 AM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

I would prefer to be guided by vendor EOL dates rather than our own
arbitrary 10 year limit. The EOL guide I would suggest is "Is the
vendor still fixing bugs in that release?".

For Solaris 8, the answer to that question is no (Phase 1 EOL support
ended in October: http://www.sun.com/service/eosl/eosl_solaris.html)

For Windows 2000, the answer is no (Extended Support ended in July:
http://support.microsoft.com/lifecycle/?p1=7274)

For Windows XP, the answer is yes (Extended Support doesn't end until
April 2014: http://windows.microsoft.com/en-us/windows/products/lifecycle#section_2)

Since the "Is the vendor still patching it?" guideline gives the
"right" answer for the 3 systems mentioned in this thread, it will
likely do a better job of covering anomalies like XP than a flat year
limit would.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Hirokazu Yamamot
Attributes: []Content: 
On 2010/12/06 6:48, "Martin v. L?wis" wrote:

I'm +1/2 for supporting Windows 2000...



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 06.12.2010 05:36, schrieb Nick Coghlan:

If available, perhaps. Not all system vendors have such a policy,
or it may not be easy to find. If that is part of the policy, I'd
like to add the specific systems where we use this approach along
with the URLs that list the policies in the PEP.

As a counter-example, I think the only way to phase out support
for old OpenBSD releases is that we set a date.

If this policy is applied to Debian also, we may get unhappy users,
since Debian releases get out of love fairly quickly. OTOH, supporting
old Linux releases really isn't that much of a problem.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Seung =?utf-8?b?U29vLA==?= H
Attributes: []Content: 
Nick Coghlan <ncoghlan <at> gmail.com> writes:

wrote:
+1

I think Nick has a point.

If the vendor is willing to patch/support said version, then I think it would be 
worthwhile for the Python community to provide support 

EOL dates of prominent Linux distribution :

RHEL:
https://access.redhat.com/support/policy/updates/errata/
Ubuntu:
http://en.wikipedia.org/wiki/List_of_Ubuntu_releases#Version_timeline
(http://www.ubuntu.com/products/ubuntu/release-cycle seems to be down)
SuSe
http://support.novell.com/lifecycle/

I do foresee a possible side-effect regarding Fedora as "maintained for 
approximately 13 months", because "Release X is supported until one 
month after the release of Release X+2."

http://fedoraproject.org/wiki/LifeCycle

Considering the nature of the Fedora project, dropping unsupported fedora 
distributions may or may not be helpful for Pyhton and it's users.

Also, it is not clear what to do about distributions/OSs 
without any official EOL or life cycles.





----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Dec 6, 2010 at 5:28 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

I would be fine with an EOL based policy for single-vendor platforms
(specifically Solaris and Windows) and a date-based policy for
everything else.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Jeroen Ruigrok van der Werve
Attributes: []Content: 
-On [20101206 08:30], "Martin v. L?wis" (martin at v.loewis.de) wrote:

If you want, I can provide you with specifics on the BSD platforms of what
is currently in use, EOL and the future.

Also, with regard to Windows 2000, XP and its APIs. Didn't XP already switch
a bunch of APIs around that 2000 doesn't use? (Need to find that migration
document.)

-- 
Jeroen Ruigrok van der Werven <asmodai(-at-)in-nomine.org> / asmodai
????? ?????? ??? ?? ??????
http://www.in-nomine.org/ | http://www.rangaku.org/ | GPG: 2EAC625B
Nothing is more honorable than enlightenment, nothing is more beautiful
than virtue...



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 06.12.2010 09:36, schrieb Jeroen Ruigrok van der Werven:

If that's publicly documented from a single starting page, having the
URL of that page would be appreciated. Also, do users accept that
policy? (i.e. would they feel sad if Python releases don't support BSD
releases anymore that don't get patches?)

For Windows and Solaris, it seems that some users continue to use the
system after the vendor stops producing patches, and dislike the
prospect of not having Python releases for it anymore. However, they
are in clear minority, so by our current policy for minority platforms
(no need to support them), that's fine. This really triggered the "ten
years" proposal: for quite some time now (20 years) people stop losing
interest in operating systems after ten years.


I don't understand the question: what does it meant to switch an API
around? XP has added new API that wasn't available in 2000, if that's
what you are asking.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

I think I would need more information than that. Nick's proposal was
more specific: when does the vendor stop producing patches? This is
a clear criterion, and one that I support.


My interpretation: Python support until end of production phase 3 (7 years).


I'd prefer something more official than Wikipedia, though.


My interpretation: Python support until end of Extended support phase
(7 years).

So by this policy, RHEL and SuSE users would be off worse than with
my original proposal (10 years).


Again, for Linux, I think the issue is somewhat less critical: in terms
of portability and ABI stability, it seems like they manage best (i.e.
we have least version-dependent code for Linux in Python, probably
because a "Linux version" doesn't exist in the first place, so
distributions must provide source and binary compatibility even
across vendors, making such support across versions more easy).


Here my proposal stands: 10 years, by default.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Seung =?utf-8?b?U29vLA==?= H
Attributes: []Content: 
Nick Coghlan <ncoghlan <at> gmail.com> writes:


+1
I also think this would be for the best.






----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Andrew Bennett
Attributes: []Content: 
"Martin v. L?wis" wrote:
[...]

https://wiki.ubuntu.com/Releases

-Andrew.




----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On 6 December 2010 09:18, "Martin v. L?wis" <martin at v.loewis.de> wrote:

How about max(EOL, 10years)?  That sounds like it could be a useful guideline.

(Personally I'd be sad to see Solaris 8 go in the next few years)

Regards
Floris


-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 06.12.2010 14:40, schrieb Floris Bruynooghe:

I guess we'll be sorry, then: under that policy, max(EOL, 10years) comes
out as "not supported" (not sure whether you were aware of that).

Of course, with these old systems, I really wonder: why do they need
current Python releases? 2.7 will remain available and maintained for
some time, and 3.1 will at least see security fixes for some more time -
something that the base system itself doesn't receive anymore. So
if you needed a Python release for Solaris 8, you could just use Python
2.3, no? We are not going to take the sources of old releases offline.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/6/2010 4:08 AM, "Martin v. L?wis" wrote:


I quite suspect that XP will be in major use (more than say, current 
BSD) for some years after MS stops official support. Why rush to drop 
it? Is there much XP specific stuff in the current xp/vista/7 code?

It seems to me that the rule should be something like "around 10 years 
or end of support, as modified by popularity, the burden of continued 
support, the availability of test machines, and the availability of people".

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Stephen Hanse
Attributes: []Content: 
On 12/6/10 10:55 AM, "Martin v. L?wis" wrote:

For things like Solaris 8, I have no thoughts one way or the other-- but
considering Windows XP is hitting 10 years next year-- Personally? My
entirely theoretical timetable for when I think I'll be able to finally
upgrade to Python 3 (where I'll skip to whatever the latest Python 3
is), is actually shorter by at least a few years then my timetable of
when I think I'll be able to drop support of Windows XP. Unfortunately.

WinXP is old but *pervasive* still in my experience with small
businesses / customers. Many aren't even considering making a plan for
Win7 yet.

So if two years rolls around and Python 3.x (where 'x' is 'whatever is
current') isn't supported on Windows XP, I'll be very sad, and will have
to be stuck on Python 3.x-1 for .. awhile, where "awhile" is out of my
control and up to the Masses who are unable or can't be bothered with
fixing what works for them w/ WinXP.

-- 

   Stephen Hansen
   ... Also: Ixokai
   ... Mail: me+python (AT) ixokai (DOT) io
   ... Blog: http://meh.ixokai.io/

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 487 bytes
Desc: OpenPGP digital signature
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101206/a918d7b8/attachment-0001.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 06.12.2010 20:25, schrieb Terry Reedy:

What rush to drop it, specifically? It will be supported as long as
Microsoft provides patches, as per Nick's amendment. For Windows XP,
the extended lifecycle support (end of security patches) will be on
April 8, 2014. I wouldn't call that "rushing", at this point in time.
By our current release pace, Python 3.5 might be the first release
to not support XP anymore.


I don't know; I haven't investigated unsupporting XP yet. I'm concerned
about Windows 2000, at the moment.


In my original posting, I proposed a clause where support could be
extended as long as an individual steps forward to provide that support.
So if XP remains popular by the time Microsoft stops providing patches
for it, some volunteer would have to step forward.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
Hi,

2010/12/6 "Martin v. L?wis" <martin at v.loewis.de>


+1. Such a clause is already used to keep supporting the old VC6.0 compiler
(a.k.a VC98!)

-- 
Amaury Forgeot d'Arc
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101206/06ca79c3/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/6/2010 3:46 PM, "Martin v. L?wis" wrote:


On the day MS stops support. But it is a moot support until them.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: David Malcol
Attributes: []Content: 
On Mon, 2010-12-06 at 10:18 +0100, "Martin v. L?wis" wrote:

(...)


Red Hat continues to provide patches for RHEL within the "Extended Life
Cycle" (years 8, 9 and 10), but it's an optional add-on.

So another interpretation of the above with Nick's proposal could be 10
years on RHEL.  (though obviously I'm biased in favor of RHEL)

Approaching this from another angle: please do add me to the "nosy" on
any compatibility bugs with running latest python code on RHEL.  I'm
also looking into getting RHEL buildbot machines, FWIW.



The other compat issues are in the toolchain: e.g. very recent versions
of gcc .  In downstream Fedora, we tend to be amongst the first to run
into new compilation warnings (and, occasionally, "exciting"
code-generation bugs...)

But this tends to be the opposite kind of problem: beginning of life,
rather than end-of-life, and these sorts of things will need fixing for
every Linux build eventually.

FWIW, I'm trying to keep Fedora's "system" python 2 and python 3 builds
as up-to-date as reasonable, so Fedora users will (I hope) be running
fairly recent code python as is.  We have 2.7 as /usr/bin/python as of
F14, for instance.


Hope this is helpful
Dave




----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

My understanding is that you keep the patches available - but you
don't produce any new ones, right?


I wouldn't count mere availability of old patches on the server as
"support".


I'll try to remember, but really can't promise. But then, as I said
before: I think Linux support in Python is particularly easy. For
example, there isn't a single distribution-specific test in
configure.in.


Dropping support for old gcc versions (or other old compiler versions)
is probably an issue on its own. It will be difficult to figure out
what work-arounds are in place for what particular compiler glitch.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On 6 December 2010 18:55, "Martin v. L?wis" <martin at v.loewis.de> wrote:

I was :-)
Which is why I said "sad" and not "panic" or something.  I do realise
I'm probably in the minority and that it doesn't justify the burden on
everyone in the Python community.


exactly

Regards
Floris

-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org



----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: David Malcol
Attributes: []Content: 
On Tue, 2010-12-07 at 00:05 +0100, "Martin v. L?wis" wrote: 

It typically involves backporting (and testing) security fixes to the
older versions of the various OS packages.  Whether or not the results
of that work count as "new patches" is debatable.

I don't know if CentOS and the other people who rebuild the RHEL sources
track those final 3 years.

Dave





----------------------------------------
Subject:
[Python-Dev] PEP 11: Dropping support for ten year old systems
----------------------------------------
Author: David Malcol
Attributes: []Content: 
On Tue, 2010-12-07 at 00:05 +0100, "Martin v. L?wis" wrote: 

It typically involves backporting (and testing) security fixes to the
older versions of the various OS packages.  Whether or not the results
of that work count as "new patches" is debatable.

I don't know if CentOS and the other people who rebuild the RHEL sources
track those final 3 years.

Dave



