
============================================================================
Subject: [Python-Dev] The step command of pdb is broken
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Xavier de Gay
Attributes: []Content: 
Issue http://bugs.python.org/issue13183 raises the point that the step
command of pdb is broken. This issue is 6 months old. A patch and test
case have been proposed. The 'Lifecycle of a Patch' at
http://docs.python.org/devguide/patch.html says
<quote>
If your patch has not received any notice from reviewers (i.e., no
comment made) after a substantial amount of time then you may email
python-dev at python.org asking for someone to take a look at your patch.
</quote>
I am the author of pyclewn, a Vim front end to pdb and gdb, and I
would be grateful for any progress on this issue.

The following pdb session shows the problem when running the three
modules main.py, foo.py and bar.py. After the second step command, pdb
does not stop (as it should) at lines foo.py:5 and foo.py:6, nor does
it stop to print the return value of increment().
=================================================
main.py
     1  import foo
     2
     3  result = foo.increment(100)
     4  print('result', result)
foo.py
     1  import bar
     2
     3  def increment(arg):
     4      v =  bar.value()
     5      result = arg + v
     6      return result
bar.py
     1  def value():
     2      return 5
=================================================
$ python -m pdb main.py
-> import foo
(Pdb) import sys; sys.version
'3.3.0a2+ (default:2c27093fd11f, Apr 30 2012, 10:51:35) \n[GCC 4.3.2]'
(Pdb) break bar.py:2
Breakpoint 1 at /path_to/bar.py:2
(Pdb) continue
-> return 5
(Pdb) step
--Return--
-> return 5
(Pdb) step
-> print('result', result)
(Pdb)
=================================================


Xavier



----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
IT would be good if the author of one of the pdb add-ons such as (I
believe) pdb2 could comment on this bug.

On Mon, Apr 30, 2012 at 3:31 AM, Xavier de Gaye <xdegaye at gmail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 30, 2012, at 08:42 AM, Guido van Rossum wrote:


Maybe we should take this opportunity (Python 3.3) to consider adopting one of
the pdb add-ons or borging the best of their bits into the stdlib?

-Barry



----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Apr 30, 2012 at 9:09 AM, Barry Warsaw <barry at python.org> wrote:

I thought we already took most of the useful bits of one of these...
(Admitted I'm vague on details and haven't the time to research.)

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Mon, Apr 30, 2012 at 12:09:02PM -0400, Barry Warsaw wrote:

Irrespective of this - Issue13183 seems to be an easy to verify bug in
3.2 and 3.3. I think, it would most visible if you were to use a full
screen debugger and you will notice that the return call indicator has
jumped to the next statement (skipping return) when returning. I
guess, that's why Xavier (pyclewn author) noted it.  The fix seems
fine too.

I have just requested an additional info and this particular one could
be fixed.

Thanks,
Senthil




----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Senthil, if you can shepherd this patch to completion that would be great!

On Mon, Apr 30, 2012 at 9:57 AM, Senthil Kumaran <senthil at uthcode.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Xavier de Gay
Attributes: []Content: 
On Mon, Apr 30, 2012 at 6:57 PM, Senthil Kumaran wrote:


Thanks for your help on this issue Senthil.

Xavier



----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On 30.04.2012 18:09, Barry Warsaw wrote:

I think the same policies should apply that I want to see followed for 
any other inclusion into the stdlib: we shouldn't "adopt" any code that
is not explicitly contributed, by it's author.

That's not only the legal issues, but also the responsibility for the
code. Otherwise, we end up with code that still nobody owns, and the
out-of-core version still gets better support.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] The step command of pdb is broken
----------------------------------------
Author: Xavier de Gay
Attributes: []Content: 
On Mon, Apr 30, 2012 at 12:31 PM, Xavier de Gaye wrote:

Other pdb commands are also broken for the same reason (no trace
function setup in the targeted caller frame).
A new http://bugs.python.org/issue14728 has been submitted with a
proposed patch for these commands and the corresponding test cases.
The patch removes a while loop from the fast path, and that should also
provide an improvement of the performance of Pdb.

Xavier

