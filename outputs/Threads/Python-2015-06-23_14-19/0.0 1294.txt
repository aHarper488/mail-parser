
============================================================================
Subject: [Python-Dev] Python 3.3 release schedule posted
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Wed, Mar 23, 2011 at 9:56 PM, Georg Brandl <g.brandl at gmx.net> wrote:

Why this isn't being added to Release Calendar on the front page?
Do you have an estimate of Python 3.2.1 release?



----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 02.04.2011 15:00, schrieb anatoly techtonik:

It is.


Not yet.

Georg




----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le samedi 02 avril 2011 ? 16:00 +0300, anatoly techtonik a ?crit :

FYI I introduced (and then fixed) two regressions specific to Windows in
Python 3.2:
http://bugs.python.org/issue11272 (input)
http://bugs.python.org/issue11395 (print)

Issue #11272 is annoying: input() returns a string ending a string
ending by '\r'. The workaround is input().rstrip('\r')or
input().rstrip().

The second end is more a corner case: print fails if you write more than
60,000 bytes at once.

Victor




----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
I've posted a very preliminary Python 3.3 release schedule as PEP 398.
The final release is set to be about 18 months after 3.2 final, which
is in August 2012.

For 3.3, I'd like to revive the tradition of listing planned large-scale
changes in the PEP.  Please let me know if you plan any such changes,
at any time.  (If they aren't codified in PEP form, we should think about
whether they should be.)

The "Candidate PEPs" I listed are those open PEPs that in my opinion have
the highest chance to be accepted and implemented for 3.3.  It is by no
means binding.

cheers,
Georg




----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Carl Meye
Attributes: []Content: 
Hi Georg,

On 03/23/2011 03:56 PM, Georg Brandl wrote:

Over in distutils-sig there's been extensive discussion at and since
PyCon of a built-in Python virtual-environment tool, similar to
virtualenv, targeted hopefully for 3.3. This is something that's seen
some discussion on python-dev previously; I now have a working prototype
and am working on a PEP. When the PEP is ready I'll bring it up for
discussion on python-ideas and then python-dev; anyone interested in
checking it out sooner can go read the discussions at distutils-sig.

Carl



----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Mar 24, 2011 at 5:56 AM, Georg Brandl <g.brandl at gmx.net> wrote:

I would add a note about a "standardised event loop interface" -
that's a PEP idea that came up at the language summit, and LvH plans
to write it with support from the Twisted crew and others.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Thomas Wouter
Attributes: []Content: 
On Thu, Mar 24, 2011 at 00:12, Nick Coghlan <ncoghlan at gmail.com> wrote:


It ended up that Jim Fulton is actually writing the PEP (with input from
Twisted people and others.)

-- 
Thomas Wouters <thomas at python.org>

Hi! I'm a .signature virus! copy me into your .signature file to help me
spread!
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110324/dcb3d296/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 23/03/2011 19:56, Georg Brandl wrote:

During the language summit we also discussed formalising in a pep the 
rule that all modules that have a pure python form and a C accelerator 
must keep the pure python and C form precisely in sync and the same 
tests must be run against both. Brett said he would write the pep after 
he has switched import to use importlib...

There was also the "compatibility warning", probably enabled by a 
command line flag, that would warn on the use of CPython specific 
implementation details (for example using non-string keys in class 
dicts). There is no owner for this as a PEP.

They're both mentioned in Nick Coghlan's summary:

http://www.boredomandlaziness.org/2011/03/python-language-summit-highlights.html

Another item discussed at the language summit was moving the standard 
library into its own repository. Jesse Noller started working on a PEP 
but it stalled waiting for the transition to mercurial and I don't know 
if he is volunteering to pick up the PEP again now that has happened.

All the best,

Michael Foord



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mercredi 23 mars 2011 ? 20:56 +0100, Georg Brandl a ?crit :

I am still working on the import machinery to fix last bugs related to
Unicode. So it will be possible to do an useless "import caf?" in Python
3.3, on any platform. But it is not really an huge change (for the user,
but an huge change in the code ;-)).

I will probably add my faulthandler module into 3.3.

Victor




----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 23/03/11 20:56, Georg Brandl wrote:

I want to integrate dependence injection in the stdlib, specially in
libs creating sockets. This would be an optional feature.

The idea would be, for instance, that smtplib could call an optional
constructor to create a socket-like object, if some is provided.

That would break the implicit dependency with sockets, reduce
monkey-patching need, and ease testing the libs.

I proposed this on the list like a year ago. Somebody else said he was
planning a PEP... not done yet, I guess.

Digging the archive:
<http://mail.python.org/pipermail/python-dev/2010-April/099237.html>

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBTYqnDplgi5GaxT1NAQJTxQP/U9dk4x9r9Fbwm7zEzEMydzxqqiY/FF6z
PA52YDRPUgyeiDnhmZwk1f9PqnwYWkgf2qmDi+v0eXLZJYqs/rBEcaY36yP1sgP+
+RrFp4aTpf6oqp7HSwrJMoOS2BmRZxLmzShfKecX+3q33Ix1C1EvcF8F8Yg4P4s6
Pd+jRaGvJRs=
=Ec5o
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 24/03/2011 02:06, Jesus Cea wrote:

Although from this email it seems like it is for much more than just 
testability, in which case go ahead. :-)

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Laurens Van Houtve
Attributes: []Content: 
On Thu, Mar 24, 2011 at 12:18 AM, Thomas Wouters <thomas at python.org> wrote:

Well, if help is still needed I'll gladly chip in. It's not  that I'm not
interested in doing it -- it's just that I don't know who's supposed to or
who's working on it :)

-- 
cheers
lvh
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110326/267099e7/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Python 3.3 release schedule posted
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 26/03/2011 00:33, Laurens Van Houtven wrote:

Hey lvh,

It's worth following this up. If Jim Fulton hasn't had time to move this 
forward and you have the bandwidth to work on it then it would be great 
to see some action.

All the best,

Michael Foord



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110525/8e5e2c48/attachment.html>

