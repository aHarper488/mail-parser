
============================================================================
Subject: [Python-Dev] cpython: give the AST class a __dict__
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: give the AST class a __dict__
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

On Mon, 12 Mar 2012 17:56:10 +0100
benjamin.peterson <python-checkins at python.org> wrote:

This seems to have broken the Windows buildbots.

cheers

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython: give the AST class a __dict__
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

http://hg.python.org/cpython/rev/6bee4eea1efa should fix the
compilation on Windows.

Victor

