
============================================================================
Subject: [Python-Dev] cpython (3.2): Fix closes Issue12315 - Updates to
 http.client documentation.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Fix closes Issue12315 - Updates to
 http.client documentation.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 20.06.2011 02:00, senthil.kumaran wrote:

This is not a big deal, and I'm not picking specially on you here, Senthil,
it's just something that I've noticed several times:

Newlines are a valuable tool for structuring reST files (just like in Python
files).  I tried to set up a convention to separate large blocks (such as
sections) by two newlines, to make it easier to scroll and find what you're
looking for.  Please try to keep this intact.

Thanks,
Georg




----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Fix closes Issue12315 - Updates to
 http.client documentation.
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Mon, Jun 20, 2011 at 09:11:20AM +0200, Georg Brandl wrote:

Noted. In the next checkin to this file, I shall correct this one and
add extra line before the Example section. 

Thanks,
Senthil

