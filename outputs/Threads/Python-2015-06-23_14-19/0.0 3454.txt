
============================================================================
Subject: [Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Jason R. Coomb
Attributes: []Content: 
I see this was reported as a debian bug.
http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=665776

 

We encountered it as well.

 

To reproduce, using virtualenv 1.7+ on Python 2.7.2 on Ubuntu, create a
virtualenv. Move that virtualenv to a host with Python 2.7.3RC2 yields:

 

jaraco at vdm-dev:~$ /usr/bin/python2.7 -V

Python 2.7.3rc2

jaraco at vdm-dev:~$ env/bin/python -V

Python 2.7.2

jaraco at vdm-dev:~$ env/bin/python -c "import os; os.urandom()"

Traceback (most recent call last):

  File "<string>", line 1, in <module>

AttributeError: 'module' object has no attribute 'urandom'

 

This bug causes Django to not start properly (under some circumstances).

 

I reviewed the changes between v2.7.2 and 2.7 (tip) and it seems there was
substantial refactoring of the os and posix modules for urandom.

 

I still don't fully understand why the urandom method is missing (because
the env includes the python 2.7.2 executable and stdlib).

 

I suspect this change is going to cause some significant backward
compatibility issues. Is there a recommended workaround? Should I file a
bug?

 

Regards,

Jason

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120328/7f70b7c8/attachment-0001.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 6662 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120328/7f70b7c8/attachment-0001.bin>



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Carl Meye
Attributes: []Content: 
Hi Jason,

On 03/28/2012 12:22 PM, Jason R. Coombs wrote:

In Python 2.6.8/2.7.3, urandom is built into the executable. A
virtualenv doesn't contain the whole stdlib, only the bits necessary to
bootstrap site.py. So the problem arises from trying to use the 2.7.3
stdlib with a 2.7.2 interpreter.


The workaround is easy: just re-run virtualenv on that path with the new
interpreter.

I was made aware of this issue a few weeks ago, and added a warning to
the virtualenv "news" page:
http://www.virtualenv.org/en/latest/news.html  I'm not sure where else
to publicize it.

Carl

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 198 bytes
Desc: OpenPGP digital signature
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120328/d40373ed/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Jason R. Coomb
Attributes: []Content: 

Thanks for the quick response Carl. I appreciate all the work that's been
done.

I'm not sure the workaround is as simple as you say. Virtualenv doesn't
replace the 'python' exe if it already exists (because it may already exist
for a different minor version of Python (3.2, 2.6)). So the procedure is
probably something like this:

For each <minor> version of Python the virtualenv wraps (ls
env/bin/python?.?):
1) Run env/bin/python -V. If the result starts with "Python <minor>", remove
env/bin/python.
2) Determine if that Python version uses distribute or setuptools.
3) Run virtualenv --python=python<minor> env (with --distribute if
appropriate)

I haven't yet tested this procedure, but I believe it's closer to what will
need to be done. There are probably other factors. Unfortunately, to
reliably repair the virtualenv is very difficult, so we will probably opt
with re-deploying all of our virtualenvs.

Will the release notes include something about this change, since it will
likely have broad backward incompatibility for all existing virtualenvs? I
wouldn't expect someone in operations to read the virtualenv news to find
out what things a Python upgrade will break. Indeed, this update will
probably be pushed out as part of standard, unattended system updates.

I realize that the relationship between stdlib.os and posixmodule isn't a
guaranteed interface, and the fact that it breaks with virtualenv is a
weakness of virtualenv. Nevertheless, virtualenv has become the defacto
technique for Python environments. Putting my sysops cap on, I might
perceive this change as being unannounced (w.r.t. Python) and having
significant impact on operations. I would think this impact deserves at
least a note in the release notes.

Regards,
Jason
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 6662 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120328/1562258f/attachment.bin>



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Jason R. Coomb
Attributes: []Content: 
Carl,

I've drafted some notes: http://piratepad.net/PAZ3CEq9CZ

Please feel free to edit them. If you want to chat, I can often be reached
on freenode as 'jaraco' or XMPP at my e-mail address if you want to sprint
on this in real-time.

Does the issue only exist for Python 2.6 and 2.7?

I'm not familiar with the release process. What's the next step?


change is
adding
your

-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 6662 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120329/f961b80d/attachment.bin>



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Carl Meye
Attributes: []Content: 
Thanks Jason for raising this. I just assumed that this was virtualenv's
fault (which it is) and didn't consider raising it here, but a note in
the release notes for the affected Python versions will certainly reach
many more of the likely-to-be-affected users.

FTR, I confirmed that the issue also affects the upcoming point releases
for 3.1 and 3.2, as well as 2.6 and 2.7. Jason filed issue 14444 to
track the addition to the release notes for those versions.

Carl

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 198 bytes
Desc: OpenPGP digital signature
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120329/ff569633/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: David Malcol
Attributes: []Content: 
On Wed, 2012-03-28 at 18:22 +0000, Jason R. Coombs wrote:

It looks like this a symptom of the move of urandom to os.py to
posximodule et al.

At first glance, it looks like this specific hunk should be reverted:
http://hg.python.org/cpython/rev/a0f43f4481e0#l7.1
so that if you're running with the new stdlib but an old python binary
the combination can still have a usable os.urandom

Should this be tracked in bugs.python.org?

Hope this is helpful
Dave




----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Carl Meye
Attributes: []Content: 
On 03/29/2012 11:39 AM, David Malcolm wrote:

Indeed, I've just tested and verified that this does fix the problem.


I've added this option as a comment on bug 14444. The title of that bug
is worded such that it could be reasonably resolved either with the
backwards-compatibility fix or the release notes addition, the release
managers can decide what seems appropriate to them.

Carl

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 198 bytes
Desc: OpenPGP digital signature
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120329/60a220e8/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 30, 2012 at 4:26 AM, Carl Meyer <carl at oddbird.net> wrote:

+1 for restoring the fallback code in os.py. A security release
definitely shouldn't be breaking that kind of thing.

Regards,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 29/03/2012 22:04, Nick Coghlan a ?crit :

The RMs have already agreed not to restore the fallback, and the
maintainer of virtualenv agreed.  See report for more details.

Cheers



----------------------------------------
Subject:
[Python-Dev] Virtualenv not portable from Python 2.7.2 to 2.7.3
 (os.urandom missing)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 30, 2012 at 12:07 PM, ?ric Araujo <merwok at netwok.org> wrote:

The details are pretty short (and make sense), so I'll repeat the most
salient points here for the benefit of others:

- if the binary in a virtualenv isn't updated, then projects running
in a virtualenv won't receive the security fix when the updated
version of Python is installed
- restoring the fallback in os.py would make this failure mode
*silent*, so a user could upgrade Python, set PYTHONHASHSEED, but fail
to realise they also need to update the binary in the virtualenv in
order to benefit from the hash protection
- with the current behaviour, failing to upgrade the binary results in
a noisy ImportError or AttributeError related to os.urandom, which the
release notes and virtualenv help channels can instruct people on
handling. This approach explicitly covers the additional steps needed
to fully deploy the security fix when using virtualenv.

That rationale makes sense to me, too.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

