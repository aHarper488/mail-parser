
============================================================================
Subject: [Python-Dev] Incorrect length of collections.Counter objects /
	Multiplicity function
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Incorrect length of collections.Counter objects /
	Multiplicity function
----------------------------------------
Author: Gustavo Nare
Attributes: []Content: 
Hello, everyone.

I've checked the new collections.Counter class and I think I've found a bug:


The length of a Counter is the amount of unique elements. But the length must 
be the cardinality, and the cardinality of a multiset is the total number of 
elements (including duplicates) [1] [2]. The source code mentions that the 
recipe on ActiveState [3] was one of the references, but that recipe has this 
right.

Also, why is it indexed? The indexes of a multiset call to mind the position 
of its elements, but there's no such thing in sets. I think this is 
inconsistent with the built-in set. I would have implemented the multiplicity 
function as a method instead of the indexes:
    c1.get_multiplicity(element)
    # instead of
    c1[element]

Is this the intended behavior? If so, I'd like to propose a proper multiset 
implementation for the standard library (preferably called "Multiset"; should 
I create a PEP?). If not, I can write a patch to fix it, although I'm afraid 
it'd be a backwards incompatible change.

Cheers,

[1] http://en.wikipedia.org/wiki/Multiset#Overview
[2] http://preview.tinyurl.com/smalltalk-bag
[3] http://code.activestate.com/recipes/259174/
-- 
Gustavo Narea <xri://=Gustavo>.
| Tech blog: =Gustavo/(+blog)/tech  ~  About me: =Gustavo/about |



----------------------------------------
Subject:
[Python-Dev] Incorrect length of collections.Counter objects /
	Multiplicity function
----------------------------------------
Author: Gustavo Nare
Attributes: []Content: 
Anyone?


Gustavo said:
-- 
Gustavo Narea <xri://=Gustavo>.
| Tech blog: =Gustavo/(+blog)/tech  ~  About me: =Gustavo/about |



----------------------------------------
Subject:
[Python-Dev] Incorrect length of collections.Counter objects /
	Multiplicity function
----------------------------------------
Author: Facundo Batist
Attributes: []Content: 
On Thu, May 20, 2010 at 5:59 PM, Gustavo Narea <me at gustavonarea.net> wrote:


The best place to post a bug is the bug tracker [0]: you'll surely
receive proper attention there.

Regards,

[0] http://bugs.python.org/

-- 
.    Facundo

Blog: http://www.taniquetil.com.ar/plog/
PyAr: http://www.python.org/ar/



----------------------------------------
Subject:
[Python-Dev] Incorrect length of collections.Counter objects /
	Multiplicity function
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Tue, May 18, 2010 at 11:00 PM, Gustavo Narea <me at gustavonarea.net> wrote:

This is the intended behaviour;  it also agrees with what you get when
you iterate
over a Counter object:

[1, 2, 3]

As I understand it, there are other uses for Counter objects besides
treating them
as multisets;  I think the choices for len() and iter() reflected
those other uses.


Feel free!  The proposal should probably go to python-list or
python-ideas rather
than here, though.

See also this recent thread on python-list, and in particular the messages
from Raymond Hettinger in that thread:

http://mail.python.org/pipermail/python-list/2010-March/thread.html

Mark



----------------------------------------
Subject:
[Python-Dev] Incorrect length of collections.Counter objects /
	Multiplicity function
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Thu, May 20, 2010 at 10:18 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

Sorry, bad thread link.  Try:

http://mail.python.org/pipermail/python-list/2010-March/1238730.html

instead.

