
============================================================================
Subject: [Python-Dev] Branch ancestry hiccup on the Mercurial repo
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Branch ancestry hiccup on the Mercurial repo
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

For the record, due to a bug in the "hg graft" command, a
recent changeset of mine basically merged branch 2.7 into 3.2:

$ hg log -r "ee8d999b6e05 or parents(ee8d999b6e05)" -G 
o    changeset:   81129:ee8d999b6e05
|\   branch:      3.2
| |  parent:      81124:e4ea38a92c4d
| |  parent:      81128:3436769a7964
| |  user:        Antoine Pitrou <solipsis at pitrou.net>
| |  date:        Fri Dec 28 19:07:43 2012 +0100
| |  summary:     Forward port new test for SSLSocket.connect_ex()
| |
| o  changeset:   81128:3436769a7964
| |  branch:      2.7
| |  user:        Antoine Pitrou <solipsis at pitrou.net>
| |  date:        Fri Dec 28 19:03:43 2012 +0100
| |  summary:     Backport Python 3.2 fix for issue #12065, and add
another test for SSLSocket.connect_ex().
| |
o |  changeset:   81124:e4ea38a92c4d
| |  branch:      3.2
| |  parent:      81118:b2cd12690a51
| |  user:        Serhiy Storchaka <storchaka at gmail.com>
| |  date:        Fri Dec 28 09:42:11 2012 +0200
| |  summary:     Issue #16761: Raise TypeError when int() called with
base argument only.
| |


The first symptoms were reported in
http://bz.selenic.com/show_bug.cgi?id=3748 but the actual cause of the
issue is http://bz.selenic.com/show_bug.cgi?id=3667.

Chances are the problem won't be very annoying in practice, but just
FYI.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Branch ancestry hiccup on the Mercurial repo
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Mon, Dec 31, 2012 at 10:47 PM, Antoine Pitrou <solipsis at pitrou.net>wrote:



I dont get this. I see 2.7 as a separate un-merged branch again (
http://hg.python.org/cpython/graph). Just curious, what happened next?



Thanks,
Senthil
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130101/ee049b91/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Branch ancestry hiccup on the Mercurial repo
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 01/01/2013 09:20 AM, Senthil Kumaran wrote:

You can still see the merge near the bottom of the graph on that URL.
Nothing too bad happened, as we usually don't merge 2.7 into 3.x anyway,
it does not matter that Mercurial now considers all changes before that
accident as "merged".

Merry 2013,
Georg


