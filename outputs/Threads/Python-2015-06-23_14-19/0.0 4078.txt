
============================================================================
Subject: [Python-Dev] Ctypes bug fix for Solaris - too late for 2.7.3?
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Ctypes bug fix for Solaris - too late for 2.7.3?
----------------------------------------
Author: Skip Montanar
Attributes: []Content: 
I hit a bug in the ctypes package on Solaris yesterday.
Investigating, I found there is a patch:

http://bugs.python.org/issue5289

I applied it and verified that when run as a main program
ctypes/util.py now works.

Is it too late to get this into 2.7.3?  Given the nature of the patch
(a new block of code only matched on Solaris, and the fact that
find_library is quite broken now on that platform), it's hard to see
how things would be worse after applying it.

BTW, this will be a requirement for getting PyPy running on Solaris.
(That's the context where i encountered it.)

Thanks,

Skip



----------------------------------------
Subject:
[Python-Dev] Ctypes bug fix for Solaris - too late for 2.7.3?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/1/27 Skip Montanaro <skip at pobox.com>:

Yes, it's far too late for 2.7.3, since that was released last April.
:) I think it could go into 2.7.4, though.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Ctypes bug fix for Solaris - too late for 2.7.3?
----------------------------------------
Author: Skip Montanar
Attributes: []Content: 

Whoops, sorry.  I thought I had remembered some recent discussion of
an upcoming 2.7 micro.  Off-by-one error, or just brain freeze? :-)

Skip



----------------------------------------
Subject:
[Python-Dev] Ctypes bug fix for Solaris - too late for 2.7.3?
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 01/27/2013 10:31 AM, Skip Montanaro wrote:

The upcoming micro you recall is 2.7.4.  The 2.7.3 train left the station
already. ;)



Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)
Comment: Using GnuPG with undefined - http://www.enigmail.net/

iEUEARECAAYFAlEFUVcACgkQ+gerLs4ltQ6X3QCgscrJNOQo2mB5ylS97OYJ7EG/
tKYAl3YvSGLNd9NeZ6AKchreRcjNvYc=
=yimz
-----END PGP SIGNATURE-----


