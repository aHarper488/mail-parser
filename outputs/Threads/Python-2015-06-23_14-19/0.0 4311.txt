
============================================================================
Subject: [Python-Dev] A 'common' respository? (was Re: IDLE in the stdlib)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] A 'common' respository? (was Re: IDLE in the stdlib)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/20/2013 8:15 PM, Terry Reedy wrote:


Here is a radical idea I have been toying with: set up a 'common' 
repository to 'factor out' files that are, could be, or should be the 
same across versions. The 'common' files would be declared (especially 
to packagers, when relevant) to be a part of each branch. Each release 
would (somehow - not my department) incorporate the latest version of 
everything in 'common'.

What would go here?

Misc/ACKS: the sensible idea that there should only be one copy of this 
file has been discussed before.

LICENSE: I believe this is the same across current versions and must be 
edited in parallel for all future branches.

xxx: others that I have not thought of.

Doc/tools (sphinx and dependencies): setting this up separately but 
identically for each branch is a bit silly if it could be avoided. The 
sphinx versions should, of course, be the new one that runs on both 
python 2 and 3.

idlelib: already discussed. Having only one IDLE version would partially 
speed up development.

(surely controvesial) tkinter and _tkinter: I think the _tkinter and 
tkinter for each release should work with and be tested with the most 
recent tcl/tk release. Having only one tkinter version might make having 
one version of IDLE even easier.

(probably even more controversial) tcl/tk (or at least the files needed 
to fetch and build - but as long as the sources are on python.org 
anyway, the sources could also be moved here from svn): For IDLE to 
really work the same across versions, it needs to run on the same tcl/tk 
version with the same bugfixes. For example, over a year ago, a French 
professor wrote python-list or idle-sig or maybe both saying that he 
would like to use IDLE in a class in Sept 2012, but there was a bug 
keeping it from working properly with French keyboards. He wanted to 
know if we were likely to fix it. The first answer (provided by Kevin 
Walzer) was that it was a tcl/tk bug that he (Kevin) was working on. The 
fix made it into 8.5.9 a year ago and hence into 3.3 but 2.7.3 or 3.2.3, 
released a month after the fix. So I later told him he could use IDLE, 
but, at least on Windows, only with the then upcoming 3.3. (I don't know 
the tcl/tk version policy for the non-Apple builds.)

I do not know if tcl/tk 8.5.z releases have added many features or are 
primarily bugfixes like our micro releases. If the latter, the case for 
distributing at least the most recent 8.5.z with windows would seem 
pretty strong. I also do not know what 8.6.z adds. But an tcl/tk 
'enhancement' of supporting astral characters might look like a bugfix 
for IDLE. (Running from IDLE, print(astral_char) raises, but I believe 
the same code works in some Linux interpreters.)

yyy: any other external dependencies that we update on all versions.

---
Terry Jan Reedy


