
============================================================================
Subject: [Python-Dev] [Python-checkins] r88601 - peps/trunk/pep-0385.txt
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r88601 - peps/trunk/pep-0385.txt
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Feb 26, 2011 at 5:12 AM, antoine.pitrou
<python-checkins at python.org> wrote:

Feedback from the Mozilla and XEmacs folks at the time of that
discussion suggested that EOL issues *were* a potential issue, and
that having to revert unintentional commits of CRLF line endings was
an annoyingly common problem.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

