
============================================================================
Subject: [Python-Dev] mingw support?
Post Count: 42
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: linux at gabriel-striewe.d
Attributes: []Content: 
Dear list,

I was wondering whether there would ever be support for python to be
build by the mingw compiler suite. I found a few patches in the
internet but there were disagreeing comments on whether they are
functional or not. 

So I would like to ask the list whether there are any technical
reasons that this is so. I did a search on the python site for mingw,
but all I found are explanations how to compile python modules, but
not python itself. 

I know the question is why anybody should want to do so, but I do
think that a project which depends on a non-free compiler is not free
after all.

Regards,

Gabriel 



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On 8/7/2010 3:55 PM, linux at gabriel-striewe.de wrote:
There have certainly been demonstrations that Python can be compiled
with mingw, but as far as I am aware what's  missing is a developer
sufficiently motivated to integrate that build system into the
distributions and maintain it.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
DjangoCon US September 7-9, 2010    http://djangocon.us/
See Python Video!       http://python.mirocommunity.org/
Holden Web LLC                 http://www.holdenweb.com/




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Aug 8, 2010 at 5:55 AM,  <linux at gabriel-striewe.de> wrote:

It's a philosophical question - Python is under a BSD style license,
so the core devs (taken as a group) don't have a fundamental objection
to the idea of closed source software, just a pragmatic view that open
source is simply a better approach most of the time (both as a
developer and as a user).

This used to be more of an issue because MS didn't provide a decent
free compiler for their platform. These days (since the release of
Visual Studio Express), we expect that people willing to use (or
support) a closed OS can cope with also using the free-as-in-beer
closed compiler provided by the vendor of that OS.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 08.08.2010 02:12, schrieb Nick Coghlan:

It's also a technical question. Supporting mingw on an ongoing basis is
fairly difficult, for various reasons. There would be nothing inherently
wrong with supporting mingw - it's just that none of the committers has
found enough time and motivation to work on this (either using one of
the contributed patches, or starting from scratch).

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Nick Coghlan wrote:


The problem with the MS "free" compilers is that it's only a
*temporary* freedom. They have a habit of withdrawing older
versions when newer ones become available. Together with their
other obnoxious habit of changing the C runtime in incompatible
ways with every release, the result is that extensions for
versions of Python older than N years can no longer be compiled
with any legally-available free MS compiler.

If you're talking about pragmatism, I think this situation
causes very pragmatic difficulties.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On 8/7/2010 9:27 PM, Greg Ewing wrote:
+0.5

Using the MS stuff is a pragmatic solution in the first place, since it
allows the software to be released. It's true that there may be problems
down the line. Microsoft's recent apparent reduction of support for
dynamic languages represents a disturbing trend to me, though that is
not directly related to the question raised by this thread.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
DjangoCon US September 7-9, 2010    http://djangocon.us/
See Python Video!       http://python.mirocommunity.org/
Holden Web LLC                 http://www.holdenweb.com/




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 08.08.2010 03:27, schrieb Greg Ewing:

However, this is out of scope for the question at hand. Compiling
extensions with mingw is fairly well supported, and people contribute
small patches when it breaks.

The OP was asking for compilation of Python itself with mingw, for
which this MS policy hasn't caused problems - I do have all the
compilers available to me that I need to do that.

Indeed, MSDN subscribers can still access older compiler versions
for quite some time after Microsoft stopped otherwise distributing
the product.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Jeroen Ruigrok van der Werve
Attributes: []Content: 
-On [20100808 02:23], Nick Coghlan (ncoghlan at gmail.com) wrote:

You don't even need VS Express.

Install Windows SDK 7.1 and the compilers are supplied with it. Less clutter
if you prefer the command line.

-- 
Jeroen Ruigrok van der Werven <asmodai(-at-)in-nomine.org> / asmodai
????? ?????? ??? ?? ??????
http://www.in-nomine.org/ | http://www.rangaku.org/ | GPG: 2EAC625B
Without a soul there can be no understanding between the heart and the mind.



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Aug 9, 2010 at 1:55 AM, Jeroen Ruigrok van der Werven
<asmodai at in-nomine.org> wrote:

MS may have improved the tools they release with the SDK to the point
where they can build a VS solution file (I haven't tried that in
years). Historically (i.e. before the first express version of Visual
Studio), there were a lot of hoops to jump through to get something
that could handle the complete build chain. It was painful enough back
when I first started hacking on the Python core that I gave up and
just switched to developing on Linux instead.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Jeroen Ruigrok van der Werve
Attributes: []Content: 
-On [20100808 18:29], Nick Coghlan (ncoghlan at gmail.com) wrote:

be the case. I have not tried it myself (yet).

-- 
Jeroen Ruigrok van der Werven <asmodai(-at-)in-nomine.org> / asmodai
????? ?????? ??? ?? ??????
http://www.in-nomine.org/ | http://www.rangaku.org/ | GPG: 2EAC625B
You yourself, as much as anybody in the entire universe, deserve your love
and affection...



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/7/2010 3:55 PM, linux at gabriel-striewe.de wrote:

The sociological problem is that currently not enough advocates of using 
a free-as-in-thought compiler both can and will provide enough 
free-as-in-beer volunteer time to deal with the sociological and 
technical problems. That could change someday.

I suspect that the persons who first ported Python to MSDOS simply used 
what they were used to using, perhaps in their paid job. And I am sure 
that is still true of at least some of the people doing Windows support 
today.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Neil Hodgso
Attributes: []Content: 
Terry Reedy:


   Some Windows developers actually prefer Visual Studio, including me.

   MingW has become less attractive in recent years by the difficulty
in downloading and installing a current version and finding out how to
do so. Some projects have moved on to the TDM packaging of MingW.

http://tdm-gcc.tdragon.net/

   Neil



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 



MinGW has become a mess. Equation.com used to have a decent installer, but
at some point they started to ship mingw builds with a Trojan. TDM looks
OK for now.

Building 32-bit Python extension works with MinGW. 64-bit extensions are
not possible due to lacking import libraries (no libmsvcr90.a and
libpython26.a for amd64). It is not possible to build Python with mingw,
only extensions.

I think it is possible to build Python with Microsoft's SDK compiler, as
it has nmake. The latest is Windows 7 SDK for .NET 4, but we need the
version for .NET 3.5 to maintain CRT compatibility with current Python
releases.

Python's distutils do not work with the SDK compiler, only Visual Studio.
Building Python extensions with the SDK compiler is not as easy as it
could (or should) be.

One advantage of mingw for scientific programmers (which a frequent users
of Python) is the gfortran compiler. Although it is not as capable as
Absoft or Intel Fortran, it is still decent and can be used with f2py.
This makes the lack of 64-bit support for Python extensions with mingw
particularly annoying. Microsoft's SDK does not have a Fortran compiler,
and commercial versions are very expensive (though I prefer to pay for
Absoft anyway).

I do not wish for a complete build process for mingw. But support for
64-bit exensions with mingw and distutils support for Microsoft's SDK
compiler would be nice.

Sturla




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Case Vanhorse
Attributes: []Content: 
On Mon, Aug 9, 2010 at 11:47 AM, Sturla Molden <sturla at molden.no> wrote:

Based on hints here:

http://docs.python.org/distutils/apiref.html?highlight=sdk#module-distutils.msvccompiler

I've been able to build GMPY and MPIR using just SDK compiler. For an
example, see

http://code.google.com/p/gmpy/source/browse/trunk/win_x64_sdk_build.txt

I agree that it should be easier but it is possible.

casevh



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

That is not true.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On 8/9/2010 2:47 PM, Sturla Molden wrote:
[...]
At one point Mike Fletcher published a patch to make distutils use the
SDK compiler. It would make a lot of sense if it were built in to
distutils as a further compiler choice.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
DjangoCon US September 7-9, 2010    http://djangocon.us/
See Python Video!       http://python.mirocommunity.org/
Holden Web LLC                 http://www.holdenweb.com/




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 09.08.2010 23:00, schrieb Steve Holden:

Please understand that this very choice is there already.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On 8/9/2010 5:37 PM, "Martin v. L?wis" wrote:
That's great. Is that what the documentation refers to when it says

"""
MSVCCompiler will normally choose the right compiler, linker etc. on its
own. To override this choice, the environment variables
DISTUTILS_USE_SDK and MSSdk must be both set. MSSdk indicates that the
current environment has been setup by the SDK?s SetEnv.Cmd script, or
that the environment variables had been registered when the SDK was
installed; DISTUTILS_USE_SDK indicates that the distutils user has made
an explicit choice to override the compiler selection by MSVCCompiler.
"""

That isn't particularly clear to me, but it may be to others more
familiar with building on Windows.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
DjangoCon US September 7-9, 2010    http://djangocon.us/
See Python Video!       http://python.mirocommunity.org/
Holden Web LLC                 http://www.holdenweb.com/



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?windows-1252?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Correct.


People familiar with that documentation fragment had no problem making
it work. The problem has rather been that people didn't expect that
to be supported, and didn't try to determine whether it was supported.
If you search for "SDK" in the online documentation, this will
be the only piece that comes up.

Not sure when Mike Fletcher wrote his code - if it is recent, he
surely must have run into the feature while reading the existing
code base.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 



Ahh...

"MSSdk must be set" typically means we must use the Windows 7 SDK command
prompt.

Without DISTUTILS_USE_SDK, the build fails:


C:\DEVELOPMENT\test-distutils>setup.py build_ext
running build_ext
building 'test' extension
Traceback (most recent call last):
  File "C:\DEVELOPMENT\test-distutils\setup.py", line 6, in <module>
    ext_modules=[Extension('test', ['test.c'])],
  File "C:\Python26\lib\distutils\core.py", line 152, in setup
    dist.run_commands()
  File "C:\Python26\lib\distutils\dist.py", line 975, in run_commands
    self.run_command(cmd)
  File "C:\Python26\lib\distutils\dist.py", line 995, in run_command
    cmd_obj.run()
  File "C:\Python26\lib\distutils\command\build_ext.py", line 340, in run
    self.build_extensions()
  File "C:\Python26\lib\distutils\command\build_ext.py", line 449, in
build_exte
nsions
    self.build_extension(ext)
  File "C:\Python26\lib\distutils\command\build_ext.py", line 499, in
build_exte
nsion
    depends=ext.depends)
  File "C:\Python26\lib\distutils\msvc9compiler.py", line 449, in compile
    self.initialize()
  File "C:\Python26\lib\distutils\msvc9compiler.py", line 359, in initialize
    vc_env = query_vcvarsall(VERSION, plat_spec)
  File "C:\Python26\lib\distutils\msvc9compiler.py", line 275, in
query_vcvarsal
l
    raise ValueError(str(list(result.keys())))
ValueError: [u'path', u'include', u'lib']



Now let's do what the documentations says:

C:\DEVELOPMENT\test-distutils>set DISTUTILS_USE_SDK=1

C:\DEVELOPMENT\test-distutils>setup.py build_ext
running build_ext
building 'test' extension
creating build
creating build\temp.win-amd64-2.6
creating build\temp.win-amd64-2.6\Release
C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\Bin\amd64\cl.exe /c
/nolog
o /Ox /MD /W3 /GS- /DNDEBUG -IC:\Python26\include -IC:\Python26\PC
/Tctest.c /Fo
build\temp.win-amd64-2.6\Release\test.obj
test.c
creating build\lib.win-amd64-2.6
C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\Bin\amd64\link.exe
/DLL /n
ologo /INCREMENTAL:NO /LIBPATH:C:\Python26\libs
/LIBPATH:C:\Python26\PCbuild\amd
64 /EXPORT:inittest build\temp.win-amd64-2.6\Release\test.obj
/OUT:build\lib.win
-amd64-2.6\test.pyd /IMPLIB:build\temp.win-amd64-2.6\Release\test.lib
/MANIFESTF
ILE:build\temp.win-amd64-2.6\Release\test.pyd.manifest
test.obj : warning LNK4197: export 'inittest' specified multiple times;
using fi
rst specification
   Creating library build\temp.win-amd64-2.6\Release\test.lib and object
build\t
emp.win-amd64-2.6\Release\test.exp
C:\Program Files\Microsoft SDKs\Windows\v7.0\Bin\x64\mt.exe -nologo
-manifest bu
ild\temp.win-amd64-2.6\Release\test.pyd.manifest
-outputresource:build\lib.win-a
md64-2.6\test.pyd;2


:-D


Sturla




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 


Yes you are right. I did not know about DISTUTILS_USE_SDK.

Sturla




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/9/2010 2:47 PM, Sturla Molden wrote:

Someone else deserves credit for writing that and giving that link ;-)

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: linux at gabriel-striewe.d
Attributes: []Content: 
On Mon, Aug 09, 2010 at 06:55:29PM -0400, Terry Reedy wrote:

Yes, that was a great link, thanks. It works fine for me.

The reason I was bringing up this topic again was that I think the gnu
autotools have been made for exactly this purpose, to port software to
different platforms, and it might in the long run be easier to have a
working mingw plus autotools platform to develop python (as well as
other software) on.

Besides, one day it would be nice even on windows to have a kind of
GNU/Windows system where you could just type win-emerge gtk or
win-emerge python or whatever. 

Gabriel 



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: David Cournapea
Attributes: []Content: 
On Tue, Aug 10, 2010 at 11:06 PM,  <linux at gabriel-striewe.de> wrote:

Autotools only help for posix-like platforms. They are certainly a big
hindrance on windows platform in general,

cheers,

David



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 

"David Cournapeau":

That is why mingw has MSYS.

mingw is not just a gcc port, but also a miniature gnu environment for
windows. MSYS' bash shell allows us to do things like:

$ ./configure
$ make && make install



Sturla




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: linux at gabriel-striewe.d
Attributes: []Content: 
On Wed, Aug 11, 2010 at 03:21:15PM +0200, Sturla Molden wrote:

I will try to compile the python-svn trunk then by using the
autotools plus tdm-gcc (a mingw fork) as well as MSYS. I might try to
report any errors back to the autotools list and sort out with them
what is happening.

Gabriel



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: David Cournapea
Attributes: []Content: 
On Wed, Aug 11, 2010 at 10:21 PM, Sturla Molden <sturla at molden.no> wrote:

I know of MSYS, but it is not very pleasant to use, if only because it
is extremely slow. When I need to build things for windows, I much
prefer cross compiling to using MSYS. I also think that cross
compilation is more useful than native mingw build alone - there are
patches for cross compilation, but I don't know their current status,

cheers,

David



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: linux at gabriel-striewe.d
Attributes: []Content: 
On Thu, Aug 12, 2010 at 05:38:52PM +0900, David Cournapeau wrote:

My argument goes that one of the biggest differences between the
GNU/Linux and the Windows way of computing is the barrier between user
and programmer. In the Windows way, you are either a user or a
programmer. On Linux, just by the way you can download software and
run ./configure &&  make && make install, you are encouraged to look at the
source code and by this you might in the not-so-long run start
reporting bugs to mailing lists and see that there are actually people
who might be able to sort out the bugs and that you might become one
of them.

The Windows way, you think those bugs are unavoidable and start making
jokes out of a feeling of frustration and helplessness.

That's where msys/mingw is supposed to come in, if only it was easier
to install, so that new Programmers don't get locked in in the
Microsoft compiler products and thereby the divide between the
software communities gets wider and wider. 

Don't get me wrong, I think the python project is doing a great job in
terms of cross-platform portability, but things would be easier if
there was an easy combination of msys, mingw and autotools.

And by the way, I think the way the big linux distros like fedora and
mandrake distribute software is more similar to the windows way of
computing. 

Gabriel 





----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Cesare Di Maur
Attributes: []Content: 
2010/8/12  <linux at gabriel-striewe.de>

Anyway Visual Studio, even with the Express Edition, is simpler and more
productive for a Windows programmer.

You must suggest at least an equivalent "free" alternative to make the
switch convenient.

Otherwise we are talking about philosophy or religion, and nobody will
change his ideas.

Cesare
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100812/4b574406/attachment.html>



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Such arguments are off-topic for python-dev; please use one of the
Linux advocacy forums.

A little bit on-topic, I think that Python actually helps to lower
the barrier between users and programmers on Windows.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Cesare Di Mauro wrote:


I think the point is that *because* people don't want to change
their ideas, it would be good to have a mingw-based alternative.
Otherwise everyone is forced to convert to the Windows religion.

-- 
Greg




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 13/08/2010 00:44, Greg Ewing wrote:
You mean people using Windows are forced to use Windows?

Michael

-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Lisandro Dalci
Attributes: []Content: 
On 12 August 2010 20:37, Michael Foord <fuzzyman at voidspace.org.uk> wrote:

I would say people that want to support Windows but are not using it.


-- 
Lisandro Dalcin
---------------
CIMEC (INTEC/CONICET-UNL)
Predio CONICET-Santa Fe
Colectora RN 168 Km 472, Paraje El Pozo
Tel: +54-342-4511594 (ext 1011)
Tel/Fax: +54-342-4511169



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Michael Foord writes:

 > > I think the point is that *because* people don't want to change
 > > their ideas, it would be good to have a mingw-based alternative.
 > > Otherwise everyone is forced to convert to the Windows religion.

 > You mean people using Windows are forced to use Windows?

By far not all, but some are.  Like me, I only use Windows when my
boss holds up a pink paper and says "shall I sign this?"  So far I
haven't felt a need to build Python on Windows, but if I did I would
strongly prefer to use a familiar (to me) build system based on the
autotools and GCC.

Because I haven't felt that need, I don't think MSYS/mingw support is
a big deal.  But apparently there are a few people whose Windows use
case is characterized both by platform coercion and a desire to build
Python themselves.

The question is "who will support those folks?"  I don't see any
reason why you or Martin should support MSYS/mingw if you don't want
to, but please don't put down the folks who ask for it.  Just say "no,
it's not worth it".  Or maybe, "if you want to do the work, I might
contribute some reviews."  Or whatever.



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Cesare Di Maur
Attributes: []Content: 
2010/8/13 Greg Ewing <greg.ewing at canterbury.ac.nz>


I think the point is that *because* people don't want to change


I like to use Windows because it's a comfortable and productive environment,
certainly not because someone forced me to use it.

Also, I have limited time, so I want to spend it the better I can, focusing
on solving real problems. Setup, Next, Next, Finish, and I want it working
without thinking about anything else.

It's a philosophy similar to Python: you don't need to know if the platform
where it's running is 32 or 64 bits, little or big endian, the operating
system, and so on. Just launch it and start typing code: it'll work.

It can be also a matter of taste. I like graphical environments since the
old Amiga days. If I need a shell, I greatly prefer Python.

Anyway, for Windows there's cygwin too, and Python works. But after some
months I replaced it with native Windows tools (with VisualStudio on top): I
work much, much better this way.

If someone is interested in a mingw port, he should consider about having
decent alternatives to what a Windows user can found on his platform,
otherwise it'll be just pure exercise or a faith matter, since nobody will
use it concretely on a daily work.

Give users a better choice, and I don't see logical reasons because they'll
not change their mind.

My 2 cents.

Cesare
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100813/59b1d28c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

The problem really is that when people ask for MingW support, they mean
all kinds of things, and they "can't agree" among themselves what that
is. Some want cross-compiling (i.e. compile using mingw on Linux). Some
want autoconf for mingw with msys. Some want autoconf for mingw with
cygwin. Some want to replace the build system entirely, and have the
new build system support mingw (and claim that you otherwise can't get
"good" mingw support).

It's not that I'm objecting mingw support per se, but have my issues
with each individual patch proposed so far. As for reviewing: people
proposing mingw patches somehow always arrive at very large patches.
Reviewing them is very difficult also.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 


Usually it means they want to build C or C++ extensions, don't have Visual
Studio, don't know about the SDK compiler, and have misunderstood the CRT
problem.

As long at Python builds with the free Windows 7 SDK, I think it is
sufficient to that mingw is supported for extensions (and the only reasons
for selecing mingw over Microsoft C/C++ on windows are Fortran and C99 --
the Windows SDK compiler is a free download as well.)

Enthought (32-bit) ships with a mingw gcc compiler configured to build
extensions. That might be something to consider for Python on Windows. It
will prevent accidental linking with wrong libraries (particularly the
CRT).

Sturla








----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 
"Cesare Di Mauro":

[...]


I use Windows too, even though I am a scientist and most people seem to
prefer Linux for scientific computing. I do like to just click on the
installer from Enthought and forget about building all the binaries an
libraries myself. Maybe I am just lazy...

But likewise, I think that most Windows users don't care which C compiler
was used to build Python. Nor do we/they care which compiler was used to
build any other third-party software, as long as the MSI installers works
and the binaries are void of malware.

Also note that there are non-standard things on Windows that mingw does
not support properly, such as COM and structured exceptions. Extensions
like pywin32 depend on Microsoft C/C++ for that reason.

So for Windows I think it is sufficient to support mingw for extension
libraries. The annoying part is the CRT DLL hell, which is the fault of
Microsoft. An easy fix would be a Python/mingw bundle, or a correctly
configured mingw compiler from python.org. Or Python devs could consider
not using Microsoft's CRT at all on Windows, and replacing it with a
custom CRT or plain Windows API calls.

Sturla












----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 13.08.2010 20:45, schrieb Sturla Molden:

True. However, that should be working just fine, for many years. It just
becomes a hassle every time we switch VS versions, and mingw fails to
support the new CRT version. There is really nothing we can do about
that, other than asking people to complain to the mingw developers.


People keep disagreeing with that judgement, and keep contributing
patches that supposed make Python itself build with mingw.


Hmm. Including a gcc seems like a lot of overhead, not at least for the
need to provide sources as well.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Sturla Molde
Attributes: []Content: 


A lighter solution would be to include an importlib for the correct CRT as
well as for Python, and make sure distutils link with both for mingw. For
example we cannot use mingw for 64 bit extensions to Python 2.6 because
libpython26.a is missing from Python and mingw-w64 don't have
libmsvcr90.a. If Python shipped with both, there would not be a missing
import library for the CRT and no confusion as to which CRT to link.

For Python 2.5 there also was a CRT licensing issue for py2exe, but that
disappaired when MS provided download links for the Visual Studio
redistributables. I think this also contributed to a motivation for a
plain mingw build of Python 2.5, as it would take the CRT licensing issue
away from py2exe. But as of Python 2.6 this is not a problem anymore. Now
it is sufficient to direct the user to Microsoft's free CRT download.
Everybody might not be aware of that.

Sturla




----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: Zooko O'Whielacron
Attributes: []Content: 
On Sat, Aug 7, 2010 at 2:14 PM, Steve Holden <steve at holdenweb.com> wrote:

It looks like quite a lot of activity on
http://bugs.python.org/issue3871 . I find it surprising that nobody
mentioned it before on this thread. Perhaps nobody who has been
posting to this thread was aware of this activity.

Regards,

Zooko



----------------------------------------
Subject:
[Python-Dev] mingw support?
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 14.08.2010 08:35, schrieb Zooko O'Whielacronx:

Or one of these:

http://bugs.python.org/issue1412448
http://bugs.python.org/issue1597850
http://bugs.python.org/issue3754
http://bugs.python.org/issue5026
http://bugs.python.org/issue6335

Because there is so many of them, all different, I didn't want to pick
out any specific. Notice that #3871 is really two patch: the original
one (from Roumen), and the one by ??????, who basically hijacked the issue.

Regards,
Martin

