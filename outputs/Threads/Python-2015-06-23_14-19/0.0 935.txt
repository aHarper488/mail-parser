
============================================================================
Subject: [Python-Dev] issue 9807 - abiflags in paths and symlinks (updated
	patch)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] issue 9807 - abiflags in paths and symlinks (updated
	patch)
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
I finally found a chance to address all the outstanding technical issues
mentioned in bug 9807:

    http://bugs.python.org/issue9807

I've uploaded a new patch which contains the rest of the changes I'm
proposing.  I think we still need consensus about whether these changes are
good to commit.  With 3.2b1 coming soon, now's the time to do that.

If there are any remaining concerns about the details of the patch, please add
them to the tracker issue.  If you have any remaining objections to the
change, please let me know or follow up here.

Cheers,
-Barry

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101110/97da93a1/attachment.pgp>

