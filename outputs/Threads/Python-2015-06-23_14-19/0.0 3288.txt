
============================================================================
Subject: [Python-Dev] Restricted API versioning
Post Count: 16
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
I've been thinking about extensions to the stable ABI. On the one hand,
introducing new API can cause extension modules not to run on older
Python versions. On the other hand, the new API may well be stable in
itself, i.e. remain available for all coming 3.x versions.

As a compromise, I propose that such API can be added, but extension
authors must explicitly opt into using it. To define their desired
target Python versions, they need to set Py_LIMITED_API to the
hexversion of the first Python release they want to support.

Objections?

The first use case of this are some glitches in the heap type API
that Robin Schreiber detected in his GSoC project. E.g. specifying
a heap type whose base also is a heap type was not really possible:
the type spec would have to contain a pointer to the base, but that
is not constant. In addition, if we use multiple interpreters, the
base type should be a different object depending on the current
interpreter - something that PyType_FromSpec couldn't support at all.
So there is a new API function PyType_FromSpecWithBases which covers
this case, and this API will only be available in 3.3+.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sat, Jun 23, 2012 at 5:31 PM, "Martin v. L?wis" <martin at v.loewis.de>wrote:


Nope, it sounds like a good idea to allow for the ABI to slowly grow.

-Brett


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120623/060af72c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Gregory P. Smit
Attributes: []Content: 
On Sat, Jun 23, 2012 at 2:31 PM, "Martin v. L?wis" <martin at v.loewis.de>wrote:


+1 This sounds reasonable to me.  Many other libraries have used this
approach in the past.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120623/f437c4e8/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 23 Jun 2012 23:31:07 +0200
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Perhaps something more user-friendly than the hexversion?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 23.06.2012 23:41, schrieb Antoine Pitrou:

IMHO 0x03030000 for 3.0.0 is user-friendly enough. A macro like
PY_VERSION(3, 0, 0) could be added, too.

Christian



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On 23.06.2012 23:41, Antoine Pitrou wrote:

Please propose something. I think the hexversion *is* user-friendly,
since it allows easy comparisons (Py_LIMITED_API+0 >= 0x03030000).
Users that run into missing symbols will, after inspection of the
header file, easily know what to do.

We could require a second macro, but users will already have to define
Py_LIMITED_API, so not making them define a second macro is also more
friendly.

Plus, with the hexversion, we can add stuff to a bugfix release, such
a annoying omissions (e.g. the omission of the _SizeT functions, which
I missed since I didn't compile the headers with PY_SSIZE_T_CLEAN when
generating the function list).

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 
On 06/23/2012 03:08 PM, "Martin v. L?wis" wrote:

+1 to the idea, and specifically to using hexversion here.  (Though what 
will we do after Python 255.0?)


//arry/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120623/f7cb2663/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 24.06.2012 01:11, schrieb Larry Hastings:

+1 for the general idea and for using Py_LIMITED_API. I still like my
idea of a simple macro based on Include/patchlevel.h, for example:

#define Py_API_VERSION(major, minor, micro) \
   (((major) << 24) | ((minor) << 16) | ((micro) << 8))

#if Py_LIMITED_API+0 >= Py_API_VERSION(3, 3, 0)
#endif



Luckily it's gonna take another 1500 years, or so. Our progenies could
rename Python to Circus ...



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Sun, Jun 24, 2012 at 9:40 AM, Christian Heimes <lists at cheimes.de> wrote:

This strikes me as in opposition to the Python-level policy of duck
typing. Would it be more appropriate to, instead of asking if it's
Python 3.3.0, ask if it's a Python that supports PY_FEATURE_FOOBAR? Or
would that result in an unnecessary proliferation of flag macros?

ChrisA



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 

On 06/23/2012 04:44 PM, Chris Angelico wrote:

python != c

Or, if you prefer

python is not c


C lacks niceties like constructors, destructors, and default arguments.  
I think C APIs need to be much more precise than Python APIs; 
mix-n-match C APIs would be an invitation to heartburn and migranes.


//arry/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120623/1d755cfd/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 24.06.2012 01:44, schrieb Chris Angelico:

The version number is a sufficient rule. Flags aren't necessary as we
can never remove or alter the signature of a API function. We can only
add new features. Otherwise we'd break the API and binary interface
(ABI) for C extensions.

C compilers, linkers, dynamic library loaders and calling conventions
are limited and don't support fancy stuff like OOP.

Christian



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Jun 24, 2012 at 9:40 AM, Christian Heimes <lists at cheimes.de> wrote:

+1 to all 3 of those from me (the general idea, using hexversion, and
providing a convenience macro to skip having to spell out hexversion
manually).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

It would, hence I'm -1. I believe it is the motivation for the gcc
assertion preprocessor feature, which never caught on.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Jun 24, 2012 at 5:00 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Right, if someone wants to check for a specific feature rather than
just figuring out once the minimum version of the stable ABI that they
need, then they can write an autotools macro (or equivalent in other
build systems).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Sun, Jun 24, 2012 at 5:08 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Fair enough. I assume these sorts of things are only ever going to be
added once, and not backported to old versions, so a single version
number is guaranteed to suffice (it's not like "available in 4.5.6 and
4.6.2 and 4.7.4"). Go with the easy option!

ChrisA



----------------------------------------
Subject:
[Python-Dev] Restricted API versioning
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jun 24, 2012, at 12:08 AM, Martin v. L?wis wrote:


+1 for hexversion for the reasons Martin states.

-Barry

