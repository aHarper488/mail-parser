
============================================================================
Subject: [Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Glyph Lefkowit
Attributes: []Content: 

On Apr 2, 2012, at 10:39 AM, Kristj?n Valur J?nsson wrote:


"No steps" means something very specific when referring to time APIs.  As I recently explained here: <http://article.gmane.org/gmane.comp.python.devel/131487/>.

-glyph


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120402/64d9879e/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

Windows provides two main monotonic clocks: QueryPerformanceCounter()
and GetTickCount(). QueryPerformanceCounter() has a good accuracy (0.3
ns - 5 ns) but has various issues and is know to not have a steady
rate. GetTickCount() has a worse accuracy (1 ms - 15 ms) but is more
stable and behave better on system suspend/resume.

The glib library prefers GetTickCount() over QueryPerformanceCounter()
for its g_get_monotonic_time() because "The QPC timer has too many
issues to be used as is."
Ihttp://mail.gnome.org/archives/commits-list/2011-November/msg04589.html

The Qt library tries QueryPerformanceCounter() but may fallback to
GetTickCount() if it is not available.

python-monotonic-time only uses GetTickCount() or GetTickCount64().

It is important to decide which clock is used for the Python
time.monotonic() because it may change the design of the PEP 418. If
we use GetTickCount() for time.monotonic(), we should use
QueryPerformanceCounter() for time.highres(). But in this case, it
means that time.highres() is not a simple "try monotonic or falls back
to system time", but may use a different clock with an higher
resolution. So we might add a third function for the "try monotonic or
falls back to system time" requirement.

Python implements time.clock() using QueryPerformanceCounter() on Windows.

Victor



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Mar 30, 2012 at 2:43 PM, Cameron Simpson <cs at zip.com.au> wrote:

I like this out-of-the-box thinking. But I'm still wondering if there
really are enough flags for this to be worth it. If there are, great,
the API is pretty. But if there are only 2 or 3 flag combinations that
make actual sense, let's forget it.

Another out-of-the-box idea, going back to simplicity: have just one
new function, time.hrtimer(), which is implemented using the
highest-resolution timer available on the platform, but with no strong
guarantees. It *may* jump, move back, drift, change its rate, or roll
over occasionally. We try to use the implementation that's got the
fewest problems, but we don't try to hide its deficiencies, and
nothing suitable exists, it may be equivalent to time.time(). If the
times you measure are too weird, measure again. For scheduling things
a day or more in the future, you should use time.time() instead.

One issue that hasn't had enough attention: *scope* of a timer. If two
processes running on the same machine ask for the time, do the values
they see use the same epoch, or is the epoch dependent on the process?
Some code I saw in timemodule.c for working around Windows clocks
rolling over seem to imply that two processes may not always see the
same timer value. Is there a use case where this matters?

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Mar 30, 2012 at 4:44 PM, Cameron Simpson <cs at zip.com.au> wrote:
[Lots of good stuff]

Maybe you and Victor should try to merge your proposals off-line and
then get back with a new proposal here.


Ah, but if a clock drifts, the epoch may too -- and we may never know
it. I like knowing all sorts of things about a clock, but I'm not sure
that for clocks other than time.time() I'd ever want to know the epoch
-- ISTM that the only thing I could do with that information would be
shooting myself in the foot. If you really want the epoch, compute it
yourself by bracketing a timer() call in two time() calls, or vice
versa (not sure which is better :-).

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Glyp
Attributes: []Content: 

On Mar 30, 2012, at 8:51 PM, Victor Stinner wrote:


QPC is not even necessarily steady for a short duration, due to BIOS bugs, unless the code running your timer is bound to a single CPU core.  <http://msdn.microsoft.com/en-us/library/ms644904> mentions SetThreadAffinityMask for this reason, despite the fact that it is usually steady for longer than that.

-glyph

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120330/fbb5318b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

These features look to be exclusive on Windows. On other platforms, it
looks like monotonic clocks are always the most accurate clocks. So I
don't think that we need to be able to combine these two "flags".


You mean "not adjusted by NTP"? Except CLOCK_MONOTONIC on Linux, no
monotonic clock is adjusted by NTP. On Linux, there is
CLOCK_MONOTONIC_RAW, but it is only available on recent Linux kernel
(2.6.28).

Do you think that it is important to be able to refuse a monotonic
clock adjusted by NTP? What would be the use case of such truly steady
clock?

--

The PEP 418 tries to expose various monotonic clocks in Python with a
simple API. If we fail to agree how to expose these clocks, e.g.
because there are major differences between these clocks, another
solution is to only expose low-level function. This is already what we
do with the os module, and there is the shutil module (and others) for
a higher level API. I already added new "low-level" functions to the
time module: time.clock_gettime() and time.clock_getres().


The purpose of such function is to fix programs written with Python <
3.3 and using time.time() whereas a monotonic would be the right clock
(e.g. implement a timeout).

Another concern is to write portable code. Python should help
developers to write portable code, and time.monotonic(fallback=False)
is not always available (and may fail).

Victor



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Mar 30, 2012 at 6:24 PM, Victor Stinner
<victor.stinner at gmail.com> wrote:

That depends on what NTP can do to the clock. If NTP makes the clock
tick *slightly* faster or slower in order to gradually adjust the wall
clock, that's fine. If NTP can make it jump wildly forward or even
backward, it's no better than time.time(), and we know why (for some
purposes) we don't want that.


-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Glyp
Attributes: []Content: 
On Mar 30, 2012, at 9:32 PM, Guido van Rossum wrote:


"no steps" means something very specific.  It does not mean "not adjusted by NTP".

In NTP, changing the clock frequency to be slightly faster or slower is called "slewing" (which is done with adjtime()).  Jumping by a large amount in a single discrete step is called "stepping" (which is done with settimeofday()).  This is sort-of explained by <http://doc.ntp.org/4.1.2/ntpd.htm>.

I think I'm agreeing with Guido here when I say that, personally, my understanding is that slewing is generally desirable (i.e. we should use CLOCK_MONOTONIC, not CLOCK_MONOTONIC_RAW) if one wishes to measure "real" time (and not a time-like object like CPU cycles).  This is because the clock on the other end of the NTP connection from you is probably better at keeping time: hopefully that thirty five thousand dollars of Cesium timekeeping goodness is doing something better than your PC's $3 quartz crystal, after all.

So, slew tends to correct for minor defects in your local timekeeping mechanism, and will compensate for its tendency to go too fast or too slow.  By contrast, stepping only happens if your local clock is just set incorrectly and the re-sync delta has more to do with administrative error or failed batteries than differences in timekeeping accuracy.

-glyph

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120330/59f7bc1b/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Glyp
Attributes: []Content: 

On Mar 30, 2012, at 10:17 PM, Victor Stinner wrote:


My understanding is:

CLOCK_REALTIME is both stepped and slewed.

CLOCK_MONOTONIC is slewed, but not stepped.

CLOCK_MONOTONIC_RAW is neither slewed nor stepped.

-glyph




----------------------------------------
Subject:
[Python-Dev] Use QueryPerformanceCounter() for time.monotonic()
	and/or time.highres()?
----------------------------------------
Author: Glyp
Attributes: []Content: 

On Mar 30, 2012, at 10:25 PM, Glyph wrote:


Sorry, I realize I should cite my source.

This mailing list post talks about all three together: <http://www.spinics.net/lists/linux-man/msg00973.html>

Although the documentation one can find by searching around the web is really bad.  It looks like many of these time features were introduced, to Linux at least, with no documentation.

-glyph

