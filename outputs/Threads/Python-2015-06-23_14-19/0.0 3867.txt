
============================================================================
Subject: [Python-Dev] Generator finalization and reference cycles
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Generator finalization and reference cycles
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

I have proposed a new generator finalization scheme in
http://bugs.python.org/issue17807: it removes the tp_del from
generator objects, instead electing to cleanup the generator
(equivalent of calling close()) in the frame's tp_clean function. This
way, generators with a try / finally block can be finalized even when
they are part of reference cycles.

This has stemmed from http://bugs.python.org/issue17468, which mentions
a memory leak in Django due to uncollectable generators.

Feedback welcome.

Regards

Antoine.



