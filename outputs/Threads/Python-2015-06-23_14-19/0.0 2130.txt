
============================================================================
Subject: [Python-Dev] Generating patch files
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-15?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
I think I figured out how to generate a single patch for
a clone that has all its changes on the default branch,
comparing it with cpython's default branch. The command to generate
the patch is

hg diff -r'max(p1(min(outgoing())) or p2(max(merge() and 
branch(default))))' -r default

If it's a branch different from default, replace 'default' in both 
places with the branch name.

I'd be curious for people to try this out and report whether it works 
correctly. You'll need a Mercurial release supporting revsets.

Here is the theory of operation: The diff needs to be either from
the oldest of the commits (in case no merges have been made from 
cpython), or from cpython's code in that was last merged. So:

- merge() gives all changesets that are merges
- branch(default) gives all changesets on the default branch
- 'merge() and branch(default)' gives all merges to the default
   branch
- max(merge() and branch(default)) gives the most recent merge
   to the default branch
- p2(max(merge() and branch(default))) is the second parent,
   which will be the upstream version of cpython that got last
   merged
- outgoing() gives all changes in the local repository not
   in cpython
- min(outgoing()) gives the first local change made
- p1(min(outgoing())) gives revision that was the starting
   point for the local modifications
- max(p1(...) or p2(...)) determines which of the two is
   later.

Having the revision of cpython to compare against is the
difficult part; the other revision must be default's head,
which is always called 'default'.

Enjoy,
Martin



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Mar 16, 2011 at 1:49 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

I get "unknown revision" (listing the full expression text) when using
Mercurial 1.6.3 (default version in Ubuntu 10.10).

That version apparently supports revsets, but I'm not sure when the
runtime evaluation of the command line arguments was added.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Apparently shortly after Debian/Ubuntu included the release; I think
I had the same issue with 1.6.4.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 16.03.11 15:20, schrieb "Martin v. L?wis":

Before you upgrade: give me some time to come up with a script that
uses Mercurial API instead to compute the revset and then invoke the
diff command.

We must get something out of using a Python-based DVCS...

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <4D810F84.5060705 at v.loewis.de>,
 "Martin v. L?wis" <martin at v.loewis.de> wrote:

Note the Mercurial project warns that use of the Mercurial API 'is a 
strong indication that you're creating a "derived work" subject to the 
GPL.'

http://mercurial.selenic.com/wiki/MercurialApi
http://mercurial.selenic.com/wiki/License

Would distributing a script that called the API in any way taint Python?

-- 
 Ned Deily,
 nad at acm.org




----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Perhaps. So I'll see whether I can make use of the command line only, first.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
On Thu, Mar 17, 2011 at 5:00 AM, Ned Deily <nad at acm.org> wrote:


Yes, if used to build or run Python.

No, if used to develop Python.



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 16.03.11 15:29, schrieb "Martin v. L?wis":

It turns out that 1.6 doesn't have the min() revset function, so I give 
up on that - the recipe can only work in 1.7 and later.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Wed, 16 Mar 2011 20:47:10 -0400, =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?= <martin at v.loewis.de> wrote:

As long as we aren't distributing mercurial itself, I don't see
how a script could be a problem.  But, then, IANAL.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 16/03/11 20:29, "Martin v. L?wis" wrote:

Beware, mercurial API is not stable. That is, it can change anytime.
Paradoxically, the stable "API" is the commandline, IIRC.

- -- 
Jesus Cea Avion                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iQCVAwUBTYGKsZlgi5GaxT1NAQLEegP9GlKeCi5FRICOXUe2gjQbEWQ3lI44V3ae
+XjGf1FqS5kWgGLM+DlpHZuOB8pQo80ZZFpxOY3MzVi/SCBm8i+SfjGDdS9QQS7X
Y6i7duZ7ubbHls6hnN9weHB6MXBCA+GQQQg8oMSnGiUdXLLBkUzhY+/pXb688wpE
dxFneCjN6W8=
=Xpds
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: Baptiste Carvell
Attributes: []Content: 
Le 16/03/2011 18:49, "Martin v. L?wis" a ?crit :


how about: 'max( ancestors(default) and not outgoing() )' ?




----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 17.03.11 07:30, schrieb Baptiste Carvello:

That fails if there have been later merges with default.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 17.03.11 07:30, schrieb Baptiste Carvello:

I take back what I just said: it looks good.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Generating patch files
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Based on Baptiste's approach, I propose the script below to compute a 
patch. Please report whether it works for you.

Regards,
Martin

#!/bin/sh
base=`hg log --template {rev} -r'max(ancestors(default)-outgoing())'`
hg diff -r$base


