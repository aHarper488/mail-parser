
============================================================================
Subject: [Python-Dev] On breaking modules into packages
 Was:	[issue10199] Move Demo/turtle under Lib/
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages
 Was:	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Hrvoje Niksi
Attributes: []Content: 
On 11/03/2010 01:47 AM, Ben Finney wrote:

I understand this reasoning, but I'd like to offer counter-examples. 
For instance, would you say that glob.glob0 and glob.glob1 are public 
API?  They're undocumented, they're not in __all__, but they don't have 
a leading underscore either, and source comments call them "helper 
functions."  I'm sure there is a lot of other examples like that, both 
in the standard library and in python packages out there.

Other than the existing practice, there is the matter of esthetics. 
Accepting underscore-less identifiers as automatically public leads to a 
proliferation of identifiers with leading underscores, which many people 
(myself included) plainly don't like.



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages
 Was:	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 26/10/2010 15:05, R. David Murray wrote:
Really, you prefer to work in single multi-thousand line modules than 
cleanly split smaller packages? (Is that how you develop your own 
projects, out of interest?)

The cross imports are unfortunate (there are very few actually circular 
dependencies in the package though I think), but more of an indictment 
on the design of unittest than the decision to split it into a package.

How the splitting happened is that I proposed it on this list many 
months ago and got one reply in favour and no dissenters. Benjamin 
actually did the splitting.

I find unittest *massively* easier to maintain and navigate now that it 
is a package. It doesn't take very long to work out where the classes 
live (no time at all if you look in the module names - where the classes 
live is *very* obvious except for the TextTestResult which lives 
alongside the TestRunner). The package split into modules is into 
conceptual units, where the responsibility of the units is clear. This 
makes the code not only easier to navigate but easier to *think about*. 
The structure of unittest is now much clearer.

I find the big-ball-of-mud style development, where everything lives 
inside huge monolithic modules, very painful. I also think that it is an 
extremely bad example for new developers. There is something to be said 
for consistency within the standard library, but I don't think it is a 
price worth paying in this particular case.

All the best,

Michael Foord



-- 
http://www.voidspace.org.uk/


