
============================================================================
Subject: [Python-Dev] None as slice params to list.index() and tuple.index()
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] None as slice params to list.index() and tuple.index()
----------------------------------------
Author: Petri Lehtine
Attributes: []Content: 
list.index() and list.tuple() don't currently accept None as slice
parameters, as reported in http://bugs.python.org/issue13340. For
example:

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: slice indices must be integers or None or have an __index__ method

The error message of list.index() and tuple.index() (as a consequence
of using _PyEval_SliceIndex() for parsing arguments) indicates that
None is a valid value.

Currently, find(), rfind(), index(), rindex(), count(), startswith()
and endswith() of str, bytes and bytearray accept None. Should
list.index() and tuple.index() accept it, too?

I'm bringing this up because I already committed a fix that enables
the None arguments, but Raymond pointed out that maybe they shouldn't
accept None at all. I also committed the fix to 3.2 and 2.7 based on
discussion in http://bugs.python.org/issue11828#msg133532, in which
Raymond says that for string functions, this can be considered a bug
because the exception's error message makes a promise that None is
accepted.

Petri

