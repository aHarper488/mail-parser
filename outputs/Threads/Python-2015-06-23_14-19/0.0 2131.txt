
============================================================================
Subject: [Python-Dev] Please retract my committer rights
Post Count: 15
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Thomas Helle
Attributes: []Content: 
I would like my committer rights to be retracted.

I have been contributing to Python here and there for 10 years now,
and it was a pleasant experience.

Unfortunately, since about a year I have lots more things to do, and
I won't be able to contribute anymore (I have not even started to
learn mercurial yet).  Plus I'm still using 2.6 or even 2.5 with my own
Python projects.

Goodbye.

Thanks,
Thomas




----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Mar 16, 2011, at 12:36 PM, Thomas Heller wrote:


You will be missed.
Thanks for all your efforts.


Raymond



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Wed, Mar 16, 2011 at 3:44 PM, Raymond Hettinger
<raymond.hettinger at gmail.com> wrote:
..

+1

In case anyone did not recognize Thomas by name, he contributed the
ctypes package, which in my opinion was a hugely successful addition.

Thanks for all your hard work!



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/3/16 Thomas Heller <theller at ctypes.org>:

Thank you very much for you work. We'd always be happy to have you back.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/17/2011 1:45 PM, Benjamin Peterson wrote:

In the meanwhile, it would be nice to have another ctypes maintainer, as 
there are several open issues. There is certainly a opening for a new 
person with C experience.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Thu, Mar 17, 2011 at 7:28 PM, Terry Reedy <tjreedy at udel.edu> wrote:

I'll ask doug hellmann if this is something we can do a blog post
about on the PSF blog. Having another ctypes expert is pretty critical
(everyone I know is using it)



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Doug Hellman
Attributes: []Content: 

On Mar 18, 2011, at 9:59 AM, Jesse Noller wrote:


This does seem like a good use of the blog. The team is working on setting up the dev blog, and I expect to have something up soon. This can be our second post there (the first being an introduction).

Is there a "job description" for a ctypes maintainer? Where should we send people for more details about this request? We would link to the dev guide and this mailing list, but is there something more specific?

Doug




----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Mar 17, 2011 at 7:28 PM, Terry Reedy <tjreedy at udel.edu> wrote:
..

I am not ready to volunteer as maintainer, but if ctypes is not listed
as my area of interest, it should be.  Unfortunately, since
Misc/maintainers.rst is gone, I am not sure how to edit maintainers
list.   I wonder if someone could replace Misc/maintainers.rst with a
short document with the proper link and describing how to edit the
list at the new location.



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 18 Mar 2011 10:18:06 -0400
Alexander Belopolsky <alexander.belopolsky at gmail.com> wrote:

Just modify experts.rst in http://hg.python.org/devguide/.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
Doug Hellmann wrote:


I am the only one who is finding the following phrases amusing?!

- "opening for a new person"
- "another ctypes expert is pretty critical"
- "job description"
- "send people to.."

-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/18/2011 10:48 AM, Senthil Kumaran wrote:

Sorry, you humor is too subtle for me ;-). I find 42 unattended open 
ctypes issues a bit sad. IDLE is a bit worse, I think, but I plan on 
helping to make a dent on *that* in the next year.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/18/2011 10:17 AM, Doug Hellmann wrote:


I would say a knowledge of C and C implementations, a tolerance of OS 
idiosyncrasies, and an interest bridging C to Python and in particular 
the way ctpes does it. (I intentionally omitted general Python knowledge.)

 > Where should we send people for more details about this request?

The tracker:
http://bugs.python.org/issue?@template=search&status=1
select Components: ctypes, enter, and take a look at some of the 42 (at 
the moment) issues. Or, also select Keyword: patch (17 issues now) to 
look at and possibly review some of the existing patches.

For more help, post here.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Mar 19, 2011 at 12:48 AM, Senthil Kumaran <orsenthil at gmail.com> wrote:

Come join python-dev - we have plenty of jobs that need filling, but
we won't offer to pay you for any of them! :)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Mar 17, 2011 at 5:44 AM, Raymond Hettinger
<raymond.hettinger at gmail.com> wrote:

Indeed!

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Please retract my committer rights
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hello

Will it be okay to make you nosy on a bug report to ask for an expert
opinion when the current developers need it?  If yes, I?ll mark your
name as ?retired? in the experts file, if not I?ll remove it.

Thanks for your contributions, and have fun in your next adventures!

[Alexander]

I?ve added a notice in 2.7 and 3.1, but the file is just gone in 3.2+.
Is adding a note to Misc/README about the removals worth it?

Regards

