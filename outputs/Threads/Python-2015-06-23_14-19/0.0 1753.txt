
============================================================================
Subject: [Python-Dev] What's new 2.x in 3.x docs.
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] What's new 2.x in 3.x docs.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
The 3.x docs mostly started fresh with 3.0. The major exception is the 
What's new section, which goes back to 2.0. The 2.x stuff comprises 
about 650KB in the repository and whatever that translates into in the 
distribution. I cannot imagine that anyone who only has 3.x and no 2.x 
version would have any interest in the 2.x history. And of course, the 
complete 2.x history will always be available with the latest 2.7.z. And 
the cover page for 3.x could even say so and include a link. So why not 
remove it from the 3.2 release (and have two separate pages for the 
online version)?

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] What's new 2.x in 3.x docs.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 22 Jan 2011 14:04:00 -0500
Terry Reedy <tjreedy at udel.edu> wrote:

Well, is there any point in doing so, apart from saving 650KB in the
repository? I'm not sure we care about the latter (right now the
whole source tree is more than 50MB, and that's without version
control information).

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] What's new 2.x in 3.x docs.
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jan 22, 2011, at 11:04 AM, Terry Reedy wrote:


I think there is value in the older whatsnew docs.  The provide a readable introduction to various features and nicely augment the plain docs which can be a little dry.

+1 for keeping the links as-is.  Removing them takes away a resource and gains nothing.


Raymond





----------------------------------------
Subject:
[Python-Dev] What's new 2.x in 3.x docs.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Jan 23, 2011 at 7:23 AM, Raymond Hettinger
<raymond.hettinger at gmail.com> wrote:

They're also a useful resource when developing compatibility guides
for projects that target older versions (including ones that support
py3k via 2to3).

With the latest 3.x release always being at the top, I agree with
Raymond that retaining the history is a better option.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] What's new 2.x in 3.x docs.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 1/22/2011 2:20 PM, Antoine Pitrou wrote:

I was only proposing actual removal of what to me is noise from the 
windows help file (now 5.6 mb) with a link to the online version. But 
the idea is rejected. Fini.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] What's new 2.x in 3.x docs.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 23.01.2011 02:48, schrieb Nick Coghlan:

Agreed.

Georg


