
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #9566: Fix a
 compiler warning in tupleiter_setstate() on Windows x64
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #9566: Fix a
 compiler warning in tupleiter_setstate() on Windows x64
----------------------------------------
Author: Jeremy Klot
Attributes: []Content: 
On Mon, Jun 24, 2013 at 3:33 PM, victor.stinner
<python-checkins at python.org> wrote:

Actually, this will still lose data when state > MAX_INT (on Windows).
 It should be changed to PyLong_AsSssize_t(state) to ensure consistent
behavior on all platforms.

-- 
Jeremy Kloth



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #9566: Fix a
 compiler warning in tupleiter_setstate() on Windows x64
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Ah yes correct, it should be better with the following commit:
http://hg.python.org/cpython/rev/3a393fc86b29

Victor

2013/6/24 Jeremy Kloth <jeremy.kloth at gmail.com>:

