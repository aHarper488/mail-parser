
============================================================================
Subject: [Python-Dev] time.wallclock()
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] time.wallclock()
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
Working on Condition variables and semaphores (see http://bugs.python.org/issue10260) I noticed that time.time() was being used to correctly time blocking system calls.  On windows, I would have used time.clock() but reading the documentation made me realize that on Unix that would return CPU seconds which are useless when blocking.  However, on Windows, time.clock() has a much higher resolution, apart from being a "wallclock" time, and is thus better suited to timing that time.time().   In addition, time.time() has the potential of giving unexpected results if someone messes with the system clock.

I was wondering if it were helpful to have a function such as time.wallclock() which is specified to give relative wallclock time between invocations or an approximation thereof, to the system's best ability?

We could then choose this to be an alias of time.clock() on windows and time.time() on any other machine, or even have custom implementations on machines that support such a notion.

Kristj?n
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101101/2370cb1f/attachment.html>



----------------------------------------
Subject:
[Python-Dev] time.wallclock()
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 01/11/2010 14:00, Kristj?n Valur J?nsson wrote:

I think this would be helpful. Having to do platform specific checks to 
choose which time function to use is annoying.

Michael



-- 

http://www.voidspace.org.uk/

READ CAREFULLY. By accepting and reading this email you agree,
on behalf of your employer, to release me from all obligations
and waivers arising from any and all NON-NEGOTIATED agreements,
licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap,
confidentiality, non-disclosure, non-compete and acceptable use
policies ("BOGUS AGREEMENTS") that I have entered into with your
employer, its partners, licensors, agents and assigns, in
perpetuity, without prejudice to my ongoing rights and privileges.
You further represent that you have the authority to release me
from any BOGUS AGREEMENTS on behalf of your employer.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101101/4cac4320/attachment.html>



----------------------------------------
Subject:
[Python-Dev] time.wallclock()
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
Ok, please see http://bugs.python.org/issue10278
K

From: Michael Foord [mailto:fuzzyman at voidspace.org.uk]
Sent: 1. n?vember 2010 22:05
To: Kristj?n Valur J?nsson
Cc: python-dev at python.org
Subject: Re: [Python-Dev] time.wallclock()


I think this would be helpful. Having to do platform specific checks to choose which time function to use is annoying.

Michael



-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101101/b5a6f25e/attachment.html>

