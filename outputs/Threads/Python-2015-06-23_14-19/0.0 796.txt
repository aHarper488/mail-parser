
============================================================================
Subject: [Python-Dev] Running Clang 2.7's static analyzer over the code
	base
Post Count: 15
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/5/3 Brett Cannon <brett at python.org>:

Do these changes even warrant a NEWS entry? NEWS is more for user
facing changes.


Why?




-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
I'll just do a single entry saying that the static analyzer was used and not
list the files.

And in reply to Benjamin's question about the whitespace, I guess it
actually doesn't matter. More important to clean up in py3k.

On May 3, 2010 4:00 PM, "Antoine Pitrou" <solipsis at pitrou.net> wrote:


Benjamin Peterson <benjamin <at> python.org> writes:

question is
Indeed. At most a single NEWS entry sounds sufficient.

Regards

Antoine.


_______________________________________________
Python-Dev mailing list
Python-Dev at python.org
http://mail.python.org/mailman/listinfo/python-dev
Unsubscribe:
http://mail.python.org/mailman/options/python-dev/brett%40python.org
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100503/a2ba4829/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
Antoine Pitrou wrote:
Let's not forget to consider what Hg can do, since that represents the
future.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
See PyCon Talks from Atlanta 2010  http://pycon.blip.tv/
Holden Web LLC                 http://www.holdenweb.com/
UPCOMING EVENTS:        http://holdenweb.eventbrite.com/




----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Alexandre Vassalott
Attributes: []Content: 
On Mon, May 3, 2010 at 7:34 PM, Barry Warsaw <barry at python.org> wrote:

Will changing the indentation of source files to 4 space indents break
patches on the bug tracker?

-- Alexandre



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mardi 04 mai 2010 04:34:13, Barry Warsaw a ?crit :

It will make the port of patches (commits) between trunk and py3k much harder. 
Can you explain why do you want to only fix only py3k and not trunk?

-- 
Victor Stinner
http://www.haypocalc.com/



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Barry Warsaw <barry <at> python.org> writes:

Probably quite a bit still; all C extension bug fixes for example.

I think we should reindent all 3 branches. Most of the work can probably be
scripted (str.replace("\t", " " * 4)), and then a visual pass is necessary to
fix vertical alignments and the like.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Tue, May 4, 2010 at 14:41, Antoine Pitrou <solipsis at pitrou.net> wrote:

If the script is robust enough, I can run it as part of the
conversion, making sure that all branches get cleaned up...

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mardi 04 mai 2010 14:41:42, Antoine Pitrou a ?crit :

2.7 will be maintained for long time, longer than any other 2.x version.


We should also add pre-commit scripts to avoid the reintroduction of 
tabulations in C (and Python?) files.

-- 
Victor Stinner
http://www.haypocalc.com/



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Zvezdan Petkovi
Attributes: []Content: 
On May 4, 2010, at 8:41 AM, Antoine Pitrou wrote:


Is it really that simple?

People often do the following:

- use tabs inside strings ("\t" should be used instead);
- use tabs for alignment of inline comments on the right side;
- mix tabs and spaces for indentation whitespace (continuation lines);
- use tabs for alignment inside comments.

A simple replacement of a tab with 4 spaces as you propose does not work  on such a code.

To do it properly, you may end up reinventing `indent` (UNIX program).

What if there was a set of `indent` flags defined for PEP-7?
It would be a nice tool to check whether a new code complies.

However, running `indent` on an existing code would probably cause non-whitespace changes too as it rearranges the comments and code.

I'm afraid that any other kind of "script + visual pass + post-edit" would incur similar changes if done properly (only more tedious).

So, the question is what bothers developers more:

- old C files with tab indentation, or
- a lot of changes in version control to fix them?

Both?
:-)

	Zvezdan




----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, May 4, 2010 at 08:27, Zvezdan Petkovic <zvezdan at zope.com> wrote:


`make patchcheck` already does this for Python code. Adding to the command
to handle C code wouldn't be hard.

-Brett



-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100504/4bc26aca/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
I am done running the analysis over trunk. I will not svnmerge these changes
into py3k as the amount of time and effort that would take equates to
running the static analyzer again just before 3.2 is released and possibly
catching more changes (and maybe even a newer version of Clang at that
point).

On Mon, May 3, 2010 at 15:37, Brett Cannon <brett at python.org> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100505/fd3f5c06/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mardi 04 mai 2010 00:37:22, Brett Cannon a ?crit :

Do you plan to port the changes to py3k? and what about 2.6 and 3.1?

--

Please don't change whitespaces in the same commit. I think that we should fix 
the whitespaces of the whole project in one unique commit. Well, or least 
don't fix whitespaces file by file...

-- 
Victor Stinner
http://www.haypocalc.com/



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, May 5, 2010 at 14:01, Victor Stinner
<victor.stinner at haypocalc.com>wrote:



In case you didn't see my follow-up email that I sent just before this
email, I will most likely do py3k when 3.2 is closer.




Not doing 2.6 as almost all changes are too minor bother. I think I found
like two potential Py_DECREF/Py_XDECREF changes, but that's about it. And
3.1 would require py3k which I am not planning on  doing in the near future.

-Brett
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100505/7cc8f68c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mardi 04 mai 2010 07:12:32, Martin v. L?wis a ?crit :

Tested on posixmodule.c: it works :-)

-- 
Victor Stinner
http://www.haypocalc.com/



----------------------------------------
Subject:
[Python-Dev] Running Clang 2.7's static analyzer over the code
	base
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Thu, May 6, 2010 at 08:09, Ronald Oussoren <ronaldoussoren at mac.com>wrote:


That's a thought, but I have not looked into it yet. As of right now the
first thing I would do is fix its NULL de-reference analysis as it had a
bunch of false-positives on that (I don't think it handles `!ptr` as
equivalent to `ptr == NULL`).

-Brett



-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100506/dc5d5f7b/attachment.html>

