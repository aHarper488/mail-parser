
============================================================================
Subject: [Python-Dev] [Python-checkins] r86965 -
	python/branches/py3k/Lib/test/__main__.py
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86965 -
	python/branches/py3k/Lib/test/__main__.py
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Dec 3, 2010 at 8:42 PM, michael.foord
<python-checkins at python.org> wrote:

If syconfig.is_python_build() returns False...


... this line is going to blow up with a NameError.

I would suggest putting this common setup code into a _make_test_dir()
helper function in regrtest, then have both regrtest and test.__main__
call it.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

