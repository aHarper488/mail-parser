
============================================================================
Subject: [Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: Toshio Kuratom
Attributes: []Content: 
On Wed, Nov 23, 2011 at 01:41:46AM +0900, Stephen J. Turnbull wrote:
For Fedora (and currently, Red Hat is based on Fedora -- a little more about
that later, though), we have parallel python2 and python3 stacks.  As time
goes on we've slowly brought more python-3 compatible modules onto the
python3 stack (I believe someone had the goal a year and a half ago to get
a complete pylons web development stack running on python3 on Fedora which
brought a lot of packages forward).

Unlike Barry's work with Ubuntu, though, we're mostly chiselling around the
edges; we're working at the level where there's a module that someone needs
to run something (or run some optional features of something) that runs on
python3.

Where distros aren't working on parallel stacks, there definitely needs to
be some transition plan.  With my experience with parallel stacks, the best
help there is to 1) help upstreams port to py3k (If someone can get PIL's
py3k support finished and into a released package, that would free up a few
things).  2) open bugs or help with creating python3 packages of modules
when the upstream support is there.

Depending on what software Barry's talking about porting to python3, that
could be a big incentive as well.  Just like with the push in Fedora to have
pylons run on python3, I think that having certain applications that run on
python3 and therefore need to have stacks of modules that support it is one
of the prime ways that distros become motivated to provide python3 packages
and support.  This is basically the "killer app" idea in a new venue :-)

-Toshio
-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 198 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111122/b7c54e1e/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: David Malcol
Attributes: []Content: 
On Tue, 2011-11-22 at 09:13 -0800, Toshio Kuratomi wrote:

FWIW, current status of Fedora's Python 3 stack can be seen here:
  http://fedoraproject.org/wiki/Python3
and that page may be of interest to other distributions - I know of at
least one other distribution that's screen-scraping it ;)




----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Nov 22, 2011, at 09:13 AM, Toshio Kuratomi wrote:


Debian (and thus Ubuntu) also has separate Python 2 and 3 stacks.  In general,
if you have a Python package (e.g. on PyPI) called 'foo', you'll have a Debian
binary package called python-foo for the Python 2 version, and python3-foo for
the Python 3 version.

/usr/bin/python will always (modulo perhaps PEP 394) point to Python 2.x with
Python 3 accessible via /usr/bin/python3.  The minor version numbered Python
binaries are also available.

Debian's infrastructure makes it fairly easy to support multiple versions of
Python at the same time, and of course to support both a Python 2 and 3 stack
simultaneously.  It's also fairly easy to switch the default Python version.
Binary packages containing pure-Python are space efficient, sharing one copy
of the Python source code for all supported versions.  A symlink farm is used
to manage the incompatibilities in .pyc files, but only for Python 2, since
PEPs 3147 and 3149 solve this problem in a better way for Python 3 (no symlink
farms necessary).  The one additional complication though is that extension
modules must be built for each supported version, and all .so's are included
in a single binary package.  E.g. if python-foo has an extension module, it
will contain the 2.6 .so and the 2.7 .so.  For the next version of Ubuntu, we
will be dropping Python 2.6 support, so our binary packages are rebuilt to
contain only the 2.7 version of the extension module.

Details on how Debian packages Python, including its deviations from upstream,
are available here:

    http://wiki.debian.org/Python

Ubuntu's deviations from Debian and other details are available here:

    https://wiki.ubuntu.com/Python


This is great, because it means Fedora's taking kind of a bottom-up approach,
while Ubuntu is taking a more top-down approach.  Working together, we'll
change the world. :)

The key here is that we push as many of the changes as possible as far
upstream as possible.  I know Toshio and David agree, we want to get upstream
package authors and application developers to support Python 3 as much as
possible.  I hope there will be no cases where a distro has to fork a package
or application to support Python 3, although we will do it if there's no other
way.  Most likely for Ubuntu though, that would be pushing the changes into
Debian.


+1


Again, wholehearted +1.

For now, we are not spending much time on server applications, though I've
seen promising talk about Twisted ports to Python 3.  We're looking
specifically at desktop applications, such as Update Manager, Software Center,
Computer Janitor, etc.  Those may be fairly Ubuntu and/or Debian specific, but
the actual applications themselves aren't too difficult to port.
E.g. switching to Gnome object introspection, which already supports Python
3.  We can easily identify the dependency stack for the desktop applications
we're targeting, which leads us to looking at ports of the dependent
libraries, and that benefits all Python users.

Our goal is for the Ubuntu 14.04 LTS release (in April 2014) to have no Python
2 on the release images, or in our "main" archive, so everything you'd get on
your desktop in a default install would be Python 3.  For the upcoming 12.04
LTS release, I'd be happy if we had just a couple of Python 3 applications on
the desktop by default.

I see the work going on in Fedora/RedHat, Debian/Ubuntu, and other
distributions as applying some positive momentum on pushing the Python
community over the tipping point for Python 3 support.

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111122/10961721/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Nov 22, 2011, at 06:10 PM, Xavier Morel wrote:


Nor should it!  PEP 394 attempts to codify the Python project's
recommendations for what version 'python' (e.g. /usr/bin/python) points to,
but I think there is widespread agreement that it would be a mistake to point
it at Python 3.


This is probably fairly distro specific, so I doubt any such document exists,
or would be helpful.  E.g. Debian's approach is fairly intimately tied to its
build tools, rules files, and policies.  There is, in fact, a separate Python
policy document for Debian.

What this means for Debian is that well-behaved distutils-based packages can
be built for all available Python 2 versions with about 3 lines of code in
your debian/rules file.  You don't even really need to think about it, which
is especially nice during Python version transitions.  Of course, in Ubuntu,
we'll never have to do one of those again <pep-404-wink>.

The tools are not quite there for Python 3, though they are being actively
worked on.  This means it takes more effort from the distro packager to get
Python 2 and Python 3 binary packages built (assuming upstream supports both),
and to built it for multiple versions of Python 3 (not an issue right now
though, since 3.2 is the minimum version we're all targeting).  It's
definitely possible, but it's not as trivially easy as it usually is for
Python 2.  I fully expect that to improve over time.

I do occasionally fiddle with MacPorts, and have used Gentoo's system in a
previous life, but I don't really know enough about them to make anything
other than general recommendations.  OTOH, I think Fedora's and Debian's
experience is that separate Python 2 and Python 3 stacks is the best way to
avoid insanity for operating systems and their users.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111122/6433072f/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Tue, Nov 22, 2011 at 17:41, Stephen J. Turnbull <stephen at xemacs.org> wrote:

Problems like what?


Please create a connection to your distro by filing bugs as you
encounter them? The Gentoo Python team is woefully understaffed (and
I've been busy with some Real Life things, although that should
improve in a couple more weeks), but we definitely care about
providing an environment where you can successfully run python2 and
python3 in parallel.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Dirkjan Ochtman writes:
 > On Tue, Nov 22, 2011 at 17:41, Stephen J. Turnbull <stephen at xemacs.org> wrote:
 > > This is still a big mess in Gentoo and MacPorts, though. ?MacPorts
 > > hasn't done anything about ceating a transition infrastructure AFAICT.
 > > Gentoo has its "eselect python set VERSION" stuff, but it's very
 > > dangerous to set to a Python 3 version, as many things go permanently
 > > wonky once you do. ?(So far I've been able to work around problems
 > > this creates, but it's not much fun.)
 > 
 > Problems like what?

Like those I explained later in the post, which you cut.  But I'll
repeat.  Some ebuilds are not prepared for Python 3, so must be
emerged with a Python 2 eselected (and sometimes they need a specific
Python 2).  Some which are prepared don't get linted often enough, so
new ebuilds are DOA because of an accented character in a changelog
triggering a Unicode exception or similar dumb things like that.

 > > I don't have any connections to the distros, so can't really offer to
 > > help directly. ?I think it might be a good idea for users to lobby
 > > (politely!) ?their distros to work on the transition.
 > 
 > Please create a connection to your distro by filing bugs as you
 > encounter them?

No, thank you.  File bugs, maybe, although most of the bugs I
encounter in Gentoo are already in the database (often with multiple
regressions going back a year or more), I could do a little more of
that.  (Response in the past has not been encouraging.)  But I don't
have time for distro politics.

Is lack of Python 3-readiness considered a bug by Gentoo?



----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Wed, Nov 23, 2011 at 13:21, Stephen J. Turnbull <stephen at xemacs.org> wrote:

They were in a later post, I didn't cut them. :)


I'm sorry for the lack of response in the past. I looked at Gentoo's
Bugzilla and didn't find any related bugs you reported or were CC'ed
on, can you name some of them?


Definitely. Again, we are trying to hard to make things better, but
there's a lot to do and going through version bumps sometimes wins out
over addressing the hard problems. Be assured, though, that we're also
trying to make progress on the latter. If you're ever on IRC, come
hang out in #gentoo-python, where distro politics should be minimal
and the crew is generally friendly and responsive.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Promoting Python 3 [was: PyPy 1.7 - widening the
 sweet spot]
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Dirkjan Ochtman writes:

 > I'm sorry for the lack of response in the past. I looked at Gentoo's
 > Bugzilla and didn't find any related bugs you reported or were CC'ed
 > on, can you name some of them?

This isn't about my bugs; I've been able to work through them
satisfactorily.  It's about what I perceive as a need for simultaneous
improvement in Python 3 support across several distros, covering
enough users to establish momentum.

I don't think Python 3 needs to (or even can) replace Python 2 as the
system python in the near future.  But the "python" that is visible to
users (at least on single-user systems) should be choosable by the
user.  eselect (on Gentoo) and port select (on MacPorts) *appear* to
provide this, but it doesn't work very well.

 > > Is lack of Python 3-readiness considered a bug by Gentoo?
 > 
 > Definitely. Again, we are trying to hard to make things better, but
 > there's a lot to do and going through version bumps sometimes wins out
 > over addressing the hard problems.

Well, as I see it the two hard problems are (1) the stack-per-python-
minor-version solution is ugly and unattractive to users, and (2) the
"select python" utility needs to be safe.  This probably means that
python-using ebuilds need to complain if the Python they find isn't a
Python 2 version by default.



