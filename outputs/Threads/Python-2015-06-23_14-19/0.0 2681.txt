
============================================================================
Subject: [Python-Dev] Assigning copyright...
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: stefan brunthale
Attributes: []Content: 
Hi,

I only had little time to spend for my open sourcing efforts, which is
why I could not get back to python-dev any time earlier...

Yesterday I forward-ported my patches to revision 76549
(13c30fe3f427), which only took 25mins or so (primarly due to the
small changes necessary to Python itself and the stability of that
parts.) Thanks to a colleague of mine (Per Larsen) I reimplemented
some of the more ugly parts of the code generator, too.
Guido's answer from the last thread was that I should duly assign the
copyright to the PSF. Unfortunatly, I don't really see any other part
than the LICENSE and README files in the Python distribution. Since my
patch basically just adds another subdirectory ("cgen") to the Python
top-level directory, I am not sure if I need to supply other
information to make my code officially PSF compatible.

Am I missing something obvious?

Thanks,
--stefan



----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi Stefan,

The PSF does not require copyright assignment (ugh!), only a contributor
agreement.  http://www.python.org/psf/contrib/contrib-form/ should give
you all you need.

Regards



----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
stefan brunthaler wrote:

A URL for the code repository (with an open-source license),
so code can be reviewed.
It is hard to review and update a giant patch.

Cheers,
Mark.



----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: stefan brunthale
Attributes: []Content: 
Hello Mark,


OK, I took Nick's advice to heart and created a fork from the official
cpython mirror on bitbucket. You can view the code patched  in
(branch: inca-only) under the following URL:
https://bitbucket.org/sbrunthaler/cpython-inline-caching

Since it is a fork, it contains the usual LICENSE from Python.

Regarding Eric's hint: It seems that this agreement needs to be signed
and mailed. Can I sign/scan and email it to somebody? (Or should I
wait until there is a decision regarding a potential integration?) The
way I understood Guido's last message it is best to use Apache 2
license without retaining my own copyright. I am perfectly fine with
that but am not sure if using the fork with sub-directories including
the official LICENSE takes care of that. Obviously, I don't have too
much experience in this area, so if I am missing something blatantly
obvious, I apologize beforehand...

Best,
--stefan



----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Yes, see

http://www.python.org/psf/contrib/

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: stefan brunthale
Attributes: []Content: 
Hello,


I took care of the formalities.

I am not sure how to proceed further. Would python-dev want me to draft a PEP?

Regards,
--stefan

PS: Personally, I am not a 100pct convinced that having a PEP is a
good thing in this case, as it makes a perfectly transparent
optimization "visible." AFAIR Sun opted to keep their instruction
derivatives secret, i.e., the second edition of the JVM internals does
not even mention them anymore.



----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 05/07/2012 09:23 PM, stefan brunthaler wrote:

I think you'll find that we don't keep a lot of things secret about CPython
and its implementation.

Although this is different when it comes to the community.  The PSU has




----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On 07.05.2012 21:23, stefan brunthaler wrote:

Submit a patch to the bug tracker, against default's head.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Assigning copyright...
----------------------------------------
Author: stefan brunthale
Attributes: []Content: 
Yeah, I agree that this is in principal a good thing and what makes
CPython ideally suited for research. However, my optimizations make
use of unused opcodes, which might be used in the future by actual
CPython instructions (e.g., from my previous patch to the new one the
YIELD_FROM instruction has been added.)
I'd say the situation is similar to the threaded code/computed goto's issue.


?

I am going to file a patch like Martin von Loewis suggested.

Thanks,
--stefan

