
============================================================================
Subject: [Python-Dev] Fwd: Mirroring Python repos to Bitbucket
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Fwd: Mirroring Python repos to Bitbucket
----------------------------------------
Author: Doug Hellman
Attributes: []Content: 

Charles McLaughlin of Atlassian has set up mirrors of the Mercurial repositories hosted on python.org as part of the ongoing infrastructure improvement work. These mirrors will give us a public fail-over repository in the event that hg.python.org goes offline unexpectedly, and also provide features such as RSS feeds of changes for users interested in monitoring the repository passively.

Thank you, Charles for setting this up and Atlassian for hosting it!

Doug

Begin forwarded message:





----------------------------------------
Subject:
[Python-Dev] Fwd: Mirroring Python repos to Bitbucket
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Aug 14, 2011 at 9:08 AM, Doug Hellmann <doug.hellmann at gmail.com> wrote:

The main advantage of those mirrors to my mind is that it makes it
easy for anyone to clone their own copy of the python.org repos
without having to upload the whole thing to bitbucket themselves. That
makes it easy for people to use a natural Mercurial workflow to
develop and collaborate on patches, even for components other than the
main CPython repo (e.g. the devguide or the benchmark suite).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Fwd: Mirroring Python repos to Bitbucket
----------------------------------------
Author: Petri Lehtine
Attributes: []Content: 
Doug Hellmann wrote:

As a side note, for those preferring git there's also a very
unofficial git mirror at https://github.com/jonashaag/cpython. It uses
hg-git for converting and syncs once a day.

Petri

