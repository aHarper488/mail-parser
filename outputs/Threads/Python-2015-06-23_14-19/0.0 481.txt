
============================================================================
Subject: [Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
MRAB <python <at> mrabarnett.plus.com> writes:

There is no "easy way" to do so. The only safe way is to examine all the
functions or macros you want to call with the GIL released, and assess whether
it is safe to call them. As already pointed out, no reference count should be
changed, and generally no mutable container should be accessed, except if that
container is known not to be referenced anywhere else (that would be the case
for e.g. a list that your function has created and is busy populating).

I agree that releasing the GIL when doing non-trivial regex searches is a
worthwhile research, so please don't give up immediately :-)

Regards

Antoine Pitrou.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Martin v. L?wis <martin <at> v.loewis.de> writes:

Why is it a problem? If we get a buffer through the new buffer API, the object
should ensure that the representation isn't moved away until the buffer is 
released.

Regards

Antoine.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
MRAB <python <at> mrabarnett.plus.com> writes:

There is no "easy way" to do so. The only safe way is to examine all the
functions or macros you want to call with the GIL released, and assess whether
it is safe to call them. As already pointed out, no reference count should be
changed, and generally no mutable container should be accessed, except if that
container is known not to be referenced anywhere else (that would be the case
for e.g. a list that your function has created and is busy populating).

I agree that releasing the GIL when doing non-trivial regex searches is a
worthwhile research, so please don't give up immediately :-)

Regards

Antoine Pitrou.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Martin v. L?wis <martin <at> v.loewis.de> writes:

Why is it a problem? If we get a buffer through the new buffer API, the object
should ensure that the representation isn't moved away until the buffer is 
released.

Regards

Antoine.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
MRAB <python <at> mrabarnett.plus.com> writes:

There is no "easy way" to do so. The only safe way is to examine all the
functions or macros you want to call with the GIL released, and assess whether
it is safe to call them. As already pointed out, no reference count should be
changed, and generally no mutable container should be accessed, except if that
container is known not to be referenced anywhere else (that would be the case
for e.g. a list that your function has created and is busy populating).

I agree that releasing the GIL when doing non-trivial regex searches is a
worthwhile research, so please don't give up immediately :-)

Regards

Antoine Pitrou.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Martin v. L?wis <martin <at> v.loewis.de> writes:

Why is it a problem? If we get a buffer through the new buffer API, the object
should ensure that the representation isn't moved away until the buffer is 
released.

Regards

Antoine.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
MRAB <python <at> mrabarnett.plus.com> writes:

There is no "easy way" to do so. The only safe way is to examine all the
functions or macros you want to call with the GIL released, and assess whether
it is safe to call them. As already pointed out, no reference count should be
changed, and generally no mutable container should be accessed, except if that
container is known not to be referenced anywhere else (that would be the case
for e.g. a list that your function has created and is busy populating).

I agree that releasing the GIL when doing non-trivial regex searches is a
worthwhile research, so please don't give up immediately :-)

Regards

Antoine Pitrou.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Martin v. L?wis <martin <at> v.loewis.de> writes:

Why is it a problem? If we get a buffer through the new buffer API, the object
should ensure that the representation isn't moved away until the buffer is 
released.

Regards

Antoine.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
MRAB <python <at> mrabarnett.plus.com> writes:

There is no "easy way" to do so. The only safe way is to examine all the
functions or macros you want to call with the GIL released, and assess whether
it is safe to call them. As already pointed out, no reference count should be
changed, and generally no mutable container should be accessed, except if that
container is known not to be referenced anywhere else (that would be the case
for e.g. a list that your function has created and is busy populating).

I agree that releasing the GIL when doing non-trivial regex searches is a
worthwhile research, so please don't give up immediately :-)

Regards

Antoine Pitrou.






----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?GIL_required_for_=5Fall=5F_Python_calls=3F?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Martin v. L?wis <martin <at> v.loewis.de> writes:

Why is it a problem? If we get a buffer through the new buffer API, the object
should ensure that the representation isn't moved away until the buffer is 
released.

Regards

Antoine.




