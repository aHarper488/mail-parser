
============================================================================
Subject: [Python-Dev] r84775 - peps/trunk/pep-3149.txt
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] r84775 - peps/trunk/pep-3149.txt
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 13 Sep 2010 16:18:44 +0200 (CEST)
barry.warsaw <python-checkins at python.org> wrote:

Are these the default paths? The PEP doesn't say how a distribution
is supposed to choose its PEP 3149 filesystem layout (instead of
/usr/lib/python3.2/site-packages).

Thanks

Antoine.





----------------------------------------
Subject:
[Python-Dev] r84775 - peps/trunk/pep-3149.txt
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Sep 13, 2010, at 04:36 PM, Antoine Pitrou wrote:


Why should it?  Distributions are going to make their own decisions
independent of the PEP.  That's why s/would/might/ in the above change.  I'm
open to suggestions for better ways to explain it.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100913/8da1d6fc/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] r84775 - peps/trunk/pep-3149.txt
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 13 Sep 2010 10:55:16 -0400
Barry Warsaw <barry at python.org> wrote:

I meant how these decisions are implemented. Is there a configure
switch (there doesn't seem to be)? Does it require patching Python?

Thanks

Antoine.





----------------------------------------
Subject:
[Python-Dev] r84775 - peps/trunk/pep-3149.txt
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Sep 13, 2010, at 05:04 PM, Antoine Pitrou wrote:


Ah, no.  Standard configure switches are used.  Debian (inherited by Ubuntu)
has a post-installation script for Python packages which create the .py
symlinks and do the byte-compilation.  The big win here is that much of this
can go away now (and in fact there are modifications to this post-installation
script already).

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100913/6600aa37/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] r84775 - peps/trunk/pep-3149.txt
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 


Ok, so can you explain how the new thing will work (on Debian)? :)
Does it mean that e.g. /usr/lib/python3.2/site-packages will get
symlinked to /usr/lib/python?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] r84775 - peps/trunk/pep-3149.txt
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 13.09.2010 17:36, schrieb Antoine Pitrou:

They currently get /usr/lib/pyshared/<version> onto sys.path, by
providing a .pth file (python-support.pth). There are many other ways:
you can edit site.py, provide a sitecustomize.py, or edit Modules/Setup
(providing a Modules/Setup.local may also work).

If I was them, I wouldn't relocate the standard extension modules, but
keep them in lib-dynload; /usr/lib/pyshared would only be there for
additional packages (which then don't need to bring /usr/lib/python3.3
into existance even though python 3.3 isn't installed).

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] r84775 - peps/trunk/pep-3149.txt
----------------------------------------
Author: Piotr =?utf-8?Q?O=C5=BCarowski?
Attributes: []Content: 
[Antoine Pitrou, 2010-09-13]

we have /usr/lib/python3/dist-packages in sys.path (via patched
Lib/site.py). Our python3.1 will use the same directory as well
(version in experimental is modified to use tagged extensions).
distutils has additional --install-layout command which when set to
"deb" uses Debian's locations, if distutils is not used (or
--install-layout=deb not set), dh_python3 will move files to the
right location at (package's) build time (and rename .so files)


no, /usr/lib/python3.2/site-packages is not used at all (we don't use
"site-packages" anymore to avoid conflicts with local installations of
Python. /usr/lib/python3.2/dist-packages on the other hand is still in
sys.path, but I'm not sure what we'll do with it (we still have to
figure out what to do with modules that work with 3.2 only and cannot be
patched due to f.e. from __future__ imports)
-- 
Piotr O?arowski                         Debian GNU/Linux Developer
www.ozarowski.pl          www.griffith.cc           www.debian.org
GPG Fingerprint: 1D2F A898 58DA AF62 1786 2DF7 AEF6 F1A2 A745 7645

