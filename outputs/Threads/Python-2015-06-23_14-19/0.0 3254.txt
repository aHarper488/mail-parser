
============================================================================
Subject: [Python-Dev] Help to fix this bug
	http://bugs.python.org/issue15068
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Help to fix this bug
	http://bugs.python.org/issue15068
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
It depends on the Python version. In 3.3, for example, look into
Modules/_io/fileio.c

Eli



On Tue, Jun 19, 2012 at 2:39 PM, gmspro <gmspro at yahoo.com> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120619/d20ca9d9/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Help to fix this bug
	http://bugs.python.org/issue15068
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hi,

On Tue, 19 Jun 2012 04:39:30 -0700 (PDT)
gmspro <gmspro at yahoo.com> wrote:

I'm not sure why you think this is fixable, given the comments on the
tracker. What is your plan?


Can I suggest you try to investigate it a bit yourself:

<_io.TextIOWrapper name='<stdin>' mode='r' encoding='UTF-8'>

So it's a TextIOWrapper from the _io module (which is really the
implementation of the io module). You'll find its source in
Modules/_io. TextIOWrapper objects are defined in Modules/_io/textio.c.
But as you know, they wrap buffered I/O objects, which are defined in
Modules/_io/bufferedio.c. In sys.stdin's case, the buffered I/O object
wraps a raw FileIO object, defined in Modules/_io/fileio.c:

<_io.BufferedReader name='<stdin>'>
<_io.FileIO name='<stdin>' mode='rb'>


Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Help to fix this bug
	http://bugs.python.org/issue15068
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 19.06.12 15:13, Antoine Pitrou wrote:

And don't forget about _pyio module.


