
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: fix compilation on
	Windows
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: fix compilation on
	Windows
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/16/2013 4:17 PM, victor.stinner wrote:


That fixed my problem with compiling 3.4, 32 bit, Win 7. Thanks.

  But I cannot compile 3.3 python_d since May 6. In fact, there are more 
errors now than 8 hours ago.
7 things failed to build instead of 5 (3 is normal for me, given the 
lack of some dependencies).
I believe the following is new.
Red error box with .../p33/PCBuild/make_versioninfo_d.exe is not a valid 
Win32 application.
The VS gui output box has "Please verify that you have sufficient rights 
to run this command."

Some more errors:
10>..\PC\pylauncher.rc(16): error RC2104: undefined keyword or key name: 
FIELD3
10>
9>  symtable.c
9>..\Python\symtable.c(1245): error C2143: syntax error : missing ';' 
before 'type'
9>..\Python\symtable.c(1246): error C2065: 'cur' : undeclared identifier
9>..\Python\symtable.c(1248): error C2065: 'cur' : undeclared identifier
9>..\Python\symtable.c(1253): error C2065: 'cur' : undeclared identifier
23>  Traceback (most recent call last):

23>    File "build_ssl.py", line 253, in <module>
23>      main()
23>    File "build_ssl.py", line 187, in main
23>      os.chdir(ssl_dir)
23>  FileNotFoundError: [WinError 2] The system cannot find the file 
specified: '..\\..\\openssl-1.0.1e'

Earlier, about 4 other files had several warnings. I do no see the 
warnings now because they compiled then and have not changed.
Errors are more urgent, but should warnings be ignored?

Terry


