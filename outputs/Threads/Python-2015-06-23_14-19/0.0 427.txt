
============================================================================
Subject: [Python-Dev] Release timer for Core Development page
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Release timer for Core Development page
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
I've got another idea of having a release timer on
http://python.org/dev/ page together with link to generated release
calendar.
It will help to automatically monitor deadlines for feature fixes in
alpha releases without manually monitoring this mailing list.

There is already a navigation box on the right side where this
information fits like a glove.
Does anybody else find this feature useful for Python development?

-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Release timer for Core Development page
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
anatoly techtonik wrote:

Not particularly. The target release dates are in the release PEPs and
if I wanted a timer I'd add it to my personal calendar.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

