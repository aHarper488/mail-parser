
============================================================================
Subject: [Python-Dev] Rough idea for adding introspection information
	for builtins
Post Count: 15
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 7 Jul 2013 05:22, "Larry Hastings" <larry at hastings.org> wrote:
better off not leaking it out of Argument Clinic--don't expose it in this
string we're talking about, and don't add support for it in the
inspect.Parameter object.  I'm not going to debate range(), the syntax of
which predates one of our release managers.  But I suggest option groups
are simply a misfeature of the curses module.  There are some other
possible uses in builtins (I forgot to dig those out this evening) but so
far we're talking adding complexity to an array of technologies (this
representation, the parser, the Parameter object) to support a handful of
uses of something we shouldn't have done in the first place, for consumers
who I think won't care and won't appreciate the added conceptual complexity.
Clinic:
generated the "stat(...)" line.  It doesn't have any info because of the
lack of introspection information.
and Argument Clinic can stop generating its redundant prototype line.  But
if pydoc doesn't have argument group information, it won't be able to tell
where one group ends and the next begins, and it won't be able to render
the prototype for the help text correctly.  I fear misleading text is even
worse than no text at all.
as we can give them.  If we give them argument group information, they can
flag malformed calls (e.g. "there's no way to legally call this function
with exactly three arguments").
enough for me: I propose we amend the Parameter object to add option group
information for positional-only parameters.

Rather than perpetuating unwanted complexity, can't we just add a single
"incomplete signature" flag to handle the legacy cases, and leave those to
the docstrings?

As in, if the flag is set, pydoc displays the "..." because it knows the
signature data isn't quite right.

Alternatively (and even more simply), is it really so bad if argument
clinic doesn't support introspection of such functions at all, and avoids
setting __signature__ for such cases?

As a third option, we could add an "alternative signatures" attribute to
capture multiple orthogonal signatures that should be presented on separate
lines.

All of those possibilities sound more appealing to me than adding direct
support for parameter groups at the Python level (with my preference being
to postpone the question to 3.5 by not allowing introspection of affected
functions in this initial iteration).

Cheers,
Nick.

http://mail.python.org/mailman/options/python-dev/ncoghlan%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130707/109b6d4d/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 7 Jul 2013 10:25, "Larry Hastings" <larry at hastings.org> wrote:
"incomplete signature" flag to handle the legacy cases, and leave those to
the docstrings?
signature data isn't quite right.
clinic doesn't support introspection of such functions at all, and avoids
setting __signature__ for such cases?
capture multiple orthogonal signatures that should be presented on separate
lines.
support for parameter groups at the Python level (with my preference being
to postpone the question to 3.5 by not allowing introspection of affected
functions in this initial iteration).
realized after I sent it that there's a related class of tools that are
interested: PyFlakes, PyLint, and the like.  I'm sure the static
correctness analyzers would like to be able to automatically determine
"this is an illegal number of parameters for this function" for
builtins--particularly for third-party builtins!  The fact that we wouldn't
need to special-case pydoc suggests it's the superior approach.  ("Special
cases aren't special enough to break the rules.")
Parameter object.  Let me propose such a parameter here, in the style of
the Parameter class documentation:
belongs to.  Optional parameter groups are contiguous sequences of
parameters that must either all be specified or all be unspecified.  For
example, if a function takes four parameters but the last two are in an
optional parameter group, you could specify either two or four arguments to
that function--it would be illegal to specify three arguments.  Parameter
groups can only contain positional-only parameters; therefore group will
only be a non-None value when kind is POSITIONAL_ONLY.
projects would very much like to have this information.
docstring but doesn't fix the introspection information (so we'd be
providing incorrect introspection information to tools), 2) a small cop-out
(which I think would also probably require a hack to pydoc), and 3) way
more complicated than doing it the right way (so I don't see how it's an
improvement).  Of your three suggestions I dislike 2) least.
range().  I concede that it's legacy, but it's not going away.  Ever.  I
now think we're better off embracing this complexity than trying to sweep
it under the rug.

The "group" attribute sounds reasonable to me, with the proviso that we use
"multiple signature lines" as the way to represent them in pydoc (rather
than inventing a notation for showing them in a single signature line).

It occurs to me that "type" is itself an example of this kind of dual
signature.

Cheers,
Nick.

http://mail.python.org/mailman/options/python-dev/ncoghlan%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130707/afa3d53d/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 6 Jul, 2013, at 19:33, Larry Hastings <larry at hastings.org> wrote:

Not entirely on topic, but close enough: pydoc currently doesn't use the __signature__ information at all. Adding such support would be easy enough, see #17053 for an implementation ;-)

Ronald



----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 07 Jul 2013 04:48:18 +0200, Larry Hastings <larry at hastings.org> wrote:

Sorry to make your life more complicated, but unless I'm misunderstanding
something, issue 18220 (http://bugs.python.org/issue18220) throws another
monkey-wrench in to this.  If I'm understanding this discussion correctly,
that example:

    islice(stop)
    islice(start, stop [, step])

requires the multiple-signature approach.

Note also that the python3 documentation has moved away from the []
notation wherever possible.

--David



----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 7 Jul, 2013, at 4:48, Larry Hastings <larry at hastings.org> wrote:


Signature objects use a name in angled brackets to indicate that a parameter is positional only, for example "input(<prompt>)". That might be an alternative to adding a "/" in the argument list in pydoc's output.

Ronald



----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 7 Jul, 2013, at 13:35, Larry Hastings <larry at hastings.org> wrote:

I have a branch of PyObjC that uses this: <https://bitbucket.org/ronaldoussoren/pyobjc-3.0-unstable/overview>. That branch isn't quite stable yet, but does add a __signature__ slot to objc.selector and objc.function (basicly methods of Cocoa classes and automaticly wrapped global functions), both of which only have positional-only arguments. With the patch for pydoc/inspect I mentioned earlier I can then generate somewhat useful documentation for Cocoa classes using pydoc.

A word of warning though: the PyObjC source code isn't the most approachable, the code that generates the Signature object is actually in python (callable_signature in pyobjc-core/Lib/objc/_callable_docstr.py)

Ronald





----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 7 Jul, 2013, at 19:20, Larry Hastings <larry at hastings.org> wrote:


I wasn't clear enough in what I wrote.  The stdlib contains support for positional-only arguments in Signature objects (see Lib/inspect.py, line 1472, which says "_POSITIONAL_ONLY        = _ParameterKind(0, name='POSITIONAL_ONLY')".  The __str__ of Parameter amongst other says:

        if kind == _POSITIONAL_ONLY:
            if formatted is None:
                formatted = ''
            formatted = '<{}>'.format(formatted)

That is, it adds angled brackets around the names of positional-only parameters.  I pointed to PyObjC as an example of code that actually creates Signature objects with positional-only arguments, as far as I know the stdlib never does this because the stdlib can only create signatures for plain python functions and those cannot have such arguments.


Using Guido's suggestion is fine by me, I agree that there is a clear risk of angle-bracket overload for functions with a lot of arguments. I do think that the __str__ for Signatures should be changed to match the convention. 

And to be clear: I'm looking forward to having Argument Clinic and __signature__ objects on built-in functions, "funcname(...)" in the output pydoc is somewhat annoying, especially for extensions where the author hasn't bothered to provide a docstring. That's one reason I wrote the __signature__ support in PyObjC in the first place (and the patch for pydoc to actually use the signature information)

Ronald




----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 7/7/2013 7:35 AM, Larry Hastings wrote:

This is currently true of Idle calltips. But with 3.2 out of the way, I 
plan to switch sometime and not worry about 2.7.


This is how it was until last September. See #15831, which also changed 
max, min, and slice entries to use two lines. The multiple lines for 
bytes and str signatures in the docstrings were another issue.


It seems that signatures that do not match what one can do with a def 
statement are confusing. The header line for the Python version of the 
above is
	def range(start_or_stop, stop=None, step=1):
My suggestion to use this, which is the actual signature, was rejected 
in favor of using two lines. This is fine with me, as it documents the 
calling signatures rather than the hybrid definition signature used to 
implement the call signatures.


Because it has not yet been changed ;-).


I look forward to the day when accurate (auto-generated) call data is 
also available for C-coded functions.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Since it's relevant: my recollection us that the current use of angle
brackets in inspect.Signature is just the default use of them for "there is
no canonical representation of this, but leaving them out would be
misleading" (I haven't checked if the PEP says that explicitly).

I previously forgot Larry, Guido & I discussed the appropriate use of
square brackets and the slash in the definition format at PyCon, so I now
think having the Argument Clinic PEP also cover their appropriate use in
the inspect.Signature string output is a good idea.

Cheers,
Nick.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130709/df2d267b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Larry Hastings, 19.03.2013 05:45:

I had already noted that this would be generally useful, specifically for
Cython, so I'm all for going this route. No need to invent something new here.



I can't see why the size would matter in any way.



Plus, if it becomes the format how C level signatures are expressed anyway,
it wouldn't require any additional build time preprocessing.



Why not require it to be there already? Maybe more like

    def foo(arg, b=3, *, kwonly='a'):
         ...

(i.e. using Ellipsis instead of pass, so that it's clear that it's not an
empty function but one the implementation of which is hidden)



IMHO, if there is no straight forward way currently to convert a function
header from a code blob into a Signature object in Python code, preferably
using the ast module (either explicitly or implicitly through inspect.py),
then that's a bug.



Is sounds simpler to me to just make it a Python syntax feature. Or at
least an optional one, supported by the ast module with a dedicated
compiler flag.

Stefan





----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Mar 18, 2013 at 11:08 PM, Stefan Behnel <stefan_ml at behnel.de> wrote:

We're mildly concerned about the possible impact on the size of the
ever-growing CPython binaries. However, it turns out that this is a
case where readability and brevity are allies rather than enemies, so
we don't need to choose one or the other.


I like this notion. The groups notation and '/' will still cause the
parser to choke and require special handling, but OTOH, they have
deliberately been chosen as potentially acceptable notations for
providing the same features in actual Python function declarations.


The complexity here is that Larry would like to limit the annotations
to compatibility with ast.literal_eval. If we drop that restriction,
then the inspect module could handle the task directly. Given the
complexity of implementing it, I believe the restriction needs more
justification than is currently included in the PEP.


Agreed. Guido had previously decided "not worth the hassle", but this
may be enough to make him change his mind. Also, Larry's "simple"
solution here isn't enough, since it doesn't handle optional groups
correctly.

While the support still has some odd limitations under the covers, I
think an explicit compiler flag is a good compromise between a lot of
custom hacks and exposing an unfinished implementation of a new
language feature.

Cheers,
Nick.




-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 19.03.13 06:45, Larry Hastings wrote:

Strip parenthesis and it will be only 21 bytes long.


It will be simpler to use some one-character separator which shouldn't 
be used unquoted in the signature. I.e. LF.




----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 19 Mar, 2013, at 10:24, Larry Hastings <larry at hastings.org> wrote:

You could also add the slash to the start of the signature, for example "/(arg1, arg2)", that way the positional only can be detected without trying to parse it first and removing a slash at the start is easier than removing it somewhere along a signature with arbitrary default values, such as "(arg1='/', arg2=4 /) -> 'arg1/arg2'".  The disadvantage is that you can't specify that only some of the arguments are positional-only, but that's not supported by PyArg_Parse... anyway.

Ronald




----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Tue, 19 Mar 2013 03:00:45 -0700,
Larry Hastings <larry at hastings.org> a ?crit :

Agreed with Larry.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Rough idea for adding introspection information
	for builtins
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Mar 19, 2013 at 3:00 AM, Larry Hastings <larry at hastings.org> wrote:

Also, we can already easily produce the extended form through:

    "def {}{}:\n    ...".format(f.__name__, inspect.signature(f))

So, agreed, capturing just the signature info is fine.


IIRC, I was arguing against allowing *pickle* because you can't audit
that just by looking at the generated source code. OTOH, I'm a big fan
of locking this kind of thing down by default and letting people make
the case for additional permissiveness, so I agree it's best to start
with literals only.

Here's a thought, though: instead of doing an Argument Clinic specific
hack, let's instead design a proper whitelist API for ast.literal_eval
that lets you accept additional constructs.

As a general sketch, the long if/elif chain in ast.literal_eval could
be replaced by:

    for converter in converters:
        ok, converted = converter(node)
        if ok:
            return converted
    raise ValueError('malformed node or string: ' + repr(node))

The _convert function would need to be lifted out and made public as
"ast.convert_node", so conversion functions could recurse
appropriately.

Both ast.literal_eval and ast.convert_node would accept a keyword-only
"allow" parameter that accepted an iterable of callables that return a
2-tuple to whitelist additional expressions beyond those normally
allowed. So, assuming we don't add it by default, you could allow
empty sets by doing:

    _empty_set = ast.dump(ast.parse("set()").body[0].value)
    def convert_empty_set(node):
        if ast.dump(node) == _empty_set:
            return True, set()
        return False, None

    ast.literal_eval(some_str, allow=[convert_empy_set])

This is quite powerful as a general tool to allow constrained
execution, since it could be used to whitelist builtins that accept
parameters, as well as to process class and function header lines
without executing their bodies. In the case of Argument Clinic, that
would mean writing a converter for the FunctionDef node.


Agreed on both points, but this should be articulated in the PEP.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia

