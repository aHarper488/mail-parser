
============================================================================
Subject: [Python-Dev] Unload a module written in C
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

I am trying to understand why I am unable to unload my faulthandler
module (implemented in C). Antoine pointed me
_PyImport_FixupExtensionObject() comment which gave me a first clue:

   Modules which do support multiple initialization set their m_size
   field to a non-negative number (indicating the size of the
   module-specific state). They are still recorded in the extensions
   dictionary, to avoid loading shared libraries twice.

Ok, so I changed the size from -1 to 0, and so the m_free callback was
called at exit. Nice. This is thanks to PyImport_Cleanup() which clears
my module attributes.

--

But if I do

  import faulthandler
  del sys.modules['faulthandler']
  del faulthandler

the module is never unloaded. There is another secret reference in the
interpreter state: state->modules_by_index. This list is cleared at exit
(by PyInterpreterState_Clear), but not my module attributes, and some of
them are functions pointing to the module.

My module attribute are not cleared at exit because PyImport_Cleanup()
clears only modules from sys.modules, and my module is no more
referenced in sys.modules.

The workaround to unload the module is to explicitly clear its
attributes:

  import faulthandler
  del sys.modules['faulthandler']
  faulthandler.__dict__.clear()
  del faulthandler

--

Is there a bug somewhere, or do I misunderstood something important?

Note: I implemented m_traversal, but it is not revelant here (you can
consider that my module only contains functions).

Victor




----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Module unloading is simply not implemented, and would be very difficult
to implement.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
"Martin v. L?wis", 25.03.2011 07:59:

Are you saying that because objects instantiated from a module do not 
normally keep a reference to it to keep it alive?

Stefan




----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le vendredi 25 mars 2011 ? 10:24 +0100, Stefan Behnel a ?crit :

If you are right, I can understand that it is not possible to unload
safely a module.  In my case, faulthandler module doesn't create an
object, and it has a function to unload itself safely.

Victor




----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le vendredi 25 mars 2011 ? 07:59 +0100, "Martin v. L?wis" a ?crit :

My problem is that if Python is embeded, my module will still be active
after Py_FinalizeEx(). For example, if it installed an handler for the
SIGSEGV signal: a segmentation fault will call the handler which will
try to get the interpreter state, but there is no more interpreter. I
don't know if it is a problem or not, but I would prefer to cleanup my
module on Py_FinalizeEx().

I can try to ensure that Python does restore all signals installed by
faulthandler and cancel the current alarm() (used by
dump_traceback_later()). Or I can hardcode something to unload
faulthandler in Py_FinalizeEx() (or somewhere else).

Victor




----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 25, 2011 at 8:14 PM, Victor Stinner
<victor.stinner at haypocalc.com> wrote:

And registering your cleanup function with atexit() isn't enough? Or
does that remove the handler too early?

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le vendredi 25 mars 2011 ? 21:14 +1000, Nick Coghlan a ?crit :

atexit() is too late: when Python is embeded, Py_Finalize() may be
called a long time before the program does really finish.

Well, I never embeded Python in another program, but it looks like some
people do initialize/finalize Python more than once:

http://bugs.python.org/issue11321
"9th import of module _pickle always crashes"

In this issue, Python is initialized/finalized 20 times.

Victor




----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 25, 2011 at 9:36 PM, Victor Stinner
<victor.stinner at haypocalc.com> wrote:

I'm talking about the Python "atexit" module - any callbacks
registered there are invoked by Py_Finalize(), not by process
termination.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 25.03.2011 11:14, schrieb Victor Stinner:

Ah. If that's the case, it's a bug.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Unload a module written in C
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 25.03.2011 10:24, schrieb Stefan Behnel:

No, I'm saying that there is no support whatsoever to unload the shared
library of the extension module when the extension module would no
longer be used.

Regards,
Martin

