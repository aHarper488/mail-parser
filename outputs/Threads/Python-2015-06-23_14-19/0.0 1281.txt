
============================================================================
Subject: [Python-Dev] [Python-checkins] r85101 - in
 python/branches/py3k/Doc/library: http.client.rst urllib.request.rst
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r85101 - in
 python/branches/py3k/Doc/library: http.client.rst urllib.request.rst
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
On 9/29/2010 7:24 AM, antoine.pitrou wrote:


That wording is a little awkward. How about: "When opening HTTPS (or 
FTPS) URLs, no attempt is made to validate the server certificate. Use 
at your own risk."

