
============================================================================
Subject: [Python-Dev] Some joker is trying to unsubscribe me
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Some joker is trying to unsubscribe me
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
I've received three messages in the past hour from mailman at
python.org notifying me of various attempts to receive a password
reminder or to remove me from python-dev. I hope they don't succeed.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Some joker is trying to unsubscribe me
----------------------------------------
Author: Thomas Wouter
Attributes: []Content: 
On Wed, Oct 24, 2012 at 9:19 PM, Guido van Rossum <guido at python.org> wrote:



Are you asking us to CC you on all messages? I'm sure it could be arranged
:>

-- 
Thomas Wouters <thomas at python.org>

Hi! I'm a .signature virus! copy me into your .signature file to help me
spread!
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121025/6cc494f7/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Some joker is trying to unsubscribe me
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
2012/10/24 Guido van Rossum <guido at python.org>:

That's probably because most of your replies contain the full message,
which contains the "Unsubscribe" link, and some stupid bot followed it.

See the one below for my account. Please don't click it!


-- 
Amaury Forgeot d'Arc



----------------------------------------
Subject:
[Python-Dev] Some joker is trying to unsubscribe me
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Thu, Oct 25, 2012 at 2:59 AM, Amaury Forgeot d'Arc
<amauryfa at gmail.com> wrote:

Or, more likely, someone *else* posted that link in their quoting of
my message. Good hypothesis!

-- 
--Guido van Rossum (python.org/~guido)

