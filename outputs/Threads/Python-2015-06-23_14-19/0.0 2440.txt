
============================================================================
Subject: [Python-Dev] cpython: PyUnicode_Join() checks output length in
	debug mode
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: PyUnicode_Join() checks output length in
	debug mode
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 10/03/11 23:35, victor.stinner wrote:

I don't understand this change. Why would you not always add "copied" once you
already have it? It seems to be the more correct version anyway.

Georg


