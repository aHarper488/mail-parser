
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 6, 2011 at 10:01 AM, victor.stinner
<python-checkins at python.org> wrote:

While these cases are legitimately changed to 'null' (since they're
lowercase descriptions of the character), I figure it's worth
mentioning again that the ASCII name for '\0' actually *is* NUL (i.e.
only one 'L'). Strange, but true [1].

Cheers,
Nick.

[1] https://secure.wikimedia.org/wikipedia/en/wiki/ASCII

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le 06/09/2011 02:25, Nick Coghlan a ?crit :

"NUL" is an abbreviation used in tables when you don't have enough space 
to write the full name: "null character".

Where do you want to mention this abbreviation?

Victor



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 6, 2011 at 6:04 PM, Victor Stinner
<victor.stinner at haypocalc.com> wrote:

Yep, fair description.


Sorry, I meant worth mentioning on the list, not anywhere particular
in the docs  - the topic came up recently when an instance of NUL was
incorrectly changed to read 'NULL' instead and it took me a moment to
figure out why the same reasoning *didn't* apply in this case.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 09/06/2011 04:04 AM, Victor Stinner wrote:

FWIW, the RFC 20 (the ASCII spec) really really defines 'NUL'  as the
*name* of the \0 character, not just an "abbreviation used in tables":

 http://tools.ietf.org/html/rfc20#section-5.2



Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk5mODcACgkQ+gerLs4ltQ7VwACgicaURzX4wAWOi+sRYGBwF5/3
8okAniSkHIlBv/VoibW6klR3WgD8T3ph
=LlKo
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 9/6/2011 11:11 AM, Tres Seaver wrote:


As I read the text, the 2 or 3 capital letter *symbols* are 
abbreviations of of the names. Looking back up, I see
'''
4. Legend
4.1 Control Characters
    NUL Null                                DLE Data Link Escape (CC)
...
4.2 Graphic Characters
    Column/Row  Symbol      Name
    2/0         SP          Space (Normally Non-Printing)
    2/1         !           Exclamation Point
'''
'NUL' and 'SP' are *symbols* that have the names 'Null' and 'Space', 
just as the symbol '!' is named 'Exclamation Point'. They just happen to 
be digraphs and trigraphs composed of 2 or 3 characters.

I am sure that the symbol SP does not appear in the docs. The symbol 
'LF' (for LineFeed) probably does not either. We just call it 'newline' 
or 'newline character' as that is how we use it.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Victor Stinner wrote:


It's also the official name of the character, for when you want
to be unambiguous about what you mean (e.g. "null character" as
opposed to "empty string" or "null pointer").

I expect it's 3 chars for consistency with all the other control
character names.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Fix
 PyUnicode_AsWideCharString() doc: size doesn't contain the null character
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 06.09.2011 10:04, schrieb Victor Stinner:

I vote to paint the bikeshed BLU.

Georg

(Seriously, how many more messages will this triviality spawn?)


