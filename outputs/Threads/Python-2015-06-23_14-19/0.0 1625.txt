
============================================================================
Subject: [Python-Dev]  Strange error importing a Pickle from 2.7 to 3.2
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]  Strange error importing a Pickle from 2.7 to 3.2
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Jesus Cea writes:

 > PPS: If there is consensus that this is a real bug, I would create an
 > issue in the tracker and try to get a minimal testcase.

All bugs are issues, but not all issues are bugs.

Please don't wait for consensus or even a second opinion to file the
issue.

It's reasonable for a new Python user to ask whether something is a
bug or not, but if somebody with your experience and contribution
level to Python doesn't understand something, at the very least we
have to suspect a doc bug.  So please file the issue to ensure that
something will be done to address the issue, and even if it turns out
to be some misunderstanding that only you are ever likely to
make<wink>, at least the issue will remain as documentation if
somebody else has the same misunderstanding.

OTOH, the testcase might require a lot of effort on your part.  Of
course it's reasonable for you to check whether it's a simple
misunderstanding before exerting that effort.

