
============================================================================
Subject: [Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
The devguide's recommendation is to "forward-port" changes withing a major
release line, i.e. if I need something in all 3.[123], then start with 3.1
and forward-port (by "hg merge <branch>") to 3.2 and then 3.3

Just to clarify - does this mean that all changesets that are applied to 3.2
eventually get to 3.3 as well? Since we just do "hg merge 3.2" in the 3.3
clone, I guess this must be true. But then what happens if there's a change
in 3.2 that shouldn't get into 3.3? (say a bug fix for a feature that has
disappeared)?

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110312/b463c9bc/attachment.html>



----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: Nadeem Vawd
Attributes: []Content: 
On Sat, Mar 12, 2011 at 9:32 AM, Eli Bendersky <eliben at gmail.com> wrote:

The "Note:" box two paragraphs down from
<http://docs.python.org/devguide/committing.html#porting-within-a-major-version>
explains how prevent a changeset from propagating: you do a dummy merge
where you revert the changes before committing. This marks the changeset from
3.2 as having been merged into default/3.3 without actually applying
the changes.

Nadeem



----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: Nadeem Vawd
Attributes: []Content: 
Hmm... it seems that the given instructions don't actually work. "hg
revert -a" fails,
saying that a specific revision needs to be provided. The command should be
"hg revert -ar default".



----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 12.03.11 04:03, schrieb Nadeem Vawda:

Isn't that command correct only if you are merging into default?
ISTM that it should be "hg revert -ar <targetbranch>".

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: Nadeem Vawd
Attributes: []Content: 
On Sat, Mar 12, 2011 at 1:29 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

In general, yes. However, the existing text refers specifically to the
case of merging 3.2
into default, so I was trying to be consistent with that.

The current text, after my proposed change, will be:

hg update default
hg merge 3.2
hg revert -ar default
hg commit

For full generality, you might want to say:

hg update <targetbranch>
hg merge <sourcebranch>
hg revert -ar <targetbranch>
hg commit

However, the entire section on forward-porting is framed in terms of
concrete examples
(and that specific subsection refers explicitly to the "3.2 ->
default" case). The existing
text makes sense to me, so I don't see any need to change this.

Regards,
Nadeem



----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 13 Mar 2011 00:24:07 +0200
Nadeem Vawda <nadeem.vawda at gmail.com> wrote:

Thanks for pointing this out. I've fixed the error.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

You can?t combine the -r option with other options.  (Yes, it?s a known
bug.)

On an unrelated note, you can use ?-r .? to tell Mercurial to find the
branch name from the working directory instead of having to remember and
retype it.

Regards



----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: Nadeem Vawd
Attributes: []Content: 
On Sun, Mar 13, 2011 at 12:43 AM, ?ric Araujo <merwok at netwok.org> wrote:

It seems to work for me (Mercurial 1.6.3 on Ubuntu). But I suppose it
wouldn't hurt
to split the options up.

Regards,
Nadeem



----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 12 Mar 2011 23:43:52 +0100
?ric Araujo <merwok at netwok.org> wrote:

Are you sure? It just worked here:

$ hg rev -ar default
reverting README

(perhaps you're thinking of -R instead?)


Does it work with merges?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] forward-porting from 3.1 to 3.2 to 3.3
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

Exactly.


It does.  hg help revs:

    The reserved name "." indicates the working directory parent. If no
    working directory is checked out, it is equivalent to null. If an
    uncommitted merge is in progress, "." is the revision of the first
    parent.

Gotta love consistency, DTRT, Just Working and all that :)

Regards

