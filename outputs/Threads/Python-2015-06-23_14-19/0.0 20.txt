
============================================================================
Subject: [Python-Dev] Python and compilers
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: willian at ufpa.b
Attributes: []Content: 
First, thank you for all opnion. Each one was considered.
I think the better question would be:
I have to develop a project that involves compilers, and being a fan of
Python, I thought about making a compiler for it (most basic idea involving
Pythin and compilers). But I saw that I can use what I learned from
compilers not only to create a compiler. What is the area of developing the
Python interpreter that I could build my project, and please give me
interesting ideas for the project.



----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 06/04/2010 12:44, willian at ufpa.br wrote:

Well, you are now thoroughly off topic for the python-dev mailing list - 
so I suggest you ask your question on comp.lang.python or some other 
Python list.

*However*, a project that would be interesting - and that I have wanted 
to do in order to program microcontrollers with *very* small memory 
address spaces [1] - would be to compile a static subset of Python down 
to C. You would need to do type inferencing and support only a basic 
minimum of the built-in types (and in fact I would start with perhaps 
functions only), but it could be fun. It would not be Python however, 
merely Python inspired.

Alternatively a general type-inferencing algorithm for Python, such as 
the one used by ShedSkin, could be interesting.

All the best,

Michael


[1] Smaller devices than those targetted by 
http://code.google.com/p/python-on-a-chip/


-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Michael Foord wrote:


That would be an excellent project -- if the result were
successful, I'd be interested in using it!

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
willian at ufpa.br wrote:

I don't think the question is necessarily off-topic.

I can propose two projects, related to Python core:

- 2to3 pattern compiler: 2to3 currently uses an interpreter for pattern
  matching. It does "compile" the patterns into some intermediate form,
  however, that is actually interpreted with an interpreter written in
  Python (actually, it is self-interpreted). It might be interesting to
  compile the pattern into actual Python code, with the hope of it
  executing faster than it does now (and yes, I proposed a similar, but
  different GSoC topic also; the two approaches are orthogonal, though).

- IDLE code completion. Currently, IDLE has some form of code
  completion, which is fairly limited. It might be useful to produce a
  better code completion library, one that works more statically and
  less based on introspection. In particular, optimistic type inference
  might help (optimistic in the sense "if foo has a method .isalpha, it
  probably is a string). In code completion, exact type inference isn't
  necessary; giving a superset (i.e. a union type) might still be
  helpful.

In addition, to-python compilers may also be interesting in various HTML
templating languages, e.g. Django templating, Zope page templates, and
the like (although some of them already have compilers of some form on
their own).

HTH,
Martin



----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 06/04/2010 23:31, "Martin v. L?wis" wrote:

This would be very useful to many Python IDE projects. Getting it right 
is one thing, getting it fast enough to be useful is another (i.e. it is 
a difficult problem) - but yes, could be both interesting and useful.

Michael




-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 06/04/2010 23:34, Michael Foord wrote:

A good basis for a project like would be PySmell (by Orestis Markou), 
which intended to provide this but was never completed:

     http://code.google.com/p/pysmell/

It would make a great gsoc project as well.

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Greg Ewing wrote:

I thought RPython already supported this? (admittedly, my knowledge of
of the inner workings of PyPy is fairly sketchy, but I thought static
compilation of RPython to a variety of backend targets was one of the
key building blocks)

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Nick Coghlan wrote:


Maybe so, but one would still have to create the appropriate
backend to target the machine in question. I wouldn't like
to rely on a generic C-generating backend to target something
very tiny in an effective way.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Python and compilers
----------------------------------------
Author: =?UTF-8?Q?Willian_Sodr=C3=A9_da_Paix=C3=A3o?
Attributes: []Content: 
Thank you all. My decision at the moment is a compiler for PICs to
accept a language more like python as possible instead of C.
How is a college project "solo", I can guarantee at least the first
part, a lexical analyzer.

Ass: Wil ('.')

