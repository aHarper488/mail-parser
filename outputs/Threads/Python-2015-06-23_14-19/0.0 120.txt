
============================================================================
Subject: [Python-Dev] Frequency of the dev docs autobuild
Post Count: 11
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Does the online dev version of the docs build in response to docs
checkins, or just once a day?

(And is that written down somewhere and I've just forgotten where to
look...)

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 29 Apr 2010 20:16:14 +1000, Nick Coghlan <ncoghlan at gmail.com> wrote:

I believe it does it once a day.  Georg recently changed how this is done,
so we should get official word from him.


I don't think so, but it should be.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 29.04.2010 13:40, schrieb R. David Murray:

Yes, it builds once a day.  The build process currently takes quite long,
since PDFs are also built every time.  We could change the procedure to
rebuild the HTML more frequently or on checkin, but that would again
make the PDFs mismatch the online docs, and I would say it's not necessary.


It is documented at <http://www.python.org/dev/doc>.  The docs themselves
bear the "last updated" date only.

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.




----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sat, 01 May 2010 11:35:04 +0200, Georg Brandl wrote:

Unless I'm missing something, I don't see any docs there about the
automated build process and when and where it runs.

(Note that the old automated build process, the build.sh script, wasn't
documented in that sense anywhere that I know of, either.  And I can
never remember where its web status page is...at least for the
new docs procedure we have the last build date in an obvious place :)

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 1 May 2010 15:28, R. David Murray <rdmurray at bitdance.com> wrote:

I assume Georg was referring to "Development versions of the Python
documentation are available on-line and updated daily".

Paul.



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sat, 01 May 2010 16:18:19 +0100, Paul Moore <p.f.moore at gmail.com> wrote:

Yes, most likely.  I think I am asking for more documentation than you
were :)

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
R. David Murray wrote:

Then I think you have to propose specific wording. It runs on
www.python.org (why would you expect it to run elsewhere???),
and what about "daily" do you consider imprecise? Do you really need
exact hour, minute, and second? What for?

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Paul Moore wrote:

Thanks, that's what I was missing.

Cheers,
Nick.


-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 02 May 2010 00:44:22 +0200, =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?= <martin at v.loewis.de> wrote:

I had in fact overlooked the indicated sentence when reading the page.

It's not that I expect it to run somewhere else, but that it did indeed
used to run somewhere else (if I'm reading the build.sh script correctly),
so I think it is worth making it explicit.  Also note that it isn't
obvious (without resolving the host names) that docs.python.org and
www.python.org are the same machine.


I wasn't asking for more precision than daily (I just hadn't seen it), but
now that I think about it it would indeed be nice to know when the cron
job starts, so that we know that if the checkin didn't happen before then,
it won't show up in the online docs until the next day.  I don't think
this is particularly *important* to know, it would just be nice to know.

Is it only the development versions that get updated, or do our updates
to the next bug fix release also get posted daily (and those are the docs
web site visitors normally see, I believe)?  I was under the impression
that it was the latter, but that Georg had also changed things so that
a reference to a particular dot release got the snapshot of the docs
that were shipped with that dot release.  This impression seems to be
confirmed by examination of the various pages involved.

To answer your question about what wording I'd like, I think that it would
be worthwhile to say somewhere (not necessarily on that page...maybe in
the doc README.txt?...but it could be ont that page...) that the docs are
auto-built by a cron job on the server hosting docs.python.org running
'make dist' in the doc directory of a freshly updated checkout and
then....doing something with the resulting files?  Or maybe the apache
URLs point right at the dist directory in that autobuild checkout?
Anyway, exactly how it all works is what I would like to see documented.

Background: one of my tasks for one of my customers is helping them try to
make it so that an outsider coming in could learn everything they needed
to know to operate the system from the available docs...a goal that they
are nowhere near achieving, but which I think is a worthwhile goal for
which to strive.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

That's different from asking that it be documented, though. I don't mind
you knowing (it's at 15:00 local time for the build machine, which sits
in the Europe/Amsterdam timezone). Just ask specific questions, and
people may give specific answers. Now that you know, I still don't think
it needs to be documented (else: where would that end? Would you want to
know account name and uid of the build also, and the partition of the
hard disk where the files are stored? Not even I know the rack slot in
which the machine sits).


That's what the documentation claims, yes. The build script currently
has these targets:

BRANCHES = [
    # checkout, target, isdev
    (BUILDROOT + '/python26', WWWROOT, False),
    (BUILDROOT + '/python31', WWWROOT + '/py3k', False),
    (BUILDROOT + '/python27', WWWROOT + '/dev', True),
    (BUILDROOT + '/python32', WWWROOT + '/dev/py3k', True),
]


Actually, the command is rather like this:

  'cd Doc; make autobuild-%s' % (isdev and 'dev' or 'stable')


For this kind of work, I think looking at the actual installation is
more productive to learn how things are done (perhaps opposed to how
they were originally planned to be done). I didn't know how this all
worked myself, either, but, using the root account, it took me only a
minute to find out - much faster than finding the documentation that may
have explained it in detail.

The only starting point that you need is the machine that you know it
runs on.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Frequency of the dev docs autobuild
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 02.05.2010 22:21, schrieb R. David Murray:

And I realize it's less than you wanted to see :)

But I also realized it's very easy to give more detail without crowding
that page; since the script used for the daily build is in SVN under
Doc/tools/dailybuild.py; and that also contains the hostname.

I've updated the dev/doc/ page to include a link to the script text.

cheers,
Georg


