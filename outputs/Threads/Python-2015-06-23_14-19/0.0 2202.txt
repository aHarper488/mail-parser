
============================================================================
Subject: [Python-Dev] GSoC idea: PEP process enhancements (Was: [issue7284]
 argparse - display version in usage by default)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] GSoC idea: PEP process enhancements (Was: [issue7284]
 argparse - display version in usage by default)
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Sat, Mar 26, 2011 at 5:14 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

What if we could accompany every PEP draft with series of use
cases/user stories? I don't know how many people actually reviewed
this particular PEP 389, and that's bad, because without this info it
is hard to say if there were enough reviews from future users.

The process is also complicated, because you can't comment on the
document inline. As a Python user I can spend 15 minutes on reading
PEP, but I can't spend hour composing email copy/pasting text for
commenting. For example, if the paragraph about "why aren't getopt and
optparse enough" [1] was reduced to just :
- no support for positional arguments
- no variable number of arguments
- subcommands

Perhaps then I could see that my simple use case to make output
formatting more flexible and concise is not covered by this PEP.

[1] http://www.python.org/dev/peps/pep-0389/#why-aren-t-getopt-and-optparse-enough


Actually, if we could bring this issue earlier, my request would be to
replace a bunch of parameters in constructor with a simple template.
But by the time I was able to get back to making a letter about this
proposal, argparse is already released and Python 3.2 is outside the
hangar too.

E.g. instead of:
argparse.ArgumentParser([description][, epilog][, prog][, usage][,
add_help][, argument_default][, parents][, prefix_chars][,
conflict_handler][, formatter_class])

use
argparse.Parser([help][, argument_default][, parents][, chars][,
conflict_handler][, formatter][, help_vars])
  help_vars = dict([prolog][, description][, epilog][, prog][,
progname][, usage][, version])
and strip HelpFormatter from formatters (they're already inherited)
  RawDescriptionHelpFormatter    -> RawDescription
  RawTextHelpFormatter              -> RawText
  ArgumentDefaultsHelpFormatter -> ArgumentDefaults

so that the final "pythonic" invocation syntax would be:

parser = argparse.Parser(help=True, formatter=argparse.RawText,
                 template=""""
%progname

%description

%help
""",
                 help_vars=dict(progname="mA keWl UtIL",
version=__version__, description=__doc__))


Actually it doesn't look as more flexible to me, but the idea is to
provide more variants for consideration and make sure as much people
as possible reviewed them.

That's why I'd like to propose to develop this system for a GSoC
project. I can mentor its development with some support from
interested people around.
--
anatoly t.

