
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Close #14205: dict
	lookup raises a RuntimeError if the dict is modified during
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Close #14205: dict
	lookup raises a RuntimeError if the dict is modified during
----------------------------------------
Author: Yury Selivano
Attributes: []Content: 
Actually, I too noticed that you've dropped few crasher tests.  I think
we need to keep them, to make sure that future development will not 
introduce the same vulnerabilities.  That's a common practice with
unit-testing.

On 2012-03-09, at 5:27 PM, Victor Stinner wrote:





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Close #14205: dict
	lookup raises a RuntimeError if the dict is modified during
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On Fri, Mar 09, 2012 at 05:38:05PM -0500, Yury Selivanov wrote:

The term for this is "regression testing" -- when you fix a bug, you 
keep a test for that bug forever, to ensure that you never have a 
regression that re-introduces the bug.

-- 
Steven


