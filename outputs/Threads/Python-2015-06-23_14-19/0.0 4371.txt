
============================================================================
Subject: [Python-Dev] PEP 435 (Enums) is Accepted
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 435 (Enums) is Accepted
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
I have reviewed the latest version of PEP 435 and I see that it is
very good. I hereby declare PEP 435 as Accepted. Congratulations go to
Barry, Eli and Ethan for pulling it through one of the most thorough
reviewing and bikeshedding processes any PEP has seen. Thanks to
everyone else for the many review comments. It is a better PEP because
of the reviews.

Barry or Eli, you can update the PEP's status. (I also expect there
will be some copy-editing still.)

Ethan: the stdlib implementation should probably be assigned a bug
tracker issue (if there isn't one already) and start code review now.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] PEP 435 (Enums) is Accepted
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 09, 2013, at 04:01 PM, Guido van Rossum wrote:


Let me echo Guido's thanks to everyone on python-dev and python-ideas, and
especially Eli and Ethan.  Our ability to come together and produce agreement
on not only a contentious, but long wished for, feature shows off the best of
our community.  Huge thanks also to Guido for the invaluable pronouncements
along the way.

I can honestly say I'm happy with the results, and the experience of
participating.

Great work everyone.
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130509/73666ba8/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 435 (Enums) is Accepted
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, May 10, 2013 at 9:01 AM, Guido van Rossum <guido at python.org> wrote:

And there was much rejoicing, huzzah! :)

As an added bonus, people trying to understand the details of
metaclasses will now have a non-trivial standard library example to
investigate (we may want to update the language reference at some
point to highlight that).

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 435 (Enums) is Accepted
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
On 05/09/2013 11:46 PM, Nick Coghlan wrote:

Hmmm... __prepare__ really isn't doing very much at the moment... I could have it do more... maybe create some kind of 
little helper class, name it the same as the class being created, and stuff it in the class dict... ;)

--
~Ethan~

