
============================================================================
Subject: [Python-Dev] [Python-checkins] peps: Added dynamic path
 computation rationale, specification, and discussion.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Added dynamic path
 computation rationale, specification, and discussion.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, May 23, 2012 at 10:35 AM, eric.smith <python-checkins at python.org> wrote:

I'd phrase this as something like "import an encoding from an
``encodings`` portion". You don't really import namespace package
portions directly - you import the modules and packages they contain.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

