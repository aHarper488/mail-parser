
============================================================================
Subject: [Python-Dev] PPC Leopard buildbot failing sqlite test for
	Python 3.2
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PPC Leopard buildbot failing sqlite test for
	Python 3.2
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sat, 16 Oct 2010 13:14:20 -0700, Bill Janssen <janssen at parc.com> wrote:

That's what I was wondering (I see you found the relevant issue...),
but it doesn't *seem* to be a version issue, but it would be
cool if you could check it against 3.7 just for confirmation.


It might be, except for two things: it passes on the Sparc Solaris 10
buildbot, and the io module also makes use of a T_BOOL field and
its tests pass on all buildbots.  On the flip side, the only other
failure I've see was on the Debian Sparc buildbot.  Which claims
it is running sqlite 3.7.2-1.  It works for me using 3.7.2 on Gentoo
i86.


Good question :)

When I added this it seemed like such a simple thing.  Maybe the
endianness issue is the one to look at, despite the pass on Solaris.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] PPC Leopard buildbot failing sqlite test for
	Python 3.2
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
R. David Murray <rdmurray at bitdance.com> wrote:


For 3.x, the "SPARC Solaris gcc" buildbot says,

  [ 87/349] test_sqlite
  test_sqlite skipped -- No module named _sqlite3


The SPARC architecture supports either big-endian and little-endian use
(as does the PowerPC -- actually, I think any program can switch
endianness on the PowerPC, except...); the OS gets to decide.  I'd
expect Solaris on SPARC to run big-endian, like OS X on PPC, and
Wikipedia says Ubuntu on SPARC runs big-endian, too.

Except: PPC Leopard is running on a G5 process, a PowerPC 970.  As far
as I know, that's the only PowerPC chip that couldn't run either-endian
-- it only runs big-endian.  So that code has to be running big-endian
on PPC Leopard.

Looks like it's not passing on any big-endian architecture.

Bill

