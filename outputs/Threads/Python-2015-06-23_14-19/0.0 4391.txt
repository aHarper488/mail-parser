
============================================================================
Subject: [Python-Dev] [RELEASED] Python 2.7.5
Post Count: 16
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
It is my greatest pleasure to announce the release of Python 2.7.5.

2.7.5 is the latest maintenance release in the Python 2.7 series. You may be
surprised to hear from me so soon, as Python 2.7.4 was released slightly more
than a month ago. As it turns out, 2.7.4 had several regressions and
incompatibilities with 2.7.3. Among them were regressions in the zipfile, gzip,
and logging modules. 2.7.5 fixes these. In addition, a data file for testing in
the 2.7.4 tarballs and binaries aroused the suspicion of some virus
checkers. The 2.7.5 release removes this file to resolve that issue.

For details, see the Misc/NEWS file in the distribution or view it at

    http://hg.python.org/cpython/file/ab05e7dd2788/Misc/NEWS

Downloads are at

    http://python.org/download/releases/2.7.5/

As always, please report bugs to

    http://bugs.python.org/

(Thank you to those who reported these bugs in 2.7.4.)

This is a production release.

Happy May,
Benjamin Peterson
2.7 Release Manager
(on behalf of all of Python 2.7's contributors)



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Carlos Nepomucen
Attributes: []Content: 
test_asynchat still hangs! What it does? Should I care?

----------------------------------------



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/5/15 Carlos Nepomuceno <carlosnepomuceno at outlook.com>:

Is there an issue filed for that?



--
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Carlos Nepomucen
Attributes: []Content: 
Just filed 17992!

http://bugs.python.org/issue17992

----------------------------------------



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Ben Hoy
Attributes: []Content: 
Thanks, Benjamin -- that's great!

This may not be a python-dev question exactly. But on Windows, is it safe
to update to 2.7.5 on top of 2.7.4 (at C:\Python27) using the .msi
installer? In other words, will it update/add/remove all the files
correctly? What if python.exe is running?

-Ben


On Thu, May 16, 2013 at 4:19 PM, Benjamin Peterson <benjamin at python.org>wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130516/a16e626b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Terry Jan Reed
Attributes: []Content: 
On 5/16/2013 1:18 AM, Ben Hoyt wrote:

Yes, I update all the time, but without python running.




----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Ben Hoy
Attributes: []Content: 
This may not be a python-dev question exactly. But on Windows, is it



Great to know -- thanks.

-Ben
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130516/51f0ad50/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Ben Hoy
Attributes: []Content: 

FYI, I tried this just now with Python 2.7.4 running, and the
installer nicely tells you that "some files that need to be updated
are currently in use ... the following applications are using files,
please close them and click Retry ... python.exe (Process Id: 5388)".

So you can't do it while python.exe is running, but at least it
notifies you and gives you the option to retry. Good work, whoever did
this installer.

-Ben



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 16.05.13 10:42, schrieb Ben Hoyt:


This specific feature is part of the MSI technology itself, so the honor
goes to Microsoft in this case. They also have an advanced feature where
the installer can tell the running application to terminate, and then
restart after installation (since Vista, IIRC). Unfortunately, this
doesn't apply to Python, as a "safe restart" is typically not feasible.

FWIW, I'm the one who put together the Python installer.

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Pierre Roulea
Attributes: []Content: 
Hi all,

I just installed Python 2.7.5 64-bit () on a Windows 7 64-bit OS computer.
 When I evaluate sys.maxint I don't get what I was expected.  I get this:

Python 2.7.5 (default, May 15 2013, 22:44:16) [MSC v.1500 64 bit (AMD64)]
on win32
Type "copyright", "credits" or "license()" for more information.
2147483647
'AMD64'
'AMD64'


 Should I not get a 64-bit integer maxint (9223372036854775807) for
sys.maxint ?

Or is there something I am missing here?

Thanks!

/ Pierre Rouleau


On Thu, May 16, 2013 at 6:23 AM, "Martin v. L?wis" <martin at v.loewis.de>wrote:




-- 
/Pierre
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130519/fecd2e5c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/5/19 Pierre Rouleau <prouleau001 at gmail.com>:

This is correct. sizeof(long) != sizeof(void *) on Win64, and size
Python int's are platform longs, you get the maxsize of a 32-bit int.
Check sys.maxsize for comparison.



--
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Pierre Roulea
Attributes: []Content: 
OK thanks, Benjamin,

you are correct sys.maxsize is 2*63-1 on it.

I was under the impression that Python was using int_64_t for the
implementation of Win64 based integers.  Most probably because I've sen
discussion on Python 64 bits and those post were most probably were in the
scope of some Unix-type platform.


Regards,


On Sun, May 19, 2013 at 6:56 PM, Benjamin Peterson <benjamin at python.org>wrote:




-- 
/Pierre
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130519/8aadcd43/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Pierre Roulea
Attributes: []Content: 
On that topic of bitness for 64-bit platforms, would it not be better for
CPython to be written such that it uses the same 64-bit strategy on all
64-bit platforms, regardless of the OS?

As it is now, Python running on 64-bit Windows behaves differently (in
terms of bits for the Python's integer) than it is behaving in other
platforms.  I assume that the Python C code is using the type 'long'
instead of something like the C99 int64_t.  Since Microsoft is using the
LLP64 model and everyone else is using the LP64, code using the C 'long'
type would mean something different on Windows than Unix-like platforms.
 Isn't that unfortunate?

Would it not be better to hide the difference at Python level?

Or is it done this way to allow existing C extension modules to work the
way they were and request Python code that depends on integer sizes to
check sys.maxint?

Also, I would imagine that the performance delta between a Windows 32-bit
Python versus 64-bit Python is not as big as it would be on a Unix
computer.  As far as I can se Python-64 bits on Windows 64-bit OS has a
larger address space and probably does not benefit from anything else. Has
anyone have data on this?

Thanks



-- 
/Pierre
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130519/ced349a7/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 19 May 2013 19:37:46 -0400
Pierre Rouleau <prouleau001 at gmail.com> wrote:


Well, it's Microsoft's choice. But from a Python point of view, which C
type a Python int maps to is of little relevance.

Moreover, the development version is 3.4, and in Python 3 the int
type is a variable-length integer type (sys.maxint doesn't exist
anymore). So this discussion is largely moot now.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Pierre Roulea
Attributes: []Content: 
On Sun, May 19, 2013 at 7:41 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:


Fair


Good to know.  Too bad there still are libraries not supporting Python 3.
 Thanks.





-- 
/Pierre
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130519/f51d3263/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.5
----------------------------------------
Author: Richard Oudker
Attributes: []Content: 
On 20/05/2013 12:47am, Pierre Rouleau wrote:

Even in Python 2, if the result of arithmetic on ints which would 
overflow, the result automatically gets promoted to a long integer which 
is variable-length.

     >>> 2**128
     340282366920938463463374607431768211456L
     >>> type(2), type(2**128)
     (<type 'int'>, <type 'long'>)

So the size of an int is pretty much irrelevant.

-- 
Richard


