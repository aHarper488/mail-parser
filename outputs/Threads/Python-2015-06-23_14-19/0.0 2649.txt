
============================================================================
Subject: [Python-Dev] [Python-checkins] peps: Note that ImportError will
 no longer be raised due to a missing __init__.py
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Note that ImportError will
 no longer be raised due to a missing __init__.py
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
It's actually an ImportWarning, not Error (or at least that's what I meant
on import-sig). If the module is eventually found then there is no error.

On Thu, Apr 19, 2012 at 18:56, eric.smith <python-checkins at python.org>wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120419/ab9e05df/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Note that ImportError will
 no longer be raised due to a missing __init__.py
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
On 4/19/2012 10:59 PM, Brett Cannon wrote:

My error. Fixed.

Eric.





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Note that ImportError will
 no longer be raised due to a missing __init__.py
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
On Thu, Apr 19, 2012 at 18:56, eric.smith wrote:


Given that there is no way to modify the __path__ of a namespace
package (short of restarting python?), *should* it be an error if
there is exactly one directory?

Or is that just a case of "other tools out there, didn't happen to
install them"?

-jJ



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Note that ImportError will
 no longer be raised due to a missing __init__.py
----------------------------------------
Author: Eric Smit
Attributes: []Content: 

Right. If I just install zope.interfaces and no other zope packages, that
shouldn't be an error.

Eric.




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] peps: Note that ImportError will
 no longer be raised due to a missing __init__.py
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Apr 25, 2012 at 2:56 AM, Jim Jewett <jimjjewett at gmail.com> wrote:

Or you installed all of them into the same directory (as distro
packages are likely to do).

Also, a namespace package __path__ is still just a list - quite
amenable to modification after creation. The only thing we're not
currently promising in PEP 420 is a programmatic interface to redo the
scan.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

