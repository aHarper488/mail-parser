
============================================================================
Subject: [Python-Dev] cpython: Avoid useless "++" at the end of functions
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Avoid useless "++" at the end of functions
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/26/2011 2:08 PM, Guido van Rossum wrote:


This explanation makes sense (more than Eric's version of perhaps the 
same thing ;-).

http://bugs.python.org/issue12188
"A condensed version of the above added to PEP 7 would help new 
developers see the usage as local idiom rather than style bug."

Terry J. Reedy



----------------------------------------
Subject:
[Python-Dev] cpython: Avoid useless "++" at the end of functions
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/5/26 Terry Reedy <tjreedy at udel.edu>:

I think a more general formulation would be: "Idiomatic code is more
important than making static analyzers happy."



-- 
Regards,
Benjamin

