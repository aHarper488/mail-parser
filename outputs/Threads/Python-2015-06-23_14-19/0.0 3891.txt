
============================================================================
Subject: [Python-Dev] test_bind_port and test_find_unused_port fail due
 to missing SO_REUSEPORT when building Python 3.3.2-r2 (from portage) on
 3.7.10-gentoo-r1 kernel
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] test_bind_port and test_find_unused_port fail due
 to missing SO_REUSEPORT when building Python 3.3.2-r2 (from portage) on
 3.7.10-gentoo-r1 kernel
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Hi Reuben,

Thanks for reporting this. I've had a similar report recently in the Tulip
tracker (http://code.google.com/p/tulip/issues/detail?id=89). But the
problem seems with the use of SO_REUSEPORT in the test.support package's
bind_port() helper.

This really belongs in the Python issue tracker (bugs.python.org) -- can
you submit a bug there? My hunch is that, because this seems to be a
relatively new feature, we should just catch and ignore the exception from
that specific call.

--Guido


On Tue, Dec 3, 2013 at 7:24 AM, Reuben Garrett <reubengarrett at gmail.com>wrote:



-- 
--Guido van Rossum (python.org/~guido)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20131203/4180bad3/attachment.html>



----------------------------------------
Subject:
[Python-Dev] test_bind_port and test_find_unused_port fail due
 to missing SO_REUSEPORT when building Python 3.3.2-r2 (from portage) on
 3.7.10-gentoo-r1 kernel
----------------------------------------
Author: Reuben Garret
Attributes: []Content: 
On Tue, Dec 3, 2013 at 9:44 AM, Guido van Rossum <guido at python.org> wrote:

With pleasure! I'm just glad to have reported a valid bug for once in my
life :b

you submit a bug there?

I've opened issue # 19901 [1] as requested.

should just catch and ignore the exception from that specific call.

I agree, although I sort of phrased it poorly in the bug.

Thank you again for your support (and sorry for the delayed response).
Python is cool :]

[1]: http://bugs.python.org/issue19901
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20131205/1da17d8b/attachment.html>

