
============================================================================
Subject: [Python-Dev] Push notification
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Push notification
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 7 Sep 2010 10:29:48 +0200
Dirkjan Ochtman <dirkjan at ochtman.nl> wrote:

Could push notification be added for the benchmarks repo?
I think the python-checkins list would be an appropriate recipient for
the e-mails (the repo has a low activity).

Thanks

Antoine.





----------------------------------------
Subject:
[Python-Dev] Push notification
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Tue, Sep 7, 2010 at 16:38, Antoine Pitrou <solipsis at pitrou.net> wrote:

Fine with me, if the list agrees.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Push notification
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Tue, Sep 7, 2010 at 10:38 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

+1


? -Fred

--
Fred L. Drake, Jr.? ? <fdrake at gmail.com>
"A storm broke loose in my mind."? --Albert Einstein

