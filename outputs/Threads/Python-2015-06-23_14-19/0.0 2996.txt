
============================================================================
Subject: [Python-Dev] Code reviews
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Code reviews
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 2 Jan 2012 14:44:49 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

Oh, by the way, this is also why I avoid arguing too much about style
in code reviews. There are two bad things which can happen:

- your advice conflicts with advice given by another reviewer (perhaps
  on another issue)
- the contributor feels drowned under tiresome requests for style
  fixes ("please indent continuation lines this way")

Both are potentially demotivating. A contributor can have his/her own
style if it doesn't adversely affect code quality.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Code reviews
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 01/02/2012 03:41 PM, Antoine Pitrou wrote:

Exactly. Especially for reviews of patches from non-core people, we
should exercise a lot of restraint: as the committers, I think we can be
expected to bite the sour bullet and apply our uniform style (such as
it is).

It is tiresome, if not downright disappointing, to get reviews that
are basically "nothing wrong, but please submit again with one more
empty line between the classes", and definitely not the way to
attract more contributors.

Georg




----------------------------------------
Subject:
[Python-Dev] Code reviews
----------------------------------------
Author: franci
Attributes: []Content: 
On 01/02/2012 06:35 PM, Georg Brandl wrote:
Hi to all member of this list,
I'm not a Python-Dev (only some very small patches over core-mentorship 
list.
Just my 2cents here).

I would try to relax this conflicts with a script that does the 
reformatting itself. If
that reformatting where part of the process itself do you thing that 
that would
be an issue anymore?

PS: I know that there?s a pep8 checker so it could be transformed into a 
reformatter
but I don't know if theres a pep7 checker (reformater)


Best regards!

francis





----------------------------------------
Subject:
[Python-Dev] Code reviews
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Mon, Jan 2, 2012 at 12:26, francis <francismb at email.de> wrote:

I don't think this is a problem to the point that it needs to be fixed
via automation. The code I write is the code I build and test, so I'd
rather not have some script that goes in and modifies it to some
accepted format, then have to go through the build/test dance again.



----------------------------------------
Subject:
[Python-Dev] Code reviews
----------------------------------------
Author: julien tayo
Attributes: []Content: 
@francis
Like indent ?
http://www.linuxmanpages.com/man1/indent.1.php

@brian


Well, it breaks committing since it adds non significative symbols,
therefore bloats the diffs.
But as far as I am concerned for using it a long time ago, it did not
break anything, it was pretty reliable.

my 2c * 0.1
-- 
jul



----------------------------------------
Subject:
[Python-Dev] Code reviews
----------------------------------------
Author: Francisco Martin Brugu
Attributes: []Content: 
On 01/02/2012 10:02 PM, julien tayon wrote:
Thank you, I wasn't aware of this one !



----------------------------------------
Subject:
[Python-Dev] Code reviews
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jan 02, 2012, at 06:35 PM, Georg Brandl wrote:


I think it's fine in a code review to point out where the submission misses
the important consistency points, but not to hold up merging the changes
because of that.  You want to educate and motivate so that the next submission
comes closer to our standards.  The core dev who commits the change can clean
up style issues.

-Barry

P.S. +1 for the change to PEP 7.

