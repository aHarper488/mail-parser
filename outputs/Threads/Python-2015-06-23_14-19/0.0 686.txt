
============================================================================
Subject: [Python-Dev] Deprecation warnings in Python 2.7
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Florent Xiclun
Attributes: []Content: 
Hello,

I would like to open a discussion on the meaning of deprecation warnings in 2.7.
I assume, but I may be wrong, that 2.7 will be the last version in the 2.x
branch.  With this assumption, we should not find many things deprecated in 2.7
final.

On the other hand, the list of py3k deprecation warnings is increasing a lot.
And more users will probably start to move from 2.7 to the 3.1 release.

While working on ticket #7092 and #7849, we started to discuss some proposals
to simplify the deprecation warnings for 2.7.

We discussed these 2 proposals:

 1) in 2.6 there's no distinction between py3k and normal deprecations: they
    share the same category, DeprecationWarning.  The "-3" switch enables the
    py3k DeprecationWarning and SyntaxWarning.
    Do we need to introduce a subclass Py3kDeprecationWarning in 2.7?  It will
    make it easier to separate both kinds of deprecations (2.x and 3.x), and
    to filter them.
    
 2) a different idea is to deprecate the "-3" switch and consider all py3k
    deprecations as normal deprecations.  They will be hidden by default
    thanks to #7309.  Since there's no future for the 2.x branch, it seems
    normal to consider the migration from 2.7 to 3.1 and show these warnings
    when the developer uses "-Wd" switch.

What do you expect as DeprecationWarning in 2.7?

I will post a list of the DeprecationWarnings in the python trunk, in a
followup message, for a review, and to help the discussion.

Best regards,

-- 
Florent Xicluna





----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Florent XICLUN
Attributes: []Content: 
2010/3/3 Florent Xicluna wrote:

Here is the list of the "Classic" DeprecationWarnings:
http://paste.pocoo.org/show/184931/

And the list of the "Py3k" DeprecationWarnings and SyntaxWarnings:
http://paste.pocoo.org/show/184932/

I expect most of the things in the first list to be removed in 2.7
instead of being only deprecated.

Then what is the best approach:
 1) to separate better the Py3k warnings giving them a specific
subclass Py3kDeprecationWarnings
 2) to merge both lists because we consider deprecation related to 3.1
(and newer) ?
 3) to keep things unchanged?


-- 
Florent



----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Mar 2, 2010 at 15:55, Florent Xicluna <florent.xicluna at gmail.com>wrote:


I like option 2; we are serious about trying to make 2.7 the last in the 2.x
series, which means if you care about deprecations then you are caring about
Python 3. So I say make -3 the equivalent of -Wd in Python 2.7

-Brett



-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100302/6a6a4370/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 


-1 for several reasons.  

1) We've advertised -3 as part of TheOneTrueWay(tm).  It's a core part of our transition story, so we should keep that as clean/consistent as possible.  Deprecating the -3 switch seems like shooting ourselves in the foot.

2) There is some chance that there will be a 2.8, so it isn't helpful to burn down our bridges.

ISTM, nothing is currently broken and in need of "fixing" in 2.7.
I don't see any advantage to conflating py3k warnings with other deprecations.


Raymond
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100302/98db9096/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Tue, 2 Mar 2010 17:28:11 -0800,
Raymond Hettinger <raymond.hettinger at gmail.com> a ?crit :

That's my feeling too. I think introducing a Py3kDeprecationWarning
subclass is the right thing to do.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/3/2 Antoine Pitrou <solipsis at pitrou.net>:

Not to bikeshed, but I think it should be called Py3DeprecationWarning
in light of the fact that Python 3 is no longer a mythical beast. :)



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Antoine Pitrou wrote:

Agreed for the reasons Raymond cited. As far as the subclass goes, I
agree that's a good idea to allow people to readily detect whether they
tripped over a normal deprecation or a Py3 only deprecation when running
with -3 enabled.

As far as naming goes, I agree with Py3kDeprecationWarning since the
flag to enable them is called sys.py3kwarning.

Cheers,
Nick.


-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Florent Xiclun
Attributes: []Content: 
2010/3/3 Raymond Hettinger <raymond.hettinger at gmail.com>:

Instead of deprecating the -3 switch, we can change it to a useful
alias (see below).


IMHO, the current deprecation and warning mechanism is not far from a
Rube Goldberg machine.
There's many options available, and it's not obvious both for the core
developer and for the end user.

On the python interpreter side, you have many options for the careful developer:
 -Wdefault: display all warnings
 -3:        display all warnings except ImportWarning and
PendingDeprecationWarning
 -Qwarn     does nothing if -Wd is omitted, else warn about 2/1 or 2L/1
 -Qwarnall  does nothing if -Wd is omitted, else warn about 2.0/1 or 2j/1
 -b/-bb     (undocumented), warns about u'' == bytearray('')
 -t/-tt     warns about tab inconsistencies

Why -Qwarn needs -Wd to show its warnings?
And you should know that -3 implies -Qwarn, but it still mask
PendingDeprecationWarnings.
So you will not see the PendingDeprecationWarning issued by
cgi.parse_qs or shutil module (for example).

At the same time, you could notice that the mhlib module yields a
non-py3k DeprecationWarning about its removal in 3.0.

And now you want to compare unicode with bytes (ok, it's a very bad
idea, but it may happen when you sort dictionary keys, for example):

~ $ ./python

False
__main__:1: UnicodeWarning: Unicode equal comparison failed to convert
both arguments to Unicode - interpreting them as being unequal
False

~ $ ./python -Wd -bb

__main__:1: BytesWarning: Comparison between bytearray and string
False
__main__:1: UnicodeWarning: Unicode equal comparison failed to convert
both arguments to Unicode - interpreting them as being unequal
False

Yeah, it may be confusing... "Comparison between bytearray and string"
is not really the correct message in 2.7. And why to keep these 2
warnings categories which are not used anywhere else in Python source
code?  The behavior should be consistent between these 2 warnings, and
they may share the same category. Why we don't use a RuntimeWarning
here?

I still support the simplification of the warnings mechanism:
 * -Qwarn/-Qwarnall should imply
    -Wdefault -Wignore::ImportWarning -Wignore::PendingDeprecationWarning
    (same as current -3 behavior)
 * BytesWarning should be replaced by UnicodeWarning or RuntimeWarning
(and -b deprecated)
 * -Wdefault should show all warnings (including py3k warnings)
 * -3 should show the PendingDeprecationWarning in addition to its
current behavior.
   (i.e. an alias for -Wdefault -Wignore::ImportWarning)

-- 
Florent



----------------------------------------
Subject:
[Python-Dev] Deprecation warnings in Python 2.7
----------------------------------------
Author: Florent Xiclun
Attributes: []Content: 
2010/3/3 Florent Xicluna <florent.xicluna at gmail.com>:

I see that the BytesWarning (and -b option) is a 3.x feature.
I don't see the reason to keep it in 2.x, though....

On the other side, UnicodeWarning is unused in 3.x, why we keep it
around? We may phase it out in 3.2?

-- 
Florent

