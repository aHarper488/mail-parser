
============================================================================
Subject: [Python-Dev] MSDN licenses available for python-dev
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
Hi python-dev,

The recent threads on builds/installers for Mac and Windows reminded me of
Steve Holden's push to get the python-dev team equipped via a connection
with the Microsoft Open Source Technology Center. The OSTC team provides
Microsoft Developer Network licenses to open source projects to assist them
in better supporting Windows.

I've talked with Steve (who passed the task to me) and the Microsoft folks,
and they are happy to provide more licenses if needed. If you are interested
in getting a copy of MSDN, please contact me off-list. I'll provide you with
a code that you'll put into their site, then around a week later you should
get your subscription.

The snippet below is taken from prior correspondence with the OSTC team in
regards to who can receive the licenses:

"""
For the purposes of providing MSDN licenses to an open source development
community, I consider anyone who writes, builds, tests or documents software
to be a "developer who contributes" to the project. (In fact, having started
out as a test engineer, I would take exception to anyone who claimed only
people who write code are "developers" :-) We do ask that requests are for
people who are active contributors and not just minor/occasional
participants.
"""

If this applies to you and you are interested, let me know.

Brian Curtin
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100416/818cbdee/attachment.html>



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
Brian Curtin wrote:
Thanks, Brian, for taking this on.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
See PyCon Talks from Atlanta 2010  http://pycon.blip.tv/
Holden Web LLC                 http://www.holdenweb.com/
UPCOMING EVENTS:        http://holdenweb.eventbrite.com/




----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Twisted folks will surely appreciate any help and may be able to
contribute back.
http://twistedmatrix.com/trac/wiki/Windows

-- 
anatoly t.



On Fri, Apr 16, 2010 at 4:01 PM, Brian Curtin <brian.curtin at gmail.com> wrote:



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: exarkun at twistedmatrix.co
Attributes: []Content: 
On 02:56 pm, techtonik at gmail.com wrote:

Extra Windows and VS licenses would certainly be helpful for Twisted 
development, and might lead indirectly to CPython/Windows improvements. 
Is this the kind of use for which it is appropriate to request an MSDN 
license via the PSF?

Jean-Paul



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Sun, Apr 18, 2010 at 10:16, <exarkun at twistedmatrix.com> wrote:


To my knowledge, no. I'm only handling PSF-specific subscriptions so I
wouldn't be comfortable giving them out for developers of Twisted or any
other project. However, I would encourage those projects to contact the OSTC
directly to start up a relationship.

Since you are active and fit the criteria for CPython, I can give you a
subscription and I know of no restrictions on what it's used for outside of
CPython.

Brian
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100418/f1ae8ae3/attachment.html>



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Fri, 16 Apr 2010 08:01:54 -0500, Brian Curtin a ?crit?:

Does it include a license for Windows itself?
Does it allow me to install and run it in a VM?
If so, I'm interested.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
Antoine Pitrou wrote:
Yes to both. MSDN offers a very broad license, with activation keys for
many products generated on demand.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
See PyCon Talks from Atlanta 2010  http://pycon.blip.tv/
Holden Web LLC                 http://www.holdenweb.com/
UPCOMING EVENTS:        http://holdenweb.eventbrite.com/




----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 19/04/2010 12:47, Antoine Pitrou wrote:

Yes, MSDN licenses give you access to several versions of Windows plus 
the Visual Studio tools for compiling Python (amongst other things).

Michael



-- 
http://www.ironpythoninaction.com/




----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 19 April 2010 11:47, Antoine Pitrou <solipsis at pitrou.net> wrote:

Yes. Arguably that's the best thing about the MSDN licenses
(particularly for non-Windows users, but even for us Windows users to
set up "clean" VM environments).

Paul.



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Tim Golde
Attributes: []Content: 
On 19/04/2010 13:33, Paul Moore wrote:

For some reason I hadn't appreciated that this was the case. I'm in
the process of negotiating [*] to repurpose an out-of-work server here
as a buildbot and being able to install an O/S will smooth the
negotiations considerably. I'm especially keen to get a buildbot
running a Windows server OS rather than desktop.

TJG

[*] May come to nothing; don't hold your breath



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Mon, Apr 19, 2010 at 06:48, Steve Holden <steve at holdenweb.com> wrote:


The left panel on
http://msdn.microsoft.com/en-us/subscriptions/cc137115.aspx has the full
list of currently available products if anyone wants to know what's all
involved here.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100419/71f8b2e0/attachment.html>



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 19, 2010, at 08:14 AM, Brian Curtin wrote:


This is really awesome.  I have an OEM license for Windows 7 but because I
dual boot that on my primary Ubuntu development machine, I rarely use it.
Being able to run Windows in a VM will mean I'll actually do it regularly.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100419/0c1df88f/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
Barry Warsaw wrote:
Well, the whole point from both sides is to ensure better support for
Python on Microsoft platforms. So go to it!

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
See PyCon Talks from Atlanta 2010  http://pycon.blip.tv/
Holden Web LLC                 http://www.holdenweb.com/
UPCOMING EVENTS:        http://holdenweb.eventbrite.com/




----------------------------------------
Subject:
[Python-Dev] MSDN licenses available for python-dev
----------------------------------------
Author: David Bole
Attributes: []Content: 
Antoine Pitrou <solipsis at pitrou.net> writes:


Yes, in fact, it's due to the availability of this license that I was
able to set up the Win7 buildbot.

-- David


