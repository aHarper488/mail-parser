
============================================================================
Subject: [Python-Dev] Readability of hex strings (Was: Use of coding cookie
	in 3.x stdlib)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Readability of hex strings (Was: Use of coding cookie
	in 3.x stdlib)
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
I find "\xXX\xXX\xXX\xXX..." notation for binary data totally
unreadable. Everybody who uses and analyses binary data is more
familiar with plain hex dumps in the form of "XX XX XX XX...".

I wonder if it is possible to introduce an effective binary string
type that will be represented as h"XX XX XX" in language syntax? It
will be much easier to analyze printed binary data and copy/paste such
data as-is from hex editors/views.

On Mon, Jul 19, 2010 at 9:45 AM, Guido van Rossum <guido at python.org> wrote:

