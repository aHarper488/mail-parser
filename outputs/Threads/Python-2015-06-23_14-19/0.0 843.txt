
============================================================================
Subject: [Python-Dev] Documenting [C]Python's Internals
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: Yaniv Akni
Attributes: []Content: 
Hi,

I wanted to let python-dev know about a series of articles about CPython's
internals I'm publishing under the collective title "Guido's Python"* (
http://tech.blog.aknin.name/tag/guidos-python/). Three articles already were
published already, more are planned (mainly focused on CPython/py3k, but
comparisons with other implementations may also be covered; we'll see). So
far I've done an introduction/whirlwind tour of Py_Main and a two-article
in-depth review of the (new-style) object system.

I'm sharing this with you (and hope you care) due to three reasons, probably
in escalating importance:
(a) Maybe some of python-dev's readers would be interested (possibly the
newer and more silent members).

(b) Maybe my scales are wrong, but I was a bit surprised by the number of
readers (>20,000 in the past two weeks); I wouldn't want to mislead such a
reader base and would be happy if a veteran here would be interested in
aiding by technically proofing the material (shan't be too hard I hope, feel
free to contact me directly if qualified and interested).

(c) While the content is currently geared to be blog-oriented, if it's found
worthy by the group I'd be delighted to formulate it into something more
'reference-material-ish' and give it back to the community. I found no
centrally organized CPython-internals material other than bits and pieces
(descrintro, eclectic blog posts, lectures, C-API reference, etc), and I
hope maybe something like this could be featured more officially on
python.org, with the relevant 'this is subject to change' disclaimers (can
be a document for new contributors, for pure Python programmers who're just
interested, or for whatever we decide).

Questions? Comments?
 - Yaniv

* think "Tim Berners-Lee's Web" or "Keanu Reeves' Green Gibberish", see the
first post for details
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100520/baa67ef2/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 19/05/2010 23:13, Yaniv Aknin wrote:

Whether or not they become part of the Python documentation I have very 
much enjoyed and appreciated this series of blog entries. I still covet 
the ability to contribute to Python in C and these articles are a great 
introduction to the underlying Python interpreter and object system.

Please continue!

All the best,

Michael Foord



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100519/4e67f571/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 
2010/5/20 Yaniv Aknin <yaniv at aknin.name>:

Great!
This can be *extremely* useful for new developers like me who still
haven't took a look at cPython internals.
Thanks for the effort.


--- Giampaolo
http://code.google.com/p/pyftpdlib
http://code.google.com/p/psutil



----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/19/2010 6:13 PM, Yaniv Aknin wrote:

This link has all post concatenated together in reverse order of how 
they should be read. The tags link returns the same page. Does your blog 
software allow you to make a master post and update with new links as 
available?


  Three


I would if I were qualified, but I an mot. One way to get people to help 
with details is to publish mistakes. This happens all the time on 
python-list ;-). Pre-review would be nice though.


People have asked for an internals-doc since I started over a decade 
ago. A coherent CPython3.2 Internals would be nice to have with the 3.2 
release next December or so, whether or not it was made 'official'.

Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: Yaniv Akni
Attributes: []Content: 
Ugh, either it doesn't or I couldn't find the feature (I'm using
wordpress.com, if someone has advice, let me know). I can clumsily suggest
scrolling from the end. Also see below about reworking this into a single
multi-chapter document with coherent form.

I would if I were qualified, but I an mot. One way to get people to help

I don't mind so much the 'humiliation' of published mistakes, but since I
want this to be perceived as reference grade material, I prefer pre-review.
Yesterday my first mistake was found (ugh), I published an 'Errata Policy'
and will stick to it from now on (see the blog itself for details of the
mistake). Thankfully, I've been approached already about pre-review, we'll
see how this develops (this doesn't mean other people can't also offer
themselves, six eyeballs are better than four).

People have asked for an internals-doc since I started over a decade ago. A

I'm targeting py3k anyway, and while I expect a bug lull in my writing
between early June and early September, I think December is a realistic date
for me to have good coverage CPython 3.2's core and rework the content into
a more reference-material-ish form. That said, working things into
reference-material form could be significant work, so if python-dev doesn't
show interest in this I think the blog posts are good enough. Other people,
this is your queue to chime in and state your opinion about this appearing
on python.org somewhere.

Cheers!
 - Yaniv
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100521/8a5a049a/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: Lie Rya
Attributes: []Content: 
On 05/21/10 15:18, Yaniv Aknin wrote:

How about a separate blog (or wiki) for alpha-quality articles? After an
article is written, it is first posted to the alpha blog, and after some
time and eyeballs, moved to the original blog. Of course with an open
comment system, so people can easily suggest corrections.




----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 21/05/2010 13:42, Lie Ryan wrote:

Separate blog is confusing I think - you then duplicate your content and 
people are just as likely to be referred to the "alpha quality" version 
as the final version.

Just publish and improve the articles based on feedback - I think your 
current approach with an established errata policy is well beyond what 
most people do or expect. When you have established the sort of coverage 
of the topic you are aiming for you can then take your blog articles, 
along with all feedback, and turn them into documentation.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Documenting [C]Python's Internals
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 20/05/10 08:13, Yaniv Aknin wrote:

A resource that may be useful to you is a 2.5 focused manuscript I put 
together a few years ago trying to bridge the gap between the library 
reference and the language reference:
http://svn.python.org/projects/sandbox/trunk/userref/

It's obviously a little dated in some areas and doesn't delve as deeply 
into the source code as you apparently plan to, but hopefully it may 
prove useful as a resource (I still have vague intentions of exporting 
that document to ReST markup and updating it to current Python, but that 
doesn't look like actually happening any time soon)

Cheers,
Nick.

P.S. For the record, the relevant URL is now 
http://tech.blog.aknin.name/tag/pythons-innards/


-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

