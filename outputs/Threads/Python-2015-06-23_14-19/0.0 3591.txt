
============================================================================
Subject: [Python-Dev] ur'string literal' in 3.3: make same as in 2.x?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] ur'string literal' in 3.3: make same as in 2.x?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
In 2.7, 'r' and 'ur' string literal prefixes have different effects:

"When an 'r' or 'R' prefix is present, a character following a backslash 
is included in the string without change, and all backslashes are left 
in the string."

"When an 'r' or 'R' prefix is used in conjunction with a 'u' or 'U' 
prefix, then the \uXXXX and \UXXXXXXXX escape sequences are processed 
while all other backslashes are left in the string."

When 'u' was deleted in 3.0, the first meaning was kept.

Was any thought given to restoring this difference in 3.3, along with 
restoring 'u', so that code using 'ur' prefixes would truly be 
cross-compatible? (I checked, and it has not been.) Cross-compatibility 
is the point of adding 'u' back, and this would give 'u' prefixes an 
actual, useful function even in Python 3.

This issue came up today in python-list thread 'python3 raw strings and 
\u escapes' by 'rurpy', who uses 'ur' for re strings with unicode chars 
and is trying to port his code to 3.x.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] ur'string literal' in 3.3: make same as in 2.x?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Sounds reasonable and within the intent of the PEP, so a tracker issue
would be the next step.

Cheers,
Nick.

--
Sent from my phone, thus the relative brevity :)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120531/761e9633/attachment.html>

