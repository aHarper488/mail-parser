
============================================================================
Subject: [Python-Dev] Persistent Python - a la Smalltalk
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Persistent Python - a la Smalltalk
----------------------------------------
Author: Albert Zeye
Attributes: []Content: 
Hi,

I was thinking about a persistent Python interpreter system. I.e. you
start a Python interpreter instance and you load and create all your
objects, classes and code in there (or load it in there from other
files).

The basic idea is that you wont restart your Python script, you would
always modify it on-the-fly. Or a bit less extreme: You would at least
have the possibility with this to do this (like just doing minor
changes). Also, if your PC halts for whatever reason, you can continue
your Python script after a restart.

This goes along my other recent proposal to store the AST of
statements in the related code objects
(http://thread.gmane.org/gmane.comp.python.devel/126754). An internal
editor could then edit this AST and recompile the code object.

For the persistance, there would be an image file containing all the
Python objects.

All in all, much like most Smalltalk systems.

---

Has anyone done something like this already?

---

There are a few implementation details which are not trivial and there
doesn't seem to be straight forward solutions, e.g. most generally:

* How to implement the persistance?
* How to handle image compatibility between CPython updates? Even possible?

Regards,
Albert



----------------------------------------
Subject:
[Python-Dev] Persistent Python - a la Smalltalk
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
[BCC python-dev, +python-ideas]

Funny you should mention this. ABC, Python's predecessor, worked like
this. However, it didn't work out very well. So, I'd say you're about
30 years too late with your idea... :-(

--Guido

On Sat, Sep 17, 2011 at 8:05 AM, Albert Zeyer <albzey at googlemail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Persistent Python - a la Smalltalk
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Albert Zeyer wrote:

python-dev is for developing the next version of Python (3.3 at this 
point).  Questions like this should go to python-list or python-ideas.

~Ethan~



----------------------------------------
Subject:
[Python-Dev] Persistent Python - a la Smalltalk
----------------------------------------
Author: Godson Ger
Attributes: []Content: 
Twisted has some feature like that implemented using pickles or some thing.
It meant to save the state of the program during restart. I am not sure if
that's what you are after. http://twistedmatrix.com
On 17 Sep 2011 20:44, "Albert Zeyer" <albzey at googlemail.com> wrote:
possible?
http://mail.python.org/mailman/options/python-dev/godson.g%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110918/723fb1f3/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Persistent Python - a la Smalltalk
----------------------------------------
Author: Zbigniew =?UTF-8?B?SsSZZHJ6ZWpld3NraS1Tem1law==?
Attributes: []Content: 
Guido van Rossum wrote:

Well, the newly developed IPython notebook [1] is something along those 
lines. So he's not late, he's a little bit early :)

-- Zbyszek

http://ipython.org/ipython-doc/dev/interactive/htmlnotebook.html


