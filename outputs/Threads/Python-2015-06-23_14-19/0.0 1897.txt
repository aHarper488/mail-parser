
============================================================================
Subject: [Python-Dev] Some additions to .hgignore
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Some additions to .hgignore
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
Hi all,
following http://docs.python.org/devguide/coverage.html doc you'll end
up with several "new" files/dirs in your checkout:

- .coverage, used by coveragepy to save its info
- coverage/ , the symlink to coveragepy/coverage
- htmlcov/ , the dir where the coverage HTML pages are written

I think they should be added to .hgignore so that hg st won't show them.

I'm writing here since I don't think an issue is needed for such
matter, if that's not the case, I apologize.

Regards,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Some additions to .hgignore
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 31/05/2011 23:59, Sandro Tosi wrote:

That sounds good to me. An issue certainly wouldn't hurt.

All the best,

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] Some additions to .hgignore
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
On 2011-06-01, Michael Foord <fuzzyman at voidspace.org.uk> wrote:

So be it: http://bugs.python.org/issue12341 :)

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi

