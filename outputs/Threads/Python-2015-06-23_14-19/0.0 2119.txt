
============================================================================
Subject: [Python-Dev] cpython (3.1): #11515: fix several typos. Patch by
 Piotr Kasprzyk.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.1): #11515: fix several typos. Patch by
 Piotr Kasprzyk.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 15 Mar 2011 04:19:24 +0100
ezio.melotti <python-checkins at python.org> wrote:

libffi is a third-party library and you should probably not introduce
gratuitous changes in these files. It will make tracking changes with
upstream more tedious.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (3.1): #11515: fix several typos. Patch by
 Piotr Kasprzyk.
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/3/15 Terry Reedy <tjreedy at udel.edu>:

It really doesn't matter if there's typos in 3rd party libraries which
we distribute.


No, because that would make updating it annoying, too. :)



-- 
Regards,
Benjamin

