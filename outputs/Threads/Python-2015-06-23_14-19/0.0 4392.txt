
============================================================================
Subject: [Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On behalf of the Python development team, I am pleased to announce the
releases of Python 3.2.5 and 3.3.2.

The releases fix a few regressions in 3.2.4 and 3.3.1 in the zipfile, gzip
and xml.sax modules.  Details can be found in the changelogs:

    http://hg.python.org/cpython/file/v3.2.5/Misc/NEWS  and
    http://hg.python.org/cpython/file/v3.3.2/Misc/NEWS

To download Python 3.2.5 or Python 3.3.2, visit:

    http://www.python.org/download/releases/3.2.5/  or
    http://www.python.org/download/releases/3.3.2/

respectively.  As always, please report bugs to

    http://bugs.python.org/

(Thank you to those who reported these regressions.)

Enjoy!

- -- 
Georg Brandl, Release Manager
georg at python.org
(on behalf of the entire python-dev team and all contributors)
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v2.0.19 (GNU/Linux)

iEYEARECAAYFAlGUbJ4ACgkQN9GcIYhpnLDH8ACdEM4k7bobLJsFmCb49zuwQR3W
EjgAoIWAOFNhJNdTAWEGSWqFWUP20wrb
=YnPr
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
16.05.13 08:20, Georg Brandl ???????(??):

It seems that I'm the main culprit of this releases.





----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 
2013/5/16 Serhiy Storchaka <storchaka at gmail.com>:

Well, when I look at the changelogs, what strikes me more is that
you're the author of *many* fixes, and also a lot of new
features/improvements.

So I wouldn't feel bad if I were you, this kind of things happens (and
it certainly did to me).

Cheers,

Charles



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/5/16 Serhiy Storchaka <storchaka at gmail.com>:

You've now passed your Python-dev initiation.



--
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 16 May 2013 13:24:36 +0200
Charles-Fran?ois Natali <cf.natali at gmail.com> wrote:

Seconded. Thanks Serhiy for your contributions :=)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, May 18, 2013 at 7:59 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Indeed!

Any need for quick releases to address regressions is always a
collective failure - for them to happen, the error has to be in
something not checked by our test suite, and the code change has to be
one where nobody monitoring python-checkins spotted a potential issue.

Hopefully the fixes for these regressions also came with new test
cases (although that is obviously difficult for upstream regressions
like those in the PyOpenSSL release bundled with the original Windows
binaries).

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 3.2.5 and Python 3.3.2
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 18.05.2013 05:21, schrieb Nick Coghlan:

Exactly.  Thanks Serhiy for making us improve the test suite :)

Georg


