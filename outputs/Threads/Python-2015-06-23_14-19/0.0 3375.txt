
============================================================================
Subject: [Python-Dev] New PEP numbering scheme
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] New PEP numbering scheme
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
It came up at the sprints about how to choose new PEP numbers. It was
agreed that the newest, *lowest* number should be used (e.g. 418) and not
the next highest number (e.g. 3156). I have already updated PEP 1 to
reflect this.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120312/c0bce734/attachment.html>



----------------------------------------
Subject:
[Python-Dev] New PEP numbering scheme
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 12, 2012, at 07:33 PM, Brett Cannon wrote:


+1

-Barry

