
============================================================================
Subject: [Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
Post Count: 24
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On 9/18/2010 10:03 PM, Nick Coghlan wrote:
That sounds pretty sane and coherent to me.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
DjangoCon US September 7-9, 2010    http://djangocon.us/
See Python Video!       http://python.mirocommunity.org/
Holden Web LLC                 http://www.holdenweb.com/




----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Sep 20, 2010 at 2:12 PM, Glyph Lefkowitz
<glyph at twistedmatrix.com> wrote:

It's primarily the "am I dealing with bytes" or "am I dealing with
text" precedent that I'm copying. That said, I'm not personally
opposed to that distinction propagating to higher level data
structures when it makes sense (e.g., to some degree it will propagate
naturally in urllib.parse, since the tuples of URL fragments can be
seen as higher level data structures analogous to the data structures
in email6).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Sun, 2010-09-19 at 12:03 +1000, Nick Coghlan wrote:

urllib.parse.urlparse/urllib.parse.urlsplit will never need to decode
anything when passed bytes input.  Both could just put the bytes
comprising the hex-encoded components (the path and query string) into
its respective place in the parse results, just like it does now for
string input.  As far as I can tell, the only thing preventing it from
working against bytes right now is the use of string literals in the
source instead of input-type-dictated-literals.  There should not really
be any need to create a "urllib.parse.urlsplitb" unless the goal is to
continue down the (not great IMO) precedent already set by the shadow
bytes API in urllib.parse (*_to_bytes, *_from_bytes) or if we just want
to make it deliberately harder to parse URLs. 

The only decoding that needs to be done to potential bytes input by APIs
in urllib.parse will be in the face of percent encodings in the path and
query components (handled entirely by "unquote" and "unquote_plus",
which already deal in bytes under the hood).  The only encoding that
needs to be done by urllib.parse is in the face of input to the
"urlencode" and "quote" APIs.  "quote" already deals with bytes as input
under the hood.  "urlencode" does not, but it might be changed use the
same strategy that "quote" does now (by using a "urlencode_to_bytes"
under the hood).

However, I think any thought about "adding raw bytes support" is largely
moot at this point.  This pool has already been peed in.There's
effectively already a "shadow" bytes-only API in the urlparse module in
the form of the *_to_bytes and *_from_bytes functions in most places
where it counts.  So as I see it, the options are:

1) continue the *_to_bytes and *_from_bytes pattern as necessary.

2) create a new module (urllib.parse2) that has only polymorphic
   functions.

#1 is not very pleasant to think about as a web developer if I need to
maintain a both-2-and-3-compatible codebase.  Neither is #2, really, if
I had to support Python 3.1 and 3.2.  From my (obviously limited)
perspective, a more attractive third option is backwards incompatibility
in a later Python 3 version, where encoding-aware functions like quote,
urlencode, and unquote_plus were polymorphic, accepting both bytes and
string objects and returning same-typed data.

- C





----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Sep 20, 2010 at 10:12 PM, Chris McDonough <chrism at plope.com> wrote:

Correct. Supporting manipulation of bytes directly is primarily a
speed hack for when an application wants to avoid the
decoding/encoding overhead needed to perform the operations in the
text domain when the fragments being manipulated are all already
correctly encoded ASCII text.

However, supporting direct manipulation of bytes *implicitly* in the
current functions is problematic, since it means that the function may
fail silently when given bytes that are encoded with an ASCII
incompatible codec (or which there are many, especially when it comes
to multibyte codecs and other stateful codecs). Even ASCII compatible
codecs are a potential source of hard to detect bugs, since using
different encodings for different fragments will lead directly to
mojibake.

Moving the raw bytes support out to separate APIs allows their
constraints to be spelled out clearly and for programmers to make a
conscious decision that that is what they want to do. The onus is then
on the programmer to get their encodings correct.

If we decide to add implicit support later, that's pretty easy (just
have urllib.parse.* delegate to urllib.parse.*b when given bytes).
Taking implicit support *away* after providing it, however, means
going through the whole deprecation song and dance. Given the choice,
I prefer the API design that allows me to more easily change my mind
later if I decide I made the wrong call.


If by "most places where it counts" you mean "quote" and "unquote",
then sure. However, those two functions predate most of the work on
fixing the bytes/unicode issues in the OS facing libraries, so they
blur the lines more than may be desirable (although reading
http://bugs.python.org/issue3300 shows that there were a few other
constraints in play when it comes to those two operations, especially
those related to the encoding of the original URL *prior* to
percent-encoding for transmission over the wire).

Regardless, quoteb and unquoteb will both be strictly bytes->bytes
functions, whereas the existing quoting APIs attempt to deal with both
text encoding and URL quoting all at the same time (and become a fair
bit more complicated as a result).

Cheers,
Nick.

--
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Mon, 2010-09-20 at 23:23 +1000, Nick Coghlan wrote:

The urllib.parse.urlparse/urlsplit functions should never need to know
or care whether the input they're passed is correctly encoded.  They
actually don't care right now: both will happily consume non-ASCII
characters and spit out nonsense in the parse results.  If passed
garbage, they'll return garbage:

  >>> urlparse('http://www.cwi.nl:80/%7Eguido/LaPe?a.html')
  ParseResult(scheme='http', netloc='www.cwi.nl:80', 
              path='/%7Eguido/LaPe?a.html', params='', 
              query='', fragment='')



The "path" component result above is potentially useless and broken, and
if it is inserted into a web page as a link, it may cause mojibake, but
urlparse doesn't (can't) complain.  As far as I can tell, there would be
no more and no less potential for mojibake if the same API were able to
be fed a bytes object.  The result can already be nonsense and allowing
for bytes as input doesn't add any greater potential to receive nonsense
back.

Most APIs in urllib.parse exhibit the same behavior today, e.g. urljoin:

   >>> urljoin('http://goo?le.com', '%7Eguido/LaPe?a.html')
   'http://goo?le.com/%7Eguido/LaPe?a.html'

The resulting URL is total nonsense.


I guess my argument is that the onus already *is* on the programmer to
get their encodings right.  They can just as easily screw up while using
str inputs.


Existing APIs save for "quote" don't really need to deal with charset
encodings at all, at least on any level that Python needs to care about.
The potential already exists to emit garbage which will turn into
mojibake from almost all existing APIs.  The only remaining issue seems
to be fear of making a design mistake while designing APIs.

IMO, having a separate module for all urllib.parse APIs, each designed
for only bytes input is a design mistake greater than any mistake that
could be made by allowing for both bytes and str input to existing APIs
and returning whatever type was passed.  The existence of such a module
will make it more difficult to maintain a codebase which straddles
Python 2 and Python 3.

- C





----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 21, 2010 at 4:30 AM, Chris McDonough <chrism at plope.com> wrote:

Failure to use quote/unquote correctly is a completely different
problem from using bytes with an ASCII incompatible encoding, or
mixing bytes with different encodings. Yes, if you don't quote your
URLs you may end up with mojibake. That's not a justification for
creating a *new* way to accidentally create mojibake.

Separating the APIs means that application programmers will be
expected to know whether they are working with data formatted for
display to the user (i.e. Unicode text) or transfer over the wire
(i.e. ASCII compatible bytes).

Can you give me a concrete use case where the application programmer
won't *know* which format they're working with? Py3k made the
conscious decision to stop allowing careless mixing of encoded and
unencoded text. This is just taking that philosophy and propagating it
further up the API stack (as has already been done with several OS
facing APIs for 3.2).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Tue, 2010-09-21 at 07:12 +1000, Nick Coghlan wrote:

There's no new way to accidentally create new mojibake here by allowing
bytes input, as far as I can tell.

- If a user passes something that has character data outside the range
  0-127 to an API that expects a URL or a "component" (in the
  definition that urllib.parse.urlparse uses for "component") of a URI,
  he can keep both pieces when it breaks.  Whether that data is
  represented via bytes or text is not relevant.  He provided 
  bad input, he is going to lose one way or another.

- If a user passes a bytestring to ``quote``, because ``quote`` is
  implemented in terms of ``quote_to_bytes`` the case is *already*
  handled by quote_to_bytes implicitly failing to convert nonascii
  characters.

What are the cases you believe will cause new mojibake? 


Yes.  Code which must explicitly deal with bytes input and output meant
to straddle both Python 2 and Python 3.  Please try to write some code
which 1) uses the same codebase to straddle Python 2.6 and Python 3.2
and 2) which uses bytes input, and expects bytes output from, say,
urlsplit.  It becomes complex very quickly.  A proposal to create yet
another bytes-only API only makes it more complex, AFAICT.

- C





----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Tue, 2010-09-21 at 08:19 +1000, Nick Coghlan wrote:

Right, the bytes issue here is really a red herring in both the urlsplit
and urljoin cases, I think.


Yay, sounds much, much better!

- C





----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 21, 2010 at 7:39 AM, Chris McDonough <chrism at plope.com> wrote:

Calling operations like urlsplit on byte sequences in non-ASCII
compatible encodings and operations like urljoin on byte sequences
that are encoded with different encodings. These errors differ from
the URL escaping errors you cite, since they can produce true mojibake
(i.e. a byte sequence without a single consistent encoding), rather
than merely non-compliant URLs. However, if someone has let their
encodings get that badly out of whack in URL manipulation they're
probably doomed anyway...

It's certainly possible I hadn't given enough weight to the practical
issues associated with migration of existing code from 2.x to 3.x
(particularly with the precedent of some degree of polymorphism being
set back when Issue 3300 was dealt with).

Given that a separate API still places the onus on the developer to
manage their encodings correctly, I'm beginning to lean back towards
the idea of a polymorphic API rather than separate functions. (the
quote/unquote legacy becomes somewhat unfortunate in that situation,
as they always returns str objects rather than allowing the type of
the result to be determined by the type of the argument. Something
like quotep/unquotep may prove necessary in order to work around that
situation and provide a bytes->bytes, str->str API)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 21, 2010 at 3:03 PM, Stephen J. Turnbull <stephen at xemacs.org> wrote:

Yeah, that's the original reasoning that had me leaning towards the
parallel API approach. If I seem to be changing my mind a lot in this
thread it's because I'm genuinely torn between the desire to make it
easier to port existing 2.x code to 3.x by making the current API
polymorphic and the fear that doing so will reintroduce some of the
exact same bytes/text confusion that the bytes/str split is trying to
get rid of.

There's no real way for 2to3 to help with the porting issue either,
since it has no way to determine the original intent of the 2.x code.

I *think* avoiding the quote/unquote precedent and applying the rule
"bytes in -> bytes out" will help with avoiding the worst of any
potential encoding confusion problems though. At some point the
programmer is going to have to invoke decode() if they want a string
to pass to display functions and the like (or vice versa with
encode()) so there are still limits to how far any poorly handled code
will get before blowing up. (Basically, while the issue of programmers
assuming 'latin-1' or 'utf-8' or similar ASCII friendly encodings when
they shouldn't is real, I don't believe a polymorphic API here will
make things any *worse* than what would happen with a parallel API)

And if this turns out to be a disaster in practice:
a) on my head be it; and
b) we still have the option of the DeprecationWarning dance for bytes
inputs to the existing functions and moving to a parallel API

Still-trying-to-figure-out-what-moment-of-insanity-prompted-me-to-volunteer-to-tackle-this'ly,
Nick.



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 21 September 2010 14:38, Nick Coghlan <ncoghlan at gmail.com> wrote:
[...]

Sorry if this is off-topic, but I don't believe I ever saw Stephen's
email. I have a feeling that's happened a couple of times recently.
Before I go off trying to work out why gmail is dumping list mails on
me, did anyone else see Stephen's mail via the list (as opposed to
being a direct recipient)?

Thanks, and sorry for the interruption.
Paul.



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Nick Coghlan writes:

 > (Basically, while the issue of programmers assuming 'latin-1' or
 > 'utf-8' or similar ASCII friendly encodings when they shouldn't is
 > real, I don't believe a polymorphic API here will make things any
 > *worse* than what would happen with a parallel API)

That depends on how far the polymorphic API goes.  As long as the
polymorphic API *never ever* does anything that involves decoding wire
format (and I include URL-quoting here), the programmer will have to
explicitly do some decoding to get into much trouble, and at that
point it's really their problem; you can't stop them.

But I don't know whether the web apps programmers will be satisfied
with such a minimal API.  If not, you're going to have to make some
delicate judgments about what to provide and what not, and whether/how
to provide a safety net of some kind.  I don't envy you that task.

 > And if this turns out to be a disaster in practice:

I would say be conservative about which APIs you make polymorphic.

And there are a lot of APIs that probably should be considered
candidates for polymorphic versions (regexp matching and searching,
for example).  So any experience or generic functionality you develop
here is likely to benefit somebody down the road.



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Sep 21, 2010, at 04:01 PM, Paul Moore wrote:


I remember seeing a message from Stephen on this topic, but I didn't read it
though and it's deleted now ;).  I don't use gmail regularly.

See also: http://wiki.list.org/x/2IA9

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100921/fbe60dd5/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 22 Sep 2010 00:10:01 +0900
"Stephen J. Turnbull" <stephen at xemacs.org> wrote:

Web app programmers will generally go through a framework, which
handles encoding/decoding for them (already so in 2.x).


As a matter of fact, the re module APIs are already polymorphic,
all the while disallowing any mixing of bytes and unicode.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Ian Bickin
Attributes: []Content: 
On Mon, Sep 20, 2010 at 6:19 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:


FWIW, while I understand the problems non-ASCII-compatible encodings can
create, I've never encountered them, perhaps because ASCII-compatible
encodings are so dominant.

There are ways you can get a URL (HTTP specifically) where there is no
notion of Unicode.  I think the use case everyone has in mind here is where
you get a URL from one of these sources, and you want to handle it.  I have
a hard time imagining the sequence of events that would lead to mojibake.
Naive parsing of a document in bytes couldn't do it, because if you have a
non-ASCII-compatible document your ASCII-based parsing will also fail (e.g.,
looking for b'href="(.*?)"').  I suppose if you did
urlparse.urlsplit(user_input.encode(sys.getdefaultencoding())) you could end
up with the problem.

All this is unrelated to the question, though -- a separate byte-oriented
function won't help any case I can think of.  If the programmer is
implementing something like
urlparse.urlsplit(user_input.encode(sys.getdefaultencoding())), it's because
they *want* to get bytes out.  So if it's named urlparse.urlsplit_bytes()
they'll just use that, with the same corruption.  Since bytes and text don't
interact well, the choice of bytes in and bytes out will be a deliberate
one.  *Or*, bytes will unintentionally come through, but that will just
delay the error a while when the bytes out don't work (e.g.,
urlparse.urljoin(text_url, urlparse.urlsplit(byte_url).path).  Delaying the
error is a little annoying, but a delayed error doesn't lead to mojibake.

Mojibake is caused by allowing bytes and text to intermix, and the
polymorphic functions as proposed don't add new dangers in that regard.

-- 
Ian Bicking  |  http://blog.ianbicking.org
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100921/a07f73ad/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 21 September 2010 16:23, Barry Warsaw <barry at python.org> wrote:

Ta. I'm seeing some other messages now, it may be just that Stephen's
were getting delayed.
Paul



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Tue, 2010-09-21 at 23:38 +1000, Nick Coghlan wrote:

In the case of urllib.parse, it's entirely safe.  If someone beats you
up over it later, you can tell them to bother "straddlers" in Web-SIG,
as we're the folks who most want the polymorphism in that particular
API.

- C





----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Sep 22, 2010 at 1:10 AM, Stephen J. Turnbull <stephen at xemacs.org> wrote:

As Chris pointed out, Issue 3300 means that particular boat has
already sailed where quote/unquote are concerned. Those are the only
APIs which ever need to do any encoding or decoding, as they deal with
percent-encoding of Unicode characters.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Sep 22, 2010 at 1:57 AM, Ian Bicking <ianb at colorstudy.com> wrote:

Indeed, this line of thinking is what brought me back around to the
polymorphic point of view.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Neil Hodgso
Attributes: []Content: 
Ian Bicking:


   It depends on what the particular ASCII-based parsing is doing. For
example, the set of trail bytes in Shift-JIS includes the same bytes
as some of the punctuation characters in ASCII as well as all the
letters. A search or split on '@' or '|' may find the trail byte in a
two-byte character rather than a true occurrence of that character so
the operation 'succeeds' but produces an incorrect result.

   Over time, the set of trail bytes used has expanded - in GB18030
digits are possible although many of the most important characters for
parsing such as ''' "#%&.?/''' are still safe as they may not be trail
bytes in the common double-byte character sets.

   Neil



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Neil Hodgson writes:

 >    Over time, the set of trail bytes used has expanded - in GB18030
 > digits are possible although many of the most important characters
 > for parsing such as ''' "#%&.?/''' are still safe as they may not
 > be trail bytes in the common double-byte character sets.

That's just not true.  Many double-byte character sets in use are
based on ISO-2022, which allows the whole GL repertoire to be used.

Perhaps you're thinking about variable-width encodings like Shift JIS
and Big5, where I believe that restriction on trailing bytes for
double-byte characters holds.  However, 7-bit encodings with control
sequences remain common in several contexts, at least in Japan and
Korea.  In particular, I can't say how frequent it is, especially
nowadays, but I have seen ISO-2022-JP in URLs "on the wire".

What really saves the day here is not that "common encodings just
don't do that".  It's that even in the case where only syntactically
significant bytes in the representation are URL-encoded, they *are*
URL-encoded.  As long as the parsing library restricts itself to
treating only wire-format input, you're OK.[1]  But once you start
doing things that involve decoding URL-encoding, you can run into
trouble.

Footnotes: 
[1]  With conforming input.  I assume that the libraries know how to
defend themselves from non-conforming input, which could be any kind
of bug or attack, not just mojibake.




----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Sep 22, 2010 at 9:37 AM, Andrew McNamara
<andrewm at object-craft.com.au> wrote:

There's an important distinction here though. Either change I could
make to urllib.parse will still result in two distinct APIs. The only
question is whether the new bytes->bytes APIs need to have a different
spelling or not.

Python 2.x is close to impossible to reliably test in this area
because there's no programmatic way to tell the difference between
encoded bytes and decoded text. In Python 3, while you can still get
yourself in trouble by mixing encodings at the bytes level, you're
almost never going to mistake bytes for text unless you go out of your
way to support working that way.

The structure of quote/unquote (which already contain implicit
decode/encode steps to allow them to consume both bytes and strings
with relative abandon and have done since 3.0) may cause us problems
in the long run, but polymorphic APIs where the type of the input is
the same as the type of the output shouldn't be any more dangerous
than if those same APIs used a different spelling to operate on bytes.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Sep 22, 2010 at 12:59 PM, Stephen J. Turnbull
<stephen at xemacs.org> wrote:

Notably, utf-16 and utf-32 make no promises regarding avoidance of
ASCII character codes in trail bytes - only utf-8 is guaranteed to be
compatible with parsing as if it were ASCII (and even then, you need
to be careful only to split the string at known ASCII characters
rather than at arbitrary points).

The known-ASCII-incompatible multibyte encodings I came up with when I
reviewed the list in the codecs module docs the other day were:
CP932 (the example posted here that prompted me to embark on this
check in the first place)
UTF-7
UTF-16
UTF-32
shift-JIS
big5
iso-2022-*
EUC-CN/KR/TW

The only known-ASCII-compatible multibyte encodings I found were UTF-8
and EUC-JP (all of the non-EBCDIC single byte encodings appeared to be
ASCII compatible though)

I didn't check any of the other CP* encodings though, since I already
had plenty of examples to show that the assumption of ASCII
compatibility isn't likely to be valid in general unless there is some
other constraint (such as the RFCs for safely encoding URLs to an
octet-sequence).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Supporting raw bytes data in urllib.parse.* (was
 Re: Polymorphic best practices)
----------------------------------------
Author: Baptiste Carvell
Attributes: []Content: 
Stephen J. Turnbull a ?crit :
If I understand you well, any processing of unquoted bytes is dangerous per se. 
If this is true, then perhaps 'unquote' doesn't disserve the criticism it 
received in this thread for always returning str. This would be in fact quite 
fortunate, as it forces url processing to either happen on quoted bytes (before 
calling 'unqote'), or on unquoted str (on the result of 'unquote'), both of 
which are safe.


