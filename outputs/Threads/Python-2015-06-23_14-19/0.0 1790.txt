
============================================================================
Subject: [Python-Dev] cpython: Issue #12451: Add
 support.create_empty_file()
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #12451: Add
 support.create_empty_file()
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le vendredi 01 juillet 2011 ? 11:24 +0200, Antoine Pitrou a ?crit :

The code was correct. I think that a function with an explicit name is
better than the open().close() pattern (which has various flavours, see
the diff). This pattern came sometimes with a comment explaining what it
does (create an empty file), which let me think that it's not easy to
understand what it is supposed to do (if you don't have the comment).

My initial need was to make quiet a warning (of my patched Python, see
#12451) if a file is opened in text mode without an explicit encoding.


For "wb", the only gain is to avoid the creation of temporary FileIO and
BufferedWriter objects, a micro optimisation. Most of the time, "w" mode
was used, so another temporary TextIOWrapper object was also created. I
also saw "w+" mode (use BufferedRandom+TextIOWrapper objects).

Victor


