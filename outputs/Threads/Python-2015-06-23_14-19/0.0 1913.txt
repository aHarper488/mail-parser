
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Fix reST label for
 collections ABCs.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Fix reST label for
 collections ABCs.
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 03/06/2011 19:43, Raymond Hettinger a ?crit :

The specific problem I addressed was that :ref:`abstract-base-classes`
was replaced by ?Collections Abstract Base Classes?, which was wrong:
the glossary entry talks about ?Abstract Base Classes?.

I will add links to abc submodules as you suggested tomorrow.

Thanks for the review.

