
============================================================================
Subject: [Python-Dev] Python 2.6.5 release candidate 1 now available
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 2.6.5 release candidate 1 now available
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
Hello everyone,

The source tarballs and Windows installer for Python 2.6.5 release candidate 1
are now available:

http://www.python.org/download/releases/2.6.5/

Please download them, install them, and try to use them with your favorite
projects and environments.  If no regressions are found, we will do the final
release on Monday March 15, 2010.  Please test the release candidate as
much as possible in the meantime, and help make 2.6.5 a rock solid release!

Thanks,
-Barry

P.S. The Mac installer will hopefully be available soon.
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100302/467fcbe0/attachment.pgp>

