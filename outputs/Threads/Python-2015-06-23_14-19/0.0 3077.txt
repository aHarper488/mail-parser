
============================================================================
Subject: [Python-Dev] Hashing proposal: 64-bit hash
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Hashing proposal: 64-bit hash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
As already mentioned, the vulnerability of 64-bit Python rather theoretical and not practical. The size of the hash makes the attack is extremely unlikely. Perhaps the easiest change, avoid 32-bit Python on the vulnerability, will use 64-bit (or more) hash on all platforms. The performance is comparable to the randomization. Keys order depended code will be braked not stronger than when you change the platform or Python feature version. Maybe all the 64 bits used only for strings, and for other objects -- only the lower 32 bits.




----------------------------------------
Subject:
[Python-Dev] Hashing proposal: 64-bit hash
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/1/27 Serhiy Storchaka <storchaka at gmail.com>:

A tempting idea, but binary incompatible.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Hashing proposal: 64-bit hash
----------------------------------------
Author: Frank Sievertse
Attributes: []Content: 


Unfortunately this assumption is not correct. It works very good with
64bit-hashing.

It's much harder to create (efficiently) 64-bit hash-collisions.
But I managed to do so and created strings with
a length of 16 (6-bit)-characters (a-z,  A-Z, 0-9, _, .). Even
14 characters would have been enough.

You need less than twice as many characters for the same effect as in
the 32bit-world.

Frank






----------------------------------------
Subject:
[Python-Dev] Hashing proposal: 64-bit hash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
27.01.12 23:08, Frank Sievertsen ???????(??):


The point is not the length of the string, but the size of string space 
for inspection. To search for a string with a specified 64-bit hash to 
iterate over 2 ** 64 strings. Spending on a single string scan 1 
nanosecond (a very optimistic estimate), it would take 2 ** 64 / 1e9 / 
(3600 * 24 * 365.25) = 585 years. For the attack we need to find 1000 
such strings -- more than half a million years. For 32-bit hash would 
need only an hour.

Of course, to calculate the hash function to use secure, not allowing 
"cut corners" and reduce computation time.




----------------------------------------
Subject:
[Python-Dev] Hashing proposal: 64-bit hash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
27.01.12 23:08, Frank Sievertsen ???????(??):


The point is not the length of the string, but the size of string space 
for inspection. To search for a string with a specified 64-bit hash to 
iterate over 2 ** 64 strings. Spending on a single string scan 1 
nanosecond (a very optimistic estimate), it would take 2 ** 64 / 1e9 / 
(3600 * 24 * 365.25) = 585 years. For the attack we need to find 1000 
such strings -- more than half a million years. For 32-bit hash would 
need only an hour.

Of course, to calculate the hash function to use secure, not allowing 
"cut corners" and reduce computation time.




----------------------------------------
Subject:
[Python-Dev] Hashing proposal: 64-bit hash
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

Zitat von Serhiy Storchaka <storchaka at gmail.com>:


I think you entirely missed the point of Frank's message. Despite your
analysis that it shall not be possible, Frank has *actually* computed
colliding strings, most likely also for a specified hash value.


This issue wouldn't be that relevant if there wasn't a documented
algorithm to significantly reduce the number of tries you need to
make to produce a string with a desired hash value. My own implementation
would need 2**33 tries in the worst case (for a 64-bit hash value);
thanks to the birthday paradox, it's actually a significant chance
that the algorithm finds collisions even faster.

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] Hashing proposal: 64-bit hash
----------------------------------------
Author: Frank Sievertse
Attributes: []Content: 


With meet-in-the-middle and some other tricks it's possible to generate 
25,000 64-bit-collisions per hour using an older desktop-cpu and 4gb ram.

H_dypmRNWgOxiaaG
A_ceO8B4Q2eKfabi
S_kpgdB3tUFJiaae
H_dypmRNWgOxiaaG
D_FYzdys3H8qbaba
0_pOwRq15h8vbabO
S_kpgdB3tUFJiaae
__mdKp1GvI_fcaaM
6_U3_B0pJT1UsaaW
4_1GnK9BmLj9naa5
__X7hMeAOpACdaaw
B_7pm.T62SiLlaai
I_HSdl0axd8tmaae
T_Dv3LwayACpdaaO

Frank

