
============================================================================
Subject: [Python-Dev] [Python-checkins] hooks: Fix checkbranch hook
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] hooks: Fix checkbranch hook
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

Wouldn?t using a whitelist instead of a blacklist protect against new
named branches too?



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] hooks: Fix checkbranch hook
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 26 Feb 2011 22:36:47 +0100
?ric Araujo <merwok at netwok.org> wrote:

Then we will have to fix the hook each time we want to add a new
legitimate branch.  I have no preference really.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] hooks: Fix checkbranch hook
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
The alternative is to edit the hook each time we want to remove a former
legitimate branch, plus have another hook to refuse new named branches.

Looks like a ?0 to me :)

Regards



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] hooks: Fix checkbranch hook
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Am 27.02.2011 00:21, schrieb ?ric Araujo:

The question is whether developers would create named branches if they
are asked not to (except when they are release manager).

There is nothing that prevents people from tagging the tree currently
in subversion, but most of the time, only the release manager does any
tagging.

If you think that it's a likely problem that people create named
branches by mistake, then we should have a hook.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] hooks: Fix checkbranch hook
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Feb 27, 2011 at 5:26 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

I suspect the concern is that people may create named branches locally
as part of their own workflow, then mistakenly push those branches
instead of collapsing back to a single commit against the relevant
line of development.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] hooks: Fix checkbranch hook
----------------------------------------
Author: Daniel Stutzbac
Attributes: []Content: 
On Sat, Feb 26, 2011 at 11:48 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:


+1

-- 
Daniel Stutzbach
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110227/64641a08/attachment.html>

