
============================================================================
Subject: [Python-Dev] Let's get PEP 380 into Python 3.3
Post Count: 15
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Now that the language moratorium is lifted, let's make sure to get PEP
380 implemented for Python 3.3. I think there are some minor issues to
be resolved, but I don't think that should stop someone from doing a
first pass of the implementation (especially since a version for 2.6
already exists).

(OTOH I am not much enamored with cofunctions, PEP 3152.)

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
From: Guido van Rossum
 

That's okay, I don't like it much myself in its current form.
I plan to revisit it at some point, but there's no hurry.

-- 
Greg

This email may be confidential and subject to legal privilege, it may
not reflect the views of the University of Canterbury, and it is not
guaranteed to be virus free. If you are not an intended recipient,
please notify the sender immediately and erase all copies of the message
and any attachments.

Please refer to http://www.canterbury.ac.nz/emaildisclaimer for more
information.



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Ok. Will you hvae time to port your patches to 3.3?

On Fri, Feb 25, 2011 at 3:01 PM, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Fri, Feb 25, 2011 at 5:43 PM, Guido van Rossum <guido at python.org> wrote:

Yay!



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Joao S. O. Buen
Attributes: []Content: 
On Fri, Feb 25, 2011 at 8:01 PM, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

I've just gone through PEP 3152 - and the first though that occurred me is
that a decorator is perfectly usable instead of the new proposed
keyword "codef".
(It would need to be able to set special attributes in the function to
indicate its nature)
Besides not adding a new keyword, it would allow for different
(concurrently running? ) types of
co-functions to be created and benefit from the other mechanisms.

But maybe considerations about this should be take place on python-ideas only?






----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Guido van Rossum wrote:

I'm not sure -- I'll see what I can do.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: rndblnc
Attributes: []Content: 
Greg Ewing <greg.ewing <at> canterbury.ac.nz> writes:


If this can help, I've "ported" the patch YieldFrom-Python3.1.2-rev5.patch
against current cpython tip.
What I've done:
- cloned current http://hg.python.org/cpython/ tip
- replaced tabs by 4 spaces in YieldFrom-Python3.1.2-rev5.patch and applied
result to tip
- manually applied failed hunks
- built and tested resulting python (configure/make/make test smoothly pass)
- successfully ran "yield from" samples with resulting binary
Resulting patch obtained by running "hg diff" is available here:
<http://tinypaste.com/4cc47d>

This patch includes graminit.[ch] modification since those files are versioned,
but I guess they are not necessary since they seem to be generated.

Hope this helps.

renaud




----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: rndblnc
Attributes: []Content: 
Greg Ewing <greg.ewing <at> canterbury.ac.nz> writes:

If this can help, I have written a script that transforms greg's patch into a
patch that applies on cpython current tip.
The script is available here:
http://iihm.imag.fr/blanch/misc/pep380_transform.py

Hope this will help to get pep 380 into Python 3.3...
If I can help to make it happend in any way, don't hesitate to ask, I really
would like to finally have it and start playing with "yield from" :)

renaud
 






----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Mar 21, 2011 at 4:06 PM, rndblnch <rndblnch at gmail.com> wrote:

In the light of the recent discussion about Hg clones, perhaps you
could make this a server-side clone so it's easier for others to play
along?

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Mar 22, 2011 at 9:21 AM, Guido van Rossum <guido at python.org> wrote:

Anyone without push rights for hg.python.org may want to fork the
mirror the bitbucket folks set up during the PyCon sprints to avoid
having to push the whole repository up to a separate hosting site:
https://bitbucket.org/mirror/cpython

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: rndblnc
Attributes: []Content: 
Nick Coghlan <ncoghlan <at> gmail.com> writes:

wrote:

I don't have push rights so I can not push anything to <http://hg.python.org/>.
However, I tried to click the "server side clone", and to my surprise, it
worked. I was expecting that some verification would be made, but there is now
a new feature branch at: <http://hg.python.org/features/pep-380/>.
Sorry to have done that, once again, I was not expecting to actually have the
rights to cause any side-effect on <http://hg.python.org/>. I guess that it may
not be desirable to allow anyone do such server-side clone.

Anyway, I can not push to this feature branch, so for now it's just a clone of
the curent tip.


Done here:
<https://bitbucket.org/rndblnch/cpython-pep380/>

The pep-380 changeset is pushed on top of current tip and is visible here:
<https://bitbucket.org/rndblnch/cpython-pep380/changeset/40b19d1933ea>

renaud







----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Mar 22, 2011 at 10:06 PM, rndblnch <rndblnch at gmail.com> wrote:

Unmodified clones don't actually take up much space due to the way the
filesystem on the server works, and if they don't get updated within a
short period of being created (i.e. by someone with commit rights),
the server gets rid of them automatically.


Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: rndblnc
Attributes: []Content: 
rndblnch <rndblnch <at> gmail.com> writes:

Now that I have figured out how to use patch queues with bitbucket, I will
maintain greg's pep380 implementation as a patch on top of cpython here:
<https://bitbucket.org/rndblnch/cpython-pep380/>

Testing it should be as simple as:
% hg qclone https://bitbucket.org/rndblnch/cpython-pep380
% cd cpython-pep380
% hg qpush pep380

and then the usual ./configure etc.


To test on an already existing clone of <http://hg.python.org/cpython>...
% hg clone http://hg.python.org/cpython
% cd cpython
% hg update -C default

you should enable mq extension...
% hg init --mq

get the pep380 patch and apply it...
% pushd .hg/patches/
% hg pull https://bitbucket.org/rndblnch/cpython-pep380/.hg/patches
% hg update
% popd
% hg qpush pep380

and then the usual ./configure etc.


The patch is now visible here:
<https://bitbucket.org/rndblnch/cpython-pep380/qseries?apply=t&qs_apply=pep380>

Hope this helps,

renaud





----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: rndblnc
Attributes: []Content: 
rndblnch <rndblnch <at> gmail.com> writes:

snip

apply=t&qs_apply=pep380>

There is a bug (acknowledged by bitbucket) preventing this page to display the
patch.
The error message is misleading, it states that the patch "pep380 did not apply
cleanly" but the problem is with bitbucket patching mechanism, not with the
patch.

The raw path is visible there:
<https://bitbucket.org/rndblnch/cpython-pep380/src/tip/pep380>
and I have documented how to use it on the wiki:
<https://bitbucket.org/rndblnch/cpython-pep380/wiki/>

I will stop spamming python-dev now...
It was just an attempt to help getting pep380 in python3.3 :)

renaud





----------------------------------------
Subject:
[Python-Dev] Let's get PEP 380 into Python 3.3
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Mar 26, 2011 at 6:25 AM, rndblnch <rndblnch at gmail.com> wrote:

Thanks for your efforts - I created tracker issue #11682 and linked it
to your bitbucket repo to help keep track of this updated version.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

