
============================================================================
Subject: [Python-Dev] Version fields [was Re: Goodbye]
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Version fields [was Re: Goodbye]
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <4C9C6A6F.6010202 at netwok.org>,
 ?ric Araujo <merwok at netwok.org> wrote:

I believe there is another separate use case for the versions field that 
hasn't been mentioned yet:  for issues with "release blocker" priority, 
the versions field is often used to identify the upcoming release for 
which a resolution is deemed mandatory.  However, having a single 
versions field is not totally satisfactory for this, particularly when - 
as has happened in the recent past - two different release cycles 
overlap (say, Python 2.7.x and 3.2.y).  A given issue may or may not 
apply to both, it may be a "release blocker" for one or both, and, if 
both, a fix may be checked-in for one branch but not the other.  The 
release managers for the two releases may end up using the one priority 
field and the one set of version fields for conflicting purposes.  It 
certainly makes it more difficult to automate a tracking report of 
exactly what are the release blocker issues for a specific release.

Besides adding fields to the database for an issue, there are other ways 
to handle the ambiguity, of course.  The simplest might be to just open 
a separate duplicate issue for each additional release blocked.  
Presumably that level of detail might only be needed in the endgame of a 
release, beta or rc stages.  It still places a restriction on the use of 
the version field and possibly other fields.  In issue workflow 
documentation, there should be some description of how "release blocker" 
should work, perhaps including something along the lines of "once a 
release enters stage <x>, 'release blocker' priority should only be 
changed with the approval of the release manager".

-- 
 Ned Deily,
 nad at acm.org


