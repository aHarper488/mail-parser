
============================================================================
Subject: [Python-Dev] r84562 - in python/branches/py3k:
 Doc/library/io.rst Lib/_pyio.py Lib/test/test_memoryio.py Misc/NEWS
 Modules/_io/_iomodule.c Modules/_io/_iomodule.h Modules/_io/bytesio.c
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] r84562 - in python/branches/py3k:
 Doc/library/io.rst Lib/_pyio.py Lib/test/test_memoryio.py Misc/NEWS
 Modules/_io/_iomodule.c Modules/_io/_iomodule.h Modules/_io/bytesio.c
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 7 Sep 2010 23:01:17 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

There was already an issue open for the context management option:
http://bugs.python.org/issue9757

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] r84562 - in python/branches/py3k:
 Doc/library/io.rst Lib/_pyio.py Lib/test/test_memoryio.py Misc/NEWS
 Modules/_io/_iomodule.c Modules/_io/_iomodule.h Modules/_io/bytesio.c
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 7, 2010 at 11:09 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Yeah, I didn't even think to check if you'd already put that up there.
I've now closed 9789 as a duplicate of that issue.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

