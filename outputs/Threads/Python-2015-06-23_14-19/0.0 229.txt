
============================================================================
Subject: [Python-Dev] IBM P-690 server looking for a home
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] IBM P-690 server looking for a home
----------------------------------------
Author: Randall Wall
Attributes: []Content: 
Greetings,

The company I work for has an IBM P-690 server that is in the process of
being retired. It is still a viable server, and has seen almost 0 use (it
was our failover machine). Unfortunately for us, this machine has little to
no resale value, and will probably be junked. I'd rather it go to a good
home, and having taken advantage of the work of the python development
community for a number of years (we use python extensively in system admin
and database work), I saw this as an opportunity to give back a little.

So, If anyone is interested in this machine, please let me know. We are
looking at perhaps a November time frame for when it will be removed from
our remote site. The P690 is no small machine, it is the size of a full rack
and has 32 Power4 processors in it and takes (I believe) 2 or 3 phase 220
Volt power. It weighs nearly a ton. We are running AIX5.3 on it, but I
believe that the machine is capable of running a PowerPC flavor of Linux as
well. This would make a great test machine for python HPC modules or as a
community box where developers could test their code against a PowerPC
architecture. It has lots of life left and I'd rather see it put to use then
thrown away.

Thanks,

-- 
Randall
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100819/5b14c466/attachment.html>



----------------------------------------
Subject:
[Python-Dev] IBM P-690 server looking for a home
----------------------------------------
Author: Trent Nelso
Attributes: []Content: 
On 19-Aug-10 10:48 AM, Randall Walls wrote:

Snakebite[1]'s always got an eye out for free hardware, but dang, that's 
one chunky piece of kit.  I'll follow up in private.

(And yeah, I'm still working on Snakebite, for those that are 
interested.  Turns out hosting three racks of heavy-duty hardware in the 
corner room of a (graciously donated) science lab takes a bit longer 
than originally anticipated.  Who would have thought.)

Regards,

	Trent "no-news-is-good-news" Nelson.

[1]: http://www.snakebite.org/

