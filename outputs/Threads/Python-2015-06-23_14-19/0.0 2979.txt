
============================================================================
Subject: [Python-Dev] New-style formatting in the logging module (was Re:
 cpython (3.2): Issue #14123: Explicitly mention that old style % string
 formatting has caveats)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] New-style formatting in the logging module (was Re:
 cpython (3.2): Issue #14123: Explicitly mention that old style % string
 formatting has caveats)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Feb 27, 2012 at 10:44 AM, Larry Hastings <larry at hastings.org> wrote:

It's half the puzzle (since composing the event fields into the actual
log output is a logger action, you know the style when you supply the
format string). The other half is that logging's lazy formatting
currently only supporting printf-style format strings - to use brace
formatting you currently have to preformat the messages, so you incur
the formatting cost even if the message gets filtered out by the
logging configuration. For that, a logger setting doesn't work, since
one logger may be shared amongst multiple modules, some of which may
use printf formatting, others brace formatting.

It could possibly be a flag to getLogger() though - provide a facade
on the existing logger type that sets an additional event property to
specify the lazy formatting style.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

