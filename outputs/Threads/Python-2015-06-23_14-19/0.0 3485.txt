
============================================================================
Subject: [Python-Dev] Python program name
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python program name
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
IIUC, the program name of the Python executable is set to whatever argv[0] is.
Is there a reason for this, rather than using one of the various OS-specific
APIs [1] for getting the name of the running executable? The reason I ask is
that in a virtual environment (venv), the exe's path is the only thing you have
to go on, and if you don't have that, you can't find the pyvenv.cfg file and
hence the base Python from which the venv was created.

Of course argv[0] is normally set to the executable's path, but there's at least
one test (in test_sys) where Python is spawned (via subprocess) with argv[0] set
to "nonexistent". If run from a venv created from a source build, with no Python
3.3 installed, this test fails because the spawned Python can't locate the
locale encoding, and bails.

It works when run from a source build ("./python ...") because the getpath.c
code to find a prefix looks in the directory implied by argv[0] (in the case of
"nonexistent" => "", i.e. the current directory) for "Modules/Setup", and also
works from a venv if created from an installed Python 3.3 (since the value of
sys.prefix is used as a fallback check, and that value will contain that
Python). However, when run from a venv created from a source build, with no
Python 3.3 installed, the error occurs.

A workaround might be one of these:

1. Use an OS-specific API rather than argv[0] to get the executable's path for
the processing done by getpath.c in all cases, or

2. If the file named by argv[0] doesn't exist, then use the OS-specific API to
find the executable's path, and try with that, or

3. If using the current logic, no prefix is found, then use the OS-specific API
to to find the executable's path, and try with that.

I would prefer to use option 2 and change getpath.c / getpathp.c accordingly.
Does anyone here see problems with that approach?

Regards,

Vinay Sajip

[1] http://stackoverflow.com/a/933996






----------------------------------------
Subject:
[Python-Dev] Python program name
----------------------------------------
Author: Michael Foor
Attributes: []Content: 

On 4 May 2012, at 09:44, Vinay Sajip wrote:



argv[0] is the *script* name, not the executable name - surely?

The executable path is normally available in sys.executable.

Michael




--
http://www.voidspace.org.uk/


May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing 
http://www.sqlite.org/different.html








----------------------------------------
Subject:
[Python-Dev] Python program name
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 4 May 2012 13:29:14 +0100
Michael Foord <fuzzyman at voidspace.org.uk> wrote:

I think Vinay is talking about C argv, not sys.argv.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Python program name
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 4 May 2012 08:44:25 +0000 (UTC)
Vinay Sajip <vinay_sajip at yahoo.co.uk> wrote:

If that's the only failing test, we can simply skip it when run from a
venv. A non-existent argv[0] is arguably a borderline case which you
should only encounter when e.g. embedding Python.


getpath.c is sufficiently byzantine that we don't want to complexify it
too much, IMHO.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Python program name
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Antoine Pitrou <solipsis <at> pitrou.net> writes:


Actually there are four module failures: test_sys, test_packaging,
test_distutils and test_subprocess. I haven't looked into all of them yet, but
many of the failure messages were "unable to get the locale encoding".


Right, but the change is unlikely to add significantly to complexity. It would
be one static function e.g. named get_executable_path and one call to it,
conditional on !isfile(argv[0]), in calculate_path. That would be in two places
- Modules/getpath.c and PC/getpathp.c.

I'll skip that test_sys test for now, and see where the other failures lead me
to.

Regards,

Vinay Sajip


