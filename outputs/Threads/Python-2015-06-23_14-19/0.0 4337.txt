
============================================================================
Subject: [Python-Dev] Google Summer of Code - Organization Deadline
 Approaching - March 29
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Google Summer of Code - Organization Deadline
 Approaching - March 29
----------------------------------------
Author: Terri Od
Attributes: []Content: 
On 03/26/2013 01:51 PM, Brian Curtin wrote:

I would obviously love it if you got your ideas up before the March 29th 
deadline so our application will totally shine and we'll be accepted 
immediately in to the program.

But I should note that while the Google deadline is March 29th, I'm 
happy to accept applications for sub-organizations and new project ideas 
after that deadline.  The students will be descending on April 9th, so 
if you want to attract the very best, try to get your project ideas up 
near then if at all possible!  Presuming we get accepted, though, I can 
take extra ideas almost up until the student applications start to come 
in, but I'd prefer them before April 15th (again, so you can attract the 
best students).


For those on the fence about mentoring or worried about doing a good 
job, I have a few experienced GSoC mentors who are currently without 
projects who have volunteered to help us out this year.  I'm happy to 
pair anyone who's interested with someone who loves working with GSoC 
students but maybe won't be familiar with the project, so you can learn 
the mentoring ropes and do code reviews but have a backup mentor there 
to help both you and the student through the GSoC process.  Please let 
me know ASAP if you'd like to do this so I can introduce you to the 
person and give them time to learn as much as they can about the 
projects you're hoping to run before students start arriving.

  Terri


