
============================================================================
Subject: [Python-Dev] More C API abstraction for user defined types
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Petre Gala
Attributes: []Content: 
Hello,
Defining a user type which implements number protocol should be treated as a 
number (long) in PyArg_ParseTuple using "l" format.
In getargs.c:
	case 'l': {/* long int */
		long *p = va_arg(*p_va, long *);
		long ival;
		if (float_argument_error(arg))
			return converterr("integer<l>", arg, msgbuf, bufsize);
		ival = PyLong_AsLong(arg);

		if (ival == -1 && PyErr_Occurred())
			return converterr("integer<l>", arg, msgbuf, bufsize);
		else
			*p = ival;
		break;
	}
ival should not be resolved through PyLong_AsLong, but through 
functionality/interface like PyNumber_Long, thus allowing more diversity in 
accessing the PyArg_Parse interfaces.

Petre




----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jul 13, 2010 at 3:35 AM, Petre Galan <Petre.Galan at gmail.com> wrote:

Sounds like a reasonable idea to me, but it needs to be a posted as a
feature request on the tracker.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Mon, Jul 12, 2010 at 10:19 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

+1, but I'd prefer it if PyNumber_Index were used, rather than PyNumber_Long.
It shouldn't be possible to pass a Decimal instance to something expecting an
integer argument.

Mark



----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Jul 13, 2010 at 6:44 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

Oops, I misread the suggestion. Indeed, PyNumber_Index is the correct
API function for this purpose.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Petre Gala
Attributes: []Content: 
Nick Coghlan <ncoghlan <at> gmail.com> writes:

 wrote:
wrote:
wrote:

No. The right interface is PyNumber_Long. The purpose of the PyNumber_Index (and 
nb_index slot) is as index in slicing.

Petre




----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Jul 14, 2010 at 11:50 PM, Petre Galan <Petre.Galan at gmail.com> wrote:

Allowing other objects to say "I'm a real integer, treat me as one" is
exactly what the nb_index slot is for (see PEP 357). The use as an
index in slicing was just the primary initial use case and the one
that was chosen as the name for the new slot because nb_int was
already taken for the lossy transformation. Indexing is not (and never
has been) intended to be the only use case.



----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Petre Gala
Attributes: []Content: 
Nick Coghlan <ncoghlan <at> gmail.com> writes:
wrote:

PyNumber_Long is the right interface as it's the right way to do it.
PyNumber_Index allows me to compute a value as index in slicing, value that
may be different that the integer behaviour of object. PyNumber_Index is serving 
it's purpose as index in slicing, beyond that it's getting abused.
As for related discussions, float should not implement nb_int slot but be
resolved in int's constructor.

Petre




----------------------------------------
Subject:
[Python-Dev] More C API abstraction for user defined types
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Jul 18, 2010 at 3:12 PM, Petre Galan <Petre.Galan at gmail.com> wrote:

What's your basis for making that assertion? As one of the devs
involved in adding PyNumber_Index and getting it to work properly, I
like to think I have some idea as to the purpose of the slot.

nb_int = can be coerced to int, but may lose numerical precision in the process
nb_index = can be coerced to int without losing numerical precision
(used for indexing, slicing and sequence repetition)

It's somewhat unintuitive, but that's backwards compatibility for you.

To quote the relevant part of PEP 357:

    Why not use nb_int which is already there?

    The nb_int method is used for coercion and so means something
    fundamentally different than what is requested here.  This PEP
    proposes a method for something that *can* already be thought of as
    an integer communicate that information to Python when it needs an
    integer.  The biggest example of why using nb_int would be a bad
    thing is that float objects already define the nb_int method, but
    float objects *should not* be used as indexes in a sequence.

    Why the name __index__?

    Some questions were raised regarding the name __index__ when other
    interpretations of the slot are possible.  For example, the slot
    can be used any time Python requires an integer internally (such
    as in "mystring" * 3).  The name was suggested by Guido because
    slicing syntax is the biggest reason for having such a slot and
    in the end no better name emerged. See the discussion thread:
    http://mail.python.org/pipermail/python-dev/2006-February/thread.html#60594
    for examples of names that were suggested such as "__discrete__" and
    "__ordinal__".


So, what spec are you using to say that you're right and I'm wrong?

(Now, I'd agree the documentation is a little lacking in this area, as
the docs for operator.index, PyNumber_Index and __index__ don't
explain very well how they differ semantically from int, PyNumber_Int
and __int__, but that's a separate issue. PEP 357 explains the intent
quite clearly, even if the published documentation doesn't do so)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

