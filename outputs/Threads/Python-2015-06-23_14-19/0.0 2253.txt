
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (2.7): Issue #11277:
 Remove useless test from test_zlib.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.7): Issue #11277:
 Remove useless test from test_zlib.
----------------------------------------
Author: Jim Jewet
Attributes: []Content: 
Can you clarify (preferably in the commit message as well) exactly
*why* these largefile tests are useless?  For example, is there
another test that covers this already?

-jJ

On 5/7/11, nadeem.vawda <python-checkins at python.org> wrote:



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.7): Issue #11277:
 Remove useless test from test_zlib.
----------------------------------------
Author: Nadeem Vawd
Attributes: []Content: 
On Mon, May 9, 2011 at 2:53 PM, Jim Jewett <jimjjewett at gmail.com> wrote:

Ah, sorry about that. It was discussed on the tracker issue, but I guess I
can't expect people to read through 90+ messages to figure it out :P

The short version is that it was supposed to test 4GB+ inputs, but in 2.7,
the functions being tested don't accept inputs that large.

The details:

The test was originally intended to catch the case where crc32() or adler32()
would get a buffer of >=4GB, and then silently truncate the buffer size and
produce an incorrect result (issue10276). It had been written for 3.x, and then
backported to 2.7. However, in 2.7, zlibmodule.c doesn't define
PY_SSIZE_T_CLEAN, so passing in a buffer of >=2GB raises an OverflowError
(see issue8651). This means that it is impossible to trigger the bug in question
on 2.7, making the test pointless.

Of course, the code that was deleted tests with an input sized 2GB-1 or 1GB,
rather than 4GB (the size used in 3.x). When the test was backported, the size
of the input was reduced, to avoid triggering an OverflowException. At the time,
no-one realized that this also would not trigger the bug being tested
for; it only
came to light when the test started crashing for unrelated reasons (issue11277).

Cheers,
Nadeem

