
============================================================================
Subject: [Python-Dev] Accessing value stack
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Accessing value stack
----------------------------------------
Author: Dima Tisne
Attributes: []Content: 
Hi, is it possible to access the values stored on the stack in Python stack
machine from Python?

As in, consider expression:

tmp = foo() + bar()

At the point in time when bar() is called, foo() already returned
something, and that value is on top of the stack in that frame, a few steps
after bar() returns, that value is added to bar and is lost.

I would very much like to be able to read (and maybe even change) that
ephemeral value.

If it is absolutely not possible to achieve from Pythonland, I welcome
pointers on how to write a C/cython/types extension to achieve this.

Thanks,
d.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130107/d7987c12/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Accessing value stack
----------------------------------------
Author: Oleg Broytma
Attributes: []Content: 
Hello.

   We are sorry but we cannot help you. This mailing list is to work on
developing Python (adding new features to Python itself and fixing bugs);
if you're having problems learning, understanding or using Python, please
find another forum. Probably python-list/comp.lang.python mailing list/news
group is the best place; there are Python developers who participate in it;
you may get a faster, and probably more complete, answer there. See
http://www.python.org/community/ for other lists/news groups/fora. Thank
you for understanding.

On Mon, Jan 07, 2013 at 03:06:51PM +0100, Dima Tisnek <dimaqq at gmail.com> wrote:

   In short: it's possible but very much discouraged. Don't hack into
python internals.

Oleg.
-- 
     Oleg Broytman            http://phdru.name/            phd at phdru.name
           Programmers don't die, they just GOSUB without RETURN.



----------------------------------------
Subject:
[Python-Dev] Accessing value stack
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Jan 7, 2013 at 4:22 PM, Oleg Broytman <phd at phdru.name> wrote:

Is it possible? I claim it's not (from *Python* because obviously data
is in memory).

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Accessing value stack
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jan 7, 2013 at 10:46 AM, Maciej Fijalkowski <fijall at gmail.com> wrote:

Nope, it's not.



----------------------------------------
Subject:
[Python-Dev] Accessing value stack
----------------------------------------
Author: PJ Eb
Attributes: []Content: 
On Mon, Jan 7, 2013 at 11:32 AM, Brett Cannon <brett at python.org> wrote:

I took that as a challenge, and just tried to do it using
gc.get_referents().  ;-)

Didn't work though...  which actually makes me wonder if that's a bug
in gc.get_referents(), or whether I'm making a bad assumption about
when you'd have to run gc.get_referents() on a frame in order to see
items from the value stack included.  Could this actually be a bug in
frames' GC implementation, or are value stack items not supposed to
count for GC purposes?



----------------------------------------
Subject:
[Python-Dev] Accessing value stack
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Jan 7, 2013 at 10:40 PM, PJ Eby <pje at telecommunity.com> wrote:

valustack is a static list in C (it's not a Python list at all) that's
stored on the frame. You can't do it that way. gc.get_referrers
returns you only python objects (and in fact should return you only
python objects that are accessible otherwise, but those details are
really implementation-dependent)

