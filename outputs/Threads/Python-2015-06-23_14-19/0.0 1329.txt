
============================================================================
Subject: [Python-Dev] Hosting the Jython hg repo
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Hosting the Jython hg repo
----------------------------------------
Author: Philip Jenve
Attributes: []Content: 
There's been some chatter in the past about moving some of Jython's infrastructure from SF.net to python.org.

We're in the process of finishing the conversion of Jython's subversion repo to mercurial. Can we host our new repo on http://hg.python.org? To whom should I speak to about setting this up?

The one question that comes to mind is how will repo write permissions be handled/shared between all our repos? With the recent policy of granting Jython committers cpython commit access if they want it, sharing the permissions wouldn't be a problem. In turn we like the idea of reciprocating commit rights to Jython back to cpython committers.

--
Philip Jenvey



----------------------------------------
Subject:
[Python-Dev] Hosting the Jython hg repo
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Georg Brandl and Antoine Pitrou are managing the Mercurial repositories.


In the past, granting permissions generously wasn't a problem as long
as users where aware what repositories they are allowed to commit to.
There isn't a true push log at this point (IIUC), but at least for
cpython, we can audit what changes have been pushed by what user through
the commit emails.
As it is always possible to revert undesired changes, and to revoke
privileges that are abused, there is no reason to technically enforce
access control.

IOW, cpython committers just shouldn't push to Jython's repository,
and vice versa, except for a good reason.

Ultimately, it's up to Georg and Antoine to decide whether they want
to accept the load. One option would be to grant a Jython developer
control to account management - preferably a single person, who would
then also approve/apply changes to the hooks.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Hosting the Jython hg repo
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 10.04.2011 21:58, "Martin v. L?wis" wrote:

At the moment, any core developer can push to any repo on hg.python.org.
I would very much like to keep it that way, it makes administration
much easier.  If you're okay with it, then I'm glad to set you and all
Jython committers up at hg.python.org.


And I believe that will work.

Georg




----------------------------------------
Subject:
[Python-Dev] Hosting the Jython hg repo
----------------------------------------
Author: Philip Jenve
Attributes: []Content: 

On Apr 10, 2011, at 12:58 PM, Martin v. L?wis wrote:


That could be me. If this would mean creating an account for me on python.org so I could handle the majority of the maintenance instead of Georg & Antoine, I'd be up for that.

--
Philip Jenvey




----------------------------------------
Subject:
[Python-Dev] Hosting the Jython hg repo
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 10 Apr 2011 21:58:40 +0200
"Martin v. L?wis" <martin at v.loewis.de> wrote:

I don't want to maintain the Jython repo myself but if Georg or Philip
accepts to do it it's fine.


+1.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Hosting the Jython hg repo
----------------------------------------
Author: Philip Jenve
Attributes: []Content: 

On Apr 10, 2011, at 2:44 PM, Antoine Pitrou wrote:


Let's go ahead with this option then. Can someone please grant me said access?

--
Philip Jenvey


