
============================================================================
Subject: [Python-Dev] Behaviour of max() and min() with equal keys
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Matthew Woodcraf
Attributes: []Content: 
In CPython, the builtin max() and min() have the property that if there
are items with equal keys, the first item is returned. From a quick look
at their source, I think this is true for Jython and IronPython too.

However, this isn't currently a documented guarantee. Could it be made
so? (As with the decision to declare sort() stable, it seems likely that
by now there's code out there relying on it anyway.)

-M-





----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Tue, Sep 7, 2010 at 8:34 PM, Matthew Woodcraft
<matthew at woodcraft.me.uk> wrote:

It's actually not clear to me that this behaviour is ideal;  it might
make more sense to have max() return the first item among equal
largest elements, and min() return the last item.  That way, the
special case of two operands has the nice property that (max(x, y),
min(x, y)) is always either (x, y) or (y, x), rather than being
possibly (x, x) or (y, y).  (That is, id(max(x, y)) and id(min(x, y))
are id(x) and id(y) in one order or the other.)

The max and min methods for the Decimal module take care to work this
way, for example:

(Decimal('2'), Decimal('2.0'))

But:

(Decimal('2'), Decimal('2'))


Can you give examples of code that relies on max and min returning the
first among equals?

Mark



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Laurens Van Houtve
Attributes: []Content: 
FWIW: I think Mark is right. I never quite understood why that was, but
never cared enough to complain.

lvh
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100907/0c475602/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Matthew Woodcraf
Attributes: []Content: 
Mark Dickinson wrote:


I don't care a great deal what the behaviour is; I would like it to be
consistent across Python versions, and I think the pragmatic way to
achieve that is to document the current behaviour.



An existing regression test whose output depends on which choice is
made.


(I was writing some code today which had to duplicate the behaviour of a
non-Python program which uses first-among-equals, which is what brought
this question up. In that case I wouldn't think it unreasonable to have
to hand-code the loop rather than using max(), but if in practice Python
is always going to be first-among-equals, it seems to me we might as
well be 'allowed' to take advantage of it.)

-M-




----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Jeffrey Yasski
Attributes: []Content: 
On Tue, Sep 7, 2010 at 1:44 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

Decimal may actually have this backwards. The idea would be that
min(*lst) == sorted(lst)[0], and max(*lst) == sorted(lst)[-1]. Given a
stable sort, then, max of equivalent elements would return the last
element, and min the first. According to Alex Stepanov, this helps
some with composing these small order statistics into larger
stably-ordered functions.

I do think Decimal.max's answer is better than the builtin's answer,
and that the incremental benefit from returning the last instead of
first is pretty small.



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Jeffrey Yasski
Attributes: []Content: 
On Tue, Sep 7, 2010 at 2:40 PM, Jeffrey Yasskin <jyasskin at gmail.com> wrote:

Actually, Decimal isn't doing anything along these lines. At least in
Python 2.6, I get:

Decimal('2')
Decimal('2')
Decimal('2.0')
Decimal('2.0')

indicating that '2' is considered larger than '2.0' in the min/max
comparison. It's ignoring the order of the arguments. It also creates
a new Decimal object for the return value, so I can't use id() to
check which one of identical elements it returns.



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Tue, Sep 7, 2010 at 10:47 PM, Jeffrey Yasskin <jyasskin at gmail.com> wrote:

Right;  there's a strict specification about how equal values with
different exponents (or different signs in the case of comparing
zeros) should be treated.


This bit surprises me.  I honestly thought I'd fixed it up so that
max(x, y) actually returned one of x and y (and min(x, y) returned the
other).  Oh well.

Mark



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Tue, Sep 7, 2010 at 10:40 PM, Jeffrey Yasskin <jyasskin at gmail.com> wrote:

Yes, you're right; that would make more sense than the other way around.

Mark



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Tue, Sep 7, 2010 at 10:51 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

Ah.  I'd forgotten that the Decimal max and min methods are context
aware, so that max(x, y) is rounded to the current context, and hence
can actually be different from both x and y.  So that was a bad
example from me.  Sorry.

Decimal('-0E-527')


Mark



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Tue, Sep 7, 2010 at 11:00 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

Grr. s/max(x, y)/x.max(y)/g

Mark



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Sep 7, 2010, at 12:34 PM, Matthew Woodcraft wrote:


That seems like a reasonable request.  This behavior has been around
for a very long time is unlikely to change.  Elsewhere, we've made
efforts to document sort stability (i.e. sorted(), heapq.nlargest(),
heapq.nsmallest, merge(), etc).

It is nice that min(it) parallels sorted(it)[0] and nsmallest(1, it).
The same goes for max(it) paralleling sorted(it,reverse=True)[0]
and nlargest(1, it).


Raymond




----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Hrvoje Niksi
Attributes: []Content: 
On 09/07/2010 11:40 PM, Jeffrey Yasskin wrote:

Here you mean "is" rather than "==", right?  The relations you spelled 
are guaranteed regardless of stability.

(This doesn't apply to Decimal.max and Decimal.min, which return new 
objects.)



----------------------------------------
Subject:
[Python-Dev] Behaviour of max() and min() with equal keys
----------------------------------------
Author: Matthew Woodcraf
Attributes: []Content: 
Raymond Hettinger  <raymond.hettinger at gmail.com> wrote:




I've submitted issue 9802 as a feature request with a concrete suggestion for
the docs.

-M-


