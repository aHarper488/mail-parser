
============================================================================
Subject: [Python-Dev] devinabox: Add a script which will build CPython.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devinabox: Add a script which will build CPython.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 28 Feb 2011 23:03:50 +0100
brett.cannon <python-checkins at python.org> wrote:

Actually, you can also build from the command line under Windows:
using Tools/buildbot/build.bat or Tools/buildbot/build-amd64.bat
depending on the build you want (but perhaps it's good to teach people
to use the MSVC UI, since that's the reference IDE under Windows;
besides, these scripts will need MSVN installed anyway).

Regards

Antoine.



