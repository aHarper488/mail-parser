
============================================================================
Subject: [Python-Dev] Garbage announcement printed on interpreter shutdown
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Garbage announcement printed on interpreter shutdown
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Hey #python-dev,

I'd like to ask your opinion on this change; I think it should be reverted
or at least made silent by default.  Basically, it prints a warning like

      gc: 2 uncollectable objects at shutdown:
          Use gc.set_debug(gc.DEBUG_UNCOLLECTABLE) to list them.

at interpreter shutdown if gc.garbage is nonempty.

IMO this runs contrary to the decision we made when DeprecationWarnings were
made silent by default: it spews messages not only at developers, but also at
users, who don't need it and probably are going to be quite confused by it,
assuming it came from their console application (imagine Mercurial printing
this).

Opinions?

Georg


Am 09.08.2010 00:18, schrieb antoine.pitrou:


-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.


