
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (merge 3.2 -> 3.3):
 Fixes Issue #16114: The subprocess module no longer provides a
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (merge 3.2 -> 3.3):
 Fixes Issue #16114: The subprocess module no longer provides a
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Wed, Oct 10, 2012 at 3:52 AM, gregory.p.smith
<python-checkins at python.org> wrote:


I was on this issue but didn't have an opportunity to comment.

It would be cleaner to use the self.assertRaises() pattern here and
also probably better to share code across the three test methods which
are nearly identical to one another (there is a fourth scenario I
would also add of shell=True).

I would also check for FileNotFoundError instead of OSError in the 3.3
and later versions.

--Chris


