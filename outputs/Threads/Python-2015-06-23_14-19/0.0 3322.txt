
============================================================================
Subject: [Python-Dev] Sandboxing Python
Post Count: 36
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

The frozendict discussion switched somewhere to sandboxing, and so I
prefer to start a new thread.

There are various ways to implement a sandbox, but I would like to
expose here how I implemented pysandbox to have your opinion.
pysandbox is written to execute quickly a short untrusted function in
a sandbox and then continue the normal execution of the program. It is
possible to "enable" the sandbox, but also later to "disable" it. It
is written for Python using only one thread and one process.

To create a sandbox, pysandbox uses various protections. The main idea
is to create an empty namespace and ensure that it is not possible to
use objects added into the namespace for escaping from the sandbox.
pysandbox only uses one thread and one process and so it doesn't
replace the existing trusted namespace, but create a new one. The
security of pysandbox depends on the sandbox namespace sealing.

I don't want to integrate pysandbox in CPython because I am not yet
conviced that the approach is secure by design. I am trying to patch
Python to help the implementation of Python security modules and of
read-only proxies.

You can find below the list of protections implemented in pysandbox.
Some of them are implemented in C.

I challenge anymore to break pysandbox! I would be happy if anyone
breaks it because it would make it more stronger.

https://github.com/haypo/pysandbox/
http://pypi.python.org/pypi/pysandbox

Namespace
=========

 * Make builtins read only
 * Remove function attribute:

   * frame.f_locals
   * function.func_closure/__closure__
   * function.func_defaults/__defaults__
   * function.func_globals/__globals__
   * type.__subclasses__
   * builtin_function.__self__

 * Workaround the lack of frozendict, remove dict attributes:

   *__init__
   * clear
   * __delitem__
   * pop
   * popitem
   * setdefault
   * __setitem__
   * update

 * Create a proxy for objects injected to the sandbox namespace and for
   the result of functions (the result of callable objects is also proxified)


Generic
=======

Remove all builtin symbols not in the whitelist.


Features
========

import
------

 * Replace __import__ function to use an import whitelist

Filesystem
----------

 * Replace open and file functions to deny access to the filesystem

Exit
----

 * Replace exit function
 * Remove SystemExit builtin exception

Standard input/output
---------------------

 * Replace sys.stdin, sys.stdout and sys.stderr


Bytecode
========

Execute arbitrary bytecode may crash Python, or lead to execution of arbitrary
(machine) code.

 * Patch code.__new__()
 * Remove attributes:

   * function.func_code/__code__
   * frame.f_code
   * generator.gi_code

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

Hum, I should give some rules for such contest:

- the C module (_sandbox) must be used
- you have to get access to a object outside the sandbox, like a real
module, or get access to a blocked resource (like the filesystem)
- the best is to be able to write into the filesystem
- you can use the interpreter ("python interpreter.py") to play with
the sandbox, but you have to be able to reproduce with a simple script
(e.g. using "python execfile.py script.py")

pysandbox works on Python 2.5, 2.6 and 2.7. It does not officially
support Python 3 yet.

Example.
---
$ python setup.py build
$ PYTHONPATH=build/lib.*/ python interpreter.py  --allow-path=/etc/issue
pysandbox 1.1
Enabled features: codecs, encodings, exit, interpreter, site, stderr,
stdin, stdout, traceback
(use --features=help to enable the help function)

Try to break the sandbox!

sandbox>>> open('/etc/issue').read()
'Ubuntu 11.10 \\n \\l\n\n'

sandbox>>> type(open('/etc/issue'))('test', 'w')
Traceback (most recent call last):
  File "<console>", line 1, in <module>
TypeError: object.__new__() takes no parameters
---
You fail!

I'm interested by vulnerabilities in pysandbox using the Python
restricted module (used when _sandbox is missing), but it is not the
official mode :-) And it is more limited: you cannot read files for
example.

See also sandbox tests to get some ideas ;-)

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Armin Rig
Attributes: []Content: 
Hi Victor,

On Thu, Mar 1, 2012 at 22:59, Victor Stinner <victor.stinner at gmail.com> wrote:

I tried to run the files from Lib/test/crashers and --- kind of
obviously --- I found at least two of them that still segfaults
execfile.py, sometimes with minor edits and sometimes directly, on
CPython 2.7.

As usual, I don't see the point of "challenging" us when we have
crashers already documented.  Also, it's not like Lib/test/crashers
contains in detail *all* crashers that exist; some of them are of the
kind "there is a general issue with xxx, here is an example".

If you are not concerned about segfaults but only real attacks, then
fine, I will not spend the hours necessary to turn the segfault into a
real attack :-)


A bient?t,

Armin.



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

Le 03/03/2012 20:13, Armin Rigo a ?crit :

As described in the README file of pysandbox, pysandbox doesn't protect 
against vulnerabilities or bugs in Python.


You may be able to exploit crashers, but I don't plan to workaround such 
CPython bug in pysandbox.

I'm looking for vulnerabilities in pysandbox, not in CPython.

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Sat, Mar 3, 2012 at 1:37 PM, Victor Stinner <victor.stinner at gmail.com> wrote:

Well ok. But then what's the point of "challenging" people?

You say "this is secure according to my knowledge" and when armin says
"no it's not", you claim this is the wrong kind of security exploit.
Segfaults (most of them) can generally be made into arbitrary code
execution, hence the pysandbox is not quite secure. Even further,
"any" sort of this "security restrictions" where you modify locals
globals etc. would be seriously prone to attacks like those segfaults,
unless you do something with the VM you're running. This makes it
slightly less convincing to argue that the VM requires new features
(in this case frozendict) in order to support the kind of program
that's broken in the first place.

Well, I think I'm seriously missing something.

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Sat, Mar 3, 2012 at 6:02 PM, Maciej Fijalkowski <fijall at gmail.com> wrote:

Could we put asserts in the places where segfaults may happen? Then
Victor could say "if you want this to be secure then you must build
your Python executable with asserts on." IIRC some of the segfaults
*already* trigger asserts when those are enabled.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Sat, Mar 3, 2012 at 6:51 PM, Guido van Rossum <guido at python.org> wrote:

It's easy for some cases. Stack exhaustion cases might be
significantly harder although you might pass some compiler-specific
options to defend against that. The problem is a bit that those are
"examples", which mean that they might either touch specific parts of
code or "code that looks like that". A good example of a latter is
chaining of iterators. Any iterators that can be chained can be made
into a stack exhaustion segfault.

I suppose with a bit of effort it might be made significantly harder though.

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
$ python execfile.py badhash.py

Hang up.
-------------- next part --------------
A non-text attachment was scrubbed...
Name: badhash.py
Type: text/x-python
Size: 83 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120304/4969224f/attachment.py>



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
There is even easier way to exceed the time-limit timeout and to eat CPU: sum(xrange(1000000000)).




----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Armin Rig
Attributes: []Content: 
Hi all,

On Sun, Mar 4, 2012 at 03:51, Guido van Rossum <guido at python.org> wrote:

No.  I checked Lib/test/crashers/*.py and none of them would be safe
with just a failing assert.  If they were, we'd have written the
assert long ago :-(  "mutation_inside_cyclegc.py" is not tied to a
particular place in the source; "loosing_mro_ref.py" requires an extra
INCREF/DECREF in a performance-critical path; etc.

Changing CPython to make it truly secure is definitely either a lost
cause or a real major effort, and pysandbox just gives another such
example.  My advise is to give up and move security at some other
level.

(Or else, if you want to play this game, there is PyPy's sandboxing,
which is just an unpolished proof a concept so far.  I can challenge
anyone to attack it, and this time it includes attempts to consume too
much time or memory, to crash the process in any other way than a
clean "fatal error!" message, and more generally to exploit issues
that are dismissed by pysandbox as irrelevant.)


A bient?t,

Armin.



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Armin Rigo wrote:

I don't think it is as hard as all that.
All the crashers can be fixed, and with minimal effect on performance.
(although the gc module might need couple of function removed)


Using too much memory can be dealt with at one place (in the allocator).
You can't solve the too much time, without solving the halting problem,
but you can make sure all code is interruptable (i.e. Cntrl-C works).

Cheers,
Mark.



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Maciej Fijalkowski wrote:


Can you give an example of how this can be done?

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Mark Shannon wrote:


If you can arrange for Ctrl-C to interrupt the process cleanly,
then (at least on Unix) you can arrange to receive a signal
after a timeout and recover cleanly from that as well..

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Armin Rig
Attributes: []Content: 
Hi Mark,

On Sun, Mar 4, 2012 at 18:34, Mark Shannon <mark at hotpy.org> wrote:

I will assume that you don't mean just to fix the files in
Lib/test/crashers, but to fix the general issues that each is a
particular case for.  I suppose there is no point in convincing you
about my point of view, so I can just say "feel free and have fun".


Armin



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Armin Rig
Attributes: []Content: 
Hi Greg,

On Sun, Mar 4, 2012 at 22:44, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

You should find tons of documented examples of various attacks.  It's
not easy, but it's possible.  For example, let's assume we can decref
a object to 0 before its last usage, at address x.  All you need is
the skills and luck to arrange that the memory at x becomes occupied
by a new bigger string object allocated at "x - small_number".  This
is enough to control exactly all the bytes that are put at address x
and following, just by choosing the characters of the string.  For
example the bytes can be built to make address x look like a built-in
function object, which you can call --- which will call an arbitrary
chosen address in memory.  This is enough to run arbitrary machine
code and do anything.


A bient?t,

Armin.



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Armin Rig
Attributes: []Content: 
Hi Mark,

On Sun, Mar 4, 2012 at 18:34, Mark Shannon <mark at hotpy.org> wrote:

Not sure what you mean by that.  It seems to me that it's particularly
easy to do in a roughly portable way, with alarm() for example on all
UNIXes.


A bient?t,

Armin.



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Armin Rigo wrote:

What time should you set the alarm for? How much time is enough before you 
decide that a piece of code is taking too long?

The halting problem is not that you can't breaking out of an infinite loop, 
but that you can't *in general* decide when you are in an infinite loop.

I think that Mark's point is that you can't, in general, tell when you are in 
a "too much time" attack (or bug) that needs to be broken out of rather than 
just a legitimately long calculation which will terminate if you wait just a 
little longer.


-- 
Steven




----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 04.03.2012 23:53, schrieb Steven D'Aprano:

This is getting off-topic, but you can *certainly* solve the
"too much time" problem without solving the halting problem.

The "too much time" problem typically has a subjective, local,
application-specific specification. Therefore, the "too much time"
problem is *easily* solved with timeouts. Too much is just too much,
even if it would eventually complete with a useful result.

I'd say that a single request should not take more than 20 seconds,
else it's too much. It must be less than 2 seconds for interactive use,
and less than 1s if you get more than 100 requests per second. If these
numbers sound arbitrary to you: they are. They are still useful to me.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

pysandbox uses SIGALRM with a timeout of 5 seconds by default. You can
change this timeout or disable it completly.

pysandbox doesn't provide a function to limit the memory yet, you have
to do it manually. It's not automatic because there is no portable way
to implement such limit and it's difficult to configure it. For my IRC
bot using pysandbox, setrlimit() is used with RLIMIT_AS.

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2012/3/5 Serhiy Storchaka <storchaka at gmail.com>:

Ah yes, I realized that SIGALRM is handled by the C signal handler,
but Python only handles the signal later. sum() doesn't call
PyErr_CheckSignals() to check for pending signals.

Apply the timeout would require to modify the sum() function. A more
generic solution would be to use a subprocess.

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Armin Rigo wrote:

That's a lot of assumptions. When you claimed that *any* segfault
bug could be turned into an arbitrary-code exploit, it sounded
like you had a provably general procedure in mind for doing so,
but it seems not.

In any case, I think Victor is right to object to his sandbox
being shot down on such grounds. The same thing equally applies
to any method of sandboxing any computation, whether it involves
Python or not. Even if you fork a separate process running code
written in Befunge, it could be prone to this kind of attack if
there is a bug in it.

What you seem to be saying is "Python cannot be sandboxed,
because any code can have bugs." Or, "Nothing is ever 100% secure,
because the universe is not perfect." Which is true, but not in
a very interesting way.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 06 Mar 2012 10:21:12 +1300
Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

There is a difference between bugs and known bugs, though.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Mar 5, 2012 at 1:16 PM, Victor Stinner <victor.stinner at gmail.com> wrote:

Just forbid the sandboxed code from using the signal module, and set
the signal to the default action (abort).


Maybe it would make more sense to add such a test to xrange()? (Maybe
not every iteration but every 10 or 100 iterations.)

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

Most crashers don't crash pysandbox because they use features blocked
by pysandbox, like the gc module. Others fail with a timeout.

3 tests are crashing pysandbox:

 - modify a dict during a dict lookup: I proposed two different fixes
in issue #14205
 - type MRO changed during a type lookup (modify __bases__ during the
lookup): I proposed a fix in issue #14199 (keep a reference to the MRO
during the lookup)
 - stack overflow because of a compiler recursion: we should limit the
depth in the compiler (i didn't write a patch yet)

pysandbox should probably hide __bases__ special attribute, or at
least make it read-only.


It's possible to fix these crashers. In my experience, Python is very
stable and has few crasher in the core language (e.g. compared to
PHP). But I agree that it would be safer to run the untrusted code in
a subprocess, by design.

Running the code in a subprocess may be an option to provide higher
level of security. Using a subprocess allows to reuse OS protections.

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

Ah yes, good idea. It may be an option because depending on the use
case, failing with abort is not always the best option.

The signal module is not allowed by the default policy.


pysandbox may replace some functions by functions checking regulary
the timeout to raise a Python exception instead of aborting the
process.

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
05.03.12 23:16, Victor Stinner ???????(??):
 > Apply the timeout would require to modify the sum() function.

sum() is just one, simple, example. Any C code could potentially run 
long enough. Another example is the recently discussed hashtable 
vulnerability:

  class badhash: __hash__ = int(42).__hash__
  set([badhash() for _ in range(100000)])

 > A more generic solution would be to use a subprocess.

Yes, it's the only way to secure implement the sandbox.





----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
05.03.12 23:47, Guido van Rossum ???????(??):

`sum([10**1000000]*1000000)` leads to same effect.




----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Mar 5, 2012 at 1:21 PM, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

Not all of segfaults are exploitable, but most of them are. Some are
super trivial (like the one armin explained, where it takes ~few hours
for a skilled person), or some are only research paper kind of proof
of concepts (double free is an example).

I strongly disagree that sandbox is secure because it's "just
segfaults" and "any code is exploitable that way". Finding segfaults
in CPython is "easy". As in all you need is armin, a bit of coffee and
a free day. Reasons for this vary, but one of those is that python is
a large code base that does not have automatic ways of preventing such
issues like C-level recursion.

For a comparison, PyPy sandbox is a compiled from higher-level
language program that by design does not have all sorts of problems
described. The amount of code you need to carefully review is very
minimal (as compared to the entire CPython interpreter). It does not
mean it has no bugs, but it does mean finding segfaults is a
significantly harder endeavour. There are no bug-free programs,
however having for example to segfault an arbitrary interpreter
*written* in Python would be significantly harder than one in C,
wouldn't it?

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

I agree that the PyPy sandbox design looks better... but some people
are still using CPython and some of them need security. That's why
there are projects like zope.security, RestrictedPython and others.
Security was not included in CPython design. Python is a highly
dynamic language which make the situation worse.

I would like to improve CPython security. pysandbox is maybe not
perfect, and it may only be a first step to improve security. Even if
pysandbox has issues, having a frozendict type would help to secure
applications. For example, it can be used later for __builtins__ or to
build read-only types.

I agree that each bug, especially segfault, may lead to exploitable
vulnerabilities, but it doesn't mean that we should not consider
hardening Python because of these bugs. Even if PHP is known for its
lack of security and its broken safe_mode, people use it and run it on
web server accessible to anyone on the Internet. There are also
projects to harden PHP. For example:
http://www.hardened-php.net/suhosin/

suhosin patch doesn't avoid the possiblity of segfault but it is
harder to exploit them with the patch.

I proposed to start with a frozendict because I consider that it is
not only useful for security, and the patch to add the type is not
intrusive. Other changes to use the patch can be discussed later,
except if you consider that related changes (__builtins__ and
read-only type) should be discussed to decide if a frozendict is
required or not.

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

While this may true, I can't conclude that we should stop fixing
crashers in CPython, or give up developing CPython altogether. While
it is a large code base, it is also a code base that will be around
for a long time to come, so any effort spend on this today will pay
off in the years to come.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Mar 5, 2012 at 3:40 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

I did not say that Martin.

PyPy sandbox does not come without issues, albeit they are not the
security related kind.

My point is that it does not make sense do add stuff to CPython to
make sandboxing on the Python easier *while* there are still easily
accessible segfaults. Fixing those issues, should be a priority
*before* we actually start and tinker with other layers. All I'm
trying to say is "if you want to make a sandbox on top of CPython, you
have to fix segfaults".

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Armin Rig
Attributes: []Content: 
Hi Stefan,

Stefan Behnel wrote:

Sorry if you were offended.  I am just trying to point out that
CPython has a rather large number of *far-fetched* corner cases in
which it is broken.  (If this is news to anyone, sorry, but examples
have been part of the CPython source tree for years and years.)  This
is of course very different from saying that CPython is generally
broken --- I don't think anyone here considers that it is.  My point
is merely to repeat that CPython is not suited to be the (only) line
of defence in any place that needs serious security.  I personally
think that the removal of 'rexec' back around Python 2.3(?) was a good
idea, as such tools give people a false sense of security.


A bient?t,

Armin.



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Maciej Fijalkowski, 06.03.2012 00:08:

Well, there's a bug tracker that lists some of them, which is not *that*
hard to find. Does your claim about "a significantly harder endeavour"
refer to finding a crash or to finding a fix for it?

Stefan




----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Armin Rig
Attributes: []Content: 
Hi Stefan,

On Wed, Mar 7, 2012 at 23:16, Stefan Behnel <stefan_ml at behnel.de> wrote:

Are you talking about the various crashes about the JIT?  That's one
of the reasons why the sandboxed PyPy does not include the JIT.


A bient?t,

Armin.



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
On 05/03/2012 23:11, Victor Stinner wrote:

I opened the following issues to fix these crashers:

#14205: Raise an error if a dict is modified during a lookup. Fixed in 
Python 3.3.

#14199: Keep a refence to mro in _PyType_Lookup() and super_getattro(). 
Fixed in Python 3.3.

#14211: Don't rely on borrowed _PyType_Lookup() reference in 
PyObject_GenericSetAttr(). Fixed in Python 3.3.

#14231: Fix or drop Lib/test/crashers/borrowed_ref_1.py, it looks like 
it was already fixed 3 years ago.

The compiler recursion is not fixed yet.

Fixes may be backported to Python 2.7 and 3.2.

Victor



----------------------------------------
Subject:
[Python-Dev] Sandboxing Python
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
On 01/03/2012 22:59, Victor Stinner wrote:

Results, one week later. Nobody found a vulnerability giving access to 
the filesystem or to the sandbox.

Armin Rigo complained that CPython has known "crasher" bugs. Except of 
the compiler recursion, I fixed those bugs in CPython 3.3.

Serhiy Storchaka found a bug in the pysandbox timeout: long operations 
implemented in C hangs the sandbox, the timeout contrain is not applied. 
Guido proposed to abort the process (use the default SIGALRM action). I 
proposed to add an option to use a subprocess. Both solutions are not 
exclusive.

Armin Rigo also noticed that PyPy sandbox design is more robust than 
pysandbox design, I agree with him even if I think a CPython sandbox is 
useful and users ask for such protection.

I have no idea how many developers tried to break the pysandbox security.

Victor

