
============================================================================
Subject: [Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 19.04.2012 03:36, ezio.melotti wrote:


I think that's misleading: there's no way to "correctly" parse malformed HTML.

Georg




----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/4/24 Georg Brandl <g.brandl at gmx.net>:

There is in the since that you can follow the HTML5 algorithm, which
can "parse" any junk you throw at it.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Tue, Apr 24, 2012 at 2:34 PM, Benjamin Peterson <benjamin at python.org> wrote:

This whole can of worms is why I gave up on HTML years ago (well, one
reason among many).

There are markup languages, and there's soup.


  -Fred

-- 
Fred L. Drake, Jr.? ? <fdrake at acm.org>
"A person who won't read has no advantage over one who can't read."
?? --Samuel Langhorne Clemens



----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 24.04.2012 20:34, Benjamin Peterson wrote:

Ah, good. Then I hope we are following the algorithm here (and are slowly
coming to use it for htmllib in general).

Georg




----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/4/24 Benjamin Peterson <benjamin at python.org>:

This is confusing, since I meant "sense".


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 24/04/2012 15:02, Georg Brandl a ?crit :

Yes, Ezio?s commits on html.parser/HTMLParser in the last months have 
been following the HTML5 spec.  Ezio, RDM and I have had some discussion 
about that on some bug reports, IRC and private mail and reached the 
agreement to do the useful thing, that is follow HTML5 and not pretend 
that the stdlib parser is strict or validating.

Ezio was thinking about a blog.python.org post to advertise this.

Regards



----------------------------------------
Subject:
[Python-Dev] cpython (2.7): #14538: HTMLParser can now parse
 correctly start tags that contain a bare /.
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Tue, Apr 24, 2012 at 14:34, ?ric Araujo <merwok at netwok.org> wrote:

Please do this, and I welcome anyone else who wants to write about
their work on the blog to do so. Contact me for info.

