
============================================================================
Subject: [Python-Dev] cpython (2.7): Fix comment blocks. Adjust
	blocksize to a power-of-two for better divmod
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (2.7): Fix comment blocks. Adjust
	blocksize to a power-of-two for better divmod
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jun 18, 2013, at 12:00 AM, Gregory P. Smith <greg at krypto.org> wrote:


I worked on the 2.7 branch first because that was the one I had loaded
and the one where I did timings and code disassembly.  I intended to
post it to 3.3 and 3.4 as well over the weekend.   

Ideally, it makes maintenance simpler for me if I keep the branches
the same as possible.  I viewed the one-line struct transposition,
comment correction, and one-line blocklen change as being somewhat 
innocuous non-algorithmic changes.  The struct change fixed an unintended
cache miss for left links and the blocksize change causes the
deque_index code to compile more efficiently (using a right-shift
and bitwise-and and rather than a measurably more expensive
division and modulo calculation).

I truly wasn't expecting the Spanish Inquisition :-)


On Jun 22, 2013, at 1:43 PM, Scott Dial <scott+python-dev at scottdial.com> wrote:


Yes, this is a micro-optimization.  In working on implementing
deque slicing for 3.4, I restudied the block access patterns.   
On an appendleft(), popleft() or extendleft() operation, the left link is
accessed immediately before or after the leftmost entry in the data block. 
The old struct arrangement can cause an unnecessary cache miss
when jumping leftward.  This was something I overlooked when I
originally wrote the code almost a decade ago.

On Jun 23, 2013, at 11:38 AM, Benjamin Peterson <benjamin at python.org> wrote:


Really, you reverted my one-line change within 24 hours of it being posted?
I can't be on-line every day and sometimes it takes a little while to catch up
with python email.

On Jun 22, 2013, at 2:55 PM, Guido van Rossum <guido at python.org> wrote:


I also tried to fix that comment so it would stop emphasizing the blocklen
being a multiple of the cache line.   Also long as there is a reasonably long
data block, it matters less whether the data block size is an exact multiple
of the cache line length (62 or 64 words of data versus a typical 8 byte
cache line).

The data block size does matter elsewhere though.
The benefit of the having the data block being a power-of-two
is that the deque_index computation can use bits shifts
rather division and modulo calculations.  The benefit
of switching data block size from 62 to 64 was measurable
(a few percent) and observable in the disassembly of the code.

I experimented with one other ordering as well
(putting the data block first and the links afterwards).
That saved the scaled-index byte in the generated code
but produced no measureable speed-up.

In short, I believe the following should go in:

* The comment fix. (Incorrectly suggesting that a 64-bit 
   Py_ssize_t would overflow).  The revised comment
   is simpler, more general, and correct.

* Putting the leftlink before the data block in the structure.

The follow is up for debate:

Changing the BLOCKLEN from 62 to 64 is debatable.
It measureably improved deque_index without an
observable negative effect on the other operations.

Lastly, there was a change I just put in to Py 3.4 replacing
the memcpy() with a simple loop and replacing the
"deque->" references with local variables.  Besides
giving a small speed-up, it made the code more clear
and less at the mercy of various implementations
of memcpy().

Ideally, I would like 2.7 and 3.3 to replace their use of
memcpy() as well, but the flavor of this thread suggests
that is right out.


Raymond



-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130623/9d70fa47/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] cpython (2.7): Fix comment blocks. Adjust
	blocksize to a power-of-two for better divmod
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jun 23, 2013, at 6:52 PM, Scott Dial <scott+python-dev at scottdial.com> wrote:


Honestly, I'm not sure what you're arguing for or against.

The struct should to be reordered so that the leftmost data element
and left link are positioned side-by-side, and the same goes for the
rightmost element and right link.

Whether the blocksize should change is debatable.
It causes an unfortunate odd-size at the end (not good for
the final cache line), but it does improve the speed of the 
deque_index() code.  The former seems to make no difference
in timings while the latter gives a measureable speed-up.

Unfortunately, I'm losing interest in this part of the deque work.
I've already invested substantial time reading papers
(such as http://www.akkadia.org/drepper/cpumemory.pdf
and http://www.intel.com/content/www/us/en/architecture-and-technology/64-ia-32-architectures-optimization-manual.html),
analyzing the code, reading disassembly, and making timings.
But it isn't worth all the second guessing (and what feels like sniping).
I've worked on this code for almost a decade.  As far as I can tell, none
of the participants in this thread has ever previously shown any interest
in the deque object.  It is discouraging to have a simple parameter 
change and struct reordering reverted.  This thread has offered zero 
support or encouragement for my work.  The python-dev 
social environment appears to be degrading over time.
I only have a few hours of development time each week
and now I'm wasting that time responding to these emails
(it may go with the territory, but it is a waste none-the-less).

If it is what you guys what, then leave the code as is
(with an incorrect comment, a blocklen that is unfavorable
to indexing, and a struct order that doesn't exploit
cache locality by following the natural access patterns
in the code).

I understand that the code for Py2.7 is sensitive and understand
if you all prefer to leave it untouched. 



Raymond


P.S. This has all arisen in context of my working on patch
for implementing slicing for deques.  In that context, the
code for deque_item() and deque_rotate() will become
more important than they were before.   Along the way,
I am re-examining all my existing code.




-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130623/aeb06fef/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython (2.7): Fix comment blocks. Adjust
	blocksize to a power-of-two for better divmod
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jun 24, 2013, at 4:07 AM, Victor Stinner <victor.stinner at gmail.com> wrote:


Yes, the goal was to have the struct size be an exact multiple
of the cache line length (always a power-of-two, typically 64 bytes).
What was different then is that deques weren't indexable.
When indexing was added, the size of 62 became an 
unfavorable choice because it made the division and modulo 
calculation in deque_index() slower than for a power of two.


I don't know what you're talking about.
This structure isn't externally visible.


Raymond









-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130624/9886627d/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython (2.7): Fix comment blocks. Adjust
	blocksize to a power-of-two for better divmod
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jun 24, 2013, at 10:12 PM, Benjamin Peterson <benjamin at python.org> wrote:


If you don't mind, I think you should be the one to undo your own reversions.

Thank you,


Raymond

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130625/5ab07ae1/attachment.html>

