
============================================================================
Subject: [Python-Dev] [Python-checkins] r84685 - in
 python/branches/py3k: Doc/library/dis.rst Doc/reference/simple_stmts.rst
 Doc/whatsnew/3.2.rst Include/opcode.h Lib/opcode.py
 Lib/test/test_exceptions.py Lib/test/test_scope.py Lib/test/test_syntax.py
 Misc/NEWS Python
In-Reply-To: <AANLkTim4bfGn7yRWaZ+SQSq6guqmzT+mZHbne+=zB4+V@mail.gmail.com>
References: <AANLkTinD95OdiuyyFEs+AMpYSw607r-y5g0HYH+kTA01@mail.gmail.com>
	<AANLkTim4bfGn7yRWaZ+SQSq6guqmzT+mZHbne+=zB4+V@mail.gmail.com>
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r84685 - in
 python/branches/py3k: Doc/library/dis.rst Doc/reference/simple_stmts.rst
 Doc/whatsnew/3.2.rst Include/opcode.h Lib/opcode.py
 Lib/test/test_exceptions.py Lib/test/test_scope.py Lib/test/test_syntax.py
 Misc/NEWS Python
In-Reply-To: <AANLkTim4bfGn7yRWaZ+SQSq6guqmzT+mZHbne+=zB4+V@mail.gmail.com>
References: <AANLkTinD95OdiuyyFEs+AMpYSw607r-y5g0HYH+kTA01@mail.gmail.com>
	<AANLkTim4bfGn7yRWaZ+SQSq6guqmzT+mZHbne+=zB4+V@mail.gmail.com>
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Sep 11, 2010 at 10:33 AM, Benjamin Peterson <benjamin at python.org> wrote:

Yeah, I saw your subsequent checkin. I've updated the comment just
above MAGIC and TAG to make it clearer when they should be changed.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

