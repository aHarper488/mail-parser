
============================================================================
Subject: [Python-Dev]
	=?utf-8?q?=5BPreview=5D_Comments_and_change_proposal?=
	=?utf-8?q?s=09on=09documentation?=
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]
	=?utf-8?q?=5BPreview=5D_Comments_and_change_proposal?=
	=?utf-8?q?s=09on=09documentation?=
----------------------------------------
Author: Don Johnsto
Attributes: []Content: 
Steven D'Aprano <steve <at> pearwood.info> writes:



As an end-user, I, too, share concerns about the accessibility of the pending 
(proposed?) commenting functionality.

A read-only JSON API would be great.

Up until now, Sphinx has been an incredibly helpful tool for generating 
beautiful documentation from ReStructuredText, which is great for limiting the 
risk of malformed input.

The new commenting feature ("dynamic application functionality") requires 
persistence for user-submitted content. Database persistence is currently 
implemented with the -excellent- SQLAlchemy ORM.

So, this is a transition from Sphinx being an excellent publishing tool to being 
a dynamic publishing platform for user-submitted content ("comments"). I am sure 
this was not without due consideration, and FUD.

The Python Web Framework communities (favorite framework *here*) will be the 
first to reiterate the challenges that all web application developers (and 
commenting API providers) face on a daily basis:

- SQL Injection
- XSS (Cross Site Scripting)
- CSRF (Cross Site Request Forgery)

Here are a few scenarios to consider:

(1) Freeloading jackass decides that each paragraph of our documentation would 
look better with 200 "comments" for viagara. Freeloading jackass is aware of how 
HTTP GETs work.

- What markup features are supported?
- How does the application sanitize user-supplied input?
- Is html5lib good enough?
- On docs.python.org, how are 1000 inappropriate (freeloading) comments from 
1000 different IPs deleted?
- What's the roadmap for {..., Akismet, ReCaptcha, ...} support?

(2) Freeloading jackass buys a block of javascript adspace on <insert-site-
here.xxx>. The block of javascript surreptitiously posts helpful comments on 
behalf of unwitting users.

- How does the application ensure that comments are submitted from the site 
hosting the documentation?
- Which frameworks have existing, reviewed CSRF protections?

Trying to read through the new source here [1], but there aren't many docstrings 
and BB doesn't yet support inline commenting. AFAIK, there are not yet any 
issues filed for these concerns. [2] 

1. In the event that that kind of bug is discovered, how should the community 
report the issues?
2. If we have an alternate method of encouraging documentation feedback, how can 
this feature be turned off?

Thanks again for a great publishing tool,
Don

[1] http://bitbucket.org/birkenfeld/sphinx
[2] http://bitbucket.org/birkenfeld/sphinx/issues/new


