
============================================================================
Subject: [Python-Dev] Python 2.6.6 release candidate 1 now available
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 2.6.6 release candidate 1 now available
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
Hello fellow Pythoneers and Pythonistas,

The source tarballs and Windows installers for the first (and hopefully only)
Python 2.6.6 release candidate is now available:

    http://www.python.org/download/releases/2.6.6/

As usual, we would love it if you could download, install, and test these with
your favorite projects and environments.  A truly impressive number of bug
have been fixed since Python 2.6.5, with the full NEWS file available here:

    http://www.python.org/download/releases/2.6.6/NEWS.txt

Barring complications, we expect to release Python 2.6.6 final on August 16,
2010.

Please note that with the release of Python 2.7 final on July 3, 2010, and in
accordance with Python policy, Python 2.6.6 is the last scheduled bug fix
maintenance release of the 2.6 series.  Because of this, your testing of this
release candidate will help immensely.  We will of course continue to support
security fixes in Python 2.6 for quite some time.

My thanks go out to everyone who has helped contribute fixes great and small,
and much testing and bug tracker gardening for Python 2.6.6.  The excellent
folks on #python-dev are true Pythonic heros too.

Enjoy,
-Barry
(on behalf of the Python development community)
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100804/cda7705c/attachment.pgp>

