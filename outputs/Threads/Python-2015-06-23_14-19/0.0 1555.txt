
============================================================================
Subject: [Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

As a mailing list, it was unmaintainable, since there was no tracking
of what patches still need consideration. So a web-based bug tracker
got into use (although I forgot the name of the tracker software that
was used before SourceForge).

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Mon, Jan 31, 2011 at 22:50, anatoly techtonik <techtonik at gmail.com> wrote:

Mercurial is a much smaller project, so it has different needs. It
would be nice if you could respect the process the developers on any
project have laid out for their project and assume they know what
works best for them.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
----------------------------------------
Author: =?UTF-8?B?xYF1a2FzeiBMYW5nYQ==?
Attributes: []Content: 
W dniu 2011-02-01 01:24, "Martin v. L?wis" pisze:

JitterBug!

Best regards,
?ukasz



----------------------------------------
Subject:
[Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Mon, Jan 31, 2011 at 10:54 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

If you don't want to receive a stupid answer, why don't you read the
link and say what you don't like in this approach in a constructive
manner?

http://mercurial.selenic.com/wiki/ContributingChanges#The_basics:_patches_by_email
-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Mon, Jan 31, 2011 at 15:50, anatoly techtonik <techtonik at gmail.com>wrote:



it can't be reviewed there, so it won't go anywhere!

We do fine with reviews on the tracker, and there has been some on and off
work on integrating Rietveld. For the people actually doing the work here,
accepting patches on the tracker and dealing with them there has been a
reasonably effective workflow, enough that we don't see a need to change it.

subscription necessary!

As you were directed to in an earlier email by Georg, there is now a way to
report bugs via email without requiring any subscription. *report*@*bugs*.*
python*.*org is the address.*
*
*
*>>> *Because this is a community project and our developers are very busy,
patches will sometimes fall through the cracks. If you've gotten no
response to your patch after a few days, feel free to resend it.

This is true of any workflow on just about any open source project. Whether
it's email or a bug tracker, not everything is going to be acknowledged,
reviewed, fixed, or rejected immediately. We feel that the tracker allows us
to, well, keep track of things. It works for us.


What they do works for them, and I'm sure it works great. Could it work for
python-dev? Maybe. Is it worth changing anything when no one who is doing
the actual work has voiced a need for change? Absolutely not.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110131/5566a307/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 31 Jan 2011 23:50:18 +0200
anatoly techtonik <techtonik at gmail.com> wrote:

Very simple: I don't want to be spammed with tons of patches, patch
reviews, and issue comments. Also, I want the history of issue
discussions to be easily accessible from permanent, issue-specific
URLs, rather than search through mailing-list archives to understand
why a change was made.

I appreciate that you refrained from giving a stupid answer, however.



----------------------------------------
Subject:
[Python-Dev] Mercurial style patch submission (Was: MSI: Remove
 dependency from win32com.client module (issue4080047))
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/1/31 anatoly techtonik <techtonik at gmail.com>:

As I understand it, there used to be patches at python.org. I'm not sure
why this was discontinued, so perhaps someone more senior should chime
in. :)



-- 
Regards,
Benjamin

