
============================================================================
Subject: [Python-Dev] binary operation heuristics -- bug or undocumented?
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] binary operation heuristics -- bug or undocumented?
----------------------------------------
Author: Alex A. Naano
Attributes: []Content: 
Hi,

A friend of mine stumbled upon the following behavior:


---cut---

...
...     def __add__(self, other):
...         print 'B: adding B and %s objects.' % other.__class__.__name__
...
...     def __radd__(self, other):
...         print 'C: adding C and %s objects.' % other.__class__.__name__
...

B: adding B and C objects.

C: adding C and A objects.


# so far, quite logical. now let's do this:

C: adding C and int objects.


--uncut--

My first expectation would be to get a TypeError here, as ints indeed
have an __add__ method, and they do not know anything about C objects
(obviously :) ). On second thought, giving client code priority to
handle things has it's merits.

The problem is that I found no mention of this behavior in the docs.


P.S. tested in 2.5 through 3.0 and PyPy

Thanks.

-- 
Alex.



----------------------------------------
Subject:
[Python-Dev] binary operation heuristics -- bug or undocumented?
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Fri, Mar 19, 2010 at 12:46 PM, Alex A. Naanou <alex.nanou at gmail.com> wrote:

Yes:  the int.__add__ method is tried first.  Since it doesn't know
anything about C objects it returns NotImplemented, and then
C.__radd__ is given a chance to do the addition.  The rules are
documented here:

http://docs.python.org/reference/datamodel.html#coercion-rules

Mark



----------------------------------------
Subject:
[Python-Dev] binary operation heuristics -- bug or undocumented?
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 19/03/2010 12:46, Alex A. Naanou wrote:

That is the whole point of the __radd__ method. ints don't know how to 
add themselves to C objects, so int.__add__ will return NotImplemented 
and then Python will try C.__radd__.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] binary operation heuristics -- bug or undocumented?
----------------------------------------
Author: Alex A. Naano
Attributes: []Content: 
Thanks!

On Fri, Mar 19, 2010 at 16:05, Michael Foord <fuzzyman at voidspace.org.uk> wrote:

P.S. like the licence :)




-- 
Alex.



----------------------------------------
Subject:
[Python-Dev] binary operation heuristics -- bug or undocumented?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/19/2010 8:46 AM, Alex A. Naanou wrote:

[snip]
Questions like this are more appropriately posted to python-list, where 
you would have gotten the same answer.

tjr



