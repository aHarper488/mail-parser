
============================================================================
Subject: [Python-Dev] 3.x as the official release
Post Count: 30
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 15 Sep 2010 10:21:11 -0400
Steve Holden <steve at holdenweb.com> wrote:

I don't think that's a good criterion. 95% of Python users (my
guesstimate) are on CPython, so whether or not alternative
implementations are up-to-date isn't critically important.

3.1 had some warts left (*), but 3.2 should really be a high-quality
release. Many bugs have been squashed, small improvements done
(including additional features in the stdlib, or the new GIL), and
unicode support has been polished again thanks to Martin's and Victor's
efforts. Not only will it be as robust as any 2.x release (**), but it's
also more pleasant to use, and there's upwards compatibility for many
years to come.

(*) some of them fixed in the 3.1 maintenance branch

(**) with a couple of lacking areas such as the email module, I suppose

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Wed, Sep 15, 2010 at 10:43 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

+0.5

The one area I have concerns about is the state of WSGI and other
web-oriented modules. These issues have been brought up by Armin and
others, but given a lack of a clear path forward (bugs, peps, etc), I
don't think it's fair to use it as a measurement of overall quality.

jesse



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Sep 15, 2010 at 07:50, Jesse Noller <jnoller at gmail.com> wrote:

The whole WSGI situation is not going to get cleared up (from my
understanding) until someone flat-out declares a winner in the whole
str/bytes argument that keeps coming up. I think it might be time to
have a PEP or two on this and use our new PEP dictator procedure to
settle this so it stops dragging on (unless it has been miraculously
settled and I am just unaware of it).



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Wed, Sep 15, 2010 at 12:22 PM, Brett Cannon <brett at python.org> wrote:
...snip...

Yup, and I spoke with some people with horses in that race at
Djangocon. The important thing is that the PEP(s) and suggestion come
from the people with the most experience in that domain. That's why I
said we (in the "committer" sense) need a clear path of things we need
to change or fix - without it we're just stabbing in the dark.

jesse



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
  On 15/09/2010 17:35, Jesse Noller wrote:

I agree. wsgi is a different kettle of fish to the other "web related 
modules" in the standard library though. (wsgiref is the only directly 
wsgi related standard library module IIUC.) email, cgi, nntplib, ftplib 
and friends all still need to work correctly with both bytes and strings 
and that shouldn't need a great deal of discussion (well perhaps except 
email) just people willing to do the work.

Unfortunately in some cases will need backwards incompatible changes. 
For example at the moment cgi reads from stdin in text mode and so is 
broken for file uploads. We have also heard recently from Antoine about 
backwards incompatible changes required in nntplib.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Sep 15, 2010 at 09:35, Jesse Noller <jnoller at gmail.com> wrote:

Yes. They have to be people who are not only stakeholders but people
who actively use and develop large applications using WSGI.


So, who do we get to write the PEP(s)? Should we ask the web-sig to
choose a person or two and then once we have the PEPs we designate PEP
dictators? Either way we should probably set a deadline to get the
PEPs in else the SIG might argue too long where to go look at paint
samples.



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Wed, Sep 15, 2010 at 12:55 PM, Brett Cannon <brett at python.org> wrote:

At Djangocon, I was told this is being discussed amongst people on
web-sig, and I encouraged a few people to get involved. I don't think
this is something we can set a deadline for (and I don't know that
it's *our* place), especially given a lack of people to actually write
the code in some cases. In at least one case, I've encouraged them to
contact the PSF with a proposal in case funding is needed (such as
your own, or Jean-Paul's work).

Fundamentally; I would gladly hold up 3.2 (just my opinion) for the
needed fixes to the standard lib I've heard discussed (once we have
bugs and/or patches) but that requires the decisions to be made, and I
don't think the people here are the ones to make the decisions - so we
can only state the release date of 3.2 and the subsequent releases and
let the people who know infinitely more about the nuances then us
decide on it.

jesse



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Jacob Kaplan-Mos
Attributes: []Content: 
On Wed, Sep 15, 2010 at 12:09 PM, Jesse Noller <jnoller at gmail.com> wrote:

I think I should share a little anecdote at this point:

Earlier in the year I worked for a while on Django/Py3. It's actually
not that hard of a task (because I'm building on the work by MvL and
some of Greg Wilson's students!) and I quickly got a simple app
working locally. So the next step for me was to see about putting the
app into production... and that's where the wheels fell off.

So that's where I stopped. As far as I'm concerned, I'm not willing to
expend the effort to get Django ported if I can't put it into
production. Most of us working on Django are going to feel the same
way, I suspect.

Further, I can say with some confidence that until the WSGI issue is
sorted the Python web world isn't going to have much enthusiasm for
Python 3.

I'm trying not to sound overly negative here -- really, I can't *WAIT*
to be able to switch to Py3! But until I have a bunch of
interoperable, robust WSGI servers like I do on Python 2 -- modwsgi,
uwsgi, cherrypy, gunicorn, ... -- Python 3 is going to remain a pipe
dream.

Jacob



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Sep 15, 2010 at 10:36, Jacob Kaplan-Moss <jacob at jacobian.org> wrote:

Which is why I would like to see this settled *now* rather than later.
It's Georg's call, but I'm also fine with holding up Python 3.2 *if*
we set a goal date to get this settled. If we release 3.2 without
these fixes we won't have a chance for wsgiref to get updated until
roughly June 2012 for Python 3.3 which will be 3.5 years since Python
3.0 was released.

The Python web development community is a big (and friendly) part of
the overall Python community. I think they deserve to have us do what
we can as the harbingers of the language (and by extension, the
technical aspect of the community as what we decide the community
takes queues from) to solve this issue to allow forward movement
towards using Python 3.



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Given that wsgiref is in the stdlib, I think we should hold up the 3.2
release (and even the first beta) until this is resolved, unless we
can convince ourselves that it's okay to delete wsgiref from the
stdlib (which sounds unlikely but may not be any more incompatible
than making it work properly :-).

I want to emphasize that I am *not* a stakeholder so my preference for
bytes or Unicode shouldn't matter; that said, given WSGI's traditional
emphasis on using the lowest-level, vanilla standard datatypes (e.g.
you can't even subclass dict let alone provide another kind of mapping
-- it has to be a real dict) it makes sense to me that the values
should be bytes, os.environ notwithstanding. The keys probably could
be Unicode (HTTP headers are required to use only 7-bit ASCII
characters anyways right?). But I'd be happy to be shown the error of
my ways (or given a link showing prior discussion of the matter --
preferably with a conclusion :-).

--Guido

On Wed, Sep 15, 2010 at 10:36 AM, Jacob Kaplan-Moss <jacob at jacobian.org> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Sep 15, 2010, at 11:11 AM, Guido van Rossum wrote:


I would much prefer holding up the release to fix wsgiref rather than remove
it.  I think it's an important module worthy of being in the stdlib.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100915/d9ad71ea/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Sep 15, 2010, at 7:50 AM, Jesse Noller wrote:

Any chance you're going to have time to do work on multiprocessing?
There are a huge number of bugs reports open for that module.


Raymond




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Wed, Sep 15, 2010 at 2:43 PM, Raymond Hettinger
<raymond.hettinger at gmail.com> wrote:

Trying to get that time; and I've recently brought on Ask Solem to
help me there, I concur that the current situation is sub optimal.



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On 9/15/2010 2:47 PM, Jesse Noller wrote:

Great that the "bus number" for multiprocessing has gone up by one!

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
DjangoCon US September 7-9, 2010    http://djangocon.us/
See Python Video!       http://python.mirocommunity.org/
Holden Web LLC                 http://www.holdenweb.com/




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Wed, Sep 15, 2010 at 2:58 PM, Steve Holden <steve at holdenweb.com> wrote:

No one is happier about this then me.



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: P.J. Eb
Attributes: []Content: 
At 11:11 AM 9/15/2010 -0700, Guido van Rossum wrote:

FWIW, I'd be fine with that option.



There isn't a conclusion yet, but the proposals under discussion are 
summarized here:

   http://www.wsgi.org/wsgi/Python_3#Proposals

The primary points of consensus are bytes for wsgi.input, and native 
strings (i.e. Unicode on Python 3) for environment keys.

If I were to offer a suggestion to a PEP author or dictator wanting 
to get something out ASAP, it would probably be to create a 
compromise between the "flat" model (my personal favorite) and the 
mod_wsgi model, as an addendum to PEP 333.  Specifically:

* leave start_response/write in play (ala mod_wsgi)

* use the required types from the "flat" proposal (i.e. status, 
headers, and output stream MUST be bytes)

* add a decorator to wsgiref that supports using native strings as 
output instead of bytes, for ease-of-porting (combine mod_wsgi's 
ease-of-porting w/"flat"'s simple verifiability)

This would probably allow us to get by with the least changes to 
existing code, the stdlib, the standard itself, and 
wsgiref.   (wsgiref itself would still need a thorough code review, 
especially wsgiref.validate, but it'd be unlikely to change much.)




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 15.09.2010 20:32, schrieb Barry Warsaw:

I also think this must be resolved before 3.2 can be released, and
especially in the case of fixing it (vs removing), it should happen
before beta 1.


Really?  I'd like to hear from some of its users first.

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Wed, 2010-09-15 at 14:59 -0400, Chris McDonough wrote:

That should read "as environ values" sorry.

- C





----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
For reference, I have developed a spec and an (untested) reference
implementation of a WSGI successor I've given the name "Web3".  Ian is
not hot on this spec (he prefers native strings as environ keys).  I'm
definitely not going to write a WebOb analogue, so I'd more or less
given up trying to promote it.  But it's here for consideration.  Given
that nobody else has written this all out in spec form, it may be
useful:

http://github.com/mcdonc/web3

- C


On Wed, 2010-09-15 at 11:11 -0700, Guido van Rossum wrote:





----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Sep 15, 2010, at 09:21 PM, Georg Brandl wrote:


I've used it.  When combined with something like restish it's about 20 lines
of code to publish a database-backed REST service.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100915/bbc4a279/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
On Wed, Sep 15, 2010 at 9:18 PM, P.J. Eby <pje at telecommunity.com> wrote:

Could we remove in any case the wsgiref.egg-info file ? Since we've
been working on a new format for that (PEP 376), that should be
starting to get used in the coming years, it'll be a bit of a
non-sense to have that metadata file in the sdtlib shipped with 3,2

I am not really sure what was the intent to have it there in the first
place though.

Regards
Tarek
-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Sep 15, 2010 at 12:45, Tarek Ziad? <ziade.tarek at gmail.com> wrote:

I'm also fine with removing the module if that is what it takes to
move all of this forward. That would remove Python 3.2's release cycle
from this picture while also allowing a finalization of WSGI specs to
continue in parallel.

But this probably shouldn't happen as Geremy's Cheeseshop #s put
wsgiref squarely in the middle of most used stdlib modules with 421
projects. So there are users of the module.


I say go ahead and remove it.



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 15/09/2010 21:45, Tarek Ziad? a ?crit :

+1 on removing wsgiref.egg-info in the same version that ships with
distutils2, or with 3.2 (in case they?re not the same version).


Unless I remember wrong, the intent was not to break code that used
pkg_resources.require('wsgiref').

Regards




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

Ah, thanks, I?ve been reading web-sig and was totally at a loss to
understand what a ?native string? was.  Now I get it?s a character
string / string / str object / former unicode object.

FWIW, I?m glad to see concern for web-sig in python-dev, and messages
from Web people in python-dev.  Some threads going ?they broke strings,
we can?t use the stdlib? were a bit alarming to me.

Regards




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Wed, Sep 15, 2010 at 21:18, P.J. Eby <pje at telecommunity.com> wrote:

The alternative is returning a three-tuple status, headers,
content-iterable, right?

I would definitely prefer just returning a three-tuple instead of the
crappy start_response callback that returns a write callable. It makes
applications easier to write, and the unified model should also make
server implemation easier. It also combines nicely with yield from in
some cases.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: P.J. Eb
Attributes: []Content: 
At 11:12 PM 9/15/2010 +0200, ??ric Araujo wrote:

More precisely, at the time it was done, setuptools was slated for 
inclusion in Python 2.5, and the idea was that when modules moved 
from PyPI to the stdlib, they would include the metadata so that 
projects requiring the module on an older version of Python would not 
need to use Python-version-dependent dependencies.

So, for example, if a package was written on 2.4 using a requirement 
of wsgiref, then that code would run unchanged on 2.5 using the 
stdlib-supplied copy.

In practice, this didn't work out in 2.x, and it's meaningless on 3.x 
where nothing has migrated yet from PyPI to stdlib AFAIK.  ;-)




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: P.J. Eb
Attributes: []Content: 
At 11:50 PM 9/15/2010 +0200, Dirkjan Ochtman wrote:

I would prefer it too (which is why the "flat" model is my favorite), 
but I think it would be easier to get a quick consensus for something 
that allows apps to be more mechanically ported from 2.x to 3.x.

That's why I said, "offer a suggestion to ... get something out ASAP".  ;-)




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 15/09/2010 21:45, Tarek Ziad? a ?crit :

On a related subject: Would it make sense not to run install_egg_info
from install anymore?  We probably can?t remove the command because of
backward compat, but we could stop running it (thus creating egg-info
files) by default.

Regards




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: P.J. Eb
Attributes: []Content: 
At 10:18 PM 9/16/2010 +0200, ??ric Araujo wrote:

If you're talking about distutils2 on Python 3, then of course 
anything goes: backward compatibility isn't an issue.  For 2.x, not 
writing the files would indeed produce backward compatibility problems.




----------------------------------------
Subject:
[Python-Dev] 3.x as the official release
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

I was talking about distutils in 3.2 (or in the release where
wsgiref.egg-info goes away).  install_egg_info.py has already been
turned into install_distinfo.py in distutils2, following PEP 376.

Thank you for your reply, I withdraw my suggestion.

Regards


