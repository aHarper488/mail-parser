
============================================================================
Subject: [Python-Dev] devguide (hg_transition): patchcheck does work
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devguide (hg_transition): patchcheck does work
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 27 Feb 2011 04:17:07 +0100
eric.araujo <python-checkins at python.org> wrote:

How does it find out which changesets it should operate on?





----------------------------------------
Subject:
[Python-Dev] devguide (hg_transition): patchcheck does work
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 27/02/2011 16:21, Antoine Pitrou a ?crit :

It operates on changed files, just like with Subversion:
http://hg.python.org/cpython/file/tip/Tools/scripts/patchcheck.py#l40
? hg status --added --modified --no-status

You can use it while making a commit, or when you collapse many commits
into one diff (and apply it with hg import --no-commit).

Rewriting patchcheck to work on diffs is another thing, see
http://bugs.python.org/issue8999#msg109255

I agree that the part about patchcheck should have a note to explain
that it works with uncommitted changes, not MQ patches or changesets.

Regards

