
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #12039: Add
 end_headers() call to avoid BadStatusLine.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #12039: Add
 end_headers() call to avoid BadStatusLine.
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Tue, May 10, 2011 at 10:10:15AM +0200, vinay.sajip wrote:

This is accurate. It should have resulted from the change made in the
http.server, because the headers are now cached and then written to
the output stream in one-shot when end_headers/flush_headers are
called. 

Thanks,
Senthil


