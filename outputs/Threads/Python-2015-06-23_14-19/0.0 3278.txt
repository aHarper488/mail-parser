
============================================================================
Subject: [Python-Dev] A reference leak with PyType_FromSpec
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] A reference leak with PyType_FromSpec
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hi,

As mentioned in the commit message for 96513d71e650, creating a type
using PyType_FromSpec seems to leak references when the type is
instantiated. This happens with SSLError:

35
36
37

(the SSLError definition is quite simple; it only uses the
Py_tp_base, Py_tp_doc and Py_tp_str slots)

The SSLError subclasses, e.g. SSLWantReadError, which are created using
PyErr_NewExceptionWithDoc, are not affected:

8
8
8

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] A reference leak with PyType_FromSpec
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 22 Jun 2012 21:23:10 +0200
Antoine Pitrou <solipsis at pitrou.net> wrote:

The patch in http://bugs.python.org/issue15142 seems to fix it.
Feedback welcome from typeobject experts :)

Regards

Antoine.



