
============================================================================
Subject: [Python-Dev] Add alternate float formatting styles to new-style
 formatting: allowed under moratorium?
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Add alternate float formatting styles to new-style
 formatting: allowed under moratorium?
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
http://bugs.python.org/issue7094 proposes adding "alternate" formatting 
[1] to floating point new-style formatting (float.__format__ and 
probably Decimal.__format__). I'd like to add this to make automated 
translation from %-formatting strings to str.format strings easier.

Would this be allowed under the moratorium? I think it falls under the 
Case-by-Case Exemptions section, but let me know what you think.

Eric.

[1] Alternate formatting for floats modifies how trailing zeros and 
decimal points are treated. See 
http://docs.python.org/dev/library/stdtypes.html#string-formatting-operations



----------------------------------------
Subject:
[Python-Dev] Add alternate float formatting styles to new-style
 formatting: allowed under moratorium?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Eric Smith wrote:

+1 to add it. We want the new-style formatting to be able to replace as
many old style uses as possible (preferably all of them) and this is a
well-defined formatting operation from C99.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Add alternate float formatting styles to new-style
 formatting: allowed under moratorium?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 2/24/2010 8:13 AM, Eric Smith wrote:

An excellent goal, for the reasons Nick stated.

I see the formating mini-language as somewhat separate from Python 
syntax itself (as is the re minilanguage). In any case, it is new and 
somewhat experimental, rather than being the result of 20 years of testing.

Terry Jan Reedy


