
============================================================================
Subject: [Python-Dev] ossaudiodev and linuxaudiodev not built on SLES11SP2
 due to change in sys.platform
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] ossaudiodev and linuxaudiodev not built on SLES11SP2
 due to change in sys.platform
----------------------------------------
Author: Wempa, Kristofe
Attributes: []Content: 

I am currently working on porting our Linux tool chains to SuSe Enterprise Linux 11 service pack 2 (SLES11SP2).  During this process, we noticed an issue due to a change in the sys.platform string.  Previously, the string was "linux2" and it has now changed to "linux3".  This seems to be due to a change in the kernel version.  This causes the ossaudiodev and linuxaudiodev modules to be omitted from the build.  I found the relevant code in setup.py:

        if platform == 'linux2':
            # Linux-specific modules
            exts.append( Extension('linuxaudiodev', ['linuxaudiodev.c']) )
        else:
            missing.append('linuxaudiodev')

        if platform in ('linux2', 'freebsd4', 'freebsd5', 'freebsd6',
                        'freebsd7', 'freebsd8'):
            exts.append( Extension('ossaudiodev', ['ossaudiodev.c']) )
        else:
            missing.append('ossaudiodev')

Since neither of these account for "linux3", they are both omitted from the build.  I can simply modify the code to include "linux3" so that these modules are built on the new platform.  However, I wanted to check and see whether they are specifically being omitted for a reason or if the setup file just wasn't ported and tested against the new platform.  Any help would be appreciated.  Thanks.

                Kris


________________________________

IMPORTANT: The information contained in this email and/or its attachments is confidential. If you are not the intended recipient, please notify the sender immediately by reply and immediately delete this message and all its attachments. Any review, use, reproduction, disclosure or dissemination of this message or any attachment by an unintended recipient is strictly prohibited. Neither this message nor any attachment is intended as or should be construed as an offer, solicitation or recommendation to buy or sell any security or other financial instrument. Neither the sender, his or her employer nor any of their respective affiliates makes any warranties as to the completeness or accuracy of any of the information contained herein or that this message or any of its attachments is free of viruses.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120521/0c3d346a/attachment.html>

