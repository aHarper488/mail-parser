
============================================================================
Subject: [Python-Dev] opinions on issue2142 ('\ No newline at end of file'
	to difflib.unified_diff)?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] opinions on issue2142 ('\ No newline at end of file'
	to difflib.unified_diff)?
----------------------------------------
Author: Trent Mic
Attributes: []Content: 
Soliciting opinions on issue 2142 (http://bugs.python.org/issue2142).
There are patched available so this isn't vapour. :)

The issue is this (I'll discuss only unified_diff(), but the same
applies to context_diff()):

...                    "one\ntwo\ntrois".splitlines(1))
---
+++
@@ -1,3 +1,3 @@
 one
 two
-three+trois

Where as with `diff`, `hg` and `git`:

diff -r 667b0870428d a
--- a/a Wed Oct 06 15:39:50 2010 -0700
+++ b/a Wed Oct 06 15:40:31 2010 -0700
@@ -1,3 +1,3 @@
 one
 two
-three
\ No newline at end of file
+trois
\ No newline at end of file



While originally marked as a *bug*, the issue was changed to be a
*feature* request, because arguably `difflib.unified_diff()` is fine,
and the problem is in the naive use of the following to create a patch
that will work with `patch`:

   ''.join(gen)


Possiblities:

1. Change `difflib.unified_diff` to emit:

    ['---  \n', '+++  \n', '@@ -1,3 +1,3 @@\n', ' one\n', ' two\n',
'-three\n', '\ No newline at end of file', '+trois\n', '\ No newline
at end of file']

instead of:

    ['---  \n', '+++  \n', '@@ -1,3 +1,3 @@\n', ' one\n', ' two\n',
'-three', '+trois']

for this case.


2. Add a `add_end_of_file_newline_markers_to_make_patch_happy` keyword
arg (probably with a different name:) to `difflib.unified_diff` to do
this additional handling. The reason is to not surprise existing code
that would be surprised with those "\No newline at end of file"
entries.

3. Not touch `difflib.unified_diff` and instead update
http://docs.python.org/library/difflib.html#difflib-interface
documentation to discuss the issue and show how users of unified_diff
should handle this case themselves.

Thoughts?



Orthogonal: *After* a decision is made for the Python 3.3 tree we can
discuss if including this in either of Python 2.7 or 3.2 would be
wanted.

-- 
Trent Mick

