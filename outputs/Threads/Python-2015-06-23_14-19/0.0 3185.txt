
============================================================================
Subject: [Python-Dev] AUTO: Jon K Peck is out of the office (returning
	06/06/2012)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] AUTO: Jon K Peck is out of the office (returning
	06/06/2012)
----------------------------------------
Author: Jon K Pec
Attributes: []Content: 


I am out of the office until 06/06/2012.

I will be out of the office Monday and Tuesday, June 4-5.  I expect to have
some email access but may be delayed in responding.


Note: This is an automated response to your message  "Python-Dev Digest,
Vol 107, Issue 9" sent on 06/04/2012 0:19:23.

This is the only notification you will receive while this person is away.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120604/f43a1e89/attachment.html>

