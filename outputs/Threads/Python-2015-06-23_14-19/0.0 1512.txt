
============================================================================
Subject: [Python-Dev] Unicode re support in Python 3
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Unicode re support in Python 3
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
Hey python-devers,

As I'm sure many of you are aware, Armin Ronacher posted a blog entry explaining the reasons he dislikes Python 3 in its current form. 

Whilst I don't agree with all of his complaints, he makes a fair point about the re module Unicode support. It seems that the specific issue he has could be fixed by accepting the re module improvement / overhaul implemented by mrab:

	http://bugs.python.org/issue2636

As it comes with an active maintainer, and is a big step forward for Python regex support, I'd like to see it in Python 3.3. Reading through the issue it's not clear to me what needs to be done for it to be accepted (or rejected), beyond a general "it's a big change". 

All the best,

Michael Foord

--
http://www.voidspace.org.uk/


May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing 
http://www.sqlite.org/different.html








----------------------------------------
Subject:
[Python-Dev] Unicode re support in Python 3
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 9 Dec 2011 14:42:43 +0000
Michael Foord <fuzzyman at voidspace.org.uk> wrote:

Reviewing. Do you volunteer?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Unicode re support in Python 3
----------------------------------------
Author: Bill Jansse
Attributes: []Content: 
Michael Foord <fuzzyman at voidspace.org.uk> wrote:


I've been using mrab's regex daily for about six months, and have found
it stable and useful.  It now includes two features which are both
unusual and useful (very Pythonic!), named lists and fuzzy matching.

Bill

