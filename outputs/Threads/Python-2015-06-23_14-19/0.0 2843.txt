
============================================================================
Subject: [Python-Dev] PEP 431 Time zone support improvements - Update
Post Count: 30
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
Happy Holidays! Here is the update of PEP 431 with the changes that emerged
after the earlier discussion.

A raw download is here:
https://raw.github.com/regebro/tz-pep/master/pep-04tz.txt

PEP: 431
Title: Time zone support improvements
Version: $Revision$
Last-Modified: $Date$
Author: Lennart Regebro <regebro at gmail.com>
BDFL-Delegate: Barry Warsaw
Status: Draft
Type: Standards Track
Content-Type: text/x-rst
Created: 11-Dec-2012
Post-History: 11-Dec-2012, 28-Dec-2012


Abstract
========

This PEP proposes the implementation of concrete time zone support in the
Python standard library, and also improvements to the time zone API to deal
with ambiguous time specifications during DST changes.


Proposal
========

Concrete time zone support
--------------------------

The time zone support in Python has no concrete implementation in the
standard library outside of a tzinfo baseclass that supports fixed offsets.
To properly support time zones you need to include a database over all time
zones, both current and historical, including daylight saving changes.
But such information changes frequently, so even if we include the last
information in a Python release, that information would be outdated just a
few months later.

Time zone support has therefore only been available through two third-party
modules, ``pytz`` and ``dateutil``, both who include and wrap the "zoneinfo"
database. This database, also called "tz" or "The Olsen database", is the
de-facto standard time zone database over time zones, and it is included in
most Unix and Unix-like operating systems, including OS X.

This gives us the opportunity to include the code that supports the zoneinfo
data in the standard library, but by default use the operating system's copy
of the data, which typically will be kept updated by the updating mechanism
of the operating system or distribution.

For those who have an operating system that does not include the zoneinfo
database, for example Windows, the Python source distribution will include a
copy of the zoneinfo database, and a distribution containing the latest
zoneinfo database will also be available at the Python Package Index, so it
can be easily installed with the Python packaging tools such as
``easy_install`` or ``pip``. This could also be done on Unices that are no
longer recieving updates and therefore has an outdated database.

With such a mechanism Python would have full time zone support in the
standard library on any platform, and a simple package installation would
provide an updated time zone database on those platforms where the zoneinfo
database isn't included, such as Windows, or on platforms where OS updates
are no longer provided.

The time zone support will be implemented by making the ``datetime`` module
into a package, and creating a new submodule called `timezone``, based on
Stuart Bishop's ``pytz`` module.


Getting the local time zone
---------------------------

On Unix there is no standard way of finding the name of the time zone that is
being used. All the information that is available is the time zone
abbreviations, such as ``EST`` and ``PDT``, but many of those abbreviations
are ambigious and therefore you can't rely on them to figure out which time
zone you are located in.

There is however a standard for finding the compiled time zone information
since it's located in ``/etc/localtime``. Therefore it is possible to create
a local time zone object with the correct time zone information even though
you don't know the name of the time zone. A function in ``datetime`` should
be provided to return the local time zone.

The support for this will be made by integrating Lennart Regebro's
``tzlocal`` module into the new ``timezone`` module.

For Windows it will look up the local Windows time zone name, and use a
mapping between Windows time zone names and zoneinfo time zone names provided
by the Unicode consortium to convert that to a zoneinfo timezone.

The mapping should be updated before each major or bugfix release, scripts
for doing so will be provided in the ``Tools/`` directory.


Ambiguous times
---------------

When changing over from daylight savings time the clock is turned back one
hour. This means that the times during that hour happens twice, once without
DST and then once with DST. Similarily, when changing to daylight savings
time, one hour goes missing.

The current time zone API can not differentiating between the two ambiguous
times during a change from DST. For example, in Stockholm the time of
2012-11-28 02:00:00 happens twice, both at UTC 2012-11-28 00:00:00 and also
at 2012-11-28 01:00:00.

The current time zone API can not disambiguate this and therefore it's
unclear which time should be returned::

    # This could be either 00:00 or 01:00 UTC:
    >>> dt = datetime(2012, 10, 28, 2, 0, tzinfo=timezone('Europe/Stockholm'))
    # But we can not specify which:
    >>> dt.astimezone(timezone('UTC'))
    datetime.datetime(2012, 10, 28, 1, 0, tzinfo=<UTC>)

``pytz`` solved this problem by adding ``is_dst`` parameters to several
methods of the tzinfo objects to make it possible to disambiguate times when
this is desired.

This PEP proposes to add these ``is_dst`` parameters to the relevant methods
of the ``datetime`` API, and therefore add this functionality directly to
``datetime``. This is likely the hardest part of this PEP as this involves
updating the C version of the ``datetime`` library with this functionality,
as this involved writing new code, and not just reorganizing existing
external libraries.


Implementation API
==================

The zoneinfo database
---------------------

The latest version of the zoneinfo database should exist in the
``Lib/tzdata`` directory of the Python source control system. This copy of
the database should be updated before every Python feature and bug-fix
release, but not for releases of Python versions that are in
security-fix-only-mode.

Scripts to update the database will be provided in ``Tools/``, and the
release instructions will be updated to include this update.


The new ``datetime.timezone``-module
------------------------------------

The public API of the new ``timezone``-module contains one new class, one new
function and one new exception.

* New class: ``DstTzInfo``

  This class provides a concrete implementation of the ``zoneinfo`` base
  class that implements DST support.


* New function :``timezone(name=None, db_path=None)``

  This function takes a name string that must be a string specifying a
  valid zoneinfo timezone, ie "US/Eastern", "Europe/Warsaw" or "Etc/GMT+11".
  If not given, the local timezone will be looked up. If an invalid zone name
  are given, or the local timezone can not be retrieved, the function raises
  `UnknownTimeZoneError`.

  The function also takes an optional path to the location of the zoneinfo
  database which should be used. If not specified, the function will look for
  databases in the following order:

  1. Use the database in ``/usr/share/zoneinfo``, if it exists.

  2. Check if the `tzdata-update` module is installed, and then use that
  database.

  3. Check the Python-provided database in ``Lib/tzdata``.

  If no database is found an ``UnknownTimeZoneError`` or subclass thereof will
  be raised with a message explaining that no zoneinfo database can be found,
  but that you can install one with the ``tzdata-update`` package.

* New Exception: ``UnknownTimeZoneError``

  This exception is a subclass of KeyError and raised when giving a time
  zone specification that can't be found::

     >>> datetime.Timezone('Europe/New_York')
     Traceback (most recent call last):
     ...
     UnknownTimeZoneError: There is no time zone called 'Europe/New_York'


Changes in the ``datetime``-module
----------------------------------

A new ``is_dst`` parameter is added to several of the `tzinfo` methods to
handle time ambiguity during DST changeovers.

* ``tzinfo.utcoffset(self, dt, is_dst=False)``

* ``tzinfo.dst(self, dt, is_dst=False)``

* ``tzinfo.tzname(self, dt, is_dst=False)``

The ``is_dst`` parameter can be ``False`` (default), ``True``, or ``None``.

``False`` will specify that the given datetime should be interpreted as not
happening during daylight savings time, ie that the time specified is after
the change from DST.

``True`` will specify that the given datetime should be interpreted as happening
during daylight savings time, ie that the time specified is before the change
from DST.

``None`` will raise an ``AmbiguousTimeError`` exception if the time specified
was during a DST change over. It will also raise a ``NonExistentTimeError``
if a time is specified during the "missing time" in a change to DST.

There are also three new exceptions:

* ``InvalidTimeError``

  This exception serves as a base for ``AmbiguousTimeError`` and
  ``NonExistentTimeError``, to enable you to trap these two separately. It
  will subclass from ValueError, so that you can catch these errors together
  with inputs like the 29th of February 2011.

* ``AmbiguousTimeError``

  This exception is raised when giving a datetime specification that
are ambigious
  while setting ``is_dst`` to None::

    >>> datetime(2012, 11, 28, 2, 0,
tzinfo=timezone('Europe/Stockholm'), is_dst=None)
    >>>
    Traceback (most recent call last):
    ...
    AmbiguousTimeError: 2012-10-28 02:00:00 is ambiguous in time zone
Europe/Stockholm


* ``NonExistentTimeError``

  This exception is raised when giving a datetime specification that
are ambigious
  while setting ``is_dst`` to None::

    >>> datetime(2012, 3, 25, 2, 0,
tzinfo=timezone('Europe/Stockholm'), is_dst=None)
    >>>
    Traceback (most recent call last):
    ...
    NonExistentTimeError: 2012-03-25 02:00:00 does not exist in time
zone Europe/Stockholm


The ``tzdata-update``-package
-----------------------------

The zoneinfo database will be packaged for easy installation with
``easy_install``/``pip``/``buildout``. This package will not install any
Python code, and will not contain any Python code except that which is needed
for installation.


Differences from the ``pytz`` API
=================================

* ``pytz`` has the functions ``localize()`` and ``normalize()`` to work
  around that ``tzinfo`` doesn't have is_dst. When ``is_dst`` is
  implemented directly in ``datetime.tzinfo`` they are no longer needed.

* ``timezone()``  will return the local time zone if called without parameters.

* The class ``pytz.StaticTzInfo`` is there to provide the ``is_dst``
support for static
  timezones. When ``is_dst`` support is included in
``datetime.tzinfo`` it is no longer needed.

* ``InvalidTimeError`` subclasses from ``ValueError``.


Discussion
==========

Should the windows installer include the data package?
------------------------------------------------------

It has been suggested that the Windows installer should include the data
package. This would mean that an explicit installation no longer would be
needed on Windows. On the other hand, that would mean that many using Windows
would not be aware that the database quickly becomes outdated and would not
keep it updated.


Resources
=========

* http://pytz.sourceforge.net/

* http://pypi.python.org/pypi/tzlocal

* http://pypi.python.org/pypi/python-dateutil

* http://unicode.org/cldr/data/common/supplemental/windowsZones.xml

Copyright
=========

This document has been placed in the public domain.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121228/35d3eaf4/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 28 Dec, 2012, at 21:23, Lennart Regebro <regebro at gmail.com> wrote:


Why is the new timezone support added in a submodule of datetime? Adding the new 
function and exception to datetime itself wouldn't clutter the API that much, and datetime 
already contains some timezone support (datetime.tzinfo). 

Ronald




----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Fri, Dec 28, 2012 at 10:12 PM, Ronald Oussoren <ronaldoussoren at mac.com>wrote:



Because several people wanted it that way and nobody objected.




It will make the datetime.py twice as long though, and the second longest
module in the stdlib, beaten only by decimal.py. Perhaps this is not a
problem.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121229/0acf4d46/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/12/28 Lennart Regebro <regebro at gmail.com>:

No one ever accused datetime manipulation of being simple.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Dec 29, 2012 at 6:23 AM, Lennart Regebro <regebro at gmail.com> wrote:

For UI purposes, "pytz" has some helpers to get lists of timezone
names (all, common and country specific):
http://pytz.sourceforge.net/#helpers

Is there a specific reason you chose to exclude those from the PEP?


I'm still a fan of *always* shipping fallback tzdata, regardless of
platform. The stdlib would then look in three places for timezone data
when datetime.timezone was first imported:

1. the "tzdata-update" database
2. the OS provided database
3. the fallback database

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 29 Dec 2012 16:00:39 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

+1 !

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Sat, Dec 29, 2012 at 11:45 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Yeah, from me as well.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sat, Dec 29, 2012 at 7:00 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:


Funnily enough, I woke up this morning thinking that this should be added,
and wondering why pytz didn't have such lists. So I just missed (or rather
forgot) that they existed. I'll add them.

Is there a specific reason you chose to exclude those from the PEP?


Yes, and I did update the rest of the PEP with this, but I missed the
discussion part.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121229/0977b350/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 12/29/2012 01:00 AM, Nick Coghlan wrote:


- -Lots for enabling fallback by default except on platforms known not to
have their own database, or given some explicit 'siteconfigure.py'-like
knob enabling it.  A clean error is better than a bad-but-silent answer.


Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)
Comment: Using GnuPG with undefined - http://www.enigmail.net/

iEYEARECAAYFAlDfOMEACgkQ+gerLs4ltQ7mfQCgxV13Ch7eW/yDwCPMfEebeNuY
xr0An1yvuUkVUQGY8nKDt9GxemdLlHMA
=JtY0
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sat, Dec 29, 2012 at 7:38 PM, Tres Seaver <tseaver at palladion.com> wrote:



Well, it's the same thing really. If the platform does have a database, the
fallback will not be used.
Of course, there is the case of the database existing on the platform
normally, but somebody for some reason deleting the files, but I don't
think that case deserves an error message.

I also expect that most platform distributions, such as for Ubuntu, will
not include the fallback database, as it will never be used. I'll add
something about that and that we need to raise an error of some sort (any
opinions on what?) if no database is found at all.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121229/e4efec26/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sat, Dec 29, 2012 at 7:54 PM, Lennart Regebro <regebro at gmail.com> wrote:


Actually I already added that, but opinions on what error to raise are
still welcome. Currently it says:

    If no database is found an ``UnknownTimeZoneError`` or subclass thereof
will
    be raised with a message explaining that no zoneinfo database can be
found,
    but that you can install one with the ``tzdata-update`` package.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121229/0a687fd1/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Arfrever Frehtes Taifersar Arahesi
Attributes: []Content: 
2012-12-29 19:54:42 Lennart Regebro napisa?(a):

I suggest that configure script support --enable-internal-timezone-database / --disable-internal-timezone-database options.
--disable-internal-timezone-database should cause that installational targets in Makefile would not install files of timezone
database.

-- 
Arfrever Frehtes Taifersar Arahesis
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: This is a digitally signed message part.
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121229/b4179427/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 29 Dec 2012 19:56:43 +0100
Lennart Regebro <regebro at gmail.com> wrote:

Why should we care about that situation if we *do* provide a database?
Distributions can decide to exclude some files from their packages, but
it's their problem, not ours.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sat, Dec 29, 2012 at 8:04 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:


Yes, but a comprehensible error message is useful even if somebody messed
up the system/configuration.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121229/d3557482/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 29Dec2012 21:16, Lennart Regebro <regebro at gmail.com> wrote:
| On Sat, Dec 29, 2012 at 8:04 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:
| > Why should we care about that situation if we *do* provide a database?
| > Distributions can decide to exclude some files from their packages, but
| > it's their problem, not ours.
| 
| Yes, but a comprehensible error message is useful even if somebody messed
| up the system/configuration.

Couldn't you just agree to augument the exception with some "I looked
here, there and there" information. It avoids a lot of bikeshedding and
makes things clear. You're not diagnosing system misconfiguration, just
saying "I can't find stuff, and here is where I looked".

Cheers,
-- 
Cameron Simpson <cs at zip.com.au>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/29/2012 3:16 PM, Lennart Regebro wrote:


Just reuse whatever exception type you create and add a sensible 
message. Hopefully, it will never be seen.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sat, Dec 29, 2012 at 11:55 PM, Cameron Simpson <cs at zip.com.au> wrote:


Since the location of the tzdata-update package isn't a fixed place it's
hard to say "I looked here, there and there", though. I don't think anyone
has suggested making any diagnostics. :-)

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121230/8dd643ef/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sat, Dec 29, 2012 at 11:59 PM, Terry Reedy <tjreedy at udel.edu> wrote:


I haven't implemented this, but I suspect the code will in the end look for
the tzdata module, which means that if it doesn't exist, the error raised
is an ImportError.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121230/d17aa8a0/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 30Dec2012 07:42, Lennart Regebro <regebro at gmail.com> wrote:
| On Sat, Dec 29, 2012 at 11:55 PM, Cameron Simpson <cs at zip.com.au> wrote:
| 
| > On 29Dec2012 21:16, Lennart Regebro <regebro at gmail.com> wrote:
| > | On Sat, Dec 29, 2012 at 8:04 PM, Antoine Pitrou <solipsis at pitrou.net>
| > wrote:
| > | > Why should we care about that situation if we *do* provide a database?
| > | > Distributions can decide to exclude some files from their packages, but
| > | > it's their problem, not ours.
| > |
| > | Yes, but a comprehensible error message is useful even if somebody messed
| > | up the system/configuration.
| >
| > Couldn't you just agree to augument the exception with some "I looked
| > here, there and there" information. It avoids a lot of bikeshedding and
| > makes things clear. You're not diagnosing system misconfiguration, just
| > saying "I can't find stuff, and here is where I looked".
| 
| Since the location of the tzdata-update package isn't a fixed place it's
| hard to say "I looked here, there and there", though. I don't think anyone
| has suggested making any diagnostics. :-)

I think I misunderstood the context. Never mind.
-- 
Cameron Simpson <cs at zip.com.au>

Displays are sold by the acre, not the function.
- overhead by WIRED at the Intelligent Printing conference Oct2006



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 29 Dec, 2012, at 5:48, Lennart Regebro <regebro at gmail.com> wrote:


The module could be split into several modules in a package without affecting the public API if that would help with maintenance, simular to unittest. 

Ronald

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121230/6a5a0834/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On 30/12/12 07:16, Lennart Regebro wrote:


+1


-- 
Steven





----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sun, Dec 30, 2012 at 10:47 AM, Ronald Oussoren <ronaldoussoren at mac.com>wrote:


This is of course true. Maybe that is a good idea.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121231/30c75720/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sat, Dec 29, 2012 at 8:05 PM, Arfrever Frehtes Taifersar Arahesis <
arfrever.fta at gmail.com> wrote:



Will this help the makers of distributions, like Ubuntu etc? If that is the
case, then it might be worth doing, otherwise I don't think it's very
useful.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130107/0cefb32b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Sun, Dec 30, 2012 at 10:47 AM, Ronald Oussoren <ronaldoussoren at mac.com>wrote:


Is there popular support for doing so, ie make datetime into a package,
have all of the pytz modules in there, but letting the public API go only
through datetime, ie no timezone submodule?

If the list decides so, I'll update the PEP.

//Lennart
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130107/bd1a2471/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Mon, Jan 7, 2013 at 5:57 PM, Lennart Regebro <regebro at gmail.com> wrote:

As a Gentoo packager, I think it's useful.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jan 7, 2013 at 12:01 PM, Lennart Regebro <regebro at gmail.com> wrote:

+1 for putting this under datetime; "Namespaces are one honking great
idea". People looking for date stuff is going to look in datetime or
time first so might as well put it there to begin with.



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Mon, Jan 7, 2013 at 1:06 PM, Brett Cannon <brett at python.org> wrote:
..

+1 for the same reason.



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Fri, Dec 28, 2012 at 10:12 PM, Ronald Oussoren <ronaldoussoren at mac.com>wrote:



Putting the API directly into the datetime module does conflict with the
new timezone class from Python 3.2. The timezone() function therefore needs
to be called something else, or the timezone class must be renamed.

Alternative names for the timezone() function is get_timezone(), which has
already been rejected, and zoneinfo() which makes it clear that it's only
zoneinfo timezones that are relevant.
Or the timezone class get's renamed TimeZone (which is more PEP8 anyway).

We can allow the timezone() function to take both timezone(offset, [name])
as now, and timezone(name) and return a TimeZone object in the first case
and a zoneinfo based timezone in the second case.

Or maybe somebody else can come up with more clever options?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130108/729f8201/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Jan 8, 2013 at 1:08 PM, Lennart Regebro <regebro at gmail.com> wrote:

Can't rename the class since that would potentially break code (e.g.
if the class can be pickled).


I'm personally +0 on zoneinfo(), but I don't have a better suggestion.

-Brett





----------------------------------------
Subject:
[Python-Dev] PEP 431 Time zone support improvements - Update
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Jan 9, 2013 at 5:02 AM, Brett Cannon <brett at python.org> wrote:

zoneinfo() sounds reasonable to me.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia

