
============================================================================
Subject: [Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 29 Jul 2011 14:35:24 +0200
eric.araujo <python-checkins at python.org> wrote:

Mercurial will prevent you from committing before you have solved
conflicts, so I'm not sure what this brings. "hg res -l" is the command
to remember when you want to list files with conflicts.

The fact that "make clean" doesn't wipe these files is an additional
annoyance.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 29/07/2011 14:50, Antoine Pitrou a ?crit :

People confused by the merge/resolve system could exit their merge tool
without actually merging the changes (I know it happened to me!), so
these files act as a reminder that not everything is right.

.orig is also created by hg revert; my usage is that I remove this file
after I?ve checked that the revert is okay.


make clean removes generated files, but *.rej and *.orig are backups,
which you may want to save or re-apply.  I?m not sure it would be right
to lose them.  Maybe distclean?

Regards



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 29 Jul 2011 15:28:44 +0200
?ric Araujo <merwok at netwok.org> wrote:

I don't know, I don't use a merge tool. But presumably the merge tool
would only call "hg resolve -m" on those files which have actually been
resolved? (if it calls that command at all)


What use are these backups really? We are using a (D)VCS, you are not
losing anything.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

You have it backwards: it?s hg resolve that runs a merge tool.  (BTW,
how can you not use a merge tool?  Even if you use internal hg tools
like the one that adds merge conflict markers, that?s a merge tool.)


The .orig files after a revert could contain code that?s not committed
anywhere.  See also RDM?s reply to your message.

Regards



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 29 Jul 2011 16:21:42 +0200
?ric Araujo <merwok at netwok.org> wrote:

I would point out that by using "hg revert", you deliberately want to
forget some local changes.

Besides, "hg status" is meant to show untracked files which could
*potentially* be tracked. It's not like anybody wants to track .orig
and .rej files, so having them in the ignore list is still the right
thing to do.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 29 Jul 2011 12:19:45 -0400
"R. David Murray" <rdmurray at bitdance.com> wrote:

Ok, I understand. However, it also makes things more tedious for other
people who don't user their VCS in such a way, so it would be nice how
other people feel about this.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Jul 29, 2011 at 7:22 PM, R. David Murray <rdmurray at bitdance.com> wrote:

Well, *I* don't ever want to check in .orig or .rej files (because
various tools create them) so I want them in my .hgignore file. Here's
a sample .hgignore file I carry around with me:
http://code.google.com/p/appengine-ndb-experiment/source/browse/.hgignore

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Stop ignoring Mercurial merge
 conflits files (#12255).
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 29 Jul 2011 22:22:05 -0400
"R. David Murray" <rdmurray at bitdance.com> wrote:


That's a good point. I hadn't thought about that.

Regards

Antoine.

