
============================================================================
Subject: [Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    >> Given that Sean is vouching for him I'm fine with it.

    Antoine> I'm not sure I agree. Of course it could be argued the risk is
    Antoine> minimal, but I think it's better if all people go through the
    Antoine> same path of proving their motivation and quality of work.  And
    Antoine> if there's something wrong with that process we'd better
    Antoine> address it than give random privileges to people we like :)

Let me expand on my original message.  Sean has been an integral part of the
Python community for many years, keeping much of our hardware and software
humming and providing critical network expertise at PyCon.  I don't think we
have to be such slaves to a set of rules that we can't use an implicit trust
network to make decisions in certain cases.  I trust Sean's judgement.

Skip



----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 26/04/2010 00:18, Steve Holden wrote:

Perhaps mentoring by an established committer could become a *standard* 
acceptable way to gain tracker privileges. It makes a lot of sense for 
the barriers to entry for bug triaging to be substantially lower than 
for commit privileges.

I agree that we should try and establish straightforward and consistent 
procedures, but also agree that those procedures should serve the 
community rather than vice-versa.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Sun, Apr 25, 2010 at 10:18:47PM +0000, Antoine Pitrou wrote:

I agree with Antoine's point here. As much as I respect Sean and his
contributions, it is important to consider the implications as it may
appear to others.

If you look at Daniel Diniz, who has enhanced tracker privileges, he
started off by using the normal tracker privilege commenting on
bugs, patches and within  *weeks*, he started triaging bugs with
enhanced privs. That kind of seems to me a middle-way, as in you start
off triaging in a normal mode  with a backing of mentor, it becomes a
easy way to upgrade very soon.

-- 
Senthil

Man must shape his tools lest they shape him.
		-- Arthur R. Miller



----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I most certainly does create work, but that could be as little as
sending an email message to some administrator.

There is no other way: somebody will have to make a decision, and that
is "work".

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,


I certainly agree we should try to attract more good-willed and
competent contributors.

I also agree with Stephen that, in a project with a non-trivial amount
of complexity such as CPython, not having (tracker or commit) privileges
is not the real barrier to entry. You have to learn how the software
works, how its development works, who are the people working on it, etc.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Scott Dia
Attributes: []Content: 
On 4/26/2010 7:24 AM, Antoine Pitrou wrote:

I'd like to respond to Michael's comment about the "possibly hundreds of
modules in the standard library without a maintainer." My own experience
(issue5949) has been positive despite the lack of a dedicated
maintainer. When I had my own itch to scratch, nobody stopped me from
scratching it.. some people told me when I could scratch it and how
they'd like it scratched.. but I wasn't ignored or rejected despite the
lack of a maintainer. Thanks to RDM for giving my issue attention.

-- 
Scott Dial
scott at scottdial.com
scodial at cs.indiana.edu



----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 26/04/2010 12:40, Scott Dial wrote:
Right, but finding counterexamples is not at all hard...

Michael

-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 26/04/2010 12:24, Antoine Pitrou wrote:

So the question remains - for *tracker* privileges, should the 
recommendation and commitment to mentor from an established commiter be 
sufficient (and therefore a standard part of our process)?

I think this is a reasonable barrier for entry and should be acceptable. 
It should be stated as part of our standard procedure however rather 
than being an exception to our standard procedure.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
R. David Murray wrote:
[...]
For which work I am truly grateful, as I am sure are many others. Please
forgive any prickliness I may have evinced in this conversation. It *is*
important to make people feel welcome, and I am happy to see the
development community growing.

As regards the procedural discussions, while I may have my opinions it's
clearly best if the procedures are maintained by those operating them. I
am usually fine with that happening, and this is no exception.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
See PyCon Talks from Atlanta 2010  http://pycon.blip.tv/
Holden Web LLC                 http://www.holdenweb.com/
UPCOMING EVENTS:        http://holdenweb.eventbrite.com/



----------------------------------------
Subject:
[Python-Dev] Enhanced tracker privileges for dangerjim to do
 triage.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
R. David Murray wrote:

Having a recommendation officially mean accelerating-but-not-skipping
the familiarisation period doesn't seem to have met with any significant
objections.

We basically do that anyway, this would just mean adding a note about it
to the "getting involved" documentation.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

