
============================================================================
Subject: [Python-Dev] ESHUTDOWN
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] ESHUTDOWN
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Some more digging indicates that ESHUTDOWN appears in asyncore with the
following commit:

changeset:   10934:c089020a7a1e
branch:      legacy-trunk
user:        Guido van Rossum <guido at python.org>
date:        Tue Jun 08 13:20:05 1999 +0000
files:       Lib/asynchat.py Lib/asyncore.py
description:
Sam's latest versions


while it appears in errnomodule.c with the following commit:

changeset:   3804:48776bf4bd49
branch:      legacy-trunk
user:        Guido van Rossum <guido at python.org>
date:        Wed Jul 24 00:51:51 1996 +0000
files:       Modules/Setup.in Modules/errnomodule.c
description:
Added Sam Rushing's errno module


It also seems that WSAESHUTDOWN can be returned under Windows by
send(), rather than EPIPE:

?WSAESHUTDOWN

The socket has been shut down; it is not possible to send on a socket
after shutdown has been invoked with how set to SD_SEND or SD_BOTH.?

Regards

Antoine.



