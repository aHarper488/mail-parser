
============================================================================
Subject: [Python-Dev] PEP for allowing 'raise NewException from None'
Post Count: 21
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
PEP: XXX
Title: Interpreter support for concurrent programming
Version: $Revision$
Last-Modified: $Date$
Author: Ethan Furman <ethan at stoneleaf.us>
Status: Draft
Type: Standards Track
Content-Type: text/x-rst
Created: 26-Jan-2012
Python-Version: 3.3
Post-History:


Abstract
========

One of the open issues from PEP 3134 is suppressing context:  currently 
there is no way to do it.  This PEP proposes one.

Motivation
==========

There are two basic ways to generate exceptions: 1) Python does it 
(buggy code, missing resources, ending loops, etc.); and, 2) manually 
(with a raise statement).

When writing libraries, or even just custom classes, it can become 
necessary to raise exceptions; moreover it can be useful, even 
necessary, to change from one exception to another.  To take an example 
from my dbf module:

     try:
         value = int(value)
     except Exception:
         raise DbfError(...)

Whatever the original exception was (ValueError, TypeError, or something 
   else) is irrelevant.  The exception from this point on is a DbfError, 
and the original exception is of no value.  However, if this exception 
is printed, we would currently see both.


Alternatives
============
Several possibilities have been put forth:

   - raise as NewException()

     Reuses the 'as' keyword; can be confusing since we are not really 
reraising the originating exception

   - raise NewException() from None

     Follows existing syntax of explicitly declaring the originating 
exception

   - exc = NewException(); exc.__context__ = None; raise exc

     Very verbose way of the previous method

   - raise NewException.no_context(...)

     Make context suppression a class method.

All of the above options will require changes to the core.


Proposal
========

I proprose going with the second option:

     raise NewException from None

It has the advantage of using the existing pattern of explicitly setting 
the cause:

     raise KeyError() from NameError()

but because the 'cause' is None the previous context is discarded. 
There is a patch to this effect attached to Issue6210 
(http://bugs.python.org/issue6210).


Copyright
=========

This document has been placed in the public domain.


..
    Local Variables:
    mode: indented-text
    indent-tabs-mode: nil
    sentence-end-double-space: t
    fill-column: 70
    coding: utf-8
    End:




----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/1/26 Ethan Furman <ethan at stoneleaf.us>:

mm?


BTW, I don't really think this needs a PEP.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jan 26, 2012, at 10:54 PM, Benjamin Peterson wrote:


I think a PEP is appropriate, but the title is certainly misnamed.

-Barry



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Benjamin Peterson wrote:

Oops!


I was surprised, but Nick seems to think it is.

If somebody could fix that oopsie, and any others ;) and then commit it 
(if necessary) I would appreciate it.

~Ethan~



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/1/26 Ethan Furman <ethan at stoneleaf.us>:

Obviously it doesn't hurt. And I see from the issue that the change
was not as uncontroversial as I originally thought, so it's likely for
the better.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Jan 27, 2012 at 1:54 PM, Benjamin Peterson <benjamin at python.org> wrote:

That's largely my influence - the discussion in the relevant tracker
item (http://bugs.python.org/issue6210) had covered enough ground that
I didn't notice that Ethan's specific proposal *isn't* a syntax
change, but is rather just a matter of giving some additional
semantics to the "raise X from Y" syntax (some of the other
suggestions like "raise as <whatever>" really were syntax changes).

So I've changed my mind to being +1 on the idea and proposed syntax of
the draft PEP, but I think there are still some details to be worked
through in terms of the detailed semantics. (The approach in Ethan's
patch actually *clobbers* the context info when "from None" is used,
and I don't believe that's a good idea. My own suggestions in the
tracker item aren't very good either, for exactly the same reason)

Currently, the raise from syntax is just syntactic sugar for setting
__cause__ manually:

...     1/0
... except ZeroDivisionError as ex:
...     new_exc = ValueError("Denominator is zero")
...     new_exc.__cause__ = ex
...     raise new_exc
...
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
ZeroDivisionError: division by zero

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
  File "<stdin>", line 6, in <module>
ValueError: Denominator is zero

The context information isn't lost in that case, the display of it is
simply suppressed when an explicit cause is set:

...     try:
...         1/0
...     except ZeroDivisionError as ex:
...         new_exc = ValueError()
...         new_exc.__cause__ = ex
...         raise new_exc
... except ValueError as ex:
...     saved = ex
...
ZeroDivisionError('division by zero',)
ZeroDivisionError('division by zero',)

This behaviour (i.e. preserving the context, but not displaying it by
default) is retained when using the dedicated syntax:

...     try:
...         1/0
...     except ZeroDivisionError as ex:
...         raise ValueError() from ex
... except ValueError as ex:
...     saved = ex
...
ZeroDivisionError('division by zero',)
ZeroDivisionError('division by zero',)

However, if you try to set the __cause__ to None explicitly, then the
display falls back to showing the context:

...     1/0
... except ZeroDivisionError as ex:
...     new_exc = ValueError("Denominator is zero")
...     new_exc.__cause__ = None
...     raise new_exc
...
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
ZeroDivisionError: division by zero

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "<stdin>", line 6, in <module>
ValueError: Denominator is zero

This happens because None is used by the exception display logic to
indicate "no specific cause, so report the context if that is set".

My proposal would be that instead of using None as the "not set"
sentinel value for __cause__, we instead use a dedicated sentinel
object (exposed to Python at least as "BaseException().__cause__", but
potentially being given its own name somewhere).

Then the display logic for exceptions would be changed to be:
- if the __cause__ is None, then don't report a cause or exception
context at all
- if the __cause__ is BaseException().__cause__, report the exception
context (from __context__)
- otherwise report __cause__ as the specific cause of the raised exception

That way we make it easy to emit nicer default tracebacks when
replacing exceptions without completely hiding the potentially useful
data that can be provided by retaining information in __context__.

I've been burnt by too much code that replaces detailed, informative
and useful error messages that tell me exactly what is going wrong
with bland, useless garbage to be in favour of an approach that
doesn't even set the __context__ attribute in the first place. If
__context__ is always set regardless, and then __cause__ is used to
control whether or not __context__ gets displayed in the standard
tracebacks, that's a much more flexible approach.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Thu, Jan 26, 2012 at 9:18 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Ditto here.


Well, but usually all you see is the printed traceback, so it might as
well be lost, right? (It gives full control to programmatic handlers,
of course, but that's usually not where the problem lies. It's when
things go horribly wrong in the hash function and all you see in the
traceback is a lousy KeyError. :-) Did you consider to just change the
words so users can ignore it more easily?

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/1/26 Ethan Furman <ethan at stoneleaf.us>:

Congratulations, you are now PEP 409.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Guido van Rossum wrote:

Yes, that has also been discussed.

Speaking for myself, it would be only slightly better.

Speaking for everyone that wants context suppression (using Steven 
D'Aprano's words):  chained exceptions expose details to the caller that 
are irrelevant implementation details.

It seems to me that generating the amount of information needed to track 
down errors is a balancing act between too much and too little; forcing 
the print of previous context when switching from exception A to 
exception B feels like too much:  at the very least it's extra noise; at 
the worst it can be confusing to the actual problem.  When the library 
(or custom class) author is catching A, saying "Yes, expected, now let's 
raise B instead", A is no longer necessary.

Also, the programmer is free to *not* use 'from None', leaving the 
complete traceback in place.

~Ethan~



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Jan 27, 2012 at 9:08 AM, Ethan Furman <ethan at stoneleaf.us> wrote:

Ok, got it. The developer has to explicitly say "raise <something>
from None" and that indicates they have really thought about the issue
of suppressing too much information and they are okay with it. I dig
that.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 1/27/2012 2:54 PM, Guido van Rossum wrote:

Especially if the users are non-programmer app users.


I find double tracebacks to be 'jarring'. If there is a double bug, one 
in both the try and except blocks, it *should* stand out. If there is 
just one bug and the developer merely wants to rename it and change the 
message, it should not.

Now that I have been reminded that 'from x' was already added to raise 
statements, I am fine with reusing that. I still think it 'sticks out' 
more than the 'as' version, but when reading code, having (rare) info 
suppression stick out is not so bad.

The PEP does not address the issue of whether the new variation of raise 
is valid outside of an except block. My memory is that it was not to be 
and I think it should not be. One advantage of the 'as' form is that it 
is clear that raising the default as something else is invalid if there 
is no default.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Terry Reedy wrote:

Or beginner programmers, e.g. on the python-list and tutor mailing lists. It 
is hard enough to get beginners to post the entire traceback without making 
them bigger. The typical newbie posts just the error message, sometimes not 
even the exception type. What they will make of chained exceptions, I hate to 
think.



Agreed with all of this.


[...]

I think that raise ... from None should be illegal outside an except block. My 
reasoning is:

1) It ensures that raise from None only occurs when the developer can see
    the old exception right there, and not "just in case".

2) I can't think of any use-cases for raise from None outside of an
    except block.

3) When in doubt, start with something more restrictive, because it is
    easier to loosen the restriction later if it turns out to be too much,
    than to change our mind and add the restriction afterwards.


-- 
Steven



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Terry Reedy wrote:

Were you speaking of the original (PEP 3134), or this new one (PEP 409)?

Because at this point it is possible to do:

     raise ValueError from NameError

outside a try block.  I don't see it as incredibly useful, but I don't 
know that it's worth making it illegal.

So the question is:

   - should 'raise ... from ...' be legal outside a try block?

   - should 'raise ... from None' be legal outside a try block?

~Ethan~



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Jan 28, 2012 at 10:33 AM, Ethan Furman <ethan at stoneleaf.us> wrote:

Given that it would be quite a bit of work to make it illegal, my
preference is to leave it alone.

I believe that means there's only one open question. Should "raise ex
from None" be syntactic sugar for:

1. clearing the current thread's exception state (as I believe Ethan's
patch currently does), thus meaning that __context__ and __cause__
both end up being None
2. setting __cause__ to None (so that __context__ still gets set
normally, as it is now when __cause__ is set to a specific exception),
and having __cause__ default to a *new* sentinel object that indicates
"use __context__"

I've already stated my own preference in favour of 2 - that approach
means developers that think about it can explicitly change exception
types such that the context isn't displayed by default, but
application and framework developers remain free to insert their own
exception handlers that *always* report the full exception stack.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jan 26, 2012, at 7:19 PM, Ethan Furman wrote:



Thanks for proposing fixes to this issue. 
It is an annoying problem.


Raymond

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120127/2f5329dc/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Nick Coghlan wrote:

The reasoning behind choice two makes a lot of sense.  My latest effort 
(I should be able to get the patch posted within two days) involves 
creating a new dummy exception, SuppressContext, and 'raise ... from 
None' sets cause to it; the printing logic checks to see if cause is 
SuppressContext, and if so, prints neither context nor cause.

Not exactly how Nick describes it, but as far as I've gotten in my 
Python core hacking skills.  ;)

~Ethan~



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Benjamin Peterson wrote:

Thanks, Benjamin!

So, how do I make changes to it?

~Ethan~



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
For those not on the nosy list, here's the latest post
to http://bugs.python.org/issue6210:

-------------------------------------------------------

It looks like agreement is forming around the

     raise ... from None

method.  It has been mentioned more than once that having the context 
saved on the exception would be a Good Thing, and for further debugging 
(or logging or what-have-you) I must agree.

The patch attached now sets __cause__ to True, leaving __context__ 
unclobbered.  The exception printing routine checks to see if __cause__ 
is True, and if so simply skips the display of either cause or 
__context__, but __context__ can still be queried by later code.

One concern raised was that since it is possible to write (even before 
this patch)

     raise KeyError from NameError

outside of a try block that some would get into the habit of writing

     raise KeyError from None

as a way of preemptively suppressing implicit context chaining;  I am 
happy to report that this is not an issue, since when that exception is 
caught and a new exception raised, it is the new exception that controls 
the display.

In other words:

 >>> >>> try:
...   raise ValueError from None
... except:
...   raise NameError
...
Traceback (most recent call last):
   File "<stdin>", line 2, in <module>
ValueError

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
   File "<stdin>", line 4, in <module>
NameError



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 29.01.2012 08:42, schrieb Ethan Furman:

Please send PEP updates to the PEP editors at peps at python.org.

Georg




----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Latest addition for PEP 409 has been sent.  Text follows:

Language Details
================

Currently, __context__ and __cause__ start out as None, and then get set
as exceptions occur.

To support 'from None', __context__ will stay as it is, but __cause__
will start out as False, and will change to None when the 'raise ...
from None' method is used.

If __cause__ is False the __context__ (if any) will be printed.

If __cause__ is None the __context__ will not be printed.

if __cause__ is anything else, __cause__ will be printed.

This has the benefit of leaving the __context__ intact for future
logging, querying, etc., while suppressing its display if it is not caught.

raise ... from ... is not disallowed outside a try block, but this
behavior is not guaranteed to remain.


------------------------------------------------------------------

Should that last disclaimer be there?  Should it be changed?

~Ethan~



----------------------------------------
Subject:
[Python-Dev] PEP for allowing 'raise NewException from None'
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Jan 30, 2012 at 2:51 PM, Ethan Furman <ethan at stoneleaf.us> wrote:

I'd leave it out - the original PEP didn't disallow it, enforcing it
would be annoying, and it's easy enough to pick up if you happen to
happen to care (it will mean __cause__ is set along with __context ==
None).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

