
============================================================================
Subject: [Python-Dev] wiki/docs enhancement idea
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] wiki/docs enhancement idea
----------------------------------------
Author: Gerard Flanaga
Attributes: []Content: 
I didn't know who to reply to in the previous thread. (Moving the 
Developer docs/ Python wiki).

scraperwiki.org is a 'site scraper automater'. I threw together a script 
just now which scrapes certain specified pages from the python wiki and 
converts to something rest-like. It runs every 24hrs and the idea would 
be that the result could be included alongside the official docs. This 
may increase people's motivation to contribute to the wiki.

Script here: http://scraperwiki.com/scrapers/pywiki2rest/edit/

Example api call:

http://api.scraperwiki.com/api/1.0/datastore/getdata?format=json&name=pywiki2rest


Regards


