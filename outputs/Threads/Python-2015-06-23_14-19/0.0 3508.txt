
============================================================================
Subject: [Python-Dev] Python 3.3 cannot import BeautifulSoup but Python 3.2
	can
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 3.3 cannot import BeautifulSoup but Python 3.2
	can
----------------------------------------
Author: Edward C. Jone
Attributes: []Content: 
I use up-to-date Debian testing (wheezy), amd64 architecture.  I compiled
and installed Python 3.3.0 alpha 3 using "altinstall".  Debian wheezy comes
with python3.2 (and 2.6 and 2.7).  I installed the Debian package
python3-bs4 (BeautifulSoup4 for Python3).  I also downloaded a "clone"
developmental copy of 3.3.

Python3.3a3 cannot find module bs4.  Neither can the "clone".  Python3.2 can
find the module.  Here is a session with the "clone":

 > ./python
Python 3.3.0a3+ (default:10ccbb90a8e9, May  6 2012, 19:11:02)
[GCC 4.6.3] on linux
Type "help", "copyright", "credits" or "license" for more information.
 >>> import bs4
Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
   File "<frozen importlib._bootstrap>", line 974, in _find_and_load
ImportError: No module named 'bs4'
[71413 refs]
 >>>

What is the problem?


