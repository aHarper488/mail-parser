
============================================================================
Subject: [Python-Dev] Python accepted as a GSoC 2013 mentoring organization!
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python accepted as a GSoC 2013 mentoring organization!
----------------------------------------
Author: Terri Od
Attributes: []Content: 
Congratulations all; we've been accepted as a mentoring organization for 
Google Summer of Code 2013!


Students can check out our ideas page here:
http://wiki.python.org/moin/SummerOfCode/2013


If you're interested in mentoring with the PSF and aren't already on the 
2013 mentors mailing list, please get in touch with me ASAP.

  Terri


