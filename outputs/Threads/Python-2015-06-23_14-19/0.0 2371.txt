
============================================================================
Subject: [Python-Dev] Is Python insider blog dead?
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Is Python insider blog dead?
----------------------------------------
Author: =?utf-8?Q?Jes=C3=BAs_Cea?
Attributes: []Content: 
Python insider blog was a great idea, trying to open and expose python-dev to the world. A great and necessary idea.

But the last post was in August.

I wonder if the project is dead... Would be sad :-(

http://blog.python.org/

Enviado desde mi iPhone



----------------------------------------
Subject:
[Python-Dev] Is Python insider blog dead?
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
No. I think, you are welcome to write something about the recent
changes you made to Python.

-- 
Senthil

On Wed, Nov 16, 2011 at 12:40:06PM +0100, Jes?s Cea wrote:



----------------------------------------
Subject:
[Python-Dev] Is Python insider blog dead?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Wed, Nov 16, 2011 at 05:40, Jes?s Cea <jcea at jcea.es> wrote:



Not dead, there was just a period where I got a little too busy with real
life, plus development seemed to slow down for a while. I have a few drafts
working (like a post on all of the recent PEP activity) and a few more in
my head, but I'd like for it to not be a one-man show :)

I've been planning to do another push to get people from around here to
write about their big commits, what's going on in the areas of code they
work on, interesting bugs they've fixed, etc. Now that you mentioned this,
I'll get going quicker and send out details in the next day or so.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111116/a553e54a/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Is Python insider blog dead?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le Mercredi 16 Novembre 2011 07:23:03 Brian Curtin a ?crit :

Some interesting topics for this blog:

 - recent implemented PEP: 393 (Unicode) and 3151 (exceptions)
 - sys.platform and Linux 3
 - deprecation of bytes filename on Windows

For PEP 393, I still have a question: does old module benefit of the memory 
reduction or not? If a string is created using the old API, it only uses a 
wchar_t* buffer. When the string is read using the new API, it is converted to 
use the best storage (UCS 1/2/4) and the wchar_t* buffer is freed, so the 
memory consumption is reduced. The problem is maybe if you access the string 
again using the old API: Python recreates the wchar_t* buffer and so the string 
has two storages (double memory usage which is worse than Python 3.2). But I 
don't understand if this case can happen or not.

FYI I added a "Deprecated" section to the What's New in Python 3.3 document:
http://docs.python.org/dev/whatsnew/3.3.html#deprecated-modules-functions-and-
methods.

Victor



----------------------------------------
Subject:
[Python-Dev] Is Python insider blog dead?
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 16.11.2011 14:41, schrieb Victor Stinner:

I've already blogged about the Linux 3 topic two months ago. You are
welcome to use my posting as a reference.

http://lipyrary.blogspot.com/2011/09/python-and-linux-kernel-30-sysplatform.html

Christian




----------------------------------------
Subject:
[Python-Dev] Is Python insider blog dead?
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 16 November 2011 13:23, Brian Curtin <brian at python.org> wrote:

I had planned to do an article on the new packaging features, but the
discussion hasn't really come to any conclusions yet, and to be honest
I don't think that everything has settled enough yet (particularly on
binary installers)[1]. Maybe I could do a short post saying something
along the lines of "packaging is coming in 3.3, it's available now in
the form of distutils2, please try it out, work out what you like and
what you have problems with, and come and contribute on
python-dev"...?

Paul.

[1] Also real life has left me with little or no spare time once more :-)

