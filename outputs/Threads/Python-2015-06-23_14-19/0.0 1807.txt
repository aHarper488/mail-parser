
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #12452: Plist and
 Dict are now deprecated
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #12452: Plist and
 Dict are now deprecated
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mardi 05 juillet 2011 ? 07:59 -0400, Eric Smith a ?crit :

Plist and Dict were never documented (in Doc/library/plistlib.rst).
These classes have no test.

You mean that I should add an entry to Misc/NEWS saying that these
classe are now deprecated? Should I also mention the deprecation to the
"What's new in Python 3.3?" document?

Victor




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #12452: Plist and
 Dict are now deprecated
----------------------------------------
Author: Eric Smit
Attributes: []Content: 

Ouch!


Yes. I think this should make it into a "What's new" document, usually
via Misc/NEWS. Thanks.

Eric.

