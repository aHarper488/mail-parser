
============================================================================
Subject: [Python-Dev] Unicode identifiers
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Unicode identifiers
----------------------------------------
Author: Egon Smiw
Attributes: []Content: 
Hi all,
I wanted to implement quantity objects in a software,
which can be used with user-friendly expressions like:
money = 3 * ?, where Euro is a special quantity object
But now I realized, Python does not allow currency
characters in names, although they can be very useful.
Is there a really convincing argument against the inclusion?

Thank you!




----------------------------------------
Subject:
[Python-Dev] Unicode identifiers
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2011/9/20 Egon Smiwa <smiwa.egon at googlemail.com>:

It's a violation of http://unicode.org/reports/tr31/



-- 
Regards,
Benjamin

