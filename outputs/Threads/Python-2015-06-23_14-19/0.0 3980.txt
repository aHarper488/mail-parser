
============================================================================
Subject: [Python-Dev] Rationale for different signatures of
 tuple.__new__ and namedtuple.__new__
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Rationale for different signatures of
 tuple.__new__ and namedtuple.__new__
----------------------------------------
Author: Hrvoje Niksi
Attributes: []Content: 
On 02/18/2013 03:32 PM, John Reid wrote:

Sharing the constructor signature with tuple would break the common case of:

namedtuple('B', 'x y z')(1, 2, 3)


