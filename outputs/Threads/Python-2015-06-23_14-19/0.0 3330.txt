
============================================================================
Subject: [Python-Dev] PEP 414 updated
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
My rewritten version of PEP 414 is now up
(http://www.python.org/dev/peps/pep-0414/). It describes in detail a
lot more of the historical background that was taken as read when
Guido accepted the PEP.

Can we let the interminable discussion die now?

Please?

Regards,
Nick.

P.S. If you find an actual factual *error* in the PEP, let me know by
private email. If you just disagree with Guido's acceptance of the
PEP, or want to quibble about my personal choice of wording on a
particular point, please just let it rest.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Nick Coghlan <ncoghlan <at> gmail.com> writes:


Nice work - thanks!

I've implemented a first attempt at an import hook as mentioned in the PEP:

https://bitbucket.org/vinay.sajip/uprefix/

It's used as follows: assume you have a simple package hierarchy of code
containing u-prefixed literals:

frob
+-- __init__.py
+-- subwob
|   +-- __init__.py
|   +-- subsubwob.py
+-- wob.py

with the following contents:

# frob/subwob/__init__.py
z = u'def'
#-------------------------
# frob/subwob/subsubwob.py
w = u'tuv'
#-------------------------
# frob/__init__.py
y = u'xyz'
#-------------------------
# frob/wob.py
x = u'abc'
#-------------------------

You can now import these in Python 3.2 using the hook:

Python 3.2.2 (default, Sep  5 2011, 21:17:14) 
[GCC 4.6.1] on linux2
Type "help", "copyright", "credits" or "license" for more information.
'tuv'
<module 'frob.subwob' from 'frob/subwob/__init__.py'>
'def'
'abc'
<module 'frob' from 'frob/__init__.py'>
'xyz'

The "import uprefix; uprefix.register_hook()" is all that's needed to enable the
hook. You can also unregister the hook by calling "uprefix.unregister_hook()".

The project is set up with a setup.py and (basic) test suite, though it's too
early to put it on PyPI. I have done basic testing with it, and it should also
work as expected in virtual environments.

The implementation uses lib2to3, though that could be changed if needed.

You can also, of course, use it in Python 3.3 right now (before the PEP gets
implemented).

Please take a look at it, try it out, and give some feedback.

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: =?UTF-8?B?WmJpZ25pZXcgSsSZZHJ6ZWpld3NraS1Tem1law==?
Attributes: []Content: 
On 03/04/2012 10:34 AM, Vinay Sajip wrote:


Hi,

it's pretty cool that 150 lines is enough to have this functionality.

This guard:

  if sys.version_info[0] < 3:
      raise NotImplementedError('This hook is implemented for Python 3 only')

Wouldn't it be better if the hook did nothing when on python 2?
I think it'll make it necessary to use something like

  import sys
  if sys.version_info[0] < 3:
      import uprefix
      uprefix.register_hook()

in the calling code to enable the code to run unchanged on both branches.

Also: have you though about providing a context manager which does
register_hook() in __enter__() and unregister_hook() in __exit__()?

I think that some code will want to enable the hook only for specific
modules. The number of lines could be minimized with something like this:
  import uprefix
  with uprefix.hook:
     import abcde_with_u
     import bcdef_with_u
  import other_module_without_u

Regards,
Zbyszek



----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Zbigniew J?drzejewski-Szmek <zbyszek <at> in.waw.pl> writes:


Actually I've realised the guard won't be invoked on Python 2, anyway: I later
added a "raise ImportError() from e" in an exception handler, which leads to a
syntax error in Python 2 before the guard even gets executed.

So, I'll remove the guard (as it does nothing useful anyway) and think a bit
more about not failing on Python 2. Perhaps - not use the "from" syntax in the
exception handler, and do a no-op in register_hook if on Python 2.


Of course, things like this can be added without too much trouble.

Thanks for the feedback.

Regards,

Vinay Sajip





----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Armin Ronache
Attributes: []Content: 
Hi,

It should also be added that the Python 3.3 alpha will release with support:

 Python 3.3.0a0 (default:042e7481c7b4, Mar  4 2012, 12:37:26)
 [GCC 4.2.1 (Apple Inc. build 5666) (dot 3)] on darwin
 Type "help", "copyright", "credits" or "license" for more information.
 >>> u"Hello" + ' World!'
 'Hello World!'


Regards,
Armin



----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Mar 4, 2012 at 11:46 PM, Armin Ronacher
<armin.ronacher at active-4.com> wrote:

Nice :)

Do you have any more updates left to do? I saw the change, the tests,
the docs and the tokenizer updates go by on python-checkins, so if
you're done we can mark the PEP as Final (at which point the inclusion
in the first alpha is implied).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Armin Ronache
Attributes: []Content: 
Hi,

On 3/4/12 2:01 PM, Nick Coghlan wrote:
Docs just have a minor notice regarding the reintroduced support for 'u'
prefixes, someone might want to add more to it.  Especially regarding
the intended use for them.


Regards,
Armin



----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Sat, Mar 3, 2012 at 11:34 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Thanks very much! It looks great to me.


+1

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 04, 2012, at 05:34 PM, Nick Coghlan wrote:


Nick, really great job with your rewrite of PEP 414.  I think you nailed it
from the technical side while bringing some much needed balance to the social
side.  Not to diminish Armin's contribution to the PEP - after all this, I'm
really glad he was able to bring it up and despite the heat of the discussion,
get this resolved to his satisfaction.

One factual omission:

In the section on WSGI "native strings", you say

 * binary data: handled as str in Python 2 and bytes in Python 3

While true, this omits that binary data can *also* be handled as bytes in
Python 2.6 and 2.7, where using `bytes` can be a more descriptive alias for
`str`.  If you can do it in a readable way within the context of that section
I think it's worth mentioning this.

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120304/a54a0405/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 414 updated
----------------------------------------
Author: Chris McDonoug
Attributes: []Content: 
On Sun, 2012-03-04 at 17:34 +1000, Nick Coghlan wrote:

"""
Just as support for string exceptions was eliminated from Python 2 using
the normal deprecation process, support for redundant string prefix
characters (specifically, B, R, u, U) may be eventually eliminated from
Python 3, regardless of the current acceptance of this PEP.
"""

We might need to clarify the feature's longevity.  I take the above to
mean that use of u'' and/or U'' won't emit a deprecation warning in 3.3.
But that doesn't necessarily mean its usage won't emit a deprecation
warning in 3.4 or 3.5 or 3.6, or whenever it "feels right"?  Does that
sound about right?

- C



