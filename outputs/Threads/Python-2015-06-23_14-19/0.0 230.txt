
============================================================================
Subject: [Python-Dev] Request for commits and/or privileges
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Request for commits and/or privileges
----------------------------------------
Author: Daniel Stutzbac
Attributes: []Content: 
Several issues that I'm involved with (listed below) are ready for
commit, as far as I can tell.  They have a patch, and either a core
developer has positively reviewed the patch or the patch is a
straightforward implementation of a core developer's suggested
approach.  They are all bug-fixes or optimizations.

May I have commit privileges so that I can commit these patches and
future patches in a similar state?  If the consensus is negative for
any reason, I completely respect that decision and will continue to
contribute patches just as I am now (but in that case, consider this
my humble request for someone to commit these changes :) ).  If
positive, I would start by committing just one (after writing an
appropriate NEWS entry) and soliciting feedback to make sure that I
had done it right.

http://bugs.python.org/issue8781 - 32-bit wchar_t doesn't need to be
unsigned to be usable
http://bugs.python.org/issue9214 - Most Set methods of KeysView and
ItemsView do not work right
http://bugs.python.org/issue8750 - Many of MutableSet's methods assume
that the other parameter is not self
http://bugs.python.org/issue5553 - Py_LOCAL_INLINE(type) doesn't
actually inline except using MSC
http://bugs.python.org/issue2521 - ABC caches should use weak refs
http://bugs.python.org/issue808164 - socket.close() doesn't play well
with __del__

Many more in the pipeline :-)

--
Daniel Stutzbach, Ph.D.
President, Stutzbach Enterprises, LLC



----------------------------------------
Subject:
[Python-Dev] Request for commits and/or privileges
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
+1

On Fri, Aug 20, 2010 at 7:23 PM, Daniel Stutzbach
<daniel at stutzbachenterprises.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Request for commits and/or privileges
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Please send me your SSH key.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Request for commits and/or privileges
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 
+1 from me too.

When you start, olease do leave me as the primary maintainer 
for issues relating to sets and set ABCs.


Raymond


On Aug 20, 2010, at 8:53 PM, Guido van Rossum wrote:





----------------------------------------
Subject:
[Python-Dev] Request for commits and/or privileges
----------------------------------------
Author: Daniel Stutzbac
Attributes: []Content: 
On Sat, Aug 21, 2010 at 12:41 PM, Raymond Hettinger
<raymond.hettinger at gmail.com> wrote:

I see myself as the primary maintainer of nothing.  :-)  I would not
commit anything related to sets or set ABCs unless you have signed off
on it in some way.  Perhaps in time there will be some piece of Python
that I've modified so heavily that I become ipso facto the primary
maintainer, but I'm in no hurry.

--
Daniel Stutzbach, Ph.D.
President, Stutzbach Enterprises, LLC



----------------------------------------
Subject:
[Python-Dev] Request for commits and/or privileges
----------------------------------------
Author: Daniel Stutzbac
Attributes: []Content: 
On Sat, Aug 21, 2010 at 3:47 AM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Done.

I have also subscribed to python-committers and python-checkins.  I
will add my interests to Misc/maintainers.rst.  Are there any other
initial start-up tasks I should perform?

--
Daniel Stutzbach, Ph.D.
President, Stutzbach Enterprises, LLC



----------------------------------------
Subject:
[Python-Dev] Request for commits and/or privileges
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sat, Aug 21, 2010 at 23:45, Daniel Stutzbach
<daniel at stutzbachenterprises.com> wrote:

Those are the biggies since you already know how to create patches and such.

-Brett



