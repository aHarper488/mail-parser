
============================================================================
Subject: [Python-Dev] io module types
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] io module types
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
Can calls to the C types in the io module be made into module lookups
more akin to how it would work were it written in Python? The C
implementation for io_open invokes the C type objects for FileIO, and
friends, instead of looking them up on the io or _io modules. This
makes it difficult to subclass and/or modify the behaviour of those
classes from Python.

http://hg.python.org/cpython/file/0bec943f6778/Modules/_io/_iomodule.c#l413

