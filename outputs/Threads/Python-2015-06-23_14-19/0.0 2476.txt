
============================================================================
Subject: [Python-Dev] PEP 3151 accepted
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 3151 accepted
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
As the BDFOP for PEP 3151, I hereby accept it for inclusion into Python 3.3.

Congratulations to Antoine for producing a great PEP that has broad acceptance
in the Python development community, with buy-in from all the major
implementations of Python.  Antoine's branch is ready to go and it should now
be merged into the default branch.

PEP 3151 will bring some much needed sanity to this part of the standard
exception hierarchy, and I for one look forward to being able to write code
directly using it, one day finally eliminating most of my `import errno`s!

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111011/f7de94e6/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3151 accepted
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 11 Oct 2011 18:22:43 -0400
Barry Warsaw <barry at python.org> wrote:

Thanks Barry!
I expect to merge the PEP 3151 into default soon (it's basically ready).

cheers

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 3151 accepted
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 
2011/10/12 Antoine Pitrou <solipsis at pitrou.net>:

Thank you for having worked on this, it was a pretty huge amount of work.
We'll probabily have to wait a long time before seeing libs/apps
freely depending on this change without caring about backward
compatibility constraints, but with this Python is a better language
now.


--- Giampaolo
http://code.google.com/p/pyftpdlib/
http://code.google.com/p/psutil/



----------------------------------------
Subject:
[Python-Dev] PEP 3151 accepted
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 12/10/2011 00:22, Barry Warsaw a ?crit :

Congratulations Antoine, and thanks!

Cheers

