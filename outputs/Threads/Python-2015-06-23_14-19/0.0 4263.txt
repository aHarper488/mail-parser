
============================================================================
Subject: [Python-Dev] Disabling string interning for null and
	single-char causes segfaults
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Disabling string interning for null and
	single-char causes segfaults
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/2/2013 10:08 AM, Nick Coghlan wrote:

Since it required patching functions rather than a configuration switch, 
it literally seems not be a supported option. If so, I would not 
consider it a bug for CPython to use the assumption of interning to run 
faster and I don't think it should be slowed down if that would be 
necessary to remove the assumption. (This is all assuming that the 
problem is not just a ref count bug.)

Stefan's question was about 2.7. I am just curious: does 3.3 still 
intern (some) unicode chars? Did the 256 interned bytes of 2.x carry 
over to 3.x?


-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Disabling string interning for null and
	single-char causes segfaults
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 02.03.13 22:32, Terry Reedy wrote:

Yes, Python 3 interns an empty string and first 256 Unicode characters.





----------------------------------------
Subject:
[Python-Dev] Disabling string interning for null and
	single-char causes segfaults
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 01.03.13 17:24, Stefan Bucur wrote:

I think this is not a bug if the code relies on the fact that an empty 
string is a singleton. This obviously is an immutable object and there 
is no public method to create different empty string. But a user can 
create different 1-character strings with same value (first create 
uninitialized a 1-character string and than fill a content). If some 
code fails when none of 1-character strings are interned, this obviously 
is a bug.



