
============================================================================
Subject: [Python-Dev] The Meaning of Resolotion (Re: bug
 tracker	permissions request)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] The Meaning of Resolotion (Re: bug
 tracker	permissions request)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
R. David Murray wrote:

Note that I'll occasionally use "pending" to mean "committed to at least
one branch, still need to port/block remaining branches". (e.g. I did it
this week, since 2.7b2 was higher priority than the other branches which
don't have near term binary releases coming up).

I've seen others use it that way as well.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

