
============================================================================
Subject: [Python-Dev] Offer of help: http://bugs.python.org/issue10910
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Offer of help: http://bugs.python.org/issue10910
----------------------------------------
Author: Barry Scot
Attributes: []Content: 
I see that issue 10910 needs a reviewer for a patch.

I know the python code and C++ and offer to review
any patches to fix this issue.

Having updated Xcode on my Mac I'm having to code
workarounds for this issue.

My understanding is that you cannot define
ispace, toupper etc as macros in C++ environment.
These are defined as functions in C++.

The minimum patch would #ifdef out the offending
lines in byte_methods.h and pyport.h if compiling
for C++.

I'm going to be releasing a PyCXX release to work around
this issue.

Barry




----------------------------------------
Subject:
[Python-Dev] Offer of help: http://bugs.python.org/issue10910
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
On 24.06.2012 14:52, Barry Scott wrote:

Is this even an issue for 3.x? ISTM that the C library macros aren't
used, anyway, so I think this entire section could go from the header
files.

For 2.7, things are more difficult.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Offer of help: http://bugs.python.org/issue10910
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I think you missed my point. Python shouldn't be using isspace anymore
at all, so any work-arounds for certain BSD versions should be outdated
and can be removed entirely.

Of course, before implementing that solution, one would have to verify
that this claim (macros not used) is indeed true.


I'm not so much concerned with compiling with C++, but care about a
potential cleanup of the headers.


Yes, there will be more 2.7 bugfix releases. If a fix is too intrusive
or too hacky, it might be that the bug must stay unfixed, though.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Offer of help: http://bugs.python.org/issue10910
----------------------------------------
Author: Barry Scot
Attributes: []Content: 

On 24 Jun 2012, at 14:29, Martin v. L?wis wrote:


 $ grep isspace /Library/Frameworks/Python.framework/Versions/3.2/include/python3.2m/*.h
/Library/Frameworks/Python.framework/Versions/3.2/include/python3.2m/pyport.h:#undef isspace
/Library/Frameworks/Python.framework/Versions/3.2/include/python3.2m/pyport.h:#define isspace(c) iswspace(btowc(c))

I'm not familiar with pyport.h usage. I do see that it protects the problem lines with:
#ifdef _PY_PORT_CTYPE_UTF8_ISSUE

So long as that is not defined when C++ is in use no problem.


This is where a fix is required. Is there going to be another 2.7 release to deliver a fix in?

Barry




----------------------------------------
Subject:
[Python-Dev] Offer of help: http://bugs.python.org/issue10910
----------------------------------------
Author: Barry Scot
Attributes: []Content: 
On 24 Jun 2012, at 17:55, "Martin v. L?wis" <martin at v.loewis.de> wrote:


Fine so long as the bad code goes.


I hope you are not claiming that it is o.k for python to ignore c++ developers!

I hope that it is rasonable to state that the pyhon api can be used from C++ without fear or the need for hacky work arounds.


It seems that the only reason for the problem in the header is to detect an unexpected use of isspace and friends. I cannot see why you could not at a minimum remove when C++ compiler is used. I suspect C users could rightly be unset at a C api being broken after Python.h is included.

Barry

