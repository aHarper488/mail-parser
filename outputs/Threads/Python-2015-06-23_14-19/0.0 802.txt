
============================================================================
Subject: [Python-Dev] reindenting
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] reindenting
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Tue, 04 May 2010 11:27:58 -0400, Zvezdan Petkovic a ?crit?:

Right, I was simplifying a bit.

I've just written a script which does a slightly more elaborate 
reindentation of C files, but still with the main objective of replacing 
tabs with 4-spaces indents. It shows good results on most of the source 
tree. In particular, it handles continuation lines and keeps vertical 
alignment correct in the majority of cases. This script could probably be 
applied with little a posteriori manual intervention.

It can be found in trunk/sandbox/untabify/untabify.py.

Regards

Antoine.


