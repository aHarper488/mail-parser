
============================================================================
Subject: [Python-Dev] docs.python.org pointing to Python 3 by default?
Post Count: 43
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
At what point should we cut over docs.python.org to point to the Python 3
documentation by default?  Wouldn't this be an easy bit to flip in order to
promote Python 3 more better?

Cheers,
-Barry

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120518/36d68dc1/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On May 18, 2012 1:26 PM, "Barry Warsaw" <barry at python.org> wrote:

Today sounds good to me.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120518/65093fc0/attachment.html>



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/5/18 Barry Warsaw <barry at python.org>:

Perhaps on the occasion on the release on Python 3.3?


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Hynek Schlawac
Attributes: []Content: 
Hi,


I?d vote for the release of 3.3 instead of a surprise change in the
middle of nowhere.

Cheers,
Hynek



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 18, 2012, at 11:36 AM, Benjamin Peterson wrote:


Of course, I'm with Brian, JFDI. :)

But coordinating with the 3.3 release would also be nice advertisement.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120518/b5d282d3/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/18/2012 2:39 PM, Hynek Schlawack wrote:

I would have done it with 3.2 and thought that was once agreed on. The 
last 3.2.3 would also have been a good time, but today might seem odd, 
so I would am willing to wait for 3.3 as long as it is not somehow 
forgotten about ;-).

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Glyp
Attributes: []Content: 
On May 18, 2012, at 2:24 PM, Barry Warsaw wrote:


I would like to suggest a less all-or-nothing approach.  Just redirecting to Python 3 docs is going to create a lot of support headaches for people trying to help others learn Python.

Right now, e.g. <http://docs.python.org/tutorial/index.html> directly renders a page.  I suggest that this be changed to a redirect to <http://docs.python.org/release/2.7/tutorial/index.html>.  The fact that people can bookmark the "default" version of a document is kind of a bug.

The front page, <http://docs.python.org/> could then be changed into a "are you looking for documentation for Python 2 or Python 3?" page, with nice big click targets for each (an initial suggestion: half the page each, split down the middle, but the web design isn't really the important thing for me).

If you want to promote python 3 then putting "most recent version" links (for example, see <http://twistedmatrix.com/documents/10.2.0/api/twisted.internet.defer.inlineCallbacks.html>) across the top of all the old versions would be pretty visible.

-glyph




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

I don't think this will be that bad. Most Python 3 documentation
pages apply to Python 2 as well. There may be features documented
that don't exist in Python 2, but it was always the case that
users of older Python versions had to watch for the
versionadded/versionchanged notices.

IMO, it would be good if each individual page had an "other
versions" section on left-hand block, or on the top along with
the "previous | next" links.

As for the amount of cross-linking, I suggest the following,
assuming 2.7 and 3.3 are the current releases:
1. 2.7 links to 2.6 and 3.3
2. 3.3 links to 3.2 and 2.7
3. all older versions link to "newest", i.e. 3.3.

I understand that this would require a custom mapping
in some cases. It would be best if Sphinx could already
consider such a mapping when generating links. Failing
that, we can also do the custom mapping in the web
server (i.e. with redirects).

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Sun, May 20, 2012 at 4:43 AM, Glyph <glyph at twistedmatrix.com> wrote:

I'm -1 on that; unless there's a strong reason to avoid it,
bookmarking the "default" version seems like the right thing to me.
(One example of a strong reason would be if all Python modules were
numbered sequentially in alphabetical order, meaning that adding a new
module changes the URLs of existing modules' pages.) Compare the
PostgreSQL documentation: if you do a web search for 'postgres
nextval', you'll find the documentation for Postgres's sequence
functions (which is correct), but chances are it'll be the old docs -
version 8.1 most likely. If there's no weighting toward one in
particular, I'd say that returning information for the latest version
is the most logical default.

Obviously there's more docs difference between Python 2 and Python 3
than between Postgres 8.1 and Postgres 9.1, but the most accessible
version of a page should not IMHO distinguish between Python minor
versions.

ChrisA



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On May 18, 2012, at 11:24 AM, Barry Warsaw wrote:


My experience teaching and consulting suggests that this would be a bad move.
People are using Python2.7 and are going to docs.python.org for information.
This would only disrupt their experience.

It wouldn't "promote" anything, it would just make accessing the documentation
more awkward for the large majority of users who are still on Python 2.

When there is more uptake of Python 3, it would be reasonable move.
If it is done now, it will just create confusion and provide no benefit.


Raymond
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120520/b294c074/attachment.html>



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
I suggest that we add a separate (virtual) subdomain, e.g. docs3.python.org.

On Sun, May 20, 2012 at 4:27 PM, Raymond Hettinger
<raymond.hettinger at gmail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, May 21, 2012 at 11:23 AM, Guido van Rossum <guido at python.org> wrote:

Rather than a new subdomain, I'd prefer to see a discreet
"documentation version" CSS widget, similar to that used in the Django
docs (see https://docs.djangoproject.com/en/1.4/) that indicated the
current displayed version and provided quick links to the 2.7 docs,
the stable 3.x docs and the development docs. The
versionadded/versionchanged notes in the 3.x series are not adequate
for 2.x development, as everything up to and including 3.0 is taken as
a given - the notes are used solely for changes within the 3.x series.

I know plenty of people are keen to push the migration to Python 3
forward as quickly as possible, but this is *definitely* a case of
"make haste slowly". We need to tread carefully or we're going to give
existing users an even stronger feeling that we simply don't care
about the impact the Python 3 migration is having (or is going to
have) on them. *We* know that we care, but there's still plenty of
folks out there that don't realise how deeply rooted the problems are
in Python 2's text model and why the Python 3 backwards compatibility
break was needed to fix them. They don't get to see the debates that
happen on this list - they only get to see the end results of our
decisions. Switching the default docs.python.org version to the 3.x
series is a move that needs to be advertised *well* in advance as a
courtesy to our users, so that those that need to specifically
reference 2.7 have plenty of time to update their links.

Back when Python 3 was first released, we set a target for the
migration period of around 5 years. Since the io performance problems
in 3.0 meant that 3.1 was the first real production ready release of
3.x, that makes June 2014 the target date for when we would like the
following things to be true:
- all major third party libraries and frameworks support Python 3 (or
there are Python 3 forks or functional replacements)
- Python 3 is the default choice for most new Python projects
- most Python instruction uses Python 3, with Python 2 differences
described for those that need to work with legacy code
- (less likely, but possible) user-focused distros such as Ubuntu and
Fedora have changed their "python" symlink to refer to Python 3

That's still 2 years away, and should line up fairly nicely with the
release of Python 3.4 (assuming the current release cadence is
maintained for at least one more version). Key web and networking
frameworks such as Django [1], Pyramid [2] and Twisted [3] should also
be well supported on 3.x by that point.

In the meantime, I propose the following steps be taken in order to
prepare for the eventual migration:
- change the current unqualified URLs into redirects to the
corresponding direct 2.7 URLs
- add a "latest" subpath that is equivalent to the current "py3k" subpath
- add a Django-inspired version switching widget to the CSS & HTML for
the 2.7, 3.2 and trunk docs that offers the following options: 2.7,
3.2, latest (3.2), dev (3.3).

Cheers,
Nick.

[1] https://www.djangoproject.com/weblog/2012/mar/13/py3k/
[2] http://docs.pylonsproject.org/projects/pyramid/en/1.3-branch/whatsnew-1.3.html
[3] http://twistedmatrix.com/trac/milestone/Python-3.x

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

I don't think users will have *that* feeling. I got comments that users
were puzzled that we kept continuing development on 2.x when 3.x was
released, so users do recognize that the migration to 3.x is not abrupt.


I don't think users care much about philosophical or abstract engineering
differences between the versions when thinking about porting. I'd expect
that most of them agree, in the abstract, that they will have to port to
Python 3 eventually. Some, of course, wish to stay with Python 2 forever,
and wish that this Python 3 madness is simply abandoned.

That they don't port is often caused by missing dependencies. If all
dependencies are met, it's caused by simple lack of time and energy.


Maybe you set this target for yourself. I set "Python 3.2/3.3" as a
target. I think Guido set an even earlier target initially.

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/21/2012 12:28 AM, Nick Coghlan wrote:

I was about to post the exact same idea.

docs.python.org/py3k is a bit obscure and buried and makes Python 3.x 
look a bit like a second-class citizen on the site. It has previously 
been our policy that each new production-ready release takes 'pride of 
place' at docs.python.org. Not doing so even with 3.3, *and doing 
nothing else*, could be taken as implying that we lack full confidence 
in the release.

On the other hand, I am sympathetic to Raymond's and Nick's points that 
switching might seem too much 'in their faces' for Py 2 users, 
especially those who do not have or use an offline help file as their 
everyday reference. I want Python 3 to get equal billing, but not to 
generate reaction against it.

I also suggest docs2.python.org as the permanent home for latest python 
2 docs for as long as it seems sensible (probably a decade at least). 
Make that operable now and suggest on the front page of docs.python.org 
that py2 users switch before 3.4.


Each page of our docs say "Python 3.3.0a3 Documentation", or the 
equivalent, at the top. So we already have that covered. The drop-down 
version selection box on the django page seems to only apply to 
searches. Merely selecting a different version does not trigger anything.

What might be useful is to have the 'Other versions' links on the left 
margin of *every* page, not just the front page, but have them link to 
the corresponding page of the other docs (if there is one, and 
non-trivial I expect). For someone trying to write combined 2/3 code, or 
merely to learn the other version, I would think it useful to be able to 
jump to the corresponding page for the other version.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, May 21, 2012 at 3:47 PM, Terry Reedy <tjreedy at udel.edu> wrote:

Please, no - proliferating subdomains can quickly get confusing and
hard to remember. It makes sense up to a point (e.g. separating out
the docs from everything else on python.org), but having multiple docs
subdomains is completely unnecessary when we already have directory
based versioning.

Namespaces are a great idea, let's do more of those :)


Having "http://docs.python.org/latest" refer to Python 3.x would
remove the "second class citizen" status, as well as providing a clear
indication right in the URL that docs.python.org contains more content
than just the latest version of the docs. The unqualified URLs could
then become redirects to "latest" after a suitable migration period
with a notification and a link to the 2.7 version specific docs on
each page.

For example, at the release of 3.3, each page of the default docs on
the website could be updated with a note like the following:

"The default documentation pages will be switching to the Python 3
series in February 2012, 6 months after the release of Python 3.3. The
permanent link for the 2.7 version of this page is: <URL with the
"2.7" directory entry>"


Right, and switching the default docs without a suitable notice period
would be a great way to generate confusion. Migrating to a "latest"
URL has no such negative impact:
- the new URLs become available immediately for those that want to use them
- the old URLs can be converted to 301 redirects after a suitable warning period


I think "http://docs.python.org/2.7" is fine as the long term home for
the final version of the Python 2 documentation (it also has the
virtue of already existing).


That's what the Django widget does. I'm not talking about their search
form - I'm talking about the floating CSS box that appears in the
bottom right of each page and stays there as you scroll down. If you
click on it, the list of available documentation versions appears,
with direct links to the corresponding page in the other versions.

It has several attractive features:
- always present, even when you scroll down on a long page
- unobtrusive when you don't need it (only displays current version by
default, have to click it to get the list of all versions)
- direct links to the corresponding page in other versions

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/5/20 Nick Coghlan <ncoghlan at gmail.com>:

A subdomain isn't a namespace?


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/21/2012 2:28 AM, Nick Coghlan wrote:


I see it now. Very nice. I hope our doc people can duplicate it.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, May 21, 2012 at 4:32 PM, Benjamin Peterson <benjamin at python.org> wrote:

A subdomain is only a namespace if you use it as one. The following
would be using docs.python.org as a namespace (and is what I think we
should move towards):

docs.python.org/latest
docs.python.org/dev
docs.python.org/3.2
docs.python.org/3.1
docs.python.org/2.7
docs.python.org/2.6
etc...

The following is *not* using it as a namespace:

docs.python.org # 2.7
docs3.python.org # 3.2

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Hynek Schlawac
Attributes: []Content: 


Bikesheddingly, I?d prefer ?stable? over ?latest?. That would also
better convey the point that 3 is ready for production.

Otherwise +1; I find the current hybrid structure suboptimal.

Also -1 on docs3, that would suggest that it?s still something special
and 2 (= docs) is the real deal.

Regards,
Hynek



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Nick Coghlan writes:

 > > A subdomain isn't a namespace?
 > 
 > A subdomain is only a namespace if you use it as one. The following
 > would be using docs.python.org as a namespace (and is what I think we
 > should move towards):

+1

 > The following is *not* using it as a namespace:
 > 
 > docs.python.org # 2.7
 > docs3.python.org # 3.2

No, but it *is* using "python.org" as a namespace.  I personally think
this is ugly and hard to use, but I'm hard-pressed to explain why. :-(
I hope you can do better (the above isn't going to convince anybody
who currently holds the opposite opinion).



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 21 May 2012 08:35, Hynek Schlawack <hs at ox.cx> wrote:

Good point.
Paul.



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On Mon, May 21, 2012 at 01:47:50AM -0400, Terry Reedy wrote:


+1


-- 
Steven




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: =?iso-8859-2?Q?=A3ukasz_Langa?
Attributes: []Content: 
Wiadomo?? napisana przez Nick Coghlan w dniu 21 maj 2012, o godz. 09:24:


Love it. +1

I also like the Django-like "Documentation version" bubble. Makes navigating between versions simple regardless where you got the original link from. Blog posts and search engines often keep links to outdated versions.

-- 
Best regards,
?ukasz Langa
Senior Systems Architecture Engineer

IT Infrastructure Department
Grupa Allegro Sp. z o.o.

http://lukasz.langa.pl/
+48 791 080 144




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 21 May 2012 14:28:06 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:


+1.
There will be some subtleties: for example, the 2.x docs for urllib2
will have to link to the 3.x docs for urllib.request.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 05/21/2012 03:23 AM, Guido van Rossum wrote:

Here are the time machine keys: this subdomain has existed for a few years now :)

Georg




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 18.05.12 21:30, Brian Curtin wrote:

Yesterday. ;-)  Issue14469.




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 05/21/2012 11:09 AM, ?ukasz Langa wrote:

Apart from the "latest" one, all these URLs already work.

Of course, /2.7 is redirected to /, and /3.3 to /dev, etc.
If required, the direction of these redirects can be changed, so
that e.g. / goes to /2.7.

What about:

* Canonical:

docs.python.org/2/
docs.python.org/3/

for latest versions of 2.x and 3.x

docs.python.org/2.7/ etc.

for latest minor versions

docs.python.org/dev/

for latest dev version.

* Redirected:

docs.python.org/  -->  either /2/ or /3/ or a "disambiguation page"
docs.python.org/py3k/ -> /3/

There is also /release/X.Y.Z for individual released versions, which
I don't want to change.


I also like Martin's idea of offering more links between individual pages, not
only the front-pages.

Georg




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Mon, 21 May 2012 11:41:29 +0200, Georg Brandl <g.brandl at gmx.net> wrote:

The fact that none of us knew about it may say something about its
effectiveness, though.

As long as it does exist, there ought to be a parallel docs2.python.org.

--David



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, May 21, 2012 at 9:42 PM, Georg Brandl <g.brandl at gmx.net> wrote:

Yeah, I was just extending the scheme I already knew existed :)


Works for me. It also means we're covered if Guido ever finds a reason
to create Python 4000 :)


Definite +1 on that. I personally like Django's version selector (for
reasons stated elsewhere in the thread), but anything that makes it
easier to hop between versions would be good.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Sun, May 20, 2012 at 10:47 PM, Terry Reedy <tjreedy at udel.edu> wrote:

Right. I don't think new subdomains and the improvements that Nick
suggests are incompatible. docs2 and docs3 can just redirect to to
docs/<some version>. It's just that docs2 and docs3 make it easier to
type or link to the (super-major) version you care about. (docs2
should be an alias for 2.7; docs3 for the latest released 3.x
version.)

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: David Malcol
Attributes: []Content: 
On Fri, 2012-05-18 at 14:24 -0400, Barry Warsaw wrote:

If we do, perhaps we should revisit http://bugs.python.org/issue10446

http://hg.python.org/cpython/rev/b41404a3f7d4/ changed pydoc in the py3k
branch to direct people to http://docs.python.org/X.Y/library/ rather
than to http://docs.python.org/library/

This was applied to the 3.2 and 3.1 branches, but hasn't been backported
to any of the 2.* - so if docs.python.org starts defaulting to python 3,
it makes sense to backport that change to 2.*


Hope this is helpful
Dave





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 05/21/2012 02:14 PM, R. David Murray wrote:

Sure.  I was never fond of it, but there was a discussion probably similar
to this one, and it was agreed to add that subdomain.

Georg




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 20, 2012, at 04:27 PM, Raymond Hettinger wrote:


How do we measure this, and what's the milestone for enough uptake to make
the switch?

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120521/57b5db45/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/21/2012 3:35 AM, Hynek Schlawack wrote:


Guido and I are proposing docs2 and docs3 each pointing to the latest 
docs for each series. That puts them on equal status.
docs.python.org, besides being a namespace for specific version docs
(/x.y, minus Nick's /latest) would be transitioned away from being a 
synonym for docs2. It could become a *neutral* index page listing docs2 
and docs3 for the 'latest' production version of each series and then 
each subdirectory.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/21/2012 3:24 AM, Nick Coghlan wrote:


This looks great except for 'latest', which is ambiguous and awkward.

Like Guido, I would have docs2 and docs3 link to the latest of each 
series. This gives both series equal billing. docs itself could then 
become a *neutral* index page. In retrospect, I wish we had done this a 
year ago.

This design would continue to work if and when we need docs4.python.org.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 21 May 2012 12:03:31 -0400
Terry Reedy <tjreedy at udel.edu> wrote:

I don't like docs2/docs3. First, they are clumsy to type and look
awkward. Second, it's not the right level of segregation; if you wanted
separate domains you'd really want docs.python2.org and
docs.python3.org.

So, in the end, I think the current scheme is ok and we only need to
add a "/stable" pointing to latest 3.x.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 21, 2012, at 02:28 PM, Nick Coghlan wrote:


I'd be all for this, as long as I can still write
chrome/firefox/genericbrowser shortcuts to give me the latest Python 2 or
Python 3 library page.


Right.  I'm just keen on continuing to make progress.  I really do think we're
not far from a tipping point on Python 3, and I want to keep nudging us over
the edge.  Roller coasters are scary *and* fun. :)


If history is repeated, my guess is that will put us a few months into Python
3.5 development.  I think Python 3.3 is shaping up to be a fantastic release,
and once it's out we should start thinking about what we want to accomplish in
Python 3.4 to achieve the goal of Python 3 dominance.


There's already great ongoing work on this.  It could use more help of course.
I've mentioned Ubuntu's efforts here before, but this is really more about the
greater Python universe, and getting Python 3 on the radar of more and more
projects.


When I talk to folks starting new Python projects, I always push for it to
begin in Python 3.  Of course, the state of their dependencies is always a
consideration, but this is becoming more feasible for more projects every day.


I doubt Debian/Ubuntu will ever switch /usr/bin/python though PEP 394 will
probably have the final word.


Rough estimate, assuming 18 month cadences and an on-time release of 3.3,
puts 3.4 final in February of 2014.

'2014-02-16T00:00:00'

Cheers,
-Barry



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Mon, 21 May 2012 11:19:56 -0400, David Malcolm <dmalcolm at redhat.com> wrote:

Note that I did apply the fix for 14434 to 2.7.  So yes, I think 10446
should be applied to 2.7 as well.

--David



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/21/2012 7:42 AM, Georg Brandl wrote:

If you prefer these to docs2, docs3, OK with me.
Whatever we do, we should encourage book/blog writers to use the 
canonical 'latest' links that will not go out of date. So there should 
definitely be one for each, with the same format. The exact format is 
less important.


While I am a strong partisan of Py 3, I do not want Py 2 users to feel 
'pushed', so I vote for a neutral index or 'disambiguation' page.

What I would do is set up the canonical pages now. Next, add a notice to 
the top of docs.python.org that it will become a neutral index page with 
the release of 3.3, so 'please change bookmarks to the new, permanent 
page for Py 2', whatever it is.



I would leave those alone too.

-- 
Terry Jan Reedy





----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Hynek Schlawac
Attributes: []Content: 

I find docs2/3 ugly as it reminds me of load balancing (like
www1.python.org) and it also doesn?t really make sense to me. I have no
problem to have these DNS records and redirect them to docs.python.org/2
or /3 but I wouldn?t like them to be the canonical URIs.



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 21/05/2012 07:42, Georg Brandl a ?crit :
+1.

I?d be +1 to adding /stable but both 2.7 and 3.2 are stable at this time.

Either sounds good, I?m in favor of redirecting to /2 for a few years
still to preserve existing links and avoid the need to click on each page.

+1, the py3k name is not obvious for everyone.

The URIs should not change, but it seems a bit bad to me that for
example the 2.7.1 docs don?t link to the latest 2.7 page and mention 2.6
as stable version

+1

On a related note, we may want to find a way to make the version more
prominent in the pages; I?ve seen beginners install Python 3 and use the
Python 2 docs and fail at the first print 'Hello, world!' example.
That?s why I support always having the version numbers in the URIs.

Cheers



----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/21/2012 11:50 AM, Georg Brandl wrote:


Since there is no link to it from docs.python.org, of course it it 
difficult to find 8-). Such a link is part of the otherwise redundant 
proposal.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] docs.python.org pointing to Python 3 by default?
----------------------------------------
Author: Stefan Scherfk
Attributes: []Content: 
Am 2012-05-21 um 19:58 schrieb ?ric Araujo:


I think this URL scheme looks most clean:

docs.python.org/   --> Points to recommended version(2 for now, 3 later)
docs.python.org/2/  --> Points to latest stable 2.x
docs.python.org/2.7/ 
docs.python.org/2.6/
...
docs.python.org/3/  --> Points to latest stable 3.x
docs.python.org/3.2/
...
docs.python.org/dev/  --> Points to dev version (e.g., 3.3)

Using something like docs.python.org/stable/ in books might not make sense if the book is about Python 3 and /stable/ points to Python 4 a few years later.

Imho, adding additional sub-domains also wouldn?t improve anything, but would add more clutter and confusion (what if somebody types "docs3.python.org/2/ ?)

A prominent CCS-box showing the current version and offering Links to other main versions would make it perfect (e.g. 2, 3 and dev for all versions, 3.x sub-releases only, if you are under docs.python.org/3/... and for 2.x accordingly).

Cheers,
Stefan

