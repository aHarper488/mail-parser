
============================================================================
Subject: [Python-Dev] PEP 380
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 380
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Mea culpa for not keeping track, but what's the status of PEP 380? I
really want this in Python 3.3!

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] PEP 380
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Nov 24, 2011 at 10:28 AM, Guido van Rossum <guido at python.org> wrote:

There are two relevant tracker issues (both with me for the moment).

The main tracker issue for PEP 380 is here: http://bugs.python.org/issue11682

That's really just missing the doc updates - I haven't had a chance to
look at Zbyszek's latest offering on that front, but it shouldn't be
far off being complete (the *text* in his previous docs patch actually
seemed reasonable - I mainly objected to way it was organised).

However, the PEP 380 test suite updates have a dependency on a new dis
module feature that provides an iterator over a structured description
of bytecode instructions: http://bugs.python.org/issue11816

I find Meador's suggestion to change the name of the new API to
something involving the word "instruction" appealing, so I plan to do
that, which will have a knock-on effect on the tests in the PEP 380
branch. However, even once I get that done, Raymond specifically said
he wanted to review the dis module patch before I check it in, so I
don't plan to commit it until he gives the OK (either because he
reviewed it, or because he decides he's OK with it going in without
his review and he can review and potentially update it in Mercurial
any time before 3.3 is released).

I currently plan to update my working branches for both of those on
the 3rd of December, so hopefully they'll be ready to go within the
next couple of weeks.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 380
----------------------------------------
Author: Michael Foor
Attributes: []Content: 

On 24 Nov 2011, at 04:06, Nick Coghlan wrote:



Is it necessary to test parts of PEP 380 through bytecode structures rather than semantics? Those tests aren't going to be usable by other implementations.

Michael



--
http://www.voidspace.org.uk/


May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing 
http://www.sqlite.org/different.html








----------------------------------------
Subject:
[Python-Dev] PEP 380
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Nov 26, 2011 at 8:14 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

The affected tests aren't testing the PEP 380 semantics, they're
specifically testing CPython's bytecode generation for yield from
expressions and disassembly of same. Just because they aren't of any
interest to other implementations doesn't mean *we* don't need them :)

There are plenty of behavioural tests to go along with the bytecode
specific ones, and those *will* be useful to other implementations.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] PEP 380
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Sat, Nov 26, 2011 at 6:39 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

I'm with nick on this one, seems like a very useful test, just
remember to mark it as @impl_detail (or however the decorator is
called).

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] PEP 380
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 26/11/2011 07:46, Maciej Fijalkowski wrote:

Fair enough. :-)

If other tests are failing (the semantics are wrong) then having a test 
that shows you the semantics are screwed because the bytecode has been 
incorrectly generated will be a useful diagnostic tool.

On the other hand it is hard to see that bytecode generation could be 
"wrong" without it affecting some test of semantics that should also 
fail - so as tests in their own right the bytecode tests *must* be 
superfluous (or there is some aspect of the semantics that is *only* 
tested through the bytecode and that seems bad, particularly for other 
implementations).

All the best,

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html


