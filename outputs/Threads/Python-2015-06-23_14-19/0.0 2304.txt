
============================================================================
Subject: [Python-Dev] [pyodbc] Setting values to SQL_* constants while
 creating a connection object
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [pyodbc] Setting values to SQL_* constants while
 creating a connection object
----------------------------------------
Author: srinivasan munisam
Attributes: []Content: 
Hi,
I would like to know how to set values to values to SQL_*  constants while
creatinga db connection through pyodbc module.
For example, i am getting a connection object like below:

In [27]: dbh1 =
pyodbc.connect("DSN=<dsn>;UID=<uid>;PWD=<pwd>;DATABASE=<database>;APP=<app_name>")

In [28]: dbh1.getinfo(pyodbc.SQL_DESCRIBE_PARAMETER)

Out[28]: True

I want to set this SQL_DESCRIBE_PARAMETER to false for this connection
object. How could i do that?
Please help me in figuring it out.

Thanks,
Srini
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110525/b6a71e01/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [pyodbc] Setting values to SQL_* constants while
 creating a connection object
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/24/2011 5:09 PM, srinivasan munisamy wrote:

Please direct Python use questions to python-listor other user 
discussion forums. Py-dev is for discussion of development of the next 
versions of Python.
-- 
Terry Jan Reedy


