
============================================================================
Subject: [Python-Dev] winsound.c fix to support python3
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] winsound.c fix to support python3
----------------------------------------
Author: Tamir Friedma
Attributes: []Content: 
Hello,
My name is Tamir Friedman, and I suggest to fix a bug in PlaySound in
winsound library. It's doesn't support the SND_MEMORY feature because its
accepts only "str" and rejects "bytes" type.
therefore i include the fixed source file:
OLD:
----------------------------------------------------------------------------
static PyObject *
sound_playsound(PyObject *s, PyObject *args)
{
    wchar_t *wsound;
    int flags;
    int ok;

    if (PyArg_ParseTuple(args, "Zi:PlaySound", &wsound, &flags)) {
        if (flags & SND_ASYNC && flags & SND_MEMORY) {
            /* Sidestep reference counting headache; unfortunately this also
               prevent SND_LOOP from memory. */
            PyErr_SetString(PyExc_RuntimeError, "Cannot play asynchronously
from memory");
            return NULL;
        }
        Py_BEGIN_ALLOW_THREADS
        ok = PlaySoundW(wsound, NULL, flags);
        Py_END_ALLOW_THREADS
        if (!ok) {
            PyErr_SetString(PyExc_RuntimeError, "Failed to play sound");
            return NULL;
        }
        Py_INCREF(Py_None);
        return Py_None;
    }
    return NULL;
}
----------------------------------------------------------------------------
NEW:
----------------------------------------------------------------------------
static PyObject *
sound_playsound(PyObject *s, PyObject *args)
{
    wchar_t *wsound;
    int flags;
    int ok;

    if (PyArg_ParseTuple(args, "z*i:PlaySound", &wsound, &flags)) {
        if (flags & SND_ASYNC && flags & SND_MEMORY) {
            /* Sidestep reference counting headache; unfortunately this also
               prevent SND_LOOP from memory. */
            PyErr_SetString(PyExc_RuntimeError, "Cannot play asynchronously
from memory");
            return NULL;
        }
        Py_BEGIN_ALLOW_THREADS
        ok = PlaySoundW(wsound, NULL, flags);
        Py_END_ALLOW_THREADS
        if (!ok) {
            PyErr_SetString(PyExc_RuntimeError, "Failed to play sound");
            return NULL;
        }
        Py_INCREF(Py_None);
        return Py_None;
    }
    return NULL;
}
----------------------------------------------------------------------------
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130629/8eb5f988/attachment.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: OLD_winsound_OLD.c
Type: text/x-csrc
Size: 5581 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130629/8eb5f988/attachment.c>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: winsound.c
Type: text/x-csrc
Size: 5637 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130629/8eb5f988/attachment-0001.c>



----------------------------------------
Subject:
[Python-Dev] winsound.c fix to support python3
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/29/2013 11:59 AM, Tamir Friedman wrote:

Thank you for tracking down the source of a problem. Please make an 
account at bugs.python.org and open a tracker issue. A post here will 
get lost. On the issue, either upload a patch file or describe the 
change. As near as I can tell, the description for your change might be:

"winsound.c, line NN in sound_playsound function, is currently
       if (PyArg_ParseTuple(args, "Zi:PlaySound", &wsound, &flags)) {
I think 'Zi' should be changed for 'z*i' because ..."


-- 
Terry Jan Reedy


