
============================================================================
Subject: [Python-Dev] AIX 5.3 - Enabling Shared Library Support Vs
	Extensions
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] AIX 5.3 - Enabling Shared Library Support Vs
	Extensions
----------------------------------------
Author: =?ISO-8859-1?Q?S=E9bastien_Sabl=E9?
Attributes: []Content: 

Hi Anurag,

Le 25/11/2010 10:24, Anurag Chourasia a ?crit :
 > All,
 >
 > When I configure python to enable shared libraries, none of the 
extensions are getting built during the make step due to this error.
 >

you may want to take a look at the following issue:

http://bugs.python.org/issue941346

Python compiled with shared libraries was broken on AIX until recently. 
There are some patches there to get it to work, or you may want to test 
the latest 2.7 or 3.x releases.

regards

--
S?bastien Sabl?



----------------------------------------
Subject:
[Python-Dev] AIX 5.3 - Enabling Shared Library Support Vs
	Extensions
----------------------------------------
Author: Anurag Chourasi
Attributes: []Content: 
Hi Sebastian,

Thanks for your response.

I  looked at http://bugs.python.org/issue941346 earlier. I was referred to
this link by Stefan Krah through another bug that i created at
http://bugs.python.org/issue10555 for this issue.

I confirm that my problem is solved with the Python 2.7.1 release which
contains the changes done by you.

Great work done by you and other folks for enabling the Shared Library build
on AIX. Hats Off !!!

Regards,
Anurag

2010/12/2 S?bastien Sabl? <sable at users.sourceforge.net>

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101202/d442dbf0/attachment.html>

