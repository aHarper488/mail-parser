
============================================================================
Subject: [Python-Dev] [Python-checkins] r84619
	-	python/branches/py3k/Misc/developers.txt
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r84619
	-	python/branches/py3k/Misc/developers.txt
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

I think that people whose alphabet is for example Cyrillic already use a
Latin transliteration in those files, so it?s good.  Are you also
including names using extended Latin alphabets (like ?ukasz) in your
suggestion?

Regards




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r84619
	-	python/branches/py3k/Misc/developers.txt
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 

Right :)  So the new policy of real name thanks to UTF-8 + ASCII
transliteration is a superset of the existing conditions, which should
be fine with everyone (i.e. names can finally be written with whatever
characters they require, and people in the old world can read it with
non-UTF 8-capable tools).


Then it?s fine.


Thanks for the feedback.  Regards


