
============================================================================
Subject: [Python-Dev] Success x86 XP-4 2.7 buildbot without any log and
	should be a failure
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Success x86 XP-4 2.7 buildbot without any log and
	should be a failure
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

I broke recently all tests of CJK encodings (#12057) in Python 2.7 (sorry, it 
is now fixed). But the "x86 XP-4 2.7" buildbot is green, I don't understand 
how (the bug was not fixed in the build 894):

http://www.python.org/dev/buildbot/all/builders/x86%20XP-4%202.7/builds/894

This build doesn't contain any log.

Victor

