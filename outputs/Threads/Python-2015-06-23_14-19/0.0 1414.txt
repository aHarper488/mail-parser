
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.2): Skip
 test_getsetlocale_issue1813() on Fedora due to setlocale() bug.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Skip
 test_getsetlocale_issue1813() on Fedora due to setlocale() bug.
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 2 Aug, 2011, at 4:22, R. David Murray wrote:


Wouldn't it be better to mark this as an expected failure on the affected platforms? Skipping the test unconditionally will skip the test even when Fedora gets around to fixing this issue.

Ronald


-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 2224 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110802/74b3caa0/attachment.bin>

