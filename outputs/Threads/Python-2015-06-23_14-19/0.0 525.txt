
============================================================================
Subject: [Python-Dev] Can Python implementations reject
 semantically	invalid expressions?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Can Python implementations reject
 semantically	invalid expressions?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 02.07.2010 08:55, schrieb Craig Citro:

The dis module is deliberately (*) not part of the Python language and
standard library; it's an implementation detail (as is the func_code
attribute, and the code object).

So the question really is: can you tell the difference, using only
mechanisms not explicitly documented as implementation-specific?

Regards,
Martin

(*) Unfortunately, the documentation fails to mention that, probably
because it's too obvious.

