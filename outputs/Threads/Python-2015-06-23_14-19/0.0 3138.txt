
============================================================================
Subject: [Python-Dev] cpython: Issue #15168: Move importlb.test to
 test.test_importlib.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #15168: Move importlb.test to
 test.test_importlib.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 20 Jul 2012 20:49:03 +0200 (CEST)
brett.cannon <python-checkins at python.org> wrote:

I don't know if I'm only speaking for myself, but I really have trouble
parsing non-trivial relative imports, and I personally prefer when
people use absolute imports (e.g. "from test import support").

cheers

Antoine.


-- 
Software development and contracting: http://pro.pitrou.net



