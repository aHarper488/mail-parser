
============================================================================
Subject: [Python-Dev] Extending os.chown() to accept user/group names
Post Count: 16
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

I think shutil.chown should aim to mimic chown(1). At least GNU chown
has a -R flag (not sure about POSIX chown), and it's useful in practice.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: =?iso-8859-2?Q?=A3ukasz_Langa?
Attributes: []Content: 

Wiadomo?? napisana przez Martin v. L?wis w dniu 2011-06-01, o godz. 08:48:


cp(1) and rm(1) also have "-r", still there are `shutil.copytree` and `shutil.rmtree`. I would like to keep that consistency, e.g. to have `shutil.chown` and `shutil.chowntree` as previously discussed by Charles-Fran?ois, Nick and Petri.

-- 
Best regards,
?ukasz Langa
Senior Systems Architecture Engineer

IT Infrastructure Department
Grupa Allegro Sp. z o.o.



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
Hi all,
before opening an issue to track the request, I'd like to ask advice
here about this: extend os.chown() to accept even user/group names
instead of just uid and gid.

On a Unix system, you can call chown command passing either id or
names, so it seems (to me at least) natural to expect os.chown() to
behave similarly; but that's not the case.

I can see os module wants to be a thin wrapper around OS syscalls and
chown(2) accepts only uid/gid as input, so what would be best: extend
os.chown() or provide a chown() function in shutil module for this
purpose?

Thanks in advance,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 25, 2011, at 10:24 AM, Sandro Tosi wrote:


I think it would be a nice feature, and I can see the conflict.  OT1H you want
to keep os.chown() a thin wrapper, but OTOH you'd rather not have to add a
new, arguably more difficult to discover, function.  Given those two choices,
I still think I'd come down on adding a new function and shutil.chown() seems
an appropriate place for it.

Cheers,
-Barry

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110525/fe45f151/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 25 May 2011 09:41:46 -0400
Barry Warsaw <barry at python.org> wrote:


+1 for shutil.chown().

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Wed, May 25, 2011 at 15:41, Barry Warsaw <barry at python.org> wrote:

Right. Please add a mention of shutil.chown() to the os.chown() docs, though.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On May 25, 2011, at 04:15 PM, Dirkjan Ochtman wrote:


Brilliant!

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110525/a2d91ff3/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 
While we're at it, adding a "recursive" argument to this shutil.chown
could also be useful.



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le mercredi 25 mai 2011 ? 18:46 +0200, Charles-Fran?ois Natali a ?crit :

I don't like the idea of a recursive flag. I would prefer a "map-like"
function to "apply" a function on all files of a directory. Something
like shutil.apply_recursive(shutil.chown)...

... maybe with options to choose between deep-first search and
breadth-first search, filter (filenames, file size, files only,
directories only, other attributes?), directory before files (may be
need for chmod(0o000)), etc.

Victor




----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
On 5/25/2011 1:17 PM, Victor Stinner wrote:

You can do all of this with an appropriate application of os.walk().

Eric.



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Petri Lehtine
Attributes: []Content: 
Victor Stinner wrote:

FWIW, the chown program (in GNU coreutils at least) has a -R flag for
recursive operation, and I've found it *extremely* useful on many
situations.

Petri



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 

I was also thinking about this possibility.
The advantage is that we could factor-out the recursive walk logic to
make it available for other functions (chown, chmod...).
It doesn't map well to the Unix command, though.


Then, I wonder why shutil.copytree and shutil.rmtree are provided.
Recursive rm/copy/chown/chmod are extremely useful in system
administration scripts. Furthermore, it's not as simple as it seems
because of symlinks, see for example http://bugs.python.org/issue4489
.



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Tim Delane
Attributes: []Content: 
2011/5/26 Victor Stinner <victor.stinner at haypocalc.com>



Pass an iterable to shutil.chown()? Then you could call it like:

shutil.chown(os.walk(path))

Then of course you have the difficulty of wanting to pass either an iterator
or a single path - probably prefer two functions e.g.:

shutil.chown(path)
shutil.chown_many(iter)

Tim Delaney
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110526/658c4dc7/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
2011/5/26 Charles-Fran?ois Natali <neologix at free.fr>:

Rather than a fixed binary flag, I would suggest following the
precedent of copytree and rmtree, and provide recursive functionality
as a separate shutil function (i.e. shutil.chmodtree,
shutil.chowntree).

As noted, while these *can* be written manually, it is convenient to
have the logic for handling symlinks dealt with for you, as well as
not having to look up the particular incantation for correctly linking
os.walk and the relevant operations.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Petri Lehtine
Attributes: []Content: 
Nick Coghlan wrote:

+1


This is exactly what I meant when saying that the -R option to chown
and chmod shell commands is useful. I *could* do it without them, but
writing the same logic every time with error handling would be
cumbersome.

Petri



----------------------------------------
Subject:
[Python-Dev] Extending os.chown() to accept user/group names
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
On Wed, May 25, 2011 at 15:58, Antoine Pitrou <solipsis at pitrou.net> wrote:

and so shutil.chown() be it: http://bugs.python.org/issue12191

Currently, only the function for a single file is implemented, let's
look later what to do for a recursive one.

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi

