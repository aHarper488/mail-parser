
============================================================================
Subject: [Python-Dev] unittest: shortDescription,
	_TextTestResult and 	other issues
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] unittest: shortDescription,
	_TextTestResult and 	other issues
----------------------------------------
Author: Olemis Lan
Attributes: []Content: 
On Tue, Feb 9, 2010 at 4:50 PM, Ben Finney <ben+python at benfinney.id.au> wrote:
[...]


Oh yes ! Thnx for mentioning that ! Very much !

If you move or remove shortDescription then I think dutest will be
broken. In that case there is an automatically generated short
description  comprising the doctest name or id (e.g. class name +
method name ;o) and example index (just remember that every
interactive example is considered to be a test case ;o)

In that case there is no other way to get this done unless an
all-mighty & heavy test result be implemented .

So I am *VERY* -1 for removing `shortDescription` (and I also think
that TC should be the one to provide the short desc rather than the
test result, just like what Ben Finney said before ;o)

-- 
Regards,

Olemis.

Blog ES: http://simelo-es.blogspot.com/
Blog EN: http://simelo-en.blogspot.com/

Featured article:
PEP 391 - Please Vote!  -
http://feedproxy.google.com/~r/TracGViz-full/~3/hY2h6ZSAFRE/110617



----------------------------------------
Subject:
[Python-Dev] unittest: shortDescription,
	_TextTestResult and 	other issues
----------------------------------------
Author: Olemis Lan
Attributes: []Content: 
On Wed, Feb 10, 2010 at 6:11 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

FWIW

+1

-- 
Regards,

Olemis.

Blog ES: http://simelo-es.blogspot.com/
Blog EN: http://simelo-en.blogspot.com/

Featured article:
Nabble - Trac Users - Embedding pages?  -
http://feedproxy.google.com/~r/TracGViz-full/~3/MWT7MJBi08w/Embedding-pages--td27358804.html

