
============================================================================
Subject: [Python-Dev] standards for distribution names
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] standards for distribution names
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
Hi All,

Following on from this question:

http://twistedmatrix.com/pipermail/twisted-python/2010-September/022877.html

...I'd thought that the "correct names" for distributions would have 
been documented in one of:

http://www.python.org/dev/peps/pep-0345
http://www.python.org/dev/peps/pep-0376
http://www.python.org/dev/peps/pep-0386

...but having read them, I drew a blank.

Where are the standards for this or is it still a case of "whatever 
setuptools does"?

Chris



----------------------------------------
Subject:
[Python-Dev] standards for distribution names
----------------------------------------
Author: P.J. Eb
Attributes: []Content: 
At 12:08 PM 9/16/2010 +0100, Chris Withers wrote:

Actually, in this case, it's "whatever distutils does".  If you don't 
build your .exe's with Distutils, or if you rename them after the 
fact, then setuptools won't recognize them as things it can consume.

FYI, Twisted has a long history of releasing distribution files that 
are either built using non-distutils tools or else renamed after being built.

Note, too, that if the Windows exe's they're providing aren't built 
by the distutils bdist_wininst command, then setuptools is probably 
not going to be able to consume them, no matter what they're called.








----------------------------------------
Subject:
[Python-Dev] standards for distribution names
----------------------------------------
Author: P.J. Eb
Attributes: []Content: 
At 12:08 PM 9/16/2010 +0100, Chris Withers wrote:

Forgot to mention: see distinfo_dirname() in PEP 376 for an 
explanation of distribution-name normalization.

(Case-insensitivity and os-specific case handling is not addressed in 
the PEPs, though, AFAICT.)




----------------------------------------
Subject:
[Python-Dev] standards for distribution names
----------------------------------------
Author: Dan Buc
Attributes: []Content: 
On Thu, Sep 16, 2010 at 12:08:59PM +0100, Chris Withers wrote:

You may also find this thread from the packaging google group useful,
although it may not be quite what you're looking for:

    http://bit.ly/96SMuM

Cheers,

-- 
~Dan

-------------- next part --------------
A non-text attachment was scrubbed...
Name: not available
Type: application/pgp-signature
Size: 230 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100917/98088a1d/attachment.pgp>

