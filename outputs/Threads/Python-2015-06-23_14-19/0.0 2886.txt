
============================================================================
Subject: [Python-Dev] [Python-checkins] Daily reference leaks
	(140f7de4d2a5): sum=888
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] Daily reference leaks
	(140f7de4d2a5): sum=888
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Feb 7, 2012 at 2:34 PM,  <solipsis at pitrou.net> wrote:

This appears to have started shortly after Benjamin's _PyExc_Init
bltinmod refcounting change to fix Brett's crash when bootstrapping
importlib. Perhaps we have a leak in import.c that was being masked by
the DECREF in _PyExc_Init?

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] Daily reference leaks
	(140f7de4d2a5): sum=888
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/2/8 Nick Coghlan <ncoghlan at gmail.com>:

According to test_capi, it's expected to leak?



-- 
Regards,
Benjamin

