
============================================================================
Subject: [Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
Post Count: 18
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 28 May 2013 23:29:46 +0200 (CEST)
brett.cannon <python-checkins at python.org> wrote:

What use case does this API solve?
(FWIW, I think "ModuleManager" is a rather bad name :-))

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, May 28, 2013 at 5:40 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

See http://bugs.python.org/issue18088 for the other part of this
story. I'm basically replacing what importlib.util.module_for_loader
does after I realized there is no way in a subclass to override
what/how attributes are set on a module before the code object is
executed. Instead of using the decorator people will be able to use
this context manager with a new method to get the same effect with the
ability to better control attribute initialization.


I'm open to suggestions, but the thing does manage the module so it at
least makes sense.



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, May 29, 2013 at 10:14 AM, Brett Cannon <brett at python.org> wrote:

I suggest ModuleInitialiser as the CM name, with a helper function to
make usage read better:

    with initialise_module(name) as m:
        # Module initialisation code goes here
        # Module is rolled back if initialisation fails

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On May 29, 2013 1:09 AM, "Nick Coghlan" <ncoghlan at gmail.com> wrote:

But you're not initializing the module; more like getting the module,
either new or from sys.modules. But I thought ModuleGetter seemed too
Java-like. Could hide the class behind a get_module function though.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130529/21fae9f9/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, May 29, 2013 at 11:04 PM, Brett Cannon <brett at python.org> wrote:

The point is to provide a useful mnemonic for *why* you would use this
context manager, and the reason is because the body of the with
statement is going to initialize the contents, and you want to unwind
things appropriately if that fails.

initializing_module is probably a better name than initialized_module,
though (since it isn't initialized yet on entry - instead, that's what
should be the case by the end of the statement)

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, May 29, 2013 at 10:34 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

You should use this context manager to get the correct module to
initialize/execute/whatever, e.g. contextlib.closing is about what the
context manager is going to do for you, not what you are doing to the
object it returned.


I am willing to compromise to module_to_initialize, module_to_init, or
module_to_load. Pick one. =)



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, May 30, 2013 at 12:47 AM, Brett Cannon <brett at python.org> wrote:

I see this as *really* similar to a database transaction, and those
start with "session.begin()".

Could you tolerate "with begin_module_init(name) as m:"?

We could even document the ability to check m.__initializing__ to see
whether this is a reload() or not.

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Wed, 29 May 2013 11:58:21 -0400,
"R. David Murray" <rdmurray at bitdance.com> a ?crit :


or "with transaction.begin()", or "with
transaction.commit_on_success()", depending on the API :-)


Agreed.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, May 29, 2013 at 11:58 AM, R. David Murray <rdmurray at bitdance.com> wrote:

In case you want to suggest a name, the context manager returns the
module that should be initialized/loaded. So typical usage will be::

  class Loader:
    def load_module(self, fullname):
      with importlib.util.module_to_init(fullname) as module:
        # Load/initialize the module
        return module

Basically the manager either gets the module from sys.modules if it is
already there (a reload) or creates a new module and sticks it into
sys.modules so other imports will grab the right module object. If
there is an exception and the module was new, it deletes it from
sys.modules to prevent stale modules from sticking around.



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, May 29, 2013 at 12:49 PM, R. David Murray <rdmurray at bitdance.com> wrote:

It's to choose the right module (sys.modules or new) and if the module
is new and there is an exception to delete it from sys.modules (other
small details like setting __initializing__ but that's not important).
So both __enter__ and __exit__ have logic.


True.


managed_module is better than managed_initialization.



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 29 May 2013 12:55:01 -0400
Brett Cannon <brett at python.org> wrote:

I don't understand how it's "managed". "manage", "manager", etc. is the
kind of dumb words everybody uses when they don't manage (!) to explain
what they're talking about.

My vote is for "module_to_init", "uninitialized_module",
"pristine_module", etc.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, May 29, 2013 at 2:56 PM, R. David Murray <rdmurray at bitdance.com> wrote:

I don't like unititionalized_module or pristine_module as that isn't
guaranteed thanks to reloading; seems misleading.


That would make me feel icky, so I won't do it.

So module_to_init it is unless someone can convince me the bikeshed is
a different colour.



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Wed, May 29, 2013 at 10:49 AM, R. David Murray <rdmurray at bitdance.com> wrote:

I was just thinking the same thing.

-eric



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Wed, May 29, 2013 at 2:22 PM, Brett Cannon <brett at python.org> wrote:

Whatever the name is, it should reflect what is happening during the
with statement, and more particularly that the thing will end at the
end of the with statement.  managed_module() seems fine to me though
it could still imply the lifetime of the module rather than the
management.  During the with statement the module is managed, and I
expect it's clear that the management is relative to the import
system.

However, it could also make sense to split the function into two
pieces: getting the module and handling it properly in the face of
exceptions in a with statement.  So, importlib.util.get_module() and
ModuleType.managed():

  class Loader:
    def load_module(self, fullname):
      module = importlib.util.get_module(fullname)
      with module.managed():
        # Load/initialize the module
        return module

If ModuleType.managed() returned the module, you could do it on one line:

  class Loader:
    def load_module(self, fullname):
      with importlib.util.get_module(fullname).managed() as module:
        # Load/initialize the module
        return module

On second thought, that "one-liner" is a little too busy.  And if it's
a problem as a method on ModuleType, make it
importlib.util.managed_module():

  class Loader:
    def load_module(self, fullname):
      module = importlib.util.get_module(fullname)
      with importlib.util.managed_module(module):
        # Load/initialize the module
        return module

It would be nice to have both parts in one function.  It would be less
boilerplate for the "common" case that way, is easier to read, and
eliminates the risk of someone not realizing they need both parts.
However, I'm not sure it buys us that much, the separate-part approach
helps keep the two concepts distinct (for better or for worse), and
each piece could be separately useful.  Maybe have a third function
that wraps the other two or have managed_module() accept strings (and
then call get_module() internally).

-eric



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 


On 29/05/13 01:14, Brett Cannon wrote:

+1. XxxManager is what Java programmers call their classes when they are forced to have an
unnecessary class because they don't have 1st class functions or modules.

(I don't like 'Context Manager' either, but it's too late to change it :( )


But what do you mean by managing? 'Manage' has many meanings.
Once you've answered that question you should have your name.

Cheers,
Mark.



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Alfredo Solan
Attributes: []Content: 
Hi,

What about ModuleProxy?

 From the dictionary:

prox?y
/?pr?ks?/
Noun
The authority to represent someone else, esp. in voting.
A person authorized to act on behalf of another.
Synonyms
deputy - representative - agent - substitute

Alfredo

On 05/30/2013 10:34 AM, Mark Shannon wrote:




----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 30 May 2013 06:25, "Brett Cannon" <brett at python.org> wrote:
wrote:
wrote:
context
actually

+1 to that bikeshed colour. It covers what we're returning (a module) and
what we plan to do with it that needs a with statement (initialising it).

Cheers,
Nick.

http://mail.python.org/mailman/options/python-dev/ncoghlan%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130530/de23fa1d/attachment.html>



----------------------------------------
Subject:
[Python-Dev] cpython: Introduce importlib.util.ModuleManager
 which is a context manager to
----------------------------------------
Author: Ron Ada
Attributes: []Content: 


On 05/30/2013 03:34 AM, Mark Shannon wrote:






It manages the context, as in the above reference to context manager.


In this case the context is the loading and unloading of a module.  Having 
context managers names end with manager helps indicate how it's used.

But other verb+er combinations also work.  Taking a hint from the first few 
words of the __doc__ string gives us an obvious alternative.

    ModuleProvider


Cheers,
     Ron



