
============================================================================
Subject: [Python-Dev] Scalability micro-conference topic proposals (LPC2012)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Scalability micro-conference topic proposals (LPC2012)
----------------------------------------
Author: Mathieu Desnoyer
Attributes: []Content: 
Hi,

We are organizing a micro-conference on scaling both upwards (many
cores) and downwards (low footprint, energy efficiency) that targets
all layers of the software stack. Our intent is to bring together
application, libraries and kernel developers to discuss the scalability
issues they currently face, and get exposure for the ongoing work on
scalability infrastructure.

Suggestions of topics are welcome. If you would like to present, please
let us know: we have lightnening-talk slots and a few 30 minutes slots
available. Presentations should be oriented towards stimulating
discussion over currently faced scalability problems and/or work in
progress in the area of scalability.

The micro-conference will be held between August 29-31, at LinuxCon
North America 2012, in San Diego.

   http://www.linuxplumbersconf.org/2012/

The Scaling Micro-Conference page is available at:

   http://wiki.linuxplumbersconf.org/2012:scaling

Best Regards,

Mathieu Desnoyers & Paul E. McKenney

-- 
Mathieu Desnoyers
Operating System Efficiency R&D Consultant
EfficiOS Inc.
http://www.efficios.com

