
============================================================================
Subject: [Python-Dev] 3.2.1 and Issue 12291
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] 3.2.1 and Issue 12291
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
I just filed an issue which shows a serious breakage in the marshal module:

"file written using marshal in 3.2 can be read by 2.7, but not 3.2 or 3.3"

http://bugs.python.org/issue12291

Perhaps this issue should be marked as a release blocker for 3.2.1 - what do
others (particularly Georg, of course) think? While it does appear to have been
broken for some time, it seems a bit too serious to leave until a 3.2.2 release.

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] 3.2.1 and Issue 12291
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jun 09, 2011, at 10:51 AM, Vinay Sajip wrote:


Sounds bad ;).  I marked it a release blocker, but of course Georg will have
the final say.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110609/202fde78/attachment.pgp>

