
============================================================================
Subject: [Python-Dev] Fwd:  i18n
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Fwd:  i18n
----------------------------------------
Author: Alcino Dall'Igna J
Attributes: []Content: 
To those beginners in programming that are not?English?speakers there
are 3 problems to be solved:
1) the logic (this is?unavoidable)
2) the programming language (hard but quite simple)
3) the messages (hard and not simple)

Those who could not cope with (1) could not be programmers

(2)?difficult?but not the main (so a 2nd step)

(3) the worst but more?treatable and more largely useful

The i18n of (2) is mainly to be used in initial stages and could not
be generally?applicable?(maybe just some?European?languages).?This
probably?could require to rewrote the scanner (or maybe only the
grammar, I haven't gone so deep yet) so it's not that big of a
problem, it hardly affects the parser and interpreter, that are the
more complex tasks.

If (3) could enter the main trunk it would be a great help by itself.
In this case access to international help is useless due the original
difficulties with the language, remember I'm talking about kids
mainly, and 1st stage to programming for youngsters. There are two
main ways to do this, one is using codes as indicated, but I prefer
using the more generally accepted and used, with messages catalogs
using gettext and the like.

Any way thanks for your comments.

Alcino

2010/8/17 Anders Sandvig <anders.sandvig at gmail.com>



----------------------------------------
Subject:
[Python-Dev] Fwd:  i18n
----------------------------------------
Author: Jeroen Ruigrok van der Werve
Attributes: []Content: 
-On [20100825 20:03], Alcino Dall'Igna Jr (adijbr at gmail.com) wrote:

Haven't been able to respond sooner, but maybe one thing was not so clear
about what I wrote since you misrepresented it here. What I was referring to
was this scenario:

ImportError: No module named blah

would become in nl_NL something like:

ImportError: Geen module genaamd blah

Now, if ImportError (or any other error) has only one text, then I could
copy-paste that error message easily and people would understand
contextually from the error which it is, despite the localization.

However, if there's multiple possible messages, how is someone else supposed
to figure out what my localized message means in order to help me out? In
this case I think an approach with a number inventory system works out well,
e.g.:

ImportError (3): Geen module genaamd blah

And the 3 would be a stable number for this particular message.

More examples:

http://infocenter.sybase.com/help/index.jsp?topic=/com.sybase.help.ase_15.0.sag1/html/sag1/sag1485.htm

Oracle:
en_US: ORA-00942: table or view does not exist
ja_JP: ORA-00942: ???????????????


-- 
Jeroen Ruigrok van der Werven <asmodai(-at-)in-nomine.org> / asmodai
????? ?????? ??? ?? ??????
http://www.in-nomine.org/ | http://www.rangaku.org/ | GPG: 2EAC625B
When you meet a master in the street, do not speak, do not be silent. Then
how will you greet him?

