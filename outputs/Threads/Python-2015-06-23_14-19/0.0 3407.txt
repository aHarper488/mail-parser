
============================================================================
Subject: [Python-Dev] cpython (3.2): 3.2 explain json.dumps for
 non-string keys in dicts. closes issue6566. Patch
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.2): 3.2 explain json.dumps for
 non-string keys in dicts. closes issue6566. Patch
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 03/17/2012 08:41 AM, senthil.kumaran wrote:

This is just a minor nitpick, and it absolutely is not specific to you, Senthil:
please try to keep the rst file structuring with newlines intact.  In
particular, I place two blank lines between top-level function/class
descriptions because single blank lines already occur so often in rst markup.

When you add paragraphs, please try to keep the blank lines.

Georg



