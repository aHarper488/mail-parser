
============================================================================
Subject: [Python-Dev] Testing the tests by modifying the ordering of
 dict items.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Testing the tests by modifying the ordering of
 dict items.
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
On 21/12/12 07:05, csebasha wrote:

No need now. The hash randomization, which was added a while ago,
will render the tests obsolete.

Cheers,
Mark.




----------------------------------------
Subject:
[Python-Dev] Testing the tests by modifying the ordering of
 dict items.
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jan 05, 2012, at 01:46 PM, Mark Shannon wrote:


I'm sure it will be a pain, but this is really the best thing to do.

-Barry

