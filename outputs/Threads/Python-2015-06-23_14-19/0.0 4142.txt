
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #18520: Add a new
	PyStructSequence_InitType2() function, same than
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #18520: Add a new
	PyStructSequence_InitType2() function, same than
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 23 Jul, 2013, at 2:01, Benjamin Peterson <benjamin at python.org> wrote:


It is not part of the stable ABI. Given that the implementation of 
PyStructSequence_InitType() in the patch just calls PyStructSequence_InitType2()
and ignores the return value you could change the return value of ..InitType().

This may or may not break existing extensions using the function (depending on
platform ABI details, AFAIK this is not a problem on x86/x86_64), but reusing
extensions across python feature releases is not supported anyway.  There are
no problems when compiling code, most C compilers won't even warn about ignored
return values unless you explicitly ask for it.

Ronald



