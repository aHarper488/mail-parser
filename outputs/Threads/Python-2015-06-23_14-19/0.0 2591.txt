
============================================================================
Subject: [Python-Dev] Removing surplus fields from the frame object and not
 adding any new ones.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Removing surplus fields from the frame object and not
 adding any new ones.
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
The frame object is a key object in CPython. It holds the state
of a function invocation. Frame objects are allocated, initialised
and deallocated at a rapid rate.
Each extra field in the frame object requires extra work for each
and every function invocation. Fewer fields in the frame object
means less overhead for function calls, and cleaner simpler code.

We have recently removed the f_yieldfrom field from the frame object.
(http://bugs.python.org/issue14230)

The f_exc_type, f->f_exc_value, f->f_exc_traceback fields which handle
sys.exc_info() in generators could be moved to the generator object.
(http://bugs.python.org/issue13897)

The f_tstate field is redundant and, it would seem, dangerous
(http://bugs.python.org/issue14432)

The f_builtins, f_globals, f_locals fields could be combined into a
single f_namespaces struct.
(http://code.activestate.com/lists/python-dev/113381/)

Now PEP 419 proposes adding (yet) another field to the frame object.
Please don't.

Clean, concise data structures lead to clean, concise code.
which we all know is a "good thing" :)

Cheers,
Mark.


