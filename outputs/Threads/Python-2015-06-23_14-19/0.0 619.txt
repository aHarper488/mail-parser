
============================================================================
Subject: [Python-Dev] OpenID login for Roundup bug trackers (was: What to do
	with languishing patches?)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] OpenID login for Roundup bug trackers (was: What to do
	with languishing patches?)
----------------------------------------
Author: Ben Finne
Attributes: []Content: 

"Stephen J. Turnbull" <stephen at xemacs.org> writes:


The tracker at <URL:http://psf.upfronthosting.co.za/roundup/meta/>
doesn't mention OpenID on its login form, so I must assume it doesn't
currently support it.

The Python bug tracker <URL:http://bugs.python.org/> does support
arbitrary OpenID for authentication. It didn't in the recent past, so
I'm very glad to see that it does now. I can finally get an account
there without the hassle of managing a site-specific auth set.


It worked for me so far on <URL:http://bugs.python.org/>, logging in
with my OpenID and then going through the registration process.

Thanks for drawing my attention to that; if the people who made OpenID
auth happen are observing this, then thank you all very much!

Any hope of feeding those changes back upstream so it's available to all
Roundup users at some point?

-- 
 \       ?Pinky, are you pondering what I'm pondering?? ?Well, I think |
  `\         so, Brain, but first you'd have to take that whole bridge |
_o__)                     apart, wouldn't you?? ?_Pinky and The Brain_ |
Ben Finney


