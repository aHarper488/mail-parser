
============================================================================
Subject: [Python-Dev] Question for you
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Question for you
----------------------------------------
Author: Connor, Craig A
Attributes: []Content: 
Hello, Dave;
   My name is Craig Connor and I am a senior s/w developer at Northrop
Grumman.
I have a question for you. I have installed Boost (via the Installer),
and stored it into my 
C Drive inside a dir called:  
    C:\boost_1_42

I also installed the Boost Jam into a directory called: 
   C:\boost-jam-3.1.17

I am using 2 separate compilers in my Win OS XP (SP3)
and I would like to be able to use the Python module of Boost
in order to embed Python.h into my C++ compiler.

The C++ compilers that I have are:
o Dev-cpp, and 
o Visual C++.net  (of MS Visual Studio.Net 2008).

Problem:
   When I compile a simple program, I keep getting the error:
"pyconfig.h: No such file or directory".

The program I am trying to start with is (below):
#include <iostream>
#include<boost/any.hpp>
#include<boost/python.hpp>

using namespace std;

int main( )
{
  cout << "Hello, Boost World!!" << endl;
  boost::any a(5);
  a = 7.67;
  std::cout<<boost::any_cast<double>(a)<<std::endl;
  
  system( "PAUSE" );
  return 0;
} 


Also:
  I did set up my environmental config to go to the Boost dir.

Question:
  Do you know what am I doing wrong?

Regards,
   Craig Connor
   720.622.2209

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100223/2558dda9/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Question for you
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
Hello Connor,

I think you have the wrong email address - this is Python-dev, an email 
list for the development *of* Python.

All the best,

Michael Foord

On 23/02/2010 15:41, Connor, Craig A. wrote:


-- 
http://www.ironpythoninaction.com/

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100223/46844ed7/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Question for you
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Michael Foord wrote:

One of the boost specific lists or the general python-list (aka
comp.lang.python) would be the place to go for help of this nature.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------

