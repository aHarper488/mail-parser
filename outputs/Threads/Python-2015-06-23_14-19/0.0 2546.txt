
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #1172711: Add
 'long long' support to the array module.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #1172711: Add
 'long long' support to the array module.
----------------------------------------
Author: Meador Ing
Attributes: []Content: 
On Wed, Sep 21, 2011 at 11:02 AM, Stefan Krah <stefan at bytereef.org> wrote:


I agree with Stefan on the have_long_long part.  This is what is used
in the array module
code, struct, ctypes, etc ... (via pyport.h as Stefan mentioned).  As
for the unless/if,
I am OK with the 'if'.  'unless' always causes a double-take for me.
Personal preference I guess.

-- Meador

