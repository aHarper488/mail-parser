
============================================================================
Subject: [Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: David Hutt
Attributes: []Content: 
Being that python is, to me, a prototyping language, then every possible
outcome should be presented to the end user.

A full variety of explanations should be presented to the programmer.



On Fri, Dec 13, 2013 at 11:56 PM, Vajrasky Kok
<sky.kok at speaklikeaking.com>wrote:




-- 
Best Regards,
David Hutto
*CEO:* *http://www.hitwebdevelopment.com <http://www.hitwebdevelopment.com>*
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20131214/6fff348a/attachment.html>



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
David Hutto wrote:

So we should produce a quantum superposition of
error messages? :-)

(Sorry, I've been watching Susskind's lectures on
QM and I've got quantum on the brain at the moment.)

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 14 December 2013 14:56, Vajrasky Kok <sky.kok at speaklikeaking.com> wrote:

The idiom has shifted over time, but the preference more recently is
definitely for length limiting user provided identifiers (which are
generally type names) to limit the maximum length of error messages
(to add another variant to the mix, PEP 7 has "%.100s" in an example
about breaking long lines that happens to include reporting
TypeError).

The question should probably be addressed directly in PEP 7, and I'd
be inclined to just bless the "%.400s" variant for future code.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 15 Dec 2013 09:10:08 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

Shouldn't we have a special "%T" shortcut instead of trying to
harmonize all the occurrences of `"%.400s", Py_TYPE(self)->tp_name` ?

Sprinkling the same magic number / convention everywhere doesn't sound
very future-proof, nor convenient.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2013/12/15 Antoine Pitrou <solipsis at pitrou.net>:

Oh, I like this proposition! The following pattern is very common in Python:

"... %.400s ...", Py_TYPE(self)->tp_name

Victor



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 15 December 2013 09:52, Victor Stinner <victor.stinner at gmail.com> wrote:

Oh, yes, a %T shortcut for "length limited type name of the supplied
object" would be brilliant. We need this frequently for C level error
messages, and I almost always have to look at an existing example to
remember the exact incantation :)

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: David Hutt
Attributes: []Content: 
Susskinds...Me too, but the refinement of the error messages is the point.
We should be looking at the full assessment of the error, which the
prototyping of python should present.

I've seen others reply that python wouldn't be around, or that theree are
other forms I've seen before that will take the forefront.

The point should be to align the prototyping of python with the updates in
technology taking place.

It should be like it usually is, line for lineerror assessments, even
followed by further info to inform the prototyper that is looking to
translate to a lower level language.


On Sat, Dec 14, 2013 at 4:07 PM, Greg Ewing <greg.ewing at canterbury.ac.nz>wrote:




-- 
Best Regards,
David Hutto
*CEO:* *http://www.hitwebdevelopment.com <http://www.hitwebdevelopment.com>*
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20131214/b7ec097e/attachment.html>



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: David Hutt
Attributes: []Content: 
We all strive to be python programmers, and some of the responses are that
it might not be around in the future.

Now we all probably speak conversational in other langs, but I'm thinking
of keeping around a great prototyping language.


So the topic becomes how too integrate it with the not just the expected,
but the unexpected technologies....Despite the topic is error messages, it
should apply to all possibilities that could be derived from a prototyping
language like python.


On Sat, Dec 14, 2013 at 11:09 PM, David Hutto <dwightdhutto at gmail.com>wrote:




-- 
Best Regards,
David Hutto
*CEO:* *http://www.hitwebdevelopment.com <http://www.hitwebdevelopment.com>*
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20131214/cc7b0da1/attachment.html>



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Vajrasky Ko
Attributes: []Content: 
On Sun, Dec 15, 2013 at 7:52 AM, Victor Stinner
<victor.stinner at gmail.com> wrote:

Okay, I have created ticket (and preliminary patch) for this
enhancement: http://bugs.python.org/issue19984

Vajrasky Kok



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
On 12/14/2013 07:51 PM, Steven D'Aprano wrote:

+1

--
~Ethan~



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 16 Dec 2013 02:58, "Ethan Furman" <ethan at stoneleaf.us> wrote:

It's less obviously correct for Python code, though. In C, we're almost
always running off slots, so type(obj).__name__ has a very high chance of
being what we want, and is also preferred for speed reasons (since it's
just a couple of pointer dereferences).

At the Python level, whether to display obj.__name__ (working with a class
directly), type(obj).__name__ (working with the concrete type, ignoring any
proxying) or obj.__class__.__name__ (which takes proxying into account)
really depends on exactly what you're doing, and the speed differences
between them aren't so stark.

Cheers,
Nick.

https://mail.python.org/mailman/options/python-dev/ncoghlan%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20131216/e411210c/attachment.html>



----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: =?UTF-8?B?V2FsdGVyIETDtnJ3YWxk?
Attributes: []Content: 
On 15.12.13 17:33, Ethan Furman wrote:

I'd vote for including the module name in the string and using 
__qualname__ instead of __name__, i.e. make "{:T}".format(obj) 
equivalent to 
"{0.__class__.__module__}.{0.__class__.qualname__}".format(obj).

Servus,
    Walter




----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
On 12/16/2013 10:29 AM, Walter D?rwald wrote:

That's not possible in general. The format specifier interpretation is
done by each type. So, you could add this to str.__format__ and
int.__format__, but you can't add it to an arbitrary type's __format__.
For example, types not in the stdlib would never know about it.

There's no logic for calling through to object.__format__ for unknown
specifiers. Look at datetime, for example. It uses strftime, so "T"
currently just prints a literal "T".

And for object.__format__, we recently made it an error to specify any
format string. This is to prevent you from calling
format(an_object, ".30")
and "knowning" that it's interpreted by str.__format__ (because that's
the default conversion for object.__format__). If in the future
an_object's class added its own __format__, this code would break (or at
least do the wrong thing).

But I really do like the idea! Maybe there's a way to just make
obj.__class__ recognize "T", so you could at least do:
format(obj.__class__, "T")
or equivalently:
"{:T}".format(obj.__class__)

I realize that having to use .__class__ defeats some of the beauty of
this scheme.

Eric.





----------------------------------------
Subject:
[Python-Dev] How long the wrong type of argument should we
 limit (or not) in the error message (C-api)?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 17 Dec 2013 02:23, "Eric V. Smith" <eric at trueblade.com> wrote:

That just suggests it would need to be a type coercion code, like !a, !r,
and !s. However, that observation also suggests that starting with a
"classname" or "typename" builtin would be more appropriate than jumping
directly to a formatting code.

We've definitely drifted well into python-ideas territory at this point,
though :)

Cheers,
Nick.

https://mail.python.org/mailman/options/python-dev/ncoghlan%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20131217/a0bc2320/attachment.html>

