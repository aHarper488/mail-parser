
============================================================================
Subject: [Python-Dev] cpython: Issue #14428: Use the new
 time.perf_counter() and time.process_time() functions
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #14428: Use the new
 time.perf_counter() and time.process_time() functions
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 29.04.2012 03:04, victor.stinner wrote:

[...]


Does it make sense to keep the options this way?  IMO the distinction should be
to use either perf_counter() or process_time(), and the options could implement
this (-t -> perf_counter, -c -> process_time).

Georg





----------------------------------------
Subject:
[Python-Dev] cpython: Issue #14428: Use the new
 time.perf_counter() and time.process_time() functions
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

You might need to use exactly the same clock to compare performance of
Python 3.2 and 3.3.

Adding an option to use time.process_time() is a good idea. Is anyone
interested to implement it?

Victor



----------------------------------------
Subject:
[Python-Dev] cpython: Issue #14428: Use the new
 time.perf_counter() and time.process_time() functions
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 01.05.2012 10:35, Victor Stinner wrote:

I implemented it in d43a8aa9dbef.  I also updated the docs in 552c207f65e4.

Georg


