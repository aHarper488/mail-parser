
============================================================================
Subject: [Python-Dev] Snakebite,
 buildbot and low hanging fruit --	feedback wanted! (Was Re: SSH
 access against buildbot boxes)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Snakebite,
 buildbot and low hanging fruit --	feedback wanted! (Was Re: SSH
 access against buildbot boxes)
----------------------------------------
Author: Scott Dia
Attributes: []Content: 
On 11/7/2010 9:58 PM, C. Titus Brown wrote:

Classy.

I don't remember being invited to help. snakebite.org is a dead end.
snakebite-list hasn't had a post for over a year. Where is the list of
things that you need done so that I can get started on that? Oh wait..

Seriously, all I asked was for you to tone down your insults to a
technology that is already solving problems today. Why you feel the need
to attack me personally is beyond my understanding. Furthermore, I don't
see why I need to be "helping" -- somebody who doesn't want help -- to
be able to deduce that your message is being insulting to the authors of
Buildbot.

On 11/7/2010 9:58 PM, C. Titus Brown wrote:

At least I am in good company. ;)

-- 
Scott Dial
scott at scottdial.com
scodial at cs.indiana.edu

