
============================================================================
Subject: [Python-Dev] cpython (2.7): - Issue #12603: Fix
 pydoc.synopsis() on files with non-negative st_mtime.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (2.7): - Issue #12603: Fix
 pydoc.synopsis() on files with non-negative st_mtime.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 27 Jul 2011 19:36:36 +0200
charles-francois.natali <python-checkins at python.org> wrote:

Surely you mean non-positive? Non-negative st_mtime being the common
case.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (2.7): - Issue #12603: Fix
 pydoc.synopsis() on files with non-negative st_mtime.
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 

Of course (st_mtime <= 0).

