
============================================================================
Subject: [Python-Dev] [Python-checkins] r87010 - in
 python/branches/py3k: Doc/library/subprocess.rst Lib/subprocess.py
 Lib/test/test_subprocess.py
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87010 - in
 python/branches/py3k: Doc/library/subprocess.rst Lib/subprocess.py
 Lib/test/test_subprocess.py
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 4 December 2010 22:51, Terry Reedy <tjreedy at udel.edu> wrote:

That's fair. Is this a "bad-enough mistake"? From a brief reading of
the 2 bug reports, combined with my own trouble-free experience with
Popen (always leaving close_fds as default), it doesn't seem so, to
me. If someone provides evidence that this is a serious bug in the
API, then that's a different matter.

Paul.

