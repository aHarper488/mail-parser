
============================================================================
Subject: [Python-Dev] Fwd: deep question re dict as formatting input
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
One of the students on an introductory Python 3 class asks a very good question about string formatting. This could be because the course materials are misleading, so I would like to understand. It would appear from tests that "{0[X]}".format(...) first tries to convert the string "X" to in integer. If it succeeds then __getitem__() is called with the integer as an argument, otherwise it is called with the string itself as an argument. Is this correct?

The documentation at http://docs.python.org/library/string.html#formatspec is silent on whether strings were ever intended to be used as subscripts. Does this seem sensible? Was it considered during design? Should I alter the materials so that only integer subscripts are used?

regards
 Steve


Begin forwarded message:


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110222/7a53741f/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Tue, Feb 22, 2011 at 6:01 PM, Steve Holden <steve at holdenweb.com> wrote:

This is addressed in the PEP 3101:
"""
    The rules for parsing an item key are very simple.
    If it starts with a digit, then it is treated as a number, otherwise
    it is used as a string.
""" http://www.python.org/dev/peps/pep-3101/

If current implementation does something more involved, I would say it is a bug.



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
Quoting PEP 3101:

An example of the 'getitem' syntax:

         "My name is {0[name]}".format(dict(name='Fred'))

It should be noted that the use of 'getitem' within a format string
is much more limited than its conventional usage.  In the above example,
the string 'name' really is the literal string 'name', not a variable
named 'name'.  The rules for parsing an item key are very simple.
If it starts with a digit, then it is treated as a number, otherwise
it is used as a string.


On 2/22/2011 6:01 PM, Steve Holden wrote:




----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Steve Holde
Attributes: []Content: 

On Feb 22, 2011, at 3:08 PM, Eric Smith wrote:

That's not strictly true:

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 21.1
'float'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: '21.2'

But I take your point, and should have thought to read the PEP. Thanks!

Kirby: Please apologize to Ethan. I can't remember being aware of the PEP 3101 specification quoted by Eric above. We will probably need to modify the course materials to take this wrinkle into account (at least by demonstrating that we are aware of it).

regards
 Steve





----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 

To the other question :


I think, parsing rule already helps to understand the problem with the
key like '1'.
The PEP also explicitly states that:

"""
Because keys are not quote-delimited, it is not possible to
    specify arbitrary dictionary keys (e.g., the strings "10" or
    ":-]") from within a format string.
"""
-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 2/22/2011 6:32 PM, Senthil Kumaran wrote:

Is this all specific in the lib docs? If not, it should be.


-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
On 2/22/2011 6:28 PM, Steve Holden wrote:

You are correct, I didn't exactly implement the PEP on this point, 
probably as a shortcut. I think there's an issue somewhere that 
discusses this, but I can't find it. The CPython implementation is 
really using "If every character is a digit, then it is treated as an 
integer, otherwise it is used as a string".

See find_name_split in Objects/stringlib/string_format.h, in particular 
the call to get_integer() and the interpretation of the result.

Eric.



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
On 02/22/2011 07:32 PM, Eric Smith wrote:

Just for the archives, I'll mention why it works this way. It's trying 
to support indexing by integers, as well as dictionary access using 
arbitrary keys. Both of course use the same syntax.

In this case it must convert the index values into ints:
 >>> a = ['usr', 'var']
 >>> '{0[0]} {0[1]}'.format(a)
'usr var'

And in this case it doesn't:
 >>> a = {'one':'usr', 'two':'var'}
 >>> '{0[one]} {0[two]}'.format(a)
'usr var'

Eric.



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Feb 23, 2011 at 9:32 AM, Senthil Kumaran <orsenthil at gmail.com> wrote:

I was curious as to whether or not nested substitution could be used
to avoid that limitation, but it would seem not:

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: '{1}'

Turns out that was also a deliberate design choice:

"""
These 'internal' replacement fields can only occur in the format
specifier part of the replacement field.  Internal replacement fields
cannot themselves have format specifiers.  This implies also that
replacement fields cannot be nested to arbitrary levels.
"""

Ah, how (much more) confused would we be if we didn't have the PEPs
and mailing list archives to remind ourselves of what we were thinking
years ago...

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 22 Feb 2011 19:32:56 -0500, Eric Smith <eric at trueblade.com> wrote:

Perhaps you are thinking of http://bugs.python.org/issue7951.  Not
directly on point, but related.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
On 02/23/2011 09:42 AM, R. David Murray wrote:

Yes, that's the one I was thinking of. Thanks, David.

I'm not sure I agree with all of the points raised there, but it does 
some additional background on the issue.



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On Feb 23, 2011, at 5:42 AM, Nick Coghlan wrote:

True. And how much more useful it would be if it were incorporated into the documentation after implementation. Too much of the format() stuff is demonstrated rather than explained.

regards
 Steve




----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Eric Smith wrote:

Given the representation issues with floating point, I think the current 
behavior is desirable.  Also, leaving digits with periods as strings 
would, I think, be more useful (Dewey Decimal, anyone?).

~Ethan~



----------------------------------------
Subject:
[Python-Dev] Fwd: deep question re dict as formatting input
----------------------------------------
Author: Eric Smit
Attributes: []Content: 

I think the documentation team has been pretty good about responding to
format() issues that have been brought up in the bug tracker.

Eric.



