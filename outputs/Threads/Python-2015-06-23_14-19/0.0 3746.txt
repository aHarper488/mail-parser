
============================================================================
Subject: [Python-Dev] Python Bug Day this Saturday announced
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python Bug Day this Saturday announced
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi everybody,

I just sent the announcement for the bug day to python-list (apparently
pending approval), core-mentorship and montrealpython.  Core developers
who plan on being on IRC can add themselves to the list on
http://wiki.python.org/moin/PythonBugDay so that people can connect
nicknames with people.

The list by Petri at http://piratepad.net/pyconfi-sprint-issues can
still be updated.  Otherwise we?ll fall back to the usual roundup query
for easy bugs.

Cheers!



----------------------------------------
Subject:
[Python-Dev] Python Bug Day this Saturday announced
----------------------------------------
Author: Petri Lehtine
Attributes: []Content: 
?ric Araujo wrote:

I've removed the issues for which a patch was submitted during the
PyCon Finland sprint, and retitled it as Python Bug Day issue list.

People found the list somewhat useful during our sprint. At least it
has some starting points to look at, before starting to dig through
the easy issues list.

