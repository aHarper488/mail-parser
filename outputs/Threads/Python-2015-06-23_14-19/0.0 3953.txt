
============================================================================
Subject: [Python-Dev] My CLA
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Python Contributor Agreement
----------------------------
I allow PSF to release all my code that I submitted to
it, under any open source license.
-- 
anatoly t.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130211/13f98ac8/attachment.html>



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: Oleg Broytma
Attributes: []Content: 
On Mon, Feb 11, 2013 at 03:49:39PM +0300, anatoly techtonik <techtonik at gmail.com> wrote:

   Good intention but wrong way of expressing it. Please do it properly --
via a signed paper. You can send it by snail mail, or you can scan it
and send by email.

Oleg.
-- 
     Oleg Broytman            http://phdru.name/            phd at phdru.name
           Programmers don't die, they just GOSUB without RETURN.



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Mon, Feb 11, 2013 at 4:01 PM, Oleg Broytman <phd at phdru.name> wrote:



What's wrong with it? Is the text not clear? Or there is a problem to
threat email as a document?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130211/441522f1/attachment.html>



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
2013/2/11 anatoly techtonik <techtonik at gmail.com>


See the "Submission Instructions" there:
http://www.python.org/psf/contrib/

The Contributor Agreement is part of a formal process. It's necessary to
follow the rules, even if they were written by a lawyer and we don't
understand all the reasons.

-- 
Amaury Forgeot d'Arc
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130211/6cb6f228/attachment.html>



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Anatoly, stop this discussion *NOW*. It is not appropriate for python-dev
and you risk being banned from python-dev if you keep it up.


On Mon, Feb 11, 2013 at 10:18 AM, anatoly techtonik <techtonik at gmail.com>wrote:



-- 
--Guido van Rossum (python.org/~guido)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130211/51ba0592/attachment.html>



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: Oleg Broytma
Attributes: []Content: 
On Mon, Feb 11, 2013 at 09:18:58PM +0300, anatoly techtonik <techtonik at gmail.com> wrote:

   Yes, email is not a legally recognized document. Electronic signature
*could* make it legally recognizable but it very much depends on the
organization where you send email to and on the certificate you use to
sign mail.
   Contact PSF for details. I doubt python-dev is a proper list to
discuss PSF-related legal issues.

Oleg.
-- 
     Oleg Broytman            http://phdru.name/            phd at phdru.name
           Programmers don't die, they just GOSUB without RETURN.



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Mon, Feb 11, 2013 at 12:30 PM, Oleg Broytman <phd at phdru.name> wrote:

There are no further details. Either the proper document is signed or it isn't.

Hopefully this is the end of the discussion.

Brian Curtin
Director
Python Software Foundation



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Mon, Feb 11, 2013 at 9:27 PM, Guido van Rossum <guido at python.org> wrote:


It is not a problem for me to keep silence for another couple of months.
But this weekend there will be an open source conference in Belarus [1],
and I will need to explain what this specific CLA is about in
developer-friendly language translated to Russian.

So what would be the best place to discuss the matter in public, so that I
can give a link to the discussion afterwards?

1. http://lvee.org/en/main
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130211/a916c955/attachment.html>



----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 11 Feb 2013 22:07:50 +0300
anatoly techtonik <techtonik at gmail.com> wrote:

The Python contributor agreement allows the PSF to safely redistribute
your contributions under its own license, the PSF license.

The Python contributor agreement is *not* a copyright assignment: you
legally remain the author of the code you contributed (i.e. you can also
publish it elsewhere under any license you want).

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] My CLA
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 


On Monday, February 11, 2013 at 2:23 PM, Antoine Pitrou wrote:

FWIW: Django's FAQ spells out the same reasons we have one for Python:

https://www.djangoproject.com/foundation/cla/faq/

just s/Django/Python/ and s/Django Software Foundation/Python Software Foundation/ - it's a good concise FAQ. 



