
============================================================================
Subject: [Python-Dev] Issue 6081: format string using mapping rather than
	kwargs
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Issue 6081: format string using mapping rather than
	kwargs
----------------------------------------
Author: =?UTF-8?Q?Filip_Gruszczy=C5=84ski?
Attributes: []Content: 
Hello everyone,

I was told to bring this issue to python-dev, but I was reluctant to
do this until I was confident I can solve the problem. I have managed
to provide the patch that works, so now I can talk about how this
should be done, just as Eric told me. Here is the link to the ticket:
http://bugs.python.org/issue6081.

In the issue Raymond Hettinger proposed adding method to string, that
would allow to explicitly pass subscriptable object, from which
formatting of a string would be done. Two questions arise:
1) whether add such functionality at all?
2) how the method should be called? Should formatting options based on
*args be kept (like using {0} in formatted string) or not and how
argument should be passed. Eric suggested passing only mapping and
only adding *args, if that is found useful by users; I have though
about having both mapping and *args, which would keep it similar to
the old formatting. Another option is to have *args and one keyword
with the mapping (instead of kwargs).

I would appreciate any advice on this topic, even if this ticket would
be dismissed altogether, as I would like to learn as much as possible
on practices on developing Python.

-- 
Filip Gruszczy?ski

