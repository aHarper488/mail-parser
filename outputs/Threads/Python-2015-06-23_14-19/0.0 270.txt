
============================================================================
Subject: [Python-Dev] [Python-checkins] r86945 -
	python/branches/py3k/Lib/test/__main__.py
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86945 -
	python/branches/py3k/Lib/test/__main__.py
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
I just want to say thanks for doing this, Michael. __main__.py is IMO
woefully underused and it's great to see Python dogfooding the feature
along with making it easier to explain how to run our unit tests.

On Thu, Dec 2, 2010 at 17:34, michael.foord <python-checkins at python.org> wrote:

