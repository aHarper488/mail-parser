
============================================================================
Subject: [Python-Dev] Format factories (was Re: sWAPcASE Was:
 transform() and untransform() methods, and the codec registry)
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Format factories (was Re: sWAPcASE Was:
 transform() and untransform() methods, and the codec registry)
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Thu, Dec 9, 2010 at 16:26, Nick Coghlan <ncoghlan at gmail.com> wrote:

But is this worth it since once you write it you won't be changing it
again, suggesting that taking the time to look up the formatting rules
isn't that much harder and wouldn't burden us w/ writing such
functions and trying to come up with a good API. I suspect what would
be more helpful is either have a rather detailed docstring for
str.format or to at least put the URL to the docs which explains the
mini-language to make it easier to find.

-Brett




----------------------------------------
Subject:
[Python-Dev] Format factories (was Re: sWAPcASE Was:
 transform() and untransform() methods, and the codec registry)
----------------------------------------
Author: Jeffrey Yasski
Attributes: []Content: 
On Sat, Dec 11, 2010 at 11:21 AM, Brett Cannon <brett at python.org> wrote:

Having a make_format_spec() would also help people trying to read the code.




----------------------------------------
Subject:
[Python-Dev] Format factories (was Re: sWAPcASE Was:
 transform() and untransform() methods, and the codec registry)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, Dec 12, 2010 at 5:21 AM, Brett Cannon <brett at python.org> wrote:

Yes, it may be worth it, since it greatly simplifies things like
generating a format spec programmatically rather than hardcoding it.
You can also run it once at the interactive prompt to figure out the
format string you want to hard code, then include the API call as a
comment for concise documentation of what your format string does. I
helped *define* the new format spec and I still need to reference the
docs to create trickier ones. A factory function would be far more
convenient than burdening str.format's docstring with a full
definition of the format spec syntax.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

