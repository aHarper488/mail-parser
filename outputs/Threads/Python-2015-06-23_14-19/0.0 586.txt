
============================================================================
Subject: [Python-Dev] python-checkins replies
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] python-checkins replies
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 12/07/2010 23:48, Eric Smith wrote:
I think this would be a good idea. It would be nice to have on-topic 
traffic here. :-)

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] python-checkins replies
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jul 12, 2010 at 16:42, Michael Foord <fuzzyman at voidspace.org.uk>wrote:

Or python-committers since this is discussing code already checked in and
thus is somewhat committer-specific. This also has the perk of being easier
to spot (don't know about the rest of you but my python-committers filter
makes those emails much more obvious than python-dev traffic).

-Brett


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100713/216309af/attachment.html>



----------------------------------------
Subject:
[Python-Dev] python-checkins replies
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 13.07.2010 22:29, schrieb Brett Cannon:


I think I've suggested this once, but it met some resistance IIRC (it supposedly
made our development exclusive).

I'm still +1 on the idea though, and +1 on python-committers.

Georg




----------------------------------------
Subject:
[Python-Dev] python-checkins replies
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
On 7/14/2010 4:21 AM, Georg Brandl wrote:

That's why I think it should go on python-dev. If the code hadn't been 
checked in and you were asking "what do you think of solving this by 
using the following code", I think you'd put it on python-dev. I'd want 
the discussion of an actual checkin to occur in that same venue.


That said, I'm +1 on the idea, but only +0 on python-dev.

Eric.



----------------------------------------
Subject:
[Python-Dev] python-checkins replies
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Jul 14, 2010 at 01:36, Eric Smith <eric at trueblade.com> wrote:



Actually, I probably wouldn't. =) When it gets to explicit code, a design
decision has been made, so I do not need to worry about involving the
general public in some low-level technical discussion that won't impact
them.




Right, which is why I want python-committers. Otherwise it's just a
glorified commit lock when we are cutting releases.




+1 on python-committers, +0 on python-dev.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100714/5afd3a6d/attachment.html>



----------------------------------------
Subject:
[Python-Dev] python-checkins replies
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Jul 15, 2010 at 5:22 AM, Brett Cannon <brett at python.org> wrote:

Yep, that's my perspective as well. If my post-commit comments are
more significant than typo fixes and internal naming suggestions, I'll
take them back to python-dev manually.

So for me, +1 on python-committers, +0 on python-dev or the status quo.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] python-checkins replies
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Tue, Jul 13, 2010 at 2:42 AM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

+1 FWIW. It is a good practice to make code reviews public. It teaches
people the proper ways and encourages to participate in reviews.
-- 
anatoly t.

