
============================================================================
Subject: [Python-Dev] =?utf-8?q?os=2Epath_function_for_=E2=80=9Cget_the_re?=
 =?utf-8?q?al_filename=E2=80=9D_=28was=3A_os=2Epath=2Enormcase_rationale?=
 =?utf-8?b?Pyk=?=
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] =?utf-8?q?os=2Epath_function_for_=E2=80=9Cget_the_re?=
 =?utf-8?q?al_filename=E2=80=9D_=28was=3A_os=2Epath=2Enormcase_rationale?=
 =?utf-8?b?Pyk=?=
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Greg Ewing <greg.ewing at canterbury.ac.nz> writes:


Your heuristics seem to assume there will only ever be a maximum of one
match, which is false. I present the following example:

    $ ls foo/
        bAr.dat  BaR.dat  bar.DAT


And what if there are also matches for a case-insensitive search? e.g.
searching for ?foo/bar.DAT? in the above example.


And what if several matches are found? e.g. searching for ?foo/BAR.DAT?
in the above example.


It seems to me this whole thing should be hashed out on ?python-ideas?.

-- 
 \           ?In case you haven't noticed, [the USA] are now almost as |
  `\     feared and hated all over the world as the Nazis were.? ?Kurt |
_o__)                                                   Vonnegut, 2004 |
Ben Finney


