
============================================================================
Subject: [Python-Dev] Jython roadmap
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Jython roadmap
----------------------------------------
Author: =?ISO-8859-1?Q?=22Juancarlo_A=F1ez_=28Apalala=29=22?
Attributes: []Content: 
It seems that Jython is under the Python Foundation, but I can't find a 
roadmap, a plan, or instructions about how to contribute to it reaching 
2.7 and 3.3.

Are there any pages that describe the process?

Thanks in advance,

-- Juanca




----------------------------------------
Subject:
[Python-Dev] Jython roadmap
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

Zitat von "Juancarlo A?ez (Apalala)" <apalala at gmail.com>:


Hi Juanca,

These questions are best asked on the jython-dev mailing list, see

http://sourceforge.net/mail/?group_id=12867

python-dev is primarily focussed on CPython instead. There doesn't
seem to be much contributor information in the web; all I could find
is the bug reporting instructions:

http://www.jython.org/docs/bugs.html

Regards,
Martin







----------------------------------------
Subject:
[Python-Dev] Jython roadmap
----------------------------------------
Author: Jeff Alle
Attributes: []Content: 

On 21/08/2012 06:34, martin at v.loewis.de wrote:
Hi Juancarlo:

I'm cross-posting this for you on jython-dev as Martin is right. Let's 
continue there.

Jython does need new helpers and I agree it isn't very easy to get 
started. And we could do with a published roadmap.

I began by fixing a few bugs (about a year ago now), as that seemed to 
be the suggestion on-line and patches can be offered unilaterally. 
(After a bit of nagging) some of these got reviewed and I'd won my spurs.

I found the main difficulty to be understanding the source, or rather 
the architecture: there is too little documentation and some of what you 
can find is out of date (svn?). A lot of basic stuff is still a complete 
mystery to me. As I've discovered things I've put them on the Jython 
Wiki ( http://wiki.python.org/jython/JythonDeveloperGuide ) in the hope 
of speeding others' entry, including up-to-date description of how to 
get the code to build in Eclipse.

One place to look, that may not occur to you immediately, is Frank 
Wierzbicki's blog ( http://fwierzbicki.blogspot.co.uk/ ). Frank is the 
project manager for Jython, an author of the Jython book, and has worked 
like a Trojan (the good kind, not the horse) over the last 6 months. 
Although Frank has shared inklings of a roadmap, it must be difficult to 
put dates to things that depend on a small pool of volunteers working in 
their spare time -- especially perfectionist volunteers who write more 
Javadoc than actual code, then delete it all because they've had a 
better idea :-). Direction of travel is easier: 2.5.3 is out, we're 
trying to get to 2.7b, but with an eye on 3.3. I haven't seen anything 
systematic on what's still to do, who's doing it, and where the gaps 
are, which is probably what you're looking for. ... Frank?

Jeff Allen



