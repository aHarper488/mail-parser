
============================================================================
Subject: [Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
Post Count: 18
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
I'm happy to announce the first release candidate of 2.7.4.

2.7.4 will be the latest maintenance release in the Python 2.7 series.
It includes hundreds of bugfixes to the core language and standard
library.

There has recently been a lot of discussion about XML-based denial of service
attacks. Specifically, certain XML files can cause XML parsers, including ones
in the Python stdlib, to consume gigabytes of RAM and swamp the CPU. 2.7.4 does
not include any changes in Python XML code to address these issues. Interested
parties should examine the defusedxml package on PyPI:
https://pypi.python.org/pypi/defusedxml

2.7.4 release candidate 1 is a testing release. Deploying it in production is
not recommended. However, please download it and test with your libraries and
applications, reporting any bugs you may find.

Assuming no horrible bugs rear their heads, a final release of 2.7.4 will occur
in 2 weeks.

Downloads are at

    http://python.org/download/releases/2.7.4/

As always, please report bugs to

    http://bugs.python.org/

Enjoy,
Benjamin Peterson
2.7 Release Manager



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Matthias Klos
Attributes: []Content: 
Am 25.03.2013 01:30, schrieb Benjamin Peterson:

I hope it's not (and in the IDLE thread you say so otherwise too).

  Matthias





----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Lennart Regebr
Attributes: []Content: 
On Tue, Mar 26, 2013 at 11:54 AM, Matthias Klose <doko at ubuntu.com> wrote:

It most certainly will be the latest once it's released. But hopefully
not the last. :-)

//Lennart



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2013/3/26 Lennart Regebro <regebro at gmail.com>:

I also read "the last" by mistake!

Anyway, you should trust Brett Canon: "Python 3.3: Trust Me, It's
Better Than Python 2.7".

https://speakerdeck.com/pyconslides/python-3-dot-3-trust-me-its-better-than-python-2-dot-7-by-dr-brett-cannon

Victor



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: a.cavallo at cavallinux.e
Attributes: []Content: 
It's already hard to sell 2.7 in most companies.

Regards,
Antonio





----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/3/26 Matthias Klose <doko at ubuntu.com>:

"latest" is different from "last" :)


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Tue, 26 Mar 2013 12:34:34 +0100,
Victor Stinner <victor.stinner at gmail.com> a ?crit :


You can always trust Brett Cannon!

cheers

Antoine.





----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 26 Mar 2013 12:54:11 +0100
a.cavallo at cavallinux.eu wrote:

Sure, it's often hard to sell free software!

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 26.03.2013 13:13, schrieb Benjamin Peterson:

As opposed to 3.2.4, which will really be the last (the last regular one,
that is -- if/when the XML stuff gets released there will be a source-only
release).

Georg




----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Sean Felipe Wolf
Attributes: []Content: 
On Tue, Mar 26, 2013 at 4:34 AM, Victor Stinner
<victor.stinner at gmail.com> wrote:

Was there supposed to be audio with this, or is it slides only? I got
no audio :P



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Hynek Schlawac
Attributes: []Content: 
Am 26.03.2013 um 23:05 schrieb Sean Felipe Wolfe <ether.joe at gmail.com>:


Speakerdeck is slides only. The video is here: http://pyvideo.org/video/1730/python-33-trust-me-its-better-than-27




----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Sean Felipe Wolf
Attributes: []Content: 
On Tue, Mar 26, 2013 at 3:26 PM, Hynek Schlawack <hs at ox.cx> wrote:

Sweet thanks!



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Andriy Kornatsky
Attributes: []Content: 
Any plans backport decimal C implementation from 3.3?

Thanks.

Andriy Kornatskyy


----------------------------------------



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
No. _decimal is new functionality that will never be backported.

On Wed, Mar 27, 2013 at 9:40 AM, Andriy Kornatskyy
<andriy.kornatskyy at live.com> wrote:



-- 
Thanks,
Andrew Svetlov



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Andriy Kornatsky
Attributes: []Content: 
Andrew,

Thank you for the prompt response back.


Who knows?

Is python 3.3 _decimal interface 100% compatible with one pure python implementation in 2.7?

I suppose code written using decimals in python 2.7 should work in python 3 with no changes, or there should be taken caution for something?

According to presentation slides, 30x performance boost is something definitely valuable for 2.7 crowd.

Thanks.

Andriy


----------------------------------------



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
2013/3/27 Andrew Svetlov <andrew.svetlov at gmail.com>


No. 2.7 does not accept new features anymore, but you can install
this backport from PyPI: https://pypi.python.org/pypi/cdecimal/2.3

-- 
Amaury Forgeot d'Arc
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130327/0ab73f9e/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Andriy Kornatsky
Attributes: []Content: 

It is clear now.


This is what I was looking for.

Thanks.

Andriy


________________________________



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7.4 release candidate 1
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Andriy Kornatskyy <andriy.kornatskyy at live.com> wrote:

Note that for numerical work _decimal from Python3.3 is vastly faster than
cdecimal. I've added two major speedups (credit for one of them goes to
Antoine), so these are the numbers for the pi benchmark:

Python3.3 (_decimal):    time:  0.15s
Python3.3 (decimal.py):  time: 17.22s
=====================================

Python2.7 (cdecimal):    time:  0.29s
Python2.7 (decimal.py):  time: 17.74s
=====================================


For database work and such the numbers should be about the same.


Stefan Krah



