
============================================================================
Subject: [Python-Dev] PyCon 2011 Reminder: Call for Proposals,
	Posters and Tutorials - us.pycon.org
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PyCon 2011 Reminder: Call for Proposals,
	Posters and Tutorials - us.pycon.org
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
PyCon 2011 Reminder: Call for Proposals, Posters and Tutorials - us.pycon.org
===============================================

Well, it's October 25th! The leaves have turned and the deadline for submitting
main-conference talk proposals expires in 7 days (November 1st, 2010)!

We are currently accepting main conference talk proposals:
http://us.pycon.org/2011/speaker/proposals/

Tutorial Proposals:
http://us.pycon.org/2011/speaker/proposals/tutorials/

Poster Proposals:
http://us.pycon.org/2011/speaker/posters/cfp/

PyCon 2011 will be held March 9th through the 17th, 2011 in Atlanta, Georgia.
(Home of some of the best southern food you can possibly find on Earth!) The
PyCon conference days will be March 11-13, preceded by two tutorial days
(March 9-10), and followed by four days of development sprints (March 14-17).

We are also proud to announce that we have booked our first Keynote
speaker - Hilary Mason, her bio:

"Hilary is the lead scientist at bit.ly, where she is finding sense in vast
data sets. She is a former computer science professor with a background in
machine learning and data mining, has published numerous academic papers, and
regularly releases code on her personal site, http://www.hilarymason.com/.
She has discovered two new species, loves to bake cookies, and asks way too
many questions."

We're really looking forward to having her this year as a keynote speaker!

Remember, we've also added an "Extreme" talk track this year - no introduction,
no fluff - only the pure technical meat!

For more information on "Extreme Talks" see:

http://us.pycon.org/2011/speaker/extreme/

We look forward to seeing you in Atlanta!

Please also note - registration for PyCon 2011 will also be capped at a
maximum of 1,500 delegates, including speakers. When registration opens (soon),
you're going to want to make sure you register early! Speakers with accepted
talks will have a guaranteed slot.

We have published all registration prices online at:
http://us.pycon.org/2011/tickets/

Important Dates
November 1st, 2010: Talk proposals due.
December 15th, 2010: Acceptance emails sent.
January 19th, 2011: Early bird registration closes.
March 9-10th, 2011: Tutorial days at PyCon.
March 11-13th, 2011: PyCon main conference.
March 14-17th, 2011: PyCon sprints days.
Contact Emails:

Van Lindberg (Conference Chair) - van at python.org
Jesse Noller (Co-Chair) - jnoller at python.org
PyCon Organizers list: pycon-organizers at python.org

