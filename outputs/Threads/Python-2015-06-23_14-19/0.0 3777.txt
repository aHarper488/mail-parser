
============================================================================
Subject: [Python-Dev] What's New in Python 3.3: missing unittest.mock!
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] What's New in Python 3.3: missing unittest.mock!
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

I just noticed that the new unittest.mock module is missing from the
What's New in Python 3.3. Can someone add it? (I cannot right now).

Thanks.
Victor

