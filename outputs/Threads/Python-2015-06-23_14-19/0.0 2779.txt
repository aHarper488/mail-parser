
============================================================================
Subject: [Python-Dev] why is _PyBytes_Join not public but PyUnicode_Join
 is?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] why is _PyBytes_Join not public but PyUnicode_Join
 is?
----------------------------------------
Author: MRA
Attributes: []Content: 
On 31/08/2012 02:43, Gregory P. Smith wrote:
For what it's worth, I could also make use of it in the regex module.

I use PyUnicode_Join when working with Unicode strings, but I could also
use PyBytes_Join if it were available instead of having to look it up.



----------------------------------------
Subject:
[Python-Dev] why is _PyBytes_Join not public but PyUnicode_Join
 is?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 31.08.12 03:43, schrieb Gregory P. Smith:

API minimalism. No API should be public unless somebody can demonstrate
an actual use case.

The Unicode API of Python 2.0 had a serious mistake in making dozens
of functions public in the API. This broke twice in Python's history:
once when UCS-4 became supported, and again for PEP 393. For the former,
a work-around was possible by introducing macros, to support API
compatibility while breaking ABI compatibility. For PEP 393, huge
efforts were necessary to even preserve the API (and this only worked
with limitations).

So by default, all new functions should be internal API (static if
possible), until somebody has explicitly considered use cases and
considered what kind of stability can be guaranteed for the API.

 > Looking up the bytes 'join' method and using the C API to call that
 > method object with proper parameters seems like overkill in the case
 > where we're not dealing with user supplied byte strings at all.

It's not really that difficult. Instead of

   r = PyBytes_Join(sep, x);

you write

   r = PyObject_CallMethod(sep, "join", "O", x);

This is just a few more letters to type.

Or are you concerned with the runtime overhead that this causes?
Don't be: the cost of actually joining is much higher than the
cost of making the call.

Regards,
Martin

