
============================================================================
Subject: [Python-Dev] Not-a-Number (was PyObject_RichCompareBool
	identity shortcut)
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
	identity shortcut)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Apr 28, 2011 at 7:17 PM, Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:

And 3rd party NaNs can still do whatever the heck they want :)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
	identity shortcut)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 4/28/2011 4:40 AM, Mark Shannon wrote:


The problem is that the committee itself did not believe or stay 
consistent with that. In the text of the draft, they apparently refer to 
Nan as an indefinite, unspecified *number*. Sort of like a random 
variable with a uniform pseudo* distribution over the reals (* 0 
everywhere with integral 1). Or a quantum particle present but smeared 
out over all space. And that apparently is their rationale for Nan != 
NaN: an unspecified number will equal another unspecified number with 
probability 0. The rationale for bool(NaN)==True is that an unspecified 
*number* will be 0 with probability 0. If Nan truly indicated an 
*absence* (like 0 and '') then bool(NaN) should be False,

I think the committee goofed -- badly. Statisticians used missing value 
indicators long before the committee existed. They has no problem 
thinking that the indicator, as an object, equaled itself. So one could 
write (and I often did through the 1980s) the equivalent of

for i,x in enumerate(datavec):
   if x == XMIS: # singleton missing value indicator for BMDP
     datavec[i] = default

(Statistics packages have no concept of identity different from equality.)

If statisticians had made XMIS != XMIS, that obvious code would not have 
worked, as it will not today for Python. Instead, the special case 
circumlocution of "if isXMIS(x):" would have been required, adding one 
more unnecessary function to the list of builtins.

NaN is, in its domain, the equivalent of None (== Not a Value), which 
also serves an an alternative to immediately raising an exception. But 
like XMIS, None==None. Also, bool(None) is corretly for something that 
indicates absence.



As I said, so did the committee, and that was its mistake that we are 
more or less stuck with.


Like None


This is wrong if False/True are interpreted as probabilities 0 and 1.


Like None.


Agreed, if we were starting fresh.


Also agreed.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
	identity shortcut)
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Fri, Apr 29, 2011 at 12:10 AM, Stephen J. Turnbull
<stephen at xemacs.org> wrote:

ISTM that the current behavior of NaN (never mind the identity issue)
helps numeric experts write better code. For naive users, however, it
causes puzzlement if they ever run into it.

Decimal, for that reason, has a context that lets one specify
different behaviors when a NaN is produced. Would it make sense to add
a float context that also lets one specify what should happen? That
could include returning Inf for 1.0/0.0 (for experts), or raising
exceptions when NaNs are produced (for the numerically naive like
myself).

I could see a downside too, e.g. the correctness of code that
passingly uses floats might be affected by the context settings.
There's also the question of whether the float context should affect
int operations; floats vs. ints is another can of worms since (in
Python 3) we attempt to tie them together through 1/2 == 0.5, but ints
have a much larger range than floats.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
	identity shortcut)
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Fri, Apr 29, 2011 at 1:11 PM, Guido van Rossum <guido at python.org> wrote:

ISTM, this is approaching py4k territory.  Adding contexts will not
solve backward compatibility problem unless you introduce a "quirks"
contexts that would preserve current warts and make it default.

For what it's worth, I think the next major version of Python should
use decimal as its main floating point type an leave binary floats to
numerical experts.



----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
	identity shortcut)
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Apr 30, 2011 at 3:11 AM, Guido van Rossum <guido at python.org> wrote:

Given that we delegate most float() behaviour to the underlying CPU
and C libraries (and then the math module tries to cope with any
cross-platform discrepancies), introducing context handling isn't
easy, and would likely harm the current speed advantage that floats
hold over the decimal module.

We decided that losing the speed advantage of native integers was
worthwhile in order to better unify the semantics of int and long for
Py3k, but both the speed differential and the semantic gap between
float() and decimal.Decimal() are significantly larger.

However, I did find Terry's suggestion of using the warnings module to
report some of the floating point corner cases that currently silently
produce unexpected results to be an interesting one. If those
operations issued a FloatWarning, then users could either silence them
or turn them into errors as desired.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
	identity shortcut)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 5/1/2011 7:27 AM, Nick Coghlan wrote:


I would like to take credit for that, but I was actually seconding 
Alexander's insight and idea. I may have added the specific name after 
looking at the currently list and seeing UnicodeWarning and 
BytesWarning, so why not a FloatWarning. I did read the warnings doc 
more carefully to verify that it would really put the user in control, 
which was apparently the intent of the committee.

I am not sure whether FloatWarnings should ignored or printed by 
default. Ignored would, I guess, match current behavior, unless 
something else is changed as part of a more extensive overhaul. -f and 
-ff are available to turn ignored FloatWarning into print or raise 
exception, as with BytesWarning. I suspect that these would get at lease 
as much usage as -b and -bb.

So I see 4 questions:
1. Add FloatWarning?
2. If yes, default disposition?
3. Add command line options?
4. Use the addition of FloatWarning as an opportunity to change other 
defaults, given that user will have more options?

-- 
Terry Jan Reedy


