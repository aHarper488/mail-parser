
============================================================================
Subject: [Python-Dev] devguide: Basic instructions on how to generate a
 patch with hg for	non-committers.
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devguide: Basic instructions on how to generate a
 patch with hg for	non-committers.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 06 Feb 2011 02:10:15 +0100
brett.cannon <python-checkins at python.org> wrote:

Should be --patch.
The problem is that it will show one several patch per changeset, which
is normally not what you want (it's a pity "hg out" doesn't have an
option to collapse them all).


You should commit before using "outgoing", otherwise the added file is
not in the repo (and therefore not in the patch).

The problem with hg (and other DVCSes) is that allows for *several*
local workflows, and therefore it's harder to advocate one of them in
such tutorial docs. I wonder what Georg and Dirkjan suggest.

We could perhaps present SVN-like "work in the working copy" workflow
(without local commits), and let seasoned hg users choose other
workflows they like more (they don't need our help anyway).


Or "hg revert -a", which is nicer to type.






----------------------------------------
Subject:
[Python-Dev] devguide: Basic instructions on how to generate a
 patch with hg for	non-committers.
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 06/02/2011 17:15, Antoine Pitrou a ?crit :

I suggest you request that feature upstream.

In the meantime, one can use hg diff -r $upstream-tip:tip to diff two
anonymous branches.  Using a named branch or local tags helps
identifying $upstream-tip.

Regards



----------------------------------------
Subject:
[Python-Dev] devguide: Basic instructions on how to generate a
 patch with hg for	non-committers.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 06 Feb 2011 19:10:37 +0100
?ric Araujo <merwok at netwok.org> wrote:

Yes. But that's where we start advocating a particular local workflow
over another (why named branches rather than mercurial queues or
bookmarks, for example?). That's why I think that part of the devguide
should stick to a trivial SVN-like use, letting people learn about more
powerful options in other resources.

Regards

Antoine.




