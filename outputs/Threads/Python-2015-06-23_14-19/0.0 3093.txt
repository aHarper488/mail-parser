
============================================================================
Subject: [Python-Dev] cpython: Add syntax highlighter tool
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Add syntax highlighter tool
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 01.07.2012 01:58, raymond.hettinger wrote:

Uh, this looks quite a lot like a new feature...

Since it's in Tools, I'm not going to veto it, just as with the
improvements to the gdb helper, but it would have been nice to
at least *ask*...

Georg


