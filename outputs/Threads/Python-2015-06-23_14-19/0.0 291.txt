
============================================================================
Subject: [Python-Dev] [Python-checkins] r87118 - in
	python/branches/py3k: Mac/BuildScript/build-installer.py
	Makefile.pre.in
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r87118 - in
	python/branches/py3k: Mac/BuildScript/build-installer.py
	Makefile.pre.in
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <4CFE6DD1.9030801 at netwok.org>,
 ?ric Araujo <merwok at netwok.org> wrote:

There is no guarantee which version of Python the build-installer script 
is being run with and, hence, no guarantee which version of sysconfig is 
available.  Typically it is an older system-supplied Python which is 
fine as it is merely used to run build-installer (which includes running 
Sphinx to build the documentation) and otherwise has no impact on the 
Python being built.

-- 
 Ned Deily,
 nad at acm.org


