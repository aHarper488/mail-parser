
============================================================================
Subject: [Python-Dev] zipimport to read from a file object instead of
	just a path?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] zipimport to read from a file object instead of
	just a path?
----------------------------------------
Author: Shy Shalo
Attributes: []Content: 


Maybe it can be seeded by both a string path *or* a file object, the same way
zipfile.ZipFile does.
This way I'll be able to somehow override or re-initialize zipimport with my own
StringIO

- Shy


