
============================================================================
Subject: [Python-Dev] Request for clarification of PEP 380
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Request for clarification of PEP 380
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Hi,

The scenario is this:
A generator, G, has a non-generator sub-iterator, S,
(ie G includes a "yield from S" experssion and S is not a generator)
and either G.close() or G.throw(GeneratorExit) is called.

In the current implementation, S.close() is called and,
if that call raises an exception, then that exception is suppressed.

Should close() be called at all? I know that it helps non-generators
to support the protocol, but there is the problem of iterables that 
happen to have a close method. This may cause unwanted side effects.

Why is the exception suppressed?

The text of the PEP seems to implicitly assume that all sub-iterators
will be generators, so it is not clear on the above points.

Cheers,
Mark.



----------------------------------------
Subject:
[Python-Dev] Request for clarification of PEP 380
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 9, 2012 at 12:06 AM, Mark Shannon <mark at hotpy.org> wrote:

On the contrary, this question is explicitly addressed in the PEP:
http://www.python.org/dev/peps/pep-0380/#finalization

If you want to block an inadvertent close() call, you need to either
wrap the object so it doesn't implement unwanted parts of the
generator API (which is the iterator protocol plus send(), throw() and
close()), or else you should continue to use simple iteration rather
than delegation.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Request for clarification of PEP 380
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Nick Coghlan wrote:

I should have read it more carefully.


What about the exception being suppressed?
That doesn't seem to be mentioned.

Cheers,
Mark.


