
============================================================================
Subject: [Python-Dev] [Python-ideas] Coroutines and PEP 380
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-ideas] Coroutines and PEP 380
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Nick Coghlan wrote:
If by the author you mean me, then of course it can be included.
Since it is a fork of CPython and I haven't changed the licence
I assumed it already was under the PSF licence.
Hard to measure, but it adds about 200 lines of code.
Its all fully portable standard C.
That may well be the biggest obstacle :)

One other obstacle (and this may be a killer) is that it may not be
practical to refactor Jython to use coroutines since Jython compiles
Python direct to JVM bytecodes and the JVM doesn't support coroutines.
Jython should be able to support yield-from much more easily.

Cheers,
Mark.

