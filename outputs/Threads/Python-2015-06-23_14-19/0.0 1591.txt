
============================================================================
Subject: [Python-Dev] Proposal / Questions about OrderedDict literals
 and/or faster C implementation
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Proposal / Questions about OrderedDict literals
 and/or faster C implementation
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hello,

Thanks for wanting to contribute and welcome to python-dev :)

Ideas are usually discussed first on python-ideas to assess usefulness,
get the pulse of the community, beat the API into shape and such things
before coming up to python-dev.  (A number of core devs are on both lists.)

You may want to search the mail archive for all the previous threads
about adding a C version and literal notation for ordered dicts.

Regards

