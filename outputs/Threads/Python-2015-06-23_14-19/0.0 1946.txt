
============================================================================
Subject: [Python-Dev] Fwd: [Python-committers] Pulling from contributors
	repositories
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Fwd: [Python-committers] Pulling from contributors
	repositories
----------------------------------------
Author: Hirokazu Yamamot
Attributes: []Content: 
I've read the Python-committers thread "Pulling from contributors 
repositories", which is about version control system. It seems there are 
two main issues, linear (cleaner) history on pushing, and NEWS merging. 
I'm newby of bazaar, but it seems to have a solution for first issue.

$ bzr checkout /repo/trunk

$ bzr merge /repo/feature-a

$ bzr revert --forget-merges

$ bzr push

See 
http://doc.bazaar.canonical.com/latest/en/user-guide/adv_merging.html#merging-without-parents


