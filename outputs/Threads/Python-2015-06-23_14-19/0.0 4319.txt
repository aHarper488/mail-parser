
============================================================================
Subject: [Python-Dev] Increase the code coverage of "OS" module
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Increase the code coverage of "OS" module
----------------------------------------
Author: rakesh karant
Attributes: []Content: 
Hi python-dev,
I'm interested in increasing the code coverage of the Python stdlib library "OS"Can some one who is already working on this or on a similar issue enlighten me on this?
Thanks in advance.
RegardsRakesh.G.K 		 	   		  
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130322/6156aae8/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Increase the code coverage of "OS" module
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Fri, Mar 22, 2013 at 2:28 AM, rakesh karanth <rakeshgk21 at hotmail.com> wrote:

Hey

You can check out pypy os tests, we cover a bit more than CPython.
(it's py.test based-though you might need to adapt it)

