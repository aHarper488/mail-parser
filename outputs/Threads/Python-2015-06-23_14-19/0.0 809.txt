
============================================================================
Subject: [Python-Dev] Did I miss the decision to untabify all of the C
 code?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Did I miss the decision to untabify all of the C
 code?
----------------------------------------
Author: Eric Smit
Attributes: []Content: 
Antoine Pitrou wrote:

That's my point. Since it's basically unreviewable, is it smart to do it 
during a beta?

I grant you that it's a largely a mechanized change (except for the "a 
posteriori manual intervention" part), but still.

Eric.

