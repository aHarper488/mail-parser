
============================================================================
Subject: [Python-Dev] [compatibility-sig] making sure
	importlib.machinery.SourceLoader doesn't throw an exception
	if bytecode is not supported by a VM
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [compatibility-sig] making sure
	importlib.machinery.SourceLoader doesn't throw an exception
	if bytecode is not supported by a VM
----------------------------------------
Author: Alex Gayno
Attributes: []Content: 
For PyPy: I'm not an expert in our import, but from looking at the source

1) imp.cache_from_source is unimplemented, it's an AttributeError.

2) sys.dont_write_bytecode is always false, we don't respect that flag (we really
   should IMO, but it's not a high priority for me, or anyone else apparently)

Alex


