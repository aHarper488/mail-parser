
============================================================================
Subject: [Python-Dev] Parser/intrcheck.c
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Parser/intrcheck.c
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

I may be missing something, but I'm wondering whether
Parser/intrcheck.c is still used anywhere.

It's only mentioned in some comments:

$ grep -r intrcheck.c *
Modules/signalmodule.c:1197:/* Replacements for intrcheck.c functionality
PC/os2vacpp/makefile.omk:217:  # intrcheck.c     -- Not Referenced by Anyone (?)
Python/sigcheck.c:3:   interrupt occurs.  It can't be in the intrcheck.c file since that

And if I remove it and "make clean", I can still rebuild successfully.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Parser/intrcheck.c
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
I think it's safe to remove it. The last reference to it I found was
in the 2.0 release, where there is a Parser/Makefile (generated from
Parser/Makefile.in) which contains the following gem:

# This target is used by the master Makefile to add the objects to the library
add2lib:        $(OBJS)
                $(AR) cr $(LIBRARY) $(AROBJS)
                if test ! -f ../Modules/hassignal; \
                then echo adding intrcheck.o; $(AR) r $(LIBRARY) intrcheck.o; \
                else echo leaving intrcheck.o out; fi
                touch add2lib

This Makefile.in was deleted in http://svn.python.org/view?view=revision

So I think you're fine killing that file.

--Guido

On Wed, Jun 15, 2011 at 3:21 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Parser/intrcheck.c
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Jun 20, 2011 at 4:40 PM, Guido van Rossum <guido at python.org> wrote:

http://svn.python.org/view?view=revision&revision=19308




-- 
--Guido van Rossum (python.org/~guido)

