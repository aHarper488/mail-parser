
============================================================================
Subject: [Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
Post Count: 11
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Meador Ing
Attributes: []Content: 
Hi All,

I was going through some of the open issues related to 'tokenize' and ran
across 'issue2180'.  The reproduction case for this issue is along the lines
of:

 >>> tokenize.tokenize(io.StringIO("if 1:\n \\\n #hey\n print 1").readline)

but, with 'py3k' I get:

    >>> tokenize.tokenize(io.StringIO("if 1:\n  \\\n  #hey\n  print
1").readline)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "/Users/minge/Code/python/py3k/Lib/tokenize.py", line 360, in
tokenize
        encoding, consumed = detect_encoding(readline)
      File "/Users/minge/Code/python/py3k/Lib/tokenize.py", line 316, in
detect_encoding
        if first.startswith(BOM_UTF8):
    TypeError: Can't convert 'bytes' object to str implicitly

which, as seen in the trace, is because the 'detect_encoding' function in
'Lib/tokenize.py' searches for 'BOM_UTF8' (a 'bytes' object) in the string
to tokenize 'first' (a 'str' object).  It seems to me that strings should
still be able to be tokenized, but maybe I am missing something.

Is the implementation of 'detect_encoding' correct in how it attempts to
determine an encoding or should I open an issue for this?

---
Meador
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100927/c154b63b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/9/27 Meador Inge <meadori at gmail.com>:

Tokenize only works on bytes. You can open a feature request if you desire.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Steve Holde
Attributes: []Content: 
On 9/27/2010 11:27 PM, Benjamin Peterson wrote:
Working only on bytes does seem rather perverse.

regards
 Steve
-- 
Steve Holden           +1 571 484 6266   +1 800 494 3119
DjangoCon US September 7-9, 2010    http://djangocon.us/
See Python Video!       http://python.mirocommunity.org/
Holden Web LLC                 http://www.holdenweb.com/




----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 27 Sep 2010 23:45:45 -0400
Steve Holden <steve at holdenweb.com> wrote:

I agree, the morality of bytes objects could have been better :)






----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
  On 28/09/2010 12:19, Antoine Pitrou wrote:
The reason for working with bytes is that source data can only be 
correctly decoded to text once the encoding is known. The encoding is 
determined by reading the encoding cookie.

I certainly wouldn't be opposed to an API that accepts a string as well 
though.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 28 September 2010 12:29, Michael Foord <fuzzyman at voidspace.org.uk> wrote:

Ah, and to explain the design decision when tokenize was ported to py3k -
the Python 2 APIs take the readline method of a file object (not a string).

http://docs.python.org/library/tokenize.html

For this to work correctly in Python 3 it *has* to be a file object open in
binary read mode in order to decode the source code correctly.

A new API that takes a string would certainly be nice. The Python 2 API for
tokenize is 'interesting'...

All the best,

Michael Foord





-- 
http://www.voidspace.org.uk
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100928/93bf7ca9/attachment.html>



----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 28, 2010 at 9:29 PM, Michael Foord
<fuzzyman at voidspace.org.uk> wrote:

A very quick scan of _tokenize suggests it is designed to support
detect_encoding returning None to indicate the line iterator will
return already decoded lines. This is confirmed by the fact the
standard library uses it that way (via generate_tokens).

An API that accepts a string, wraps a StringIO around it, then calls
_tokenise with an encoding of None would appear to be the answer here.
A feature request on the tracker is the best way to make that happen.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Meador Ing
Attributes: []Content: 
On Tue, Sep 28, 2010 at 7:09 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:


Done - http://bugs.python.org/issue9969.  Thanks for the feedback everyone.

-- Meador
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100928/5eab25ed/attachment.html>



----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 28.09.2010 05:45, schrieb Steve Holden:

Yeah, source code really should stop being stored on disks, or else
disks should stop being byte-oriented.

Let's go the Smalltalk way - they store all source code in the image,
no need to deal with perversities like files anymore.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: =?windows-1252?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Notice that this can't really work for Python 2 source code (but of
course, it doesn't need to).

In Python 2, if you have a string literal in the source code, you need
to know the source encoding in order to get the bytes *back*. Now,
if you parse a Unicode string as source code, and it contains byte
string literals, you wouldn't know what encoding to apply.

Fortunately, Python 3 byte literals ban non-ASCII literal characters,
so assuming an ASCII-compatible encoding for the original source is
fairly safe.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] issue2180 and using 'tokenize' with Python 3 'str's
----------------------------------------
Author: Michael Foor
Attributes: []Content: 

On 29 Sep 2010, at 00:22, "Martin v. L?wis" <martin at v.loewis.de> wrote:


The new API couldn't be ported to Python 2 ?anyway?. As Nick pointed out, the underlying tokenization happens on decoded strings - so starting with source as Unicode will be fine. 

Michael 





