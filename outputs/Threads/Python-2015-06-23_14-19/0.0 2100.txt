
============================================================================
Subject: [Python-Dev] another message to release
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] another message to release
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
I believe the tracker sent an error message to python-dev as a
result of a failed hook execution.  If someone with the
power would release that message so we can see what the error
was, I'd appreciate it :)

--David

