
============================================================================
Subject: [Python-Dev] Doubly linked lists in Python core?
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Doubly linked lists in Python core?
----------------------------------------
Author: Skip Montanar
Attributes: []Content: 
I encountered this disconcerting message yesterday on a Linux system
running Python 2.7.2:

*** glibc detected *** /opt/local/bin/python: corrupted double-linked
list: 0x0000000003b01c90 ***

Of course, no core file or other information about where the problem
occurred was left behind, just the raw pointer value.  Running under
valgrind didn't report anything obvious, at least nothing different
than a run with slightly different command line parameters which
didn't elicit the message.

I suspect the problem actually lies in one of our C++ libraries here
at work, but before I attempt any debugging gymnastics (ow! my back
hurts) I thought I would check to see if Python makes much use of
double linked lists in a non-debug build.  I don't recall a lot from
days of yore, perhaps some in the dict implementation?

Thanks,

Skip



----------------------------------------
Subject:
[Python-Dev] Doubly linked lists in Python core?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2013/6/11 Skip Montanaro <skip at pobox.com>:

I suspect that's a corrupt linked list interal to glibc.


--
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Doubly linked lists in Python core?
----------------------------------------
Author: David Malcol
Attributes: []Content: 
On Tue, 2013-06-11 at 12:14 -0700, Benjamin Peterson wrote:

Yes: almost certainly the one inside glibc's implementation of malloc.

Somewhere in the process you have a double-free, or a buffer overrun
that's splatting the links that live in memory between the allocated
bufffers.

You may want to try running the process under valgrind.

Hope this is helpful
Dave




----------------------------------------
Subject:
[Python-Dev] Doubly linked lists in Python core?
----------------------------------------
Author: Skip Montanar
Attributes: []Content: 

Thanks.  I'm trying that but have been so far unable to get valgrind
to report any problems within Python or our libraries before that
message, just a couple things at startup which seem to occur before
Python gets going:

==21374== Invalid read of size 1
==21374==    at 0x4C2DEA9: putenv (in
/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==21374==    by 0x400E71: append_env_vars (binary-wrapper-main.c:175)
==21374==    by 0x4008CA: set_binary_wrapper_env (c-python-wrapper.c:58)
==21374==    by 0x40168D: main (binary-wrapper-main.c:382)
==21374==  Address 0x51d6a84 is 0 bytes after a block of size 52 alloc'd
==21374==    at 0x4C2ACCE: realloc (in
/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==21374==    by 0x400E61: append_env_vars (binary-wrapper-main.c:173)
==21374==    by 0x4008CA: set_binary_wrapper_env (c-python-wrapper.c:58)
==21374==    by 0x40168D: main (binary-wrapper-main.c:382)
==21374==
==21374== Syscall param execve(envp[i]) points to unaddressable byte(s)
==21374==    at 0x4EE7067: execve (in /lib64/libc-2.15.so)
==21374==    by 0x4016AA: main (binary-wrapper-main.c:385)
==21374==  Address 0x51d6a84 is 0 bytes after a block of size 52 alloc'd
==21374==    at 0x4C2ACCE: realloc (in
/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==21374==    by 0x400E61: append_env_vars (binary-wrapper-main.c:173)
==21374==    by 0x4008CA: set_binary_wrapper_env (c-python-wrapper.c:58)
==21374==    by 0x40168D: main (binary-wrapper-main.c:382)

Skip

