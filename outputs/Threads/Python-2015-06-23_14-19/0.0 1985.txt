
============================================================================
Subject: [Python-Dev] pdb and generators
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] pdb and generators
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
When debugging generators (especially when using them as coroutines) I
often want 'next' to step over a yield statement/expression, instead
of the current behavior, which is to step out of the frame, since pdb
doesn't seem to see the difference between yield and return. Or I
could live with a separate command to step over the yield. Has anyone
looked into this yet? I found a question about this on
StackOverflow.com but the answers were particularly unhelpful, so I'm
guessing I'm not the first to want this but it hasn't been solved yet.
It might be a good project for someone looking into hacking pdb.

-- 
--Guido van Rossum (python.org/~guido)

