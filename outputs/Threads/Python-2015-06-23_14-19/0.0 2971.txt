
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.2): Issue #14123:
 Explicitly mention that old style % string formatting has caveats
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Issue #14123:
 Explicitly mention that old style % string formatting has caveats
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

Please consider just deleting the last sentence. Documentation is meant for
users (often new users) and not core devs. As such, I just don't see what
it adds. If the aim to to document this intent somewhere, a PEP would be a
better place than the formal documentation.

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120226/e3ff20de/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.2): Issue #14123:
 Explicitly mention that old style % string formatting has caveats
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 2/26/2012 1:50 PM, martin at v.loewis.de wrote:

I agree that the 'recommendation' is subjective, even though I strongly 
agree with it *for new Python programmers who are not already familiar 
with printf style formatting*. However, that sort of nuanced 
recommendation goes better in a HowTo. Statements about non-deprecation 
are also out of place as that is the default. So I agree with both of 
you. Let us drop both of the last two sentences. Then we can all be happy.

There is a difference between 'There are no current plans to ...' and 
'We will never ...'. However, '...' should not be discussed or even 
proposed or even mentioned until there is a bug-free automatic 
converter. I think the recent rehashing was mostly a needless irritation 
except as it prompted a doc update.

---
Terry Jan Reedy

