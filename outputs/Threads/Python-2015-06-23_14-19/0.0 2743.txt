
============================================================================
Subject: [Python-Dev] Mountain Lion drops sign of zero,
 breaks test_cmath...
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Mountain Lion drops sign of zero,
 breaks test_cmath...
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
R. David Murray <rdmurray at bitdance.com> wrote:

I think this issue covers the problem:

http://bugs.python.org/issue15477


Stefan Krah





----------------------------------------
Subject:
[Python-Dev] Mountain Lion drops sign of zero,
 breaks test_cmath...
----------------------------------------
Author: Trent Nelso
Attributes: []Content: 
On Fri, Aug 17, 2012 at 06:50:09AM -0700, Stefan Krah wrote:

    Ah!  I'll update that with my notes.  (FWIW, I've changed my mind; I
    think the correct action is to remove the erroneous entries from the
    test file, rather than patch rAssertAlmostEqual.)

        Trent.

