
============================================================================
Subject: [Python-Dev] Early results from Argument Clinic automation
	discussion
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Early results from Argument Clinic automation
	discussion
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 

Mark Shannon, Dmitry Jemerov (from PyCharm) and I sat down to talk about 
rearchitecting the Argument Clinic prototype to make it easily to 
interact with.  We came up with the following.

The DSL will now produce an intermediate representation.  The output 
will consume this intermediate representation.

The two defined outputs from the IR so far:

 1. The inplace generated C code (what it has right now), and
 2. Argument Clinic DSL code itself.


The intermediate representation will be subclasses of inspect.Signature 
and inspect.Parameter that add the extra bits of information needed by 
Argument Clinic, as follows:

class Function(inspect.Signature):
         name = 'function name'
         module = 'module name if any
         class = 'class name if any, can't actually call this class'
         docstring = 'function docstring'
         c_id = 'stub id to use for generated c functions'
         return_converter = SeeBelow()

class Parameter(inspect.Parameter):
          docstring = 'per-parameter docstring'
          group = <group membership integer or None>
          converter = ConverterFunction()

Parameter.group is an integer, specifying which "option group" the 
parameter is in.  This must be an integer for positional-only arguments, 
and None for other argument types.  0 indicates "required 
positional-only parameter".  Left-optional groups get negative numbers, 
decreasing as they get further from the required parameters; 
right-optional get positive numbers, increasing as they get further from 
the required parameters.  (Groups cannot nest, so a parameter cannot be 
in more than one group.)

Function.return_converter was suggested by Mark.  This is the inverse of 
the per-parameter converter function: it defines the return type of the 
impl function, and the conversion process to turn it into the PyObject * 
that gets returned to Python.  And, like the converter functions, it 
will define the actual return annotation of the function (if any).

Mark wants to write something that parses C code implementing builtins 
and produces the IR; he can then use that to write Argument Clinic DSL.  
This will make the conversion process go much more quickly.


//arry/
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130319/60ab46b4/attachment.html>

