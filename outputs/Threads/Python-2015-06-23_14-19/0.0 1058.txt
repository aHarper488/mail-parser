
============================================================================
Subject: [Python-Dev] stable builders
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] stable builders
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 05 Oct 2010 17:06:40 +0200
"Martin v. L?wis" <martin at v.loewis.de> wrote:

By the way, is the distinction between "stable" and "unstable" builders
still relevant?
If I look at http://www.python.org/dev/buildbot/3.x.stable/, two of our
four stable builders are offline (and at least one of them - the ia64
one - has been for a long time). Granted, this makes them "stable" in a
sense ;)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] stable builders
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 05.10.10 19:07, schrieb Antoine Pitrou:

It's still formally the requirement for making releases.


I guess somebody would need to do monitoring on them, and ping operators
if the buildbot is down for an extended period of time. Feel free to 
ping any operator whenever you notice that a slave is down (they do get
an automated email, but people can get resistant to automated emails).

Also, if you would want to propose that a different set than the current 
ones should be considered stable, please let me know. I believe
"stable" was meant in a different way, though - it would reliably pass 
all tests, and a test failure should be considered a bug, rather than
some random failure on the slave.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] stable builders
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Oct 6, 2010 at 6:55 AM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

To simplify the task of contacting buildbot operators, would it be
worth having a "python-buildbot-owners" mailing list?

Regards,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] stable builders
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Depends on the traffic. It might spam those owners who are never the
target of any of these messages, because they are "well-behaving".
Do you really find it difficult to determine the email address of any
specific operator? Who?

If you really need to broadcast a message to all owners, let me know
and I can forward it to them (or you can collect the addresses
yourself).

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] stable builders
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 05 Oct 2010 22:55:41 +0200
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Well, I suppose manual pinging can become pretty much like automated
emails if it becomes frequent.
(in any case, I think I've pinged Matthias at least twice on
IRC, but perhaps he isn't really present on that medium, although he
doesn't appear away)


I see. Well, apart from the current ones, "alpha Debian" and "i386
Ubuntu" have been quite stable.

Antoine.

