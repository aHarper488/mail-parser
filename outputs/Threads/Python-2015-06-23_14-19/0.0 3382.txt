
============================================================================
Subject: [Python-Dev] [RELEASED] Distutils2 1.0a4
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [RELEASED] Distutils2 1.0a4
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
What would be a release email without errors?  :)  The wiki link I gave
doesn?t work, it should be
http://wiki.python.org/moin/Distutils2/Contributing



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Distutils2 1.0a4
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hello,

On behalf of the distutils2 contributors, I am thrilled to announce the
release of Distutils2 1.0a4.

Distutils2 is the packaging library that supersedes Distutils. It
supports distributing, uploading, downloading, installing and removing
projects, and is also a support library for other packaging tools like
pip and buildout.  It will be provided as part of Python 3.3; this
release is a backport compatible with Python 2.5 to 2.7.

Distutils2 1.0a4 contains a number of known bugs, limitations and
missing features, but we have released it to let users and developers
download and try it.  This means you!  If you want to report new issues,
request features or contribute, please read DEVNOTES.txt in the source
distribution or http://wiki.python.org/Distutils2/Contributing

More alpha releases will be cut when important bugs get fixed during the
next few months, like Windows or PyPy compatibility.  The first beta is
planned for June, and 1.0 final for August (to follow Python 3.3.0).
Until beta, the API is subject to drastic changes and code can get removed.

Basic documentation is at http://docs.python.org/dev/packaging ; it will
get updated, expanded and improved in the coming months.

Enjoy!

Repository: http://hg.python.org/distutils2
Bug tracker: http://bugs.python.org/ (component "Distutils2")
Mailing list: http://mail.python.org/mailman/listinfo/distutils-sig/



----------------------------------------
Subject:
[Python-Dev] [RELEASED] Distutils2 1.0a4
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Thanks a lot for your hard work and dedication on packaging  !

On 3/13/12 9:37 AM, ?ric Araujo wrote:


