
============================================================================
Subject: [Python-Dev] CRLF line endings
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Hi,

A commit hook prevented pushing changes to the cdecimal repository:

pushing to ssh://hg at hg.python.org/features/cdecimal
searching for changes
8 changesets found
remote: adding changesets
remote: adding manifests
remote: adding file changes
remote: added 8 changesets with 117 changes to 117 files
remote: error: pretxnchangegroup.eol hook failed: Modules/cdecimal/tests/dnloop.patch in 80914c366edf should not have CRLF line endings
remote: transaction abort!
remote: rollback completed
remote: abort: Modules/cdecimal/tests/dnloop.patch in 80914c366edf should not have CRLF line endings


However, dnloop.patch is correct and must have CRLF line endings. How
can I disable the commit hook?


Stefan Krah




----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 25, 2011 at 7:36 PM, Stefan Krah <stefan at bytereef.org> wrote:

Don't disable the commit hook, update .hgeol to flag that file as
requiring CRLF line endings.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Nick Coghlan <ncoghlan at gmail.com> wrote:

Thanks, that works well.


Stefan Krah





----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 25 Mar 2011 21:12:19 +1000, Nick Coghlan <ncoghlan at gmail.com> wrote:

Note however that we discovered that the server side hook looks at the
.hgeol file in the *server's checkout*.  I don't know if Georg or Antoine
have fixed this or not (it's a standard Mercurial plugin, so probably
not).

So, I think that once you check in the .hgeol, you have to get a server
admin to update the repo checkout on the server.  Maybe we should
set up a cron job.

But, yeah, updating .hgeol is the correct solution, otherwise you'll
just run in to the trouble when you try to merge the branch into default.

--
R. David Murray           http://www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 25, 2011 at 11:12 PM, R. David Murray <rdmurray at bitdance.com> wrote:

Not that I noticed - to get my sandbox updated after you moved all the
email test files, I just had to one do push to get the .hgeol file
updated, then the second push with all the subsequent changes went
through without any trouble.

The one thing you can't do is push an updated .hgeol *and* files that
depend on the updated ruleset in one operation - the server will
process the whole transaction using the *old* version of the .hgeol
file and it will fail as a result. But "hg log .hgeol" will tell you
which version last changed it, then a "hg push -r <rev>" will get the
server's version updated.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 25 Mar 2011 09:12:53 -0400
"R. David Murray" <rdmurray at bitdance.com> wrote:

We have "fixed" it by nulling the server's checkout (updating to it to
changeset 0000000000, that is). Unless someone logs in and manually
does "hg up" in that directory, it shouldn't happen again.

(but, yes, it's probably a bug or misfeature in hgeol)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
To facilitate review of the PEP 405 reference implementation, I want to update
my sandbox repository on hg.python.org with the relevant changes, so I can
create a patch for Rietveld.

I've added some files with CRLF line endings:

Lib/venv/scripts/nt/Activate.ps1
Lib/venv/scripts/nt/Dectivate.ps1
Lib/venv/scripts/nt/activate.bat

Although these are text files, the CRLF line endings are needed because
otherwise, the files won't be presented correctly on Windows, e.g. in Notepad.

I'd like to update the .hgeol file to add these entries, as otherwise the commit
hook rejects them. Can anyone please let me know if they object? Otherwise I'll
go ahead and add them to .hgeol in the next hour or so.

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Thu, May 3, 2012 at 10:50 PM, Vinay Sajip <vinay_sajip at yahoo.co.uk> wrote:

Not all Windows editors choke on \n line endings; when I'm on Windows
and run into one, I open it in Wordpad (or, if I have one, a dedicated
programming editor like SciTE or the Open Watcom editor). AFAIK only
Notepad (of standard Windows utilities) has trouble.

Not sure if that makes a difference or not.

Chris Angelico



----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Chris Angelico <rosuav <at> gmail.com> writes:


It's only really an issue for new / inexperienced users, I agree. Since these
files are installed only on Windows systems, there's no reason for them not to
have the native line endings.

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Chris Angelic
Attributes: []Content: 
On Fri, May 4, 2012 at 1:28 AM, Vinay Sajip <vinay_sajip at yahoo.co.uk> wrote:

Then sure, doesn't make a lot of difference that it's only Notepad.

Somebody needs to rewrite that ancient editor and give Windows a
better default...

ChrisA



----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: martin at v.loewis.d
Attributes: []Content: 

Zitat von Chris Angelico <rosuav at gmail.com>:


I think that .bat files strictly *have* to have CRLF line endings. Not sure
about PowerShell, though.

In any case, having CRLF for these files sounds good to me.

Regards,
Martin





----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 5/3/2012 2:00 PM, martin at v.loewis.de wrote:

Nope.  Both .bat and .cmd work fine with LF only in Win7 (and IIRC, in 
XP as well, but I just tested Win7)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120503/6c0a1823/attachment.html>



----------------------------------------
Subject:
[Python-Dev] CRLF line endings
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, May 4, 2012 at 7:00 AM,  <martin at v.loewis.de> wrote:

Right. While Windows has been getting much better at coping with LF
only line endings over the years, being able to explicitly flag files
for CRLF endings is the entire reason we held out for the EOL
extension before making the switch to Mercurial.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

