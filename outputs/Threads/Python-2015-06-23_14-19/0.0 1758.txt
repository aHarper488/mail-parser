
============================================================================
Subject: [Python-Dev] r88147 - in python/branches/py3k: Misc/NEWS
 Modules/_pickle.c Tools/scripts/find_recursionlimit.py
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] r88147 - in python/branches/py3k: Misc/NEWS
 Modules/_pickle.c Tools/scripts/find_recursionlimit.py
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 23 Jan 2011 18:12:26 +0100 (CET)
antoine.pitrou <python-checkins at python.org> wrote:

I forgot to mention that it was ok'ed by Georg, so there it is.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] r88147 - in python/branches/py3k: Misc/NEWS
 Modules/_pickle.c Tools/scripts/find_recursionlimit.py
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 1/23/2011 1:58 PM, Antoine Pitrou wrote:


12 hours after the report!

I am still curious why a previous exception changed pickle behavior, and 
only in 3.2, but I would rather you fix another bug than speeding much 
time to get me up to speed on the intricacies of _pickle ;-).

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] r88147 - in python/branches/py3k: Misc/NEWS
 Modules/_pickle.c Tools/scripts/find_recursionlimit.py
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 23 Jan 2011 18:45:50 -0500
Terry Reedy <tjreedy at udel.edu> wrote:


It was not about a previous exception. The issue is that pickle
detected the recursion overflow but returned a successful status after
having set the exception. This is the kind of mistake that produces
strange "delayed" exceptions.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] r88147 - in python/branches/py3k: Misc/NEWS
 Modules/_pickle.c Tools/scripts/find_recursionlimit.py
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

IIUC, the code change made pickle actually aware of the exception,
rather than just setting it in the thread state, but then happily
declaring that pickle succeeded (with what would turn out to be
incorrect data).

As for why an explicit exception breaks the reporting, and omitting
it makes it report the exception correctly:

the report that it gave wasn't actually correct. I got

raceback (most recent call last):
  File "a.py", line 4, in <module>
    for i in range(100):
RuntimeError: maximum recursion depth exceeded while pickling an object

So the exception is reported on the range call, or the for loop.
After the change, we get

Traceback (most recent call last):
  File "a.py", line 7, in <module>
    _pickle.Pickler(io.BytesIO(), protocol=-1).dump(l)
RuntimeError: maximum recursion depth exceeded while pickling an object

So it appears that the interpreter would actually pick up the exception
set by pickle, and attribute it to the for loop. When you add an
explicit raise, this raise will clear the stack overflow exception,
and set the new exception. So the error manages to pass silently,
without being explicitly silenced.

I wonder whether we could sprinkle more exception-set? checks in
the interpreter loop, at least in debug mode.

It's a design flaw in CPython that there are two ways to report
an exception: either through the thread state, or through the return
value. I don't think this flaw can be fully fixed. However, I
wonder whether static analysis of the C code could produce better
detection of this kind of bug.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] r88147 - in python/branches/py3k: Misc/NEWS
 Modules/_pickle.c Tools/scripts/find_recursionlimit.py
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 24 Jan 2011 01:28:26 +0100
"Martin v. L?wis" <martin at v.loewis.de> wrote:

Yes, this would be nice. Nicer if it can be centralized, of course.
That said, it probably wouldn't have helped here, since the code which
exhibited the bug (the find_recursion_limit.py script) is basically
never run automatically, and very rarely by a human.

Regards

Antoine.



