
============================================================================
Subject: [Python-Dev] cpython: Issue #13248: removed deprecated and
	undocumented difflib.isbjunk, isbpopular.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #13248: removed deprecated and
	undocumented difflib.isbjunk, isbpopular.
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 21 Mar 2013 01:19:42 -0400, Terry Reedy <tjreedy at udel.edu> wrote:

Looks fine to me.  A link on SequenceMatcher probably isn't necessary,
but might be handy.  If you want to add it it would be
:class:`~difflib.SequenceMatcher`.

--David

