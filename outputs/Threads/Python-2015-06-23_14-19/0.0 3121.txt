
============================================================================
Subject: [Python-Dev]
 =?utf-8?q?How_to_recruit_open-source_contributors_?=
 =?utf-8?q?=E2=80=93_The_Story_of_Data?=
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]
 =?utf-8?q?How_to_recruit_open-source_contributors_?=
 =?utf-8?q?=E2=80=93_The_Story_of_Data?=
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 11 Jul 2012 19:54:33 -0500
Steve Holden <steve at holdenweb.com> wrote:

This is quite an interesting article, but I don't think we are that
high in pyramid of needs; we would first need someone dedicated enough
to keep track of all GSoC projects.
(and we would also need mentoring of potential mentors)

Regards

Antoine.



-- 
Software development and contracting: http://pro.pitrou.net



