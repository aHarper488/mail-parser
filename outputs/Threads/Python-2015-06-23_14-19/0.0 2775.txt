
============================================================================
Subject: [Python-Dev] Py_buffer.obj documentation
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Py_buffer.obj documentation
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
I am trying to reconcile this section in 3.3 documentation:

"""
void *obj

A new reference to the exporting object. The reference is owned by the
consumer and automatically decremented and set to NULL by
PyBuffer_Release(). The field is the equivalent of the return value of
any standard C-API function.

As a special case, for temporary buffers that are wrapped by
PyMemoryView_FromBuffer() or PyBuffer_FillInfo() this field is NULL.
In general, exporting objects MUST NOT use this scheme.
""" -- http://docs.python.org/dev/c-api/buffer.html#Py_buffer.obj

with the following comment in the code (Objects/memoryobject.c:762):

    /* info->obj is either NULL or a borrowed reference. This
reference
       should not be decremented in PyBuffer_Release(). */

I have not studied the code yet, but given the history of bugs in this
area the code may not have the most authoritative answer.  In any
case, either the comment or the ReST section should be corrected.



----------------------------------------
Subject:
[Python-Dev] Py_buffer.obj documentation
----------------------------------------
Author: Jeff Alle
Attributes: []Content: 
On 29/08/2012 22:28, Alexander Belopolsky wrote:

I've studied this code in the interests of reproducing something similar 
for Jython.

The comment is in the context of PyMemoryView_FromBuffer(Py_buffer 
*info), at a point where the whole info struct is being copied to 
mbuf->master, then the code sets mbuf->master.obj = NULL. I think the 
comment means that the caller,
which is in the role of consumer to the original exporter, owns the info 
struct and therefore the reference info.obj.
That caller will eventually call PyBuffer_Release(info), which will 
result in a DECREF(obj) matching the INCREF(obj) that happened during 
bf_getbuffer(info). In this sense obj is a borrowed reference as far as 
the memoryview is concerned. mbuf->master must not also keep a 
reference, or it risks making a second call to DECREF(obj).

Jeff Allen



----------------------------------------
Subject:
[Python-Dev] Py_buffer.obj documentation
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Alexander Belopolsky <alexander.belopolsky at gmail.com> wrote:

The semantics of PyMemoryView_FromBuffer() are problematic. This function
is the odd one in memoryobject.c since it's the only function that breaks
the link in consumer -> exporter chains. This has several consequences:

   1) One can't rely on the fact that 'info' has PyBUF_FULL information.
      This is a major inconvenience and the reason for *a lot* of code
      in memoryobject.c that reconstructs PyBUF_FULL information.

   2) One has to make a decision whether PyMemoryView_FromBuffer() steals
      the reference to view.obj or treats it as a borrowed reference.



My view on this is that it's safer to treat it as a borrowed reference.


Additionally, I can't see a scenario where PyMemoryView_FromBuffer(info)
could be used for creating a non-temporary memoryview with automatic
decref of info.obj: If 'info' is allocated on the stack, then the
memoryview shouldn't be returned from a function. If 'info' is allocated
on the heap, then who frees 'info' when the memoryview is deallocated?


Permanent memoryviews can now be safely created with PyMemoryView_FromMemory().
PyMemoryView_FromBuffer() isn't really that useful any more.


It's hard to document all this in a few lines. Perhaps you can open an
issue for this?



Stefan Krah




