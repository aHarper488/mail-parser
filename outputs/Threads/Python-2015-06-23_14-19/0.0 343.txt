
============================================================================
Subject: [Python-Dev]
 =?iso-8859-1?q?Issue_=238863_adds_a_new_PYTHONNOFAUL?=
 =?iso-8859-1?q?THANDLER=09environment_variable?=
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]
 =?iso-8859-1?q?Issue_=238863_adds_a_new_PYTHONNOFAUL?=
 =?iso-8859-1?q?THANDLER=09environment_variable?=
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le lundi 20 d?cembre 2010 12:08:35, Stefan Krah a ?crit :

The backtrace is valid. Don't you think that this backtrace is more useful 
than just "Segmentation fault"?


I didn't tried with patch version 9, but with the patch version 10 I get:
-----------------
$ ./python Lib/test/crashers/nasty_eq_vs_dict.py 
Fatal Python error: Segmentation fault

Traceback (most recent call first):
  File "Lib/test/crashers/nasty_eq_vs_dict.py", line 16 in __hash__
  File "Lib/test/crashers/nasty_eq_vs_dict.py", line 40 in __fill_dict
  File "Lib/test/crashers/nasty_eq_vs_dict.py", line 30 in __eq__
  File "Lib/test/crashers/nasty_eq_vs_dict.py", line 47 in <module>
Segmentation fault
Erreur de segmentation
$ echo $?
139
-----------------


Patch version 9 doesn't call the previous handler. Please retry with patch 
version 10 (which call the previous handler).

With the patch version 10 I get:
-----------------
$ ./python Lib/test/crashers/compiler_recursion.py 
Fatal Python error: Segmentation fault

Traceback (most recent call first):
  File "Lib/test/crashers/compiler_recursion.py", line 5 in <module>
Segmentation fault
Erreur de segmentation
$ echo $?
139
-----------------

Victor



----------------------------------------
Subject:
[Python-Dev]
 =?iso-8859-1?q?Issue_=238863_adds_a_new_PYTHONNOFAUL?=
 =?iso-8859-1?q?THANDLER=09environment_variable?=
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le lundi 20 d?cembre 2010 12:08:35, Stefan Krah a ?crit :

Oh, I understood. I always test with Python compiled using --with-pydebug. 
With pydebug, a segfault occurs. Without pydebug, there is an unlimited loop.

The patch is not related to the lack of segfault :-) (Retry without the patch)

Victor

