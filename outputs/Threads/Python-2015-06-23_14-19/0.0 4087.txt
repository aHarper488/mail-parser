
============================================================================
Subject: [Python-Dev] Why are captured parameters also listed in co_varnames?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Why are captured parameters also listed in co_varnames?
----------------------------------------
Author: Andrea Griffin
Attributes: []Content: 
I'm trying to understand how CPython implements closure variable capture
and there is one minor point I can't understand.

When a local is captured it gets allocated in co_cellvars and is accessed
with (LOAD|STORE)_DEREF, and this is clear.
However when a local is coming from a parameter it gets ALSO allocated in
co_varnames even if the local slot apparently is not accesible because
*_FAST opcodes are not generated.

Is there a technical reason for this? It happens in CPython 2, 3 and even
in PyPy...

Andrea Griffini
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130702/d86dd488/attachment.html>

