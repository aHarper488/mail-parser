
============================================================================
Subject: [Python-Dev] cpython: Remove display options (--name,
 etc.) from the Distribution class.
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Remove display options (--name,
 etc.) from the Distribution class.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 30 Aug 2011 16:22:14 +0200
eric.araujo <python-checkins at python.org> wrote:


I don't want to sound nitpicky, but it's the first time I see
"home-page" hyphenized. How about "homepage"?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython: Remove display options (--name,
 etc.) from the Distribution class.
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi,

Le 30/08/2011 17:20, Antoine Pitrou a ?crit :

This value is defined in the accepted Metadata PEPs, which use home-page.

Regards

