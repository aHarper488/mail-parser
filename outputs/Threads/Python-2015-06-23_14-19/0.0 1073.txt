
============================================================================
Subject: [Python-Dev] Inconsistencies if locale and	filesystem?encodings
	are different
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Inconsistencies if locale and	filesystem?encodings
	are different
----------------------------------------
Author: Oleg Broytma
Attributes: []Content: 
On Thu, Oct 07, 2010 at 09:12:13PM +0200, Victor Stinner wrote:

   Most of them don't because - you are right - most programs assume fs
encoding to be the same as stdio locale. But some programs are more clever;
for example, one can define G_FILENAME_ENCODING env var to guide GTK2/GLib
programs; it can be a fixed encoding or a special value "@locale". On the
other side there are programs that ignore locale completely and read/write
filenames using their own fixed encoding; for example, Transmission
bittorrent client read/write files in the encoding defined in the .torrent
metafile.

Oleg.
-- 
     Oleg Broytman            http://phd.pp.ru/            phd at phd.pp.ru
           Programmers don't die, they just GOSUB without RETURN.

