
============================================================================
Subject: [Python-Dev] Python 3 metaclasses, doctest, and unittest
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 3 metaclasses, doctest, and unittest
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
I just wanted to take a minute and say THANK YOU to everyone involved in getting Python 3 to where it is today.

It is so much easier to use, especially metaclasses (nearly pulled my hair out trying to get Enum working on Py2!).

Also a big thank you to the doctest folks as it was invaluable in getting my Py2 examples correct, and, of course, the 
unittest folks as I would have been lost without those.

Thank you thank you thank you.

--
~Ethan~

P.S.  I'll get a backported package on PyPI after the code commit for 3.4.

