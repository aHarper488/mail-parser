
============================================================================
Subject: [Python-Dev] Differences among Emacsen
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Differences among Emacsen
----------------------------------------
Author: Ralf Schmit
Attributes: []Content: 
Barry Warsaw <barry at python.org> writes:


https://github.com/fgallina/python.el is the fourth one..



----------------------------------------
Subject:
[Python-Dev] Differences among Emacsen
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 30, 2011, at 09:43 AM, Ralf Schmitt wrote:


Wonderful.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110330/a0184325/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] Differences among Emacsen
----------------------------------------
Author: Glyph Lefkowit
Attributes: []Content: 

On Mar 30, 2011, at 2:54 PM, Barry Warsaw wrote:


I have a plea for posterity: since I'm sure that a hundred people will see this post and decide that the best solution to this proliferation of python plugins for emacs is that there should be a new one that is even better than all these other ones (and also totally incompatible, of course)...

I won't try to stop you all from doing that, but please at least don't call it "python.el".  This is like if ActiveState, Wing, PyCharm and PyDev for Eclipse had all decided to call their respective projects "IDLE" because that's what you call a Python IDE :).  It would be nice to be able to talk about Python / Emacs code without having to do an Abbott and Costello routine.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110330/3c05d151/attachment.html>

