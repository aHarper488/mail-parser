
============================================================================
Subject: [Python-Dev] short fetch for NEXTARG macro (was: one byte byte code
	arguments)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] short fetch for NEXTARG macro (was: one byte byte code
	arguments)
----------------------------------------
Author: Jurjen N.E. Bo
Attributes: []Content: 
I just did it: my first python source code hack.
I replaced the NEXTARG and PEEKARG macros in ceval.c using a cast to  
short pointer, and lo and behold, a crude measurement indicates one  
to two percent speed increase.
That isn't much, but it is virtually for free!

Here are the macro's I used:
#define NEXTARG() (next_instr +=2, *(short*)&next_instr[-2])
#define PEEKARG() (*(short*)&next_instr[1])

- Jurjen

