
============================================================================
Subject: [Python-Dev] set/dict comprehensions don't leak in
	Py2.7	-	intentional?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] set/dict comprehensions don't leak in
	Py2.7	-	intentional?
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 05/07/2010 14:12, Tim Golden wrote:

Similarly my understanding was that list comprehensions leaking was an 
unfortunate implementation detail and that to depend on it was therefore 
"a bug". I don't see it as an issue that set and dict comprehensions 
don't leak in 2.7 (and in fact it is a good thing as it makes it harder 
to write code that works in 2.7 but fails in 3.x).

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.



