
============================================================================
Subject: [Python-Dev] Why are there no 'set' and 'frozenset' types in the
	'types' module?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Why are there no 'set' and 'frozenset' types in the
	'types' module?
----------------------------------------
Author: haae
Attributes: []Content: 

Sorry if I am asking the obvious, but why are the aliases of set types not 
included in the 'types' module? I thought for a moment that they are just 
classes, but no, they introduce themselves as built-in types, just like any 
other standard Python type.

 > print type(set([1, 2, 4]))
<type 'set'>

 > print type(frozenset([3, 5]))
<type 'frozenset'>

Is it intentional, or is there some meaning behind this? If not, shouldn't they 
be added to the module?


Regards,
Bartosz Tarnowski



---------------------------------------------------------------
Darmowy program do wype?niania PIT: http://linkint.pl/f2931


