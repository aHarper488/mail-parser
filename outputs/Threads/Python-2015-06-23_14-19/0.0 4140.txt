
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Fix #18530. Remove
 extra stat call from posixpath.ismount
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Fix #18530. Remove
 extra stat call from posixpath.ismount
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Could you please keep the comment "# A symlink can never be a mount point"
? It is useful. (I didn't know that, I'm not a windows developer.)

Victor
Le 22 juil. 2013 20:08, "brian.curtin" <python-checkins at python.org> a
?crit :

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130723/0df23566/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Fix #18530. Remove
 extra stat call from posixpath.ismount
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Mon, Jul 22, 2013 at 6:36 PM, Victor Stinner
<victor.stinner at gmail.com> wrote:

I don't think that's specific to Windows, but I added it back in d6213012d87b.

