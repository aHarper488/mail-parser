
============================================================================
Subject: [Python-Dev] Signs of neglect?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Signs of neglect?
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hello pydev

I?d like to volunteer to maintain a tool but I?m not sure where I can
help. I?m already proposing changes to Brett for
Tools/scripts/patchcheck.py, and I have commented on Tools/i18n bugs,
but these ones are already maintained by their authors (e.g. Barry is
assigned pygettext bugs) and I?m by no means a gettext expert.

If I adopt a tool, it will still require some core dev time to review
and commit, but I?ll do the bug triage, discussion and coding.

I suggest that each adoption starts with a reindent.py run and a PEP 8
compliance check. Let demos and tools become a showcase to be proud of!

I have opened a bug to discuss adding a simple sanity test that would
just make sure tools and demos can run without syntax errors:
http://bugs.python.org/issue9153

There was an open bug about demos, http://bugs.python.org/issue7962,
I?ve widened it into a meta-bug for demos and tools. (I hope it is
okay.) Please comment, adopt or condemn demos and tools in that report.

Hope this helps.

Regards




----------------------------------------
Subject:
[Python-Dev] Signs of neglect?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jul 04, 2010, at 06:58 PM, ?ric Araujo wrote:


It's been a while since I did much pygettext stuff.  I think Martin's
basically taken it over in recent years.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100706/668819e7/attachment.pgp>

