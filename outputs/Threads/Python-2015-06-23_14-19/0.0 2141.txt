
============================================================================
Subject: [Python-Dev] Documentation not being updated
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Documentation not being updated
----------------------------------------
Author: Ross Lagerwal
Attributes: []Content: 
A change that I made (a49bda5ff3d5) to the documentation 3 days ago does
not appear to have propagated to

http://docs.python.org/dev/library/multiprocessing.html

Building the docs locally shows the change. Am I doing something wrong?

Cheers
Ross




----------------------------------------
Subject:
[Python-Dev] Documentation not being updated
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 18.03.2011 07:15, Ross Lagerwall wrote:

Nope, that was me.  Should be fixed now.

Georg


