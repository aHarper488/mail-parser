
============================================================================
Subject: [Python-Dev] Use of coding cookie in 3.x stdlib
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Use of coding cookie in 3.x stdlib
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
I was looking at the inspect module and noticed that it's source
starts with "# -*- coding: iso-8859-1 -*-".   I have checked and there
are no non-ascii characters in the file.   There are several other
modules that still use the cookie:

Lib/ast.py:# -*- coding: utf-8 -*-
Lib/getopt.py:# -*- coding: utf-8 -*-
Lib/inspect.py:# -*- coding: iso-8859-1 -*-
Lib/pydoc.py:# -*- coding: latin-1 -*-
Lib/shlex.py:# -*- coding: iso-8859-1 -*-
Lib/encodings/punycode.py:# -*- coding: utf-8 -*-
Lib/msilib/__init__.py:# -*- coding: utf-8 -*-
Lib/sqlite3/__init__.py:#-*- coding: ISO-8859-1 -*-
Lib/sqlite3/dbapi2.py:#-*- coding: ISO-8859-1 -*-
Lib/test/bad_coding.py:# -*- coding: uft-8 -*-
Lib/test/badsyntax_3131.py:# -*- coding: utf-8 -*-

I understand that coding: utf-8 is strictly redundant in 3.x.  There
are cases such as Lib/shlex.py where using encoding other than utf-8
is justified.  (See
http://svn.python.org/view?view=rev&revision=82560).  What are the
guidelines for other cases?  Should redundant cookies be removed?
Since not all editors respect the  -*- cookie, I think the answer
should be "yes" particularly when the cookie is setting encoding other
than utf-8.



----------------------------------------
Subject:
[Python-Dev] Use of coding cookie in 3.x stdlib
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Sounds like a good idea to try to remove redundant cookies *and* to
remove most occasional use of non-ASCII characters outside comments
(except for unittests specifically trying to test Unicode features).
Personally I would use \xXX escapes instead of spelling out the
characters in shlex.py, for example.

Both with or without the coding cookies, many ways of displaying text
files garble characters outside the ASCII range, so it's better to
stick to ASCII as much as possible.

--Guido

On Mon, Jul 19, 2010 at 1:21 AM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Use of coding cookie in 3.x stdlib
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Mon, Jul 19, 2010 at 2:45 AM, Guido van Rossum <guido at python.org> wrote:

Please see http://bugs.python.org/issue9308 .

I am going to post a patch shortly.

