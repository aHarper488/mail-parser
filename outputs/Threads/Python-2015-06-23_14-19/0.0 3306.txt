
============================================================================
Subject: [Python-Dev] On a new version of pickle [PEP 3154]:
 self-referential frozensets
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] On a new version of pickle [PEP 3154]:
 self-referential frozensets
----------------------------------------
Author: Alexandre Vassalott
Attributes: []Content: 
On Sat, Jun 23, 2012 at 3:19 AM, M Stefan <mstefanro at gmail.com> wrote:


Since frozenset are immutable, could you explain how adding the
UNION_FROZENSET opcode helps in pickling self-referential frozensets? Or
are you only adding this one to follow the current style used for pickling
dicts and lists in protocols 1 and onward?


I don't think that's the only way. You could also emit POP opcode to
discard the frozenset from stack and then emit a GET to fetch it back from
the memo. This is how we currently handle self-referential tuples. Check
out the save_tuple method in pickle.py to see how it is done. Personally, I
would prefer that approach because it already well-tested and proven to
work.

That said, your approach sounds good too. The memory trade-off could lead
to smaller pickles and more efficient decoding (though these
self-referential objects are rare enough that I don't think that any
improvements there would matter much).

While self-referential frozensets are uncommon, a far more problematic

Your example seems to work on Python 3. I am not sure if I follow what you
are trying to say. Can you provide a working example?

$ python3
Python 3.1.2 (r312:79147, Dec  9 2011, 20:47:34)
[GCC 4.4.3] on linux2
Type "help", "copyright", "credits" or "license" for more information.
...
True



I would advise against any such change. The reduce protocol is already
fairly complex. Further I don't think change it this way would give us any
extra flexibility.

The documentation has a good explanation of how __getstate__ works under
hood:
http://docs.python.org/py3k/library/pickle.html#pickling-class-instances

And if you need more, PEP 307 (http://www.python.org/dev/peps/pep-0307/)
provides some of the design rationales of the API.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120627/8210b61b/attachment.html>

