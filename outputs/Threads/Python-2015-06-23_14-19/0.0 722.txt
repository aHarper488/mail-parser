
============================================================================
Subject: [Python-Dev] test_bigmem and test_bigaddrspace broken
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] test_bigmem and test_bigaddrspace broken
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

The title says it all. test_bigmem and test_bigaddrspace are partly 
broken under py3k, and no buildbot ever tests that they run successfully.
As an example, here is a test_bigmem run under a 16GB machine. It gets 
"killed" at some point, probably by the Linux kernel's OOM killer, 
although I instruct the test suite to only use a fraction of the 
available RAM. There are also a couple of failures:


$ ./python -m test.regrtest -v -M 8Gb test_bigmem
== CPython 3.2a0 (py3k, Mar 17 2010, 18:38:10) [GCC 4.3.2]
==   Linux-a.b.c.d-grsec-xxxx-grs-ipv4-64-x86_64-with-debian-lenny-sid
==   /home/antoine/py3k/py3k/build/test_python_10071
test_bigmem
test_capitalize (test.test_bigmem.StrTest) ... Skipping test_capitalize 
because of memory constraint
ok
test_center (test.test_bigmem.StrTest) ... ok
test_compare (test.test_bigmem.StrTest) ... Skipping test_compare because 
of memory constraint
ok
test_concat (test.test_bigmem.StrTest) ... ok
test_contains (test.test_bigmem.StrTest) ... Skipping test_contains 
because of memory constraint
ok
test_count (test.test_bigmem.StrTest) ... Skipping test_count because of 
memory constraint
ok
test_encode (test.test_bigmem.StrTest) ... ERROR
test_encode_ascii (test.test_bigmem.StrTest) ... ok
test_encode_raw_unicode_escape (test.test_bigmem.StrTest) ... ok
test_encode_utf32 (test.test_bigmem.StrTest) ... ok
test_encode_utf7 (test.test_bigmem.StrTest) ... ok
test_endswith (test.test_bigmem.StrTest) ... Skipping test_endswith 
because of memory constraint
ok
test_expandtabs (test.test_bigmem.StrTest) ... Skipping test_expandtabs 
because of memory constraint
ok
test_find (test.test_bigmem.StrTest) ... Skipping test_find because of 
memory constraint
ok
test_format (test.test_bigmem.StrTest) ... Skipping test_format because 
of memory constraint
ok
test_hash (test.test_bigmem.StrTest) ... ok
test_index (test.test_bigmem.StrTest) ... Skipping test_index because of 
memory constraint
ok
test_isalnum (test.test_bigmem.StrTest) ... Skipping test_isalnum because 
of memory constraint
ok
test_isalpha (test.test_bigmem.StrTest) ... Skipping test_isalpha because 
of memory constraint
ok
test_isdigit (test.test_bigmem.StrTest) ... Skipping test_isdigit because 
of memory constraint
ok
test_islower (test.test_bigmem.StrTest) ... Skipping test_islower because 
of memory constraint
ok
test_isspace (test.test_bigmem.StrTest) ... Skipping test_isspace because 
of memory constraint
ok
test_istitle (test.test_bigmem.StrTest) ... Skipping test_istitle because 
of memory constraint
ok
test_isupper (test.test_bigmem.StrTest) ... Skipping test_isupper because 
of memory constraint
ok
test_join (test.test_bigmem.StrTest) ... Skipping test_join because of 
memory constraint
ok
test_ljust (test.test_bigmem.StrTest) ... ok
test_lower (test.test_bigmem.StrTest) ... Skipping test_lower because of 
memory constraint
ok
test_lstrip (test.test_bigmem.StrTest) ... ok
test_repeat (test.test_bigmem.StrTest) ... ok
test_replace (test.test_bigmem.StrTest) ... Skipping test_replace because 
of memory constraint
ok
test_repr_large (test.test_bigmem.StrTest) ... Skipping test_repr_large 
because of memory constraint
ok
test_repr_small (test.test_bigmem.StrTest) ... Skipping test_repr_small 
because of memory constraint
ok
test_rfind (test.test_bigmem.StrTest) ... Skipping test_rfind because of 
memory constraint
ok
test_rindex (test.test_bigmem.StrTest) ... Skipping test_rindex because 
of memory constraint
ok
test_rjust (test.test_bigmem.StrTest) ... ok
test_rstrip (test.test_bigmem.StrTest) ... ok
test_slice_and_getitem (test.test_bigmem.StrTest) ... Skipping 
test_slice_and_getitem because of memory constraint
ok
test_split_large (test.test_bigmem.StrTest) ... Skipping test_split_large 
because of memory constraint
ok
test_split_small (test.test_bigmem.StrTest) ... Skipping test_split_small 
because of memory constraint
ok
test_splitlines (test.test_bigmem.StrTest) ... Skipping test_splitlines 
because of memory constraint
ok
test_startswith (test.test_bigmem.StrTest) ... Skipping test_startswith 
because of memory constraint
ok
test_strip (test.test_bigmem.StrTest) ... ok
test_swapcase (test.test_bigmem.StrTest) ... Skipping test_swapcase 
because of memory constraint
ok
test_title (test.test_bigmem.StrTest) ... Skipping test_title because of 
memory constraint
ok
test_translate (test.test_bigmem.StrTest) ... Skipping test_translate 
because of memory constraint
ok
test_unicode_repr (test.test_bigmem.StrTest) ... Skipping 
test_unicode_repr because of memory constraint
ok
test_unicode_repr_overflow (test.test_bigmem.StrTest) ... Skipping 
test_unicode_repr_overflow because of memory constraint
ok
test_unicode_repr_wide (test.test_bigmem.StrTest) ... Skipping 
test_unicode_repr_wide because of memory constraint
ok
test_upper (test.test_bigmem.StrTest) ... Skipping test_upper because of 
memory constraint
ok
test_zfill (test.test_bigmem.StrTest) ... ok
test_capitalize (test.test_bigmem.BytesTest) ... ok
test_center (test.test_bigmem.BytesTest) ... ok
test_compare (test.test_bigmem.BytesTest) ... ok
test_concat (test.test_bigmem.BytesTest) ... ok
test_contains (test.test_bigmem.BytesTest) ... FAIL
test_count (test.test_bigmem.BytesTest) ... ok
test_decode (test.test_bigmem.BytesTest) ... ok
test_endswith (test.test_bigmem.BytesTest) ... ok
test_expandtabs (test.test_bigmem.BytesTest) ... ok
test_find (test.test_bigmem.BytesTest) ... ok
test_hash (test.test_bigmem.BytesTest) ... ok
test_index (test.test_bigmem.BytesTest) ... ok
test_isalnum (test.test_bigmem.BytesTest) ... ok
test_isalpha (test.test_bigmem.BytesTest) ... ok
test_isdigit (test.test_bigmem.BytesTest) ... ok
test_islower (test.test_bigmem.BytesTest) ... ok
test_isspace (test.test_bigmem.BytesTest) ... ok
test_istitle (test.test_bigmem.BytesTest) ... ok
test_isupper (test.test_bigmem.BytesTest) ... ok
test_join (test.test_bigmem.BytesTest) ... ok
test_ljust (test.test_bigmem.BytesTest) ... ok
test_lower (test.test_bigmem.BytesTest) ... ok
test_lstrip (test.test_bigmem.BytesTest) ... ok
test_repeat (test.test_bigmem.BytesTest) ... ok
test_replace (test.test_bigmem.BytesTest) ... ok
test_rfind (test.test_bigmem.BytesTest) ... ok
test_rindex (test.test_bigmem.BytesTest) ... ok
test_rjust (test.test_bigmem.BytesTest) ... ok
test_rstrip (test.test_bigmem.BytesTest) ... ok
test_slice_and_getitem (test.test_bigmem.BytesTest) ... ok
test_split_large (test.test_bigmem.BytesTest) ... Skipping 
test_split_large because of memory constraint
ok
test_split_small (test.test_bigmem.BytesTest) ... ok
test_splitlines (test.test_bigmem.BytesTest) ... ok
test_startswith (test.test_bigmem.BytesTest) ... ok
test_strip (test.test_bigmem.BytesTest) ... ok
test_swapcase (test.test_bigmem.BytesTest) ... ok
test_title (test.test_bigmem.BytesTest) ... ok
test_translate (test.test_bigmem.BytesTest) ... ok
test_upper (test.test_bigmem.BytesTest) ... ok
test_zfill (test.test_bigmem.BytesTest) ... ok
test_capitalize (test.test_bigmem.BytearrayTest) ... ok
test_center (test.test_bigmem.BytearrayTest) ... ok
test_compare (test.test_bigmem.BytearrayTest) ... ok
test_concat (test.test_bigmem.BytearrayTest) ... ok
test_contains (test.test_bigmem.BytearrayTest) ... FAIL
test_count (test.test_bigmem.BytearrayTest) ... ok
test_decode (test.test_bigmem.BytearrayTest) ... ok
test_endswith (test.test_bigmem.BytearrayTest) ... ok
test_expandtabs (test.test_bigmem.BytearrayTest) ... ok
test_find (test.test_bigmem.BytearrayTest) ... ok
test_index (test.test_bigmem.BytearrayTest) ... ok
test_isalnum (test.test_bigmem.BytearrayTest) ... ok
test_isalpha (test.test_bigmem.BytearrayTest) ... ok
test_isdigit (test.test_bigmem.BytearrayTest) ... ok
test_islower (test.test_bigmem.BytearrayTest) ... ok
test_isspace (test.test_bigmem.BytearrayTest) ... ok
test_istitle (test.test_bigmem.BytearrayTest) ... ok
test_isupper (test.test_bigmem.BytearrayTest) ... ok
test_join (test.test_bigmem.BytearrayTest) ... ok
test_ljust (test.test_bigmem.BytearrayTest) ... ok
test_lower (test.test_bigmem.BytearrayTest) ... ok
test_lstrip (test.test_bigmem.BytearrayTest) ... ok
test_repeat (test.test_bigmem.BytearrayTest) ... ok
test_replace (test.test_bigmem.BytearrayTest) ... ok
test_rfind (test.test_bigmem.BytearrayTest) ... ok
test_rindex (test.test_bigmem.BytearrayTest) ... ok
test_rjust (test.test_bigmem.BytearrayTest) ... ok
test_rstrip (test.test_bigmem.BytearrayTest) ... ok
test_slice_and_getitem (test.test_bigmem.BytearrayTest) ... ok
test_split_small (test.test_bigmem.BytearrayTest) ... ok
test_splitlines (test.test_bigmem.BytearrayTest) ... ok
test_startswith (test.test_bigmem.BytearrayTest) ... ok
test_strip (test.test_bigmem.BytearrayTest) ... ok
test_swapcase (test.test_bigmem.BytearrayTest) ... ok
test_title (test.test_bigmem.BytearrayTest) ... ok
test_translate (test.test_bigmem.BytearrayTest) ... ok
test_upper (test.test_bigmem.BytearrayTest) ... ok
test_zfill (test.test_bigmem.BytearrayTest) ... ok
test_compare (test.test_bigmem.TupleTest) ... Skipping test_compare 
because of memory constraint
ok
test_concat_large (test.test_bigmem.TupleTest) ... Skipping 
test_concat_large because of memory constraint
ok
test_concat_small (test.test_bigmem.TupleTest) ... Skipping 
test_concat_small because of memory constraint
ok
test_contains (test.test_bigmem.TupleTest) ... Skipping test_contains 
because of memory constraint
ok
test_from_2G_generator (test.test_bigmem.TupleTest) ... Skipping 
test_from_2G_generator because of memory constraint
ok
test_from_almost_2G_generator (test.test_bigmem.TupleTest) ... Skipping 
test_from_almost_2G_generator because of memory constraint
ok
test_hash (test.test_bigmem.TupleTest) ... Skipping test_hash because of 
memory constraint
ok
test_index_and_slice (test.test_bigmem.TupleTest) ... Skipping 
test_index_and_slice because of memory constraint
ok
test_repeat_large (test.test_bigmem.TupleTest) ... Skipping 
test_repeat_large because of memory constraint
ok
test_repeat_large_2 (test.test_bigmem.TupleTest) ... Skipping 
test_repeat_large_2 because of memory constraint
ok
test_repeat_small (test.test_bigmem.TupleTest) ... Skipping 
test_repeat_small because of memory constraint
ok
test_repr_large (test.test_bigmem.TupleTest) ... Skipping test_repr_large 
because of memory constraint
ok
test_repr_small (test.test_bigmem.TupleTest) ... ERROR
test_append (test.test_bigmem.ListTest) ... Skipping test_append because 
of memory constraint
ok
test_compare (test.test_bigmem.ListTest) ... Skipping test_compare 
because of memory constraint
ok
test_concat_large (test.test_bigmem.ListTest) ... Skipping 
test_concat_large because of memory constraint
ok
test_concat_small (test.test_bigmem.ListTest) ... Skipping 
test_concat_small because of memory constraint
ok
test_contains (test.test_bigmem.ListTest) ... Skipping test_contains 
because of memory constraint
ok
test_count (test.test_bigmem.ListTest) ... Skipping test_count because of 
memory constraint
ok
test_extend_large (test.test_bigmem.ListTest) ... Skipping 
test_extend_large because of memory constraint
ok
test_extend_small (test.test_bigmem.ListTest) ... Skipping 
test_extend_small because of memory constraint
ok
test_hash (test.test_bigmem.ListTest) ... Skipping test_hash because of 
memory constraint
ok
test_index (test.test_bigmem.ListTest) ... Skipping test_index because of 
memory constraint
ok
test_index_and_slice (test.test_bigmem.ListTest) ... Skipping 
test_index_and_slice because of memory constraint
ok
test_inplace_concat_large (test.test_bigmem.ListTest) ... Skipping 
test_inplace_concat_large because of memory constraint
ok
test_inplace_concat_small (test.test_bigmem.ListTest) ... Skipping 
test_inplace_concat_small because of memory constraint
ok
test_inplace_repeat_large (test.test_bigmem.ListTest) ... Skipping 
test_inplace_repeat_large because of memory constraint
ok
test_inplace_repeat_small (test.test_bigmem.ListTest) ... Skipping 
test_inplace_repeat_small because of memory constraint
ok
test_insert (test.test_bigmem.ListTest) ... Skipping test_insert because 
of memory constraint
ok
test_pop (test.test_bigmem.ListTest) ... Skipping test_pop because of 
memory constraint
ok
test_remove (test.test_bigmem.ListTest) ... Skipping test_remove because 
of memory constraint
ok
test_repeat_large (test.test_bigmem.ListTest) ... Skipping 
test_repeat_large because of memory constraint
ok
test_repeat_small (test.test_bigmem.ListTest) ... Skipping 
test_repeat_small because of memory constraint
ok
test_repr_large (test.test_bigmem.ListTest) ... Skipping test_repr_large 
because of memory constraint
ok
test_repr_small (test.test_bigmem.ListTest) ... Killed





----------------------------------------
Subject:
[Python-Dev] test_bigmem and test_bigaddrspace broken
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 

Hello,

Le mercredi 17 mars 2010 ? 11:58 -0700, C. Titus Brown a ?crit :

Thanks for the proposal.
That wasn't really the point of my message, though :)

As mentioned I have access to a 16GB machine which is able to run the
tests. The problem is that nobody seems interested in maintaining these
tests.
As far as I can say, the initial committer is Thomas Wouters, and not
much happened since then. It seems to me that big companies such as
Google and Apple would be the primarily concerned ones by such
scenarios, and willing to ensure they run fine.

(by the way, even without swapping and in non-debug mode, the tests are
quite long to run)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] test_bigmem and test_bigaddrspace broken
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

So what do you propose to do?

I personally don't see that as a problem. Many rarely-used features get
infrequent testing - that's the nature of rarely-used features. In turn,
they tend to break over time, until somebody comes along and fixes them.
That's open source: scratch your own itches.


I'm not sure Apple has really Python applications running that use a lot
of memory - being a big company doesn't necessarily mean you operate
large machines. I'm sure Google has applications that use that much
memory, and I wouldn't be surprised if Google has also bug fixes for
various bugs in large objects that they haven't been contributing (yet).

In any case, I think it's a good thing that these tests are there for
people to run if they have the hardware and the time. If somebody wanted
to contribute such hardware as a build slave - that could certainly be
arranged.

Regards,
Martin

