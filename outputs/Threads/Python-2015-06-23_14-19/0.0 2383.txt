
============================================================================
Subject: [Python-Dev] PyUnicode_Resize
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PyUnicode_Resize
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

In Python 3.2, PyUnicode_Resize() expects a number of Py_UNICODE units, 
whereas Python 3.3 expects a number of characters.

It is tricky to convert a number of Py_UNICODE units to a number of 
characters, so it is diffcult to provide a backward compatibility 
PyUnicode_Resize() function taking a number of Py_UNICODE units in Python 3.3.

Should we rename PyUnicode_Resize() in Python 3.3 to avoid surprising bugs?

The issue only concerns Windows with non-BMP characters, so a very rare use 
case.

The easiest solution is to do nothing in Python 3.3: the API changed, but it 
doesn't really matter. Developers just have to be careful on this particular 
issue (which is not well documented today).

Victor



----------------------------------------
Subject:
[Python-Dev] PyUnicode_Resize
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
2011/11/22 Victor Stinner <victor.stinner at haypocalc.com>


+1. A note in the "Porting C code" section of whatsnew/3.3 should be enough.

-- 
Amaury Forgeot d'Arc
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111122/6e7c5235/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PyUnicode_Resize
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Is that really the case? If the string is not ready (i.e. the kind is
WCHAR_KIND), then it does count Py_UNICODE units, no?

Callers are supposed to call PyUnicode_Resize only while the string is
under construction, i.e. when it is not ready. If they resize it after
it has been readied, changes to the Py_UNICODE representation wouldn't
be reflected in the canonical representation, anyway.


IIUC (and please correct me if I'm wrong) this issue won't cause memory
corruption: if they specify a new size assuming it's Py_UNICODE units,
but interpreted as code points, then the actual Py_UNICODE buffer can
only be larger than expected - right?

If so, callers could happily play with Py_UNICODE representation. It
won't have the desired effect if the string was ready, but it won't
crash Python, either.


See above. I think there actually is no issue in the first place. Please
do correct me if I'm wrong.

Regards,
Martin

