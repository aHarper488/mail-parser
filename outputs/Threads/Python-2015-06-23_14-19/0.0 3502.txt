
============================================================================
Subject: [Python-Dev] peps: Update PEP 1 to better reflect current
	practice
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] peps: Update PEP 1 to better reflect current
	practice
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 6 May 2012 15:08:52 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

+1 for overthrowing czars!

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] peps: Update PEP 1 to better reflect current
	practice
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sun, May 6, 2012 at 5:29 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

I expect PEP czar will stick as the nickname (that's why I still
mention it in PEP 1), but I definitely prefer having something a bit
more self explanatory as the official designation.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

