
============================================================================
Subject: [Python-Dev] A grammatical oddity: trailing commas in argument
	lists -- continuation
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] A grammatical oddity: trailing commas in argument
	lists -- continuation
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 12/13/2010 08:25 AM, Nick Coghlan wrote:

I actually make use of the feature when dealing with APIs which both a)
take lots of arguments (more than fit comfortably on two lines at
whatever indentation they are called), and b) have optional trailing
arguments:  I always leave the trailing comma in place in such cases,
with the closing paren on the following line, so that adding or removing
an argument at the end of the list stays consistent (the diffs are
better, too, when I use this pattern).



Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk0GIsoACgkQ+gerLs4ltQ5HLwCfYFSyVrFtt04h6a39hyK6BD2c
t8oAoJdXNS7wIsjF34ZiOQCwQGq9Qs2v
=ZWqW
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] A grammatical oddity: trailing commas in argument
	lists -- continuation
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Mon, 13 Dec 2010 23:25:58 +1000, Nick Coghlan <ncoghlan at gmail.com> wrote:

Counter examples from google code search:
    
    http://www.google.com/codesearch/p?hl=en#copo3dCwf5E/django/utils/simplejson/encoder.py&q=^\s*\):&sa=N&cd=5&ct=rc
        (also appears in json in the stdlib)
    http://www.google.com/codesearch/p?hl=en#algXCqBNNP0/vendor/python-clientform/ClientForm.py&q=^\ *\):&sa=N&cd=3&ct=rc
        (class def)
    http://www.google.com/codesearch/p?hl=en#KT-ZlRkUunU/trunk/code/output/ExprParser.py&q=def\(.*,\s\):&sa=N&cd=2&ct=rc
    http://www.google.com/codesearch/p?hl=en#XnG7n8Mjf2s/GoogleSearch.py&q=def\(.*,\s\):&sa=N&cd=3&ct=rc
    http://www.google.com/codesearch/p?hl=en#MokQ50OeeyU/src/python/ndogen/parser/matlab/parser.py&q=def\(.*,\s\):&sa=N&cd=5&ct=rc

Not many, granted, but not zero, either, and I'm sure there are lots
more out there[*].  I do especially like the fact that there is one in the
stdlib :)

It seems like the status quo is fine.  I wouldn't object to it being
made more consistent.  I would object to removing the existing cases.

--
R. David Murray                                      www.bitdance.com

[*] code search's response to various regexes was somewhat surprising;
expressions I thought should have been supersets resulted in fewer
hits.  Nor could I think of a way to search for function invocations
ending with a comma.  Then again, I usually make lots of mistakes
with regexes.



----------------------------------------
Subject:
[Python-Dev] A grammatical oddity: trailing commas in argument
	lists -- continuation
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 12/13/2010 2:17 PM, Antoine Pitrou wrote:

Same here. A strong +1 for a consistent rule (always or never allowed) 
with a +1 for always given others use case of one param/arg per line.

So I think the issues should be reopened.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] A grammatical oddity: trailing commas in argument
	lists -- continuation
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Dec 13, 2010, at 1:21 PM, Terry Reedy wrote:




It seems to me that a trailing comma in an argument list is more likely to be a user error than a deliberate comma-for-the-future. 


Raymond





----------------------------------------
Subject:
[Python-Dev] A grammatical oddity: trailing commas in argument
	lists -- continuation
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Dec 13, 2010, at 2:16 PM, Guido van Rossum wrote:


I only have one data point, my own mistakes.  The SyntaxError
has occasionally been helpful to me when working out a function
signature or to detect a copy and paste error.  In both cases,
it meant that there was supposed to be another argument
and it had been either forgotten or mispasted.

Also, if I were reviewing someone else's code and saw
a trailing comma in a function definition, it would
seem weird and I would wonder if the author intended
a different signature.

FWIW, this isn't important to me at all.  Was just noting
my own experience.  Don't put assign much weight to it,
I don't have much of a preference either way.


Right.  I see that in the wild quite often and use it myself.


Raymond




----------------------------------------
Subject:
[Python-Dev] A grammatical oddity: trailing commas in argument
	lists -- continuation
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 13.12.2010 21:08, schrieb Glenn Linderman:

Clearly Emacs is superior to Vim because (insert some random fact here).

Clearly the only thing that is clear about coding style details (even if
we all more or less agree on PEP 8) is that it is a matter of personal
taste.

Georg


