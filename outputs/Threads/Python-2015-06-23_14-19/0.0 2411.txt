
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Implement PEP 393.
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Implement PEP 393.
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Am 29.09.2011 01:21, schrieb Eric V. Smith:

Yes: I couldn't figure out how to do it any other way. The formatting
code had a few basic assumptions which now break (unless you keep using
the legacy API). Primarily, the assumption is that there is a notion of
a "STRINGLIB_CHAR" which is the element of a string representation. With
PEP 393, no such type exists anymore - it depends on the individual
object what the element type for the representation is.

In other cases, I worked around that by compiling the stringlib three
times, for Py_UCS1, Py_UCS2, and Py_UCS4. For one, this gives
considerable code bloat, which I didn't like for the formatting code
(as that is already a considerable amount of code). More importantly,
this approach wouldn't have worked well, anyway, since the formatting
combines multiple Unicode objects (especially with the OutputString
buffer), and different inputs may have different representations. On
top of that, OutputString needs widening support, starting out with
a narrow string, and widening step-by-step as input strings are more
wide than the current output (or not, if the input strings are all
ASCII).

It would have been possible to keep the basic structure by doing
all formatting in Py_UCS4. This would cost a significant memory and
runtime overhead.


I'm sorry about that. Try applying them to the new files, though - patch
may still be able to figure out how to integrate them, as the
algorithms and function structure hasn't changed.


Please try for yourself. On string_format.h, I think there is zero
chance, unless you want to compromise and efficiency (in addition to
the already-present compromise on code cleanliness, due the the fact
that the code is more general than it needs to be).

On formatter.h, it may actually be possible to restore what it was - in
particular if you can make a guarantee that all number formatting always
outputs ASCII-strings only (which I'm not so sure about, as the
thousands separator could be any character, in principle). Without that
guarantee, it may indeed be reasonable to compile formatter.h in
Py_UCS4, since the resulting strings will be small, so the overhead is
probably negligible.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Implement PEP 393.
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
On 10/1/2011 9:26 AM, "Martin v. L?wis" wrote:

Martin: Thanks so much for your thoughtful answer. You've obviously
given this more thought than I have. From your answer, it does indeed
sound like string_format.h needs to be removed from stringlib. I'll have
to think more about formatter.h.

On the other hand, not having this code in stringlib would certainly be
liberating! Maybe I'll take this opportunity to clean it up and simplify
it now that it's free of the stringlib constraints.

Eric.



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Implement PEP 393.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Oct 1, 2011 at 4:07 PM, Eric V. Smith <eric at trueblade.com> wrote:

Yeah, don't sacrifice speed in str.format for a
still-hypothetical-and-potentially-never-going-to-happen bytes
formatting variant. If the latter does happen, the use cases would be
different enough that I'm not even sure the mini-language should
remain entirely the same (e.g. you'd likely want direct access to some
of the struct module formatting more so than str-style formats).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Implement PEP 393.
----------------------------------------
Author: Eric V. Smit
Attributes: []Content: 
Is there some reason str.format had such major surgery done to it? It
appears parts of it were removed from stringlib. I had not even thought
to look at the code before it was merged, as it never occurred to me
anyone would do that.

I left it in stringlib even in 3.x because there's the occasional talk
of adding bytes.bformat, and since all of the code works well with
stringlib (since it was used by str and unicode in 2.x), it made sense
to leave it there.

In addition, there are outstanding patches that are now broken.

I'd prefer it return to how it used to be, and just the minimum changes
required for PEP 393 be made to it.

Thanks.
Eric.

On 9/28/2011 2:35 AM, martin.v.loewis wrote:



