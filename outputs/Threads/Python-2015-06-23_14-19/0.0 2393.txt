
============================================================================
Subject: [Python-Dev] cpython: fix compiler warning by implementing this
	more cleverly
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: fix compiler warning by implementing this
	more cleverly
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 11/22/2011 7:42 PM, Benjamin Peterson wrote:

I personally strongly prefer the one-line formula to the hardcoded magic 
numbers calculated from the formula. I find it much more readable. To 
me, the only justification for the switch would be if there is a serious 
worry about the kind being changed to something other than 1, 2, or 4. 
But the fact that this is checked with an assert that can be optimized 
away negates that. The one-liner could be followed by
   assert(kind==1 || kind==2 || kind==4)
which would also serve to remind the reader of the possibilities. You 
could even follow the formula with /* 4, 6, or 10 */ I think you 
reverted too soon.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] cpython: fix compiler warning by implementing this
	more cleverly
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le Mercredi 23 Novembre 2011 01:49:28 Terry Reedy a ?crit :

For a ready string, kind must be 1, 2 or 4. We might rename "kind" to 
"charsize" because its value changed from 1, 2, 3 to 1, 2, 4 (to make it easy 
to compute the size of a string: length * kind).

You are not supposed to see the secret kind==0 case. This value is only used 
for string created by _PyUnicode_New() and not ready yet:

  str = _PyUnicode_New()
  /* use str */
  assert(PyUnicode_KIND(str) == 0);
  if (PyUnicode_READY(str) < 0)
     /* error */
  assert(PyUnicode_KIND(str) != 0); /* kind is 1, 2, 4 */

Thanks to the effort of t0rsten, Martin and me, quite all functions use the 
new API (PyUnicode_New). For example, PyUnicode_AsRawUnicodeEscapeString() 
starts by ensuring that the string is ready.

For your information, PyUnicode_KIND() fails with an assertion error in debug 
mode if the string is not ready.

--

I don't have an opinion about the one-liner vs the switch :-)

But if you want to fix compiler warnings, you should use "enum PyUnicode_Kind" 
type and PyUnicode_WCHAR_KIND should be removed from the enum.

Victor

