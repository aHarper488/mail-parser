
============================================================================
Subject: [Python-Dev] Unladen swallow status
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Unladen swallow status
----------------------------------------
Author: Reid Kleckne
Attributes: []Content: 
On Wed, Jul 21, 2010 at 8:11 AM, Tim Golden <mail at timgolden.me.uk> wrote:

Yeah, this has slipped.  I have patches that need review, and Jeff and
Collin have been distracted with other work.  Hopefully when one of
them gets around to that, I can proceed with the merge without
blocking on them.

Reid



----------------------------------------
Subject:
[Python-Dev] Unladen swallow status
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Wed, Jul 21, 2010 at 6:50 PM, Reid Kleckner <reid.kleckner at gmail.com> wrote:

The merge py3k-jit to trunk?

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] Unladen swallow status
----------------------------------------
Author: Collin Winte
Attributes: []Content: 
On Wed, Jul 21, 2010 at 2:43 PM, Maciej Fijalkowski <fijall at gmail.com> wrote:

I believe he's talking about the merger of the Unladen tree into the
py3k-jit branch.

Collin



----------------------------------------
Subject:
[Python-Dev] Unladen swallow status
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Wed, Jul 21, 2010 at 11:45 PM, Collin Winter <collinwinter at google.com> wrote:

If so, how does this relate to 3.2 and 3.3 release schedule?

Cheers,
fijal

