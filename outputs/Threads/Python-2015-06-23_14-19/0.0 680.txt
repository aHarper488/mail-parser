
============================================================================
Subject: [Python-Dev] Python and Windows 2000
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
I don't recall whether we have already decided about continued support
for Windows 2000.

If not, I'd like to propose that we phase out that support: the Windows
2.7 installer should display a warning; 3.2 will stop supporting Windows
2000.

Opinions?

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Mar 1, 2010 at 1:40 PM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

+2000

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: ssteinerX@gmail.co
Attributes: []Content: 

On Mar 1, 2010, at 4:53 PM, Guido van Rossum wrote:


I guess i'll finally have to retire my ten year old MSDN cd's and my favorite version of Windows -- the last one I ever used.

S





----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Mar 1, 2010 at 13:40, "Martin v. L?wis" <martin at v.loewis.de> wrote:


Considering MS is retiring support for Win2K in July (
http://support.microsoft.com/lifecycle/?p1=3071) I think we can be faster
than them in dropping it. =)  +1 from me.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100301/2d7e1943/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: Neil Hodgso
Attributes: []Content: 
Martin v. L?wis:


   Is there any reason for this? I can understand dropping Windows 9x
due to the lack of Unicode support but is there anything missing from
Windows 2000 that makes supporting it difficult?

   Neil



----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

See http://bugs.python.org/issue6926

The SDK currently hides symbolic constants from us that people are
asking for.

In addition, we could simplify the code in dl_nt.c around
GetCurrentActCtx and friends, by linking to these functions directly.

Regards,
Martni



----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: Neil Hodgso
Attributes: []Content: 
Martin v. L?wis:


   Setting the version to 0x501 (XP) doesn't actively try to stop
running on version 0x500 (2K), it just reveals the symbols and APIs
from 0x501. Including a call to an 0x501-specific API will then fail
at load.

    IPPROTO_IPV6 (the cause of issue 6926) isn't a new symbol that
started working in Windows XP - it was present in older SDKs without a
version guard so was visible when compiling for any version of
Windows.


   It would be simpler but its not as if this code needs any changes
at this point.

   I don't really have a strong need for Windows 2000 although I keep
an instance for checking compatibility of my code and I do still get
queries from people using old versions of Windows, including 9x. There
is the question of whether to force failure on Windows 2000 or just
remove it from the list of known-working platforms while still
allowing it to run.

   Neil



----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Neil Hodgson wrote:

I'd be grateful if you could refrain from doing anything to
actively break it. Win 2000 was the last version to be free
of Microsoft DRM crap, and I'd be unhappy if I were forced
to downgrade to a later version just so I could use Py3 on
it.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Python and Windows 2000
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 
Neil Hodgson wrote:

I'm not proposing to actively break Windows 2000 support. I propose to
stop supporting it, which means that we will not strive for Python
running on Windows 2000, and that bug reports that report issues
specific with Windows 2000 will be closed as "won't fix".

I would not want to bump the SDK version while still aiming at Windows
2000 support - as that gets rarely tested, this API version is actually
a mechanism to simplify ongoing support for Windows 2000.


I would propose the latter; see above. However, patches that do actively
break it would then be acceptable.

In any case, we would need a warning in the installer that Windows 2000
supports is going away.

Regards,
Martin


