
============================================================================
Subject: [Python-Dev] Ack, wrong list
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Ack, wrong list
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Sorry, my last mail was meant to go to python-ideas, not python-dev
(and the gmail/mailman disagreement means I can't easily reply to it).

Reply to the version on python-ideas please, not the version on here.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

