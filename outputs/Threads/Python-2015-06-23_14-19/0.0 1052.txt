
============================================================================
Subject: [Python-Dev] Patch making the current email package
	(mostly)	support bytes
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Patch making the current email package
	(mostly)	support bytes
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
R. David Murray writes:
 > On Mon, 04 Oct 2010 12:32:26 -0400, Scott Dial <scott+python-dev at scottdial.com> wrote:
 > > On 10/2/2010 7:00 PM, R. David Murray wrote:
 > > > The clever hack (thanks ultimately to Martin) is to accept 8bit data
 > > > by encoding it using the ASCII codec and the surrogateescape error
 > > > handler.
 > > 
 > > I've seen this idea pop up in a number of threads. I worry that you are
 > > all inventing a new kind of dual that is a direct parallel to Python 2.x
 > > strings.
 > 
 > Yes, that is exactly my worry.

I don't worry about this.  Strings generated by decoding with
surrogate-escape are *different* from other strings: they contain
invalid code units (the naked surrogates).  These cannot be encoded
except with a surrogate-escape flag to .encode(), and sane developers
won't do that unless she knows precisely what she's doing.  This is
not true with Python 2 strings, where all bytes are valid.

 > > Any reasonable 2.x code has to guard on str/unicode and it would seem in
 > > 3.x, if this idiom spreads, reasonable code will have to guard on
 > > surrogate escapes (which actually seems like a more expensive test).
 > 
 > Right, I mentioned that concern in my post.

Again, I don't worry about this.  It is *not* an *extra* cost.  Those
messages are *already broken*, they *will* crash the email module if
you fail to guard against them.  Decoding them to surrogates actually
makes it easier to guard, because you know that even if broken
encodings are present, the parser will still work.  Broken encodings
can no longer crash the parser.  That is a Very Good Thing IMHO.

 > Only if the email package contains a coding error would the
 > surrogates escape and cause problems for user code.

I don't think it is reasonable to internalize surrogates that way;
some applications *will* want to look at them and do something useful
with them (delete them or replace them with U+FFFD or ...).  However,
I argue below that the presence of surrogates already means the user
code is under fire, and this puts the problem in a canonical form so
the user code can prepare for it (if that is desirable).

 > > It seems like this hack is about making the 3.x unicode type more like
 > > the 2.x string type,

Not at all.  It's about letting the parser be a parser, and letting
the application handle broken content, or discard it, or whatever.
Modularity is improved.  This has been a major PITA for Mailman
support over the years: every time the spammers and virus writers come
up with a new idea, there's a chance it will leak out and the email
parser will explode, stopping the show.  These kinds of errors are a
FAQ on the Mailman lists (although much less so in recent years).

 > > How will developers not have to ask themselves whether a given
 > > string is a "real" string or a byte sequence masquerading as a
 > > string? Am I missing something here?

There are two things to say, actually.  First, you're in a war zone.
*All* email is bytes sequences masquerading as text, and if you're not
wearing armor, you're going to get burned.  The idea here is to have
the email package provide the armor and enough instrumentation so you
can do bomb detection yourself (or perhaps just let it blow, if you're
hacking up a quick and dirty script).

Second, there are developers who will not care whether strings are
"real" or "byte sequences in drag", because they're writing MTAs and
the like.  Those people get really upset, and rightly so, when the
parser pukes on broken headers; it is not their app's job at all to
deal with that breakage.

 > I think this question is something that needs to be considered any
 > time using surrogates is proposed.

I don't agree.  The presence of naked surrogates is *always* (assuming
sane programmers) an indication of invalid input.  The question is,
should the parser signal invalidity, or should it allow the
application to decide?  The email module *doesn't have enough
information to decide* whether the invalid input is a "real" problem,
or how to handle it (cf the example of a MTA app).  Note that a
completely naive app doesn't care -- it will crash either way because
it doesn't handle the exception, whether it's raised by the parser or
by a codec when the app tries to do I/O.  A robust app *does* care: if
the parser raises, then the app must provide an alternative parser
good enough to find and fix the invalid bytes.  Clearly it's much
better to pass invalid (but fully parsed) text back to the app in this
case.

Note that if the app really wants the parser to raise rather than pass
on the input, that should be easy to implement at fairly low cost; you
just provide a variable rather than hardcoding the surrogate-escape
flag.



----------------------------------------
Subject:
[Python-Dev] Patch making the current email package
	(mostly)	support bytes
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
R. David Murray writes:

 > version of headers to the email5 API, but since any such data would
 > be non-RFC compliant anyway, [access to non-conforming headers by
 > reparsing the bytes] will just have to be good enough for now.

But that's potentially unpleasant for, say, Mailman.  AFAICS, what
you're saying is that Mailman will have to implement a full header
parser and repair module, or shunt (and wait for administrator
intervention on) any mail that happens to contain even one byte of
non-RFC-conforming content in a header it cares about.  (Note that
we're not talking about moderator-level admins here; we're talking
about the Big Cheese with access to the command line on the list
host.)  That's substantially worse than the current system, where (in
theory, and in actual practice where it distributes its own version of
email) it can trap the Unicode exception on a per-header basis.

I also worry about the implications for backwards compatibility.
Eventually email-N needs to handle non-conforming mail in a sensible
way, or anybody who gets spam (ie, everybody) and wants a reliable
email system will need to implement their own.  If you punt completely
on handling non-conforming mail now, when is it going to be done?  And
when it is done, will the backward-compatible interface be able to
access the robust implementation, or will people who want robust APIs
have to use rather different ones?  The way you're going right now, I
have to worry about the answer to the second question, at least.

 > [*] Why '?' and not the unicode invalid character character?  Well, the
 > email5 Generate.flatten can be used to generate data for transmission over
 > the wire *if* the source is RFC compliant and 7bit-only, and this would
 > be a normal email5 usage pattern (that is, smtplib.SMTP.sendmail expects
 > ASCII-only strings as input!).  So the data generated by Generator.flatten
 > should not include unicode...

I don't understand this at all.  Of course the byte stream generated
by Generator.flatten won't contain Unicode (in the headers, anyway);
it will contain only ASCII (that happens to conform to QP or Base64
encoding of Unicode in some appropriate UTF in many cases).  Why is
U+FFFD REPLACEMENT CHARACTER any different from any other non-ASCII
character in this respect?

(Surely you are not saying that Generator.flatten can't DTRT with
non-ASCII content *at all*?)

The only thing I can think of is that you might not want to introduce
non-ASCII characters into a string that looks like it might simply be
corrupted in transmission (eg, it contains only one non-ASCII byte).
That's reasonable; there are a lot of people who don't have to deal
with anything but ASCII and occasionally Latin-1, and they don't like
having Unicode crammed down their throats.

 > which raises a problem for CTE 8bit sections
 > that the patch doesn't currently address.

AFAIK, there's no requirement, implied or otherwise, that a conforming
implementation *produce* CTE 8bit.  So just don't do that; that will
keep smtplib happy, no?



----------------------------------------
Subject:
[Python-Dev] Patch making the current email package
	(mostly)	support bytes
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
R. David Murray writes:

 > So the only parsing issue is if Mailman cares about *the non-ASCII
 > bytes* in the headers it cares about.  If it has to modify headers that
 > contain non-ASCII bytes (for example, addresses and Subject) and cares
 > about preserving the non-ASCII bytes, then there is indeed an issue;
 > see previous email for a possible way around that.

OK.

 > I thought mailman no longer distributed its own version of email?

I believe so; the point is that it could do so again.

 > And the email API currently promises not to raise during parsing,
 > which is a contract my patch does not change.

Which is a contract that has historically been broken frequently.
Unhandled UnicodeErrors have been one of the most common causes of
queue stoppage in Mailman (exceeded only by configuration errors
AFAICS).  I haven't seen any reports for a while, but with the email
package being reengineered from the ground up, the possibility of
regression can't be ignored.

Granted, there should be no regression problem in the current model
for Email5, AIUI.

 > We're (in the current patch) not punting on handling non-conforming
 > email, we're punting on handling non-conforming bytes *if the headers
 > that contain them need to be modified*.  The headers can still be
 > modified, you just (currently) lose the non-ASCII bytes in the process.

Modified *or examined*.  I can't think of any important applications
offhand that *need* to examine the non-ASCII bytes (in particular,
Mailman doesn't need to do that).  Verbatim copying of the bytes
themselves is almost always the desired usage.

 > And robustness is not the issue, only extended-beyond-the-RFCs handling
 > of non-conforming bytes would be an issue.

And with that, I'm certain that Jon Postel is really dead. :-(

 > > (Surely you are not saying that Generator.flatten can't DTRT with
 > > non-ASCII content *at all*?)
 > 
 > Yes, that is *exactly* what I am saying:
 > 
 > >>> m = email.message_from_string("""\
 > ... From: p?stal
 > ...   
 > ... """)
 > >>> str(m)
 > Traceback (most recent call last):
 >   ....
 > UnicodeEncodeError: 'ascii' codec can't encode character '\xf6' in position 1: ordinal not in range(128)

But that's not interesting; you did that with Python 3.  We want to
know what people porting from Python 2 will expect.  So, in 2.5.5 or
2.6.6 on Mac, with email v4.0.2, it *doesn't* raise, it returns

wideload:~ 4:14$ python
Python 2.5.5 (r255:77872, Jul 13 2010, 03:03:57) 
[GCC 4.0.1 (Apple Inc. build 5490)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
'From nobody Thu Oct  7 04:18:25 2010\nFrom: p\xc3\xb6stal\n\n'
'p\xc3\xb6stal'

That's hardly helpful!  Surely we can and should do better than that
now, especially since UTF-8 (with a proper CTE) is now almost
universally acceptable to MUAs.  When would it be a problem for that
to return

'From nobody Thu Oct  7 04:18:25 2010\nFrom: =?UTF-8?Q?p=C3=B6stal?=\n\n'

 > Remember, email5 is a direct translation of email4, and email4 only
 > handled ASCII and oh-by-the-way-if-there-are-bytes-along-for-the-
 > -ride-fine-we'll-pass-then-along.  So if you want to put non-ASCII
 > data into a message you have to encode it properly to ASCII in
 > exactly the same way that you did in email4:

But if you do it right, then it will still work in a version that just
encodes non-ASCII characters in UTF-8 with the appropriate CTE.  Since
you'll never be passing it non-ASCII characters, it's already ASCII
and UTF-8, and no CTE will be needed.

 > Yes, exactly.  I need to fix the patch to recode using, say,
 > quoted-printable in that case.

It really should check for proportions of non-ASCII.  QP would be
horrible for Japanese or Chinese.

 > DecodedGenerator could still produce the unicode, though, which is
 > what I believe we want.  (Although that raises the question of
 > whether DecodedGenerator should also decode the RFC2047 encoded
 > headers....but that raises a backward compatibility issue).

Can't really help you there.  While I would want the RFC 2047 headers
decoded if I were writing new code (which is generally the case for
me), I haven't really wrapped my head around the issues of porting old
code using Python2 str to Python3 str here.  My intuition says "no
problem" (there won't be any MIME-words so the app won't try to decode
them), but I'm not real sure of that. ;-)



----------------------------------------
Subject:
[Python-Dev] Patch making the current email package
	(mostly)	support bytes
----------------------------------------
Author: lutz at rmi.ne
Attributes: []Content: 
Stephen J. Turnbull wrote (giving me an opening to jump in here):

Well, yes there are, and yes it is.  As I pointed out in a thread 
on this list back in June, there are multiple large Python 3 email 
"apps" in the new Programming Python, a book which is about to be 
released, and which will be read by at least tens of thousands of 
people, many of whom will be evaluating the stability of Python 3.

These apps include both a simple webmail site, as well as a more
sophisticated 5k-line tkinter email client -- one which I've been 
using for all my personal and business email over the last 6 months,
and which works well with the email package as it is in 3.1 (albeit
with a bit of workaround code).  This includes support for Unicode,
MIME, headers, attachments, and the lot.

I'm forwarding a link to the code of these clients to David by 
private email in case they might be useful as a test case (O'Reilly
has already posted them ahead of the book, but they may be a bit too
heavy for use in formal testing).

The email package is obviously less than ideal today, and there are
many other clients for it besides my own, of course.  But making it 
backward incompatible at this point is likely to be seen as a big 
negative to newcomers evaluating 3.X viability.  And as I tried to 
make clear in June, this list should carefully weigh the PR cost of 
pulling the rug out from under those brave souls who have already 
taken the time to accommodate the 3.X world you've mandated.

To put that more strongly, the Python user base is much larger than 
this list's readership.  If I'm using 3.1 email, so are many others.
People will accept the 3.X world you make up to a point, but it's 
impossible to code to a moving target, much less base a product on 
it.  At some point, they'll simply stop trying to keep up; in fact, 
some already have.

Fixes are a Good Thing, of course, and this particular change's scope
remains to be seen; but to channel most of the users I meet out there
in the real world today: Enough with the 3.X changes already, eh?

--Mark Lutz  (http://learning-python.com, http://rmi.net/~lutz)






----------------------------------------
Subject:
[Python-Dev] Patch making the current email package
	(mostly)	support bytes
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
R. David Murray writes:

 > > The MIME-charset = UNKNOWN dodge might be a better way of handling
 > > this.
 > 
 > That is a very interesting idea.  It is the *right* thing to do, since it
 > would mean that a message parsed as bytes could be generated via Generator
 > and passed to, say, smtplib without losing any information.  However,
 > It's not exactly trivial to implement, since issues of runs of characters
 > and line re-wrapping need need to be dealt with.  Perhaps Header can be
 > made to handle bytes in order to do this; I'll have to look in to
 > it.

Ouch.  RFC 822 line wrapping is a bytes->bytes transformation, and the
client shouldn't see it at all unless it inspects the wire format.
MIME-encoding is a text->bytes transformation, again an internal
matter.  The constraints on the wire format means that the MIME-
encoder needs to careful about encoded-word length.  ISTM that all you
need to know, assuming that this is a method on a Header, and it's
normally invoked just before conversion to bytes, is the codec and the
CTE, and both can be optional (default to 'utf-8' and a value
depending on the proportion of encodable characters).

You take the header, encode according to the codec, then start
MIME-encoding according to the CTE.  The maximum size of encoded words
is chosen to fit on a line within 78 bytes.  The number of bytes
encoded in each word depends only on the size of metadata associated
with the word.  (Sure you could make it prettier for those reading it
with an "MUA" like less, but I don't think that's really worth
anybody's time.)

*If* you have an 8-bit value of unknown encoding on input, this will
appear in the Header's value as a surrogate.  Hm, OK, I see the
problem ... as usual, it's that the only efficient thing to do is
encode using surrogate-escape which loses the information that these
are invalid bytes.  Would it really be that bad to add an O(length)
component where you examine the string for surrogates (and too-long
words, for that matter), and chop off those pieces for MIME encoding?

 > >  > Presumably you are suggesting that email5 be smart enough to turn my
 > >  > example into properly UTF-8/CTE encoded text.
 > > 
 > > No, in general that's undecidable without asking the originator,
 > > although humans can often make a good guess.
 > 
 > I was talking about unicode input, though, where you do know (modulo
 > the language differences that unicode hasn't yet sorted out).

I don't understand why this is difficult.  As far as what Unicode has
and hasn't sorted out, that's not your job AFAICS.  If clients want a
specific codec or other language-based style, they'd better specify it
themselves.  Else, you just stuff the Unicode into a UTF-8-encoded
bytes, and go from there.  This is *why* Unicode was designed, so that
software could do something standard and sane with text which needs to
be readable but not exquisitely crafted literary works.  No?  If you
want beauty, then use a markup language.

 > Right, but I was talking about my python3 example, where I was using
 > the email5 parser to (unsuccessfully) parse unicode.  *That's* the thing
 > email5 can't really handle, but email6 will be able to.

For email5 it would be an extension, yes, but I don't see why it would
be hard to handle Unicode input, assuming it's *really* Unicode,
unless you want to cater to "legacy" systems that might not understand
Unicode (or at least would prefer an alternative encoding).  Since
it's an extension, I don't think that's your problem, and the people
who would really like this extension (eg, the Japanese) are used to
dealing with mojibake issues.  (Of course, as an extension, you don't
need to do it at all.  This is just speculation.)

The problem would be with careless clients of email5 that find a way
to hand it bogus Unicode (eg, by inappropriately using the latin-1
codec to get a binary represention of their bytes in Unicode), but I'm
not sure how big a problem that would be.

 > Thank you very much for this piece of perspective.  I hadn't thought
 > about it that clearly before, but what you say makes perfect sense to me,
 > and is in fact the implicit perspective I've been working from when
 > working on the email6 stuff.

You're welcome, of course, and it makes me feel much better about
email6.  (Not that I had any real worries, but here we are about
halfway up a 100m cliff, and the trail just widened from 20cm to
2m. :-)




----------------------------------------
Subject:
[Python-Dev] Patch making the current email package
	(mostly)	support bytes
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
R. David Murray writes:
 > On Sat, 09 Oct 2010 01:06:29 +0900, "Stephen J. Turnbull" <stephen at xemacs.org> wrote:
 > > That mess is entirely unnecessary in Python 3.  Text and wire format
 > > can be easily distinguished with three different representations of
 > > email: Unicode for the conceptual RFC 822 layer (of course this is an
 > > extension, because RFC 822 itself is strictly limited to the ASCII
 > > subset), bytes for wire format, and Message objects for modern
 > > structured mail (including MIME, etc).

 > That engineering is pretty much what we are looking at, although in
 > practice I think you have to hang wire-format and text-format bits off
 > of appropriate places in the model in order to keep everything properly
 > coordinated.

Right.  That's where I was going with my comment to Barry about the
Received headers.  Even if email isn't going to serve clients working
with wire format, it needs to deal with those headers.  But where I
think the headers defined by RFC 822 should be stored as str in
email6, I am leaning toward storing Received headers verbatim as bytes
(including any RFC 822 folding whitespace) because of the RFC 5321
requirement that they be preserved exactly.




----------------------------------------
Subject:
[Python-Dev] Patch making the current email package
	(mostly)	support bytes
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Steven D'Aprano writes:

 > I don't think anyone has ever suggested change for change's sake. If 
 > they have, I'd love to read the PEP for it.

Not to mention the BDFL's pronouncement message!<wink>

