
============================================================================
Subject: [Python-Dev] PEP 1 updated to reflect current practices
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 1 updated to reflect current practices
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
I just pushed an update to PEP 1 to give additional guidance to core
developers that are directly updating a PEP in Mercurial, to account
for the automatic generation of PEP 0 and to mention the "PEP czar"
role.

Updated PEP:
http://www.python.org/dev/peps/pep-0001/

Changes:
http://hg.python.org/peps/rev/bdbbd3ce97d9

Any additional feedback here (I'll leave the issue open for a while):
http://bugs.python.org/issue14703

(although remember that the bar for this PEP is "useful and fairly
accurate" rather than "perfect")

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

