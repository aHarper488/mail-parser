
============================================================================
Subject: [Python-Dev] Daemon creation code in the standard library
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Daemon creation code in the standard library
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Ben Finney <ben+python at benfinney.id.au> writes:


At PyPI <URL:http://pypi.python.org/pypi/python-daemon/>, and
development co-ordinated at Alioth
<URL:https://alioth.debian.org/projects/python-daemon/>.


The correct link for the ?python-daemon-devel? forum is
<URL:http://lists.alioth.debian.org/cgi-bin/mailman/listinfo/python-daemon-devel>.
For announcements only, we have 
<URL:http://lists.alioth.debian.org/cgi-bin/mailman/listinfo/python-daemon-announce>.

-- 
 \        ?This sentence contradicts itself ? no actually it doesn't.? |
  `\                                               ?Douglas Hofstadter |
_o__)                                                                  |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Daemon creation code in the standard library
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Antoine Pitrou <solipsis at pitrou.net> writes:


Cameron Simpson <cs at zip.com.au> writes:



Thanks folks. We'd love to have this discussion over at the
?python-daemon-devel? discussion forum if you want to have it in more
detail.

-- 
 \     ?We are all agreed that your theory is crazy. The question that |
  `\      divides us is whether it is crazy enough to have a chance of |
_o__)            being correct.? ?Niels Bohr (to Wolfgang Pauli), 1958 |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Daemon creation code in the standard library
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 25 Jul, 2013, at 4:18, Ben Finney <ben+python at benfinney.id.au> wrote:


At first glance the library appears to close all open files, with an option
to exclude some specific file descriptors (that is, you need to pass a list
of files that shouldn't be closed). 

That makes it a lot harder to do some initialization before daemonizing.
I prefer to perform at least some initialization early in program startup to
be able to give sensible error messages. I've had too many initscripts that
claimed to have started a daemon sucessfully, only to have that daemon stop
right away because it noticed a problem right after it detached itself.

Ronald





----------------------------------------
Subject:
[Python-Dev] Daemon creation code in the standard library
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Fri, 26 Jul 2013 09:38:10 +0200
Ronald Oussoren <ronaldoussoren at mac.com> wrote:

Indeed, it's annoying when you want to setup logging before
daemonization starts. I had to hack my way through logging handlers to
find the fd I had to keep open.


Agreed.

Regards

Antoine.



