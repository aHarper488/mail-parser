
============================================================================
Subject: [Python-Dev] cpython: Issue #11377: platform.popen() emits a
	DeprecationWarning
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #11377: platform.popen() emits a
	DeprecationWarning
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 24.05.2011 00:17, victor.stinner wrote:

Please see http://mail.python.org/pipermail/python-dev/2011-May/111303.html
about the style of your commit messages. 9a16fa0c9548 is another example.

Georg


