
============================================================================
Subject: [Python-Dev] Introductions
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Introductions
----------------------------------------
Author: Ross Lagerwal
Attributes: []Content: 
Hi,

I have been offered commit rights for Python after making a few patches
on subprocess and the os module.

Antoine suggested that I should introduce myself on the python-dev list
so here we go:

I am a student from South Africa and decided to do some work on Python
in my spare time. I think I chose the Python project because it
definitely seems to be one of the friendliest projects around and the
easiest to get started with - the dev guide helps a lot! Also, with the
big standard library, there is place for people with all different
levels of experience to contribute.

Cheers
Ross




----------------------------------------
Subject:
[Python-Dev] Introductions
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Welcome Ross! Glad you like the new dev guide. (Brett and the
community should be proud of their recent work on that.) Remember,
there are no stupid questions. And while occasionally it may appear as
if someone tries to bite your head off, our bite is not as bad as our
bark.

--Guido

On Sat, Mar 12, 2011 at 9:41 AM, Ross Lagerwall <rosslagerwall at gmail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Introductions
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 3/12/2011 9:41 AM, Ross Lagerwall wrote:

Great to have you with us!

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Introductions
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <1299940862.1632.7.camel at hobo>,
 Ross Lagerwall <rosslagerwall at gmail.com> wrote:

Welcome, Ross!  2011 is turning out to be a good year for new 
committers, if I do say so myself (and being one myself).  I hope you 
get the opportunity to meet others here in person, too, to hear and see 
our barks.

It was good to meet some more of you all in person, if all too briefly, 
at the language summit on Thursday.  Alas, I was unable to stay for the 
rest of PyCon but Ronald and I had a chance to spend some quality time 
together again brainstorming issues in the OS X area.  Onward!

-- 
 Ned Deily,
 nad at acm.org


