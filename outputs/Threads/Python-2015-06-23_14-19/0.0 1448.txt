
============================================================================
Subject: [Python-Dev] Mirroring Python repos to Bitbucket
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Mirroring Python repos to Bitbucket
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 13 Aug 2011 19:08:40 -0400
Doug Hellmann <doug.hellmann at gmail.com> wrote:

There is already an RSS feed at http://hg.python.org/cpython/rss-log
Another possibility is the gmane mirror of python-checkins, which has
its own RSS feed: http://rss.gmane.org/gmane.comp.python.cvs

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Mirroring Python repos to Bitbucket
----------------------------------------
Author: Doug Hellman
Attributes: []Content: 

On Aug 13, 2011, at 7:23 PM, Antoine Pitrou wrote:


Thanks for the tip, I didn't know about either of those.

Doug


