
============================================================================
Subject: [Python-Dev] caching in the stdlib? (was: New regex module for 3.2?)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] caching in the stdlib? (was: New regex module for 3.2?)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
R. David Murray, 28.07.2010 03:43:

What about actually putting it visibly into the stdlib? Except for files, I 
didn't see much about caching there, which seems like a missing battery to 
me. Why not do it as with the collections module and add stuff as it comes in?

Stefan


