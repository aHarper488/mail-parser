
============================================================================
Subject: [Python-Dev] [help wanted] - IrDA sockets support
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [help wanted] - IrDA sockets support
----------------------------------------
Author: =?ISO-8859-1?Q?Charles=2DFran=E7ois_Natali?
Attributes: []Content: 
Hi,

Issue #1522400 (http://bugs.python.org/issue1522400) has a patch
adding IrDA socket support.
It builds under Linux and Windows, however it cannot go any further
because no developer involved in the issue has access to IrDA capable
devices, which makes testing impossible.
So, if you have access to such devices and are interested, feel free
to chime in and help get this merged.

Cheers,

cf

