
============================================================================
Subject: [Python-Dev] moving issues from argparse tracker to python tracker?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] moving issues from argparse tracker to python tracker?
----------------------------------------
Author: Steven Bethar
Attributes: []Content: 
Before I go and add about 30 open issues to the Python tracker, I
figured I should ask. What's the normal process for the bug trackers
of modules that move to the standard library? I have a few feature
requests, etc. for argparse, and I was planning to just copy them over
to the Python bug tracker (and close them on the Google code tracker).
Is this what people normally do? (It should be easy enough to do - I
just don't want to mess up the tracker if this is usually done some
other way.)

Thanks,

Steve
-- 
Where did you get that preposterous hypothesis?
Did Steve tell you that?
        --- The Hiphopopotamus

