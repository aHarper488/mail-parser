
============================================================================
Subject: [Python-Dev] .hgignore (was: Mercurial conversion repositories)
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] .hgignore (was: Mercurial conversion repositories)
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Fri, 04 Mar 2011 13:01:02 -0800, Santoso Wijaya <santoso.wijaya at gmail.com> wrote:

I, on the other hand, would like to see .rej and .orig removed from
the ignore list.  I don't like having these polluting my working
directory, and 'hg status' is the easiest way to find them (if
they aren't ignored).

Or if there's some way to configure my personal .hgrc to ignore
those particular ignore lines, that would be fine too :)

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] .hgignore (was: Mercurial conversion repositories)
----------------------------------------
Author: Tim Delane
Attributes: []Content: 
On 6 March 2011 00:44, R. David Murray <rdmurray at bitdance.com> wrote:



If those were to be removed from .hgignore then there would be a high
likelihood of someone doing "hg addremove" and inadvertently tracking them.
The purpose of .hgignore is to prevent inadventently tracking files that
shouldn't be tracked.

"hg status  -i" will list all ignored files that are present in your working
directory. For other options, "hg help status".

Tim Delaney
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110306/419c69ef/attachment.html>



----------------------------------------
Subject:
[Python-Dev] .hgignore (was: Mercurial conversion repositories)
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 06 Mar 2011 00:54:39 +1100, Tim Delaney <timothy.c.delaney at gmail.com> wrote:

Ah, well, I don't like that UI.  The purpose for me of .hgignore (and
similar ignore files) is to make the status command show any files that
have been modified or aren't normal build/run products.  I'd rather add
and remove files individually by hand (except when adding or removing
a directory).  I also want a --strict option for the commit command
that refuses to commit if there are unignored unadded or missing files.
(--strict is the bzr spelling; I don't care about the spelling :)


hg status -i is useless because there are a *lot* of ignored files in
a working directory where python has been built.  I'd have to do a
distclean first, which would mean I'd have to do a rebuild after...and
all of that just takes too long :)

I guess I have some hg hacking in my future, unless someone has already
written extensions for this stuff.

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] .hgignore (was: Mercurial conversion repositories)
----------------------------------------
Author: Daniel Stutzbac
Attributes: []Content: 
On Sat, Mar 5, 2011 at 5:54 AM, Tim Delaney <timothy.c.delaney at gmail.com>wrote:


If the goal is to prevent something from being committed, shouldn't the
check go in a pre-commit hook instead?

-- 
Daniel Stutzbach
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110305/c9f3cfe8/attachment.html>



----------------------------------------
Subject:
[Python-Dev] .hgignore (was: Mercurial conversion repositories)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 5 Mar 2011 08:36:04 -0800
Daniel Stutzbach <stutzbach at google.com> wrote:


Well, it's more user-friendly to help "hg addremove" work as expected,
rather than add hooks down the line to forbid pushing any mistakes.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] .hgignore (was: Mercurial conversion repositories)
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Mar 5, 2011, at 8:44 AM, Antoine Pitrou wrote:


I concur.


Raymond


