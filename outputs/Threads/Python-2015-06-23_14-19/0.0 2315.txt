
============================================================================
Subject: [Python-Dev] PhD ideas
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PhD ideas
----------------------------------------
Author: Tiago Boldt Sous
Attributes: []Content: 
Hi,

I'm now currently finishing my MsC and am thinking about enrolling
into the PhD program. I was wondering if any of you would like to
suggest me some research topic that could benefit the scientific
community, that might also result as a potential improvement for
Python.

I love everything that's web related (Django here) and software
engineering but I?don't yet have any idea for a research topic that
would be relevant?for a PhD so I'm completely open to suggestions.

Please contact me directly.

Best regards

--
Tiago Boldt Sousa

