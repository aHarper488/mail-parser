
============================================================================
Subject: [Python-Dev] distutils and distutils2 bugs (Was: Re: Goodbye)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] distutils and distutils2 bugs (Was: Re: Goodbye)
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Le 23/09/2010 22:51, ?ric Araujo a ?crit :

There has been no further feedback after Georg (thanks Georg), so I went
ahead and set ?3rd party? for distutils2 bugs instead of 2.5-3.2, for
all the aforementioned reasons.  Bugs applying to distutils and
distutils2 have versions 2.7, 3.1, 3.2, 3rd party so that the
forward-port is not forgotten.

If you commit a change to distutils, sysconfig or pkgutil, it?s fine to
assign the forward-port to me or, if you can use Mercurial, to publish a
changeset somewhere and request a pull/import.

(Those versions changes sent a lot of email, but this had to be done.
Sorry for the inconvenience.)

Thanks again to Terry and Mark for their triage work.  Hope this helps!

Kind regards

