
============================================================================
Subject: [Python-Dev] Anyone building Python --without-doc-strings?
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Antoine Pitrou <solipsis at pitrou.net> wrote:

I'm not sure how accurate the output is for measuring these things, but
according to ``ls'' and ``du'' the option is indeed quite worthless:

./configure CFLAGS="-Os -s" LDFLAGS="-s" && make

$ du -h build/lib.linux-x86_64-3.4/
24K     build/lib.linux-x86_64-3.4/__pycache__
3.9M    build/lib.linux-x86_64-3.4/

$ ls -lh python
  1.8M Jan 26 16:36 python
$ ls -lh libpython3.4m.a
  9.6M Jan 26 16:36 libpython3.4m.a


===============================================================

./configure --without-doc-strings CFLAGS="-Os -s" LDFLAGS="-s" && make

$ du -h build/lib.linux-x86_64-3.4/
24K     build/lib.linux-x86_64-3.4/__pycache__
3.8M    build/lib.linux-x86_64-3.4/

$ ls -lh python
  1.6M Jan 26 16:33 python
$ ls -lh libpython3.4m.a
  9.4M Jan 26 16:33 libpython3.4m.a



Stefan Krah






----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Stefan Krah <stefan at bytereef.org> wrote:

The original contribution *was* in fact aiming for "10% smaller", see:

http://docs.python.org/release/2.3/whatsnew/node20.html


So apparently people thought it was useful.



Stefan Krah





----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sat, 26 Jan 2013 17:03:59 +0100
Stefan Krah <stefan at bytereef.org> wrote:

After a bit of digging, I found the following discussions:
http://mail.python.org/pipermail/python-dev/2001-November/018444.html
http://mail.python.org/pipermail/python-dev/2002-January/019392.html
http://bugs.python.org/issue505375

Another reason for accepting the patch seemed to be that it introduced
the Py_DOCSTR() macros, which were viewed as helpful for other reasons
(some people talked about localizing docstrings).

I would point out that if 200 KB is really a big win for someone, then
Python (and especially Python 3) is probably not the best language for
them.

It is also ironic how the executable size went up since then (from 0.6
to more than 1.5 MB) :-)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sat, 26 Jan 2013 17:19:32 +0100, Antoine Pitrou <solipsis at pitrou.net> wrote:

200K can make a difference.  It does on the QNX platform, for example,
where there is no virtual memory.  It would be nice to reduce that
executable size, too....but I'm not volunteering to try (at least not
yet) :)

--David



----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: =?iso-8859-1?Q?Kristj=E1n_Valur_J=F3nsson?
Attributes: []Content: 
We (CCP) are certainly compiling python without docstrings for our embedded platforms (that include the PS3)
Anyone using python as en engine to be used by programs and not users will appreciate the deletion of unneeded memory.
K

-----Original Message-----
From: Python-Dev [mailto:python-dev-bounces+kristjan=ccpgames.com at python.org] On Behalf Of R. David Murray
Sent: 27. jan?ar 2013 00:38
To: python-dev at python.org
Subject: Re: [Python-Dev] Anyone building Python --without-doc-strings?

On Sat, 26 Jan 2013 17:19:32 +0100, Antoine Pitrou <solipsis at pitrou.net> wrote:

200K can make a difference.  It does on the QNX platform, for example, where there is no virtual memory.  It would be nice to reduce that executable size, too....but I'm not volunteering to try (at least not
yet) :)

--David
_______________________________________________
Python-Dev mailing list
Python-Dev at python.org
http://mail.python.org/mailman/listinfo/python-dev
Unsubscribe: http://mail.python.org/mailman/options/python-dev/kristjan%40ccpgames.com





----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Why don't you compile using python -OO and distribute only .pyo code?

Victor

2013/1/27 Kristj?n Valur J?nsson <kristjan at ccpgames.com>:



----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sun, Jan 27, 2013 at 4:58 PM, Victor Stinner <victor.stinner at gmail.com>wrote:


Because .pyo files can be much larger than necessary, e.g. using my mnfy
project on decimal (with --safe-transforms) compared to -OO yields::

224K Lib/decimal.py
200K Lib/__pycache__/decimal.cpython-34.pyc
120K Lib/__pycache__/decimal.cpython-34.pyo
 80K decimal-mnfy.py

And before you ask, the bytecode is still larger on the minified source.

So if you truly want to shrink your binary plus overall memory usage
footprint you want to go beyond -OO sometimes (although it is a cheap, fast
way to save if you are not explicitly striving to eek out every byte).

-Brett

-Brett


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130127/9704332b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Anyone building Python --without-doc-strings?
----------------------------------------
Author: =?utf-8?B?S3Jpc3Rqw6FuIFZhbHVyIErDs25zc29u?
Attributes: []Content: 
We do that, of course, but compiling python without the doc strings removes those from all built-in modules as well.
That's quite a lot of static data.
K

-----Original Message-----
From: Victor Stinner [mailto:victor.stinner at gmail.com] 
Sent: 27. jan?ar 2013 21:58
To: Kristj?n Valur J?nsson
Cc: R. David Murray; python-dev at python.org
Subject: Re: [Python-Dev] Anyone building Python --without-doc-strings?

Why don't you compile using python -OO and distribute only .pyo code?

Victor

2013/1/27 Kristj?n Valur J?nsson <kristjan at ccpgames.com>:


