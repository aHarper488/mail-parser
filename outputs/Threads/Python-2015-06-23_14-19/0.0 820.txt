
============================================================================
Subject: [Python-Dev]  urlparse.urlunsplit should be smarter about +
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev]  urlparse.urlunsplit should be smarter about +
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
David Abrahams writes:
 > 
 > This is a bug report.  bugs.python.org seems to be down.
 > 
 >   >>> from urlparse import *
 >   >>> urlunsplit(urlsplit('git+file:///foo/bar/baz'))
 >   git+file:/foo/bar/baz
 > 
 > Note the dropped slashes after the colon.

That's clearly wrong, but what does "+" have to to do with it?  AFAIK,
the only thing special about + in scheme names is that it's not
allowed as the first character.

