
============================================================================
Subject: [Python-Dev] ints not overflowing into longs?
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Derek Shocke
Attributes: []Content: 
I just found an unexpected behavior and I'm wondering if it is a bug.
In my 2.7.2 interpreter on OS X, built and installed via MacPorts, it
appears that integers are not correctly overflowing into longs and
instead are yielding bizarre results. I can only reproduce this when
using the exponent operator with two ints (declaring either operand
explicitly as long prevents the behavior).

0
1267650600228229401496703205376L

-2101438300051996672
104857600000000000000000000L

7766279631452241920
100000000000000000000L

To confirm I'm not crazy, I tried in the 2.7.1 and 2.6.7 installations
included in OS X 10.7, and also a 2.7.2+ (not sure what the + is) on
an Ubuntu machine and didn't see this behavior. This looks like some
kind of truncation error, but I don't know much about the internals of
Python and have no idea what's going on. I assume since it's only in
my MacPorts installation, it must be build configuration issue that is
specific to OS X, perhaps only 10.7, or MacPorts.

Am I doing something wrong, and is there a way to fix it before I
compile? I could find any references to this problem as a known issue.

Thanks,
Derek



----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Apparently Macports is still using a buggy compiler. I reported a
similar issue before and got this reply from Ned Delly:

"""
Thanks for the pointer.  That looks like a duplicate of Issue11149 (and
Issue12701).  Another manifestation of this was reported in Issue13061
which also originated from MacPorts.  I'll remind them that the
configure change is likely needed for all Pythons.  It's still safest to
stick with good old gcc-4.2 on OS X at the moment.
"""

(Those issues are on bugs.python.org.)

--Guido

On Wed, Nov 2, 2011 at 7:32 PM, Derek Shockey <derek.shockey at gmail.com> wrote:



-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Derek Shocke
Attributes: []Content: 
Thank you, I narrowed it down from there and got a properly working
build. I gather the problem is that in Xcode 4.2 the default compiler
was changed to clang, but the version of clang bundled with it has a
bug that breaks overflows in intobject.c.

In case anyone else hits this, I fixed this in MacPorts by forcing it
to use gcc. Edit the portfile (port edit python27) and add this
anywhere after the 5th or so line:
configure.compiler  llvm-gcc-4


-Derek

On Wed, Nov 2, 2011 at 7:41 PM, Guido van Rossum <guido at python.org> wrote:



----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 2 Nov 2011 19:41:30 -0700
Guido van Rossum <guido at python.org> wrote:

If I understand things correctly, this is technically not a buggy
compiler but Python making optimistic assumptions about the C standard.
(from issue11149: "clang (as with gcc 4.x) assumes signed integer
overflow is undefined. But Python depends on the fact that signed
integer overflow wraps")

I'd happily call that a buggy C standard, though :-)

Regards

Antoine.







----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le Mercredi 2 Novembre 2011 19:32:38 Derek Shockey a ?crit :

This issue has already been fixed twice in Python 2.7 branch: int_pow() has 
been fixed and -fwrapv is now used for Clang.

http://bugs.python.org/issue11149
http://bugs.python.org/issue12973

It is maybe time for a new release? :-)

Victor



----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi Derek,


The + means that?s it?s 2.7.2 + some commits, in other words the
in-development version that will become 2.7.3.  This bit of info seems
to be missing from the doc.

Regards



----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Derek Shocke
Attributes: []Content: 
I believe you're right. The 2.7.2 MacPorts portfile definitely passes
the -fwrapv flag to clang, but the bad behavior still occurs with
exponents. I verified the current head of the 2.7 branch does not have
this problem when built with clang, so I'm assuming that issue12973
resolved this with a patch to int_pow() and that it will be out in the
next release.

-Derek

On Thu, Nov 3, 2011 at 4:30 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:



----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Stefan Kra
Attributes: []Content: 
Derek Shockey <derek.shockey at gmail.com> wrote:

Really? Even without the fix for issue12973 the -fwrapv flag
should be sufficient, as reported in issue13061 and Issue11149.

For clang version 3.0 (trunk 139691) on FreeBSD this is the case.


Stefan Krah





----------------------------------------
Subject:
[Python-Dev] ints not overflowing into longs?
----------------------------------------
Author: Derek Shocke
Attributes: []Content: 
You're right; among my many tests I think I muddled the situation with
a stray CFLAGS variable in my environment. Apologies for the
misinformation. The current MacPorts portfile does not add -fwrapv.
Adding -fwrapv to OPT in the Makefile solves the problem. I confirmed
by manually building the v2.7.2 tag with clang and -fwrapv, and the
overflow behavior is correct. I've notified the MacPorts package
maintainer.


-Derek

On Thu, Nov 3, 2011 at 11:07 AM, Stefan Krah <stefan at bytereef.org> wrote:

