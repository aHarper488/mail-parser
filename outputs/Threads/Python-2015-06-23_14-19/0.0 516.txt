
============================================================================
Subject: [Python-Dev] Mercurial migration readiness (was: Taking over the
	Mercurial Migration)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Mercurial migration readiness (was: Taking over the
	Mercurial Migration)
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
After reading PEP 384 and PEP 385 (finally) I got a strong impression
that they are not ready for the change (read below the line for
details), because they do not propose any workflow. So, instead of
rushing with migration I'd like to propose incremental change rather
than revolutionary that will allow people to taste what it like to
work with Mercurial and update all relevant papers on the website.

I propose to setup workflow that will include pulling changes to
Mercurial master clone from current Subversion repository and pushing
them back to Subversion using Mercurial. This will be similar to
http://mercurial.selenic.com/wiki/DeveloperRepos scheme, but with
hg-stable replaced with svn.

I am willing to collaborate for getting this system in place before
EuroPython even if I won't find a chance to get there.


--- the line ---

I didn' t have time to follow Mercurial discussion, so I expected PEP
384 and PEP 385 to be exhaustive reference on both the migration and
workflow before and after the switch. Unfortunately, both are too
vague in this respect and my opinion that migration now will do more
harm than good.


Primary concern is that will happen with central Subversion
repository. There are a plenty of private tools and automated scripts
that were written to work with central Subversion repository, so it is
important that central Subversion repository continue to operate and
be synchronized.

However, PEP 385 "The Future of Subversion" [1] does not propose any
way to keep Subversion repository in sync. "it will probably live on
for a bit" phrase looks like Subversion will be immediately dead after
switching.


The second concern is workflow. Even after getting my hands dirty I
still can't find the best solution for working with Mercurial copy of
Python. In Subversion I do shallow checkouts of interesting features
when I run into one bug or another. Not all patches are ready, so they
stay in those checkouts as uncommitted diffs. To be prepared for
conflicts I review code with `svn log -vr base:head` before updating.
But with Mercurial I see two major problems with my workflow (which I
am unlikely to change for the next few weeks due to heavy automation):
1. No shallow checkouts - that means that I have to copy 200Mb+ for
every patch I have
2. No `hg log` command to see changes in my branch from remote
repository before fetching them
(I used to invoke `hg incoming`, but ended up with fetching everything
with `hg pull` and then using `hg log -vr .:tip`, because `hg inc`
didn't show changes that were already pulled, but not updated in wc,
and `hg log` could not show incoming)
3. There is no clear branch separation in my working copy. I don't
know how to diff files in different branches without copying 200Mb+
clone. I don't know how to diff my files with remote repository
without pulling it.
4. Even if I make patch in my Mercurial clone - you still can't pull
it and I have to attach it to tracker. No gain.

So from these PEPs I can't really see how Mercurial solves problems
with development. I can feel that it doesn't solve my problems, but I
see no way where I can submit my user story.


I would put accent on keeping mirror of Subversion as easy way to
contribute for those who are not yet ready for DVCS. Subversion also
provides greater interoperability. Assuming that any modern DVCS tool
may act as Subversion client, we will gain more contributors if we
won't try to force people use Python and Mercurial.

[1] http://www.python.org/dev/peps/pep-0385/#the-future-of-subversion
-- 
anatoly t.

