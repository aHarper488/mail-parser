
============================================================================
Subject: [Python-Dev] opinions on issue2142 ('\ No newline at end of
 file' to difflib.unified_diff)?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] opinions on issue2142 ('\ No newline at end of
 file' to difflib.unified_diff)?
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Thu, Oct 7, 2010 at 00:53, Trent Mick <trentm at gmail.com> wrote:

Mark (in the issue) argues that there is no specification for diffs,
and that this is thus a feature, not a bug. On the other hand, in
Mercurial we've maintained the idea that diffs are specified by
whatever (GNU) patch(1) accepts. So I would support treating this as a
bug, not just a feature. As such, I think 3.2 should emit the extra
line by default and add a keyword argument to make it easy to revert
to the old behavior (and add docs to 2.7, 3.1 and 3.2 about the
issue!).

Cheers,

Dirkjan

