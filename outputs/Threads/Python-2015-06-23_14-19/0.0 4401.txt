
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.3): #17973: Add FAQ
 entry for ([], )[0] += [1] both extending and raising.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): #17973: Add FAQ
 entry for ([], )[0] += [1] both extending and raising.
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, May 21, 2013 at 12:35 AM, r.david.murray
<python-checkins at python.org> wrote:

Yay for having this in the FAQ, but...


For the immutable case, this expansion is incorrect:

False

With immutable objects, += almost always expands to:


(For containers that support binary operators, the presence of the
relevant __i*__ methods is actually a reasonable heuristic for
distinguishing the mutable and immutable versions)

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia

