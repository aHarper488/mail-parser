
============================================================================
Subject: [Python-Dev] Include datetime.py in stdlib or not?
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
This idea has been discussed extensively in this and other forums and
I believe it is time to make a decision.

The proposal is to add pure python implementation of datetime module
to stdlib.   The current C implementation will transparently override
pure python definitions in CPython.  Other python implementations will
have an option of supplying their own fast implementation.  This
approach has already been adopted by several modules including pickle,
heapq and warnings.   It has even been suggested [1] that this is the
direction in which the majority of CPython extension modules should be
heading.

 This proposal has brought mostly positive feedback on the tracker [2]
with only a few objections being raised.

1. Since this does not bring any new functionality and datetime module
is not expected to evolve, there is no need for pure python version.
2. There are other areas of stdlib that can benefit more from pure
python equivalents.
3. Reference implementations should be written by a senior CPython
developer and not scraped from external projects like PyPy.

Let me briefly address these objections:

1. Availability of pure python equivalents of standard library modules
is very helpful for debugging python applications. This is
particularly true when the stdlib module is designed to be extendable
by and calls into user-supplied code.  This is true in the case of
datetime module which relies on 3rd-party or user-supplied code for
any timezone support.

The datetime module indeed saw very little development in the last 6
years.   However this lack of development may itself be the result of
pure python version not being available.  For example, the idea to
supply a concrete tzinfo object representing UTC has been brought up
back in 2002. [3]  An RFE [4] was created in the tracker in January,
2009 and took more than 1.5 years to implement.  If you look at the
history of issue5094, you will see that development slowed down
considerably when C coding started.  Note that for this particular
feature, there was probably no need to have it implemented in C to
begin with.  (Most common operations involve datetime objects in the
same timezone and those don't need to call timezone methods.)

2. Unlike other areas of stdlib, datetime module was originally
prototyped in python and it turns out that it hardly changed between
python 2.3 and 2.6 with a couple of features added in 2.7.  A port to
3.x was uneventful as well.

3. The version of datetime.py [5] that I propose for inclusion is
substantially the pure python prototype written by Tim Peters and
others back in 2003.  The PyPy changes are very few [6].

I believe the code is substantially ready for inclusion.  There are a
few items that need to be fixed related to how floating point
arguments to timedelta are handled, as well as some clean-up of
docstrings and error messages (both C and python implementations can
see some improvement in this area).  The biggest item in terms of
development effort would be to refactor  test_datetime to test both
implementations.  A simple solution [7] of importing test_datetime
twice with and without _datetime will probably not be accepted because
it is not compatible with alternative unittest runners.

What do you think?  Please reply here or add a comment at
http://bugs.python.org/issue7989.

[1] http://bugs.python.org/issue5094#msg106498
[2] http://bugs.python.org/issue7989
[3] http://www.zope.org/Members/fdrake/DateTimeWiki/SuggestedRequirements
[4] http://bugs.python.org/issue5094
[5] http://svn.python.org/view/*checkout*/sandbox/branches/py3k-datetime/datetime.py
[6] http://bugs.python.org/file17701/datetime-sandbox-pypy.diff
[7] http://bugs.python.org/file17848/issue7989.diff



----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Jul 6, 2010 at 12:59, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

I should mention that PyPy has said they are quite happy to donate
their datetime implementation which is what Alexander (I believe) has
been working off of.

Also, adding a pure Python version alleviates the need of the other
VMs from having to maintain the same module separately. Making the
stdlib shareable (and thus eventually breaking it out from CPython)
was discussed at the language summit at PyCon 2010 and generally
agreed upon, and this is a step towards making that happen.

-Brett





----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Jul 7, 2010 at 5:59 AM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

(For those that haven't read the tracker discussion, it's long, but
worth skimming to get a better idea of the various points of view).

+1 on the general idea, but I haven't looked at the patches in order
to be able to comment on the specifics (except that following any of
the test_warnings, test_heapq, test_pickle, test_io, etc. styles of
testing parallel implementations should be fine).

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 7/6/2010 3:59 PM, Alexander Belopolsky wrote:

I am more interested in Brett's overall vision than this particular 
module. I understand that to be one of a stdlib that is separate from 
CPython and is indeed the standard Python library.

Questions:

!. Would the other distributions use a standard stdlib rather than 
current individual versions? If so, and if at least one used the Python 
version of each module, this would alleviate the concern that non-use == 
non-testing. (Test improvement would also help this.)

2. Would the other distributions pool their currently separate stdlib 
efforts to help maintain one standard stdlib. If so, this would 
alleviate the concern about the extra effort to maintain both a C and 
Python version. (Test improvement would also help this also.)

3. What version of Python would be allowed for use in the stdlib? I 
would like the stdlib for 3.x to be able to use 3.x code. This would be 
only a minor concern for CPython as long as 2.7 is maintained, but a 
major concern for the other implementation currently 'stuck' in 2.x 
only. A good 3to2 would be needed.

I generally favor having Python versions of modules available. My 
current post on difflib.SequenceMatcher is based on experiments with an 
altered version. I copied difflib.py to my test directory, renamed it 
diff2lib.py, so I could import both versions, found and edited the 
appropriate method, and off I went. If difflib were in C, my post would 
have been based on speculation about how a fixed version would operate, 
rather than on data.

4. Does not ctypes make it possible to replace a method of a 
Python-coded class with a faster C version, with something like
   try:
     connect to methods.dll
     check that function xyx exists
     replace Someclass.xyy with ctypes wrapper
   except: pass
For instance, the SequenceMatcher heuristic was added to speedup the 
matching process that I believe is encapsulated in one O(n**2) or so 
bottleneck method. I believe most everything else is O(n) bookkeeping.



see above


Possibly true, but developers do what they do, and this seems mostly done.


I did not see that im my reading of the thread. In any case, what 
matters is quality, not authorship.

 > What do you think?  Please reply here or add a comment at
 > http://bugs.python.org/issue7989.

 From scanning that and the posts here, it seems like a pep or other doc 
on dual version modules would be a good idea. It should at least 
document how to code the switch from python version to the x coded 
version and how to test both, as discussed.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Tue, Jul 6, 2010 at 11:54 PM, Terry Reedy <tjreedy at udel.edu> wrote:
[.. skipping more general stdlib discussion see "Python equivalents in
stdlib" thread ..]


The reason I want to do datetime module is that there are some
long-standing bugs/RFEs that would require some experimentation to get
it right.  Such experimentation is unfeasible in C where more effort
goes into thinking about integer overflow and reference counting than
into actual design.  Here are some of those issues:

http://bugs.python.org/issue5516 = equality not symmetric for
subclasses of datetime.date and datetime.datetime
http://bugs.python.org/issue2736 = datetime needs an "epoch" method
http://bugs.python.org/issue7584 = datetime.rfcformat() for Date and
Time on the Internet
http://bugs.python.org/issue1100942 = Add datetime.time.strptime and
datetime.date.strptime
http://bugs.python.org/issue8860 = Rounding in timedelta constructor
is inconsistent with that in timedelta arithmetics
http://bugs.python.org/issue1647654 = No obvious and correct way to
get the time zone offset
http://bugs.python.org/issue5288 = tzinfo objects with sub-minute
offsets are not supported (e.g. UTC+05:53:28)
http://bugs.python.org/issue1982 = Feature: extend strftime to accept
milliseconds

This POV was brought up in the #python-dev IRC channel.


I completely agree and I think the sooner the python code gets into
the main tree the more reviews it will get before the next release.



----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Tue, Jul 6, 2010 at 20:54, Terry Reedy <tjreedy at udel.edu> wrote:

The idea is that the stdlib just becomes a subrepo that the other VMs
simply pull in to gain their version of the stdlib.


That's the idea. We already have contributors from the various VMs who
has commit privileges, but they all work in their own repos for
convenience. My hope is that if we break the stdlib out into its own
repository that people simply pull in then other VM contributors will
work directly off of the stdlib repo instead of their own, magnifying
the usefulness of their work.


This will only affect py3k.


The effect upon CPython would be the extension modules become just
performance improvements, nothing more (unless they have to be in C as
in the case for sqlite3).


There is no need to go that far. All one needs to do is structure the
extension code such that when the extension module is imported, it
overrides key objects in the Python version. Using ctypes is just
added complexity.


Franke Wierzbicki and I started such a PEP, but we both got busy with
other stuff. And since I am most likely going to be the one
spearheading this on the CPython side this will most likely not move
forward until I have time to get to it (which might be quite a while).

-Brett




----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 7/7/2010 3:32 PM, Brett Cannon wrote:


I was wondering if you had more than 'hope', but thinking about it now, 
I think it premature to ask for commitments. Once a Python3 stdlib hg 
subrepository is set up and running, the logic of joining in should be 
obvious -- or not.

I am now seeing that a more complete common Python-level test suite is 
also important. Being able to move Python code, that only uses the 
stdlibk,between implementations and have it just work would be good for 
all of them.


Good. The Python3 stdlib should gradually become modern Python3 code. 
(An example archaism -- the use in difflib of dicts with arbitrary 
values used as sets -- which I plan to fix.)


As pre- and jit compilation improve, the need for hand-coded C will go 
down. For instance, annotate (in a branch, not trunk) and compile with 
Cython.



Is it possible to replace a python-coded function in a python-coded 
class with a C-coded function? I had the impression from the issue 
discussion that one would have to recode the entire class, even if only 
a single method really needed it.


Only to be used if easier than extra C coding.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Jul 7, 2010 at 15:17, Terry Reedy <tjreedy at udel.edu> wrote:

I can say that all the VM representatives have all said they like the idea.

-Brett





----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Thu, Jul 8, 2010 at 3:09 PM, Brett Cannon <brett at python.org> wrote:
..

This is encouraging.  Here is an update on the status of datetime.py.
 I believe it is mostly ready to move from sandbox to py3k/Lib.  The
patch is available on the tracker at
http://bugs.python.org/file17978/issue9206b.diff and on Rietveld at
http://codereview.appspot.com/1824041 . Unfortunately since many
changes are just file renames will minor code modifications, neither
the patch nor Rietveld give a good overview of the proposed commit.  I
will try to give this overview here:

1. Copy datetime.py from sandbox to py3k/Lib.  (I need some help from
an SVN expert on how to do that.)  The diff between sandbox version
and proposed commit is just

========================================================
--- ../py3k-datetime/datetime.py	2010-07-07 20:12:56.000000000 -0400
+++ Lib/datetime.py	2010-07-08 21:15:52.000000000 -0400
@@ -1555,7 +1555,6 @@
     @classmethod
     def strptime(cls, date_string, format):
         'string, format -> new datetime parsed from a string (like
time.strptime()).'
-        import _strptime
         return _strptime._strptime_datetime(cls, date_string, format)

     def utcoffset(self):
@@ -1874,6 +1873,13 @@
 timezone.min = timezone(timezone._minoffset)
 timezone.max = timezone(timezone._maxoffset)

+try:
+    from _datetime import *
+except ImportError:
+    pass
+
+import _strptime
+
========================================================
The "from _datetime import *" is the standard fast implementation
override and "import _strptime" had to be moved from function level to
module level after class definitions due to circular dependency of
_strptime on datetime.

The best place to review the entire datetime.py is on Rietveld at
http://codereview.appspot.com/1824041/diff/1/4.  I have only one
remaining issue with this code - it leaves a number of "private" _xyz
functions in the datetime module that are neither used nor overridden
by the C implementation.   In my view, this is not a big issue, but it
can be dealt with by either moving module level functions to class
namespace (making them static or class methods as appropriate) or by
deleting them explicitly in the else clause of the try statement that
imports the fast overrides.

2. Rename datetimemodule.c to _datetimemodule.c. The code changes are
trivial: module name and the name of the init function.
http://codereview.appspot.com/1824041/diff/1/7

3. Rename test_datetime.py to datetimetester.py.  The testing strategy
implements Nick Coghlan's idea of importing the test classes from a
tester module with and without _datetime and injecting them into
test_datetime namespace under different names.
http://mail.python.org/pipermail/python-dev/2010-July/101598.html

The changes required to the code are minimal:
http://codereview.appspot.com/1824041/diff/1/5 .

4. Add new test_datetime.py that contains somewhat elaborate import
machinery to create duplicates of each test case from
datetimetester.py running with and without datetime acceleration.

Unfortunately, test_datetime.py did not make it to Rietveld, but it is
small enough to include here:

================================================
import unittest
import sys
from test.support import import_fresh_module, run_unittest
TESTS = 'test.datetimetester'
pure_tests = import_fresh_module(TESTS, fresh=['datetime', '_strptime'],
                                 blocked=['_datetime'])
fast_tests = import_fresh_module(TESTS, fresh=['datetime',
                                               '_datetime', '_strptime'])

test_modules = [pure_tests, fast_tests]
test_suffixes = ["_Pure", "_Fast"]

for module, suffix in zip(test_modules, test_suffixes):
    for name, cls in module.__dict__.items():
        if isinstance(cls, type) and issubclass(cls, unittest.TestCase):
            name += suffix
            cls.__name__ = name
            globals()[name] = cls
            def setUp(self, module=module, setup=cls.setUp):
                self._save_sys_modules = sys.modules.copy()
                sys.modules[TESTS] = module
                sys.modules['datetime'] = module.datetime_module
                sys.modules['_strptime'] = module.datetime_module._strptime
                setup(self)
            def tearDown(self, teardown=cls.tearDown):
                teardown(self)
                sys.modules.__init__(self._save_sys_modules)
            cls.setUp = setUp
            cls.tearDown = tearDown

def test_main():
    run_unittest(__name__)

if __name__ == "__main__":
    test_main()
================================================

Since this change does not introduce any new features, I think it is
safe to commit it and make further improvements to datetime.py (if
any) once it is in the main tree.



----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
On Tue, Jul 13, 2010 at 3:52 PM, Alexander Belopolsky
<alexander.belopolsky at gmail.com> wrote:

This turned out to be not such a great idea.  Importing _strptime at
the module level even after all datetime initializations does not work
because of another circular dependency: _strptime imports calendar
which imports calendar which imports datetime.  New patch is at
http://bugs.python.org/file17978/issue9206c.diff



----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Alexander Belopolsk
Attributes: []Content: 
Thanks, everyone who followed up here and on the tracker.  I am
readying the patch for check in, but as I look back through the
messages, I don't really see anyone's answer to the question in the
subject:

*  Include datetime.py in stdlib or not?

I hope this means an implied "yes, include."  Since committing the
patch will case a bit of file movement, I would like to pre-announce
the commit in case my timing is not right.

Here is the link to the issue: http://bugs.python.org/issue7989 .



----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/7/23 Alexander Belopolsky <alexander.belopolsky at gmail.com>:

It's a bit hard to get a strong affirmative because it's not really
anyone's decision. I would say, though, given that there haven't been
any strong -1s, you can go ahead.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Include datetime.py in stdlib or not?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Jul 24, 2010 at 3:44 AM, Benjamin Peterson <benjamin at python.org> wrote:

Yep, that sounds right to me too.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

