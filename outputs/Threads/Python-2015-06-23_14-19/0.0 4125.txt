
============================================================================
Subject: [Python-Dev] cpython: Issue #18408: Fix fileio_read() on
 _PyBytes_Resize() failure
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Issue #18408: Fix fileio_read() on
 _PyBytes_Resize() failure
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
17.07.13 00:09, victor.stinner ???????(??):

Why not Py_DECREF?





----------------------------------------
Subject:
[Python-Dev] cpython: Issue #18408: Fix fileio_read() on
 _PyBytes_Resize() failure
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2013/7/16 Serhiy Storchaka <storchaka at gmail.com>:

Because Py_DECREF(NULL) does crash.

Victor



----------------------------------------
Subject:
[Python-Dev] cpython: Issue #18408: Fix fileio_read() on
 _PyBytes_Resize() failure
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
17.07.13 01:03, Victor Stinner ???????(??):

Oh, I meaned Py_XDECREF.




----------------------------------------
Subject:
[Python-Dev] cpython: Issue #18408: Fix fileio_read() on
 _PyBytes_Resize() failure
----------------------------------------
Author: Christian Heime
Attributes: []Content: 
Am 17.07.2013 00:03, schrieb Victor Stinner:

Why not Py_XDECREF() then?





----------------------------------------
Subject:
[Python-Dev] cpython: Issue #18408: Fix fileio_read() on
 _PyBytes_Resize() failure
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2013/7/17 Serhiy Storchaka <storchaka at gmail.com>:

Ah ok :-) Well, for this specific code, it can probably be replaced with:

         if (_PyBytes_Resize(&bytes, n) < 0)
             return NULL;

I'm not sure that _PyBytes_Resize() *always* decref bytes and then set
bytes to NULL. I was too lazy to check this.

If someone wants to audit the code of _PyBytes_Resize(), there are
many other places where Py_XDECREF / Py_CLEAR can be removed on
_PyBytes_Resize() failure.

And I'm quite sure that there are still places where Py_DECREF() is
still used on _PyBytes_Resize() failure ;-)

Victor

