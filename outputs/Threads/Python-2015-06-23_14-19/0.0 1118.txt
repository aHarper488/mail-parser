
============================================================================
Subject: [Python-Dev] =?utf-8?q?About_resolution_=E2=80=9Caccepted?=
	=?utf-8?q?=E2=80=9D_on_the_tracker?=
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] =?utf-8?q?About_resolution_=E2=80=9Caccepted?=
	=?utf-8?q?=E2=80=9D_on_the_tracker?=
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Mon, 18 Oct 2010 17:20:13 +0200, <merwok at netwok.org> wrote:

'twasn't me, I figured the resolution field was only for resolved bugs.
I did notice a number of people using for accepted feature requests or
'valid bugs', or sometimes patches accepted in principle that were
nonetheless not quite complete.

Clearly (some) people would *like* a toggle that indicates such a status :)

--
R. David Murray                                      www.bitdance.com

