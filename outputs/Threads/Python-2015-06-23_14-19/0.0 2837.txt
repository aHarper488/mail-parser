
============================================================================
Subject: [Python-Dev] Raising OSError concrete classes from errno code
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
Currently we have exception tree of classes inherited from OSError
When we use C API we can call PyErr_SetFromErrno and
PyErr_SetFromErrnoWithFilename[Object] functions.
This ones raise concrete exception class (FileNotFoundError for
example) looking on implicit errno value.
I cannot see the way to do it from python.

Maybe adding builtin like exception_from_errno(errno, filename=None)
make some value?
Function returns exception instance, concrete class depends of errno value

For example if I've got EPOLLERR from poller call I can get error code
via s.getsockopt(SOL_SOCKET, SO_ERROR)
but I cannot raise concrete exception from given errno code.


--
Thanks,
Andrew Svetlov



----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/12/25 Andrew Svetlov <andrew.svetlov at gmail.com>:

I think a static method on OSError like .from_errno would be good.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
static method is better than new builtin function, agree.

On Wed, Dec 26, 2012 at 12:03 AM, Benjamin Peterson <benjamin at python.org> wrote:



-- 
Thanks,
Andrew Svetlov



----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 25.12.12 23:55, Andrew Svetlov wrote:

 >>> raise OSError(errno.ENOENT, 'No such file or directory', 'qwerty')
Traceback (most recent call last):
   File "<stdin>", line 1, in <module>
FileNotFoundError: [Errno 2] No such file or directory: 'qwerty'





----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Dec 26, 2012 at 6:50 PM, Serhiy Storchaka <storchaka at gmail.com> wrote:

As Serhiy's example shows, this mapping of error numbers to subclasses
is implemented directly in OSError.__new__. We did this so that code
could catch the new exceptions, even when dealing with old code that
raises the legacy exception types.

http://docs.python.org/3/library/exceptions#OSError could probably do
with an example like the one quoted in order to make this clearer

Cheers,
Nick.


-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
On Wed, Dec 26, 2012 at 12:16 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:
Sorry.
Looks like OSError.__new__ requires at least two arguments for
executing subclass search mechanism:

OSError(2,)
FileNotFoundError(2, 'error msg')

I had tried first one and got confuse.

Added http://bugs.python.org/issue16785 for this.





--
Thanks,
Andrew Svetlov



----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
It sould be in Python-Ideas: add keyword argument support for OSError 
and subclasses with suitable default values. I.e.

 >>> OSError(errno=errno.ENOENT)
FileNotFoundError(2, 'No such file or directory')
 >>> FileNotFoundError(filename='qwerty')
FileNotFoundError(2, 'No such file or directory')
 >>> FileNotFoundError(strerr='Bad file')
FileNotFoundError(2, 'Bad file')





----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 26 Dec 2012 13:37:13 +0200
Andrew Svetlov <andrew.svetlov at gmail.com> wrote:

Indeed, it does. I did this for consistency, because calling OSError
with only one argument doesn't set the "errno" attribute at all:


Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Raising OSError concrete classes from errno code
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
Thanks for the elaboration!

On Wed, Dec 26, 2012 at 6:42 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:



-- 
Thanks,
Andrew Svetlov

