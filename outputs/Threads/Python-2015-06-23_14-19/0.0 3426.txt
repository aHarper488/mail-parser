
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

Congrats Stefan! And thanks for the huge chunk of code.

Victor



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Wed, Mar 21, 2012 at 23:22, Victor Stinner <victor.stinner at gmail.com> wrote:

As a Python user, this looks really cool, thanks!

As a packager, is the libmpdec library used elsewhere? For Gentoo, we
generally prefer to package libraries separately and have Python
depend on them. From the site, it seems like you more or less wrote
libmpdec for usage in Python, but if it's general-purpose and actually
used in other software, it would be nice if Python grew a configure
option to make it use the system libmpdec.

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 21.03.2012 23:22, Victor Stinner wrote:

Seconded.  This is the kind of stuff that will make 3.3 the most awesomest
3.x release ever (and hopefully convince people that it does make sense to
port)...

cheers,
Georg




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
2012/3/22 Georg Brandl <g.brandl at gmx.net>:

On the other hand, porting PyPy to 3.3 will be more work ;-)
Fortunately the libmpdec directory should be reusable as is.
Nice work!

-- 
Amaury Forgeot d'Arc



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
----------------------------------------
Author: Dirkjan Ochtma
Attributes: []Content: 
On Fri, Mar 23, 2012 at 09:26, Stefan Krah <stefan at bytereef.org> wrote:

Sounds good, thanks!

Cheers,

Dirkjan



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
By the way, how much faster is cdecimal? 72x or 80x?
http://docs.python.org/dev/whatsnew/3.3.html#decimal

Victor

2012/3/23 Stefan Krah <stefan at bytereef.org>:



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #7652: Integrate
 the decimal floating point libmpdec library to speed
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 

Ok, but it's just surprising when you read the What's New document.
72x and 80x look to be inconsistent.


Hum, with a resolution able to store the result with all digits? If
yes, would it be possible to reuse the multiply algorithm of _decimal
(and maybe of other functions) for int? Or does it depend heavily on
_decimal internal structures?

Victor

