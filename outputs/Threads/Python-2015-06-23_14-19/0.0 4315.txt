
============================================================================
Subject: [Python-Dev] Federated repo model [was: IDLE in the stdlib]
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Federated repo model [was: IDLE in the stdlib]
----------------------------------------
Author: fwierzbicki at gmail.co
Attributes: []Content: 
On Thu, Mar 21, 2013 at 8:23 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:
I would love to have a federated repo model. I have recently made the
attempt to port the devguide for CPython to Jython with some
reasonable success. Part of that success has come because the devguide
is in its own repo and so forking it and continuing to merge
improvements from the original has been very easy.

I'd love to be able to do the same for the Doc/ directory at the root
of the CPython repo, but currently would have to fork the entire code
and doc repository etc. This would mean that merging the Doc/
improvements would be a big pain, with lots and lots of useless merges
where it would be hard to pick out the Doc changes.

To a lesser extent the same would hold for the Lib/ area - though in
that case I don't mind just pushing our changes to the CPython Lib/
(through the tracker and with code reviews of course) in the medium
term. Still, a separate repo for Lib would definitely be nice down the
road.

-Frank

