
============================================================================
Subject: [Python-Dev] Mercurial move?
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Mercurial move?
----------------------------------------
Author: Frank Wierzbick
Attributes: []Content: 
An advantage of being at PyCon :)

We *may* be able to get on mercurial very fast -- since all of the
interested parties are here. I'm going to get an svndump now -- the
downside to this is whatever anyone checks in during this in between
stage would need to get re-checked in after we move.

I'll let you know how it goes.

-Frank



----------------------------------------
Subject:
[Python-Dev] Mercurial move?
----------------------------------------
Author: Frank Wierzbick
Attributes: []Content: 
On Mon, Feb 22, 2010 at 5:45 PM, Frank Wierzbicki <fwierzbicki at gmail.com> wrote:
Sorry python-dev autocomplete fail -- I meant this to go to
jython-dev, sorry.  Please ignore.



----------------------------------------
Subject:
[Python-Dev] Mercurial move?
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Mon, Feb 22, 2010 at 5:57 PM, Frank Wierzbicki <fwierzbicki at gmail.com> wrote:

In that case congrats on beating us to the punch!

Let us know how it goes.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Mercurial move?
----------------------------------------
Author: Frank Wierzbick
Attributes: []Content: 
On Mon, Feb 22, 2010 at 6:12 PM, Guido van Rossum <guido at python.org> wrote:
Will, do, thanks!

-Frank

