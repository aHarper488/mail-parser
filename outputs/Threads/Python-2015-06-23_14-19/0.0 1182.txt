
============================================================================
Subject: [Python-Dev] [Python-checkins] r84559 -
	python/branches/py3k/Lib/subprocess.py
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r84559 -
	python/branches/py3k/Lib/subprocess.py
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 7, 2010 at 2:29 AM, brian.curtin <python-checkins at python.org> wrote:

Would it be worth including the signal number here, to at least give
some hint as to exactly which signal was received?

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r84559 -
	python/branches/py3k/Lib/subprocess.py
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Tue, Sep 7, 2010 at 07:34, Nick Coghlan <ncoghlan at gmail.com> wrote:



Sure, seems reasonable to me.
Does """raise ValueError("Unsupported signal: {}".format(sig))""" look fine,
or is there a more preferred format when displaying bad values in exception
messages?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100907/76f260fe/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r84559 -
	python/branches/py3k/Lib/subprocess.py
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Tue, Sep 7, 2010 at 11:05 PM, Brian Curtin <brian.curtin at gmail.com> wrote:

No, that's about what I was thinking as well. When all we have is an
error code (or similar number), it's difficult to make the associated
error message particularly pretty.

Cheers,
Nick.


-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r84559 -
	python/branches/py3k/Lib/subprocess.py
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Tue, Sep 7, 2010 at 08:19, Nick Coghlan <ncoghlan at gmail.com> wrote:



Made the adjustment in r84582 (py3k) and r84583 (release27-maint). Thanks
for the suggestion.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100907/9b2b2e62/attachment-0001.html>

