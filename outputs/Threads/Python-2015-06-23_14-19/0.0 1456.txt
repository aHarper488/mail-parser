
============================================================================
Subject: [Python-Dev] Sphinx version for Python 2.x docs
Post Count: 13
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
Hi All,

Any chance the version of sphinx used to generate the docs on 
docs.python.org could be updated?

I'd love to take advantage of the "new format" intersphinx mapping:

http://sphinx.pocoo.org/ext/intersphinx.html#confval-intersphinx_mapping

...but since it looks like docs.python.org uses a version of sphinx 
that's too old for that, I can't like to:

:ref:`Foo <python:logrecord-attributes>`

...and have to link to:

`LogRecord attributes 
<http://docs.python.org/library/logging.html#logrecord-attributes>`__

instead :-S

cheers,

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
Hello Chris,

On Wed, Aug 17, 2011 at 00:58, Chris Withers <chris at simplistix.co.uk> wrote:

I think what's needed first is to run a pilot: take the current 2.7
doc, update sphinx and look at what breaks, and evaluate if it's
fixable in a reasonable amount of time, or it's just too much and so
on.

Currently no-one has done that yet: would you ? :) That would helps up
quite much

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Chris Wither
Attributes: []Content: 
On 16/08/2011 16:05, Sandro Tosi wrote:

Where does that live?
Where are the instructions for building the docs? (dependencies needed, etc)

cheers,

Chris

-- 
Simplistix - Content Management, Batch Processing & Python Consulting
             - http://www.simplistix.co.uk



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Aug 17, 2011 at 12:08 PM, Chris Withers <chris at simplistix.co.uk> wrote:

'make html' in the Docs directory of a CPython checkout ("hg clone
http://hg.python.org/cpython") usually does the trick.

See http://docs.python.org/dev/documenting/building.html for more
detail if the above doesn't work.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
Hi all,


I'd like to discuss this aspect, in particular for the implication it
has on http://bugs.python.org/issue12409 .

Personally, I do think it has a value to have the same set of tools to
build the Python documentation of the currently active branches.
Currently, only 2.7 is different, since it still fetches (from
svn.python.org... can we fix this too? suggestions welcome!) sphinx
0.6.7 while 3.2/3.3 uses 1.0.7.

If you're worried about the time needed to convert the actual 2.7 doc
to new sphinx format and all the related changes, I volunteer to do
the job (and/or collaborate with whom is already on it), but what I
want to understand if it's an acceptable change.

I see sphinx more as of an internal, building tool, so freezing it
it's like saying "don't upgrade gcc" or so. Now the delta is just the
C functions definitions and some py-specific roles, but during the
years it will increase. Keeping it small, simplifying the forward-port
of doc patches (not needing to have 2 version between 2.7 and 3.x
f.e.) and having a common set of tools for doc building is worth IMHO.

What do you think about it? and yes Georg, I'd like to hear your opinion too :)

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: =?iso-8859-2?Q?=A3ukasz_Langa?
Attributes: []Content: 

Wiadomo?? napisana przez Sandro Tosi w dniu 23 sie 2011, o godz. 01:09:


Normally I'd say it's natural for us to specify that for a legacy release we're using build tools in versions up to so-and-so. Plus, requiring changes in the repository additionally points that this is indeed touching "frozen" code.

In case of 2.7 though, it's our "LTS release" so I think if Georg agrees, I'm also in favor of the upgrade.

As for Sphinx using svn.python.org, the main issue is not altering the scripts to use Hg, it's the weight of the whole Sphinx repository that would have to be cloned for each distclean. By using SVN you're only downloading a specifically tagged source tree.

-- 
Best regards,
?ukasz Langa
Senior Systems Architecture Engineer

IT Infrastructure Department
Grupa Allegro Sp. z o.o.


Pomy?l o ?rodowisku naturalnym zanim wydrukujesz t? wiadomo??!
Please consider the environment before printing out this e-mail.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110825/630fdb84/attachment.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: image002.jpg
Type: image/jpeg
Size: 1898 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110825/630fdb84/attachment.jpg>



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 23.08.2011 01:09, schrieb Sandro Tosi:

One of the main reasons for keeping Sphinx compatibility to 0.6.x was to
enable distributions (like Debian) to build the docs for the Python they ship
with the version of Sphinx that they ship.

This should now be fine with 1.0.x, so since you are ready to do the work of
converting the 2.7 Doc sources, it will be accepted.  The argument of easier
backports is a very good one.

The issue of using svn to download the tools is orthogonal; for this I would
agree to just packaging up a tarball or zipfile that is then downloaded using a
small Python script (should be properly cross-platform then).  Cloning the
original repositories is a) not useful, b) depends on availability of at least
two additional servers (remember docutils) and c) requires hg and svn.

Georg




----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
Hi all,

On Sat, Aug 27, 2011 at 07:47, Georg Brandl <g.brandl at gmx.net> wrote:

Not exactly as quickly as I would, I started to work on upgrading
sphinx for 2.7. Currently I've all the preliminary patches at:

http://hg.python.org/sandbox/morph/shortlog/5057ce392838

in the 2.7-sphinx branch (they fix one thing at a time, they'll be
collapsed once all ready).

During the build process, there are some warnings that I can understand:

writing output... [100%] whatsnew/index
/home/morph/cpython/morph_sandbox/Doc/glossary.rst:520: WARNING:
unknown keyword: nonlocal
/home/morph/cpython/morph_sandbox/Doc/library/stdtypes.rst:2372:
WARNING: more than one target found for cross-reference u'next':
iterator.next, multifile.MultiFile.next, csv.csvreader.next,
dbhash.dbhash.next, mailbox.oldmailbox.next, ttk.Treeview.next,
nntplib.NNTP.next, file.next, bsddb.bsddbobject.next,
tarfile.TarFile.next, generator.next
/home/morph/cpython/morph_sandbox/Doc/library/stdtypes.rst:2372:
WARNING: more than one target found for cross-reference u'next':
iterator.next, multifile.MultiFile.next, csv.csvreader.next,
dbhash.dbhash.next, mailbox.oldmailbox.next, ttk.Treeview.next,
nntplib.NNTP.next, file.next, bsddb.bsddbobject.next,
tarfile.TarFile.next, generator.next
/home/morph/cpython/morph_sandbox/Doc/library/sys.rst:651: WARNING:
unknown keyword: None
/home/morph/cpython/morph_sandbox/Doc/library/sys.rst:712: WARNING:
unknown keyword: None
/home/morph/cpython/morph_sandbox/Doc/reference/datamodel.rst:1942:
WARNING: unknown keyword: not in
/home/morph/cpython/morph_sandbox/Doc/reference/expressions.rst:1101:
WARNING: unknown keyword: not in
/home/morph/cpython/morph_sandbox/Doc/reference/expressions.rst:1135:
WARNING: unknown keyword: not in
/home/morph/cpython/morph_sandbox/Doc/reference/expressions.rst:1176:
WARNING: unknown keyword: not in
/home/morph/cpython/morph_sandbox/Doc/reference/expressions.rst:1184:
WARNING: unknown keyword: is not
/home/morph/cpython/morph_sandbox/Doc/reference/expressions.rst:1362:
WARNING: unknown keyword: is not
/home/morph/cpython/morph_sandbox/Doc/reference/simple_stmts.rst:700:
WARNING: unknown keyword: None
/home/morph/cpython/morph_sandbox/Doc/reference/simple_stmts.rst:729:
WARNING: unknown keyword: None
/home/morph/cpython/morph_sandbox/Doc/reference/simple_stmts.rst:729:
WARNING: unknown keyword: None
writing additional files... genindex py-modindex search download index
opensearch

Do you know how I can fix them?

Thanks & Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: =?UTF-8?Q?=C3=89ric_Araujo?
Attributes: []Content: 
Hi Sandro,

Thanks for getting the ball rolling on this.  One style for markup, one
Sphinx version to code our extensions against and one location for the
documenting guidelines will make our work a bit easier.

I assume you mean ?can?t?, as you later ask how to fix them.  As a
general rule, they?re only warnings, so they don?t break the build, 
only
some links or stylings, so I think it?s okay to ignore them *right 
now*.

That?s a mistake I did in cefe4f38fa0e.  This sentence should be 
removed.

Need to use :meth:`.next` to let Sphinx find the right target (more 
info
on request :)

Should use ``None``.

I don?t know if these should work (i.e. create a link to the 
appropriate
language reference section) or abuse the markup (there are ?not? and
?in? keywords, but no ?not in? keyword ? use ``not in``).  I?d say 
ignore
them.

Cheers



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
On Sat, Jan 14, 2012 at 04:24, ?ric Araujo <merwok at netwok.org> wrote:

thanks :) I'm happy to help!


yes, indeed


but I like to get them fixed nonetheless: after all, the current build
doesn't show warnings - but I agree it's a non-blocking issue.


Do you mean revert this whole hunk:

@@ -480,10 +516,11 @@
    nested scope
       The ability to refer to a variable in an enclosing definition.  For
       instance, a function defined inside another function can refer to
-      variables in the outer function.  Note that nested scopes work only for
-      reference and not for assignment which will always write to the innermost
-      scope.  In contrast, local variables both read and write in the innermost
-      scope.  Likewise, global variables read and write to the global
namespace.
+      variables in the outer function.  Note that nested scopes by default work
+      only for reference and not for assignment.  Local variables both read and
+      write in the innermost scope.  Likewise, global variables read and write
+      to the global namespace.  The :keyword:`nonlocal` allows writing to outer
+      scopes.

    new-style class
       Any class which inherits from :class:`object`.  This includes
all built-in

or just "The :keyword:`nonlocal` allows writing to outer scopes."?


it seems what it needed to was :meth:`next` (without the dot). The
current page links all 'next' in file.next() to functions.html#next,
and using :meth:`next` does that.


fixed


ACK, but I'm willing to fix them if someone tells me how to :)

I'm going to prepare the patches and then push - i'll send a heads-up afterward.

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: =?UTF-8?Q?=C3=89ric_Araujo?
Attributes: []Content: 
Hi,

Le 14/01/2012 15:31, Sandro Tosi a ?crit :

My proposal was to remove just that one last sentence, but the only
other change in the diff hunk is the addition of ?by default?, which is
connected to the existence of nonlocal.  Both changes, i.e. the whole
hunk, should be reverted (I think I?ll have time to do that today).


I should have given more info, as I wanted the opposite result :)
file.next should not link to the next function but to the file.next
method.  Because Sphinx does not differentiate between
meth/func/class/mod roles, :meth:`next` is not resolved to the nearest
next method as one could expect but to the next function, so we have to
use :meth:`~SomeClass.next` or :meth:`.next` (local ref markup) to get
our links to methods.


Georg fixed them.

Cheers




----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
On Mon, Jan 16, 2012 at 16:42, ?ric Araujo <merwok at netwok.org> wrote:

I've reverted it with ef1612a6a4f7


I tried :meth:`.next` but got a lots of :

/home/morph/cpython/py27/Doc/library/stdtypes.rst:2372: WARNING: more
than one target found for cross-reference u'next': iterator.next,
multifile.MultiFile.next, csv.csvreader.next, dbhash.dbhash.next,
mailbox.oldmailbox.next, ttk.Treeview.next, nntplib.NNTP.next,
file.next, bsddb.bsddbobject.next, tarfile.TarFile.next,
generator.next

so I ended up with :meth:`next` but it was still wrong. I've committed
51e11b4937b7 which uses :meth:`~file.next` instead, and it works.

Cheers,
-- 
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi



----------------------------------------
Subject:
[Python-Dev] Sphinx version for Python 2.x docs
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 17.01.2012 19:02, schrieb Sandro Tosi:


No need to try, just read the docs :)

`next` looks in the current (class, then module) namespaces.
`.next` looks everywhere, so the match must be unique.
So for something as common as "next", an explicit `file.next` is
required.

Georg


