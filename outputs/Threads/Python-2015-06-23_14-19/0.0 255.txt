
============================================================================
Subject: [Python-Dev] Deprecating undocumented, unused functions in difflib.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Deprecating undocumented, unused functions in difflib.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
Difflib.SequenceMatcher object currently get two feature attributes:
     self.isbjunk = junk.__contains__
     self.isbpopular = popular.__contains__

Tim Peters agrees that the junk and popular sets should be directly 
exposed and documented as part of the api, thereby making the functions 
redundant. The two functions are not currently documented (and should 
not be now). A google codesearch of 'isbjunk' and 'isbpopular' only 
returns hits in difflib.py itself (and its predecessor, ndiff.py).

It would be easiest to just remove the two lines above.
Or should I define functions _xxx names that issue a deprecation warning 
and attach them as attributes to each object? (Defining instance methods 
would not be the same).

There is only one internal use of one of the two functions which is 
easily edited.

-- 
Terry Jan Reedy


