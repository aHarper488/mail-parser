
============================================================================
Subject: [Python-Dev] [Python-checkins] Daily reference leaks
	(aef7db0d3893): sum=287
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] Daily reference leaks
	(aef7db0d3893): sum=287
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Jan 11, 2013 at 2:57 PM,  <solipsis at pitrou.net> wrote:

Hmm, I'm starting to wonder if there's something to this one - it
seems to be popping up a bit lately.


I'm gonna take a wild guess and suggest there may be a problem with
the recent pickling fix in the C extension :)

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] Daily reference leaks
	(aef7db0d3893): sum=287
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Thu, Jan 10, 2013 at 10:09 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:


Yep. We're on it :-)

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130111/62b63e81/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] Daily reference leaks
	(aef7db0d3893): sum=287
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Fri, Jan 11, 2013 at 8:08 AM, Christian Heimes <christian at python.org>wrote:


The second report is indeed a false positive. Coverity doesn't know that
PyList_GET_SIZE returns 0 for PyList_New(0). Maybe it can be taught some
project/domain-specific information?

The first report is legit, however. PyTuple_New(0) was called and its
return value wasn't checked for NULL. I actually think Coverity is very
useful for such cases because forgetting to check NULL returns from
PyObject constructors is a common mistake and it's not something that would
show up in tests. Anyway, this was fixed.

Thanks for reporting

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130112/d9a5a3e6/attachment.html>

