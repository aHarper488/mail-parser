
============================================================================
Subject: [Python-Dev] Not-a-Number (was PyObject_RichCompareBool
 identity shortcut)
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
 identity shortcut)
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Mark Shannon wrote:


Perhaps, but that wouldn't solve anything on its own. If
this new class compares reflexively, then it still violates
IEE754. Conversely, existing NaNs could be made to compare
reflexively without making them a new class.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
 identity shortcut)
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Mark Shannon wrote:

I would argue that the implementation of NANs is irrelevant. If NANs are 
useful in hardware floats -- and I think they are -- then they're just 
as equally useful as objects, or as strings in languages like REXX or 
Hypertalk where all data is stored as strings, or as quantum wave 
functions in some future quantum computer.



I see your wink, but what do you make of these?

class NotAnObject(object):
     pass

nao = NotAnObject()
assert isinstance(nao, object)

class NotAType(object):
     pass

assert type(NotAType) is type




Others have already pointed out this won't make any difference.

Fundamentally, the problem is that some containers bypass equality tests 
for identity tests. There may be good reasons for that shortcut, but it 
leads to problems with *any* object that does not define equality to be 
reflexive, not just NANs.


 >>> class Null:
...     def __eq__(self, other):
...             return False
...
 >>> null = Null()
 >>> null == null
False
 >>> [null] == [null]
True




I don't agree with this argument. I think Meyer is completely mistaken 
there. The question of NAN equality is that of a vacuous truth, quite 
similar to the Present King of France:

http://en.wikipedia.org/wiki/Present_King_of_France

Meyer would have us accept that:

     The present King of France is a talking horse

and

     The present King of France is not a talking horse

are equally (pun not intended) valid. No, no they're not. I don't know 
much about who the King of France would be if France had a king, but I 
do know that he wouldn't be a talking horse.

Once you accept that NANs aren't equal to anything else, it becomes a 
matter of *practicality beats purity* to accept that they can't be equal 
to themselves either. A NAN doesn't represent a specific thing. It's a 
signal that your calculation has generated an indefinite, undefined, 
undetermined value. NANs aren't equal to anything. The fact that a NAN 
happens to have an existence as a bit-pattern at some location, or as a 
distinct object, is an implementation detail that is irrelevant. If you 
just happen by some fluke to compare a NAN to "itself", that shouldn't 
change the result of the comparison:

     The present King of France is the current male sovereign who
     rules France

is still false, even if you happen to write it like this:

     The present King of France is the present King of France


This might seem surprising to those who are used to reflexivity. Oh 
well. Just because reflexivity holds for actual things, doesn't mean it 
holds for, er, things that aren't things. NANs are things that aren't 
things.






Perhaps they shouldn't rely on it. Identity tests are an implementation 
detail. But in any case, reflexivity is *not* a guarantee of Python. 
With rich comparisons, you can define __eq__ to do anything you like.





-- 
Steven




----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
 identity shortcut)
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
Steven D'Aprano wrote:

So,
Indeed, so its OK if type(NaN) != type(0.0) ?


Trying to make something not an object in a language where everything is 
an object is bound to be problematic.


Just because you can do that, doesn't mean you should.
Equality should be reflexive, without that fundamental assumption many 
non-numeric algorithms fall apart.


Not breaking a whole bunch of collections and algorithms has a certain 
practical appeal as well ;)


The problem with this argument is the present King of France does not 
exist, whereas NaN (as a Python object) does exist.

The present King of France argument only applies to non-existent things. 
Python objects do exist (as much as any computer language entity 
exists). So the expression "The present King of France" either raises an 
exception (non-existence) or evaluates to an object (existence).
In this case "the present King of France" doesn't exist and should raise 
a FifthRepublicException :)
inf / inf does not raise an exception, but evaluates to NaN, so NaN
exists. For objects (that exist):
(x is x) is True.
The present President of France is the present President of France,
regardless of who he or she may be.
A NaN is thing that *is* a thing; it exists:
object.__repr__(float('nan'))

Of course if inf - inf, inf/inf raised exceptions,
then NaN wouldn't exist (as a Python object)
and the problem would just go away :)
After all 0.0/0.0 already raises an exception, but the
IEEE defines 0.0/0.0  as NaN.

And if you do define __eq__ to be non-reflexive then things will break.
Should an object that breaks so much (ie NaN in its current form) be in 
the standard library?
Perhaps we should just get rid of it?





----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
 identity shortcut)
----------------------------------------
Author: Rob Cliff
Attributes: []Content: 

On 28/04/2011 15:58, Steven D'Aprano wrote:
I say you have that backwards.  It is a legitimate shortcut, and any 
object that (perversely) doesn't define equality to be reflexive leads 
(unsurprisingly) to problems with it (and with *anything else* that - 
very reasonably - assumes that identity implies equality).

And you can write
     True = False
(at least in older versions of Python you could).  No language stops you 
from writing stupid programs.

In fact I would propose that the language should DEFINE the meaning of 
"==" to be True if its operands are identical, and only if they are not 
would it use the comparison operators, thus enforcing reflexivity.  
(Nothing stops you from writing your own non-reflexive __eq__ and 
calling it explicitly, and I think it is right that you should have to 
work harder and be more explicit if you want that behaviour.)

Please, please, can we have a bit of common sense and perspective here.  
No-one (not even a mathematician) except someone from Wonderland would 
seriously want an object not equal to itself.

Regards
Rob Cliffe



----------------------------------------
Subject:
[Python-Dev] Not-a-Number (was PyObject_RichCompareBool
 identity shortcut)
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Mark Shannon wrote:

Sure. But that just adds complexity without actually resolving anything.



[...]

So what? If I have a need for non-reflexivity in my application, why 
should I care that some other algorithm, which I'm not using, will fail?

Python supports non-reflexivity. If I take advantage of that feature, I 
can't guarantee that *other objects* will be smart enough to understand 
this. This is no different from any other property of my objects.



[...]

NANs (as Python objects) exist in the same way as the present King of 
France exists as words. It's an implementation detail: we can't talk 
about the non-existent present King of France without using words, and 
we can't do calculations on non-existent/indeterminate values in Python 
without objects.

Words can represent things that don't exist, and so can bit-patterns or 
objects or any other symbol. We must be careful to avoid mistaking the 
symbol (the NAN bit-pattern or object) for the thing (the result of 
whatever calculation generated that NAN). The idea of equality we care 
about is equality of what the symbol represents, not the symbol itself.

The meaning of "spam and eggs" should not differ according to the 
typeface we write the words in. Likewise the number 42 should not differ 
according to how the int object is laid out, or whether the bit-pattern 
is little-endian or big-endian. What matters is the "thing" itself, 42, 
not the symbol: it will still be 42 even if we decided to write it in 
Roman numerals or base 13.

Likewise, what matters is the non-thingness of NANs, not the fact that 
the symbol for them has an existence as an object or a bit-pattern.



-- 
Steven

