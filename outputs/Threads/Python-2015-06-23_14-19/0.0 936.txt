
============================================================================
Subject: [Python-Dev] [Python-checkins] r86355
	-	python/branches/py3k/Modules/_pickle.c
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86355
	-	python/branches/py3k/Modules/_pickle.c
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Alexander Belopolsky writes:
 > On Wed, Nov 10, 2010 at 7:28 AM, Victor Stinner
 > <victor.stinner at haypocalc.com> wrote:
 > ..
 > > I don't know, but the commit is trivial and cheap. If it improves the support
 > > on uncommon compiler, I agree to commit such change.
 > >
 > 
 > But it does it at the cost of invalidating the "svn blame" for the
 > last enum entry now and for future additions.   The problem is that
 > when you change from
 > 
 > enum {
 >   ..
 >   X
 > }
 > 
 > to
 > 
 > enum {
 >   ..
 >   X,
 >   Y
 > }

If that bothers you, you can write

enum {
  A
  , B
  /* etc */
  , X
}

or

enum {
  A,
  B,
  /* etc */
  X,
  enum_bound_otherwise_unused
}

I prefer the last; it's a compiler (and debugger) space burden, but
shouldn't affect the running python.  On the original question, I
think it's preferable to keep compilers happy unless you're willing to
*require* C99.




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r86355
	-	python/branches/py3k/Modules/_pickle.c
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Alexander Belopolsky writes:
 > On Wed, Nov 10, 2010 at 10:04 PM, Stephen J. Turnbull
 > <stephen at xemacs.org> wrote:
 > > ... ?On the original question, I
 > > think it's preferable to keep compilers happy unless you're willing to
 > > *require* C99.
 > 
 > Hmm, maybe I should take another look at http://bugs.python.org/issue4805 .
 > 
 > Note that issue #10359 was not about any real compiler

True, but a real compiler has been mentioned in the thread, and I know
that every time XEmacs lets a non-C89 feature slip through (most
commonly, "//" comments and declarations following non-declarations,
the latter being a killer feature in C-like languages IMO, but our
current coding standard says "C89") we get build breakage reports.

