
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: #14533: if a test has
 no test_main, use loadTestsFromModule.
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: #14533: if a test has
 no test_main, use loadTestsFromModule.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 

On 4/9/2012 9:13 AM, r.david.murray wrote:
...

Being on Windows, I sometimes run single tests interactively with

from test import test_xxx as t; t.test_main()

Should t.unittest.main(t.__name__) work as well?
Should this always work even if there is still a test_main?

tjr



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: #14533: if a test has
 no test_main, use loadTestsFromModule.
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
On Apr 10, 2012 2:36 AM, "Terry Reedy" <tjreedy at udel.edu> wrote:
facilities
correctly
converted to
no
Both questions have the same answer. Yes, because this is how discovery
works.
http://mail.python.org/mailman/options/python-dev/anacrolix%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120410/94216a78/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: #14533: if a test has
 no test_main, use loadTestsFromModule.
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 4/9/2012 3:57 PM, R. David Murray wrote:


Good. The only doc for the parameter is "unittest.main(module='__main__',"
with no indication other than the name 'module' that both a module 
object or a name is accepted (as with some file object or name interfaces).


One way to again run each would be nice. I will open an issue if I find 
any laggards.

-- 
Terry Jan Reedy


