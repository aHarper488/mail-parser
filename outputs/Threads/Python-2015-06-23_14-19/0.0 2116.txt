
============================================================================
Subject: [Python-Dev] NetBSD and curses
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] NetBSD and curses
----------------------------------------
Author: Bill Gree
Attributes: []Content: 
Hi all,

I ran across this issue several months ago and filed a bug report 
(#9667).  It just came up again, and it doesn't look like anything's 
been done with the bug report, so I thought I'd post here.

In _cursesmodule.c there are a lot of preprocesser conditionals that 
test if the system is NetBSD. In my case, the issue was that the module 
built lacked the KEY_UP / _DOWN / etc. constants, but there are other 
changes as well.  This is the case even if you're compiling against 
ncurses instead of the system curses.  ?ttached below is a patch against 
2.7.1 that negates the NetBSD conditionals if ncurses is present.  It 
seems to work as expected, although I haven't done any real testing.  I 
assumed this was done because NetBSD curses was missing something, but I 
looked at the headers and it seems to have most of the constants that 
the compilation directives are leaving out (A_INVIS, the aforementioned 
KEY_* constants, at least), so I'm not sure why that code isn't compiled 
in anyway.  Please let me know if I'm misunderstanding this.

Thanks,
Bill
-------------- next part --------------
An embedded and charset-unspecified text was scrubbed...
Name: _cursesmodule.c.diff
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110314/fb15c810/attachment.ksh>



----------------------------------------
Subject:
[Python-Dev] NetBSD and curses
----------------------------------------
Author: Gregory P. Smit
Attributes: []Content: 
Would you please post this to bugs.python.org so that it doesn't get lost?
 thanks!

-gps

On Mon, Mar 14, 2011 at 8:51 PM, Bill Green <bill at supposedly.org> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110315/6f1faa93/attachment.html>



----------------------------------------
Subject:
[Python-Dev] NetBSD and curses
----------------------------------------
Author: Gregory P. Smit
Attributes: []Content: 
Hi, Sorry, it was just laughingly pointed out to me that I responded to your
complaint about a bug being ignored by asking you to file a bug. :)  Thats
what I get for "reading" things late at night.

regardless, any time you have a patch for something, please attach it to the
issue, things on the mailing list get lost.  I've gone ahead and attached
the patch and accepted the issue.  I have a netbsd ec2 instance to test with
now but haven't had time to get it setup for python development.

None of the devs currently run netbsd on a regular basis or have much
experience with it as a platform so patches are a great help.  Any chance
you can also make a version of the patch that applies against hg cpython tip
(3.3)?

-gps

2011/3/15 Gregory P. Smith <greg at krypto.org>

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110315/50778b7b/attachment.html>

