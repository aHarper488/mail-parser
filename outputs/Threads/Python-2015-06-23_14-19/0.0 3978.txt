
============================================================================
Subject: [Python-Dev] PEP 426 is now the draft spec for distribution
 metadata 2.0
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 426 is now the draft spec for distribution
 metadata 2.0
----------------------------------------
Author: Daniel Holt
Attributes: []Content: 
On Sun, 17 Feb 2013 20:11:23 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

So is it possible to bundle several versioned metadata files in order
to maximize compatibility? Or does a distribution with 2.0 metadata
have to abandon all hope of being compatible with all tools?

Regards

Antoine.

_______________________________________________
Python-Dev mailing list
Python-Dev at python.org
http://mail.python.org/mailman/listinfo/python-dev
Unsubscribe:
http://mail.python.org/mailman/options/python-dev/dholth%40gmail.com

Not likely to matter for a while as the current md v1 tools don't
understand this new obsolescence rule :-)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130217/a9ed4e25/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 426 is now the draft spec for distribution
 metadata 2.0
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Sun, Feb 17, 2013 at 1:45 PM, Daniel Holth <dholth at gmail.com> wrote:

Using a separate file for post-obsolescence-rule metadata versions would
allow coexistance, which would likely improve adoption.


  -Fred

-- 
Fred L. Drake, Jr.    <fred at fdrake.net>
"A storm broke loose in my mind."  --Albert Einstein



----------------------------------------
Subject:
[Python-Dev] PEP 426 is now the draft spec for distribution
 metadata 2.0
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 18 Feb 2013 04:54, "Fred Drake" <fred at fdrake.net> wrote:
understand

Perhaps, but not something we actually need to worry about until we're
contemplating metadata 3.0, especially since whether or not it helps will
depend on why we're breaking backwards compatibility. At the very least,
updated wheel, sdist and installation database specs would be needed to
allow multiple versions of the metadata.

As Daniel pointed out, easy_install and pip also don't follow this rule
yet, so it won't really have any impact if we never get to metadata 3.0.

Cheers,
Nick.

http://mail.python.org/mailman/options/python-dev/ncoghlan%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130218/e0dd87e5/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 426 is now the draft spec for distribution
 metadata 2.0
----------------------------------------
Author: Fred Drak
Attributes: []Content: 
On Sun, Feb 17, 2013 at 4:21 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Actually, my point was that using a separate filename for version 2.0 would
allow provision of both 1.x and 2.0, so version 2.0 metadata can be deployed
as tool support becomes available, instead of having to wait until 1.x tools
are replaced.

Once tools are following the new rule about version compatibility, there's
less worry about this (at least on my part).


  -Fred

-- 
Fred L. Drake, Jr.    <fred at fdrake.net>
"A storm broke loose in my mind."  --Albert Einstein



----------------------------------------
Subject:
[Python-Dev] PEP 426 is now the draft spec for distribution
 metadata 2.0
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 18 Feb 2013 08:36, "Fred Drake" <fred at fdrake.net> wrote:
yet,
would
deployed
tools

None of sdist, wheel or the installation database currently support
parallel metadata versions. Interpreting 2.0 metadata as 1.1 also won't be
catastrophically wrong in general, especially if the production tools still
also ship the legacy dependency and entry points data as separate files.
The worst thing that is likely to happen is an old tool may fail to show a
2.0 style description.

However, I *will* update the PEP to state explicitly that tools should emit
the lowest required metadata version for the fields being published.

Cheers,
Nick.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130218/511d207b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] PEP 426 is now the draft spec for distribution
 metadata 2.0
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Feb 18, 2013 at 9:54 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

I clarified the metadata version handling here:
http://hg.python.org/peps/rev/630c5dd5a123

Rather than "should", I softened the "use the lowest metadata format"
suggestion to a "may". I also pointed out that these new rules will
only apply to a future major version transition, and for this one,
people can just emit the extra files they do today and even older
versions of tools will probably get things fairly right.

Cheers,
Nick.

diff --git a/pep-0426.txt b/pep-0426.txt
--- a/pep-0426.txt
+++ b/pep-0426.txt
@@ -89,9 +89,15 @@

 Version of the file format; "2.0" is the only legal value.

-Automated tools should warn if ``Metadata-Version`` is greater than the
-highest version they support, and must fail if ``Metadata-Version`` has
-a greater major version than the highest version they support.
+Automated tools consuming metadata should warn if ``Metadata-Version`` is
+greater than the highest version they support, and must fail if
+``Metadata-Version`` has a greater major version than the highest
+version they support.
+
+For broader compatibility, automated tools may choose to produce
+distribution metadata using the lowest metadata version that includes
+all of the needed fields.
+

 Example::

@@ -1283,9 +1289,19 @@
 metadata specifications is unlikely to give the expected behaviour.

 Whenever the major version number of the specification is incremented, it
-is expected that deployment will take some time, as metadata consuming tools
-much be updated before other tools can safely start producing the new
-format.
+is expected that deployment will take some time, as either metadata
+consuming tools must be updated before other tools can safely start
+producing the new format, or else the sdist and wheel formats, along with
+the installation database definition, will need to be updated to support
+provision of multiple versions of the metadata in parallel.
+
+Existing tools won't abide by this guideline until they're updated to
+support the new metadata standard, so the new semantics will first take
+effect for a hypothetical 2.x -> 3.0 transition. For the 1.x -> 2.0
+transition, it is recommended that tools continue to produce the
+existing supplementary files (such as ``entry_points.txt``) in addition
+to any equivalents specified using the new features of the standard
+metadata format (including the formal extension mechanism).

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia

