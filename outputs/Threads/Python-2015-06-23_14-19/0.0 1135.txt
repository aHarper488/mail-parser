
============================================================================
Subject: [Python-Dev] [Python-checkins] r85822
	-	python/branches/py3k/Modules/ossaudiodev.c
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] r85822
	-	python/branches/py3k/Modules/ossaudiodev.c
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

FWIW, I feel that these casts are misguided. Having function pointer
casts means that type errors in the signature get suppressed. E.g.
people using such casts for tp_hash will be unable to detect the change
in the tp_hash return type that we have implemented for 3.2.

It would be better, IMO, if these casts get avoided everywhere, even
if that means that the functions get a line longer.

Regards,
Martin


