
============================================================================
Subject: [Python-Dev] module shutdown procedure
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] module shutdown procedure
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le jeudi 22 juillet 2010 ? 07:23 -0500, Benjamin Peterson a ?crit :

Is it the reason why? With the new module creation API in 3.x, extension
modules should be able to handle deletion of their own internal
resources.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] module shutdown procedure
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/7/22 Antoine Pitrou <solipsis at pitrou.net>:

Well, then the reason is that no modules use it.

I also believe the new API is dangerous because modules can be
deallocated before objects in them resulting in segfaults when those
objects require globals stored in the module state.


-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] module shutdown procedure
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 22.07.2010 13:29, schrieb Antoine Pitrou:

Yes, but as Martin noted at the summit, nobody since went through all the
extension modules and changed them to use a struct instead of globals.

Georg

-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.




----------------------------------------
Subject:
[Python-Dev] module shutdown procedure
----------------------------------------
Author: Neil Schemenaue
Attributes: []Content: 
Georg Brandl <g.brandl at gmx.net> wrote:

And that's a necessary but likely not sufficient condition for the
GC based module shutdown scheme to work.  It really would take a
huge amount of effort to make the idea fly.

That doesn't mean it is not worth doing.  The current scheme is
pretty ugly.

  Neil




----------------------------------------
Subject:
[Python-Dev] module shutdown procedure
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Georg Brandl, 22.07.2010 16:13:

The Cython project has had this on the agenda ever since the early days of 
3.0, but we never got around to investing the time.

http://trac.cython.org/cython_trac/ticket/173
http://trac.cython.org/cython_trac/ticket/218

We already generate optional module cleanup code as an atexit function to 
help with valgrind, so much of the code required for m_clear() is already 
there, but getting it right (i.e. making all module globals local to an 
instance) would require some effort on our side that is not currently 
considered worth it (just ask yourself what you want first: blazingly fast 
generator functions? Or Py3-only module cleanup code?).

It wouldn't be hard to do, though, and adapting Cython here would 
immediately migrate a whole bunch of extension modules to the new module 
handling API. Note that this may not mean that all of these modules will 
work out of the box. The current cleanup code is disabled by default 
because module globals can be used in finalisers, which means that the 
module cleanup can crash if the globals are cleaned up in the wrong order. 
This could be worked around, though, as we could potentially detect 
required globals at compile time and either change the order of their 
cleanup or emit a warning that there is no such order in the face of cycles.

So, as always, it's all just a matter of investing the time to get this 
implemented.

Stefan


