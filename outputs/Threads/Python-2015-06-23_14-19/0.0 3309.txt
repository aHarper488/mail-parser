
============================================================================
Subject: [Python-Dev] [Infrastructure] Buildbot master moved
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Infrastructure] Buildbot master moved
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le jeudi 28 juin 2012 ? 09:13 +0200, "Martin v. L?wis" a ?crit :

It seems we lost the "force build" button. Judging from the templates,
the form is here, it just isn't displayed.

I also don't see any custom builders:
http://buildbot.python.org/all/waterfall?category=custom.stable&category=custom.unstable

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Infrastructure] Buildbot master moved
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le jeudi 28 juin 2012 ? 12:06 +0200, Antoine Pitrou a ?crit :

Ok, it seems we should migrate to the ForceScheduler API:
http://buildbot.net/buildbot/docs/0.8.6/manual/cfg-schedulers.html#forcescheduler-scheduler






----------------------------------------
Subject:
[Python-Dev] [Infrastructure] Buildbot master moved
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le jeudi 28 juin 2012 ? 12:11 +0200, Antoine Pitrou a ?crit :

Ok, both are fixed.
Note: there are uncommitted changes in the local git repo, I left them
uncommitted.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Infrastructure] Buildbot master moved
----------------------------------------
Author: Hynek Schlawac
Attributes: []Content: 
Hi,

I don?t know if it?s known, but the bot infrastructure is FUBAR now.
http://buildbot.python.org/all/waterfall is a stacktrace and all tests
fail because of the XML-RPC tests that use our buildbot API.

Regards
Hynek



----------------------------------------
Subject:
[Python-Dev] [Infrastructure] Buildbot master moved
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le jeudi 28 juin 2012 ? 16:04 +0200, Hynek Schlawack a ?crit :

It works if you reload the page, though. Looks like a weird bug in
buildbot, has anyone reported it upstream?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [Infrastructure] Buildbot master moved
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/28/2012 10:04 AM, Hynek Schlawack wrote:

Errors seem intermittant. Above just worked for me after failing.
Clicking on links on http://www.python.org/dev/buildbot/ (which 
builtbot.python.org redirects to) may give display or error (from FF)

web.Server Traceback (most recent call last):

<type 'exceptions.AttributeError'>: 'NoneType' object has no attribute 
'getStatus'
/usr/lib/python2.7/dist-packages/twisted/web/server.py, line 132 in process
130    try:
131      resrc = self.site.getResourceFor(self)
132      self.render(resrc)
133    except:
/usr/lib/python2.7/dist-packages/twisted/web/server.py, line 167 in render
165    """
166    try:
167      body = resrc.render(self)
168    except UnsupportedMethod, e:
/data/buildbot/lib/python/buildbot/status/web/base.py, line 324 in render
322      return ''
323
324    ctx = self.getContext(request)
325
/data/buildbot/lib/python/buildbot/status/web/base.py, line 196 in 
getContext
194class ContextMixin(AccessorMixin):
195  def getContext(self, request):
196    status = self.getStatus(request)
197    rootpath = path_to_root(request)
/data/buildbot/lib/python/buildbot/status/web/base.py, line 182 in getStatus
180class AccessorMixin(object):
181  def getStatus(self, request):
182    return request.site.buildbot_service.getStatus()
183
/data/buildbot/lib/python/buildbot/status/web/baseweb.py, line 498 in 
getStatus
496
497  def getStatus(self):
498    return self.master.getStatus()
499

<type 'exceptions.AttributeError'>: 'NoneType' object has no attribute 
'getStatus'

-- 
Terry Jan Reedy







----------------------------------------
Subject:
[Python-Dev] [Infrastructure] Buildbot master moved
----------------------------------------
Author: =?UTF-8?B?Ik1hcnRpbiB2LiBMw7Z3aXMi?
Attributes: []Content: 

I'm quite certain it can be repaired.

Regards,
Martin

