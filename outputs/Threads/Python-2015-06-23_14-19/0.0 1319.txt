
============================================================================
Subject: [Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
Post Count: 22
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Anthony Scopat
Attributes: []Content: 
Hi Daniel,

Thanks for putting this together.  I am a huge supporter of benchmarking
efforts.  My brief comment is below.

On Wed, Apr 6, 2011 at 11:52 AM, DasIch <dasdasich at googlemail.com> wrote:

If you are reaching out to other projects at this stage, I think you should
also be in touch with the Cython people  (even if its 'implementation'
sits on top of CPython).

As a scientist/engineer what I care about is how Cython benchmarks to
CPython.  I believe that they have some ideas on benchmarking and have
also explored this space.  Their inclusion would be helpful to me thinking
this GSoC successful at the end of the day (summer).

Thanks for your consideration.
Be Well
Anthony


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110407/bf80fa63/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Thu, Apr 7, 2011 at 3:54 PM, Anthony Scopatz <scopatz at gmail.com> wrote:

Right now, we are talking about building "speed.python.org" to test
the speed of python interpreters, over time, and alongside one another
- cython *is not* an interpreter.

Cython is out of scope for this.



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 04/07/2011 04:28 PM, Jesse Noller wrote:

Why is it out of scope to use the benchmarks and test harness to answer
questions like "can we use Cython to provide optional optimizations for
the stdlib"?  I can certainly see value in havng an objective way to
compare the macro benchmark performance of a Cython-optimized CPython
vs. a vanilla CPython, as well as vs. PyPY, Jython, or IronPython.


Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk2eLWcACgkQ+gerLs4ltQ4R7wCgmam/W+3JzJRgxtehnnfbE54S
RxcAn0ooO2kpw84kRvmTP5dCAWir9g3i
=3mL7
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 07 Apr 2011 17:32:24 -0400
Tres Seaver <tseaver at palladion.com> wrote:

Agreed. Assuming someone wants to take care of the Cython side of
things, I don't think there's any reason to exclude it under the
dubious reason that it's "not an interpreter".
(would you exclude Psyco, if it was still alive?)

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Anthony Scopat
Attributes: []Content: 
On Thu, Apr 7, 2011 at 6:11 PM, Michael Foord <fuzzyman at voidspace.org.uk>wrote:



Jesse, I understand that we are talking about the benchmarks on
speed.pypy.org.  The current suite, and correct me if I
am wrong, is completely written in pure python so that any of the
'interpreters' may run them.

My point, which I stand by, was that during the initial phase (where
benchmarks are defined) that the Cython crowd
should have a voice.  This should have an enriching effect on the whole
benchmarking task since they have
thought about this issue in a way that is largely orthogonal to the methods
PyPy developed.  I think it
would be a mistake to leave Cython out of the scoping study.

I actually agree with Micheal.  I think the onus of getting the benchmarks
working on every platform is the
onus of that interpreter's community.

The benchmarking framework that is being developed as part of GSoC should be
agile enough to add and
drop projects over time and be able to make certain tests as 'known
failures', etc.

I don't think I am asking anything unreasonable here.  Especially, since at
the end of the day the purview of
projects like PyPy and Cython ("Make Python Faster") is the same.

Be Well
Anthony


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110407/fac291a8/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Anthony Scopat
Attributes: []Content: 
On Thu, Apr 7, 2011 at 6:52 PM, Michael Foord <fuzzyman at voidspace.org.uk>wrote:

*some* good benchmarks in place (and the pypy ones are good ones).

Agreed. The PyPy ones are good.



I was simply going with what the abstract said.  I am fine with discussion
needing to be timely (a week?).  But it seems that from what you are saying,
just to be clear, "Point (2) Implementation" is also non-existent as the
PyPy benchmarks already exist.  If the point of the GSoC is to port the PyPy
benchmarks to Python 3, under "Point (3) Porting", might I suggest a slight
revision of the proposal ;)?

Be Well
Anthony


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110407/6bc134c6/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Thu, Apr 7, 2011 at 7:52 PM, Michael Foord <fuzzyman at voidspace.org.uk> wrote:

What michael said: My goal is is to get speed.pypy.org ported to be
able to be used by $N interpreters, for $Y sets of performance
numbers. I'm trying to constrain the problem, and the initial
deployment so we don't spend the next year meandering about. It should
be sufficient to port the benchmarks from speed.pypy.org, and any
deltas from http://hg.python.org/benchmarks/ to Python 3 and the
framework that runs the tests to start.

I don't care if we eventually run cython, psyco, parrot, etc. But the
focus at the language summit, and the continued focus of me getting
the hardware via the PSF to host this on performance/speed.python.org
is tightly focused on the pypy, ironpython, jython and cpython
interpreters.

Let's just get our basics done first before we go all crazy with adding stuff :)

jesse



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Anthony Scopat
Attributes: []Content: 
On Thu, Apr 7, 2011 at 8:29 PM, Jesse Noller <jnoller at gmail.com> wrote:


Ahh gotcha, I think I misunderstood the scope in the short term ;).

Be Well
Anthony



-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110407/89a82e18/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Fri, Apr 8, 2011 at 3:29 AM, Jesse Noller <jnoller at gmail.com> wrote:

Hi.

Spending significant effort to make those benchmarks run on cython is
definitely out of scope. If cython can say compile twisted we can as
well run it (or skip few ones that are not compilable for some time).
If cython won't run every of the major benchmarks (using large
libraries) or cython people would complain all the time about adding
static type analysis (this won't happen) then the answer is cython is
not python enough.

The second part is porting to python3. I would like to postpone this
part (I already chatted with DasIch about it) until libraries are
ready. As of 8th of April 2010 none of the interesting benchmarks
would run or be easy to port to Python 3. by skipping all interesting
ones (each of them requiring large library), the outcome would be a
set of benchmarks that not that many people care about and also not a
very interesting one.

My proposal to steer this project would be to first have an
infrastructure running, improve the backend running the benchmarks,
make sure we build all the interpreters and have this running. Also
polish codespeed to look good for everyone. This is already *a lot* of
work, even though it might not look like it.

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Jesse Noller, 07.04.2011 22:28:

Would you also want to exclude Psyco then? It clearly does not qualify as a 
Python interpreter.



Why? It should be easy to integrate Cython using pyximport. Basically, all 
you have to do is register the pyximport module as an import hook. Cython 
will then try to compile the imported Python modules and fall back to the 
normal .py file import if the compilation fails for some reason.

So, once CPython is up and running in the benchmark test, adding Cython 
should be as easy as copying the configuration, installing Cython and 
adding two lines to site.py.

Obviously, we'd have to integrate a build of the latest Cython development 
sources as well, but it's not like installing a distutils enabled Python 
package from sources is so hard that it pushes Cython out of scope for this 
GSoC.

Stefan




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Fri, Apr 8, 2011 at 11:22 AM, Stefan Behnel <stefan_ml at behnel.de> wrote:

Why not? it does run those benchmarks just fine.


then it's fine to include it. we can even include it now in
speed.pypy.org that way. would it compile django?


can you provide a simple command line tool for that? I want
essentially to run ./cython-importing-stuff some-file.py


no, that's fine. My main concern is - will cython run those
benchmarks? and will you complain if we don't provide a custom cython
hacks? (like providing extra type information)




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Maciej Fijalkowski, 08.04.2011 11:41:

Sure.



Never tried. Likely not completely, but surely some major parts of it. 
That's the beauty of it - it just falls back to CPython. :) If we're lucky, 
it will manage to compile some performance critical parts without 
modifications. In any case, it'll be trying to compile each module.



You can try

     python -c 'import pyximport; \
                pyximport.install(pyimport=True); \
                exec("somefile.py")'

You may want to configure the output directory for the binary modules, 
though, see

https://github.com/cython/cython/blob/master/pyximport/pyximport.py#L343

Please also take care to provide suitable gcc CFLAGS, e.g. "-O3 
-march=native" etc.



In the worst case, they will run at CPython speed with uncompiled modules.



I don't consider providing extra type information a hack. Remember that 
they are only used for additional speed-ups in cases where the author is 
smarter than the compiler. It will work just fine without them.

Stefan




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Fri, Apr 8, 2011 at 12:18 PM, Stefan Behnel <stefan_ml at behnel.de> wrote:

Ok, sure let's try.


I think you meant execfile. Also, how do I make sure that somefile.py
is also compiled?


ok, fine.


We can agree to disagree on this one.




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Maciej Fijalkowski, 08.04.2011 13:37:

Ah, yes. Untested. ;)



It's not getting compiled because it's not getting imported. Maybe we 
should discuss the exact setup for speed.pypy.org in private e-mail.

Stefan




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 04/07/2011 07:52 PM, Michael Foord wrote:


Somehow I missed seeing '[GSoC]' in the subject line (the blizzard of
notification messages to the various GSoC specific lists must've
snow-blinded me :).  I'm fine with leaving Cython out-of-scope for the
GSoC effort, just not for perf.python.org as a whole.


Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk2fBLgACgkQ+gerLs4ltQ67jACgozHfglhw7QQQH42hdwXy4VLX
fXQAn33X/rq71BdZxmfsGn0swdeseHxJ
=Ttvg
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Fri, Apr 8, 2011 at 8:51 AM, Tres Seaver <tseaver at palladion.com> wrote:

We don't need a massive outstanding todo list for perf.python.org - we
need to get the current speed.pypy.org stuff made more generic for the
purposes we're aiming for and to get the hardware (on my plate) first.

Then we can talk about expanding it. I'm just begging that we not add
a bunch of stuff to a todo list for something that doesn't exist right
now.

jesse



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Anthony Scopat
Attributes: []Content: 
The way to think about this is really that Cython is its own (creole)
language that which has major intersections with the Python language.
The goal of the Cython project is to have Python be a strict subset of
Cython.  Therefore constructions such as type declarations are really
self-consistent Cython.

Because it aims to be a superset of Python, Cython is more like a two-stage
Python compiler (compile to C, then compile to assembly) than
an interpreter.  For the purposes of benchmarking, the distinction between
compiler and interpreter, as some one said above, 'dubious'.

You wouldn't want to add all of the type info or do anything in Cython that
is *not* in Python here.  That would defeat the purpose of benchmarking
where you absolutely have to compare apples to apples.

That said, despite the abstract, it seems that points 1 and 2 won't actually
be present in this GSoC.  We are not defining benchmarks.  This
project is more about porting to Python 3.

Thus, I agree with Jesse, and we shouldn't heap on more TODOs than already
exist. As people
have mentioned here, it will be easy to add Cython support once the system
is up and running.

Be Well
Anthony


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110408/1c814e78/attachment.html>



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 4/8/2011 11:32 AM, Anthony Scopatz wrote:


I agree. We should be comparing 'Python execution systems'. My 
impression is that some of what Cython does in terms of code analysis is 
similar to PyPy does, perhaps in the jit phase. So comparing PyPy and 
CPython+Cython on standard Python code is a fair and interesting comparison.


If Cython people want to modify benchmarks to show what speedup one can 
get with what effort, that is a separate issue.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: DasIc
Attributes: []Content: 
I talked to Fijal about my project last night, the result is that
basically the project as is, is not that interesting because the means
to execute the benchmarks on multiple interpreters are currently
missing.

Another point we talked about was that porting the benchmarks would
not be very useful as the interesting ones all have dependencies which
have not (yet) been ported to Python 3.x.

The first point, execution on multiple interpreters, has to be solved
or this project is pretty much pointless, therefore I've changed my
proposal to include just that. However the proposal still includes
porting the benchmarks although this is planned to happen after the
development of an application able to run the benchmarks on multiple
interpreters.

The reason for this is that even though the portable benchmarks might
not prove to be that interesting the basic stuff for porting using
2to3 would be there, making it easier to port benchmarks in the
future, as the dependencies become available under Python 3.x. However
I plan to do that after implementing the prior mentioned application
putting the application at higher priority.

This way, should I not be able to complete all my goals, it is
unlikely that anything but the porting will suffer and the project
would still produce useful results during the GSoC.

Anyway here is the current, updated, proposal:

Abstract
=======

As of now there are several benchmark suites used by Python
implementations, PyPy uses the benchmarks[1] developed for the Unladen
Swallow[2] project as well as several other benchmarks they
implemented on their own, CPython[3] uses the Unladen Swallow
benchmarks and several "crap benchmarks used for historical
reasons"[4].

This makes comparisons unnecessarily hard and causes confusion. As a
solution to this problem I propose merging the existing benchmarks -
at least those considered worth having - into a single benchmark suite
which can be shared by all implementations and ported to Python 3.x.

Another problem reported by Maciej Fijalkowski is that currenly the
way benchmarks are executed by PyPy is more or less a hack. Work will
have to be done to allow execution of the benchmarks on different
interpreters and their most recent versions (from their respective
repositories). The application for this should also be able to upload
the results to a codespeed instance such as http://speed.pypy.org.

Milestones
=========
The project can be divided into several milestones:

1. Definition of the benchmark suite. This will entail contacting
developers of Python implementations (CPython, PyPy, IronPython and
Jython), via discussion on the appropriate mailing lists. This might
be achievable as part of this proposal.
2. Merging the benchmarks. Based on the prior agreed upon definition,
the benchmarks will be merged into a single suite.
3. Implementing a system to run the benchmarks. In order to execute
the benchmarks it will be necessary to have a configurable application
which downloads the interpreters from their repositories, builds them
and executes the benchmarks with them.
4. Porting the suite to Python 3.x. The suite will be ported to 3.x
using 2to3[5], as far as possible. The usage of 2to3 will make it
easier make changes to the repository especially for those still
focusing on 2.x. It is to be expected that some benchmarks cannot be
ported due to dependencies which are not available on Python 3.x.
Those will be ignored by this project to be ported at a later time,
when the necessary requirements are met.

Start of Program (May 24)
======================

Before the coding, milestones 2 and 3, can begin it is necessary to
agree upon a set of benchmarks, everyone is happy with, as described.

Midterm Evaluation (July 12)
=======================

During the midterm I want to merge the benchmarks and implement a way
to execute them.

Final Evaluation (Aug 16)
=====================

In this period the benchmark suite will be ported. If everything works
out perfectly I will even have some time left, if there are problems I
have a buffer here.

Implementation of the Benchmark Runner
==================================

In order to run the benchmarks I propose a simple application which
can be configured to download multiple interpreters, to build them and
execute the benchmarks. The configuration could be similar to tox[6],
downloads of the interpreters could be handled using anyvc[7].

For a site such as http://speed.pypy.org a cronjob, buildbot or
whatelse is preferred, could be setup which executes the application
regularly.

Repository Handling
================

The code for the project will be developed in a Mercurial[8]
repository hosted on Bitbucket[9], both PyPy and CPython use Mercurial
and most people in the Python community should be able to use it.

Probably Asked Questions
======================

Why not use one of the existing benchmark suites for porting?

The effort will be wasted if there is no good base to build upon,
creating a new benchmark suite based upon the existing ones ensures
that.

Why not use Git/Bazaar/...?

Mercurial is used by CPython, PyPy and is fairly well known and used
in the Python community. This ensures easy accessibility for everyone.

What will happen with the Repository after GSoC/How will access to the
repository be handled?

I propose to give administrative rights to one or two representatives
of each project. Those will provide other developers with write
access.

Communication
=============

Communication of the progress will be done via Twitter[10] and my
blog[11], if desired I can also send an email with the contents of the
blog post to the mailing lists of the implementations. Furthermore I
am usually quick to answer via IRC(DasIch on freenode), Twitter or
E-Mail(dasdasich at gmail.com) if anyone has any questions.

Contact to the mentor can be established via the means mentioned above
or via Skype.

About Me
========
My name is Daniel Neuh?user, I am 19 years old and currently a student
at the Bergstadt-Gymnasium L?denscheid[12]. I started programming
(with Python) about 4 years ago and became a member of the Pocoo
Team[13] after successfully participating in the Google Summer of Code
last year, during which I ported Sphinx[14] to Python 3.x and
implemented an algorithm to diff abstract syntax trees to preserve
comments and translated strings which has been used by the other GSoC
projects targeting Sphinx.


.. [1]: https://bitbucket.org/pypy/benchmarks/src
.. [2]: http://code.google.com/p/unladen-swallow/
.. [3]: http://hg.python.org/benchmarks/file/tip/performance
.. [4]: http://hg.python.org/benchmarks/file/62e754c57a7f/performance/README
.. [5]: http://docs.python.org/library/2to3.html
.. [6]: http://codespeak.net/tox/
.. [7]: http://anyvc.readthedocs.org/en/latest/?redir
.. [8]: http://mercurial.selenic.com/
.. [9]: https://bitbucket.org/
.. [10]: http://twitter.com/#!/DasIch
.. [11]: http://dasdasich.blogspot.com/
.. [12]: http://bergstadt-gymnasium.de/
.. [13]: http://www.pocoo.org/team/#daniel-neuhauser
.. [14]: http://sphinx.pocoo.org/



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Fri, Apr 8, 2011 at 11:22 AM, Stefan Behnel <stefan_ml at behnel.de> wrote:

Just to clarify - the crucial word here is Python and not the
interpreter. I don't care myself if it's an interpreter or a compiler,
I do care if it can pass the python test suite (modulo things that are
known to be implementation details and agreed upon).

How far is Cython from passing the full test suite? Are there known
incompatibilities that would be considered wontfix?

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Maciej Fijalkowski, 11.04.2011 11:39:

Psyco is also not a Python implementation. It doesn't work without CPython, 
just like Cython. But I doubt that anyone would seriously argue for 
excluding Psyco from a Python speed comparison. That was my point here.



According to our CI server, we currently have 255 failing tests out of 7094 
in Python 2.7.

https://sage.math.washington.edu:8091/hudson/view/cython-devel/job/cython-devel-tests-pyregr-py27-c/

This is not completely accurate as a) it only includes compiling the test 
module, and e.g. not the stdlib modules that are being tested, and b) the 
total number of tests we see depends on how many test modules we can 
compile in order to import and run the contained tests. It also doesn't 
mean that we have >200 compatibility problems, the majority of failures 
tends to be because of just a hand full of bugs.

Another measure is that Cython can currently compile some 160 modules out 
of a bit less than 200 in Django (almost all failures due to one bug about 
incompatibilities between PyCFunction and Python functions) and an 
(untested!) 1219 out of 1538 modules in the stdlib. We haven't put that 
together yet in order to actually test the compiled stdlib modules. That'll 
come.



There are known incompatibilities that are considered bugs. There are no 
"wontfix" bugs when it comes to Python compatibility. But there are 
obviously developer priorities when it comes to fixing bugs. Cython is a 
lot more than just a Python compiler (such as a programming language that 
keeps people from writing C code), so there are also bugs and feature 
requests apart from Python semantics that we consider more important to 
fix. It's not like all bugs on CPython's bug tracker would get closed 
within a day or so.

Stefan




----------------------------------------
Subject:
[Python-Dev] [GSoC] Developing a benchmark suite (for Python
	3.x)
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Apr 11, 2011 at 12:43 PM, Stefan Behnel <stefan_ml at behnel.de> wrote:

Sure, that was more of a question "do you consider cython
compatibility an issue?". I'm sure there are bugs.


