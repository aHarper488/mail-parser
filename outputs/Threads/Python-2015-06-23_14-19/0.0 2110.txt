
============================================================================
Subject: [Python-Dev] imaplib: Time2Internaldate() returns localized strings
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] imaplib: Time2Internaldate() returns localized strings
----------------------------------------
Author: Sebastian Spaet
Attributes: []Content: 
Hi there,

imaplib has an issue (well, more than one :-)), but in this specific
case there is even a patch that I want to lobby.

http://bugs.python.org/issue11024
imaplib: Time2Internaldate() returns localized strings while the IMAP
RFC requires English month names. As that function is used for every
.append(), this outright prevents some international users during some
months from adding messages. :-)

There is a patch that fixes imaplib and I would be grateful if someone
with commit access would have a look at it, and consider pushing it in.

Thank you so much,
Sebastian

P.S., please keep me CC'd

