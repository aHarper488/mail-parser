
============================================================================
Subject: [Python-Dev] Approach for constructing Global Variables for Python
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Approach for constructing Global Variables for Python
----------------------------------------
Author: ali mus
Attributes: []Content: 
Hi All:
?
I would like to propose a solution for a proper use of Global Variables. My 
assumption came from the fact that most databases are using share memory to 
construct on the main memory a complete block of their processes and resources. 
Construction a method for utilizing Global Variable using block concept for 
common block or sub-block that can be share in any module or main program (i.e. 
main application and their modules).
?
The solution implemented in the attached paper can ignite this idea and make you 
elaborate it to make better use of Python.
?
Best regards.
--------------------------------------------------------------------------------
Ali Abdelaziz Musa
Saudi Telecom Company (Technical Advisor)
Mob: (+9665)50570742, Hm:(+9661) 405-9425, Off:(+9661) 452-5539
Email: amusa07 at yahoo.com ?


      
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110113/52962656/attachment-0001.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: --static--bg_snowblue_1.gif
Type: image/gif
Size: 7874 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110113/52962656/attachment-0001.gif>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: Paper_GlobalVariable.pdf
Type: application/pdf
Size: 144594 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110113/52962656/attachment-0001.pdf>

