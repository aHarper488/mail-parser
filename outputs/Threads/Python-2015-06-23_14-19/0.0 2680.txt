
============================================================================
Subject: [Python-Dev] Python 3 porting
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Python 3 porting
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
I want to take this opportunity to make folks aware of several Python 3
porting initiatives and resources.

In Ubuntu 12.10, we are going to be making a big push to target all the
applications and libraries on the desktop CDs to Python 3.  While this is a
goal of Ubuntu, the intent really is to work with the wider Python community
(i.e. *you*!) to help drive more momentum toward Python 3.

We can't do this alone, and we hope you will participate.  While we on Ubuntu
have our own list of priorities, of course we want to push as much of this as
possible upstream so that everyone can benefit, regardless of platform.  We
also want to help spread the word about Python 3, and how easy it can be to
support it.

One of the best ways to get involved is to join the 'python-porting' mailing
list:

    http://mail.python.org/mailman/listinfo/python-porting

which is *the* forum for discussion issues, getting help, and coordinating
with others on Python 3 ports of your favorite upstream projects.

I've also resurrected the #python3 IRC channel on Freenode, for those of you
who want to provide or receive more real-time help.

Web resources for porters include:

 * http://getpython3.com/
   General Python 3 resources, forkable on github

 * http://python3porting.com/
   Lennart Regebro's excellent in-depth porting guide

 * https://wiki.ubuntu.com/Python/3
   My quick guide for porting

 * https://wiki.ubuntu.com/Python/FoundationsQPythonVersions
   Detailed plans for Python 3 on Ubuntu 12.10

 * http://tinyurl.com/6vm3egu
   My recent blog post on Ubuntu's plans for Python 3

 * http://tinyurl.com/7dsyywo
   Ubuntu's top priorities for porting, as a shared Google doc spreadsheet

Many of these pages have additional links and resources for porting, and can
help you find packages that need resources in getting to Python 3.

At the Ubuntu Developer Summit in Oakland, California, May 7-11, 2012, we'll
also be holding some sessions on Python 3, so if you're in the area, please
come by.

    http://uds.ubuntu.com/

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120425/5a50959c/attachment.pgp>

