
============================================================================
Subject: [Python-Dev] ctypes: alignment of (simple) types
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] ctypes: alignment of (simple) types
----------------------------------------
Author: Michael Wall
Attributes: []Content: 
Hi all,

gcc allows to set alignments for typedefs like:

typedef double MyDouble __attribute__((__aligned__(8)));

Now if i use this new type within a structure:

struct s {
	char c;
	MyDouble d;
};

The following holds: sizeof(struct s) == 16 and offsetof(struct s, d) == 8.

ctypes doesn't seem to support this, although i saw a 'padded' function on
the ctypes-users mailinglist which dynamically insert padding fields. I would 
consider this more or less a hack :)

What do you think about adding a special attribute '_align_' which, if set for
a data type overrides the hardcoded align property of that type?

-- 
Michael



----------------------------------------
Subject:
[Python-Dev] ctypes: alignment of (simple) types
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
Hi,

2011/11/7 Michael Walle <michael at walle.cc>


It's a good idea.  But you should also consider the other feature requests
around custom alignments.
IMO a good thing would be a way to specify a function that computes sizes
and alignments, that one can override to implement specific compiler
features.

-- 
Amaury Forgeot d'Arc
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111108/69f0ba87/attachment.html>

