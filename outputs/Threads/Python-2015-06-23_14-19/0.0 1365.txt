
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (2.7): Fix a few hyphens
 in argparse.rst.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.7): Fix a few hyphens
 in argparse.rst.
----------------------------------------
Author: =?UTF-8?Q?=C3=89ric_Araujo?
Attributes: []Content: 
 Hi,



 I believe that change should be reverted.  ?argument parsing library? 
 is
 a noun determined (qualified) by another noun itself determined by a
 noun, not by an adjective.


 You changed ?arg? to ?argument? in one place but not throughout.  I
 think it?s best to use only full words in the docs (and for names in
 code, as recommended by PEP 8 :).

 Regards

