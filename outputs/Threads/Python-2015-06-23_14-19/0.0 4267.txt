
============================================================================
Subject: [Python-Dev] built-in Python test runner (was: Python Language
	Summit at PyCon: Agenda)
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] built-in Python test runner (was: Python Language
	Summit at PyCon: Agenda)
----------------------------------------
Author: Glyp
Attributes: []Content: 
On Mar 4, 2013, at 11:13 PM, Robert Collins <robertc at robertcollins.net> wrote:


Some of trial's lessons might be also useful for the stdlib going forward, given the hope of doing some event-loop stuff in the core.

But, I feel like this might be too much to cover at the language summit; there could be a test frameworks summit of its own, of about equivalent time and scope, and we'd still have a lot to discuss.

Is there a unit testing SIG someone from Twisted ought to be a member of, to represent Trial, and to get consensus on these points going forward?

-glyph

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130305/d81f97ff/attachment.html>



----------------------------------------
Subject:
[Python-Dev] built-in Python test runner (was: Python Language
	Summit at PyCon: Agenda)
----------------------------------------
Author: Michael Foor
Attributes: []Content: 

On 5 Mar 2013, at 07:19, Antoine Pitrou <solipsis at pitrou.net> wrote:



For Python 3.3 onwards "python -m unittest" does run test discovery by default. However if you want to provide parameters you still need the "discover" subcommand to disambiguate from the other command line options. So I agree - a shorthand command would be an improvement.

Michael





--
http://www.voidspace.org.uk/


May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing 
http://www.sqlite.org/different.html








----------------------------------------
Subject:
[Python-Dev] built-in Python test runner (was: Python Language
	Summit at PyCon: Agenda)
----------------------------------------
Author: Michael Foor
Attributes: []Content: 

On 5 Mar 2013, at 09:02, Glyph <glyph at twistedmatrix.com> wrote:



The testing-on-python mailing list is probably the best place (and if doesn't have that status already I'd be keen to elevate it to "official sig for Python testing issues" status).

	http://lists.idyll.org/listinfo/testing-in-python

Like the "massively distributed testing" use case, I'd be very happy for the standard library testing capabilities to better support this use case - but I wouldn't like to design their apis around that as the sole use case. :-)

Michael



--
http://www.voidspace.org.uk/


May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing 
http://www.sqlite.org/different.html






