
============================================================================
Subject: [Python-Dev] Code cleanups in stable branches?
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Code cleanups in stable branches?
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Oct 31, 2011, at 06:23 PM, ?ric Araujo wrote:


I hope not.  Sure, if they fix actual bugs, that's fine, but as MvL often
points out, even innocent looking changes can break code *somewhere*.  We
don't lose by being conservative with our stable branches.

-Barry



----------------------------------------
Subject:
[Python-Dev] Code cleanups in stable branches?
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Nov 1, 2011, at 1:31 PM, Barry Warsaw wrote:


I concur with Barry and MvL.   Random code cleanups increase the risk of introducing new bugs.


Raymond




----------------------------------------
Subject:
[Python-Dev] Code cleanups in stable branches?
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Nick and Brett share the opinion that some code cleanups can be
considered bugfixes, whereas MvL, Barry and Raymond defend that we never
know what can get broken and it?s not worth risking it.

I have added a comment on #13283 (removal of two unused variable in
locale.py) to restate this policy, but haven?t asked for a revert; I did
not comment on #10519 (Avoid unnecessary recursive function calls), as
it can be considered a bugfix and Raymond took part in the discussion
and thus implicitly approved the change.

Regards



----------------------------------------
Subject:
[Python-Dev] Code cleanups in stable branches?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, Nov 5, 2011 at 3:05 AM, ?ric Araujo <merwok at netwok.org> wrote:

There's always going to be a grey area where it's a judgment call - my
opinion is basically the same as what you state in your second
paragraph.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Code cleanups in stable branches?
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi,



I thought that patches that clean up code but don?t fix actual bugs were
not done in stable branches.  Has this changed?

(The first commit quoted above may be a performance fix, which would be
okay IIRC.)

Regards



----------------------------------------
Subject:
[Python-Dev] Code cleanups in stable branches?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Removing dead code and bypassing redundant code are both reasonable bug
fixes. The kind of change to be avoided is gratuitous replacement of older
idioms with newer ones.

--
Nick Coghlan (via Gmail on Android, so likely to be more terse than usual)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111101/270470e7/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Code cleanups in stable branches?
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Oct 31, 2011 at 13:24, Nick Coghlan <ncoghlan at gmail.com> wrote:

What Nick said as I was in the middle of typing when he sent this. =)

-Brett


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111031/3d773eb2/attachment.html>

