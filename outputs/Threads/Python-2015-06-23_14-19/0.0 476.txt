
============================================================================
Subject: [Python-Dev] --enabled-shared broken on freebsd5?
Post Count: 40
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
(This may occur on more platforms - I can test on more unix platforms
if the consensus is this is an actual problem and I'm not just a nut)

On freebsd5, if you do a simple ./configure --enable-shared in current
(2.7) trunk, your python shared library will build properly, but all
modules will fail to find the shared library and thus fail to build:

gcc -shared build/temp.freebsd-5.3-RELEASE-i386-2.7/u1/Python/Python-2.7a1/Modules/_struct.o
   -L/u1/tmp/python2.7a1/lib -L/usr/local/lib -lpython2.7 -o
build/lib.freebsd-5.3-RELEASE-i386-2.7/_struct.so
/usr/bin/ld: cannot find -lpython2.7
building '_ctypes_test' extension
...

This of course is because libpython2.7.so is in the current directory
and not (yet) installed in /usr/local/lib.  I've made a very simple
fix for this problem that works, but at least to me smells a bit
funny, which is to modify setup.py to add the following to
detect_modules():

        # If we did --enable-shared, we need to be able to find the library
        # we just built in order to build the modules.
        if platform == 'freebsd5':
            add_dir_to_list(self.compiler_obj.library_dirs, '.')


Which brings me to a few questions:

a) Does this seem like a real problem, or am I missing something obvious?

b) Does this fix seem like the sensible thing to do?  (it seems at
least that we ought to check that the user configured --enable-shared
and only set -L. in that case, if that's possible)

Setting --enable-shared when you actually have a libpython2.7.so in
/usr/local/lib (or whatever --prefix you've selected) is possibly even
more dangerous, because it may succeed in linking against a
differently-built library than what you intended.

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 16:14, Nicholas Bastin <nick.bastin at gmail.com> wrote:

One minor correction - as you could see from the compile line, the
actual --prefix in this case is /u1/tmp/python2.7a1, but the libraries
obviously aren't installed there yet either.  Perhaps a better fix
than setting -L. would be to put the shared library in
build/lib.freebsd-5.3-RELEASE-i386-2.7 and add that to the library
path for the linker (the build creates this directory, but installs
nothing in it).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

No. Linking in setup.py should use the same options as if the module
was built as *shared* through Modules/Setup, which, IIUC, should use
BLDLIBRARY.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 17:21, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Thanks for that pointer, that makes much more sense.  Indeed,
BLDLIBRARY on FreeBSD* is set to '-L. -lpython$(VERSION)' if you set
--enable-shared, but somehow that piece of information doesn't
propagate into the module build.  More investigation to be done...

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
I think this problem probably needs to move over to distutils-sig, as
it doesn't seem to be specific to the way that Python itself uses
distutils.  distutils.command.build_ext tests for Py_ENABLE_SHARED on
linux and solaris and automatically adds '.' to the library_dirs, and
I suspect it just needs to do this on FreeBSD as well (adding bsd to
the list of platforms for which this is performed "solves" the
problem, but I don't pretend to know enough about either distutils or
freebsd to determine if this is the correct solution).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Nicholas Bastin wrote:

I wouldn't say it needed discussion on the SIG:  just create a bug
report, with the tentative patch you have worked out, and get it
assigned to Tarek.


Tres.
- --
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org

iEYEARECAAYFAktGsdQACgkQ+gerLs4ltQ5BMQCgtV8snMXH/6dDwgdN4sIJljLd
koYAoKq6c0tKsRSrITHcygu4Od9FVzF5
=BJaE
-----END PGP SIGNATURE-----





----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Nicholas Bastin wrote:

I'm fairly skeptical that anybody on distutils SIG is interested in
details of the Python build process...

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On Fri, Jan 08, 2010 at 10:11:51AM +0100, "Martin v. L?wis" wrote:

Uh, hum.  Unfounded skepticism.  ;-)
But as said filing a bug sounds better in this case.

Regards
Floris

-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
(This may occur on more platforms - I can test on more unix platforms
if the consensus is this is an actual problem and I'm not just a nut)

On freebsd5, if you do a simple ./configure --enable-shared in current
(2.7) trunk, your python shared library will build properly, but all
modules will fail to find the shared library and thus fail to build:

gcc -shared build/temp.freebsd-5.3-RELEASE-i386-2.7/u1/Python/Python-2.7a1/Modules/_struct.o
   -L/u1/tmp/python2.7a1/lib -L/usr/local/lib -lpython2.7 -o
build/lib.freebsd-5.3-RELEASE-i386-2.7/_struct.so
/usr/bin/ld: cannot find -lpython2.7
building '_ctypes_test' extension
...

This of course is because libpython2.7.so is in the current directory
and not (yet) installed in /usr/local/lib.  I've made a very simple
fix for this problem that works, but at least to me smells a bit
funny, which is to modify setup.py to add the following to
detect_modules():

        # If we did --enable-shared, we need to be able to find the library
        # we just built in order to build the modules.
        if platform == 'freebsd5':
            add_dir_to_list(self.compiler_obj.library_dirs, '.')


Which brings me to a few questions:

a) Does this seem like a real problem, or am I missing something obvious?

b) Does this fix seem like the sensible thing to do?  (it seems at
least that we ought to check that the user configured --enable-shared
and only set -L. in that case, if that's possible)

Setting --enable-shared when you actually have a libpython2.7.so in
/usr/local/lib (or whatever --prefix you've selected) is possibly even
more dangerous, because it may succeed in linking against a
differently-built library than what you intended.

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 16:14, Nicholas Bastin <nick.bastin at gmail.com> wrote:

One minor correction - as you could see from the compile line, the
actual --prefix in this case is /u1/tmp/python2.7a1, but the libraries
obviously aren't installed there yet either.  Perhaps a better fix
than setting -L. would be to put the shared library in
build/lib.freebsd-5.3-RELEASE-i386-2.7 and add that to the library
path for the linker (the build creates this directory, but installs
nothing in it).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

No. Linking in setup.py should use the same options as if the module
was built as *shared* through Modules/Setup, which, IIUC, should use
BLDLIBRARY.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 17:21, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Thanks for that pointer, that makes much more sense.  Indeed,
BLDLIBRARY on FreeBSD* is set to '-L. -lpython$(VERSION)' if you set
--enable-shared, but somehow that piece of information doesn't
propagate into the module build.  More investigation to be done...

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
I think this problem probably needs to move over to distutils-sig, as
it doesn't seem to be specific to the way that Python itself uses
distutils.  distutils.command.build_ext tests for Py_ENABLE_SHARED on
linux and solaris and automatically adds '.' to the library_dirs, and
I suspect it just needs to do this on FreeBSD as well (adding bsd to
the list of platforms for which this is performed "solves" the
problem, but I don't pretend to know enough about either distutils or
freebsd to determine if this is the correct solution).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Nicholas Bastin wrote:

I wouldn't say it needed discussion on the SIG:  just create a bug
report, with the tentative patch you have worked out, and get it
assigned to Tarek.


Tres.
- --
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org

iEYEARECAAYFAktGsdQACgkQ+gerLs4ltQ5BMQCgtV8snMXH/6dDwgdN4sIJljLd
koYAoKq6c0tKsRSrITHcygu4Od9FVzF5
=BJaE
-----END PGP SIGNATURE-----





----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Nicholas Bastin wrote:

I'm fairly skeptical that anybody on distutils SIG is interested in
details of the Python build process...

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On Fri, Jan 08, 2010 at 10:11:51AM +0100, "Martin v. L?wis" wrote:

Uh, hum.  Unfounded skepticism.  ;-)
But as said filing a bug sounds better in this case.

Regards
Floris

-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
(This may occur on more platforms - I can test on more unix platforms
if the consensus is this is an actual problem and I'm not just a nut)

On freebsd5, if you do a simple ./configure --enable-shared in current
(2.7) trunk, your python shared library will build properly, but all
modules will fail to find the shared library and thus fail to build:

gcc -shared build/temp.freebsd-5.3-RELEASE-i386-2.7/u1/Python/Python-2.7a1/Modules/_struct.o
   -L/u1/tmp/python2.7a1/lib -L/usr/local/lib -lpython2.7 -o
build/lib.freebsd-5.3-RELEASE-i386-2.7/_struct.so
/usr/bin/ld: cannot find -lpython2.7
building '_ctypes_test' extension
...

This of course is because libpython2.7.so is in the current directory
and not (yet) installed in /usr/local/lib.  I've made a very simple
fix for this problem that works, but at least to me smells a bit
funny, which is to modify setup.py to add the following to
detect_modules():

        # If we did --enable-shared, we need to be able to find the library
        # we just built in order to build the modules.
        if platform == 'freebsd5':
            add_dir_to_list(self.compiler_obj.library_dirs, '.')


Which brings me to a few questions:

a) Does this seem like a real problem, or am I missing something obvious?

b) Does this fix seem like the sensible thing to do?  (it seems at
least that we ought to check that the user configured --enable-shared
and only set -L. in that case, if that's possible)

Setting --enable-shared when you actually have a libpython2.7.so in
/usr/local/lib (or whatever --prefix you've selected) is possibly even
more dangerous, because it may succeed in linking against a
differently-built library than what you intended.

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 16:14, Nicholas Bastin <nick.bastin at gmail.com> wrote:

One minor correction - as you could see from the compile line, the
actual --prefix in this case is /u1/tmp/python2.7a1, but the libraries
obviously aren't installed there yet either.  Perhaps a better fix
than setting -L. would be to put the shared library in
build/lib.freebsd-5.3-RELEASE-i386-2.7 and add that to the library
path for the linker (the build creates this directory, but installs
nothing in it).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

No. Linking in setup.py should use the same options as if the module
was built as *shared* through Modules/Setup, which, IIUC, should use
BLDLIBRARY.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 17:21, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Thanks for that pointer, that makes much more sense.  Indeed,
BLDLIBRARY on FreeBSD* is set to '-L. -lpython$(VERSION)' if you set
--enable-shared, but somehow that piece of information doesn't
propagate into the module build.  More investigation to be done...

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
I think this problem probably needs to move over to distutils-sig, as
it doesn't seem to be specific to the way that Python itself uses
distutils.  distutils.command.build_ext tests for Py_ENABLE_SHARED on
linux and solaris and automatically adds '.' to the library_dirs, and
I suspect it just needs to do this on FreeBSD as well (adding bsd to
the list of platforms for which this is performed "solves" the
problem, but I don't pretend to know enough about either distutils or
freebsd to determine if this is the correct solution).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Nicholas Bastin wrote:

I wouldn't say it needed discussion on the SIG:  just create a bug
report, with the tentative patch you have worked out, and get it
assigned to Tarek.


Tres.
- --
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org

iEYEARECAAYFAktGsdQACgkQ+gerLs4ltQ5BMQCgtV8snMXH/6dDwgdN4sIJljLd
koYAoKq6c0tKsRSrITHcygu4Od9FVzF5
=BJaE
-----END PGP SIGNATURE-----





----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Nicholas Bastin wrote:

I'm fairly skeptical that anybody on distutils SIG is interested in
details of the Python build process...

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On Fri, Jan 08, 2010 at 10:11:51AM +0100, "Martin v. L?wis" wrote:

Uh, hum.  Unfounded skepticism.  ;-)
But as said filing a bug sounds better in this case.

Regards
Floris

-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
(This may occur on more platforms - I can test on more unix platforms
if the consensus is this is an actual problem and I'm not just a nut)

On freebsd5, if you do a simple ./configure --enable-shared in current
(2.7) trunk, your python shared library will build properly, but all
modules will fail to find the shared library and thus fail to build:

gcc -shared build/temp.freebsd-5.3-RELEASE-i386-2.7/u1/Python/Python-2.7a1/Modules/_struct.o
   -L/u1/tmp/python2.7a1/lib -L/usr/local/lib -lpython2.7 -o
build/lib.freebsd-5.3-RELEASE-i386-2.7/_struct.so
/usr/bin/ld: cannot find -lpython2.7
building '_ctypes_test' extension
...

This of course is because libpython2.7.so is in the current directory
and not (yet) installed in /usr/local/lib.  I've made a very simple
fix for this problem that works, but at least to me smells a bit
funny, which is to modify setup.py to add the following to
detect_modules():

        # If we did --enable-shared, we need to be able to find the library
        # we just built in order to build the modules.
        if platform == 'freebsd5':
            add_dir_to_list(self.compiler_obj.library_dirs, '.')


Which brings me to a few questions:

a) Does this seem like a real problem, or am I missing something obvious?

b) Does this fix seem like the sensible thing to do?  (it seems at
least that we ought to check that the user configured --enable-shared
and only set -L. in that case, if that's possible)

Setting --enable-shared when you actually have a libpython2.7.so in
/usr/local/lib (or whatever --prefix you've selected) is possibly even
more dangerous, because it may succeed in linking against a
differently-built library than what you intended.

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 16:14, Nicholas Bastin <nick.bastin at gmail.com> wrote:

One minor correction - as you could see from the compile line, the
actual --prefix in this case is /u1/tmp/python2.7a1, but the libraries
obviously aren't installed there yet either.  Perhaps a better fix
than setting -L. would be to put the shared library in
build/lib.freebsd-5.3-RELEASE-i386-2.7 and add that to the library
path for the linker (the build creates this directory, but installs
nothing in it).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

No. Linking in setup.py should use the same options as if the module
was built as *shared* through Modules/Setup, which, IIUC, should use
BLDLIBRARY.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 17:21, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Thanks for that pointer, that makes much more sense.  Indeed,
BLDLIBRARY on FreeBSD* is set to '-L. -lpython$(VERSION)' if you set
--enable-shared, but somehow that piece of information doesn't
propagate into the module build.  More investigation to be done...

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
I think this problem probably needs to move over to distutils-sig, as
it doesn't seem to be specific to the way that Python itself uses
distutils.  distutils.command.build_ext tests for Py_ENABLE_SHARED on
linux and solaris and automatically adds '.' to the library_dirs, and
I suspect it just needs to do this on FreeBSD as well (adding bsd to
the list of platforms for which this is performed "solves" the
problem, but I don't pretend to know enough about either distutils or
freebsd to determine if this is the correct solution).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Nicholas Bastin wrote:

I wouldn't say it needed discussion on the SIG:  just create a bug
report, with the tentative patch you have worked out, and get it
assigned to Tarek.


Tres.
- --
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org

iEYEARECAAYFAktGsdQACgkQ+gerLs4ltQ5BMQCgtV8snMXH/6dDwgdN4sIJljLd
koYAoKq6c0tKsRSrITHcygu4Od9FVzF5
=BJaE
-----END PGP SIGNATURE-----





----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Nicholas Bastin wrote:

I'm fairly skeptical that anybody on distutils SIG is interested in
details of the Python build process...

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On Fri, Jan 08, 2010 at 10:11:51AM +0100, "Martin v. L?wis" wrote:

Uh, hum.  Unfounded skepticism.  ;-)
But as said filing a bug sounds better in this case.

Regards
Floris

-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
(This may occur on more platforms - I can test on more unix platforms
if the consensus is this is an actual problem and I'm not just a nut)

On freebsd5, if you do a simple ./configure --enable-shared in current
(2.7) trunk, your python shared library will build properly, but all
modules will fail to find the shared library and thus fail to build:

gcc -shared build/temp.freebsd-5.3-RELEASE-i386-2.7/u1/Python/Python-2.7a1/Modules/_struct.o
   -L/u1/tmp/python2.7a1/lib -L/usr/local/lib -lpython2.7 -o
build/lib.freebsd-5.3-RELEASE-i386-2.7/_struct.so
/usr/bin/ld: cannot find -lpython2.7
building '_ctypes_test' extension
...

This of course is because libpython2.7.so is in the current directory
and not (yet) installed in /usr/local/lib.  I've made a very simple
fix for this problem that works, but at least to me smells a bit
funny, which is to modify setup.py to add the following to
detect_modules():

        # If we did --enable-shared, we need to be able to find the library
        # we just built in order to build the modules.
        if platform == 'freebsd5':
            add_dir_to_list(self.compiler_obj.library_dirs, '.')


Which brings me to a few questions:

a) Does this seem like a real problem, or am I missing something obvious?

b) Does this fix seem like the sensible thing to do?  (it seems at
least that we ought to check that the user configured --enable-shared
and only set -L. in that case, if that's possible)

Setting --enable-shared when you actually have a libpython2.7.so in
/usr/local/lib (or whatever --prefix you've selected) is possibly even
more dangerous, because it may succeed in linking against a
differently-built library than what you intended.

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 16:14, Nicholas Bastin <nick.bastin at gmail.com> wrote:

One minor correction - as you could see from the compile line, the
actual --prefix in this case is /u1/tmp/python2.7a1, but the libraries
obviously aren't installed there yet either.  Perhaps a better fix
than setting -L. would be to put the shared library in
build/lib.freebsd-5.3-RELEASE-i386-2.7 and add that to the library
path for the linker (the build creates this directory, but installs
nothing in it).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

No. Linking in setup.py should use the same options as if the module
was built as *shared* through Modules/Setup, which, IIUC, should use
BLDLIBRARY.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
On Wed, Jan 6, 2010 at 17:21, "Martin v. L?wis" <martin at v.loewis.de> wrote:

Thanks for that pointer, that makes much more sense.  Indeed,
BLDLIBRARY on FreeBSD* is set to '-L. -lpython$(VERSION)' if you set
--enable-shared, but somehow that piece of information doesn't
propagate into the module build.  More investigation to be done...

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Nicholas Basti
Attributes: []Content: 
I think this problem probably needs to move over to distutils-sig, as
it doesn't seem to be specific to the way that Python itself uses
distutils.  distutils.command.build_ext tests for Py_ENABLE_SHARED on
linux and solaris and automatically adds '.' to the library_dirs, and
I suspect it just needs to do this on FreeBSD as well (adding bsd to
the list of platforms for which this is performed "solves" the
problem, but I don't pretend to know enough about either distutils or
freebsd to determine if this is the correct solution).

--
Nick




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Nicholas Bastin wrote:

I wouldn't say it needed discussion on the SIG:  just create a bug
report, with the tentative patch you have worked out, and get it
assigned to Tarek.


Tres.
- --
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org

iEYEARECAAYFAktGsdQACgkQ+gerLs4ltQ5BMQCgtV8snMXH/6dDwgdN4sIJljLd
koYAoKq6c0tKsRSrITHcygu4Od9FVzF5
=BJaE
-----END PGP SIGNATURE-----





----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 
Nicholas Bastin wrote:

I'm fairly skeptical that anybody on distutils SIG is interested in
details of the Python build process...

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] --enabled-shared broken on freebsd5?
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On Fri, Jan 08, 2010 at 10:11:51AM +0100, "Martin v. L?wis" wrote:

Uh, hum.  Unfounded skepticism.  ;-)
But as said filing a bug sounds better in this case.

Regards
Floris

-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org


