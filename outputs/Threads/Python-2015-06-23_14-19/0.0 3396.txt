
============================================================================
Subject: [Python-Dev] [Issue1531415] Using PyErr_WarnEx on parsetok
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Issue1531415] Using PyErr_WarnEx on parsetok
----------------------------------------
Author: =?UTF-8?Q?Michele_Orr=C3=B9?
Attributes: []Content: 
As pointed by Sean Reifschneider in issue 1531415,  I'm writing this
mail mainly to ask for advices concerning python's makefile.

Currently, Parser/parsetok.c writes directly to stderr in case no more
memory is avaible. So, it would be nice? to use, instead of a raw
printf, the functions provided by Python/_warnings.c (PyErr_NoMemory
and/or PyErr_WarnEx). This, right now, leads to a circular dependency,
as described here: http://bugs.python.org/msg154939 .

So far I've seen some functions present both in Python/ and pgenmain.c
: PyErr_Occurred(), Py_FatalError(const char *msg), Py_Exit(int).
This means a dirty alternative could be to implement another function
PyErr_WarnEx; but probably there is a better way to organize the
makefile, becouse currently I'm using the entire $(PYTHON_OBJS) (seems
needed by warnigs.o).

This is the first time I run into python c code, so please be patient :)
-- 
?

