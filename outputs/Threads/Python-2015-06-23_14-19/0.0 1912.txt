
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Fix reST label for
	collections ABCs.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Fix reST label for
	collections ABCs.
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Jun 3, 2011, at 10:27 AM, eric.araujo wrote:

I think the users are better served by links to collections.abc, io.abc, etc.
For the most part, glossary readers will be interested in actual
abstract classes than in the underlying implementation.

IOW, I believe this edit makes the docs worse rather than better.


Raymond
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110603/43b3b816/attachment.html>

