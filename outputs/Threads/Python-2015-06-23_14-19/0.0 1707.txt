
============================================================================
Subject: [Python-Dev] Where are Python 2.5.5 binaries for Windows?
Post Count: 6
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Where are Python 2.5.5 binaries for Windows?
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
I need Python 2.5.5 binaries to run Google AppEngine SDK 1.4.1 on
Windows, but can't find them on
http://www.python.org/download/releases/2.5.5/

Why are they removed?
--
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Where are Python 2.5.5 binaries for Windows?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Tue, Jan 11, 2011 at 13:04, anatoly techtonik <techtonik at gmail.com>wrote:



Nothing was removed. From that page: "This is a source-only release that
only includes security fixes."
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110111/fd81daa4/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Where are Python 2.5.5 binaries for Windows?
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Tue, Jan 11, 2011 at 9:08 PM, Brian Curtin <brian.curtin at gmail.com> wrote:

Oh. Thanks. The page should have a more prominent Download section
with a direct link to a page with previous release binaries. Not many
people know English to figure this out from the text even if they are
able to follow AppEngine tutorials in Russian.
--
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Where are Python 2.5.5 binaries for Windows?
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Tue, Jan 11, 2011 at 13:56, anatoly techtonik <techtonik at gmail.com>wrote:



That's right next to the other sentence I mentioned: "The last full bug-fix
release of Python 2.5 was Python
2.5.4<http://www.python.org/download/releases/2.5.4/>
."

Not many

There hasn't been a problem with this in the past that I know of, so I
suspect a lot of people actually do understand the page and English, but I
imagine translations of the page might be accepted.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110111/b3686143/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Where are Python 2.5.5 binaries for Windows?
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
On Tue, Jan 11, 2011 at 10:08 PM, Brian Curtin <brian.curtin at gmail.com> wrote:

That's not consistent with the download section present on all other pages.


Of course you can't know about problems that users complain about in
Russian, but ok, the page can be translated.

BTW, the page http://www.python.org/download/releases/2.5.5/ lists
wrong latest release of Python 2.7 version.
-- 
anatoly t.



----------------------------------------
Subject:
[Python-Dev] Where are Python 2.5.5 binaries for Windows?
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Jan 12, 2011 at 9:54 AM, anatoly techtonik <techtonik at gmail.com> wrote:

Deliberately so - we don't really want people to download those binary
releases while naively thinking they're getting all the security fixes
from the source-only updates (see also the notice at the top of the
2.5.4 page).

Regards,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

