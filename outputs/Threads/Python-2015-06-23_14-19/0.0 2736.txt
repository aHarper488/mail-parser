
============================================================================
Subject: [Python-Dev] Installation on Macs
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Installation on Macs
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 
On Mountain Lion, the default security settings only allow installation of applications downloaded from the Mac App Stored and "identified developers".

We need to either become an "identified developer" or include some instructions on how to change the security settings (System Preference -- General -- Unlock --Select the "Anywhere" radio button -- Install Python -- Restore the original settings -- and Relock).  Changing the security settings isn't appealing because 1) it weakens the user's security 2) it involves multiple steps and 3) the user will see an unsettling warnings along the way. 

Another unrelated issue is that the instructions for updating Tcl/Tk are problematic.  In the past few months, I've witnessed hundreds of people unsuccessfully trying follow the instructions and having an immediate unpleasant out-of-the-box experience when IDLE crashes.  I suggest that we stop being so indirect about the chain of footnotes and links leading to a Tcl/Tk download.  I would like to see a direct Tcl/Tk updater link side-by-side with our Python installer link at http://www.python.org/download/releases/2.7.3/

Someone did add a note the the IDLE startup screen to the effect of:  "WARNING: The version of Tcl/Tk (8.5.9) in use may be unstable.
Visit http://www.python.org/download/mac/tcltk/ for current information."   In some ways this is progress.  In others, it falls short.  If IDLE crashes, you can't see the message.  If you have installed the ActiveTCL 8.5.12 update, you still see the warning eventhough it isn't necessary.  Also, I don't link that the referenced page is so complex and that it is full unsettling warnings, important notices, do-not-use advice, mentions of instability,  etc.  

I would like to see our download page have something more simple, affirmative, positively worded and direct.  For example:

*  Mac OS X 64-bit/32-bit Installer (3.2.3) for Mac OS X 10.6 and 10.7 [2] (sig).  To run IDLE or Tkinter, you need to update Tcl/Tk to ActiveTcl 8.5.12 <http://www.activestate.com/activetcl/downloads> .

That saves you from having to click a links down to a footnote at the bottom of the page <http://www.python.org/download/releases/2.7.3/#id6 > that sends you to <http://www.python.org/download/mac/tcltk> which is  another page full of tables, warnings,etc that leads you to the Apple 8.5.9 section <http://www.python.org/download/mac/tcltk/#apple-8-5-9> which is a dead-end because there are still known issues with 8.5.9, leaving you with the ActiveTCL section <http://www.python.org/download/mac/tcltk/#activetcl-8-5-12> which has a paragraph of text obscuring the link you actually needed: http://www.activestate.com/activetcl/downloads .

I applaud that some effort was made to document a solution; however, in practice the daisy chain of footnotes, tables, and links has proven unworkable for most of the engineers I've been working with.


Raymond

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120814/83574e72/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Installation on Macs
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
I think becoming an apple signed developer to get a cert is the best approach.

If anyone wanted to approach apple about open source/non profit gratis licenses, that would be appreciated. 

Otherwise I could do it / fund it from the PSF board side, which I am happy to do.

I also concur with Raymond that the download/install instructions could be simplified. Noting for users that rather than downloading Xcode, they can just download the OSX Command Line Tools installer and easy_install/pip/etc will just work would also be nice

Jesse 

On Aug 14, 2012, at 8:33 PM, Raymond Hettinger <raymond.hettinger at gmail.com> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120814/4671f373/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Installation on Macs
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 15 Aug, 2012, at 2:33, Raymond Hettinger <raymond.hettinger at gmail.com> wrote:


You don't have to change the security settings, choosing "open" from the context menu will to the trick. This is only needed for the installation after downloading using a browser that supports Apple's quarantine solution (such as Safari, I don't know if other browsers mark files as being quarantined). 

That said, signing the installer would be more friendly to users and Ned has opened an issue for this: <http://bugs.python.org/issue15661>


Tk on OSX is a mess. The version that Apple ships tends to crash a lot on even slightly complicated code (or just someone that tries to input accented characters).  The ActiveState download is better, but there are still crashes and unexpected behavior with that version.  AFAIK most bug reports about IDLE not working correctly on OSX are due to issues with Tk, Ned should know more about that as he's the one that tends to look into those issues.


+1 on adding direct download links for Tk to the main download page.

Ronald

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120815/2178f6b7/attachment-0001.html>
-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 4788 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120815/2178f6b7/attachment-0001.bin>



----------------------------------------
Subject:
[Python-Dev] Installation on Macs
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
Raymond raises a couple of issues and Jesse comments on those and brings 
up another issue.  Let me address each in turn (and I apologize for the 
length!):

1. Gatekeeper singing on 10.8

In article <E0FF98C8-13D2-44CA-B890-03F12D836F4A at gmail.com>,
 Raymond Hettinger <raymond.hettinger at gmail.com> wrote:

Yes, Gatekeeper support is a known desirable feature for OS X 10.8 
(Mountain Lion).  There are a number of issues involved, involving code, 
process, and the PSF as a legal entity.  Rather than going into all the 
gory details here, see http://bugs.python.org/issue15661 which I've 
opened to track what needs to be done.  Quick summary is that we need to 
change the installer format that is used to be able to participate in 
the installer singing program and the PSF will likely need to be 
involved as the legal entity "owning" the certificates that need to be 
signed.  Ronald and I are aware of the issues but, to be honest, this 
has been a lower-priority issue compared to others for the Python 3.3.0 
release.  Now that 3.3.0b2 is out and things seem to be in pretty good 
shape, this issue is now in the top set of my list and I have been 
working on it recently. 

By the way, it's not necessary to use System Preferences to change the 
security settings, although Apple doesn't make it obvious that this is 
the case.  As documented here (http://support.apple.com/kb/HT5290) and 
in the online help, you can override the signing check by using 
control-click on the installer mpkg file and selecting Open using ... 
Installer (or use spctl(8) from the command line).

Thread-tie: the current ActiveTcl installers for OS X are also not yet 
signed so attempting to install them on 10.8 currently results in the 
same user experience as with python.org installers.

In article <5432C23E-CABB-476E-966C-164209BA47AE at gmail.com>,
 Jesse Noller <jnoller at gmail.com> wrote:

Thanks, Jesse.  There seems to be a fairly straightforward process for a 
corporate entity to request a development team membership from Python 
(at nominal cost, see the references in the opened issue).  As the 
developer ID program is new to me, I have been intending to propose 
something officially to PSF officers once we were further along with 
implementation and testing.  With Ronald's concurrence, I will make sure 
to follow up with you and/or Van when we are further along.


2. Tcl/Tk on OS X

Raymond:
[...]

I am open to changing the wording.  However, as I've noted in the past, 
I think it's problematic to use wording that implies you can 
unconditionally download and install ActiveState's Tcl.   I really 
appreciate the great work that the ActiveState folks do and am happy to 
recommend people to use it.  But not everyone can without cost.  The 
free (as in beer) ActiveTcl Community Edition is not open source and it 
is released with a license that restricts the use of some parts of 
ActiveTcl, the pieces that ActiveState have developed themselves.  
That's a perfectly understandable business decision.  I am not a lawyer 
so I'm not in a position to say to our users whether or not they can 
legally download and use ActiveTcl without entering into some other 
license arrangement.  That's one reason why the links send users to the 
special page.  I'd would be happy to see wording on the release pages 
that incorporate that sense.  I'll see what I can come up with and 
propose something.  Let me know if you have any specific suggestions or 
if you think my concerns are misplaced.

http://www.activestate.com/activetcl/license-agreement
http://www.python.org/download/releases/3.3.0/
http://www.python.org/download/mac/tcltk/
 

The warning message when IDLE.app is run with the buggy Apple-supplied 
10.6 Tcl/Tk has been available since 3.2 and 2.7.2 (Issue10907).  I did 
recently update all branches to warn about the 10.7/10.8 versions as 
well; they are not as totally broken as 10.6 was but can still crash 
easily from the "wrong" user keyboard input.


That should not be the case with installers downloaded from python.org.     
If you can reproduce, please check to see the actual path to the Tcl and 
Tk frameworks.  Probably the easiest way for IDLE.app is to launch 
"/Applications/Utilites/Actiity Monitor.app", select the IDLE process 
and click on Inspect.  In the list of open files, you should see 
/Library/Frameworks/Tcl.framework/Versions/8.5/Tcl and 
/Library/Frameworks/Tk.framework/Versions/8.5/Tk if ActiveTcl is being 
linked or /System/Library/Frameworks/Tcl.framework/Versions/8.5/Tcl and 
Tk if the Apple system versions are being used.  If you are using 
Pythons from another source or self-built, there is no guarantee that 
they will link to the ActiveTcl versions without taking some steps 
during building.  Let me know if I can help with that.


Well, the situation *is* pretty complex.  We support a *lot* of 
configurations and there are a lot of gotchas.  We have have taken some 
steps to simplify things, like dropping installer support for 10.3 and 
10.4 with Python 3.3 but unfortunately the most problematic OS X 
releases for Apple Tcl are the most recent ones (10.6, in particular).  
Prior to 3.3.0 release, I intend to review and revise it.  Wording 
change suggestions are welcome!

But I totally agree that the user experience is not good.  The only way 
I see to make a major improvement is to get into the business of 
building and supplying Tcl/Tk with the OS X installers, as is currently 
done for the Windows installers.   Now that the Mac Tcl community has 
been getting more involved in maintaining the Cocoa port themselves and 
things are getting more stable (and Apple continues to appear to be 
uninterested), perhaps it is time for us to bite the bullet.  I've 
opened Issue15663 to look into that for 3.4 (and *possibly* for earlier 
maintenance releases).


3. Download instructions and Xcode

Jesse:

The Mac section of the Python docset is woefully out-of-date (from long 
before I got involved!) and I plan to give it a major update for 3.3.0.  
The whole business of what's needed to build extension modules on OS X 
got *much* more complicated with Xcode 4 (the default for 10.7 and 10.8) 
and each minor release of Xcode 4 has brought new changes.  The 
introduction of the stand-alone Command Line Tools (with Xcode 4.2?) was 
a nice addition but there are gotchas with using it, for example, 
extension building with current Python 3.2.3 installers do not work 
out-of-the-box with the CLT because the CLT do not provide SDKs.  2.7.3 
is better but neither of the current 32-bit installers will work out of 
the box with Xcode 4.   A lot of work has gone into 3.3.0 to make 
extension building and building Python itself play more nicely with 
Xcode 4 without breaking support for older versions.  Some subset of 
that support will get backported for 2.7.4 and 3.2.4 once 3.3.0 is done.

Also, if network bandwidth and disk space usage are not major concerns, 
it may now be procedurally easier for most people to install Xcode 4 
than the Command  Line Tools package since the former is now available 
for free download from the Mac App Store while the latter still requires 
registering for a (free) Apple Developer Id and download from the Apple 
Developer site.  And since Xcode 4 has been partitioned up into smaller 
downloadable components, the Xcode download is smaller as it is not 
necessary to download everything (including iOS development tools) as 
was the case with Xcode 3.

-- 
 Ned Deily,
 nad at acm.org




----------------------------------------
Subject:
[Python-Dev] Installation on Macs
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Wed, 15 Aug 2012 02:30:17 -0700
Ned Deily <nad at acm.org> wrote:

I first thought Apple had gone poetic and then I realized it's a typo
(singing / signing). Too bad.

Regards

Antoine.


-- 
Software development and contracting: http://pro.pitrou.net





----------------------------------------
Subject:
[Python-Dev] Installation on Macs
----------------------------------------
Author: Ronald Oussore
Attributes: []Content: 

On 15 Aug, 2012, at 11:30, Ned Deily <nad at acm.org> wrote:

Another advantage of installing all of Xcode is that the appstore will warn when a new version is available, and when you start Xcode you can still install the command-line tools (and Xcode will warn when that installation is out of date).

Ronald

-------------- next part --------------
A non-text attachment was scrubbed...
Name: smime.p7s
Type: application/pkcs7-signature
Size: 4788 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120815/17688b67/attachment.bin>



----------------------------------------
Subject:
[Python-Dev] Installation on Macs
----------------------------------------
Author: Ned Deil
Attributes: []Content: 
In article <20120815122105.3847e734 at pitrou.net>,
 Antoine Pitrou <solipsis at pitrou.net> wrote:

Perhaps the installers periodically get together to silently sing _The 
Old Signing Blues_.

-- 
 Ned Deily,
 nad at acm.org


