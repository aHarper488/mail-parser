
============================================================================
Subject: [Python-Dev] looking for a contact at Google on the Blogger team
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Doug Hellman
Attributes: []Content: 
Several of the PSF blogs hosted on Google's Blogger platform are experiencing issues as fallout from the recent maintenance problems they had. We have already had to recreate at least one of the translations for Python Insider in order to be able to publish to it, and now we can't edit posts on Python Insider itself.

Can anyone put me in contact with someone at Google from the Blogger team? I would at least like to know whether the "bX-qpvq7q" problem is being worked on, so I can decide whether to take a hiatus or start moving us to another platform. There are a lot of posts about the error on the support forums, but no obvious response from Google.

Thanks,
Doug

--
Doug Hellmann
Communications Director
Python Software Foundation
http://python.org/psf/




----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Thu, May 19, 2011 at 16:07, Doug Hellmann <doug.hellmann at gmail.com> wrote:

With respect to Google Blogger, I don't see a good reason to use it as
the platform for the blog. IMHO it would be much better to go for a
less-dependencies approach and just deploy a Wordpress installation,
or possibly even something Python-based (if volunteers to maintain it
are found.

Eli



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, May 20, 2011 at 5:02 PM, Eli Bendersky <eliben at gmail.com> wrote:

As with any infrastructure, there is a reasonably high cost in
changing, as people have become used to a certain way of doing things,
and porting the contents from the old system to the new one requires
additional effort.

Blogger has its problems, but it typically gets the job done well
enough (modulo cases like the one currently affecting Doug and his
team).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

Has the Python insider blog really accumulated enough history and
cruft to make this move problematic? It's a fairly new blog, with not
much content in it. From my blogging experience, Blogger has other
limitations which eventually bite you, and since it's not very
flexible you can either live with it or move to a more flexible
platform.

All of this completely IMHO, of course. Just friendly advice ;-)
Eli



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Fri, May 20, 2011 at 5:39 AM, Eli Bendersky <eliben at gmail.com> wrote:

There is ongoing work for an RFP by the board to improve the
python.org publishing system/site to allow us to self-host these
things. Moving PSF properties off of it, and onto another "hosted by
someone else" site is probably not a good idea, but our hands may be
forced if google/blogger can not resolve the issues.

jesse



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

The whole idea of a Wordpress-(or similar)-based solution is self
hosting, and less reliance on outside providers like blogger.
Wordpress is just a bunch of PHP code you place in a directory on your
server and you have a blog. You don't depend on anyone, except your
own hosting.

Eli



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, May 20, 2011 at 7:39 PM, Eli Bendersky <eliben at gmail.com> wrote:

It's not just the Python Insider blog that is affected (and *any*
effort directed towards platform changes is effort that isn't going
towards writing new articles. Of course, if Blogger don't fix the
currrent problems, then that will be a moot point - moving will be
necessary to get *anything* done).

In general, though, infrastructure changes start from a position of
"not worth the hassle", just like code changes. It takes a pretty
compelling set of features to justify switching, and, while Blogger
isn't the best engine out there, it isn't terrible either (especially
once you replace their lousy comment system with something that is at
least half usable like DISQUS).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, May 21, 2011 at 1:35 AM, Eli Bendersky <eliben at gmail.com> wrote:

As Jesse has said, there is an RFP in development to improve
python.org to the point where we can self-host blogs and the like and
deal with the associated user account administration appropriately.
But when it comes to collaborative blogs, it *isn't* just a matter of
dropping a blogging engine in and running with it.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 05/20/2011 11:35 AM, Eli Bendersky wrote:

And your own sysadmins now have to chase fixes for remotely-exploitable
WP bugs:

 http://www.wordpressexploit.com/



Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk3WkBQACgkQ+gerLs4ltQ72iwCeIhkCLXm26ujJJ3kqh9vKB4fr
dMYAn05qsoyiNxio02UAYJ7luLjVaSML
=OFdv
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 20.05.2011 17:35, Eli Bendersky wrote:

That's exactly the problem.

Georg




----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

To run a blog on www.python.org, a PEP is not needed. If anybody would
volunteer to set this up, it could be done in no time.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Doug Hellman
Attributes: []Content: 

On May 20, 2011, at 5:47 PM, Martin v. L?wis wrote:


The blog is working again, so we can continue using the tool chain we have.

Thanks,
Doug

--
Doug Hellmann
Communications Director
Python Software Foundation
http://python.org/psf/




----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Sat, May 21, 2011 at 7:47 AM, "Martin v. L?wis" <martin at v.loewis.de> wrote:

If I understand correctly, the RFP is more about improving the entire
python.org toolchain to make it something that non-programmers can
easily provide content for (and even *programmers* don't particularly
like the current toolchain).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] looking for a contact at Google on the Blogger team
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Mon, May 23, 2011 at 2:15 AM, Nick Coghlan <ncoghlan at gmail.com> wrote:

That is correct.

