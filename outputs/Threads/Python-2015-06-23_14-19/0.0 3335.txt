
============================================================================
Subject: [Python-Dev] Install Hook [Was: Re: PEP 414 updated]
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Install Hook [Was: Re: PEP 414 updated]
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Sun, Mar 4, 2012 at 6:43 AM, Armin Ronacher
<armin.ronacher at active-4.com> wrote:

I'd love a pointer to the rubygems debacle...


+1


Yee!

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Install Hook [Was: Re: PEP 414 updated]
----------------------------------------
Author: Armin Ronache
Attributes: []Content: 
Hi,

On 3/4/12 4:44 PM, Guido van Rossum wrote:
Setuptools worked because Python had .pth files for a long, long time.
When the Ruby world started moving packages into nonstandard locations
(GameName/<the files in that gem>) something needed to activate that
import machinery hack.  For a while all Ruby projects had the line
"require 'rubygems'" somewhere in the project.  Some libraries even
shipped that line to bootstrap rubygems.

I think an article about that should be found here:
http://tomayko.com/writings/require-rubygems-antipattern

But since the page errors out currently I don't know if that is the one
I'm referring to.

Considering such an import hook has to run over all imports because it
would not know which to rewrite and which not I think it would be
equally problematic, especially if libraries would magically activate
that hook.


Regards,
Armin



----------------------------------------
Subject:
[Python-Dev] Install Hook [Was: Re: PEP 414 updated]
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Armin Ronacher <armin.ronacher <at> active-4.com> writes:


You could be right, but it sounds a little alarmist to me - "problematic" -
"magical". For example, in the current implementation of uprefix, the hook does
nothing for files in the stdlib, could be refined to be more intelligent about
what to run on, etc. Plus, as Zbigniew pointed out in his post, ways could be
found (e.g. via a context manager) to give users control of when the hook runs.

I'm not sure your rubygems example is analogous - I would have thought the
equivalent for Python would be to stick "import setuptools" everywhere, which
is not an anti-pattern we lose sleep over, AFAIK.

It's early days, but it seems reasonable to document in the usage of the hook
that it is intended to be used in certain ways and not in others. IIRC Ryan's
post was doing just that - telling people how the requiring of rubygems should
work.

AFAIK the approach hasn't been tried before, and was suggested by Nick (so I
assume is not completely off the wall). My particular implementation might have
holes in it (feedback welcome on any such, and I'll try to fix them) but I
would think the approach could be given a chance in some realistic scenarios to
see what problems emerge in practice, rather than trying to shoot it down
before it's even got going.

Regards,

Vinay Sajip

"It is not enough merely to win; others must lose." - Gore Vidal


