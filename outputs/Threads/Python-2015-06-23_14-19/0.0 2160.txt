
============================================================================
Subject: [Python-Dev] GSoC: speed.python.org
Post Count: 16
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: DasIc
Attributes: []Content: 
Hello Guys,
I'm interested in participating in the Google Summer of Code this year
and I've been looking at projects in the Wiki, particularly
speed.pypy.org[1] as I'm very interested in the current VM
development. However given my knowledge that project raised several
questions:

1. Up until now the only 3.x Implementation is CPyhon. IronPython,
Jython and PyPy don't support it - and to my knowledge won't get
support for it during or before GSoC - and could not benefit from it.
It seems that a comparison between a Python 2.x implementation and a
3.x implementation is rather pointless; so is this intended to be
rather an additional "feature" to have 3.x there as well?

2. As a follow-up to 1: It is not specified whether the benchmarks
should be ported using a tool such as 2to3, if this should not happen
or if this is up to the student, this needs clarification. This may be
more clear if it were considered under which "umbrella" this project
is actually supposed to happen; will those ported benchmarks end up in
CPython or will there be a separate repository for all VMs?

3. Several benchmarks (at least the Django and Twisted ones) have
dependencies which are not (yet) ported to 3.x and porting those
dependencies during GSoC as part of this project is an unrealistic
goal. Should those benchmarks, at least for now, be ignored?

[1]: http://wiki.python.org/moin/SpeedDotPythonDotOrg



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Mar 21, 2011 at 12:33 PM, DasIch <dasdasich at googlemail.com> wrote:

Hi.

There might be two independent SoCs but as far as I know, a very
interesting SoC on it's own (and one I'm willing to mentor) would be
to:

1. Get this stuff running on speed.python.org
2. Improve backend infrastructure so it actually *can* easily run
multiple VMs, especially that some benchmarks won't run on all.
3. Fix some bugs in frontend, improve things that don't look quite as
good if that should be for the general Python community.

Note that this is a bit orthogonal to port benchmarks to Python 3.
Also if noone would run those benchmarks, just porting them to Python
3 makes little sense.

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 21 Mar 2011 19:33:55 +0100
DasIch <dasdasich at googlemail.com> wrote:

Why not reuse the benchmarks in http://hg.python.org/benchmarks/ ?
Many of them are 3.x-compatible.
I don't understand why people are working on multiple benchmark suites
without cooperating these days.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
Some remarks below.

On Mon, Mar 21, 2011 at 2:33 PM, DasIch <dasdasich at googlemail.com> wrote:

You are correct: But the point of this GSOC project is to do the
porting, the initial deployment of speed.python.org will be on the 2.x
implementation with 3.x to follow as other VMs migrate.


We will have a common repository for all benchmarks, for all of the
implementations.


IMHO: Yes. I think MvL can expand on this as well.


FYI, I recommend coordinating with Miquel Torres (cc'ed) and Maciej
Fijalkowski from PyPy on these questions / this project as well. I am
currently coordinating getting the hardware setup for this project's
hosting.

jesse



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Jesse Nolle
Attributes: []Content: 
On Mon, Mar 21, 2011 at 2:48 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Antoine: The goal is to *get* PyPy, Jython, IronPython, etc all using
a common set of benchmarks. The idea stems from http://speed.pypy.org/
- those benchmarks grew from the unladen swallow benchmarks, which I
think are the ones in http://hg.python.org/benchmarks/.

So, yeah - the goal is to get us all reading off the same page. The
PyPy people can speak to the changes they made/had to make.

jesse



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le lundi 21 mars 2011 ? 14:51 -0400, Jesse Noller a ?crit :

Ok, I can stand corrected. Last time I looked, speed.pypy.org had an
entirely disjunct set of benchmarks.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    Antoine> Why not reuse the benchmarks in
    Antoine> http://hg.python.org/benchmarks/ ?

These looks like basically the same benchmarks as the Unladen Swallow folks
put together, right?  Is there any value in them as regression tests (maybe
with more elaborate inputs and/or longer runtimes)?  If so, I think there is
value it having both 2.x and 3.x versions, even if the 3.x stuff won't be
useful for speed comparisons.

Skip



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le lundi 21 mars 2011 ? 14:06 -0500, skip at pobox.com a ?crit :

Yes, it's basically the continuation of their work. Most changes since
then have been relatively minor.


You mean to check behaviour or to check for performance regressions?
IMHO the test suite is long enough to run already and performance tests
should be kept in a separate suite. Whether that suite should be
separate from the main distribution is another question.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: skip at pobox.co
Attributes: []Content: 

    >> Is there any value in them as regression tests (maybe with more
    >> elaborate inputs and/or longer runtimes)?

    Antoine> You mean to check behaviour or to check for performance
    Antoine> regressions?

Both.  Semantic regressions, and secondarily, performance regressions.
I can understand the need to stabilize the code and inputs for measuring
performance.  As bad a benchmark as pystone is, one of its saving graces is
that it hasn't changed in so long.

When looking for semantic problems I can see that you would add new packages
to the suite where they expose problems, just as we do today for unit tests.

Skip



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 21 Mar 2011 14:30:45 -0500
skip at pobox.com wrote:

I think that semantic regressions would be better checked by running
the test suites of these third-party libraries, not benchmarks.
Ideally a separate suite of buildbots or builders could do that. We had
the community buildbots but it seems they ran out of steam.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: DasIc
Attributes: []Content: 
On Mon, Mar 21, 2011 at 7:48 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

I haven't looked to closely but those benchmarks appear to be the ones
developed by the unladen swallow guys and those are used by PyPy among
others.

The difference is that PyPy has more benchmarks particularly ones that
measure performance of real world applications. As good benchmarks are
very hard to come by those appear to be a better starting point.



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Mar 21, 2011 at 2:24 PM, DasIch <dasdasich at googlemail.com> wrote:

The benchmark running code didn't change significantly. The actual
benchmarks however did. US's micro-benchmarks were removed and new
things were added (the biggest addition is probably the whole set of
twisted benchmarks). The original idea was to converge and have the
common repo on hg.python.org, but since unladen run out of steam,
nobody bothered to update the hg.python.org one so we continued on our
own.

Cheers,
fijal



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le lundi 21 mars 2011 ? 16:21 -0600, Maciej Fijalkowski a ?crit :

I'm not sure who is "nobody" in that statement...
The repo certainly didn't receive a lot of changes, but it *did* get
several of them anyway, post-unladen:
http://hg.python.org/benchmarks/

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Indeed. It is, in fact, not true that Django hasn't been ported to
Python 3. So anybody porting the benchmark should IMO also consider
the Django ones. For the Twisted ones, I think some progress was
made at PyCon, but I don't know what the status is.

But in general, Jesse is right: if there is an external dependency
that hasn't been ported to Python 3 yet, that should rather be
a GSoC project of its own (assuming the project maintainers agree
to then integrate the project results).


I also agree that the GSoC project should focus on the coding/porting
part. Setting up infrastructure is not a good GSoC project, as this
is rather a long-term commitment to keep it running.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Maciej Fijalkowsk
Attributes: []Content: 
On Mon, Mar 21, 2011 at 4:27 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Ok.

Indeed, sorry, I didn't follow too closely. It's worth considering
what would be the obvious next goal. This repo changed (and I believe
you're the main person according to the change log) to provide python
3 support. On the other hand our repo changed to accomodate changes
that were required to push it to speed.pypy.org as well as adding lots
of new benchmarks. What's the desired goal of this repo then?

Cheers,
fijal




----------------------------------------
Subject:
[Python-Dev] GSoC: speed.python.org
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 21 Mar 2011 17:06:09 -0600
Maciej Fijalkowski <fijall at gmail.com> wrote:

AFAIR, the goal when it was created was precisely to share a benchmark
suite accross VMs, instead of having many efforts scattered around.
You'd have to ask whoever created the repo on hg.python.org, though.

It should not be difficult to reconcile changes anyway. I think you have
push rights on hg.python.org :)

Regards

Antoine.

