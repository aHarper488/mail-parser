
============================================================================
Subject: [Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
Post Count: 19
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Sep 20, 2012 at 9:56 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

+1 for using the unqualified "argument" in these error messages to
mean "positional or keyword argument" (inspect.Parameter spells it out
as POSITIONAL_OR_KEYWORD, but the full phrase is far too verbose for
an error message).

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Thu, Sep 20, 2012 at 1:21 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

Ah yes;  I see that 'positional or keyword' is a more accurate term
(but agree it's unwieldy for an error message).  I also see that I was
naive to think that the 'fix' is as simple as dropping the word
'positional':

    >>> def f(a, *, b):
    ...     pass
    ...
    >>> f()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: f() missing 1 required positional argument: 'a'

If the word 'positional' were dropped here, it would give the
incorrect impression that f only requires one argument.

Perhaps this simply isn't worth worrying about, especially since the
current error messages are all but certain to make it into the 3.3
release.

Mark



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Sep 20, 2012 at 10:59 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

No "all but" about it at this point - the earliest they could change
again is 3.3.1. Hopefully the new signature inspection support will
help explain some of the intricacies of binding argument values to
parameter names :)

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/9/20 Mark Dickinson <dickinsm at gmail.com>:

I tried to define the error messages in terms of the callee's
signature. I call the formals that are not variadic, keyword variadic,
or keyword-only, positional. For example, in

def f(a, b, c, *args, d):
     pass

a, b, and c are positional. Hence the "positional" in error messages.

As you noted in your next message, keyword-only arguments need to be
distinguished from these "positional" arguments somehow. Maybe it
helps to think of "positional" to mean "the only formals you can pass
to with position" (excepting variadic ones).

I'm certainly open to suggestions.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Thu, Sep 20, 2012 at 7:12 AM, Benjamin Peterson <benjamin at python.org> wrote:

That is how I think of "positional".

However, the other wrinkle is that some arguments really are
"position-only," for example:

Traceback (most recent call last):
 ...
TypeError: len() takes no keyword arguments

I think it is a defect of our documentation that we don't have a way
to distinguish between "positional" and "position-only" arguments in
the function signature notation we use in our documentation, leading
to issues like this one:

"accept keyword arguments on most base type methods and builtins":

http://bugs.python.org/issue8706

--Chris



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Thu, Sep 20, 2012 at 3:12 PM, Benjamin Peterson <benjamin at python.org> wrote:


And excepting optional ones, too, right?  E.g., the c in

    def foo(a, b, c=1, *args, d):
        pass

can be passed to by position, but isn't "positional".


Yes, I don't have a good alternative suggestion.  If we could find a
suitable word and bless it in the documentation, it might make it
easier to make clear and accurate statements about Python's function
calling.

Mark



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2012/9/20 Mark Dickinson <dickinsm at gmail.com>:

Why not?

...
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: f() missing 2 required positional arguments: 'a' and 'b'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: f() takes from 2 to 3 positional arguments but 4 were given



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Thu, Sep 20, 2012 at 4:14 PM, Benjamin Peterson <benjamin at python.org> wrote:

Ah, okay;  I was assuming (wrongly) that your definition of
'positional' was intended to exclude these.  My bad.

Mark



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Thu, Sep 20, 2012 at 7:12 AM, Benjamin Peterson <benjamin at python.org> wrote:

No -- Mark's point is that (even given this syntax) you *could* pass
them using keywords.

I think Brett's got it right and we should just refer to a and b as
'arguments'. For d, we should use keyword arguments (or, in full,
keyword-only arguments). That's enough of a distinction.

Of course, in a specific call, we can continue to refer to positional
and keyword arguments based on the actual syntax used in the call.

Maybe this is also a good time to start distinguishing between
arguments (what you pass, call syntax) and parameters (what the
function receives, function definition syntax)?


-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Oscar Benjami
Attributes: []Content: 
On 20 September 2012 16:14, Benjamin Peterson <benjamin at python.org> wrote:


The difference between c and a,b is that c is optional, whereas a and b are
required.

In Python 2.x there are named arguments and variadic arguments. There are
two types of named arguments: required and optional. There are also two
types of variadic arguments: positional and keyword. i.e.:

named
  required
  not-required
variadic
  positional
  keyword

In Python 2.x all named parameters can be passed by position or by keyword,
so it doesn't make sense to use those concepts to distinguish them. On the
other hand, for variadic parameters that distinction is crucial.

In Python 3.x there are two orthogonal properties for each named parameter.
The parameter can be required or optional as before, and then the parameter
can be keyword-only or positional. There are 4 combinations of these two
properties:

def f(a, b=1, *, c, d=3): pass

                   | required | optional
positional | a              | b
kwonly       | c              | d

Since there are two orthogonal properties of a parameter (requiredness and
positionness) it makes perfect sense to use two adjectives to describe each
parameter as is the case for the error message shown at the start of this
thread:

Mark Dickinson wrote:

I would say that the only problem with this terminology is that it would be
good to think of a word to replace "keyword-only" (positionless?).

Oscar
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120920/fb93c5ca/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Thu, Sep 20, 2012 at 8:52 AM, Guido van Rossum <guido at python.org> wrote:

The glossary is one place to start making this distinction.  It
currently has entries for "argument," "positional argument," and
"keyword argument" that could perhaps use a review from this
discussion.  For example:

http://docs.python.org/dev/glossary.html#term-positional-argument

The entries currently blur the distinction between the calling and
definition perspectives.  Ideally, the glossary definitions of these
terms would match and be consistent with their usage in error
messages.

--Chris



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On 20/09/12 22:59, Mark Dickinson wrote:


I don't expect error messages to give a complete catalog of every
problem with a specific function call. If f() reports that required
argument 'a' is missing, that does not imply that no other required
arguments are also missing. I think it is perfectly acceptable to
not report the missing 'b' until the missing 'a' is resolved.

But I do expect error messages to be accurate. +1 to remove the
word "positional" from the message.



-- 
Steven



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On 21/09/12 00:49, Antoine Pitrou wrote:

Furthermore, since the parameter has a name, it can be given as a
keyword argument. Describing positional-or-keyword as "positional"
is misleading, although I admit that I often do that too. I think that
"positional or keyword argument" is too wordy, and is ambiguous as to
whether the argument can be given as either positional or keyword, or
we're unsure which of the two it is.

"Named positional argument" is more accurate, but also too wordy, and
it relies on the reader knowing enough about Python's calling semantics
to infer that therefore it can be given as positional or keyword style.

Since this is way too complicated to encapsulate in a short error
message, I'm with Nick and Mark that "positional" should be dropped
unless the argument is positional-only.



-- 
Steven



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On 21/09/12 01:53, Oscar Benjamin wrote:


I disagree completely. I think keyword-only is the right terminology to
use for arguments which can only be passed by keyword. It is *positional*
that is questionable, since named positional arguments can be given by
keyword.

I would like to see error messages reserve the terms:

1) "positional" for explicitly positional-only parameters;
2) "keyword" for explicitly keyword-only parameters;

(I don't mind whether or not they use "-only" as a suffix)

For normal, named-positional-or-keyword arguments, just use an unqualified
"argument".



-- 
Steven



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
On Thu, Sep 20, 2012 at 10:18 AM, Chris Jerdonek
<chris.jerdonek at gmail.com> wrote:

I took the liberty to create an issue in the tracker to settle on and
document preferred terminology in the area of positional/keyword
arguments/parameters, etc.  The issue is here:

http://bugs.python.org/issue15990

--Chris



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Steven D'Aprano wrote:

I disagree.  There is no reason (that I'm aware of ;) that the missing 
'b' cannot be noticed and reported at the same time as the missing 'a'.



And then it's still not accurate as 'b' is also a required argument that 
is missing.  Unless and until all error messages adopt your proposed 
'positional argument', 'argument', 'keyword argument' *and* describe 
_all_ the problems with the call confusion will reign supreme.

So, ideally, the above example would be:

      >>>  def f(a, *, b):
      ...     pass
      ...
      >>>  f()
      Traceback (most recent call last):
        File "<stdin>", line 1, in<module>
      TypeError: f() missing 2 required arguments: positional: 'a', 
keyword: 'b'

~Ethan~

P.S.
Also, a big thank-you -- the error messages *are* getting better all the 
time!



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Steven D'Aprano wrote:

+1



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On 21/09/12 05:45, Ethan Furman wrote:


Listing every missing argument does not scale well as the number of
arguments increases.

def f(spam, ham, cheese, aardvark, halibut, *,  shrubbery, parrot, wafer_thin_mint):
     pass

f()

I would be -0 on an error message like:

TypeError: f() needs arguments 'spam', 'ham', 'cheese', 'aardvark', 'halibut' and keyword-only arguments 'shrubbery', 'parrot', 'wafer_thin_mint'

but wouldn't strongly object. I think it is acceptable (although not
ideal) if calling f() only reported the first missing argument it
noticed.

But I do think that we should not make any language guarantees about
error messages being "complete" or not.


-- 
Steven



----------------------------------------
Subject:
[Python-Dev] TypeError: f() missing 1 required positional
 argument: 'x'
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
We've already had this terminology discussion and documented the results in
PEP 362. The rest of the docs may require updates to be brought in line
with that.

Cheers,
Nick.

--
Sent from my phone, thus the relative brevity :)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120921/4427d080/attachment.html>

