
============================================================================
Subject: [Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
Post Count: 15
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 
On Nov 1, 2010, at 7:35 PM, Brett Cannon wrote:

I'm not sure I follow where we're stuck with the current package.
AFAICT, the module is still used with "import unittest".
The file splitting was done badly, so I don't think there any of the
components are usable directly, i.e. "from unitest.case import SkipTest".
Also, I don't think the package structure was documented or announced.

This is in contrast to the logging module which does have a
clean separation of components and where it isn't unusual
to import just part of the package.

What is it you're seeing as a risk that I'm not seeing?
Are we permanently locked into the exact ten filenames
that are currently used:  utils, suite, loader, case, result, main, signals, etc?
Is the file structure now frozen?

Raymond
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101102/2b621d9a/attachment.html>



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Nov 2, 2010, at 3:33 PM, Nick Coghlan wrote:


I'll propose some PEP 8 wording in the bug tracker
(essentially advice on when and how to use packaging),
and everyone can offer their assent, dissent, and
word-smithing.


Raymond 




----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Nov 2, 2010, at 3:58 PM, Guido van Rossum wrote:


I don't find anything offensive about it.  The issues have to do
with being able to find and analyze code.

For example, to find-out what assert.ItemsEqual does, I have
to figure-out that it was put in the case.py file.  In Py2.6,
you code use IDLE's Open Module tool to immediately
bring up all the source for unittest.   Then you could fire-up
the class browser to quickly see and navigate the structure,
but that also no longer works in Py2.7.   Also, it used to be
the just knowing the module name was sufficient to find the
code with http://svn.python.org/view/python/branches/release26-maint/Lib/unittest.py?view=markup
All you needed to study the code was a web browser and
its find function.   Now you need to open ten tabs to be able
to browse this code.  IOW, the packaging broke a read-the-source-luke
style of research that I've been teaching people to use for years.

I probably didn't articulate the above very well, but I think
Martin said it more succinctly in this same thread.

The other issue that Brett pointed out is that the file names
now become part of the API, "from unittest.utils import safe_repr".

In the logging module, packaging was done well.  The files
fell along natural lines in the API, some of the components
we usable separately and testable separately.  Likewise
with the xml packages.  In contrast, the unittest module
is full of cross-imports and tightly coupled pieces (like
suite and case) have been separated.


Raymond

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101102/1ee3f763/attachment.html>



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Nov 2, 2010, at 4:00 PM, Brett Cannon wrote:

That's a bummer.

Sounds like a decision to split a module into a package is a big commitment.  Each of the individual file names becomes a permanent part of the API.  Even future additional splits are precluded because it might break someones dotted import (i.e. not a single function can be moved between those files -- once in unittest.utils, alway in unittest.utils).


Raymond



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Nov 2, 2010, at 4:43 PM, Guido van Rossum wrote:


I was thinking of PEP 8 wording that listed the forces for and against.

For example, ply.yacc and ply.lex was a very useful split (separately testable, natural division of concerns, no nested of cross-imports).

The xml.sax, xml.dom, and xml.minidom was a nice split because it separated distinct tools.  The xml packaging also worked well because it is easy to substitute in alternate parsers implementing the same API.

I think we also want to recommend against putting much if any code in __init__.py.

Some forces against packaging are that it breaks the class browser.  As you say, different users of different toolsets are affected differently.  For me, the unittest split broke my usual ways of finding out how the new methods were implemented.

Another force against is what Brett pointed-out, that the package file structure becomes a permanent and unchangeable part of the API.  It's a one-way street.

In general, I think the advice should be that packaging should be done when there is some clear benefit beyond "turning one big file into lots of smaller files". 


Raymond  





----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Antoine Pitrou <solipsis at pitrou.net> writes:


If it's an implementation detail, shouldn't it be named as one (i.e.
with a leading underscore)?


I would say that names without a single leading underscore are part of
the public API, whether documented or not.

-- 
 \        ?Your [government] representative owes you, not his industry |
  `\   only, but his judgment; and he betrays, instead of serving you, |
_o__)        if he sacrifices it to your opinion.? ?Edmund Burke, 1774 |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: James Y Knigh
Attributes: []Content: 

On Nov 3, 2010, at 11:25 AM, Eric Smith wrote:


This is the strongest reason why I recommend to everyone I know that they not use pickle for storage they'd like to keep working after upgrades [not just of stdlib, but other 3rd party software or their own software]. :)

James



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Glyph Lefkowit
Attributes: []Content: 

On Nov 3, 2010, at 1:04 PM, James Y Knight wrote:


+1.

Twisted actually tried to preserve pickle compatibility in the bad old days, but it was impossible.  Pickles should never really be saved to disk unless they contain nothing but lists, ints, strings, and dicts.


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101103/3756d84a/attachment.html>



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Glyph Lefkowit
Attributes: []Content: 

On Nov 3, 2010, at 11:26 AM, Alexander Belopolsky wrote:



Maybe this is the real problem?  It's 2010, we should all be far enough beyond EDLIN that our editors can jump to the definition of a Python class.  Even Vim can be convinced to do this (<http://rope.sourceforge.net/ropevim.html>).  Could Python itself make this easier?  Maybe ship with a command that says "hey, somewhere on sys.path, there is a class with <this name>.  Please run '$EDITOR file +line' (or the current OS's equivalent) so I can look at the source code".


-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101103/9d3937da/attachment.html>



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 26 Oct 2010 10:39:19 -0700, Raymond Hettinger <raymond.hettinger at gmail.com> wrote:

+1 (or more)

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Oct 26, 2010, at 12:18 PM, Benjamin Peterson wrote:


Now, it's huge and split across multiple files so it's harder to 
search, the class browser won't work, and the full source 
cannot be brought up immediately using just the module name.
The svn annotations and history are munged-up.  The components 
were highly interdependent so now every file has to start with a 
set of cross-imports.  Well, at least the __init__.py is not full of code.
That's something.

FWIW, it wasn't that big (approx 2500 lines).
The argparse, difflib, doctest, pickletools, pydoc, tarfile modules
are about the same size and the decimal module is even larger.  
Please don't split those.


Raymond


  




----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Oct 26, 2010, at 2:54 PM, Ron Adam wrote:


While you're at it.  Can you please modernize the html
and create a style sheet?  Right now, all of formatting
is deeply intertwined with content generation.

Fixing that would be a *huge* improvement.


Raymond



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 26 Oct 2010 16:46:15 -0400, Michael Foord wrote:

Yes.

Note that this is a more recent tendency, a few years ago I used to
split things pretty finely right off the bat, but I found I wasted a
lot of time on py-file interdependencies doing it that way.

Now I generally only split when the pieces end up generalized enough
that I want to reuse them.  Which, granted, is often enough.  But what
I'm doing in that case is moving code out of a file at the top level of
my library....into a new file at the top level of my library.

Now, it's true that I haven't written any huge applications lately,
but looking at a couple of typical projects the main files run between
1500 and 2000 lines (including comments), and I don't find those to
be particularly big and certainly not awkward.

I do understand the attraction of putting "related stuff" into separate
files.  I just think the balance may have tilted too far in the direction
of nested and could stand to be nudged back toward flat :)

Unittest is only 2600 lines total including comments.  Typical file
sizes are between 200 and 300 lines.  Those feel tiny to me :)

(And email is worse...a number of files only have a single class in them,
and some of those classes consist of maybe 20 lines...)

--
R. David Murray                                      www.bitdance.com



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Oct 26, 2010, at 8:37 PM, Barry Warsaw wrote:

No doubt that is true.  The problem is that splitting can also impair discoverability.

When unittest was in one file, you knew the filename was unittest.py 
from just the module name.  Now, there are ten files and you have
no way to guess any of their names or how the author grouped
the functions and methods inside them.

Let's say that I'm mystified about the difference between
assertSameElements() and assertItemsEqual() and assertSetEqual().
So, I go to http://svn.python.org/view/python/branches/py3k/Lib/unittest/ .
Now, how do you know which file contains the source?  Formerly,
I could call-up the one source file and do a find.   Or, I could do 
OpenModule: unittest with IDLE and instantly see the source and
let the class browser analyze the structure of the file.  Now, that's
not possible either.  The source for those methods is now less discoverable.

In the unittest/case.py file, the safe_repr() function function is called over
40 times and it is not used in any other file.  So how do be benefit from
it being defined in the utils.py file?  ISTM, this saved nothing.  The case.py
file is over 1000 lines long and utils is 80.  How did we benefit from that split?  
For me, it makes it harder to read the code because I have to go looking
elsewhere for commonly called functions.

The unittest module grew from one file in Py2.6 to ten files and one directory 
with 2500 lines in Py2.7.  Was that a win?  I've spent time trying to
read through the changes and cannot follow it without having most of 
those ten files open in my editor.  For me, it's a PITA to read the code.
It doesn't help that the file split blew away the svn blame history, so I
have a harder time being about to tell what changed.

All that being said, I started this thread with:  "Packaging is not 
always wrong.  Maybe it was the right thing to do for unittest, maybe not".

The goal of the post was just to raise awareness that splitting modules
is not a straight win.  There are costs to discoverability and searchability.
It makes pyclbr less useful.  It makes svn viewer less useful.  And it loses
history.  

The split of unittest is a done deal.   Who knows, it may have been a net win.
Am just hoping that we all understand that it was not cost free.   My hope is 
that the splitting modules unnecessarily does not become a trend.
Packages should be the last tool we reach for, not the first.
In many cases, flat is better than nested.


Raymond

P.S.  I liked your qualifier, "if done well".  There's a lot in those three words.

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20101027/6034bad6/attachment.html>



----------------------------------------
Subject:
[Python-Dev] On breaking modules into packages Was:
	[issue10199] Move Demo/turtle under Lib/
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Tue, 26 Oct 2010 23:37:10 -0400, Barry Warsaw <barry at python.org> wrote:

To put your mind at ease, Barry, I'd not want to do that either :)

But by (IMO good) design Generator, FeedParser, and Message are all
supposed to be independent (use only each other's public APIs).  And
Header can be (and is, I think) used without the other pieces of email,
as is true for other of the helper modules (base64mime, quoprimime, etc).
On the other hand, I have no clue why 'iterators.py' exists :)

The one that bugs me most, though, is MIME.  Combining all the mime
stuff into one file seems like it would be a big win (not that it's
possible, now).  What is the benefit of email.mime.text.MIMEText over
email.mime.MIMEText, when each of the files in the mime package consists
of a single subclass?

So, to clarify, like Raymond I'm not saying that packages are always bad.
I'm just saying that packages are also not always good; and, further,
that the number of lines of code in a file should, IMO, have nothing to
do with the decision as to whether or not to create a package.

--
R. David Murray                                      www.bitdance.com

