
============================================================================
Subject: [Python-Dev] r84771 -
	python/branches/release27-maint/Lib/test/test_io.py
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] r84771 -
	python/branches/release27-maint/Lib/test/test_io.py
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Maybe you want to mention *who* warns?

Georg

Am 13.09.2010 10:20, schrieb florent.xicluna:


-- 
Thus spake the Lord: Thou shalt indent with four spaces. No more, no less.
Four shall be the number of spaces thou shalt indent, and the number of thy
indenting shall be four. Eight shalt thou not indent, nor either indent thou
two, excepting that thou then proceed to four. Tabs are right out.




----------------------------------------
Subject:
[Python-Dev] r84771 -
	python/branches/release27-maint/Lib/test/test_io.py
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 16 Sep 2010 17:27:50 +0200
Georg Brandl <g.brandl at gmx.net> wrote:

I suppose it's the -3 flag:

$ ~/cpython/27/python -3 -c "1/0"
-c:1: DeprecationWarning: classic int division
Traceback (most recent call last):
  File "<string>", line 1, in <module>
ZeroDivisionError: integer division or modulo by zero




