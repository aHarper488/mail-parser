
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.3): Issue #17098: Be
 more stringent of setting __loader__ on early imported
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.3): Issue #17098: Be
 more stringent of setting __loader__ on early imported
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
FYI, Raymond let me know that this is causing the buildbots to occasionally
fail thanks to xml.parsers.expat.errors somehow circumventing import. I'm
going to try and figure out what's going on.

On Fri, Feb 1, 2013 at 4:38 PM, brett.cannon <python-checkins at python.org>wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130203/9c8e9283/attachment-0001.html>

