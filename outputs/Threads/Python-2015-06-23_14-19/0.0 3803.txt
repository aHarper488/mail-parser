
============================================================================
Subject: [Python-Dev] What code in Python creates .exe launchers for
	'entry_points' in 'setup.py'?
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] What code in Python creates .exe launchers for
	'entry_points' in 'setup.py'?
----------------------------------------
Author: Grigory Petro
Attributes: []Content: 
Hello.


On Windows, 'setuptools' and 'distribute' package systems allows to add
'entry_points' definition into 'setup.py' python distribution script. For
each entry in this definition, some kind of bootstrapper '.exe' file will
be created and placed into 'Scripts' python dir. For example, if i install
'pip' via 'setup.py', a file named 'pip.exe' will be created and placed
into 'C:\Python27\Scripts'.


But what python code is responsible for creation of this bootstrapper
executables? I have searched python 2.7.3 source code for some time, but
can't find a place. Of course i can spend lots of time to debug 'setup.py'
installation (really lots of source code), but maybe someone just knows the
place and can pinpoint it?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130402/c1d7b8b6/attachment.html>

