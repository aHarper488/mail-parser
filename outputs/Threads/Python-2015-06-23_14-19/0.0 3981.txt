
============================================================================
Subject: [Python-Dev] cpython (2.7): Fix posixpath.realpath() for
 multiple pardirs (fixes issue #6975).
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (2.7): Fix posixpath.realpath() for
 multiple pardirs (fixes issue #6975).
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 18 Feb 2013 11:24:40 +0100 (CET)
serhiy.storchaka <python-checkins at python.org> wrote:

What if there's a symlink along os.getcwd()?

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython (2.7): Fix posixpath.realpath() for
 multiple pardirs (fixes issue #6975).
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 18.02.13 19:26, Antoine Pitrou wrote:

1. AFAIK, os.getcwd() returns the path with resolved symlinks.
2. realpath() first resolve relative path and then prepends cwd to the 
result.





----------------------------------------
Subject:
[Python-Dev] cpython (2.7): Fix posixpath.realpath() for
 multiple pardirs (fixes issue #6975).
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Mon, 18 Feb 2013 19:56:07 +0200
Serhiy Storchaka <storchaka at gmail.com> wrote:

Indeed, it seems you are right (under POSIX at least):

?The getcwd() function shall place an absolute pathname of the current
working directory in the array pointed to by buf, and return buf. The
pathname shall contain no components that are dot or dot-dot, or are
symbolic links.?

http://pubs.opengroup.org/onlinepubs/9699919799/functions/getcwd.html

Regards

Antoine.



