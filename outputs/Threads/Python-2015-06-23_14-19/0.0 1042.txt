
============================================================================
Subject: [Python-Dev] [Pythonmac-SIG] sad state of OS X Python	testing...
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Pythonmac-SIG] sad state of OS X Python	testing...
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I think Bill was specifically after Snow Leopard - what system are you
using?

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] [Pythonmac-SIG] sad state of OS X Python	testing...
----------------------------------------
Author: Nicholas Rile
Attributes: []Content: 
On Sat, Oct 02, 2010 at 10:08:09PM +0200, "Martin v. L?wis" wrote:

I'm already running a Jython buildslave on an Intel Mac Pro which is
pretty underused - I'd be happy to run a CPython one there too, if
it'd be worthwhile.

-- 
Nicholas Riley <njriley at illinois.edu>

