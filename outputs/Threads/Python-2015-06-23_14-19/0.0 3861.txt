
============================================================================
Subject: [Python-Dev] PEP 435 -- Adding an Enum type to the
 Python	standard library
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the
 Python	standard library
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
R. David Murray wrote:

This attitude baffles me. In most other languages having a
notion of an enum, when you define an enum, you're defining
a type. The name of the enum is the name of the type, and
its values are instances of that type.

Why should our enums be any different?

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] PEP 435 -- Adding an Enum type to the
 Python	standard library
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
R. David Murray wrote:


That is indeed a quirk, but it's not unprecedented. Exactly
the same thing happens in Java. This compiles and runs:

   enum Foo {
     a, b
   }

   public class Main {

     public static void main(String[] args) {
       System.out.printf("%s\n", Foo.a.b);
     }

   }

There probably isn't much use for that behaviour, but on
the other hand, it's probably not worth going out of our
way to prevent it.

-- 
Greg

