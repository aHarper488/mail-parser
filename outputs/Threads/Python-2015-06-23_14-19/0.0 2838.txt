
============================================================================
Subject: [Python-Dev] push changesets hooks failing
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] push changesets hooks failing
----------------------------------------
Author: Jesus Ce
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

I got this when pushing:

"""
jcea at ubuntu:~/hg/python/cpython$ hg push
pushing to ssh://hg at hg.python.org/cpython/
searching for changes
searching for changes
remote: adding changesets
remote: adding manifests
remote: adding file changes
remote: added 4 changesets with 9 changes to 3 files
remote: buildbot: change(s) sent successfully
remote: sent email to roundup at report at bugs.python.org
remote: notified python-checkins at python.org of incoming changeset
0ffaf1079a7a
remote: error: incoming.irker hook raised an exception: [Errno 111]
Connection refused
remote: notified python-checkins at python.org of incoming changeset
3801ee5d5d73
remote: error: incoming.irker hook raised an exception: [Errno 111]
Connection refused
remote: notified python-checkins at python.org of incoming changeset
b6a9f8fd9443
remote: error: incoming.irker hook raised an exception: [Errno 111]
Connection refused
remote: notified python-checkins at python.org of incoming changeset
3f7d5c235d82
remote: error: incoming.irker hook raised an exception: [Errno 111]
Connection refused
"""

- -- 
Jes?s Cea Avi?n                         _/_/      _/_/_/        _/_/_/
jcea at jcea.es - http://www.jcea.es/     _/_/    _/_/  _/_/    _/_/  _/_/
jabber / xmpp:jcea at jabber.org         _/_/    _/_/          _/_/_/_/_/
.                              _/_/  _/_/    _/_/          _/_/  _/_/
"Things are not so easy"      _/_/  _/_/    _/_/  _/_/    _/_/  _/_/
"My name is Dump, Core Dump"   _/_/_/        _/_/_/      _/_/  _/_/
"El amor es poner tu felicidad en la felicidad de otro" - Leibniz
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with undefined - http://www.enigmail.net/

iQCVAwUBUNsdf5lgi5GaxT1NAQKuzQP+IPef5nx00zKdUwL4LoLDds05Dl+WtrFu
Vs+Nvm4haa1+NNJ1owodtA5Xp01pDhMrhv4dvFcfEdbF2zLi3h8Xo+9oO6sEGhqE
cMJZJxRCa4RdC9zpFzw0jWS7Udn/j91veWqaR/HLPYeKWcaXqWOegI+f2aoCBbQ7
5cd8Ynqihxw=
=xUEy
-----END PGP SIGNATURE-----



----------------------------------------
Subject:
[Python-Dev] push changesets hooks failing
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
Looks like IRC bot is broken for last days.
I constantly get the same, but it related only to IRC, not to HG repo itself.

On Wed, Dec 26, 2012 at 5:53 PM, Jesus Cea <jcea at jcea.es> wrote:



-- 
Thanks,
Andrew Svetlov



----------------------------------------
Subject:
[Python-Dev] push changesets hooks failing
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Should now be fixed.  I updated the daemon behind the hook to the newest
version, and hope it will be more stable now.

Georg

On 12/26/2012 05:07 PM, Andrew Svetlov wrote:





----------------------------------------
Subject:
[Python-Dev] push changesets hooks failing
----------------------------------------
Author: Andrew Svetlo
Attributes: []Content: 
Thanks

On Wed, Dec 26, 2012 at 7:50 PM, Georg Brandl <g.brandl at gmx.net> wrote:



-- 
Thanks,
Andrew Svetlov

