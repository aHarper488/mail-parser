
============================================================================
Subject: [Python-Dev] Draft PEP and reference implementation of a Python
	launcher for Windows
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Draft PEP and reference implementation of a Python
	launcher for Windows
----------------------------------------
Author: Mark Hammon
Attributes: []Content: 
Hi all,
   During the huge thread about PEP 394, a suggestion was made that a 
"launcher" for Python on Windows could be implemented which would allow 
for some of the guidelines in that PEP to apply to the Windows version 
of Python.

I've attached the first draft of a PEP for such a launcher and put the 
first draft of the "reference implementation" (implemented in Python 3) 
at http://starship.python.net/crew/mhammond/downloads/python_launcher.py

The reference implementation is fairly complete WRT the features 
described in the PEP.  However, I'm sure there will be comments on both 
the PEP itself and the implementation, so I'm putting them up for 
discussion now rather than spending more time polishing things which may 
end up changing.  I'll start work on a C implementation once there is 
broad agreement on the functionality in the reference implementation.

All comments welcome, including, but not limited to:

* Is this really PEP material, or will turning the PEP into a regular 
spec be suitable?

* If it is a PEP, is it "Standards Track" or "Informational"?

* Does the functionality described strike the right balance between 
simplicity and usefulness?

* Does the strategy have general support from Martin, who as the person 
making Python distributions would need to be involved in at least some 
aspects of it (specifically, having the installer MSI install the 
launcher and update the file associations to reference it).

Thanks,

Mark
-------------- next part --------------
An embedded and charset-unspecified text was scrubbed...
Name: launcher-pep.txt
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110319/6b99a0e6/attachment.txt>



----------------------------------------
Subject:
[Python-Dev] Draft PEP and reference implementation of a Python
	launcher for Windows
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Terry Reedy <tjreedy at udel.edu> writes:


For clarity: the reason it's not enough information is because the
kernel doesn't go hunting for the interpreter. The shebang line needs to
specify the full filesystem path of the interpreter program.

-- 
 \     ?The Vatican is not a state.? a state must have territory. This |
  `\         is a palace with gardens, about as big as an average golf |
_o__)                         course.? ?Geoffrey Robertson, 2010-09-18 |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Draft PEP and reference implementation of a Python
	launcher for Windows
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Sun, 20 Mar 2011 15:35:02 -0700, Westley =?ISO-8859-1?Q?Mart=EDnez?= <anikom15 at gmail.com> wrote:

+1.  If one of the points is to be compatible with PEP 394, then
the above seems like a bad idea.

However, I don't use windows, so I don't know if my vote should
count ;)

--
R. David Murray           http://www.bitdance.com

