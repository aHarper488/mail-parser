
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (3.1): Do not add txt
	files twice.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (3.1): Do not add txt
	files twice.
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
I added the "if dir=='cjkencodings':" to msi.py, based on tests for other 
subdirectories in Lib/tests/. Can you explain me why cjkencodings should not 
have a special case? The fix should maybe be ported to 3.2, 3.3 and 2.7.

Victor

Le dimanche 05 juin 2011 11:00:30, martin.v.loewis a ?crit :

