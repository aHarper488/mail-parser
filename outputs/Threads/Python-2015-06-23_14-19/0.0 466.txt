
============================================================================
Subject: [Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
Post Count: 20
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 


Ok.


  Requires-Dist: [Windows] pywin32 1.0+

That's simpler, shorter, and less ambiguous. Easier to
parse for package managers.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: MRA
Attributes: []Content: 
David Lyon wrote:
'win32' is more specific than 'Windows' and, to me, '1.0+' means
'>=1.0', not '>1.0'.




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Don't you want the PEP to complete? Why this bike-shedding?

I can agree it's shorter. I can't agree that it's simpler,
or less ambiguous.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 

Hi Martin, Happy New Year,


Well, I'm just helping out by pointing out some simpler ways
as Tarek asked me. I was only answering his question. I was out
celebrating so it took longer to reply than normal.

Bike-Shedding ? Me ? which bikeshed? wanting simple?

Anyway, I'm just reading the PEPs and commenting. If there
are some alterations that can be done, lets discuss them.


Cool.

What I'd really like is a 'Code-Repository:' keyword
so that we can install programs/libraries directly into
a system.

I feel that this would really simplify the packaging
landscape, making it much easier for the scientific
community and others to do python software installs.

This would allow us to perphaps have something even
*more modern* than CPAN.

So yes, getting PEP-345 right is important to me.

Have a nice day.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 


Ok.


  Requires-Dist: [Windows] pywin32 1.0+

That's simpler, shorter, and less ambiguous. Easier to
parse for package managers.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: MRA
Attributes: []Content: 
David Lyon wrote:
'win32' is more specific than 'Windows' and, to me, '1.0+' means
'>=1.0', not '>1.0'.




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Don't you want the PEP to complete? Why this bike-shedding?

I can agree it's shorter. I can't agree that it's simpler,
or less ambiguous.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 

Hi Martin, Happy New Year,


Well, I'm just helping out by pointing out some simpler ways
as Tarek asked me. I was only answering his question. I was out
celebrating so it took longer to reply than normal.

Bike-Shedding ? Me ? which bikeshed? wanting simple?

Anyway, I'm just reading the PEPs and commenting. If there
are some alterations that can be done, lets discuss them.


Cool.

What I'd really like is a 'Code-Repository:' keyword
so that we can install programs/libraries directly into
a system.

I feel that this would really simplify the packaging
landscape, making it much easier for the scientific
community and others to do python software installs.

This would allow us to perphaps have something even
*more modern* than CPAN.

So yes, getting PEP-345 right is important to me.

Have a nice day.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 


Ok.


  Requires-Dist: [Windows] pywin32 1.0+

That's simpler, shorter, and less ambiguous. Easier to
parse for package managers.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: MRA
Attributes: []Content: 
David Lyon wrote:
'win32' is more specific than 'Windows' and, to me, '1.0+' means
'>=1.0', not '>1.0'.




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Don't you want the PEP to complete? Why this bike-shedding?

I can agree it's shorter. I can't agree that it's simpler,
or less ambiguous.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 

Hi Martin, Happy New Year,


Well, I'm just helping out by pointing out some simpler ways
as Tarek asked me. I was only answering his question. I was out
celebrating so it took longer to reply than normal.

Bike-Shedding ? Me ? which bikeshed? wanting simple?

Anyway, I'm just reading the PEPs and commenting. If there
are some alterations that can be done, lets discuss them.


Cool.

What I'd really like is a 'Code-Repository:' keyword
so that we can install programs/libraries directly into
a system.

I feel that this would really simplify the packaging
landscape, making it much easier for the scientific
community and others to do python software installs.

This would allow us to perphaps have something even
*more modern* than CPAN.

So yes, getting PEP-345 right is important to me.

Have a nice day.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 


Ok.


  Requires-Dist: [Windows] pywin32 1.0+

That's simpler, shorter, and less ambiguous. Easier to
parse for package managers.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: MRA
Attributes: []Content: 
David Lyon wrote:
'win32' is more specific than 'Windows' and, to me, '1.0+' means
'>=1.0', not '>1.0'.




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Don't you want the PEP to complete? Why this bike-shedding?

I can agree it's shorter. I can't agree that it's simpler,
or less ambiguous.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 

Hi Martin, Happy New Year,


Well, I'm just helping out by pointing out some simpler ways
as Tarek asked me. I was only answering his question. I was out
celebrating so it took longer to reply than normal.

Bike-Shedding ? Me ? which bikeshed? wanting simple?

Anyway, I'm just reading the PEPs and commenting. If there
are some alterations that can be done, lets discuss them.


Cool.

What I'd really like is a 'Code-Repository:' keyword
so that we can install programs/libraries directly into
a system.

I feel that this would really simplify the packaging
landscape, making it much easier for the scientific
community and others to do python software installs.

This would allow us to perphaps have something even
*more modern* than CPAN.

So yes, getting PEP-345 right is important to me.

Have a nice day.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 


Ok.


  Requires-Dist: [Windows] pywin32 1.0+

That's simpler, shorter, and less ambiguous. Easier to
parse for package managers.

David







----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: MRA
Attributes: []Content: 
David Lyon wrote:
'win32' is more specific than 'Windows' and, to me, '1.0+' means
'>=1.0', not '>1.0'.




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

Don't you want the PEP to complete? Why this bike-shedding?

I can agree it's shorter. I can't agree that it's simpler,
or less ambiguous.

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] Proposing PEP 345 : Metadata for Python Software
 Packages 1.2
----------------------------------------
Author: David Lyo
Attributes: []Content: 

Hi Martin, Happy New Year,


Well, I'm just helping out by pointing out some simpler ways
as Tarek asked me. I was only answering his question. I was out
celebrating so it took longer to reply than normal.

Bike-Shedding ? Me ? which bikeshed? wanting simple?

Anyway, I'm just reading the PEPs and commenting. If there
are some alterations that can be done, lets discuss them.


Cool.

What I'd really like is a 'Code-Repository:' keyword
so that we can install programs/libraries directly into
a system.

I feel that this would really simplify the packaging
landscape, making it much easier for the scientific
community and others to do python software installs.

This would allow us to perphaps have something even
*more modern* than CPAN.

So yes, getting PEP-345 right is important to me.

Have a nice day.

David





