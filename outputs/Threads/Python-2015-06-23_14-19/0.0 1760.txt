
============================================================================
Subject: [Python-Dev] Keeping __init__.py empty for Python packages used for
	module grouping.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Keeping __init__.py empty for Python packages used for
	module grouping.
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 
Looking at http://docs.python.org/dev/library/html.html#module-html it would appear that we've created a new module with a single trivial function.

In reality, there was already a python package, html, that served to group two loosely related modules, html.parser and html.entities.

ISTM, that if we're going to use python packages as "namespace containers" for categorizing modules, then the top level __init__ namespace should be left empty.

Before the placement of html.escape() becomes set in stone, I think we should consider putting it somewhere else.


Raymond


