
============================================================================
Subject: [Python-Dev] [Python-checkins] Daily reference leaks
	(a2cf07135e4f): sum=6
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] Daily reference leaks
	(a2cf07135e4f): sum=6
----------------------------------------
Author: Mark Shanno
Attributes: []Content: 
solipsis at pitrou.net wrote:

These leaks are due to 6e5855854a2e: ?Implement
PEP 412: Key-sharing dictionaries (closes #13903)?.

They both occur in tests for tempfile.TemporaryDirectory,
although I don't know what is special about that code.

I'll investigate further when I have time.

Cheers,
Mark.

