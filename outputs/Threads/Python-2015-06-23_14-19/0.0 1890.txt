
============================================================================
Subject: [Python-Dev] cpython: we can call singleton types now
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: we can call singleton types now
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 07/30/11 17:03, benjamin.peterson wrote:

I know this is technically correct, but it will look like you mean "calling
type with None as argument" (same for the other singletons).

Probably better to say something like "``type(None)()`` produces ...".

Georg


