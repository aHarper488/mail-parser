
============================================================================
Subject: [Python-Dev] Debian wheezy,
	amd64: make not finding files for bz2 and other packages
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Debian wheezy,
	amd64: make not finding files for bz2 and other packages
----------------------------------------
Author: Edward C. Jone
Attributes: []Content: 
I use up-to-date Debian testing (wheezy), amd64 architecture.
I have made a "clone" of the developmental version of Python 3.3.
"make -s -j3" prints:

====
...
Python build finished, but the necessary bits to build these modules were
not found:
_bz2               _curses            _curses_panel
_dbm               _gdbm              _lzma
_sqlite3           _ssl               readline
zlib
To find the necessary bits, look in setup.py in detect_modules() for the
module's name.


Failed to build these modules:
_crypt             nis

[101752 refs]
====

I looked into bz2.  My system already contained the Debian packages 
libbz2-dev,
libbz2-1.0, and bzip2.  From the Debian website, I got the list of all the
files in these three packages:

====
Filelist of package libbz2-dev in wheezy of architecture amd64

/usr/include/bzlib.h
/usr/lib/x86_64-linux-gnu/libbz2.a
/usr/lib/x86_64-linux-gnu/libbz2.so
/usr/share/doc/libbz2-dev
====
Filelist of package libbz2-1.0 in wheezy of architecture amd64

/lib/x86_64-linux-gnu/libbz2.so.1
/lib/x86_64-linux-gnu/libbz2.so.1.0
/lib/x86_64-linux-gnu/libbz2.so.1.0.4
/usr/share/doc/libbz2-1.0/changelog.Debian.gz
/usr/share/doc/libbz2-1.0/changelog.gz
/usr/share/doc/libbz2-1.0/copyright
====
Filelist of package bzip2 in wheezy of architecture amd64

/bin/bunzip2
/bin/bzcat
/bin/bzcmp
/bin/bzdiff
/bin/bzegrep
/bin/bzexe
/bin/bzfgrep
/bin/bzgrep
/bin/bzip2
/bin/bzip2recover
/bin/bzless
/bin/bzmore
/usr/share/doc/bzip2/changelog.Debian.gz
/usr/share/doc/bzip2/changelog.gz
/usr/share/doc/bzip2/copyright
/usr/share/man/man1/bunzip2.1.gz
/usr/share/man/man1/bzcat.1.gz
/usr/share/man/man1/bzcmp.1.gz
/usr/share/man/man1/bzdiff.1.gz
/usr/share/man/man1/bzegrep.1.gz
/usr/share/man/man1/bzexe.1.gz
/usr/share/man/man1/bzfgrep.1.gz
/usr/share/man/man1/bzgrep.1.gz
/usr/share/man/man1/bzip2.1.gz
/usr/share/man/man1/bzip2recover.1.gz
/usr/share/man/man1/bzless.1.gz
/usr/share/man/man1/bzmore.1.gz
====

What is the problem?  Does wheezy amd64 put files in unusual places?


