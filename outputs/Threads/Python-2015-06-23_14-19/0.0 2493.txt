
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython (2.7): Fix closes
 Issue12529 - cgi.parse_header failure on double quotes and
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.7): Fix closes
 Issue12529 - cgi.parse_header failure on double quotes and
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Oct 20, 2011 at 2:53 AM, senthil.kumaran
<python-checkins at python.org> wrote:

NEWS entry? (same question for the later _sre fix)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.7): Fix closes
 Issue12529 - cgi.parse_header failure on double quotes and
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Thu, Oct 20, 2011 at 07:17:10AM +1000, Nick Coghlan wrote:

Added. Thanks for catching this.

For some reason, I had slight doubt, if those issues were NEWS worthy
items.  IIRC, devguide recommends that a NEWS entry be added for all fixes
made, but still had a suspicion, if we unnecessarily add up too many
entries to NEWS.

Thanks,
Senthil




----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython (2.7): Fix closes
 Issue12529 - cgi.parse_header failure on double quotes and
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
My take is that a further fix or tweak to something that already has a NEWS
entry for the current release doesn't get a new entry, but everything else
does. "What's New" is the place to get selective.

--
Nick Coghlan (via Gmail on Android, so likely to be more terse than usual)
On Oct 21, 2011 2:46 AM, "Senthil Kumaran" <senthil at uthcode.com> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111021/14687f32/attachment-0001.html>

