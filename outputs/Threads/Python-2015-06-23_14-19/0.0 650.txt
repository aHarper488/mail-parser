
============================================================================
Subject: [Python-Dev] Fwd: Readability of hex strings (Was: Use of coding
	cookie in 3.x stdlib)
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Fwd: Readability of hex strings (Was: Use of coding
	cookie in 3.x stdlib)
----------------------------------------
Author: anatoly techtoni
Attributes: []Content: 
Don't you think it is a bad idea to require people to subscribe to
post to python-ideas of redirected there?
--
anatoly t.




---------- Forwarded message ----------
From:  <python-ideas-owner at python.org>
Date: Mon, Jul 26, 2010 at 11:24 PM
Subject: Re: [Python-Dev] Readability of hex strings (Was: Use of
coding cookie in 3.x stdlib)
To: techtonik at gmail.com


You are not allowed to post to this mailing list, and your message has
been automatically rejected. ?If you think that your messages are
being rejected in error, contact the mailing list owner at
python-ideas-owner at python.org.



---------- Forwarded message ----------
From:?anatoly techtonik <techtonik at gmail.com>
To:?Alexandre Vassalotti <alexandre at peadrop.com>
Date:?Mon, 26 Jul 2010 23:24:15 +0300
Subject:?Re: [Python-Dev] Readability of hex strings (Was: Use of
coding cookie in 3.x stdlib)
On Mon, Jul 26, 2010 at 9:42 PM, Alexandre Vassalotti
<alexandre at peadrop.com> wrote:

The idea is to make "print f.read(50)" usable.
Your current solution is:

-Beautiful is better than ugly.
-Explicit is better than implicit.
-Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
-Sparse is better than dense.
-Readability counts.
Special cases aren't special enough to break the rules.
-Although practicality beats purity.
Errors should never pass silently.
...

--
anatoly t.



