
============================================================================
Subject: [Python-Dev] Warnings
Post Count: 14
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 
When updating the documentation, please don't go overboard with warnings.
The docs need to be worded affirmatively -- say what a tool does and show how to use it correctly.
See http://docs.python.org/documenting/style.html#affirmative-tone

The docs for the subprocess module currently have SEVEN warning boxes on one page:
http://docs.python.org/library/subprocess.html#module-subprocess
The implicit message is that our tools are hazardous and should be avoided.

Please show some restraint and aim for clean looking, high-quality technical writing without the FUD.

Look at the SQLite3 docs for an example of good writing.  The prevention of SQL injection attacks is discussed briefly and effectively without big red boxes littering the page.


Raymond






-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111130/943b6f09/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Glyp
Attributes: []Content: 

On Dec 1, 2011, at 1:10 AM, Raymond Hettinger wrote:


I'm not convinced this is actually a great example of how to outline pitfalls clearly; it doesn't say what an SQL injection attack is, or what the consequences might be.

Also, it's not the best example of a positive tone.  The narrative is:

You probably want to do X.
Don't do Y, because it will make you vulnerable to a Q attack.
Instead, do Z.
Here's an example of Y.  Don't do it!
Okay, finally, here's an example of Z.

It would be better to say "You probably want to do X.  Here's how you do X, with Z.  Here's an example of Z."  Then, later, discuss why some people want to do Y, and why you should avoid that impulse.

However, what 'subprocess' is doing clearly isn't an improvement, it's not an effective introduction to secure process execution, just a reference document punctuated with ambiguous anxiety.  sqlite3 is at least somewhat specific :).

I think both of these documents point to a need for a recommended idiom for discussing security, or at least common antipatterns, within the Python documentation.  I like the IETF's "security considerations" section, because it separates things off into a section that can be referred to later, once the developer has had an opportunity to grasp the basics.  Any section with security implications can easily say "please refer to the 'security considerations' section for important information on how to avoid common mistakes" without turning into a big security digression on its own.

-glyph

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111201/692f0dde/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Dec 1, 2011 at 4:10 PM, Raymond Hettinger
<raymond.hettinger at gmail.com> wrote:

I have no problem with eliminating a lot of those specific warnings -
I kept them there in the last rewrite (and added a couple of new ones)
because avoiding shell injection vulnerabilities is such a driving
theme behind the subprocess module design. Since I was already
changing a lot of other things, messing with that aspect really wasn't
high on my priority list.

Now that we have the "frequently used arguments" section, though, the
rest of the warnings could fairly readily be downgraded to notes or
inline references to that section.


I do object to you calling genuine attempts to educate programmers
about security issues FUD, though. It's not FUD - novice programmers
inflict shell injection, script injection and SQL injection
vulnerabilities on the world every day. The multiple warnings are
there in the subprocess docs because people often only look at the
documentation for the specific function they're interested in, not at
the broader context of the page it is part of.

"Overkill" is a legitimate complaint, but calling attempts to
highlight genuinely insecure practices FUD is the kind of attitude
that has given the world so many years of persistent vulnerability to
buffer overflow attacks :P

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Dec 1, 2011 at 5:15 PM, Glyph <glyph at twistedmatrix.com> wrote:

I like that approach - one of the problems with online docs is the
fact people don't read them in order, hence the proliferation of
warnings for the subprocess module. A clear "Security Considerations"
section with appropriate cross links would allow us to be clear and
explicit about common problems without littering the docs with red
warning boxes for security issues that are inherent in a particular
task rather than being a Python-specific problem.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Dec 1, 2011 at 5:36 PM, Nick Coghlan <ncoghlan at gmail.com> wrote:

I created http://bugs.python.org/issue13515 to propose a specific
documentation style guide adopt along these lines (expanded a bit to
cover other cross-cutting concerns like the pipe buffer blocking I/O
problem in subprocess).

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 01.12.2011 07:10, schrieb Raymond Hettinger:

Obviously, +1.

Georg




----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 30Nov2011 22:10, Raymond Hettinger <raymond.hettinger at gmail.com> wrote:
| When updating the documentation, please don't go overboard with warnings.
| The docs need to be worded affirmatively -- say what a tool does and show how to use it correctly.
| See http://docs.python.org/documenting/style.html#affirmative-tone

I come to this late, but if we're going after the docs...

At the above link one finds this text:

  This assures that files are flushed [...]

It does not. It _ensures_ that files are flushed. The doco style "affirmative
tone" _assures_. The coding practice _ensures_!

Pedanticly,
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

There is one evil which...should never be passed over in silence but be
continually publicly attacked, and that is corruption of the language...
        - W.H. Auden



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Raymond Hettinge
Attributes: []Content: 

On Dec 6, 2011, at 7:23 PM, Cameron Simpson wrote:


I can assure you that I've ensured that you're fully insured ;-)

Raymond
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111207/0cbaddb9/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 07.12.2011 02:23, schrieb Cameron Simpson:

Oh, come on, surely this doesn't effect the casual reader?

Georg




----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Georg Brandl wrote:

No, of course not -- although it might /affect/ said reader by causing 
him/her to think, "I don't think that word means what you think it 
means..."  ;)

Seriously, it's best to use the correct words with the correct meanings. 
  If someone is willing to fix it, let them.

~Ethan~



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Ben Wolfso
Attributes: []Content: 
On Wed, Dec 7, 2011 at 11:00 AM, Ethan Furman <ethan at stoneleaf.us> wrote:

I'm sure this hypothetical reader will then look "assure" up in the
OED and find this:

 5. To make certain the occurrence or arrival of (an event); to ensure.

-- 
Ben Wolfson
"Human kind has used its intelligence to vary the flavour of drinks,
which may be sweet, aromatic, fermented or spirit-based. ... Family
and social life also offer numerous other occasions to consume drinks
for pleasure." [Larousse, "Drink" entry]



----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Georg Brandl <g.brandl at gmx.net> writes:


Some readers could of been confused irregardless.

-- 
 \       ?We must find our way to a time when faith, without evidence, |
  `\    disgraces anyone who would claim it.? ?Sam Harris, _The End of |
_o__)                                                     Faith_, 2004 |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Tres Seave
Attributes: []Content: 
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On 12/07/2011 01:22 PM, Georg Brandl wrote:

/me presumes an ironic mispeling there. ;)


Tres.
- -- 
===================================================================
Tres Seaver          +1 540-429-0999          tseaver at palladion.com
Palladion Software   "Excellence by Design"    http://palladion.com
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)
Comment: Using GnuPG with Mozilla - http://enigmail.mozdev.org/

iEYEARECAAYFAk7fyZgACgkQ+gerLs4ltQ5eaQCeL+E4CVxa1BWhm/MsPw29u/Ym
QnUAoKBOY37dNA9aT5TZkv4hu9ixZjBn
=jg86
-----END PGP SIGNATURE-----




----------------------------------------
Subject:
[Python-Dev] Warnings
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Georg Brandl writes:

 > Oh, come on, surely this doesn't effect the casual reader?

Casual readers aren't effective in any case; you want to hear the
opinions of those who care.


