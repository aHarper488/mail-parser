
============================================================================
Subject: [Python-Dev] cpython (3.2): Fix closes Issue12359 - Minor
 update to module import description.
Post Count: 3
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Fix closes Issue12359 - Minor
 update to module import description.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 20.06.2011 02:42, senthil.kumaran wrote:

By just adding "or the current directory", you've actually made this more
confusing: now the reader will wonder when it's the script directory and when
it's the current directory.

Georg




----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Fix closes Issue12359 - Minor
 update to module import description.
----------------------------------------
Author: Senthil Kumara
Attributes: []Content: 
On Mon, Jun 20, 2011 at 09:13:07AM +0200, Georg Brandl wrote:

I added that statement in the bracket, after looking at another
instance in the next para which had this mention. I think, the point
here is that the reader would understand, where the import is looking
for the module based on the context.  Fine with removing this sentence
("or the current directory"), if statement explains the idea better
without it.

-- 
Senthil



----------------------------------------
Subject:
[Python-Dev] cpython (3.2): Fix closes Issue12359 - Minor
 update to module import description.
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
On 20.06.2011 09:34, Senthil Kumaran wrote:

Thanks!

Georg



