
============================================================================
Subject: [Python-Dev] type vs. class terminology
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] type vs. class terminology
----------------------------------------
Author: Chris Jerdone
Attributes: []Content: 
I would like to know when we should use "class" in the Python 3
documentation, and when we should use "type."  Are these terms
synonymous in Python 3, and do we have a preference for which to use
and when?

I'm sure this has been discussed before.  But if this terminology
issue has already been resolved, the resolution doesn't seem to be
reflected in the docs.  For example, the glossary entries for type and
class don't reference each other.

Thoughts?

--Chris



----------------------------------------
Subject:
[Python-Dev] type vs. class terminology
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Nov 26, 2012 at 3:01 PM, Chris Jerdonek <chris.jerdonek at gmail.com>wrote:


The historical distinction between "builtin types" and "user-defined
classes" predates new-style classes (which unified the type system) and
Python 3 (which eliminated the "instance" type that was provided to
preserve the legacy user-defined class semantics in Python 2). The glossary
unfortunately still reflects this distinction, which no longer exists in
Python 3.

A slightly more useful distinction would be if type was used consistently
to refer to type(x), while class was used to refer to x.__class__, since
they can and do differ in the case of proxy types (like weakref.proxy).
However, it's probably too late for that kind of fine distinction - in
reality, the two terms are now used pretty much interchangeably.

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121126/7ee8f491/attachment.html>



----------------------------------------
Subject:
[Python-Dev] type vs. class terminology
----------------------------------------
Author: Xavier More
Attributes: []Content: 

On 2012-11-26, at 07:54 , Nick Coghlan wrote:


There's an other possible usage which is between `type` subclasses and
`type` instances (`type` essentially becomes a synonym for `metaclass`),
I've seen that kind of usage at least once in the docs:
http://docs.python.org/2/c-api/object.html#PyObject_IsInstance




----------------------------------------
Subject:
[Python-Dev] type vs. class terminology
----------------------------------------
Author: Hrvoje Niksi
Attributes: []Content: 
On 11/26/2012 06:01 AM, Chris Jerdonek wrote:

Some people like to use "class" for the subset of types created by 
Python's "class" statement or its moral equivalent (explicit invocation 
of the metaclass). It makes sense that "class" is used to create 
classes. The word "type" then refers to both classes and built-in and 
extension types, such as "list" or "array.array".



----------------------------------------
Subject:
[Python-Dev] type vs. class terminology
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
Hm. None of the distinctions brought up so far really hit true with me
(though they all are part of the picture). For example, I think the
distinction between type(x) and x.__class__ is rarely significant -- I
bet that if anyone were to rely on this they'd first have to change a
lot of code that used one or the other without much thinking about the
difference. So we might as well consider these equivalent.

Part of the distinction is probably just in historical usage -- many
idioms hark back to when there *was* a difference.

If I had to invent an artificial difference, I'd say that I use "type"
when talking about the type as a fairly abstract concept, such as the
type of a variable (which may be required to be in a given set, for
example), whereas I'll say "class" when I'm interested in the class as
an object, or its definition (through a class statement). So, if I'm
going to ask for the name, the phrase "the name of the class" rolls
easier off my tongue than "the name of the type". OTOH if I'm going to
just assert set membership, I might slightly prefer "x's type must be
int or str".

It's clear that we ought to at least cross-link the two glossary
entries and point out that they are the same concept (type(x) vs.
x.__class__) notwithstanding. I don't think it's important to try and
eradicate the word type from our conversation or our docs -- so no
sweeping global changes, please. However, if you come across a use of
either one in the docs that seems odd given modern usage, feel free to
clean it up, perhaps after checking with someone else to make sure
your intuition matches that of others.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] type vs. class terminology
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Sun, Nov 25, 2012 at 9:01 PM, Chris Jerdonek <chris.jerdonek at gmail.com>wrote:


Good question,

[shameless plug follows, I post this because I truly believe it's very
relevant to the discussion]

I had the same doubts some months ago, which led to writing this article
(relevant to Python 3):
http://eli.thegreenplace.net/2012/03/30/python-objects-types-classes-and-instances-a-glossary/

It examines the class vs. type issue, as well as object vs. instance
And this diagram can also be useful to understand how similar built-in and
user-defined types are in Python 3:
http://eli.thegreenplace.net/2012/04/03/the-fundamental-types-of-python-a-diagram/

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20121129/bf5d19a4/attachment.html>



----------------------------------------
Subject:
[Python-Dev] type vs. class terminology
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 11/29/2012 11:55 PM, Eli Bendersky wrote:


You usage seems to me to be stuck in the now mostly useless Python 1 
distinction between built-in types and user-defined classes. In Python 
3, all instances of type are classes, whether defined with the C or 
Python API. Indeed, the existence of a C API to make classes is an 
implementation detail, not a language feature. The second parameter of 
isinstance or issubclass is a class or set thereof (implemented as a 
(homogeneous!) tuple for historical reasons), without distinction of 
definition mode. When using an imported class, it mostly does not matter 
how the class was defined.

I agree with Guido that it is more useful to use 'class' for the 
concrete run-time object and 'type' (except when referring to the object 
of that name) for abstract (and static) types. (From this viewpoint, 
duck-typing rather than duck-classing *is* the proper term).

Consider the quote from the manual. "An object?s type determines the 
operations that the object supports (e.g., ?does it have a length??)." 
An object potentially supports len(), and one might say should support 
it, if and only if it is a 'finite collection'. That is a 'type' (duck 
type) of object, but not a class in the Python sense. Whether an object 
actually supports len depends on its run-time class. Similarly, an 
object 'should' support sqrt if it is a non-negative scalar number or a 
complex number. Square-root-able is also an abstract type, not a 
concrete class.

I might suggest that types are used to reason about programs while 
classes are used to execute programs.

-- 
Terry Jan Reedy



