
============================================================================
Subject: [Python-Dev] cpython: Fix findnocoding.p and pysource.py scripts
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Fix findnocoding.p and pysource.py scripts
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
On 01.08.12 21:16, victor.stinner wrote:


infile.close() is unnecessary here.



----------------------------------------
Subject:
[Python-Dev] cpython: Fix findnocoding.p and pysource.py scripts
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
2012/8/1 Serhiy Storchaka <storchaka at gmail.com>:

Ah yes correct, I forgot this one. The new changeset 8ace059cdffd removes it.

It is not perfect, there is still a race condition in
looks_like_python() (pysource.py) if KeyboardInterrupt occurs between
the file is opened and the beginning of the "with infile" block, but
it don't think that it is really important ;-)

Victor

