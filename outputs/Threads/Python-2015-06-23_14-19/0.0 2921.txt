
============================================================================
Subject: [Python-Dev] problem after installing python 3.2.2
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] problem after installing python 3.2.2
----------------------------------------
Author: Vahid Ghader
Attributes: []Content: 
hi 
i have downloaded and installed python 3.2.2 but still when i use python in terminal it show's:
root at debian:~# python
Python 2.6.6 (r266:84292, Dec 27 2010, 00:02:40) 
[GCC 4.4.5] on linux2
Type "help", "copyright", "credits" or "license" for more information.
how can i change the default to python 3.2.2?
tHanks.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120218/1d358cd1/attachment.html>



----------------------------------------
Subject:
[Python-Dev] problem after installing python 3.2.2
----------------------------------------
Author: Sandro Tos
Attributes: []Content: 
Hello Vahid,
i'm sorry but this mailing list is not the right place where to ask
such question, I suggest get in touch with
http://mail.python.org/mailman/listinfo/python-list for support.

On Sat, Feb 18, 2012 at 18:15, Vahid Ghaderi <vahid_male1384 at yahoo.com> wrote:

you're using the system 'python' here, not the new installed, which
probably has landed in /usr/local (or where you installed it).


from a Debian Developer perspective, I'd suggest you not to switch the
Debian default interpreter to python3.2 since it will make several
system tools/debian packages to fail. If you need 3.2 explicitly,
state it in the shebang or call the script with py3.2 explicitly.

Regards,
--
Sandro Tosi (aka morph, morpheus, matrixhasu)
My website: http://matrixhasu.altervista.org/
Me at Debian: http://wiki.debian.org/SandroTosi

