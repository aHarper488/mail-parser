
============================================================================
Subject: [Python-Dev] [Python-ideas] PEP
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-ideas] PEP
----------------------------------------
Author: Masklin
Attributes: []Content: 
On 2012-03-08, at 22:08 , Mark Janssen wrote:

Python calls ``hash`` on the object and uses the result.


No. Not that it makes sense, people could ask for object hashes on
their own and end up with the same result.


What you're asking does not make sense, the dict key is not the name
but whatever object is bound to the name.

And yes I've used non-string objects as names before: tuples, frozensets,
integers, my own objects, ?

