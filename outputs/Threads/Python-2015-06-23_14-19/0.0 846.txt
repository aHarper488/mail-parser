
============================================================================
Subject: [Python-Dev] Reasons behind misleading TypeError message
	when	passing the wrong number of arguments to a method
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message
	when	passing the wrong number of arguments to a method
----------------------------------------
Author: Stephen J. Turnbul
Attributes: []Content: 
Giampaolo Rodol? writes:
 > >>> class A:
 > ...     def echo(self, x):
 > ...             return x
 > ...
 > >>> a = A()
 > >>> a.echo()
 > Traceback (most recent call last):
 >   File "<stdin>", line 1, in <module>
 > TypeError: echo() takes exactly 2 arguments (1 given)
 > >>>
 > 
 > I bet my last 2 cents this has already been raised in past but I want
 > to give it a try and revamp the subject anyway.
 > Is there a reason why the error shouldn't be adjusted to state that
 > *1* argument is actually required instead of 2?

As a function, it does take two arguments, and can be called
explicitly that way, no?  Adjustment is not enough, the message needs
to be substantially rewritten.  Something like

TypeError: invoked as a method, echo() takes exactly 1 argument (0 given)

captures the semantics, but is perhaps too verbose.



----------------------------------------
Subject:
[Python-Dev] Reasons behind misleading TypeError message
	when	passing the wrong number of arguments to a method
----------------------------------------
Author: Floris Bruynoogh
Attributes: []Content: 
On Thu, May 20, 2010 at 11:55:02AM +0900, Stephen J. Turnbull wrote:

How about:

TypeError: bound method echo() takes exactly 1 argument (0 given)

That way you can also have: "unbound method echo() ...".  And it's as
semantically correct as the short "echo() takes ..."

Not having looked at the code I don't know how hard it is for the code
that raises this traceback to notice if it's a bound or unbound method
tough.

Regards
Floris

-- 
Debian GNU/Linux -- The Power of Freedom
www.debian.org | www.gnu.org | www.kernel.org

