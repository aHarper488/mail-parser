
============================================================================
Subject: [Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
Post Count: 18
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Thu, Nov 24, 2011 at 20:29, Antoine Pitrou <solipsis at pitrou.net> wrote:


Sure. Updated the default branch just now and built:

$1 -m timeit -s'import fileread_bytearray' 'fileread_bytearray.justread()'
1000 loops, best of 3: 1.14 msec per loop
$1 -m timeit -s'import fileread_bytearray'
'fileread_bytearray.readandcopy()'
100 loops, best of 3: 2.78 msec per loop
$1 -m timeit -s'import fileread_bytearray' 'fileread_bytearray.readinto()'
1000 loops, best of 3: 1.6 msec per loop

Strange. Although here, like in python 2, the performance of readinto is
close to justread and much faster than readandcopy, but justread itself is
much slower than in 2.7 and 3.2!

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111124/09bbde84/attachment.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
What if you broke up the read and built the final string object up. I
always assumed this is where the real gain was with read_into.
On Nov 25, 2011 5:55 AM, "Eli Bendersky" <eliben at gmail.com> wrote:

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/86a8864a/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Fri, Nov 25, 2011 at 00:02, Matt Joiner <anacrolix at gmail.com> wrote:

Matt, I'm not sure what you mean by this - can you suggest the code?

Also, I'd be happy to know if anyone else reproduces this as well on other
machines/OSes.

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/2a9e3e74/attachment.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
Eli,

Example coming shortly, the differences are quite significant.

On Fri, Nov 25, 2011 at 9:41 AM, Eli Bendersky <eliben at gmail.com> wrote:



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
It's my impression that the readinto method does not fully support the
buffer interface I was expecting. I've never had cause to use it until
now. I've created a question on SO that describes my confusion:

http://stackoverflow.com/q/8263899/149482

Also I saw some comments on "top-posting" am I guilty of this? Gmail
defaults to putting my response above the previous email.

On Fri, Nov 25, 2011 at 11:49 AM, Matt Joiner <anacrolix at gmail.com> wrote:



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
On Fri, Nov 25, 2011 at 12:07 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Cheers, this seems to be what I wanted. Unfortunately it doesn't
perform noticeably better if I do this.

Eli, the use pattern I was referring to is when you read in chunks,
and and append to a running buffer. Presumably if you know in advance
the size of the data, you can readinto directly to a region of a
bytearray. There by avoiding having to allocate a temporary buffer for
the read, and creating a new buffer containing the running buffer,
plus the new.

Strangely, I find that your readandcopy is faster at this, but not by
much, than readinto. Here's the code, it's a bit explicit, but then so
was the original:

BUFSIZE = 0x10000

def justread():
    # Just read a file's contents into a string/bytes object
    f = open(FILENAME, 'rb')
    s = b''
    while True:
        b = f.read(BUFSIZE)
        if not b:
            break
        s += b

def readandcopy():
    # Read a file's contents and copy them into a bytearray.
    # An extra copy is done here.
    f = open(FILENAME, 'rb')
    s = bytearray()
    while True:
        b = f.read(BUFSIZE)
        if not b:
            break
        s += b

def readinto():
    # Read a file's contents directly into a bytearray,
    # hopefully employing its buffer interface
    f = open(FILENAME, 'rb')
    s = bytearray(os.path.getsize(FILENAME))
    o = 0
    while True:
        b = f.readinto(memoryview(s)[o:o+BUFSIZE])
        if not b:
            break
        o += b

And the timings:

$ python3 -O -m timeit 'import fileread_bytearray'
'fileread_bytearray.justread()'
10 loops, best of 3: 298 msec per loop
$ python3 -O -m timeit 'import fileread_bytearray'
'fileread_bytearray.readandcopy()'
100 loops, best of 3: 9.22 msec per loop
$ python3 -O -m timeit 'import fileread_bytearray'
'fileread_bytearray.readinto()'
100 loops, best of 3: 9.31 msec per loop

The file was 10MB. I expected readinto to perform much better than
readandcopy. I expected readandcopy to perform slightly better than
justread. This clearly isn't the case.


If tehre's a magical option in gmail someone knows about, please tell.




----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

Just to be clear, there were two separate issues raised here. One is the
speed regression of readinto() from 2.7 to 3.2, and the other is the
relative slowness of justread() in 3.3

Regarding the second, I'm not sure it's an issue because I tried a larger
file (100MB and then also 300MB) and the speed of 3.3 is now on par with
3.2 and 2.7

However, the original question remains - on the 100MB file also, although
in 2.7 readinto is 35% faster than readandcopy(), on 3.2 it's about the
same speed (even a few % slower). That said, I now observe with Python 3.3
the same speed as with 2.7, including the readinto() speedup - so it
appears that the readinto() regression has been solved in 3.3? Any clue
about where it happened (i.e. which bug/changeset)?

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/83d16831/attachment.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
What is 'python3' on your machine? If it's 3.2, then this is consistent
with my results. Try it with 3.3 and for a larger file (say ~100MB and up),
you may see the same speed as on 2.7

Also, why do you think chunked reads are better here than slurping the
whole file into the bytearray in one go? If you need it wholly in memory
anyway, why not just issue a single read?

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/3645e2b7/attachment.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
On Fri, Nov 25, 2011 at 5:41 PM, Eli Bendersky <eliben at gmail.com> wrote:

It's Python 3.2. I tried it for larger files and got some interesting results.

readinto() for 10MB files, reading 10MB all at once:

readinto/2.7 100 loops, best of 3: 8.6 msec per loop
readinto/3.2 10 loops, best of 3: 29.6 msec per loop
readinto/3.3 100 loops, best of 3: 19.5 msec per loop

With 100KB chunks for the 10MB file (annotated with #):

matt at stanley:~/Desktop$ for f in read bytearray_read readinto; do for
v in 2.7 3.2 3.3; do echo -n "$f/$v "; "python$v" -m timeit -s 'import
readinto' "readinto.$f()"; done; done
read/2.7 100 loops, best of 3: 7.86 msec per loop # this is actually
faster than the 10MB read
read/3.2 10 loops, best of 3: 253 msec per loop # wtf?
read/3.3 10 loops, best of 3: 747 msec per loop # wtf??
bytearray_read/2.7 100 loops, best of 3: 7.9 msec per loop
bytearray_read/3.2 100 loops, best of 3: 7.48 msec per loop
bytearray_read/3.3 100 loops, best of 3: 15.8 msec per loop # wtf?
readinto/2.7 100 loops, best of 3: 8.93 msec per loop
readinto/3.2 100 loops, best of 3: 10.3 msec per loop # suddenly 3.2
is performing well?
readinto/3.3 10 loops, best of 3: 20.4 msec per loop

Here's the code: http://pastebin.com/nUy3kWHQ


Sometimes it's not available all at once, I do a lot of socket
programming, so this case is of interest to me. As shown above, it's
also faster for python2.7. readinto() should also be significantly
faster for this case, tho it isn't.




----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
You can see in the tests on the largest buffer size tested, 8192, that
the naive "read" actually outperforms readinto(). It's possibly by
extrapolating into significantly larger buffer sizes that readinto()
gets left behind. It's also reasonable to assume that this wasn't
tested thoroughly.

On Fri, Nov 25, 2011 at 9:55 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
On Fri, Nov 25, 2011 at 10:04 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Is there any way to bring back that optimization? a 30 to 100x slow
down on probably one of the most common operations... string
contatenation, is very noticeable. In python3.3, this is representing
a 0.7s stall building a 10MB string. Python 2.7 did this in 0.007s.


This change makes readinto() faster for 100K chunks than the other 2
methods and clears the differences between the versions.
readinto/2.7 100 loops, best of 3: 6.54 msec per loop
readinto/3.2 100 loops, best of 3: 7.64 msec per loop
readinto/3.3 100 loops, best of 3: 7.39 msec per loop

Updated test code: http://pastebin.com/8cEYG3BD


So as I think Eli suggested, the readinto() performance issue goes
away with large enough reads, I'd put down the differences to some
unrelated language changes.

However the performance drop on read(): Python 3.2 is 30x slower than
2.7, and 3.3 is 100x slower than 2.7.



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 

Great, thanks. This is an important change, definitely something to wait
for in 3.3
Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/e4ff0f8f/attachment.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Matt Joine
Attributes: []Content: 
I was under the impression this is already in 3.3?

On Nov 25, 2011 10:58 PM, "Eli Bendersky" <eliben at gmail.com> wrote:
although
3.3
for in 3.3
http://mail.python.org/mailman/options/python-dev/anacrolix%40gmail.com
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/13fd18d5/attachment.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
On Fri, Nov 25, 2011 at 14:02, Matt Joiner <anacrolix at gmail.com> wrote:

Sure, but 3.3 wasn't released yet.

Eli

P.S. Top-posting again ;-)
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/d25d238e/attachment.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 25 November 2011 11:37, Matt Joiner <anacrolix at gmail.com> wrote:

It's a fundamental, but sadly not well-understood, consequence of
having immutable strings. Concatenating immutable strings in a loop is
quadratic. There are many ways of working around it (languages like C#
and Java have string builder classes, I believe, and in Python you can
use StringIO or build a list and join at the end) but that's as far as
it goes.

The optimisation mentioned was an attempt (by mutating an existing
string when the runtime determined that it was safe to do so) to hide
the consequences of this fact from end-users who didn't fully
understand the issues. It was relatively effective, but like any such
case (floating point is another common example) it did some level of
harm at the same time as it helped (by obscuring the issue further).

It would be nice to have the optimisation back if it's easy enough to
do so, for quick-and-dirty code, but it is not a good idea to rely on
it (and it's especially unwise to base benchmarks on it working :-))

Paul.



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Amaury Forgeot d'Ar
Attributes: []Content: 
2011/11/25 Paul Moore <p.f.moore at gmail.com>


Note that this string optimization hack is still present in Python 3,
but it now acts on *unicode* strings, not bytes.

-- 
Amaury Forgeot d'Arc
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20111125/40b9d293/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Paul Moor
Attributes: []Content: 
On 25 November 2011 15:07, Amaury Forgeot d'Arc <amauryfa at gmail.com> wrote:

Ah, yes. That makes sense.
Paul



----------------------------------------
Subject:
[Python-Dev] file.readinto performance regression in Python 3.2
 vs. 2.7?
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 25/11/2011 15:48, Paul Moore wrote:

Although for concatenating immutable bytes presumably the same hack 
would be *possible*.

Michael


-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html


