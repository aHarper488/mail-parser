
============================================================================
Subject: [Python-Dev] r84204 - in python/branches/py3k/Lib: os.py
	test/test_os.py
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] r84204 - in python/branches/py3k/Lib: os.py
	test/test_os.py
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 19 Aug 2010 19:10:19 +0200 (CEST)
victor.stinner <python-checkins at python.org> wrote:

You should not catch warnings, but silence them using constructs
provided by the warnings module:

    with warnings.catch_warnings():
        warnings.simplefilter(ignore, BytesWarning)
        # the rest of your code


Otherwise you'll get buggy behaviour where e.g. env[b'PATH'] raises
BytesWarning because of an unicode key, but it would have succeeded
otherwise.

Regards

Antoine.



