
============================================================================
Subject: [Python-Dev] [Preview] Comments and change proposals
	on	documentation
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Preview] Comments and change proposals
	on	documentation
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Nick Coghlan wrote:

I wonder what the point of the comment bubbles is? This isn't a 
graphical UI where (contrary to popular opinion) a picture is *not* 
worth a thousand words, but may require a help-bubble to explain. This 
is text. If you want to make a comment on some text, the usual practice 
is to add more text :)

I wasn't able to find a comment bubble that contained anything, so I 
don't know what sort of information you expect them to contain -- every 
one I tried said "0 comments". But it seems to me that comments are 
superfluous, if not actively harmful:

(1) Anything important enough to tell the reader should be included in 
the text, where it can be easily seen, read and printed.

(2) Discovery is lousy -- not only do you need to be running Javascript, 
which many people do not for performance, privacy and convenience[*], 
but you have to carefully mouse-over the paragraph just to see the blue 
bubble, and THEN you have to *precisely* mouse-over the bubble itself.

(3) This will be a horrible and possibly even literally painful 
experience for anyone with a physical disability that makes precise 
positioning of the mouse difficult.

(4) Accessibility for the blind and those using screen readers will 
probably be non-existent.

(5) If the information in the comment bubbles is trivial enough that 
we're happy to say that the blind, the disabled and those who avoid 
Javascript don't need it, then perhaps *nobody* needs it.




[*] In my experience, websites tend to fall into two basic categories: 
those that don't work at all without Javascript, and those that run 
better, faster, and with fewer anti-features and inconveniences without 
Javascript.


-- 
Steven



----------------------------------------
Subject:
[Python-Dev] [Preview] Comments and change proposals
	on	documentation
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
Georg Brandl wrote:


Aha! I never would have guessed that the bubbles are clickable -- I 
thought you just moused-over them and they showed static comments put 
there by the developers, part of the documentation itself. I didn't 
realise that it was for users to add spam^W comments to the page. With 
that perspective, I need to rethink.

Yes, I failed to fully read the instructions you sent, or understand 
them. That's what users do -- they don't read your instructions, and 
they misunderstand them. If your UI isn't easily discoverable, users 
will not be able to use it, and will be frustrated and annoyed. The user 
is always right, even when they're doing it wrong *wink*



Lowering accessibility to parts of the documentation is what I was 
talking about when I said "actively harmful". But now that I have better 
understanding of what the comment system is actually for, I have to rethink.


-- 
Steven

