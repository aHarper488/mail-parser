
============================================================================
Subject: [Python-Dev] Building python 2.7.3 with Visual Studio 2012
	(VS11.0)
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Building python 2.7.3 with Visual Studio 2012
	(VS11.0)
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Tue, Jul 24, 2012 at 1:02 PM, Wim Colgate <wimcolgate at gmail.com> wrote:

If you can get it working on VS2010 first, VS2012 can read that
project file, but without converting it'll just run the 2010 compiler
and allow you to use the 2012 IDE.

Competing the actual port from 2010 to 2012 did not appear to be very
hard, but I didn't look to far into it.


You don't need the old VS sub-directories unless you are compiling
with those versions.



----------------------------------------
Subject:
[Python-Dev] Building python 2.7.3 with Visual Studio 2012
	(VS11.0)
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I think nobody *really* knows at this point. Microsoft has a tradition
of breaking Python with every VS release, by making slight incompatible
changes in the C library. With VS 2012, on the one hand, they give
explicit consideration to VS 2010 and continued use of its tools; OTOH,
they also deliberately broke XP support in the CRT.

So you have to try for yourself. If Python passes the test suite (as
good as the official release), then the build was successful.

A different matter is dependent libraries (zlib, openssl, Tcl/Tk, ...).
You also have to build those with VS 2012 (if you want to use them),
each one likely posing its own challenges.

If you manage to succeed, don't forget to post your findings here.
Also if you fail.

Good luck,
Martin


