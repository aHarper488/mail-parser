
============================================================================
Subject: [Python-Dev] Dash
Post Count: 23
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
What type of dash is preferable in the documentation? The en dash (?) or 
the em dash (?)?




----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
I believe there are only a few places where en-dashes should be used,
for most things you should use either em-dash or hyphen. Consult your
trusted typography source (for US English, please, punctuation
preferences vary by locale). E.g. Google for "em dash en dash".

On Thu, Jul 18, 2013 at 10:38 AM, Serhiy Storchaka <storchaka at gmail.com> wrote:

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
18.07.13 20:48, Guido van Rossum ???????(??):

Currently Python documentation in most cases uses en-dashes. Should we 
replace them to em-dashes? Should we remove spaces around dashes?

Or we should replace a half-dozen of em-dashes found in Python 
documentation to en-dashes?

I believe all hypens used in place of dash should be replaced to dash 
(but to en- or em- dash?) in any case.





----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Brian Curti
Attributes: []Content: 
On Jul 18, 2013 1:46 PM, "Serhiy Storchaka" <storchaka at gmail.com> wrote:
replace them to em-dashes? Should we remove spaces around dashes?
documentation to en-dashes?
(but to en- or em- dash?) in any case.

Besides visual consistency in a couple of places, is there a reason to care
enough to make a wholesale change?
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130718/bbe6e397/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Thu, Jul 18, 2013 at 11:46 AM, Serhiy Storchaka <storchaka at gmail.com> wrote:

This may well be a relic from times when in typewritten text there
were really only two types of dashes: a short one, typed as a hyphen,
used for both hyphen and en-dash (and minus sign), and a long one,
typed as two hyphens, used for em-dash.

I suspect at some point the conversion to HTML or typeset text changed
so that two hyphens mean en-dash and three mean em-dash, and few
people noticed (and nobody really wants to type three hyphens except
for a handful of typographical nuts).


If my theory is right that makes sense. Especially if it's only a half-dozen.


But I wouldn't go change hundreds of hyphens -- chances are that you
get bored reviewing the scripted automation you're using and break a
few places. I'd say there are more important things to fix in the docs
(like writing better overviews for many modules and classes). If we
have a writing guide for the Python docs it would be useful to add
something about dash types though. And if we don't have a writing
guide, perhaps it makes sense to start one?

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Richard Oudker
Attributes: []Content: 
On 18/07/2013 7:55pm, Guido van Rossum wrote:

Weren't the docs originally done LaTeX?  In LaTeX "--" is also an 
en-dash and "---" is an em-dash.

-- 
Richard




----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Thu, Jul 18, 2013 at 12:16 PM, Richard Oudkerk <shibturn at gmail.com> wrote:

Right -- but I've been typing two hyphens to mean an em-dash all my life. :-)

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
Hi,

On Thu, Jul 18, 2013 at 7:38 PM, Serhiy Storchaka <storchaka at gmail.com> wrote:

Both should be used where appropriate [0].  The em dash is more
common, however "--" (commonly used to indicate the em dash in e.g.
emails) gets converted to an en dash by Sphinx [1].  I noticed this a
while ago, and I started using "---" in the documentation whenever I
wanted an em dash.
If this is not documented it should be added to the "documenting" page
of the devguide, so that people start using the right ones and convert
the wrong ones when they come across them.

Best Regards,
Ezio Melotti

[0]: http://en.wikipedia.org/wiki/Dash
[1]: https://bitbucket.org/birkenfeld/sphinx/src/default/sphinx/util/smartypants.py#cl-261



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On 19/07/13 04:46, Serhiy Storchaka wrote:

It depends on the context, and I don't believe you could completely automate the process (at least not without using something that understands natural language, like NLTK, and probably not even then). I think it will require a human reader to review them, like any other style and grammar edit.

Wikipedia has a good overview which mostly agrees with my typesetting and style books:

https://en.wikipedia.org/wiki/Dash

Hyphens are commonly used for compound words, although in practice hyphenated words gradually lose the hyphen. E.g. we usually write "inbox" rather than "in-box", although we still write "in-tray". Hyphens are also used at the end of the line to break a word to the next line.

En-dashes are used for durations and ranges (sometimes with a thin space on either side, otherwise a regular space can be used). E.g. "October?December".

En-dash is also used when making a compound word from words which themselves are compound words, e.g. "The pre?World War II economy" joins "pre-" with "World War II", not just "World".

Em-dashes are used for parenthetical asides, or to indicate a break in speech or thought. They are often used when a comma is too weak and a period is too strong?a bit like a colon.

Different sources give different recommendations regarding spaces around dashes. The Chicago Manual of Style agrees with most British sources that em-dashes should never have spaces around them, but the New York Times style guide sets hair-spaces around them. Unusually for me, I tend to agree with the NY Times on this one. A regular space is usually too wide.

Many of these conventions are style conventions, rather than strictly grammatical. For example, although English grammar says we can use an en-dash to make ranges of numbers, the SI standard recommends against expressions like "10?50 volts" since it can be mistaken for subtraction, and recommends "10 to 50 volts".

Optimistically, I think it would probably be safe[1] to replace " -- " or " --- " in text with "\N{THIN SPACE}\N{EM DASH}\N{THIN SPACE}" (or \N{HAIR SPACE} if you prefer) without human review, but for any other changes, I wouldn't even try to automate it.






[1] Famous last words.


-- 
Steven



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Serhiy Storchaka <storchaka at gmail.com> writes:


They have different purposes; use whichever is appropriate for the
context.

This isn't an official Python developer position. But all of ?-?, ???,
???, ???, etc. have distinct and common meanings in English text. So
it's a mistaken idea to think of ?which dash is preferable? since they
are *all* preferable for their distinct uses.

-- 
 \                ?Science doesn't work by vote and it doesn't work by |
  `\        authority.? ?Richard Dawkins, _Big Mistake_ (The Guardian, |
_o__)                                                      2006-12-27) |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On 19 July 2013 17:25, Ben Finney <ben+python at benfinney.id.au> wrote:

I don't know about "common". I had no idea there were 3 dash types
until a couple of weeks ago. I thought there were only two
(short/hyphen/minus-sign and long/em-dash). I still don't really know
what the difference is between the two long ones (en-dash vs em-dash).

Cheers,
Nick.

--
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le Fri, 19 Jul 2013 18:38:05 +1000,
Nick Coghlan <ncoghlan at gmail.com> a ?crit :

But there aren't 3 dash types. There are at least 5 of them!
https://en.wikipedia.org/wiki/Dash#Common_dashes

(not to mention that Twisted has/had its own dash, IIRC)

cheers

Antoine.





----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On 19/07/13 18:38, Nick Coghlan wrote:


*Way* more than three.

hyphen
minus sign (not the same as a hyphen!)
en-dash
em-dash
two-em-dash
three-em-dash
figure dash
horizontal bar
swung dash

plus another half-dozen or so non-English hyphens, all of which have different typographic (and sometimes grammatical) purposes. Wikipedia has a good description:

https://en.wikipedia.org/wiki/Dash

but a brief summary is:

? hyphen - is the smallest and is used for compound words like "out-tray";
? minus sign ? is normally raised compared to the others;
? en-dash ? should be the width of "n" and is used for ranges, like Mon?Fri;
? em-dash ? should be the width of "m" and is used for a break stronger than a comma but weaker than a full stop;
? horizontal bar ? should be longer than the em-dash and is used for quotations in some European languages;
? swung dash ? is normally found in dictionaries (paper, not Python) to stand in for the word being defined.


But of course different fonts follow these rules to a greater or lesser degree.



-- 
Steven



----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
18.07.13 21:54, Brian Curtin ???????(??):

Single hyphen instead of a dash just looks too ugly to me.

Trying to fix this I noticed that the documentation is inconsistent 
regarding the dash. Sometimes en and em dashes meet in the same line and 
in the same sentence. I.e.:

"""
The :mod:`weakref` module also allows creating proxy objects which 
behave like
weak references --- an object referenced only by proxy objects is 
deallocated --
but instead of requiring an explicit call to retrieve the object, the proxy
"""





----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
18.07.13 21:55, Guido van Rossum ???????(??):

I'm sorry, I were wrong. Actually em-dash used much more then en-dash in 
Python documentation.





----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
19.07.13 00:49, Ezio Melotti ???????(??):

Of course I looked in Wikipedia before asking on this list. And I were 
surprised that en-dash allowed in same places as em-dashes:

"""
Either version may be used to denote a break in a sentence or to set off 
parenthetical statements, although writers are generally cautioned to 
use a single form consistently within their work. In this function, en 
dashes are used with spaces and em dashes are used without them:[1]

[Em dash:] In matters of grave importance, style?not sincerity?is the 
vital thing.

[En dash:] In matters of grave importance, style ? not sincerity ? is 
the vital thing.
"""

I'm asking only about this case, when the dash is used to denote a break 
in a sentence or to set off parenthetical statements. Currently Python 
documentation uses fourth variants:

1. A single hyphen "-" surrounded with spaces. "In matters of grave 
importance, style - not sincerity - is the vital thing."
I think this case should be converted to one of followed, but to what of 
them?

2. A double hyphen "--" (converted to en-dash) surrounded with spaces. 
"In matters of grave importance, style -- not sincerity -- is the vital 
thing."

3. A triple hyphen "---" (converted to em-dash) surrounded with spaces. 
"In matters of grave importance, style --- not sincerity --- is the 
vital thing."

4. A triple hyphen "---" (converted to em-dash) which isn't surrounded 
with spaces. "In matters of grave importance, style---not sincerity---is 
the vital thing."


This is common in TeX.





----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
19.07.13 10:25, Ben Finney ???????(??):

I mean only a context where a dash is used to denote a break in a 
sentence or to set off parenthetical statements. Currently Python 
documentation uses four different variants (see my answer to Ezio).





----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
19.07.13 07:51, Steven D'Aprano ???????(??):

No, it is not safe. " -- " used in code examples in which it doesn't 
converted to en-dash. Also we should correct tables and underlines in 
titles.

I have a manually written and multiple times checked patch (modifies 
about 640 lines) which safely replaces " -- " with " --- " but I doubt 
what actually should be a preferable dash.




----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Serhiy Storchaka <storchaka at gmail.com> writes:


Yes, and both of those are used for different purposes, so ?what type is
preferable (for all the purposes you mention)? is a poor way of looking
at it, IMO.

Local style guides have rules about when and how to use the different
dashes for specific contexts.

(For myself, I prefer parenthetic dashes to be en dashes ???, and a
break in a sentence to be marked by an em dash ???. But I also prefer to
surround the dash with space for the latter purpose, so my preference
isn't universal.)

Wikipedia's article (already referred) discusses the matter fairly well
<URL:https://en.wikipedia.org/wiki/Dashes>.


I'd recommend that the Python documentation BDFL should choose and
dictate an existing well-groomed style guide for Python documentation,
preferably one which explicitly talks about when to use each of the
commonly-used dashes.

-- 
 \        ?If you have the facts on your side, pound the facts. If you |
  `\     have the law on your side, pound the law. If you have neither |
_o__)                       on your side, pound the table.? ?anonymous |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Serhiy Storchaka <storchaka at gmail.com> writes:


But, as you've also discovered, many authors will type two hyphens ?--?
when they want an em dash ???, yet this is rendered to an en dash ???.
So the prevalence of the latter in the documentation probably does not
reflect the preferences of the authors.

-- 
 \         ?Nature hath given men one tongue but two ears, that we may |
  `\          hear from others twice as much as we speak.? ?Epictetus, |
_o__)                                                      _Fragments_ |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Serhiy Storchaka <storchaka at gmail.com> writes:


That's two separate cases:

* denote a break in a sentence
* set off parenthetical statements

In my experience, an em dash is commonly used for the former, and en
dashes commonly used for the latter. Using the same dash for both is
unusual (and IMO needlessly ambiguous to the reader).


All your examples are only for parenthetical statements. Can you find
examples of the former, where a break (not parenthetical) in the
sentence is intended?

-- 
 \     ?When I was a kid I used to pray every night for a new bicycle. |
  `\    Then I realised that the Lord doesn't work that way so I stole |
_o__)                   one and asked Him to forgive me.? ?Emo Philips |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
19.07.13 22:32, Ben Finney ???????(??):

In Wikipedia they considered as one case.


This is definitely should be described in the official guide. I never 
heard about this.


(2) and (4) are from Wikipedia (which consider it as one case). Here are 
real random examples from Python documentation:

1. "Common installation tools such as ``Distribute`` and ``pip`` work as 
expected with venvs - i.e. when a venv is active, they install Python 
packages into the venv without needing to be told to do so explicitly."

2. "Enter *string* in the table of "interned" strings and return the 
interned string which is *string* itself or a copy."

3. "The :class:`set` type is mutable --- the contents can be changed 
using methods like :meth:`add` and :meth:`remove`."

4. "Three locking mechanisms are used---dot locking and, if available, 
the :c:func:`flock` and :c:func:`lockf` system calls."





----------------------------------------
Subject:
[Python-Dev] Dash
----------------------------------------
Author: Serhiy Storchak
Attributes: []Content: 
I have opened an issue (http://bugs.python.org/issue18529) for patches.


