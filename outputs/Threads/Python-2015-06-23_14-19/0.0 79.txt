
============================================================================
Subject: [Python-Dev] PEP 328, relative imports and Python 2.7
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 328, relative imports and Python 2.7
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
While talking about Python 2.6 -> 2.7 transitions, the subject of relative and
absolute imports has come up.  PEP 328 states that absolute imports will be
enabled by default in Python 2.7, however I cannot verify that this has
actually happened.

* Misc/NEWS doesn't mention it
* What's New in Python 2.7 doesn't mention it either
* My own limited testing on trunk indicates it hasn't happened

Now, truth be told I only spent 15 minutes investigating this so I could
certainly have missed something.  Are absolute imports enabled by default in
Python 2.7?  If not, given that we're into beta, I don't think we can do it
now, so I would suggest updating the PEP.  If they have been enabled then we
need to update NEWS and What's New.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100421/ebc6a6b5/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 328, relative imports and Python 2.7
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Wed, Apr 21, 2010 at 2:40 PM, Barry Warsaw <barry at python.org> wrote:

I'm fairly sure it hasn't.  I brought this up on python-dev in
February (around Feb 2nd;  thread entitled 'Absolute imports in Python
2.x'), but for some reason I can only find the tail end of that thread
on mail.python.org:

http://mail.python.org/pipermail/python-dev/2010-February/097458.html


Agreed.  There's also the question of whether deprecation warnings or
-3 warnings should be raised;  see

http://bugs.python.org/issue7844

Mark



----------------------------------------
Subject:
[Python-Dev] PEP 328, relative imports and Python 2.7
----------------------------------------
Author: Mark Dickinso
Attributes: []Content: 
On Wed, Apr 21, 2010 at 2:56 PM, Mark Dickinson <dickinsm at gmail.com> wrote:

Ah, here's a better link to a different archive of the previous discussion.

http://www.mail-archive.com/python-dev at python.org/msg45275.html

Mark



----------------------------------------
Subject:
[Python-Dev] PEP 328, relative imports and Python 2.7
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Wed, Apr 21, 2010 at 06:58, Mark Dickinson <dickinsm at gmail.com> wrote:



And it looks like it mostly got hijacked by a discussion of how to keep
track of long-term issues. =)

As for changing the semantics of this, I would love to see it happen, but as
Barry said, into a beta probably means no. So we should at least add a Py3K
warning (if there isn't one already) as implicit relative imports cannot be
guaranteed correct by 2to3 in the face of ambiguity. Plus the silencing of
DeprecationWarnings by default means it won't trigger more noise for users.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100421/93e6b1f6/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] PEP 328, relative imports and Python 2.7
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 21, 2010, at 02:56 PM, Mark Dickinson wrote:


For the time being, I've removed the sentence from the PEP that says absolute
imports will be enabled by default in Python 2.7.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100421/3e8402bb/attachment.pgp>

