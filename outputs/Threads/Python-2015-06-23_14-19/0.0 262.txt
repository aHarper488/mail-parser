
============================================================================
Subject: [Python-Dev] Change to the Distutils / Distutils2 workflow
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Change to the Distutils / Distutils2 workflow
----------------------------------------
Author: =?ISO-8859-1?Q?Tarek_Ziad=E9?
Attributes: []Content: 
Hey

We discussed with Eric about the debugging workflow and we agreed that
our life would be easier if every bug fix would land first in
Distutils2 when it makes sense, then get backported to Distutils1.

For other core-devs that would mean that your patches should be done
against hg.python.org/distutils2, which uses unittest2. Then Eric and
I would take care of the backporting.

I am planning to set up a wiki page with the workflow as soon as I get a chance.

Thanks
Tarek

-- 
Tarek Ziad? | http://ziade.org



----------------------------------------
Subject:
[Python-Dev] Change to the Distutils / Distutils2 workflow
----------------------------------------
Author: =?UTF-8?B?w4lyaWMgQXJhdWpv?
Attributes: []Content: 
Hi everyone,

I have sketched a workflow guide on
http://wiki.python.org/moin/Distutils/FixingBugs

Cheers


