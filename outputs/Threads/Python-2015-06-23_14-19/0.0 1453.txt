
============================================================================
Subject: [Python-Dev] cpython: Add Py_RETURN_NOTIMPLEMENTED macro. Fixes
 #12724.
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: Add Py_RETURN_NOTIMPLEMENTED macro. Fixes
 #12724.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le lundi 15 ao?t 2011 ? 13:16 +1000, Nick Coghlan a ?crit :

AFAICT, often with True and False:

    x = (some condition) ? Py_True : Py_False;
    Py_INCREF(x);
    return x;

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] cpython: Add Py_RETURN_NOTIMPLEMENTED macro. Fixes
 #12724.
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Aug 15, 2011, at 05:46 AM, Raymond Hettinger wrote:


I can see the small value in the convenience, but I tend to agree with Raymond
here.  I think we have to be careful about not descending into macro
obfuscation world.

-Barry



----------------------------------------
Subject:
[Python-Dev] cpython: Add Py_RETURN_NOTIMPLEMENTED macro. Fixes
 #12724.
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Aug 16, 2011, at 08:32 AM, Nick Coghlan wrote:


My problem with Py_RETURN(x) is that it's not clear that it also does an
incref, and without that, I think it's *more* confusing to use rather than
just writing it out explicitly, Py_RETURN_NONE's historic existence
notwithstanding.

So I'd opt for #1, unless we can agree on a better color for the bikeshed.

-Barry



----------------------------------------
Subject:
[Python-Dev] cpython: Add Py_RETURN_NOTIMPLEMENTED macro. Fixes
 #12724.
----------------------------------------
Author: Ethan Furma
Attributes: []Content: 
Barry Warsaw wrote:

My apologies if this is just noise, but are there RETURN macros that 
don't do an INCREF?

~Ethan~

