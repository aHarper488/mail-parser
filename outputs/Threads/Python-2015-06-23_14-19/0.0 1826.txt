
============================================================================
Subject: [Python-Dev] Failed to install PIL on windows,
	may have something to do with python implementaton
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Failed to install PIL on windows,
	may have something to do with python implementaton
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Thu, 14 Jul 2011 09:02:01 +0800, smith jack <thinke365 at gmail.com> wrote:

Yes, but this list is about the *development* of Python.  For help on
topic like this, which are about using Python, you'll have better luck
getting an answer if you post to python-list.  There are also probably
a lot more people with experience using Python on windows on that list
than there are here.  Since we release Python as an MSI, we're not too
likely to know how to work with it on Windows if you don't install it
from the MSI.

--
R. David Murray           http://www.bitdance.com

