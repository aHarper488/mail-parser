
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: store the current scope
 on the stack right away
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: store the current scope
 on the stack right away
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Jun 30, 2011 at 1:53 PM, benjamin.peterson
<python-checkins at python.org> wrote:

Heh, I was going to comment that the spate of refleaks after your
previous commit looked mighty suspicious :)

End result is simpler, cleaner code overall, though.

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

