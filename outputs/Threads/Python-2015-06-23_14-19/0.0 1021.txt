
============================================================================
Subject: [Python-Dev] [RELEASED] Python 2.7.1
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [RELEASED] Python 2.7.1
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
On behalf of the Python development team, I'm happy as a clam to announce the
immediate availability of Python 2.7.1.

2.7 includes many features that were first released in Python 3.1. The faster io
module, the new nested with statement syntax, improved float repr, set literals,
dictionary views, and the memoryview object have been backported from 3.1. Other
features include an ordered dictionary implementation, unittests improvements, a
new sysconfig module, auto-numbering of fields in the str/unicode format method,
and support for ttk Tile in Tkinter.  For a more extensive list of changes in
2.7, see http://doc.python.org/dev/whatsnew/2.7.html or Misc/NEWS in the Python
distribution.

To download Python 2.7.1 visit:

     http://www.python.org/download/releases/2.7.1/

The 2.7.1 changelog is at:

     http://svn.python.org/projects/python/tags/r271/Misc/NEWS

2.7 documentation can be found at:

     http://docs.python.org/2.7/

This is a production release.  Please report any bugs you find to the bug
tracker:

     http://bugs.python.org/


Enjoy!

--
Benjamin Peterson
Release Manager
benjamin at python.org
(on behalf of the entire python-dev team and 2.7.1's contributors)

