
============================================================================
Subject: [Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: fwierzbicki at gmail.co
Attributes: []Content: 
On Thu, Feb 28, 2013 at 11:24 AM, Chris Jerdonek
<chris.jerdonek at gmail.com> wrote:
What about a plat-java section to parallel plat-aix4, plat-darwin,
etc? The analogy being that the Java platform is somewhat analogous to
being it's own os? And these areas are not active when on other
operating systems...

-Frank



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: fwierzbicki at gmail.co
Attributes: []Content: 
On Thu, Feb 28, 2013 at 11:46 AM, fwierzbicki at gmail.com
<fwierzbicki at gmail.com> wrote:
Oh yeah and this does not preclude the zlib refactoring, and again for
the record once this gets serious I'd want to help in bringing up a
Jython buildbot to track breakage.

-Frank



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 28 Feb 2013 11:46:04 -0800
"fwierzbicki at gmail.com" <fwierzbicki at gmail.com> wrote:

IMHO, we should remove the plat-* directories, they are completely
unmaintained, undocumented, and serve no useful purpose.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: fwierzbicki at gmail.co
Attributes: []Content: 
On Thu, Feb 28, 2013 at 12:00 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:
Oh I didn't know that - so definitely adding to that is right out :)

Really for cases like Jython's zlib.py (no useful code for CPython) I
don't have any trouble keeping them entirely in Jython. It just would
have been fun to delete our Lib/ :)

It would be nice in this particular case if there was a zlib.py that
imported _zlib -- then it would be easy to shim in Jython's version,
whether it is written in a .py file or in Java.

-Frank



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Thu, Feb 28, 2013 at 3:17 PM, fwierzbicki at gmail.com <
fwierzbicki at gmail.com> wrote:


That should be fine as that is what we already do for accelerator modules
anyway. If you want to work towards having an equivalent of CPython's
Modules/ directory so you can ditch your custom Lib/ modules by treating
your specific code as accelerators I think we can move towards that
solution.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130228/a9fdb73e/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: Eric Sno
Attributes: []Content: 
On Thu, Feb 28, 2013 at 1:35 PM, Brett Cannon <brett at python.org> wrote:

+1

-eric



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: fwierzbicki at gmail.co
Attributes: []Content: 
On Thu, Feb 28, 2013 at 12:35 PM, Brett Cannon <brett at python.org> wrote:
Sounds great! I'm betting that implementing PEP 420 on Jython will
make mixed Python/Java code easier to deal with, so _zlib.py might
just end up living next to our Java code. So deleting Jython's Lib/
may still be an option.

-Frank



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Fri, Mar 1, 2013 at 6:35 AM, Brett Cannon <brett at python.org> wrote:

I'd go further and say we *should* move to that solution.

Here's an interesting thought: for pure C modules without a Python
implementation, we can migrate to this architecture even *without*
creating pure Python equivalents. All we shou;d have to do is change
the test of the pure Python version to be that the module *can't be
imported* without the accelerator, rather than the parallel tests that
we normally implement when there's a pure Python alternative to the
accelerated version. (There would likely still be some mucking about
to ensure robust pickle compatibility, since that leaks implementation
details about exact module names if you're not careful)

PyPy, Jython, IronPython would then have RPython, Java, C# versions,
while CPython has a C version, and the test suite should work
regardless. (If PyPy have equivalents in Python, they can either push
them upstream, overwrite the "import the accelerator" version).

Cheers,
Nick.


-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 3 Mar 2013 01:17:35 +1000
Nick Coghlan <ncoghlan at gmail.com> wrote:

What benefit would this have?

Current situation: each Python implementation has its own
implementation of the zlib module (as a C module for CPython, etc.).

New situation: all Python implementations share a single, mostly empty,
zlib.py file. Each Python implementation has its own implementation of
the _zlib module (as a C module for CPython, etc.) which is basically
the same as the former zlib module.

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] Merging Jython code into standard Lib [was Re:
 Python Language Summit at PyCon: Agenda]
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sat, Mar 2, 2013 at 10:28 AM, Antoine Pitrou <solipsis at pitrou.net> wrote:


Bare minimum? They all share the same module docstring. But it could be
extended to explicitly import only the public API into zlib.py, helping to
prevent leaking interpreter-specific APIs by accident (obviously would
still be available off of _zlib if people wanted them).
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20130302/f88314dc/attachment-0001.html>

