
============================================================================
Subject: [Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
Post Count: 8
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
Dan, I once had the more or less the same opinion/question as you with 
regard to ctypes, but I now see at least 3 problems.

1) It seems hard to write it correctly. There are currently 47 open 
ctypes issues, with 9 being feature requests, leaving 38 
behavior-related issues. Tom Heller has not been able to work on it 
since the beginning of 2010 and has formally withdrawn as maintainer. No 
one else that I know of has taken his place.

2) It is not trivial to use it correctly. I think it needs a SWIG-like 
companion script that can write at least first-pass ctypes code from the 
.h header files. Or maybe it could/should use header info at runtime 
(with the .h bundled with a module).

3) It seems to be slower than compiled C extension wrappers. That, at 
least, was the discovery of someone who re-wrote pygame using ctypes. 
(The hope was that using ctypes would aid porting to 3.x, but the time 
penalty was apparently too much for time-critical code.)

If you want to see more use of ctypes in the Python community (though 
not necessarily immediately in the stdlib), feel free to work on any one 
of these problems.

A fourth problem is that people capable of working on ctypes are also 
capable of writing C extensions, and most prefer that. Or some work on 
Cython, which is a third solution.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Hi,

sorry for hooking in here with my usual Cython bias and promotion. When the 
question comes up what a good FFI for Python should look like, it's an 
obvious reaction from my part to throw Cython into the game.

Terry Reedy, 28.08.2011 06:58:

Cython has an active set of developers and a rather large and growing user 
base.

It certainly has lots of open issues in its bug tracker, but most of them 
are there because we *know* where the development needs to go, not so much 
because we don't know how to get there. After all, the semantics of Python 
and C/C++, between which Cython sits, are pretty much established.

Cython compiles to C code for CPython, (hopefully soon [1]) to 
Python+ctypes for PyPy and (mostly [2]) C++/CLI code for IronPython, which 
boils down to the same build time and runtime kind of dependencies that the 
supported Python runtimes have anyway. It does not add dependencies on any 
external libraries by itself, such as the libffi in CPython's ctypes 
implementation.

For the CPython backend, the generated code is very portable and is 
self-contained when compiled against the CPython runtime (plus, obviously, 
libraries that the user code explicitly uses). It generates efficient code 
for all existing CPython versions starting with Python 2.4, with several 
optimisations also for recent CPython versions (including the upcoming 3.3).



Cython is basically Python, so Python developers with some C or C++ 
knowledge tend to get along with it quickly.

I can't say yet how easy it is (or will be) to write code that is portable 
across independent Python implementations, but given that that field is 
still young, there's certainly a lot that can be done to aid this.



 From my experience, this is a "nice to have" more than a requirement. It 
has been requested for Cython a couple of times, especially by new users, 
and there are a couple of scripts out there that do this to some extent. 
But the usual problem is that Cython users (and, similarly, ctypes users) 
do not want a 1:1 mapping of a library API to a Python API (there's SWIG 
for that), and you can't easily get more than a trivial mapping out of a 
script. But, yes, a one-shot generator for the necessary declarations would 
at least help in cases where the API to be wrapped is somewhat large.



Cython code can be as fast as C code, and in some cases, especially when 
developer time is limited, even faster than hand written C extensions. It 
allows for a straight forward optimisation path from regular Python code 
down to the speed of C, and trivial interaction with C code itself, if the 
need arises.

Stefan


[1] The PyPy port of Cython is currently being written as a GSoC project.

[2] The IronPython port of Cython was written to facility a NumPy port to 
the .NET environment. It's currently not a complete port of all Cython 
features.





----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Glyph Lefkowit
Attributes: []Content: 

On Aug 28, 2011, at 7:27 PM, Guido van Rossum wrote:


Unfortunately I don't know a lot about this, but I keep hearing about something called "rffi" that PyPy uses to call C from RPython: <http://readthedocs.org/docs/pypy/en/latest/rffi.html>.  This has some shortcomings currently, most notably the fact that it needs those .h files (and therefore a C compiler) at runtime, so it's currently a non-starter for code distributed to users.  Not to mention the fact that, as you can see, it's not terribly thoroughly documented.  But, that "ExternalCompilationInfo" object looks very promising, since it has fields like "includes", "libraries", etc.

Nevertheless it seems like it's a bit more type-safe than ctypes or cython, and it seems to me that it could cache some of that information that it extracts from header files and store it for later when a compiler might not be around.

Perhaps someone with more PyPy knowledge than I could explain whether this is a realistic contender for other Python runtimes?

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110828/ef91a36b/attachment.html>



----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
"Martin v. L?wis", 30.08.2011 10:46:

I had written a bit about this here:

http://thread.gmane.org/gmane.comp.python.devel/126340/focus=126419

Stefan




----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Vinay Saji
Attributes: []Content: 
Meador Inge <meadori <at> gmail.com> writes:

 

I raised a question about this patch (in the issue tracker).


I presume, since Amaury has commit rights, that he could commit these.

Regards,

Vinay Sajip




----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 8/30/2011 1:05 PM, Guido van Rossum wrote:


Thank you for this elaboration. My earlier comment that ctypes seems to 
be hard to use was based on observation of posts to python-list 
presenting failed attempts (which have included somehow getting function 
signatures wrong) and a sense that ctypes was somehow bypassing the 
public compiler API to make a more direct access via some private api. 
You have explained and named that as the 'linker API', so I understand 
much better now.

Nothing like 'linker API' or 'signature' appears in the ctypes doc. All 
I could find about discovering specific function calling conventions is 
"To find out the correct calling convention you have to look into the C 
header file or the documentation for the function you want to call." 
Perhaps that should be elaborated to explain, as you did above, the need 
to trace macro definitions to find the actual calling convention and the 
need to be aware that macro definitions can change to accommodate 
implementation detail changes even as the surface calling conventions 
seems to remain the same.

-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Stefan Behne
Attributes: []Content: 
Guido van Rossum, 30.08.2011 19:05:

Sure. They even coerce from Python dicts and accept keyword arguments in 
Cython.



Right.



Right again. The declarations that Cython uses describe the API at the C or 
C++ level. They do not describe the ABI. So the situation is the same as 
with ctypes, and the same solutions (or work-arounds) apply, such as 
generating additional glue code that calls macros or reads compile time 
constants, for example. That's the approach that the IronPython backend has 
taken. It's a lot more complex, but also a lot more versatile in the long run.

Stefan




----------------------------------------
Subject:
[Python-Dev] Ctypes and the stdlib (was Re: LZMA compression
	support in 3.3)
----------------------------------------
Author: Jeremy Sander
Attributes: []Content: 
Dan Stromberg wrote:


http://www.riverbankcomputing.co.uk/software/sip/intro

"What is SIP?

One of the features of Python that makes it so powerful is the ability to 
take existing libraries, written in C or C++, and make them available as 
Python extension modules. Such extension modules are often called bindings 
for the library.

SIP is a tool that makes it very easy to create Python bindings for C and 
C++ libraries. It was originally developed to create PyQt, the Python 
bindings for the Qt toolkit, but can be used to create bindings for any C or 
C++ library. "


It's not C++ only. The code for SIP is also in C.

Jeremy



