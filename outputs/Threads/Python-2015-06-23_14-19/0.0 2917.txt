
============================================================================
Subject: [Python-Dev] dll name for embedding?
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] dll name for embedding?
----------------------------------------
Author: Egon Smiw
Attributes: []Content: 
Hi all,
I'm an app developer with a CPython dll in the folder of that app.
In general, are there strict requirements about the dll name
(a preference would be "python.dll" (easy to update (simple replace) ).
I successfully used "python.dll" and a few standard modules,
then I tried to use the sympy library and its import fails with an
AV exception, unless I rename the dll back to the original "python32.dll"
Is there an intrinsic filename requirement inside the CPython dll, modules,
or are name-restrictions to be presumed only in case of third-party libs?



----------------------------------------
Subject:
[Python-Dev] dll name for embedding?
----------------------------------------
Author: Mark Hammon
Attributes: []Content: 
On 17/02/2012 7:44 PM, Egon Smiwa wrote:

Note that this is off-topic for python-dev, which is for the development 
of Python - python-list would be a better choice.  But the short story 
is that given Python extensions have a link-time dependency on the core 
Python DLL, it isn't possible to rename the DLL without breaking all 
extensions built against the original name - this is just how link-time 
dependencies work on Windows.

You may also find http://www.python.org/dev/peps/pep-0384 of interest, 
but this still includes the major version in the DLL name and also 
depends on the authors of the extensions you want to use opting in.

As mentioned above, please followup on python-list.

Cheers,

Mark.

