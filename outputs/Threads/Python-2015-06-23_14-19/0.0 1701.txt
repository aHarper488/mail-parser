
============================================================================
Subject: [Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
Post Count: 9
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Georg Brand
Attributes: []Content: 
Am 08.01.2011 23:22, schrieb Antoine Pitrou:

+1.  Remember, this is not a purely hypothetical statement.


Yep.

Georg




----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sun, Jan 9, 2011 at 00:26, Georg Brandl <g.brandl at gmx.net> wrote:

OK, so the sentence is poorly phrased, but in the list of tasks it is
labeled explicitly as intermediate when one is comfortable with the
process, not a newbie. Does that alleviate the worry you both have?

-Brett




----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Sun, 9 Jan 2011 15:18:12 -0800
Brett Cannon <brett at python.org> wrote:

It does seem to alleviate it :) Sorry for not noticing!
However, could the following be removed from the list:

?Is there a proper unit test that can reproduce the bug??

We don't need or require unit tests to reproduce bugs; and besides,
some things simply are very difficult to write an unit test for. A
reporter need not be an experienced Python developer able (or willing)
to write an elaborate unit test reproducing, for example, a timing
issue involving Unix signals and the IO stack ;)

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jan 10, 2011 at 10:24, Antoine Pitrou <solipsis at pitrou.net> wrote:

Fair enough. I will remove it.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110110/b506f4ac/attachment-0001.html>



----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 10/01/2011 19:05, Brett Cannon wrote:

Well, *often* a test that exposes the issue can be written - and if so 
it is a useful exercise (surely).

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html

-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110110/f024ebef/attachment.html>



----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le lundi 10 janvier 2011 ? 19:26 +0000, Michael Foord a ?crit :

Yes, well, that's a matter of "useful exercise for the contributor" vs.
"required to advance on the issue". AFAICT the "stage" field aims at
conveying the latter piece of information (the current wording says
"unit test *needed*").

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 10/01/2011 19:31, Antoine Pitrou wrote:

Aren't we discussing the dev guide? Discussion about tracker field is 
that away <-----.

Michael



-- 
http://www.voidspace.org.uk/

May you do good and not evil
May you find forgiveness for yourself and forgive others
May you share freely, never taking more than you give.
-- the sqlite blessing http://www.sqlite.org/different.html




----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le lundi 10 janvier 2011 ? 19:37 +0000, Michael Foord a ?crit :

Oh, well. I think we're discussing the directions that a contributor
willing to help triage could give so to advance an issue. I hope I'm not
mistaken.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] devguide: Add an intermediate task of helping
 triage issues (not to be confused with the
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Jan 10, 2011 at 11:44, Antoine Pitrou <solipsis at pitrou.net> wrote:



The doc has already been tweaked on my machine, so there is no need to
continue this discussion.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110111/2fbf0666/attachment.html>

