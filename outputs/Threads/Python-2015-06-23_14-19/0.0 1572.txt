
============================================================================
Subject: [Python-Dev] News of the faulthandler project
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] News of the faulthandler project
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Hi,

Since the end of last december, I'm still working on my fault handler
project:
https://github.com/haypo/faulthandler

You can use it to get more information after a crash or if you program
hangs somewhere. It helps if you don't have access to other debugging
tool (eg. install gdb7+python-gdb.py on Windows is not trivial today) or
if you cannot interact with your program (eg. on a buildbot).

The last version works on Python 2.5, 2.6, 2.7, 3.1 and 3.2, on Windows,
Linux and FreeBSD. It can display the Python backtrace on a fatal fault
(SIGSEGV, SIGFPE, SIGILL, SIGBUS), after a delay in seconds, when an
user signal is received (eg. SIGUSR1) or explicitly (call directly the
dumpbacktrace() function).

By default, it is disabled: you have to call faulthandler.enable() to
install the signal handlers. You can choose in which file the backtrace
is written (sys.stderr by default) and if it displays the backtrace of
the current thread or of all threads. If you use the delay: you can
choose to repeat the operation (dump the backtrace each delay seconds).

The project is now a module, so it is no more enabled by default. It is
more configurable, and has more features. It has a better API (so it was
a good idea to not include it in Python 3.2).

I plan to integrate this project into Python 3.3. I hope that it can
help to debug some buildbots issues, but also any segfault in your
programs.

Note: faulthandler.register() (dump the backtrace when an user signal is
raised) is only available in the development version.

--

The project is not perfect yet:

 - I have to write something to be able to enable the faulthandler
before starting your program (write a program for that?)
 - faulthandler.dumpbacktrace_later() uses alarm() which interrupts the
current system call when SIGALARM is raised: it may be a problem (it's
maybe not a problem if you try to debug a program hang)
 - I don't know if something should be done on a fork()
 - SIGABRT is not handled
 - The module is unloaded using Py_AtExit(): it cannot release
references because the unload function is called too late

--

There are similar projects, tipper and crier, using a signal handler
implemented in Python or a signal handler implemented in Python. These
projects give more information (eg. local variables) and more control on
how the informations are written, but I think that there are less
reliable: it doesn't work if Python hangs (eg. deadlock) and signal
handlers implemented in Python are asynchronous. And there are unable to
catch fatal faults (eg. SIGSEGV).

http://pypi.python.org/pypi/tipper/
https://gist.github.com/737056

Victor




----------------------------------------
Subject:
[Python-Dev] News of the faulthandler project
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Thu, Feb 3, 2011 at 11:05 PM, Victor Stinner
<victor.stinner at haypocalc.com> wrote:

I don't know enough about signal handling to help with your other
remaining concerns, but an appropriate "-X" command line option seem
like a reasonable way to activate it before main starts running (-X is
currently documented as reserved for use by other implementations, but
it's really more a "implementation dependent options" marker)

(+1 on the general idea, though)

Cheers,
Nick.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] News of the faulthandler project
----------------------------------------
Author: Reid Kleckne
Attributes: []Content: 
On Thu, Feb 3, 2011 at 8:05 AM, Victor Stinner
<victor.stinner at haypocalc.com> wrote:

Why not?  That seems useful for debugging assertion failures, although
most C code in Python raises exceptions rather than asserting.

I'm guessing it's because it aborts the process after printing the
backtrace.  You could just clear the signal handler before aborting.

Reid



----------------------------------------
Subject:
[Python-Dev] News of the faulthandler project
----------------------------------------
Author: Victor Stinne
Attributes: []Content: 
Le jeudi 03 f?vrier 2011 ? 12:22 -0500, Reid Kleckner a ?crit :

Just because I forgot to handle it. But I don't know if it is a good
thing to display the Python backtrace on abort() or not. Python uses
abort() on Py_FatalError().

Victor




----------------------------------------
Subject:
[Python-Dev] News of the faulthandler project
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Thu, 03 Feb 2011 21:52:40 +0100
Victor Stinner <victor.stinner at haypocalc.com> wrote:

I think that precisely makes it a good idea.  It's much better to know
where a fatal error was triggered from if you want to have a chance of
at least working around it.

Regards

Antoine.



