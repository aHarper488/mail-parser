
============================================================================
Subject: [Python-Dev] PEP 3147 working implementation
Post Count: 7
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 3147 working implementation
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
I now have a working implementation of PEP 3147 which passes all the existing,
and new, tests.  I'm sure there's still work to do, but I think the branch
is in good enough shape to start getting some feedback from python-dev.

You can grab the changes in several ways.  If you have Bazaar, you can check
out the branch by doing:

% bzr branch lp:~barry/python/pep3147

You can also view a live diff online:

https://code.launchpad.net/~barry/python/pep3147/+merge/22648

or just download the diff, which should apply cleanly against the py3k
Subversion branch (or pretty close):

https://code.launchpad.net/~barry/python/pep3147/+merge/22648/+preview-diff/+files/preview.diff

Modulo some lag time, the diffs should track changes I make to the branch.

You can provide feedback to me directly, as a follow up to this message, or in
Launchpad.  I've also updated the PEP to point to these locations for the
reference implementation.

Cheers,
-Barry

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100401/2282d7d7/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147 working implementation
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Barry Warsaw <barry <at> python.org> writes:

If you want a review, perhaps you should post it to Rietveld.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] PEP 3147 working implementation
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 01, 2010, at 09:27 PM, Antoine Pitrou wrote:


Good idea.

http://codereview.appspot.com/842043/show

I've never used Rietveld before so let me know if I need to do anything to
invite you or otherwise make the review possible.  I will update the PEP.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100401/3b8c13e8/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147 working implementation
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 01, 2010, at 04:12 PM, Barry Warsaw wrote:


Thanks to Antoine for doing a review of the diff on Rietveld.  I've uploaded a
second patch set that addresses these and other issues.

I think there are still a few corners of PEP 3147 not yet implemented, and I
need to update some documentation, but I think it's getting close enough for
consideration soon.  The Rietveld issue is here:

http://codereview.appspot.com/842043/show

Cheers,
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100406/c04e9fff/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147 working implementation
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/4/6 Barry Warsaw <barry at python.org>:

I've now added a review, too.



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] PEP 3147 working implementation
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Apr 06, 2010, at 09:57 PM, Benjamin Peterson wrote:


As did Brett.  Thanks!  I've responded and will upload a new patch set as soon
as I've verified the test suite passes.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100408/d9a96886/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147 working implementation
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
Patch set 4 has been uploaded to Rietveld:

http://codereview.appspot.com/842043/show

This includes some fixes for Windows and support for the __cached__ attribute
on modules.  While I need to do another pass through the PEP to make sure I've
gotten everything, this code is very nearly feature complete, so it's probably
worth getting Guido to pronounce on the PEP pretty soon.

-Barry

P.S. 'bzr branch lp:~barry/python/pep3147'
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100409/07d47791/attachment.pgp>

