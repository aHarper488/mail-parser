
============================================================================
Subject: [Python-Dev] stdlib socket usage and &quot;keepalive&quot;
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] stdlib socket usage and &quot;keepalive&quot;
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Jesus Cea <jcea <at> jcea.es> writes:

The socket timeout doesn't shutdown anything. It just puts a limit on how much
time recv() and send() can block. Then it's up to you to detect whether the
server is still alive (for example by pinging it through whatever means the
application protocol gives you).


I don't know whether there are any negative implications of such solutions.
Granted, the first one is less radical than the second (because servers wouldn't
be impacted).


This could be useful too.

Regards

Antoine.



