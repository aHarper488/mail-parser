
============================================================================
Subject: [Python-Dev] cpython: os.sendfile(): on Linux if offset
 parameter is passed as NULL we were
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: os.sendfile(): on Linux if offset
 parameter is passed as NULL we were
----------------------------------------
Author: =?ISO-8859-1?Q?Giampaolo_Rodol=E0?
Attributes: []Content: 
No we haven't.
I plan to make a unique commit for offset=None on Linux and a serie of
other tests I have implemented for py-sendfile module [1].
In details test for small file, empty file and (most important) large file:
http://code.google.com/p/py-sendfile/source/browse/trunk/test/test_sendfile.py?spec=svn68&r=68#296

[1] http://code.google.com/p/py-sendfile

--- Giampaolo
http://code.google.com/p/pyftpdlib
http://code.google.com/p/psutil



2011/4/19 Antoine Pitrou <solipsis at pitrou.net>:



----------------------------------------
Subject:
[Python-Dev] cpython: os.sendfile(): on Linux if offset
 parameter is passed as NULL we were
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 4/20/2011 2:09 PM, Giampaolo Rodol? wrote:

"No we haven't" what? Such out-of-context responses exemplify why 
top-posting is greatly inferior for readers, who vastly outnumber the 
one writer. If that line had been put where it belongs, right after what 
it refers to, it would have been clear.

-- 
Terry Jan Reedy



