
============================================================================
Subject: [Python-Dev] cpython: #1874: detect invalid multipart CTE and
	report it as a defect.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] cpython: #1874: detect invalid multipart CTE and
	report it as a defect.
----------------------------------------
Author: R. David Murra
Attributes: []Content: 
On Wed, 22 Jun 2011 21:40:57 +0200, Georg Brandl <g.brandl at gmx.net> wrote:

See, there are hidden benefits to following the existing coding
conventions of stdlib modules...

(I initially called it InvalidMultipartCTEDefect, but all of the other
names were spelled out, so....)

--
R. David Murray           http://www.bitdance.com

