
============================================================================
Subject: [Python-Dev] avoiding accidental shadowing of top-level
 libraries by the main module
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] avoiding accidental shadowing of top-level
 libraries by the main module
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 12/07/2010 23:05, Nick Coghlan wrote:

An explicit error being raised instead would be just as good.

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] avoiding accidental shadowing of top-level
 libraries by the main module
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 12/07/2010 22:59, Antoine Pitrou wrote:

Allowing a module-executed-as-script to be in sys.modules twice with 
different names and classes is still a recipe for problems. For example 
see this blog entry from Phil Hassey, an experienced Python developer, 
caused by this issue:

http://www.philhassey.com/blog/2009/07/16/oddball-python-252-import-issue/

For scripts that need to be in sys.modules with their "real" name (e.g. 
modules executed with python -m) the following trivial workaround is 
possible:

if __name__ == '__main__':
sys.modules[real_name] = sys.modules['__main__']

That avoids the problem of error messages like:

TypeError: unbound method foo() must be called with A instance as first 
argument (got A instance instead)

(Two different classes with the same name created - one from __main__ 
and one from real_name.) Use cases for *genuinely* reimporting the same 
module with different names (as different module objects rather than 
aliases) are relatively rare, and the problem of modules *accidentally* 
reimporting themselves not that rare.

All the best,

Michael






-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] avoiding accidental shadowing of top-level
 libraries by the main module
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 12/07/2010 23:23, Nick Coghlan wrote:
That's what I was suggesting.


That would be a nice thing to have for earlier versions of Python. 
Personally I think the confusion the problem it causes would be nicer fixed.

All the best,

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] avoiding accidental shadowing of top-level
 libraries by the main module
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 13/07/2010 14:00, Nick Coghlan wrote:

Sure - there are trivial workarounds which is why I don't think there 
are *many* genuine use cases for a module reimporting itself with a 
different name.

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.



