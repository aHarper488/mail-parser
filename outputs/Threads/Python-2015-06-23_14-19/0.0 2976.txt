
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: Issue #14080: fix
 sporadic test_imp failure. Patch by Stefan Krah.
Post Count: 1
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: Issue #14080: fix
 sporadic test_imp failure. Patch by Stefan Krah.
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Sun, Feb 26, 2012 at 12:13, antoine.pitrou <python-checkins at python.org>wrote:



Should that just go into support.create_empty_file()? Since it's just a
performance issue I don't see it causing unexpected test failures and it
might help with any future issues.
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20120226/fb9ba841/attachment.html>

