
============================================================================
Subject: [Python-Dev] Decimal &lt;-&gt; float comparisons in py3k.
Post Count: 4
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Decimal &lt;-&gt; float comparisons in py3k.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Glenn Linderman <v+python <at> g.nevcal.com> writes:

What is a "proper comparison"?
A comparison is a lossy operation by construction (it returns only 1 bit of
output, or 2 bits for rich comparison), so the loss in precision that results
from mixing float and decimal operands should not be a concern here.

Unless there are situations where the comparison algorithm might return wrong
results (are there?), I don't see why we should forbid such comparisons.


Regressing to an useless behaviour sounds like a deliberate annoyance. I don't
think this proposal should be implemented.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] Decimal &lt;-&gt; float comparisons in py3k.
----------------------------------------
Author: Michael Foor
Attributes: []Content: 
On 18/03/2010 18:44, Antoine Pitrou wrote:

I agree, comparisons here have completely defined semantics - it sounds 
crazy not to allow it. (The argument 'because some programmers might do 
it without realising' doesn't hold much water with me.)

Michael



-- 
http://www.ironpythoninaction.com/
http://www.voidspace.org.uk/blog

READ CAREFULLY. By accepting and reading this email you agree, on behalf of your employer, to release me from all obligations and waivers arising from any and all NON-NEGOTIATED agreements, licenses, terms-of-service, shrinkwrap, clickwrap, browsewrap, confidentiality, non-disclosure, non-compete and acceptable use policies (?BOGUS AGREEMENTS?) that I have entered into with your employer, its partners, licensors, agents and assigns, in perpetuity, without prejudice to my ongoing rights and privileges. You further represent that you have the authority to release me from any BOGUS AGREEMENTS on behalf of your employer.





----------------------------------------
Subject:
[Python-Dev] Decimal &lt;-&gt; float comparisons in py3k.
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Glenn Linderman <v+python <at> g.nevcal.com> writes:

Please stick to the topic. We are talking about Python's default behaviour here.

Antoine.





----------------------------------------
Subject:
[Python-Dev] Decimal &lt;-&gt; float comparisons in py3k.
----------------------------------------
Author: Glenn Linderma
Attributes: []Content: 
On 3/18/2010 6:18 PM, Antoine Pitrou wrote:

Yes, I consider my comment relevant, and think that you should apologize 
for claiming it is off topic.

There are two choices on the table -- doing comparisons implicitly 
between Decimal and float, and raising an exception.  It seems the 
current behavior, sorting by type, is universally disliked, but doing 
nothing is a third choice.

So if the default behavior is to raise an exception, my comment pointed 
out the way comparisons could be provided, for those that need them.  
This allows both behaviors to exist concurrently.  Python developers 
could even consider including such a library in the standard library, 
although my suggestion didn't include that.

On the other hand, if the default behavior is to do an implicit 
conversion, I don't know of any way that that could be turned into an 
exception for those coders that don't want or don't like the particular 
type of implicit conversion chosen.

Glenn

