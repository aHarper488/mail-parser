
============================================================================
Subject: [Python-Dev] PEP 3147, __pycache__ directories and umask
Post Count: 23
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
I have a pretty good start on PEP 3147 implementation [1], but I've
encountered a situation that I'd like to get some feedback on.  Here's the
test case illustrating the problem.  From test_import.py:

    def test_writable_directory(self):
        # The umask is not conducive to creating a writable __pycache__
        # directory.
        with umask(0o222):
            __import__(TESTFN)
        self.assertTrue(os.path.exists('__pycache__'))
        self.assertTrue(os.path.exists(os.path.join(
            '__pycache__', '{}.{}.pyc'.format(TESTFN, self.tag))))

The __pycache__ directory does not exist before the import, and the import
machinery creates the directory, but the umask leaves the directory unwritable
by anybody.  So of course when the import machinery goes to write the .pyc
file inside __pycache__, it fails.  This does not cause an ImportError though,
just like if today the package directory were unwritable.

This might be different than today's situation though because once the
unwritable __pycache__ directory is created, nothing is going to change that
without explicit user interaction, and that might be difficult after the
fact.

I'm not sure what the right answer is.  Some possible choices:

* Tough luck
* Force the umask so that the directory is writable, but then the question is,
  by whom?  ugo+w or something less?
* Copy the permissions from the parent directory and ignore umask
* Raise an exception or refuse to create __pycache__ if it's not writable
  (again, by whom?)

Or maybe you have a better idea?  What's the equivalent situation on Windows
and how would things work there?

-Barry

[1] https://edge.launchpad.net/~barry/python/pep3147

P.S. I'm down to only 8 unit test failures.

-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100322/80ada87a/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

On Windows, this problem is easy: create the directory with no
specification of an ACL, and it will (usually) inherit the ACL of the
parent directory. IOW, the same users will have write access to
__pycache__ as to the parent directory.

It is possible to defeat this machinery, by setting ACL entries on the
parent that explicitly don't get inherited, or that only apply to
subdirectories. In this case, I would say "tough luck": this is what the
users requested, so it may have a reason.

Regards,
Martin



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: =?ISO-8859-1?Q?=22Martin_v=2E_L=F6wis=22?
Attributes: []Content: 

I don't think an ImportWarning should be raised: AFAICT, we are not
raising one, either, when the pyc file cannot be created (and it may
well be the case that you don't have write permission at all, which
would trigger lots of ImportWarnings).

Regards,
Martin




----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 22, 2010, at 02:30 PM, Antoine Pitrou wrote:


Yay.


I actually think "tough luck" might be the right answer and that it's not
really all that serious.  Certainly not serious enough not to enable this by
default.

When Python is being installed, either by a from-source 'make install' or by
the distro packager, then you'd expect the umask not to be insane.  In the
latter case, it's a bug and in the former case you screwed up so you should be
able to delete and reinstall, or at the very least execute the same `find`
command that Python's own Makefile calls (in my branch) for 'make clean'.

When you're installing packages, again, I would expect that the system
installer, or you via `easy_install` or whatever, would not have an insane
umask.  As with above, if you *did* have a bad umask, you could certainly also
have a bad umask whenever you ran the third party tool as when you ran
Python.

You could also have a bad umask when you're doing local development, but that
should be easy enough to fix, and besides, I think you'd have other problems
if that were the case.

One possibility would be to instrument compileall to remove __pycache__
directories with a flag.


I don't particularly like this much, because most people won't get the benefit
of it in a local dev environment.  While not the primary use case, it's a
useful enough side benefit, and unless Guido changes his mind, we want to
enable this by default.


It's not really that complicated. :)

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100322/739eff36/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Brett Canno
Attributes: []Content: 
On Mon, Mar 22, 2010 at 06:56, Barry Warsaw <barry at python.org> wrote:

I say tough luck. At the moment if you can't write a .pyc it is a
silent failure; don't see the difference as significant enough to
warrant changing the practice.

-Brett




----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Antoine Pitrou wrote:


Doesn't the existing .pyc mechanism have the same problem? Seems
to me it's just as insecure to allow the Apache user to create
.pyc files, since an attacker could overwrite them with arbitrary
bytecode.

The only safe way is to pre-compile under a different user and
make everything read-only to Apache. The same thing would apply
under the __pycache__ regime.


What about development, or if a user installs by dragging into
site-packages instead of using an installer? I don't like the
idea of being required to use an installation tool in order to
get .pyc files.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 22Mar2010 09:56, Barry Warsaw <barry at python.org> wrote:
| I have a pretty good start on PEP 3147 implementation [1], but I've
| encountered a situation that I'd like to get some feedback on.  Here's the
| test case illustrating the problem.  From test_import.py:
| 
|     def test_writable_directory(self):
|         # The umask is not conducive to creating a writable __pycache__
|         # directory.
|         with umask(0o222):
[...]
| The __pycache__ directory does not exist before the import, and the import
| machinery creates the directory, but the umask leaves the directory unwritable
| by anybody.  So of course when the import machinery goes to write the .pyc
| file inside __pycache__, it fails.  This does not cause an ImportError though,
| just like if today the package directory were unwritable.
| 
| This might be different than today's situation though because once the
| unwritable __pycache__ directory is created, nothing is going to change that
| without explicit user interaction, and that might be difficult after the
| fact.

Like any bad/suboptimal permission.

| I'm not sure what the right answer is.  Some possible choices:
| 
| * Tough luck

+1
I'd go with this one myself.

| * Force the umask so that the directory is writable, but then the question is,
|   by whom?  ugo+w or something less?

-2
Racy and dangerous. The umask is a UNIX process global, and other threads may
get bad results if they're active during this window.

| * Copy the permissions from the parent directory and ignore umask

-1
Maybe. But consider that you may not be the owner of the parent: then
the new child will have different ownership than the parent but the same
permission mask. Potentially a bad mix. This approach is very hard to
get right.

| * Raise an exception or refuse to create __pycache__ if it's not writable
|   (again, by whom?)

-3
Bleah. My python program won't run because an obscure (to the user)
directory had unusual permissions?

Tough ove, it's the only way:-)
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

When asked what would I most want to try before doing it, I said Death.
        - Michael Burton, michaelb at compnews.co.uk



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Cameron Simpso
Attributes: []Content: 
On 23Mar2010 11:40, I wrote:
| | * Raise an exception or refuse to create __pycache__ if it's not writable
| |   (again, by whom?)
| 
| -3
| Bleah. My python program won't run because an obscure (to the user)
| directory had unusual permissions?

Clarification: I'm -3 on the exception. Silent failure to make the
__pycache__ would do, and may be better than silently making a useless
(unwritable) one.  

How about:

  made_it = False
  ok = False
  try:
    if not isdir(__pycache__dir):
      mkdir(__pycache__dir)
      made_it = True
    write pyc content ...
    ok = True
  except OSError, IOerror etc:
    if not ok:
      os.remove pyc content file
      if made_it:
        rmdir(__pycache__dir) but be quiet if this fails,
          eg if it is not empty because another process or thread
             added stuff

So silent tidyup attempt on failure, but no escaping exception.

Cheers,
-- 
Cameron Simpson <cs at zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

The govt MUST regulate the Net NOW! We can't have average people saying
what's on their minds!  - ezwriter at netcom.com



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Antoine Pitrou <solipsis at pitrou.net> writes:


+1.

Taking advantage of caching directories that have already been set up
correctly in advance at install time is friendly. Littering the runtime
directory with new subdirectories by default is not so friendly.

Perhaps also of note is that the FHS recommends systems use
?/var/cache/foo/? for cached data from applications:

    /var/cache : Application cache data

    Purpose

    /var/cache is intended for cached data from applications. Such data is
    locally generated as a result of time-consuming I/O or calculation. The
    application must be able to regenerate or restore the data. Unlike
    /var/spool, the cached files can be deleted without data loss. The data
    must remain valid between invocations of the application and rebooting
    the system.

    <URL:http://www.debian.org/doc/packaging-manuals/fhs/fhs-2.3.html#VARCACHEAPPLICATIONCACHEDATA>

This would suggest that Python could start using ?/var/cache/python/?
for its cached bytecode tree on systems that implement the FHS.

-- 
 \         ?Simplicity and elegance are unpopular because they require |
  `\           hard work and discipline to achieve and education to be |
_o__)                                appreciated.? ?Edsger W. Dijkstra |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Matthias Klos
Attributes: []Content: 
On 23.03.2010 02:28, Ben Finney wrote:

it reads *data*, not code.



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Matthias Klose <doko at ubuntu.com> writes:


So what? There's no implication that data-which-happens-to-be-code is
unsuitable for storage in ?/var/cache/foo/?. Easily-regenerated Python
byte code for caching meets the description quite well, AFAICT.

-- 
 \      ?It seems intuitively obvious to me, which means that it might |
  `\                                           be wrong.? ?Chris Torek |
_o__)                                                                  |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Antoine Pitrou wrote:

Yes. We do it all the time with unpackaged only-for-internal-use Python
code.

I wouldn't expect it to work with random packages downloaded from the
'net, but it works fine for our own stuff (since it expects to be used
this way).

Cheers,
Nick.

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Steven D'Apran
Attributes: []Content: 
On Wed, 24 Mar 2010 12:35:43 am Ben Finney wrote:


While I strongly approve of the concept of a central cache directory for 
many things, I don't think that .pyc files fit the bill.

Since there is no privileged python user that runs all Python code, and 
since any unprivileged user needs to be able to write .pyc files, the 
cache directory needs to be world-writable. But since the data being 
stored is code, that opens up a fairly nasty security hole: user fred 
could overwrite the cached .pyc files used by user barney and cause 
barney to run any arbitrary code fred likes.

The alternative would be to have individual caches for every user. Apart 
from being wasteful of disk space ("but who cares, bigger disks are 
cheap") that just complicates everything. You would need:

/var/cache/python/<user>/

which would essentially make it impossible to ship pre-compiled .pyc 
files, since the packaging system couldn't predict what usernames (note 
plural) to store them under.

It is not true that one can necessarily delete the cached files without 
data loss. Python still supports .pyc-only packages, and AFAIK there 
are no plans to stop that, so deleting the .pyc file may delete the 
module.


-- 
Steven D'Aprano



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Russell E. Owe
Attributes: []Content: 
In article <4BA80418.6030905 at canterbury.ac.nz>,
 Greg Ewing <greg.ewing at canterbury.ac.nz> wrote:


This does sound like a bit security hole both in existing Python and the 
new __pycache__ proposed mechanism. It seems like this is the time to 
address it, while changing the caching mechanism.

If .pyc files are to be shared, it seems essential to (by default) 
generate them at install time and make them read-only for unprivileged 
users.

This in turn implies that we may have to give up some support for 
dragging python modules into site-packages, e.g. not generate .pyc files 
for such modules. At least if we go that route it will mostly affect 
power users, who can presumably cope.

-- Russell




----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Antoine Pitrou wrote:


Maybe it's one point, but I'm not sure it's the *main* one.
Personally I would benefit most from it during development.
I hardly ever look in the directories of installed packages,
so I don't care what they look like.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Russell E. Owen wrote:


No, I don't think so. Currently, when you install a package (by
whatever means) in a directory that's not writable by the people
who will be running it, there are two possibilities:

1) Precompiled .pyc files are generated at installation time,
which are then read-only to users.

2) No .pyc files are installed, in which case none can or will be
created by users either, since they don't have write permission
to the directory.

None of this would change if __pycache__ directories were used.
The only difference would be that there would be an additional
mode for failure to create .pyc files, i.e. __pycache__ could
be created but nothing could be written to it because of a
umask issue.

If you install in a shared site-packages by dragging, you
already have to be careful about setting the permissions. You'd
just have to be sure to extended that diligence to any
contained __pycache__ directories.

-- 
Greg



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 22, 2010, at 08:33 PM, Antoine Pitrou wrote:


I don't think they're mutually exclusive.  We will definitely give users the
tool to do compilation at install time via compileall.  That needn't preclude
on-demand creation, which will generally Just Work.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100323/002307ce/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 24, 2010, at 12:35 AM, Ben Finney wrote:


pyc files don't go there now, so why would PEP 3147 change that?

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100323/4fcc82d0/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Mar 23, 2010, at 12:49 PM, Russell E. Owen wrote:


I think in practice this is what's almost always going to happen for system
Python source, either via your distribution's installer or distutils.  I think
most on-demand creation of pyc files will be when you're developing your own
code.  I don't want to have to run a separate step to get the benefit of
__pycache__, but I admit it's outside the scope of the PEP's original
intention.  I leave it to the BDFL.

-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100323/fc7697e0/attachment.pgp>



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Ben Finne
Attributes: []Content: 
Antoine Pitrou <solipsis at pitrou.net> writes:

Steven D'Aprano <steve at pearwood.info> writes:


Hold up; my understanding is that, as Antoine Pitrou says:


So, the packaging system will, by definition, have access to write to
FHS directories and those directories don't need to be world-writable.

-- 
 \         ?Pinky, are you pondering what I'm pondering?? ?I think so, |
  `\         Brain, but how will we get a pair of Abe Vigoda's pants?? |
_o__)                                           ?_Pinky and The Brain_ |
Ben Finney




----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
Le mardi 23 mars 2010 ? 20:50 -0400, Isaac Morland a ?crit :

I don't really get it. I have never had any problem to see "what is
actually here" in a Python source directory, despite the presence of pyc
files.


Well, I was assuming that everyone had been using svn:ignore,
or .hgignore, for years.
Similarly, you will configure svn, hg, or any other system, to ignore
__pycache__ directories (or .pyc files) so that they don't appear in
"svn status".


It's still mostly cosmetic and I don't think it's as serious as any
positive or negative system administration effects the change may have.





----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
Russell E. Owen wrote:

And when someone drags a Python module into the per-user site-packages
instead? [1]

Yes, a shared Python needs to be managed carefully. Systems with a
shared Python should also generally have a vaguely competent sysadmin
running them.

An unshared Python and associated packages under PEP 3147 should work
just as well as they do under the existing pyc scheme (only without the
source directory clutter).

Cheers,
Nick.

[1] http://www.python.org/dev/peps/pep-0370/

-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia
---------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] PEP 3147, __pycache__ directories and umask
----------------------------------------
Author: Greg Ewin
Attributes: []Content: 
Isaac Morland wrote:

Yes. When using MacOSX I do most of my work using the
Finder's column view. With two windows open one above the
other, there's room for about a dozen files to be seen
at once. There's no way to filter the view or sort by
anything other than name, so with the current .pyc
scheme, half of that valuable screen space is wasted.

-- 
Greg

