
============================================================================
Subject: [Python-Dev] question/comment about documentation of relative
	imports
Post Count: 10
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Darren Dal
Attributes: []Content: 
I have a couple questions/comments about the use of PEP 328-style
relative imports. For example, the faq at
http://docs.python.org/py3k/faq/programming.html#what-are-the-best-practices-for-using-import-in-a-module
reads:

"Never use relative package imports. If you?re writing code that?s in
the package.sub.m1 module and want to import package.sub.m2, do not
just write from . import m2, even though it?s legal. Write from
package.sub import m2 instead. See PEP 328 for details."

There is no explanation to support the claim that relative imports
should "never" be used. It seems to me that someone read the following
in PEP 328::

   from .moduleY import spam
   from .moduleY import spam as ham
   from . import moduleY
   from ..subpackage1 import moduleY
   from ..subpackage2.moduleZ import eggs
   from ..moduleA import foo
   from ...package import bar
   from ...sys import path

   Note that while that last case is legal, it is certainly
discouraged ("insane" was the word Guido used).

... and interpreted it to mean that relative imports are in general
discouraged. I interpreted it to mean that relative imports should not
be used to import from python's standard library.

There are cases where it is necessary to use relative imports, like a
package that is included as a subpackage of more than one other
project (when it is not acceptable to add an external dependency, for
example due to version/compatibility issues). There is some additional
context on relative imports in the programming faq for python-2.7 at
http://docs.python.org/faq/programming.html#what-are-the-best-practices-for-using-import-in-a-module
:

"Never use relative package imports. If you?re writing code that?s in
the package.sub.m1 module and want to import package.sub.m2, do not
just write import m2, even though it?s legal. Write from package.sub
import m2 instead. Relative imports can lead to a module being
initialized twice, leading to confusing bugs. See PEP 328 for
details."

Is there some documentation explaining why the module may be
initialized twice? I don't see it in PEP 328. Is this also the case
for python-3, or does it only apply to the old-style (pre-PEP 328)
relative imports in python-2? If relative imports are truly so
strongly discouraged, then perhaps warnings should also be included in
places like http://docs.python.org/library/functions.html#__import__ ,
and especially http://docs.python.org/tutorial/modules.html#intra-package-references
and http://www.python.org/dev/peps/pep-0328/ (which, if I have
misinterpreted, is ambiguously written. Though I doubt this is the
case).

There is also this warning against relative imports in PEP 8:

    - Relative imports for intra-package imports are highly discouraged.
      Always use the absolute package path for all imports.
      Even now that PEP 328 [7] is fully implemented in Python 2.5,
      its style of explicit relative imports is actively discouraged;
      absolute imports are more portable and usually more readable.

... but one could argue, as I just have, that relative imports are
more portable, not less. In a sense, the statement "explicit relative
imports is actively discouraged" is objectively false. They are
passively discouraged. If they were actively discouraged, perhaps
performing a relative import would raise a warning, or maybe distutils
would raise a warning at install time, or maybe an additional import
would be required to enable them. Up until now, I was not aware that
use of PEP 328 relative imports might be discouraged. I'm still
unclear as to why they might be discouraged. I recently helped convert
a popular package to use PEP 328 relative imports. Would the python
devs consider this a mistake?


Thanks,
Darren



----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Simon Cros
Attributes: []Content: 
On Tue, Oct 5, 2010 at 4:56 PM, Darren Dale <dsdale24 at gmail.com> wrote:

Only if by "legal" you mean "happened to work". It stops "happening to
work" in Python 2.6.6. :)

Generally I'm +0 on relative imports as a whole.

Schiavo
Simon



----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 05 Oct 2010 17:18:18 +0100
Michael Foord <fuzzyman at voidspace.org.uk> wrote:

You can put several packages in a single distribution.

Regards

Antoine.





----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Darren Dal
Attributes: []Content: 
On Tue, Oct 5, 2010 at 12:43 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

Thats not the point though. Due to compatibility issues, maybe I don't
want to expose the code at the top level. Maybe the foo package is
distributed elsewhere as a top-level package, but I need to use an
older version due to compatibility problems. I certainly don't want to
risk overwriting a pre-existing installation of foo with my required
version of foo. This is not a hypothetical, we once had exactly this
problem when we distributed an old version of enthought.traits with
matplotlib (even though we checked for pre-existing installations,
crufty build/ directories containing the out-of-date traits package
were overwriting existing installations).

Darren



----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Darren Dal
Attributes: []Content: 
On Tue, Oct 5, 2010 at 1:45 PM, Antoine Pitrou <solipsis at pitrou.net> wrote:

I'm not talking about requiring other projects to follow my coding style.


The issue is implementing a PEP with nice support for relative
imports, and then documenting that it should never be used.

Darren



----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Antoine Pitro
Attributes: []Content: 
On Tue, 5 Oct 2010 14:17:47 -0400
Darren Dale <dsdale24 at gmail.com> wrote:

Then can you explain your use case a bit better? It is not
obvious at all what kind of "solution" relative imports allow, and why
not using them is a heavy burden.


While I haven't written the FAQ, I certainly sympathize with the idea
that relative imports are much less readable than absolute imports
(especially when they climb upwards in the directory hierarchy, i.e.
"from ..foo import bar").

Regards

Antoine.



----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Guido van Rossu
Attributes: []Content: 
On Tue, Oct 5, 2010 at 11:17 AM, Darren Dale <dsdale24 at gmail.com> wrote:

Isn't this mostly historical? Until the new relative-import syntax was
implemented there were various problems with relative imports. The
short-term solution was to recommend not using them. The long-term
solution was to implement an unambiguous syntax. Now it is time to
withdraw the anti-recommendation. Of course, without going overboard
-- I still find them an acquired taste; but they have their place.

-- 
--Guido van Rossum (python.org/~guido)



----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 10/5/2010 2:21 PM, Guido van Rossum wrote:

Darren, if you have not yet done so, open a tracker that quotes the 
above and gives your recommended changes at which locations.


-- 
Terry Jan Reedy




----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Darren Dal
Attributes: []Content: 
On Tue, Oct 5, 2010 at 3:37 PM, Terry Reedy <tjreedy at udel.edu> wrote:

Done: http://bugs.python.org/issue10031

Darren



----------------------------------------
Subject:
[Python-Dev] question/comment about documentation of relative
	imports
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Wed, Oct 6, 2010 at 4:21 AM, Guido van Rossum <guido at python.org> wrote:

Indeed, the objections raised in those FAQ entries mostly applied to
the problems implicit relative imports could silently cause.  Explicit
relative imports will throw exceptions for those cases instead. They
read like someone took the old text and just modified it to refer to
explicit imports instead of implicit ones without softening the
language at all.

The remaining scenarios we have that can lead to duplication of a
module happen regardless of the import style you use*.

Cheers,
Nick.

*For the curious - those scenarios relate to ending up with the same
module present both as "__main__" and under its normal module name.

-- 
Nick Coghlan?? |?? ncoghlan at gmail.com?? |?? Brisbane, Australia

