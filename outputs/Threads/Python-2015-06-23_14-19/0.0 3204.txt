
============================================================================
Subject: [Python-Dev] Static type analysis
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] Static type analysis
----------------------------------------
Author: Edward K. Rea
Attributes: []Content: 
Hello all,

I'm wondering whether this is the appropriate place to discuss
(global) static type analysis, a topic Guido mentioned around the 28
min mark in his PyCon 2012 keynote,
http://pyvideo.org/video/956/keynote-guido-van-rossum

This is a topic that has interested me for a long time, and it has
important implications for Leo.  Just now I have some free time to
devote to it.

Edward
------------------------------------------------------------------------------
Edward K. Ream email: edreamleo at gmail.com
Leo: http://webpages.charter.net/edreamleo/front.html
------------------------------------------------------------------------------



----------------------------------------
Subject:
[Python-Dev] Static type analysis
----------------------------------------
Author: Terry Reed
Attributes: []Content: 
On 6/6/2012 7:24 AM, Edward K. Ream wrote:

I think either python-list or python-ideas list would be more 
appropriate. Start with a proposal, statement, or question that others 
can respond to.


-- 
Terry Jan Reedy


