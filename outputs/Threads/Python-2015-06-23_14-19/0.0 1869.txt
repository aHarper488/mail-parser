
============================================================================
Subject: [Python-Dev] [Python-checkins] cpython: fix doc typo for
	library/test.rst
Post Count: 2
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: fix doc typo for
	library/test.rst
----------------------------------------
Author: Ezio Melott
Attributes: []Content: 
Hi,

On 27/07/2011 20.31, eli.bendersky wrote:

Actually I think this is no longer true.  import_fresh_module raises an 
ImportError if *name* can't be imported, or returns None if the fresh 
module is not found.

Its use case is to enable or block accelerations for modules that 
optionally provide one.  All the modules that currently use 
import_fresh_module are (afaik) always available (json, warnings, 
heapq), so raising SkipTest when the module is missing is not useful now.
It returns None in the case an acceleration is missing, so e.g. in 
"cjson = import_fresh_module('json', fresh=['_json'])" cjson will be 
None and it will be possible to do things like @skipUnless(cjson, 
'requires _json').  Here raising an ImportError will defeat (part of) 
the purpose of the function, i.e. avoiding:
try:
   import _json
except ImportError:
   _json = None

and raising a SkipTest when the accelerations are missing is not an 
option if there are other tests (e.g. the tests for the Python 
implementation).

These changes come from http://hg.python.org/cpython/rev/c1a12a308c5b .  
Before the change import_fresh_module was still returning the module 
(e.g. json) even when the acceleration (fresh=['_json']) was missing, 
and the C tests were run twice using the same pure-python module used 
for the Py ones.

The typo and the wrong doc is also on 2.7.

Best Regards,
Ezio Melotti





----------------------------------------
Subject:
[Python-Dev] [Python-checkins] cpython: fix doc typo for
	library/test.rst
----------------------------------------
Author: Eli Bendersk
Attributes: []Content: 
Actually I think this is no longer true.  import_fresh_module raises an
ImportError if *name* can't be imported, or returns None if the fresh module
is not found.

Ezio, thanks. I opened issue 12645 to track this.

Eli
-------------- next part --------------
An HTML attachment was scrubbed...
URL: <http://mail.python.org/pipermail/python-dev/attachments/20110727/09c84edd/attachment.html>

