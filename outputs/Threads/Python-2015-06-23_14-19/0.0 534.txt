
============================================================================
Subject: [Python-Dev] [RELEASE] Python 2.7 released
Post Count: 5
Norm Emergent Posts: 0
Weight: 0.0


----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7 released
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
On behalf of the Python development team, I'm jocund to announce the second
release candidate of Python 2.7.

Python 2.7 will be the last major version in the 2.x series. However, it will
also have an extended period of bugfix maintenance.

2.7 includes many features that were first released in Python 3.1. The faster io
module, the new nested with statement syntax, improved float repr, set literals,
dictionary views, and the memoryview object have been backported from 3.1. Other
features include an ordered dictionary implementation, unittests improvements, a
new sysconfig module, auto-numbering of fields in the str/unicode format method,
and support for ttk Tile in Tkinter.  For a more extensive list of changes in
2.7, see http://doc.python.org/dev/whatsnew/2.7.html or Misc/NEWS in the Python
distribution.

To download Python 2.7 visit:

     http://www.python.org/download/releases/2.7/

2.7 documentation can be found at:

     http://docs.python.org/2.7/

This is a production release and should be suitable for all libraries and
applications.  Please report any bugs you find, so they can be fixed in the next
maintenance releases.  The bug tracker is at:

     http://bugs.python.org/


Enjoy!

--
Benjamin Peterson
Release Manager
benjamin at python.org
(on behalf of the entire python-dev team and 2.7's contributors)



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7 released
----------------------------------------
Author: Benjamin Peterso
Attributes: []Content: 
2010/7/4 Benjamin Peterson <benjamin at python.org>:

Arg!!! This should, of course, be "final release".



-- 
Regards,
Benjamin



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7 released
----------------------------------------
Author: Larry Hasting
Attributes: []Content: 
On 07/04/2010 08:34 AM, Benjamin Peterson wrote:

s/second release candidate/release/

*standing ovation*


//larry//



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7 released
----------------------------------------
Author: Nick Coghla
Attributes: []Content: 
On Mon, Jul 5, 2010 at 2:03 AM, Benjamin Peterson <benjamin at python.org> wrote:

It wouldn't be a proper Python release without a typo *somewhere* in
the release announcement :)

Well shepherded!

Cheers,
Nick.


-- 
Nick Coghlan   |   ncoghlan at gmail.com   |   Brisbane, Australia



----------------------------------------
Subject:
[Python-Dev] [RELEASE] Python 2.7 released
----------------------------------------
Author: Barry Warsa
Attributes: []Content: 
On Jul 04, 2010, at 11:03 AM, Benjamin Peterson wrote:


Congratulations Benjamin!
-Barry
-------------- next part --------------
A non-text attachment was scrubbed...
Name: signature.asc
Type: application/pgp-signature
Size: 836 bytes
Desc: not available
URL: <http://mail.python.org/pipermail/python-dev/attachments/20100706/7f27569d/attachment.pgp>

