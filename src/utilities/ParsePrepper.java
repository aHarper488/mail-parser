package utilities;

import java.util.ArrayList;
import java.util.List;

public class ParsePrepper {
	
	int maxLength;
	
	public ParsePrepper(){
		
		maxLength = 10000;
		
	}
	
	/**
	 * replaces raw xml character changes to their human readable correspondents
	 * @param input
	 * @return
	 */
	public static String reformatXML(String input){
		
		input = input.replace("&lt;", "<");
		input = input.replace("&gt;", ">");
		input = input.replace("&quot;", "\"");
		input = input.replace("&apos;", "'");

		return input;
	}
	
	public String purgeString(String str){
		
		str = str.replace("\n", "").replace("\r", "\\n");
		str = str.replace("'", "\\n'");
		
		return str;
	}
	
	public static String purgeNewLines(String str){
		return str.replace("\n", "").replace("\r", " ");
	}
	
	public String prepMailingListString(String strInput){
		
		
		if(strInput.charAt(0) == '|')
			return null;
		
		if(strInput.contains("-apmail-ant-dev-archive=ant.apache.org@ant.apache.org"))
			return null;
		
		
		strInput.replaceAll("\n", ".");
		strInput.replaceAll("\n", ".");
		
		return strInput;
	}
	
	public String prepXMLString(String strInput){
	
		if(strInput.length() >  maxLength)
			return null;
		
		strInput.replaceAll("\n", ".");
		strInput.replaceAll("\n", ".");
		
		return strInput;
	}

	public ArrayList<String> XMLExtract(String strInput, String meta){
		
		ArrayList<String> outputStrings = new ArrayList<String>();

		int metaIndexStart = 0;
		int metaIndexEnd = 0;
		
		String metaTagStart = meta;//"<bug>";
		String MetaTagEnd = meta.substring(0, 1) + "/" + meta.substring(1, meta.length());//"</bug>";
		
		
		for(;;){
			
			StringBuilder sb = new StringBuilder();	
			
			//get next bug index
			metaIndexStart = strInput.indexOf(metaTagStart, metaIndexEnd);
			
			//if none found break, else continue
			if(metaIndexStart == -1)
				break;
			
			//gets bugs end index
			metaIndexEnd = strInput.indexOf(MetaTagEnd, metaIndexStart) + 6;			
			
			String addString = strInput.substring(metaIndexStart, metaIndexEnd);		
			if(addString != null)
				sb.append(addString).append('\n');
			
			//System.out.println("endBug: " + metaIndexStart + " " + metaIndexEnd);
			metaIndexStart = metaIndexEnd + MetaTagEnd.length();
			//System.out.println("endBug: " + metaIndexStart + " " + metaIndexEnd);
			//System.out.println(sb.toString());
			
			if(sb.length() > 1)
				outputStrings.add(sb.toString());
		}
		
		return outputStrings;
	}
	
	public ArrayList<String> subDivXML(String strInput, String meta, String sub){
		
		ArrayList<String> outputStrings = new ArrayList<String>();

		int metaIndexStart = 0;
		int metaIndexEnd = 0;
		int substartIndex = 0;
		int subendIndex = 0;
		
		String metaTagStart = meta;//"<bug>";
		String MetaTagEnd = meta.substring(0, 1) + "/" + meta.substring(1, meta.length());//"</bug>";
		
		String subTagStart = sub;//"<thetext>";
		String subTagEnd = sub.substring(0, 1) + "/" + sub.substring(1, sub.length());//"</thetext>";
		
		for(;;){
			
			StringBuilder sb = new StringBuilder();	
			//get next bug index
			metaIndexStart = strInput.indexOf(metaTagStart, metaIndexEnd);
			
			//if none found break, else continue
			if(metaIndexStart == -1)
				break;
			
			
			//gets bugs end index
			metaIndexEnd = strInput.indexOf(MetaTagEnd, metaIndexStart);
			
			substartIndex = metaIndexStart;
			
			//find each subtag in bug till no more is found
			for(;;){
				
				substartIndex = strInput.indexOf(subTagStart, substartIndex);
				
				//if none found break, else continue
				if(substartIndex == -1 || substartIndex > metaIndexEnd)
					break;
				
				substartIndex += subTagStart.length();
				
				subendIndex = strInput.indexOf(subTagEnd, substartIndex);
				
				//System.out.println("Start: " + substartIndex + " End: " + subendIndex);
				String addString = strInput.substring(substartIndex, subendIndex);
				//System.out.println("String: " + addString);
				
				addString = prepXMLString(addString);
				
				if(addString != null)
					sb.append(addString).append('\n');
				
				substartIndex = subendIndex + subTagEnd.length();
			}
			
			//System.out.println("endBug: " + metaIndexStart + " " + metaIndexEnd);
			metaIndexStart = metaIndexEnd + MetaTagEnd.length();
			//System.out.println("endBug: " + metaIndexStart + " " + metaIndexEnd);
			
			if(sb.length() > 1)
				outputStrings.add(sb.toString());
		}
		
		
		//System.out.println("Begin: ---------------------------------------------------------------");
		//for(String s: outputStrings){
			//System.out.println(s);
			//System.out.println("------------------------------------------------------------------");
		//}
		
		return outputStrings;
	}

	private String extractTag(String tag, int metaIndexStart, String strInput){
		
		int startIndex;
		int endIndex;
		String endTag;
		
		endTag = tag.substring(0, 1) + "/" + tag.substring(1, tag.length());
		startIndex = strInput.indexOf(tag, metaIndexStart);
		startIndex += tag.length();
		endIndex = strInput.indexOf(endTag, metaIndexStart);
		
		return strInput.substring(startIndex, endIndex);
		
	}
}
