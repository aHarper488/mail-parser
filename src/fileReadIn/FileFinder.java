/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileReadIn;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
/**
 * Source: http://www.heimetli.ch/directory/RecursiveTraversal.html
 * this class recurses through the file system looking for java files
 * @author Phillip Smith
 */
public class FileFinder {
	
	List<String> fileContents = new ArrayList<String>();
  
   /** 
    * this method calls the recursiveTravel Function
    * @param path 
    */
    public void traverse(String path, String type) {
        File dir = new File(path);
        recursiveTraversal(dir, type);
    }
    /** 
    * this method calls the recursiveTravel Function
    * @param path 
    */
    public void traverse(File path, String type) {
        recursiveTraversal(path, type);
    }
    
    /**
     * this function recurses through the file system from the given starting
     * point looking for java files 
     * @param dir 
     */
    private void recursiveTraversal(File dir, String type) {
    	
      if( dir.isDirectory()){
    	  
         String entries[] = dir.list() ;

         if( entries != null ){ 	 
            for( String entry : entries ){
               traverse( new File(dir,entry), type ) ;
            }
         }
      } else {
            String Dir = dir.toString();
            String Cname = dir.getName(); 
            if (Dir.endsWith("." + type)) {
            	System.out.println(Dir);
	    		try {
					fileContents.add(readFile(Dir, Charset.defaultCharset()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
      }
    }
    
    @SuppressWarnings("unchecked")
	public ArrayList<String> getFileContents(){
		
		return (ArrayList<String>) fileContents;
	}
    
    private String readFile(String path, Charset encoding) throws IOException 
	{
		  byte[] encoded = Files.readAllBytes(Paths.get(path));
		  return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}
}
