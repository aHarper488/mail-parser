package fileReadIn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConfigReader {

	String FileName;
	ArrayList<String> verblist = new ArrayList<String>();
	
	
	
	public ConfigReader(String strFileName){
		
		FileName = strFileName;
		
	}
	
	public void read(){
		
		BufferedReader br;
		try {
			
			br = new BufferedReader(new FileReader(FileName));

	        String line = br.readLine();

	        while (line != null) {
	        	
	        	verblist.add(line);
	            line = br.readLine();
	        }
	        
	        br.close();

	    } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getList(){
		
		return verblist;
		
	}
	
	public static ArrayList<String> read(String strFileName){
		
		ArrayList<String> lines = new ArrayList<String>();
		BufferedReader br;
		try {
			
			br = new BufferedReader(new FileReader(strFileName));

	        String line = br.readLine();

	        while (line != null) {
	        	
	        	lines.add(line);
	            line = br.readLine();
	        }
	        
	        br.close();

	    } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lines;
	}
	
	public static Map<String, String> readMap(String strFileName){
		
		Map<String, String> map = new HashMap<String, String>();
		BufferedReader br;
		try {
			
			br = new BufferedReader(new FileReader(strFileName));

	        String line = br.readLine();

	        while (line != null) {
	        	map.put(line.substring(0, line.indexOf(";")), line.substring(line.indexOf(";") + 1));
	            line = br.readLine();
	        }
	        
	        br.close();

	    } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
}
