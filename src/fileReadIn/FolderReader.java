package fileReadIn;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FolderReader {

	String directory;
	ArrayList<String> fileContents;
	
	
	public FolderReader(String dir){
		
		directory = dir;
		fileContents = new ArrayList<String>();
	}
	
	public void readFolder(){
		
		File[] files = new File(directory).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		    	try {
		    		System.out.println(file.getName());
		    		fileContents.add(readFile(directory + file.getName(), Charset.defaultCharset()));

		    	} catch (IOException e) {
		    		e.printStackTrace();
		    	}
		    }
		}
	}
	
	public void readFolder(String str){
		System.out.println("here");
		File[] files = new File(directory).listFiles();

		for (File file : files) {
			System.out.println(file.getName());
		    if (file.isFile() && file.getName().contains(str)) {
		    	try {
		    		System.out.println(file.getName());
		    		fileContents.add(readFile(directory + file.getName(), Charset.defaultCharset()));

		    	} catch (IOException e) {
		    		e.printStackTrace();
		    	}
		    }
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> getFileContents(){
		
		return (ArrayList<String>) fileContents.clone();
	}
	
	private String readFile(String path, Charset encoding) throws IOException 
	{
		  byte[] encoded = Files.readAllBytes(Paths.get(path));
		  return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}
}
