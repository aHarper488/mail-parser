package mailingList;

import java.util.ArrayList;
import java.util.List;

public class Thread {

	static int count = 0;
	
	final int id;
	List<Mail> posts;
	int normEvidentPosts; //posts with norm evidence
	double weight; //weight of posts attributes
	
	String subject; 
	
	public Thread(String subject) {
		
		posts = new ArrayList<Mail>();
		weight = 0;
		id = count++;
		this.subject = subject;
		normEvidentPosts = 0;
	}
	
	public void calcWeight(){
		weight = normEvidentPosts;
	}
	
	public double getWeight(){
		return weight;
	}
	
	public void addPost(Mail post){
		
		if(post.isValid())
			normEvidentPosts++;
		posts.add(post);
	}
	
	public List<Mail> getPosts(){
		return posts;
	}
	
	public String toString(){
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("\n============================================================================");
		sb.append("\nSubject: " + subject);
		sb.append("\nPost Count: " + posts.size());
		sb.append("\nNorm Emergent Posts: " + normEvidentPosts);
		sb.append("\nWeight: " + weight + "\n"); 
		
		for(Mail m : posts){
			sb.append("\n" + m.toString());
		}
		return sb.toString();
	}

	public int getId() {
		return id;
	}

	public String getSubject() {
		return subject;
	}
}
