package mailingList;

import java.util.ArrayList;
import java.util.List;

public class Mail {
	
	private String subject;
	private String author;
	private String content;
	private String header;
	
	boolean valid; //does mail have norm evidence
	
	List<String> attributes;
	
	public Mail(String subject, String author, String content, String header) {

		this.subject = subject;
		this.author = author;
		this.content = content;
		this.header = header;
		valid = false;
		attributes = new ArrayList<String>(); 
	}
	
	public String toString(){
		
		String ret;
		
		ret = "\n----------------------------------------\nSubject:\n"
				+ subject + 
				"\n----------------------------------------\n" 
				+ "Author: " + author 
				+ "\nAttributes: " + attributes.toString()
				/*+ header*/ 
				+ "Content: " + content;
		
		return ret;
	}
	
	public void addAttribute(String att){
		
		//XXX
		if(att.equals("Yes") && attributes.size() < 2)
			valid = true;
		
		attributes.add(att);
	}
	public String getAttribute(int c){
		
		return attributes.get(c);
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getheader() {
		return header;
	}
	public void setheader(String header) {
		this.header = header;
	}

	public boolean isValid() {
		return valid;
	}

	public String getTrainedClass() {
		// TODO Auto-generated method stub
		return null;
	}
}
