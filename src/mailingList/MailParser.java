package mailingList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import fileReadIn.FolderReader;

public class MailParser {
	
	FolderReader folderReader;
	MailReadIn mailReader;
	
	//posts
	List<Mail> allPosts;
	Map<String, Thread> threadMap;
	
	String projectTitle;
	
	int totalCount;
	
	public MailParser(String dir){
			
		folderReader = new FolderReader("inputs/mailinglist/" + dir + "/");
		mailReader = new MailReadIn();
		allPosts = new ArrayList<Mail>();
		threadMap = new HashMap<String, Thread>();
		
		projectTitle = dir;
		totalCount = 0;
	}
	
	@SuppressWarnings("unchecked")
	public void Run(){
		
		loadIn();	
		
		for(Mail post : allPosts){
			
			Thread t = threadMap.get(post.getSubject());
			if (t == null) { //not found, new thread
				
				Thread newThread = new Thread(post.getSubject());
				newThread.addPost(post);
				
				threadMap.put(post.getSubject(), newThread);
			}
			else { //else merge
	
				t.addPost(post);
				threadMap.put(post.getSubject(), t);
			}
		}
		
		/*for(Thread t : threadMap.values()){
			t.calcWeight();
		}*/
	}
	
	public void loadIn(){
		
		//load mail
		folderReader.readFolder(); 
		
		//parse and merge posts into one list
		for(String s : folderReader.getFileContents()){			
			allPosts.addAll(mailReader.readMail(s, true));
		}
	}
	
	public Collection<Thread> getThreads(){
		return threadMap.values();
	}
	
	private int estimateExtractedNorms(){
		
		int estCount = 0;
		
		for(Mail m : allPosts){
			
			if(m.isValid() && m.getAttribute(2).equals("Yes"))
				estCount++;
		}
		
		return estCount;
	}
	
	@Deprecated
	private void MergeMap(Map<String, List<Mail>> orig, Map<String, List<Mail>> add){
		
		for(Map.Entry<String, List<Mail>> entry : add.entrySet()){
			
			String key = entry.getKey();
			List<Mail> values = entry.getValue();
			
			List<Mail> l = orig.get(key);
            if (l == null)
            	orig.put(key, l = new ArrayList<Mail>());
            l.addAll(values);	
		}
	}
	
	public String getStats(){
		StringBuilder sb = new StringBuilder();
		
		double normPosts = 0;
		double normthreads = 0;
		
		//count posts with norm evidence
		for(Mail m : allPosts)
			if(m.isValid())
				normPosts++;
		
		//count threads with norm evidence
		for(Thread t : threadMap.values())
			if(t.getWeight() > 0.00)
				normthreads++;
				
		sb.append("\nTotal posts: " + allPosts.size());
		sb.append("\n\nTotal threads: " + threadMap.size());
		
		return sb.toString();
	}

}
