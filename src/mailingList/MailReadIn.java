package mailingList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailReadIn {
	
	//ArrayList<Mail> RetMail = new ArrayList<Mail>();
	@Deprecated
	public Map<String, List<Mail>> parseString(String mail, boolean purgeQuotes){
		
	//	int index = 0;
		int headerStart = 0;
		int start = 0;
		int subStart = 0;
		int end  = 0;
		int subEnd = 0;
		int headerEnd = 0;
	//	boolean foound = false;
		boolean running = true;
		Map<String, List<Mail>> mailMap = new HashMap<String, List<Mail>>();
		
		//each mail
		while(running){
			
			String subject;
			String content;
			String header;
			
			headerStart = end;
			
			//find start
			start = mail.indexOf("Message-ID:", end);
			
			//break if end of string
			if(start == -1)
				break;
			
			start = mail.indexOf("\n", start) + 1;
			
			//start -= 200;
									
			//find end
			end = mail.indexOf("Message-ID:", start);
			if(end == -1)
				end = mail.length();
			else{
				
				end = mail.lastIndexOf("From:", end);
				end = mail.lastIndexOf("From", end - 1);
			}
				
			if(end < start){
				
				end = mail.indexOf("Message-ID:", start);
				continue;
			}
			
			subStart = mail.indexOf("Subject:", headerStart) + 9;
			subEnd = mail.indexOf("In-Reply-To:", subStart) - 1;
			if(subEnd < 0 || subEnd - subStart > 200){
		
				subEnd = mail.indexOf("References:", subStart) - 1;
				if(subEnd < 0 || subEnd - subStart > 200){
					subEnd = mail.indexOf("Message-ID:", subStart) - 1;
				}
			}
			
			headerEnd = start;			
			subject = mail.substring(subStart, subEnd);
			content = mail.substring(start, end);
			header = mail.substring(headerStart, headerEnd);
			
			//ThreadCounter.addSubject(subject);
			
			//System.out.println(content);
			
			//purge each content
			if(purgeQuotes)
				content = purgeQuotes(content);
			
			Mail addmail = new Mail(subject, extractAuthor(header), content, header);
			
			List<Mail> l = mailMap.get(subject);
            if (l == null)
            	mailMap.put(subject, l = new ArrayList<Mail>());
            l.add(addmail);	
		}
				
		return mailMap;
	}
	
	/**
	 * 
	 * @param mail the file to extract posts from (typically and entire months mailing lists)
	 * @param purgeQuotes purge quotes from saved posts 
	 * @return list of extracted mails
	 */
	public List<Mail> readMail(String mail, boolean purgeQuotes){
		
		//	int index = 0;
			int headerStart =0;
			int start = 0;
			int subStart = 0;
			int end  = 0;
			int subEnd = 0;
			int headerEnd = 0;
		//	boolean foound = false;
			boolean running = true;
			
			List<Mail> posts = new ArrayList<Mail>();
			
			//each mail
			while(running){
				
				String subject;
				String author;
				String content;
				String header;
				
				headerStart = end;
				
				//find start
				start = mail.indexOf("Message-ID:", end);
				
				//break if end of string
				if(start == -1)
					break;
				
				start = mail.indexOf("\n", start) + 1;
				
				//start -= 200;
										
				//find end
				end = mail.indexOf("Message-ID:", start);
				if(end == -1)
					end = mail.length();
				else{
					
					end = mail.lastIndexOf("From:", end);
					end = mail.lastIndexOf("From", end - 1);
				}
					
				if(end < start){
					
					end = mail.indexOf("Message-ID:", start);
					continue;
				}
				
				subStart = mail.indexOf("Subject:", headerStart) + 9;
				subEnd = mail.indexOf("In-Reply-To:", subStart) - 1;
				if(subEnd < 0 || subEnd - subStart > 200){
			
					subEnd = mail.indexOf("References:", subStart) - 1;
					if(subEnd < 0 || subEnd - subStart > 200){
						subEnd = mail.indexOf("Message-ID:", subStart) - 1;
					}
				}
				
				headerEnd = start;			
				subject = mail.substring(subStart, subEnd);
				content = mail.substring(start, end);
				header = mail.substring(headerStart, headerEnd);
				author = extractAuthor(header);
				
				//purge each content
				if(purgeQuotes)
					content = purgeQuotes(content);
				
				Mail addmail = new Mail(subject, author, content, header);
				posts.add(addmail);
			}
					
			return posts;
		}
	
	private String extractAuthor(String source){
		
		
		int startIndex ;
		startIndex = source.indexOf("From:");
		
		try{
			return source.substring(source.indexOf('(',startIndex) + 1, source.indexOf(')',startIndex) - 1);
		}catch(StringIndexOutOfBoundsException e){
			return "N/A";
		}
	}
	
	private String purgeQuotes(String input){
		
		
		int index = 0;
		int lineStart = 0;
		int lineEnd = 0;
		StringBuilder sb = new StringBuilder(input);
		
		//FIX ME
		try{	
			for(;;){
			
				//find next line
				index = sb.indexOf("\n", index);
				
				if(index == -1)
					break;
				
				//catch outof range
				if(index + 1 < sb.length())
					lineStart = index + 1;
				
				//if lines is a qoute delete it
				if(sb.charAt(lineStart) == '>'){
					
					//get line ending
					lineEnd = sb.indexOf("\n", lineStart);
					//if no newline but EOF
					if(lineEnd == -1)
						lineEnd = sb.length();
					
					sb.delete(lineStart, lineEnd + 1);	
					index--;
				}
				
				index++;
			}
		}catch(StringIndexOutOfBoundsException e){
				
			System.err.println("Purge error, skipping purge");
			return input;
		}
		
		return sb.toString();
	}
}
