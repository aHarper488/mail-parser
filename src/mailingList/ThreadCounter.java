package mailingList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class ThreadCounter {

	static ArrayList<String> subjectList = new ArrayList<String>();
	static HashMap<String, Double> retMap = new HashMap<String, Double>();
	static int total = 0;	

	public ThreadCounter() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * counts number of threads from input via hashmap
	 */
	public static void addSubject(String subject){
		
		subjectList.add(subject);
		total++;
		
		/*total++;
		
		Double count = retMap.get(subject);
		if (count == null) {
			retMap.put(subject, (double) 1.0f);
		}
		else {
			retMap.put(subject, count + (double) 1);
		}*/
	}
	
	public static void buildRandomSet(){
		
		Random rand = new Random();
		
		System.out.print(total + " " + subjectList.size());
		
		for(int c = 0; c < 461 ; c++){
			
			int randomNum = rand.nextInt(total + 1);
			
			String subject = subjectList.get(randomNum);
		
			Double count = retMap.get(subject);
			if (count == null) {
				retMap.put(subject, (double) 1.0f);
			}
			else {
				//System.out.print("Double detected");
				retMap.put(subject, count + (double) 1);
			}
		}
	}
	
	public static String getTotal(){
		return Integer.toString(total);
	}
	
	public static String getResults(){
		return Integer.toString(retMap.size());
	}
}
