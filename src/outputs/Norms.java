package outputs;

import java.util.ArrayList;

public class Norms {
	
	int id;
	String norm;
	ArrayList<String> referenceAt;
	ArrayList<String> references;
	
	
	static int counter = 1;
	
	public Norms(String strNorm){
		
		id = counter++;
		norm = strNorm;
		
		referenceAt = new ArrayList<String>();
		references = new ArrayList<String>();
	}
	
	public void addReferenceAt(String ref){
		
		referenceAt.add(ref);
	}
	
	public void addReferences(String ref){
		
		references.add(ref);
	}
	
	public String toString(){
		
		 StringBuilder sb = new StringBuilder();
		 
		 sb.append("\nNorm: \n").append(norm).append("\n\n");
		 
		 if(referenceAt.size() != 0){
			 
			 sb.append("References:\n\n");
			 
			 for(int c = 0; c < referenceAt.size(); c++){
				 		 
				 sb.append(referenceAt.get(c)).append("\n");
				 sb.append(references.get(c)).append("\n\n");
			 }		 			 
		 }
		 
		return sb.toString();
	}
}
