import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import mailingList.MailParser;
import outputs.OutputWritter;


//sax xml parser

public class Project {
	
	public static void main(String[] args) {
		runMailParser("Python");
	}
	
	/**
	 * @param dir source directory that the mailing archives are stored under prefixed with "inputs/mailinglist/"
	 */
	public static void runMailParser(String dir){
		
		MailParser par = new MailParser(dir);
		par.Run();
		System.out.print("Outputting...");
		
		
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(Calendar.getInstance().getTime());
		File dirPath = new File("outputs/Threads/" + dir + "-" + timeStamp);
		dirPath.mkdir();
		for(mailingList.Thread t : par.getThreads()){
			OutputWritter ow = new OutputWritter(t.toString(), "outputs/Threads/" + dir + "-" + timeStamp + "/" + t.getWeight() + " " + t.getId() + ".txt");
		}
		
		OutputWritter ow = new OutputWritter(par.getStats(), "outputs/Threads/" + dir + "-" + timeStamp + "/" + "Stats.txt");
		System.out.print("Done");
		return;
	}	
}
